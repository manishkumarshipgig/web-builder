# Readme #

## Table of Contents ##

- [About this project](#about-this-project)
	- [What is PrismaNote?](#what-is-prismanote)
	- [How do the social media tool, CRM tool, and website/webshop tool work?](#how-do-the-social-media-tool-crm-tool-and-websitewebshop-tool-work)
	- [Social Media tool](#social-media-tool)
	- [CRM & Emailmarketing tool](#crm--emailmarketing-tool)
	- [Website & Webshop tool](#website--webshop-tool)
	- [Who is using PrismaNote besides jewellers?](#who-is-using-prismanote-besides-jewellers)
	- [Concepts of the old version which we’re currently brainstorming about:](#concepts-of-the-old-version-which-were-currently-brainstorming-about)
- [Directory structure](#directory-structure)
- [Getting started](#getting-started)
- [Version control](#version-control)
- [Running the project](#running-the-project)
- [Build process](#build-process)
- [Testing](#testing)
- [Dependencies](#dependencies)
- [Deployment](#deployment)

## About this project ##

### What is PrismaNote? ###
Firstly, PrismaNote helps jewellers with their E-commerce efforts. In order to do that, we have three main solutions: 

1. Social Media tool (do easy marketing)
2. Website/webshop building tool (build your online shop in minutes) 
3. CRM/ Emailmarketing tool (shop loyalty program). 

The PrismaNote modules are additional to eachother. Its very important that PrismaNote works together with POS software (mainly Clarity & Success) so that products will be removed from the website automatically if they are sold through the POS. We want to provide this connection to Clarity & Success as a basic feature. PrismaNote is sold by Excellent Electronics. Starting from the Netherlands.

### How do the social media tool, CRM tool, and website/webshop tool work? ###

#### Social Media tool ####

<https://www.prismanote.com/social> 

The Admin adds “Posts” to the timeline. Post’s contain one or more actions that a retailer can do separately or do them all. Doing all the tasks of a post can be done easily by adding the actions to the Social Media Calender. In this way retailers get the tasks on the date they should be done.

#### CRM & Emailmarketing tool ####
Retailers add customers via their loyalty program. In this program a photo of a receipt or warranty card could be easily sent to the customer's email adress. With this proces the retailer effortlessly collects data from his/her clients. The system sends all the added customers service emails automatically.

#### Website & Webshop tool ####
The primary goal is to build a working tool in which a jeweller website/webshop could be managed easily. We provide all jewellers product information from a central database which saves the jeweller a lot of time because the products don't have to be added by the retailer manually.

### Who is using PrismaNote besides jewellers? ###
B2B value of PrismaNote is very important. The use of the system in the future relies on adoption by the branche. Manufacturiers/distributors should init their productinformation/marketing content. Other users are consumers, who are mainly added by jewellers through the CRM-tool.

### Concepts of the old version which we’re currently brainstorming about: ###
* Watchstrap with NFC-chip inside. We’re still brainstorming if we keep this feature into the system. If so we need to add a “note-ID” into every consumer-request from the jewellers loyalty program and make the information public accessible by adding the “Note-ID” into a public link.
* Consumer platform working with a provision based business model like Amazon. It differs from Amazon because on this platform consumers can find detailed information about where to get their watch. Where is my product located, so I can view it in a shop. Currently our main sales & marketing focus is on jewellers. This is because we need to build a up an active group of jewellers first before starting a consumer platform.

## Directory structure ##

```
.
+-- /api [Node.js backend using Express.js, Mongoose] 
|   +-- /config
|       +-- [config files to initialize the API, e.g. environment variables, db config or Express router]
|   +-- /controllers
|       +-- [Express controllers for the REST API]
|   +-- /import
|       +-- [Database contents that are to be imported into the database using an import script. Has been used to migrate data from an old version]
|   +-- /mail-templates
|       +-- [Pug templates for default e-mails]
|   +-- /models
|       +-- [Mongoose models]
|   +-- /schema
|       +-- /interfaces
|           +-- [GraphQL interface types that can be implemented by object types]
|       +-- /types
|           +-- [GraphQL object types for different kinds of data]
|       +-- enums.graphql [Contains all enum values. Could be changed to a directory later if the schema becomes very large]
|       +-- mutation.type.graphql [GraphQL root mutation query, describes all possible PUT/POST/DELETE-type mutation queries]
|       +-- query.type.graphql [GraphQL root query, describes all possible GET-type queries]
|       +-- scalars.graphql [Contains all custom scalars for the schema]
|       +-- subscription.type.graphql [GraphQL root subscription query, can be used later to subscribe to live content changes in the frontend]
|   +-- /services
|       +-- [Different general-purpose services/utils that can be used in controllers or other parts of the API when necessary. E.g. file operations, mailing, throwing errors, etc.]
|   +-- app.js [The main entry point where the app is started/initialized]
|   +-- test-config.spec.js [The testing setup that is used to initialize unit testing with Mocha, Chai, Sinon]
+-- /app [Frontend using AngularJS, Pug, Stylus]
|   +-- /controllers
|       +-- [Frontend controllers, in part grouped by part of the application it belongs to. TODO: make directory structure more consistent and practical]
|   +-- /directives
|       +-- [Angular directives]
|   +-- /filters
|       +-- [Angular filters]
|   +-- /images
|       +-- [Images that are used for the website layout]
|   +-- /libs
|       +-- [Third-party libraries that are not included using a CDN. Are compiled into /scripts/libs.js and /scripts/libs.min.js when building]
|   +-- /routes
|       +-- [Angular routing using UI-router (state-based navigation)]
|   +-- /scripts
|       +-- [Compiled and minified JS from libraries as well as Angular directives, filters, controllers, etc.]
|   +-- /services
|       +-- [General JS files that can be used in different controllers when required. E.g. the shopping cart or asynchronous API calls]
|   +-- /style
|       +-- [Stylus files and the resulting compiled CSS stylesheets]
|   +-- /views
|       +-- /account
|           +-- [User portal ('My Account') related HTML files]
|       +-- /admin
|           +-- [Admin portal related HTML files]
|       +-- /brand
|           +-- [Brand portal related HTML files]
|       +-- /consumer
|           +-- [Consumerplatform (horlogemagazijn.nl) portal related HTML files]
|       +-- /css
|           +-- style.css [Single css source file that is compiled by Gulp from the source files in /app/style]
|       +-- /fonts
|           +-- [FontAwesome and Bootstrap glyphicons. These are all used as icons, the actual fonts are imported from Google Fonts]
|       +-- /modal
|           +-- [HTML templates for Angular-UI modal windows]
|       +-- /pug
|           +-- [Pug templates using the same directory structure as the HTML files in the parent folder. Will be compiled to HTML at build time]
|       +-- /retailer
|           +-- [Retailer portal related HTML files]
|       +-- /shop
|           +-- [Shop and pro-shop related HTML files]
|       +-- [Compiled HTML files]
|   +-- app.js [Entry point for the frontend Angular app]
|   +-- index.js [?]
|   +-- index.html [Initial page that is sent to the user from the API and includes the Angular app]
+-- /node_modules
|   +-- [All npm dependencies for the project]
+-- /public
|   +-- /static
|       +-- [Product images on the production server]
|       +-- [Possibly other publicly available files that are not part of the app itself]
+-- .gitignore [Contains some files that can be customized by the devs or that are otherwise not neccessary to keep track of in the git repo]
+-- CHANGE.md [Changelog]
+-- gulpfile.js [Gulp scripts for building. Mainly to compile Stylus, Pug and Angular code into a few minified source files]
+-- package.json [The Node.js package file including dev and production dependencies, description, launch and test scripts, etc.]
+-- README.md [General information about the project and instructions for getting it up and running]
```

## Getting started ##

* Install a git client (like the git CLI, SourceTree or GitKraken).
* Install an IDE (like Visual Studio Code, Atom, WebStorm). Or use vim/emacs if you're hardcore.
* Install NodeJS. Use at least version 8.x.x, because of ES6 support. https://nodejs.org/en/download/
* Install Visual Studio for the compiler (Manual compile is possible with gulp, but VSC has beautifull commandline integration).
* Install Python (2.7.3).
* Install MongoDB version 3.4.x (3.2.x does not have certain Aggregation features that the project needs, so it is not supported). <https://www.mongodb.com/download-center?jmp=nav>

* Add to system variabeles: NODE_PATH with value: 

```
#!cmd

%AppData%\npm\node_modules
```
and add 
```
#!cmd

'%AppData%\npm\'
```
as first in 'PATH'

* Use your favorite git client to get the code on your pc
* Access to the folder with cmd and run the following commands to get all the right files on the right place.
```
npm instal && npm dedupe;
gulp all
```
* Start MongoDB and NodeJS via commandline or via the debugger in Visual Studio Code (use default settings for a nodeJS environment)
* Open source in Visual Studio Code and start gulp with CTRL+SHIFT+B, use this to setup gulp if needed
```
{
	"version": "2.0.0",
	"command": "gulp",
	"tasks": [
		{
			"label": "default",
			"group": "build",
			"type": "shell",
			"promptOnClose": false,
			"presentation": {
				"echo": true,
				"reveal": "always",
				"focus": false,
				"panel": "shared"
			},
			"isBackground": true
		}
	]
}

```
* Project can be accessed via localhost:3000

## Version control ##

We use git for version control and store the repository on BitBucket.

## Running the project ##

* Navigate to your local project root directory in the command line of your choice (cmd, powershell, bash, ...).
* Start the MongoDB server by running 'mongod' in this directory.
* (Optional) Start the MongoDB shell by typing 'mongo'. This can be used to run commands on the database.
* Run 'node api/app.js'. You can also use Nodemon if you have it installed: 'nodemon api/app.js'.
* Open your browser and go to http://127.0.0.1:3000/ or http://localhost:3000 and you should see the PrismaNote website running locally.

## Build process ##

We use gulp tasks to build the project. The build scripts include compiling, merging and minifying Stylus files to CSS stylesheets and Pug templates to HTML files. It also includes merging and minifying all AngularJS code and third-party libraries to scripts.js, scripts.min.js, libs.js, and libs.min.js.

The task 'all' compiles and minifies everything, whereas the default task watches all files and compiles only when a file changes. Run 'gulp default' from the command line or use the F1 key and type 'task' in VS Code to run the default task.

## Testing ##

We use Mocha, Chai and Sinon for unit testing. Full production tests can be made on the 'testing' branche. That branche will be merged into the 'master'. 
To test external services like Mollie (payments) and Amazon SES/SNS (email/feedback on send emails) we use ngrok (prismanote.ngrok.io). That address is only available to one specific account. Ask @niekvanoost_excellent for more details. 

Images and some other files are hosted on a S3 bucket. We have 2 buckets, prismanote and prismanotetest. The code should automatically reffer to the correct one using the node environment variables.  

## Dependencies ##

Open package.json to see all development and production dependencies or run 'npm ls' in the project root directory for all installed dependencies (should be identical to package.json on a working installation, but the minor version numbers may differ).

## Deployment ##

Commits on the master branch have a BitBucket hook that pulls the newest version to the production server and runs the autodeploy script. Only push to the master branch when you're sure it is ready for production!

