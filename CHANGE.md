# Changelog #

## 0.2.0 ##

Major version number is 0 because the project is not yet fully stable. Minor version number is 2 because the project has been refactored completely since the first version went live.

* Start to use versioning for existing prismanote project.

- 2458099.0108782174
- 2458099.0143296067
- 2458137.942156134
- 2458137.945933009
- 2458137.9480674886
- 2458137.9524970604
