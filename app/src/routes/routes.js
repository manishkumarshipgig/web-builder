prismanoteApp.config(['$locationProvider', '$sceProvider', '$stateProvider', '$urlMatcherFactoryProvider', '$urlRouterProvider',
	function($locationProvider, $sceProvider, $stateProvider, $urlMatcherFactoryProvider, $urlRouterProvider) {

		$urlMatcherFactoryProvider.strictMode(false);

		$urlRouterProvider.rule(function($injector, $location) {
			var path = $location.path();
			var hasTrailingSlash = path[path.length-1] === '/';

			if(hasTrailingSlash && !location.search) {
				//if last charcter is a slash, return the same url without the slash  
				var newPath = path.substr(0, path.length - 1); 
				return newPath; 
			}
		});
		var hostnames = ['prismanote.com', 'www.prismanote.com', 'nieuw.prismanote.com', '127.0.0.1', 'localhost', 'prismanote.ngrok.io', 'goedenoot.nl', 'www.goedenoot.nl', 'www.prismanote.it', 'prismanote.it'];
		var getShopType = function() {
			if (hostnames.indexOf(window.location.hostname) == -1) {
				return "proshop";
			} else {
				return "shop";
			}
		}
		var shop = getShopType();

		$urlRouterProvider
			// Show homepage by default on the shop/pro-shop.
			.when('/shop/:nameSlug', '/shop/:nameSlug/home')
			.when('/extern/:nameSlug', '/extern/:nameSlug/home')
			.when('/iframe/:nameSlug', '/iframe/:nameSlug/home')
			// Show homepage by default in the admin portal.
			.when('/admin', '/admin/home')

			//Show homepage by default in the retailer portal
			.when('/retailer', '/retailer/home')
			//Show homepage by default in the user account page
			.when('/my-account', '/my-account/home')
			.when('/brand', '/brand/welcome')

			// Redirect invalid routes to homepage.
			.otherwise('/home');

			$stateProvider
			.state('extern', {
				url: '/extern/:nameSlug/:page',
				controller: ['$stateParams', '$state', function($stateParams, $state) {
					localStorage.setItem("nameSlug", $stateParams.nameSlug);
					$state.go('proshop.' + $stateParams.page, {nameSlug: $stateParams.nameSlug});
				}],
			})

			.state('iframe', {
				url: '/iframe/:nameSlug/:page',
				controller: 'iframeShopController',
				templateUrl: '/views/iframe/shop.html'
			})
			
			.state("shop", {
				abstract: true,
				url: '/shop/:nameSlug',
				templateUrl: '/views/shop/index.html',
				controller: 'shopController',
				params: {
					nameSlug: null,
					transactionid: null
				}
			})

			.state("proshop", {
				abstract: true,
				templateUrl: '/views/shop/index.html',
				controller: 'shopController',
				params: {
					nameSlug: null,
					transactionid: null
				}
			})

			.state(shop + '.home', {
				url: '/home',
				templateUrl: '/views/shop/home.html',
			})

			.state("builderhome", {
				url: "/web-builder-home",
				templateUrl: "/views/grapes/prisma-editor-home.html",
				controller: 'grapeHomeController'
			})
			.state("buildertemplate", {
				url: "/web-builder-template",
				templateUrl: "/views/grapes/template-chooser.html",
				controller: 'grapeTemplateController'
			})
			.state("buildereditor", {
				url: "/web-builder-editor",
				templateUrl: "/views/grapes/editor-page.html",
				controller: 'grapeEditorController'
			})

			.state(shop + '.product', {
				url: '/products/:productNameSlug',
				templateUrl: '/views/product.html',
				controller: 'productController'
			})

			.state(shop + '.products', {
				url: '/products',
				templateUrl: '/views/products.html',
				params: {
					category: null
				}
			})

			.state(shop + '.brand', {
				url: '/brands/:brandNameSlug',
				templateUrl: '/views/brand.html',
				controller: 'brandController'
			})

			.state(shop + '.brands', {
				url: '/brands',
				templateUrl: '/views/brands.html'
			})

			.state(shop + '.campaign', {
				url: '/campaigns/:campaignNameSlug',
				templateUrl: '/views/campaign.html',
				controller: 'campaignController'
			})

			.state(shop + '.campaigns', {
				url: '/campaigns',
				templateUrl: '/views/campaigns.html'
			})

			.state(shop + '.reviews', {
				url: '/reviews',
				templateUrl: '/views/shop/reviews.html',
				controller: 'shopReviewController'
			})

			.state(shop + '.contact', {
				url: '/contact',
				templateUrl: '/views/shop/contact.html'
			})

			.state(shop + '.loyalty-program', {
				url: '/loyalty-program',
				templateUrl: '/views/shop/loyalty-program.html',
				controller: 'loyaltyController'
			})

			.state(shop + '.loyalty-watches', {
				url: '/loyalty-watches',
				templateUrl: '/views/shop/loyalty-watches.html',
				controller: 'loyaltyController',
			})

			.state(shop + '.loyalty-other', {
				url: '/loyalty-other',
				templateUrl: '/views/shop/loyalty-other.html',
				controller: 'loyaltyController'
			})

			.state(shop + '.loyalty-repair', {
				url: '/loyalty-repair',
				templateUrl: '/views/shop/loyalty-repair.html',
				controller: 'loyaltyController'
			})
			
			.state(shop + '.loyalty-special', {
				url: '/loyalty-special',
				templateUrl: '/views/shop/loyalty-special.html',
				controller: 'loyaltyController'
			})

			.state('loyalty-repair-settings', {
				url: '/loyalty-repair/settings',
				templateUrl: '/views/shop/loyalty-repair-settings.html',
				controller: 'loyaltySettingsController'
			})

			.state(shop + '.custom-page', {
				url: '/page/:customPageNameSlug',
				templateUrl: '/views/shop/custom-page.html'
			})

			.state(shop + '.cart', {
				url: '/cart',
				templateUrl: '/views/cart.html'
			})
			.state(shop + '.checkout', {
				url: '/checkout',
				templateUrl: '/views/checkout.html',
				controller: 'checkoutController'
			})
			.state(shop + '.payment', {
				url: '/payment/:payId',
				templateUrl: 'views/payment.html',
				controller: 'checkoutController',
				params: {
					payId: null,
				}
			})
			.state(shop + '.news', {
				url: '/news/:newsItemSlug',
				templateUrl: '/views/news-item.html',
				controller: 'shopNewsController'
			})

			.state('admin', {
				abstract: true,
				templateUrl: '/views/admin/index.html',
				data: {
					restricted: true,
					role: "admin"
				}
			})
			
			.state('admin.home', {
				url: '/admin/home',
				templateUrl: '/views/admin/home.html',
				controller: 'adminHomeController'
			})

			.state('admin.users', {
				url: '/admin/users',
				templateUrl: '/views/admin/users.html',
				controller: 'usersController'
			})

			.state('admin.user', {
				url: '/admin/user/:userId',
				templateUrl: '/views/admin/user-edit.html',
				controller: 'usersController'
			})

			.state('admin.add-user', {
				url: '/admin/users/add',
				templateUrl: '/views/add-user.html',
				controller: 'addUserController'
			})

			.state('admin.shops', {
				url: '/admin/shops',
				templateUrl: '/views/admin/shops.html',
				controller: 'shopsController'
			})

			.state('admin.shop', {
				url: '/admin/shop/:nameSlug',
				templateUrl: '/views/admin/shop-edit.html',
				controller: 'shopsController'
			})

			.state('admin.brands', {
				url: '/admin/brands',
				templateUrl: '/views/admin/brands.html',
				controller: 'brandsController'
			})

			.state('admin.brand', {
				url: '/admin/brand/:nameSlug',
				templateUrl: '/views/admin/brand-edit.html',
				controller: 'adminBrandsController'
			})

			.state('admin.campaigns', {
				url: '/admin/campaigns',
				templateUrl: '/views/admin/campaigns.html',
				controller: 'adminCampaignsController'
			})

			.state('admin.campaign', {
				url: '/admin/campaigns/:nameSlug',
				templateUrl: '/views/admin/campaign.html',
				controller: 'adminCampaignController'
			})

			.state('admin.social-portals', {
				url: '/admin/social-portals',
				templateUrl: '/views/admin/social-portals.html',
				controller: 'socialPortalsController'
			})

			.state('admin.news', {
				url: '/admin/news',
				templateUrl: '/views/admin/news.html',
				controller: 'adminNewsController'
			})

			.state('admin.news-item', {
				url: '/admin/news/:nameSlug',
				templateUrl: '/views/admin/news-item.html',
				controller: 'adminNewsItemController'
			})

			.state('admin.news-item-new', {
				url: '/admin/news/news-item-new',
				templateUrl: '/views/admin/news-item-new.html',
				controller: 'adminNewsItemController'
			})

			.state('admin.products', {
				url: '/admin/products',
				templateUrl: '/views/admin/products.html',
				controller: 'adminProductsController'
			})
			.state('admin.products-to-merge', {
				url: '/admin/products-to-merge',
				templateUrl: '/views/admin/products-to-merge.html',
				controller: 'adminProductsToMergeController'
			})
			.state('admin.products-to-delete', {
				url: '/admin/products-to-delete',
				templateUrl: '/views/admin/products-to-delete.html',
				controller: 'adminProductsToDeleteController'
			})
			//CREATED BECAUSE I COULDN'T FIND ADD PRODUCT PAGE IN ADMIN.
			.state('admin.newProduct', {
				url: '/admin/add-product',
				templateUrl: '/views/admin/product.html',
				controller: 'adminProductController',
			})
			.state('admin.product', {
				url: '/admin/edit-product/:nameSlug',
				templateUrl: '/views/admin/product.html',
				controller: 'adminProductController',
			})
			//- verifing submitted imports
			.state('admin.imports', {
				url: '/admin/imports',
				templateUrl: '/views/admin/imports.html',
				//-controller: 'adminImportsController'
			})
			//- checking result of one submitted import
			.state('admin.import', {
				url: '/admin/import',
				templateUrl: '/views/admin/import.html',
				//-controller: 'adminImportsController'
			})
			//- route for doing an import
			.state('admin.product-import', {
				url: '/admin/product-import',
				templateUrl: '/views/admin/product-import.html',
				controller: 'adminProductImportController'
			})
			.state('admin.wholesalers', {
				url: '/admin/wholesalers',
				templateUrl: '/views/admin/wholesalers.html',
				controller: 'adminWholesalerController'
			})
			.state('admin.wholesaler', {
				url: '/admin/wholesaler/:nameSlug',
				templateUrl: '/views/admin/wholesaler-edit.html',
				controller: 'adminWholesalerController'
			})
			.state('admin.clarity', {
				url: '/admin/clarity/articlebase',
				templateUrl: '/views/admin/articlebase.html',
				controller: 'adminArticleBaseController'
			})
			.state('admin.product-collections', {
				url: '/admin/product-collections',
				templateUrl: '/views/admin/product-collections.html',
				controller: 'adminProductCollectionsController'
			})
			.state('admin.collection', {
				url: '/admin/collection/:id',
				templateUrl: '/views/admin/collection.html',
				controller: 'adminProductCollectionsController'
			})

			.state('brand', {
				abstract: true,
				templateUrl: '/views/brand/index.html',
				controller: 'wholesalerController'
			})
			.state('brand.welcome', {
				url: '/brand/welcome',
				templateUrl: '/views/brand/welcome.html',
				controller: 'welcomeBrandController',
				breadcrumb: 'Welcome'
			})

			.state('brand.new-campaign', {
				url: '/brand/new-campaign',
				templateUrl: '/views/brand/new-campaign.html',
				controller: 'brandNewCampaignController',
				breadcrumb: 'New campaign'
			})

			.state('brand.home', {
				url: '/brand/home',
				templateUrl: '/views/brand/home.html',
				breadcrumb: 'Home',
				data: {
					restricted: true,
					role: "wholesaler"
				}
			})

			.state('brand.company-info', {
				url: '/brand/company-info',
				templateUrl: '/views/brand/company-info.html',
					//-controller: 'brandHomeController',
					breadcrumb: 'Company Info',
					data: {
						restricted: true,
						role: "wholesaler"
					}
				})
			.state('retailer.new_campaign', {
				url: '/retailer/new-campaign',
				templateUrl: '/views/retailer/new-campaign.html',
				controller: 'retailerCampaignController',
			})
			.state('brand.myuser', {
				url: '/brand/user',
				templateUrl: '/views/brand/user.html',
				controller: 'brandUserController',
				breadcrumb: 'User',
				data: {
					restricted: true,
					role: "wholesaler"
				}
			})
			.state('brand.marketingUser', {
				url: '/brand/marketing-user',
				templateUrl: '/views/brand/marketingUser.html',
				controller: 'brandMarketingUserController',
				data: {
					restricted: true,
					role: "wholesaler"
				}
			})


			.state('brand.brand', {
				url: '/brand/brand/:nameSlug',
				templateUrl: '/views/brand/brand-edit.html',
				controller: 'brandBrandsController',
				breadcrumb: 'Brands',
				data: {
					restricted: true,
					role: "wholesaler"
				}
			})

			.state('brand.performance', {
				url: '/brand/performance',
				templateUrl: '/views/brand/performance.html',
				controller: 'brandPerformanceController',
				breadcrumb: 'Performance',
				data: {
					restricted: true,
					role: "wholesaler"
				}
			})
			.state('brand.shops', {
				url: '/brand/shops',
				templateUrl: '/views/brand/shops.html',
					//-controller: 'brandHomeController',
					breadcrumb: 'Shops and Contributions',
					data: {
						restricted: true,
						role: "wholesaler"
					}
				})
			.state('brand.social-portals', {
				url: '/brand/social-portals',
				templateUrl: '/views/brand/social-portals.html',
					//-controller: 'brandHomeController',
					breadcrumb: 'Shops and Contributions',
					data: {
						restricted: true,
						role: "wholesaler"
					}
				})
			.state('brand.campaigns', {
				url: '/brand/campaigns',
				templateUrl: '/views/brand/campaigns.html',
				controller: 'brandCampaignsController',
				breadcrumb: 'Social Media',
				data: {
					restricted: true,
					role: "wholesaler"
				}
			})
			.state('brand.campaign', {
				url: '/brand/campaigns/:nameSlug',
				templateUrl: '/views/brand/campaign.html',
				controller: 'brandCampaignController',
				breadcrumb: 'Social Media',
				data: {
					restricted: true,
					role: "wholesaler"
				}
			})

			.state('retailer', {
				abstract: true,
				templateUrl: '/views/retailer/index.html',
				controller: 'retailerController',
				data: {
					restricted: true,
					role: "retailer"
				}
			})
			.state('retailer.upgrade', {
				url: '/retailer/upgrade?return&orderid',
				templateUrl: '/views/retailer/upgrade.html',
				controller: 'retailerUpgradeController',
				breadcrumb: 'Upgrade'
			})
			
			.state('retailer.video-course', {
				url: '/retailer/video-course',
				templateUrl: '/views/retailer/video-course.html',
				//-controller: 'retailerUpgradeController',
				breadcrumb: 'Course'
			})

			.state('retailer.home', {
				url: '/retailer/home',
				templateUrl: '/views/retailer/home.html',
				controller: 'retailerHomeController',
				breadcrumb: 'Home'
			})
			.state('retailer.add-campaign', {
				url: '/retailer/add-campaign',
				templateUrl: '/views/retailer/add-campaign.html',

			})
			.state('retailer.myshop', {
				url: '/retailer/shop',
				templateUrl: '/views/retailer/shop.html',
				controller: 'retailerShopController',
				breadcrumb: 'Mijn winkel'
			})

			.state('retailer.myuser', {
				url: '/retailer/user',
				templateUrl: '/views/retailer/user.html',
				controller: 'retailerUserController',
				breadcrumb: 'Mijn profiel'
			})

			.state('retailer.assortment', {
				url: '/retailer/assortment',
				templateUrl: '/views/retailer/assortment.html',
				controller: 'retailerAssortmentController',
				breadcrumb: 'Assortiment',
				params:      {'modalFor': null},

			})

			.state('retailer.product-import', {
				url: '/retailer/assortment/import',
				templateUrl: '/views/retailer/product-import.html',
				controller: 'retailerProductImportController'
			})

			.state('retailer.add-product', {
				url: '/retailer/assortment/add-product',
				templateUrl: '/views/retailer/product.html',
				controller: 'retailerProductController',
				breadcrumb: 'Add product'
			})

			.state('retailer.edit-product', {
				url: '/retailer/assortment/edit-product/:nameSlug',
				templateUrl: '/views/retailer/product.html',
				controller: 'retailerProductController',
				breadcrumb: 'Edit product'
			})

			//- verifing submitted imports
			.state('retailer.imports', {
				url: '/retailer/imports',
				templateUrl: '/views/retailer/imports.html',
				//-controller: 'retailerImportsController'
			})
			//- checking result of one submitted import
			.state('retailer.import', {
				url: '/retailer/import',
				templateUrl: '/views/retailer/import.html',
				//-controller: 'retailerImportsController'
			})

			.state('retailer.orders', {
				url: '/retailer/orders',
				templateUrl: '/views/retailer/orders.html',
				controller: 'retailerOrdersController',
				breadcrumb: 'Orders'
			})

			.state('retailer.purchase-orders', {
				url: '/retailer/purchase-orders',
				templateUrl: '/views/retailer/purchase-orders.html',
				//-controller: 'retailerPurchaseOrdersController'
			})

			.state('retailer.purchase-order', {
				url: '/retailer/purchase-order',
				templateUrl: '/views/retailer/purchase-order.html',
				//-controller: 'retailerPurchaseOrderController'
			})
			.state('retailer.loyalty-portal', {
				url: '/retailer/loyalty-portal',
				templateUrl: '/views/retailer/loyalty-portal.html',
				controller: 'retailerLoyaltyPortalController',
				breadcrumb: 'Premium Service'
			})

			.state('retailer.loyalty-portal-settings', {
				url: '/retailer/loyalty-portal/settings',
				templateUrl: '/views/retailer/loyalty-portal-settings.html',
				controller: 'retailerLoyaltyPortalController',
				breadcrumb: 'Premium Service Settings'
			})

			.state('retailer.campaigns', {
				url: '/retailer/campaigns',
				templateUrl: '/views/retailer/campaigns.html',
				controller: 'retailerCampaignsController',
				breadcrumb: 'Acties'
			})

			.state('retailer.campaign', {
				url: '/retailer/campaigns/:nameSlug',
				templateUrl: '/views/retailer/campaign.html',
				controller: 'retailerCampaignController'
			})

			.state('retailer.news', {
				url: '/retailer/news',
				templateUrl: '/views/retailer/news.html',
				controller: 'retailerNewsController'
			})

			.state('retailer.news-item', {
				url: '/retailer/news/:nameSlug',
				templateUrl: '/views/retailer/news-item.html',
				controller: 'retailerNewsItemController'
			})

			.state('retailer.news-item-new', {
				url: '/retailer/news/news-item-new',
				templateUrl: '/views/retailer/news-item-new.html',
				controller: 'retailerNewsItemController'
			})

			.state('retailer.financial', {
				url: '/retailer/financial',
				templateUrl: '/views/retailer/financial.html',
				controller: 'retailerFinancialController'
			})

			.state('retailer.payments', {
				url: '/retailer/financial/payments',
				templateUrl: '/views/retailer/payments.html',
				controller: 'retailerPaymentsController'
			})

			.state('retailer.suppliers', {
				url: '/retailer/suppliers',
				templateUrl: '/views/retailer/suppliers.html',
				controller: 'retailerCampaignsController',
				breadcrumb: 'Bijdragen van leveranciers'
			})

			.state('retailer.crmuser-item', {
				url: '/retailer/:crmUserId/crmuser-item/:crmUserItemId',
				templateUrl: '/views/retailer/crmuser-item.html',
				controller: 'retailerCrmUserItemController',
			})

			.state('consumer', {
				abstract: true,
				templateUrl: '/views/layout.html'
			})
			.state('consumer.home', {
				url: "/consumer/home",
				templateUrl: '/views/consumer/home.html',
				controller: 'consumerHomeController'
			})

			.state('layout', {
				abstract: true,
				templateUrl: '/views/layout.html'
			})

			.state('layout.welcome-retailer', {
				url: '/welcome-retailer',
				templateUrl: '/views/welcome-retailer.html',
				controller: 'welcomeRetailerController',

			})

			.state('layout.welcome-campaigns', {
				url: '/retailer/welcome-campaigns/:wholesalerName/:hashedData/:brandId',
				templateUrl: '/views/welcome-campaigns.html',
				controller : 'welcomeCampaignController'
			})


			.state('layout.home', {
				url: "/home",
				templateUrl: '/views/home.html',
				controller: 'homeController',
			})

			.state('layout.product', {
				url: '/products/:nameSlug',
				templateUrl: '/views/product.html',
				controller: 'productController'
			})

			.state('layout.products', {
				url: '/products',
				templateUrl: '/views/products.html',
				controller: 'productsController',
				data: {
					pageTitle: 'Producten'
				},
				params: {
					category: null
				}
			})

			.state('layout.brand', {
				url: '/brands/:nameSlug',
				templateUrl: '/views/brand.html',
				controller: 'brandController'
			})

			.state('layout.brands', {
				url: '/brands',
				templateUrl: '/views/brands.html',
				controller: 'brandsController'
			})

			.state('layout.shops', {
				url: '/shops',
				templateUrl: '/views/shops.html',
				controller: 'shopsController'
			})

			.state('layout.news', {
				url: '/news',
				templateUrl: '/views/news-items.html',
				controller: 'newsItemsController'
			})

			.state('layout.news-item', {
				url: '/news/:nameSlug',
				templateUrl: '/views/news-item.html',
				controller: 'newsItemController'
			})

			.state('layout.cart', {
				url: '/cart',
				templateUrl: '/views/cart.html'
			})
			.state('layout.checkout', {
				url: '/checkout',
				templateUrl: '/views/checkout.html',
				controller: 'checkoutController'
			})

			.state('layout.checkout-login', {
				url: '/checkout-login',
				templateUrl: '/views/checkout-login.html',
				controller: 'checkoutController'
			})

			.state('layout.terms-of-service', {
				url: '/terms-of-service',
				templateUrl: '/views/terms-of-service.html'
			})

			.state('layout.privacy-policy', {
				url: '/privacy-policy',
				templateUrl: '/views/privacy-policy.html'
			})

			.state('layout.activate-user', {
				url: '/activate-user/:id/:code',
				templateUrl: 'views/activate-user.html',
				controller: 'authController'
			})

			.state('layout.access-denied', {
				url: '/access-denied/:to',
				templateUrl: 'views/acces-denied.html',
				controller: 'authController'
			})

			.state('layout.payment/', {
				url: '/payment/:payId',
				templateUrl: '/views/payment.html',
				controller: 'checkoutController',
				params: {
					payId: null,
				}
			})
			.state('layout.my-account',{
				url: '/my-account',
				templateUrl: 'views/account/index.html',
				controller: 'accountController',
				data: {
					restricted: true,
				}
			})
			.state('layout.my-account.home',{
				url: '/home',
				templateUrl: 'views/account/home.html',
			})
			.state('layout.my-account.my-details',{
				url: '/my-details',
				templateUrl: 'views/account/my-details.html',
			})
			.state('layout.my-account.orders',{
				url: '/orders',
				templateUrl: 'views/account/orders.html',
			})
			.state('layout.my-account.notes',{
				url: '/notes',
				templateUrl: 'views/account/notes.html',
			})
			.state('layout.my-account.password',{
				url: '/password',
				templateUrl: 'views/account/password.html',
			})

			.state('layout.logout',{
				url: '/logout',
				templateUrl: 'views/logout.html',
			})

			.state('layout.instruction-crmApp',{
				url: '/instruction-crmApp',
				templateUrl: 'views/instruction-crmApp.html',
				controller: 'instructionCrmApp'
			})

			.state('layout.resetpassword',{
				url: '/reset-password/:id/:code',
				templateUrl: 'views/reset-password.html',
				controller: 'authController'
			})

			.state('layout.setpassword',{
				url: '/set-password/:id/:code',
				templateUrl: 'views/set-password.html',
				controller: 'authController'
			})

			.state('layout.brandblacklist', {
				url: '/unsubscribe-brand/:code',
				templateUrl: 'views/unsubscribe-brand.html',
				controller: 'unsubscribeBrandController'
			})
			;

			$locationProvider.html5Mode({enabled: true, requireBase: false});
			$sceProvider.enabled(false); 
		}
		]);
