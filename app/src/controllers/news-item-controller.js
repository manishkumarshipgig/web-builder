prismanoteApp.controller('newsItemController', ['$scope', '$rootScope', '$stateParams' ,'$api', 
	function($scope, $rootScope, $stateParams, $api) {

		$stateParams.newsItemNameSlug ? this.nameSlug = $stateParams.newsItemNameSlug : this.nameSlug = $stateParams.nameSlug;

		$api.get('news/' + $stateParams.nameSlug)
		
			.then(function(response) {
				$scope.newsItem = response.data.newsItem;

				$rootScope.breadcrumb = ['Nieuws', $scope.newsItem.name];
				$rootScope.pageTitle = 'PrismaNote | ' + $scope.newsItem.name;

				$scope.newsItem.content = $scope.newsItem.content.trim();
			})
			
			.catch(function(reason) {
				console.log(reason);
			})
			
			.finally(function() {
				
				$api.get('news', {'sort': {'publicationDate': 'desc'}, 'limit': 6})
				
					.then(function(response) {
						$scope.recentNews = response.data.newsItems;
					})
					
					.catch(function(reason) {
						console.log(reason);
					});
			});
}]);
