prismanoteApp.controller("checkoutController", ['$scope', '$rootScope', '$cart', '$location', '$api', '$window', '$state', '$stateParams', '$timeout', '$uibModal','campaignFact',
	function($scope, $rootScope, $cart, $location, $api, $window, $state,$stateParams, $timeout, $uibModal,campaignFact) {
	$scope.step = ['Cart'];
	$rootScope.breadcrumb = $scope.step;
	$scope.alert ="";
	$scope.submitted = false;

	if(campaignFact.campBudgetCheckout != 0){
		$scope.total = campaignFact.campBudgetCheckout;
		campaignFact.campBudgetCheckout = 0;
	}
	// Get the shopping cart once from the session and use that from now on, to prevent changes to the cart while the order is being processed.
	$cart.get();
	
	$scope.initShoppingCart = function() {
		// $cart.getTotal(true)
		// 	.then(function(total) {
		// 		console.log("getTotal", total);
		// 		$rootScope.total = total;
		// 	})

		if($rootScope.user){
			$scope.sendTo = {
				firstName: $rootScope.user.firstName,
				lastNamePrefix: $rootScope.user.lastNamePrefix,
				lastName: $rootScope.user.lastName,
				email: $rootScope.user.email,		
				shipToOtherAddress: false,					
			}
			
			if($rootScope.user.address.length > 0){
				$scope.sendTo.address = $rootScope.user.address[0].street;
				$scope.sendTo.houseNumber = $rootScope.user.address[0].houseNumber;
				$scope.sendTo.houseNumberSuffix = $rootScope.user.address[0].houseNumberSuffix;
				$scope.sendTo.zipcode = $rootScope.user.address[0].postalCode;
				$scope.sendTo.city = $rootScope.user.address[0].city;
				$scope.sendTo.country = $rootScope.user.address[0].country;
				
				if($rootScope.user.phone){
					$scope.sendTo.phone = '+' + $rootScope.user.phone.countryCode + $rootScope.user.phone.landLine;
				}

			}

			$scope.billTo = {
				email: $rootScope.user.email
			}
		}
		$scope.loading = false;
	}

	$scope.submitOrder = function(){
		console.log("submit", $scope.checkoutForm);
		$scope.submitted = true;
		$scope.alert = null;
		if($scope.checkoutForm.$invalid){
			$scope.alert = {
				msg: "Niet alle verplichte velden zijn ingevuld",
				type: 'danger'
			};
			return;
		}
		$scope.loading = true;
		$scope.alert = null;
		var amount = 0;
		var valid = false;

		$cart.getShops()
		
			.then(function(shops) {
				// var shopProductsInCart = shops[$scope.shop.nameSlug].items;
				// for(i = 0; i < shopProductsInCart.length; i++) {
				// 	if(shopProductsInCart.price == __SHOP.PRICE__) {
				// 		if(shopProductsInCart[i]. == __SHOP.__) {

				// 		}
				// 	}
				// }
			})
		
		$cart.getTotal($rootScope.cart, true)
			.then(function(total) {
				amount = total;
				var data = {
					billTo: $scope.sendTo.shipToOtherAddress ? $scope.billTo : $scope.sendTo,
					sendTo: $scope.sendTo,
					cart: $rootScope.cart,
					comment: $scope.comment || null,
					cartTotal: amount,
					guest: !$rootScope.user
				}

				$api.post("create-order", data)

					.then(function(response) {
						console.log("res", response);
						if(response.data.success && response.data.success == true){
							var paymentData = {
								order: {
									payId: response.data.order.payId,
									amount: amount,
									description: 'Uw bestelling bij PrismaNote',
									redirectUrl: response.data.order.redirectUrl + '/' + response.data.order.payId
								}
							};
							$api.post("create-payment", paymentData)
							.then(function(response){
								$window.location.href = response.data.payment.links.paymentUrl;
								
							})
							.catch(function(reason){
								$scope.alert = {
									msg: reason,
									type: 'alert'
								};
								console.log("Mollie fout!", reason);
							})
						}else{
							$scope.loading = false;
							console.log(response.data.message.code, response.data.message.msg);
							if(response.data.message.code == 401){
								
								$scope.alert = {
									msg: 	'<h5>Dit e-mailadres is al gekoppeld aan een account.</h5>' +
											'<p>Om gebruik te maken van dit e-mailadres moet u inloggen op het bijhorende account</p>' +
											'<a class="btn btn-lg btn-green" ng-controller="authController" ng-click="openLoginModal()"> Inloggen</a>',
									type: 'warning'
								};
							}else if(response.data.message.code == 409){
								$scope.alert = {
									msg: 	"<h5>Het account bij dit e-mailadres is nog niet geactiveerd</h5>" +
											"<p>Om gebruik te maken van dit e-mailadres moet u het bijhorende account activeren.</p>" +
											"<a class='btn btn-lg btn-green' ng-click=sendActivation('"+ $scope.sendTo.email +"')> Verstuur <br> activatieinstructies</a>",
									type: 'warning'
								};
							
							}else{
								$scope.alert = {
									msg: "<p>" + response.data.message + "</p>",
									type: 'danger'
								};
							}							
						}
					})
					
					.catch(function(reason) {
						$scope.alert = {
							msg: reason,
							type: 'danger'
						};
						$scope.loading = false;
						console.log(reason);
					});
			})

			.catch(function(reason) {
				$scope.alert = {
					msg: reason,
					type: 'alert'
				};
				$scope.loading = false;
				console.log(reason);
			});
	};

	$scope.checkPayment = function() {
		var payId = $stateParams.payId;
		$scope.loading = true;

		$api.get('check-payment/' + payId)
		.then(function(response) {
			$scope.loading = false;
			if(response.data.code == 400){
				$scope.paymentResult = "<i class='fa fa-exclamation fa-5x' aria-hidden='true'></i><br>" + response.data.msg;
			}else{
				$scope.paymentResult = "<i class='fa fa-check fa-5x' aria-hidden='true'></i><br>" + response.data.msg;
			}
		})
		.catch(function(reason){
			console.warn("error", reason);
		})		
	}

	$scope.openLoginModal = function(){
		var mode = 'login';
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/login-modal.html',
			controller: 'loginModalController',
			resolve: {
				mode: function() {
					return mode;
				}
			}
		});

		modalInstance.result.then(function(result){
			$scope.initShoppingCart();
		}, function(){
			
		})
	}

	$scope.sendActivation = function(email){
		$api.post('send-activation-link', {email:email})
		.then(function(response){
			$scope.alert = {
				msg: "<h5>Email verstuurd!</h5>" +
					"<p>We hebben een email verstuurd met instructies om uw account te activeren!</p>",
				type: 'success'
			};
		})
		.catch(function(reason){
			console.log("send activation mail failed", reason);
			$scope.alert = {
				msg: reason,
				type: 'danger'
			}
		})
	}
}]);