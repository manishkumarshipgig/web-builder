prismanoteApp.controller('brandsController', ['$scope', '$rootScope', '$location', '$api', function($scope,	$rootScope, $location, $api) {

	$rootScope.breadcrumb = ['Merken'];
	$scope.totalDisplayed = 20;
    $scope.brands =[];
    $scope.enabled =true;
	// $scope.isVerified = true ;
	// $api.get('brands', {'filter': {'isFeatured': true}, 'sort': {'name': 'asc'}, 'limit': 24})
	
	// 	.then(function(response) {
	// 		$scope.brands = response.data.brands;
	// 	})
		
	// 	.catch(function(reason) {
	// 		console.log(reason);
	// 	});

	$scope.getBrandList = function() {
		$api.get('brands',{'filter':{'isVerified':true}})
            .then(function(response) {
							console.log("response",response);
                // $scope.loading = false;
                $scope.brands = response.data.brands;
               
            })
            .catch(function(reason) {
                console.log(reason);
            });
		
}
    
    console.log("inside Brands length", $scope.brands.length);

    $scope.resetSelected = function() {
        $scope.currentSocialMedia = null;
        $scope.currentHome = null;
        $scope.currentSlide = null;
    }

    $scope.loadMore = function() {
        $scope.totalDisplayed += 20;
    }

    $scope.loadBrand = function() {
      
        $api.get('brands/' + $stateParams.nameSlug)
    
            .then(function(response) {
                
                $scope.brand = response.data.brand;
                //- set hidden search input filter to name of the brand
                $scope.search.brand.name = $scope.brand.name;
            })
            
            .catch(function(reason) {
                console.log(reason);
            });
    }

    $scope.updateBrand = function(redirect){
        console.log("[updateBrand controller]");
        console.log("$stateParams.nameSlug", $stateParams.nameSlug);
        console.log("$scope.brand", $scope.brand);
        $api.put('brands/' + $stateParams.nameSlug, {brand: $scope.brand})
        
            .then(function() {
                if(redirect){
                    $state.go('admin.users');
                }
            })
            
            .catch(function(reason) {
                console.log(reason);
            });
    }
    $scope.changeCallback = function() {
        console.log('This is the state of my model ' + $scope.enabled);
        if($scope.enabled){
        $api.get('brands',{'filter':{'isVerified':true}})
        
            .then(function(response) {
							console.log("response",response);
                // $scope.loading = false;
                $scope.brands = response.data.brands;
               
            })
            
            .catch(function(reason) {
                console.log(reason);
            });
        }
    
	else{
		console.log("brand.isVerified", $scope.enabled);
		$api.get('brands',{'filter':{'isVerified':false}})
        
            .then(function(response) {
			 console.log("response",response);
                // $scope.loading = false;
                $scope.brands = response.data.brands;
               
            })
            
            .catch(function(reason) {
                $scope.brands = '';
                alert(reason);
               
                console.log(reason);
            });
    }

      };

}]);