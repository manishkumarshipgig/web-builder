prismanoteApp.controller('brandUserController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', '$wholesaler',
	function($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt, $wholesaler) {

		$scope.loadUser = function() {
			$api.get('user/' + $stateParams.userId)
			
				.then(function(response) {
					$scope.user = response.data.user;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}
		$scope.updateLoading = false;
		$scope.afterClick = false;
		$scope.updateUser = function() {
			$scope.updateLoading = true;
			$scope.afterClick = true;
			$api.put('user/' + $stateParams.userId, {user: $scope.user})
			
				.then(function(response) {
					console.log('response',response);
					$scope.updateLoading = false;
					$scope.afterClick = false;
					$state.go('brand.home');
				})
				
				.catch(function(reason) {
					console.log('catch error',reason);
				});
		}


		$scope.openAddress = function(index){
			$scope.resetSelected();
			$scope.addNewAddress = false;
			$scope.currentAddress = $scope.user.address[index];
			$scope.currentAddress.index = index;
		}

		$scope.newAddress = function() {
			$scope.resetSelected();
			$scope.addNewAddress = true;
		}

		$scope.deleteAddress = function(index){
			prompt({
				title: 'Adres verwijderen?',
				message: 'Weet u zeker dat u dit adres wilt verwijderen?'
			}).then(function() {
				$scope.user.address.splice(index, 1);
				$scope.resetSelected();
			});
		}

		$scope.pushAddress = function() {
			$scope.user.address.push($scope.currentAddress);
			$scope.addNewAddress = false;
		}

		$scope.cancelNewAddress = function() {
			$scope.addNewAddress = false;
			$scope.resetSelected();
		}


		$scope.openPhone = function(index){
				$scope.resetSelected();
				$scope.currentPhone = $scope.user.phone[index];
				$scope.currentPhone.index = index;
		}

		$scope.deletePhone = function(index){
				prompt({
						title: 'Telefoonnummer verwijderen?',
						message: 'Weet u zeker dat u dit telefoonnummer wilt verwijderen?'
				}).then(function() {
						$scope.user.phone.splice(index, 1);
						$scope.resetSelected();
				});
		}

		$scope.newPhone = function(index){
				$scope.resetSelected();
				$scope.addNewPhone = true;
		}

		$scope.pushPhone = function() {
				$scope.user.phone.push($scope.currentPhone);
				$scope.addNewPhone = false;
		}

		$scope.cancelNewPhone = function() {
				$scope.addNewPhone = false;
				$scope.resetSelected();
		}

			$scope.resetSelected = function() {

			$scope.currentAddress = null;
			$scope.currentPhone = null;
		}

}])