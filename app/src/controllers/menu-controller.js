prismanoteApp.controller("menuController", ['$scope',
	function($scope) {
		$scope.toggleMenu = function() {
			$scope.showMenu = !$scope.showMenu;
			$scope.showCart = false;
		}
	}
]);