prismanoteApp.controller("loginModalController", ['$scope', '$rootScope', '$uibModalInstance', '$api', '$http', 'mode', 'tab', 'redirect', 'data', '$state',
	function($scope, $rootScope, $uibModalInstance, $api, $http, mode, tab, redirect, data, $state) {
		$scope.forgotmode = false;
		$scope.registermode = false;
		$scope.requestmode = true;
		$scope.alert = null;
		$scope.registerComplete = false; //alleen true als registreren gelukt is
		$scope.interest = {};
		$scope.verify = true;

		$scope.mode = mode;

		if($scope.mode){
			if($scope.mode == 'register'){
				$scope.registermode = true;
				$scope.requestmode = false;

				$scope.newUser = {
					email: data && data.username ? data.username: null
				}
				//auto verify accounts only possible when the modal is directly opened in register mode
				if(data && (typeof data.verify == 'undefined' || data.verify == true)){
					$scope.verify = true;	
				}else if (data && data.verify == false){
					$scope.verify = false;
					if(data.role){
						$scope.userRole = data.role
					}
				}
			}else if($scope.mode == 'forgot'){
				$scope.forgotmode = true;
			}else if($scope.mode == 'login'){
				$scope.forgotmode = false;
				$scope.registermode = false;
				$scope.requestmode = false;
				
				$scope.username = data && data.username ? data.username: null;
				
			}else if($scope.mode == 'request'){
				$scope.requestmode = true;
				$scope.registermode = true;
				$scope.forgotmode = false;
				$scope.today = new Date();
				$scope.interest.cs = {openend: false};
				if(tab){
					$scope.interest[tab] = true;
				}
			}
		}else{
			$scope.requestmode = false;
			$scope.registermode = false;
			$scope.forgotmode = false;
		}

		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		}

		function completeLogin(user, redirect){
			$rootScope.user = user;
	
			delete $scope.username;
			delete $scope.password;

			$scope.close();

			if(redirect){
				var role = user.role == 'wholesaler' ? 'brand' : user.role;
				$state.go(role + ".home");
			}
		}
		
		$scope.errExist = false;
		$scope.login = function() {
			$scope.alert = null;
			$scope.loading = true;

			$api.post('login', {username: $scope.username, password: $scope.password})

			.then(function(response) {
				$scope.loading = false;

				if(response.data.mustReset){
					$scope.alert = {
						type: 'info',
						msg: "Your must set new password before continuing"
					}
					$scope.setNewPassword = true;
					return;
				}else{
					completeLogin(response.data, redirect);
				}				
			})
			.catch(function(reason) {
				$scope.loading = false;
				console.error(reason);
				$scope.alert = {
					type: 'danger',
					msg: reason
				}
			})
		};


		$scope.setPassword = function(){
			$scope.loading = true;
			if($scope.newPassword1 != $scope.newPassword2){
				$scope.loading = false;
				return $scope.alert = {
					type: 'danger',
					msg: "The passwords doesn't match!"
				}
			}else{
				$api.post('set-new-password', {
					username: $scope.username,
					newPassword1: $scope.newPassword1,
					newPassword2: $scope.newPassword2
				})
				.then(function(response) {
					$scope.loading = false;
					if(response.data.success && response.data.user){
						completeLogin(response.data.user, redirect);
					}
					
				})
				.catch(function(reason) {
					$scope.loading = false;
					console.error(reason);
					$scope.alert = {
						type: 'danger',
						msg: reason
					}
				})	
			}
		}

		$scope.closeAlert = function() {
			$scope.alert = null;
		}

		$scope.forgot = function() {
			$scope.alert = null;
			$api.post('send-password-reset-link', {email: $scope.username})
			.then(function(response){
				$scope.alert = {
					type: 'success',
					msg: response.data.message
				}
			})
			.catch(function(reason){
				$scope.alert = {
					type: 'danger',
					msg: reason
				}
			})
		}

		$scope.close = function() {
			$uibModalInstance.close(mode);
		}

		$scope.register = function(isValid, request) {

			if (isValid) {
				$api.post('register', {
					user: $scope.newUser, 
					request: request, 
					extraData: request ? $scope.interest : null,
					verify: $scope.verify,
					userRole: $scope.userRole ? $scope.userRole : null
				})
				.then(function(response){
					$scope.alert = null;
					if(response.data.user){
						$rootScope.user = response.data.user;
						$scope.close();
					}

					$scope.registerComplete = true;

					
				})
				.catch(function(reason){
					console.error("error", reason);
					$scope.alert = {
						type: 'danger',
						msg: reason 
					};
				})
			}
		};

		$scope.logout = function() {
			$api.get('logout')
				.then(function() {
					$scope.alert = null;
					$rootScope.user = null;
					$scope.close();
					$state.go('layout.logout');
				})
				
				.catch(function(reason) {
					console.error(reason);
				});
		};
}]);