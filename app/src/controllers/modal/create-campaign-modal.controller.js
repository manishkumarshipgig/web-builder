prismanoteApp.controller("createCampaignModalController", ['$scope', '$uibModalInstance', '$api',
function($scope, $uibModalInstance, $api) {
    console.log("createCampaignModalController");

    $scope.campaign = {};

    $scope.closeModal = function(){
        $uibModalInstance.close($scope.campaign);
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

	var getItems = function(type){
		$api.get(type)
		
			.then(function(response) {
				$scope[type] = response.data[type];
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

	$scope.getBrands = function() {
		return getItems('brands');
	};

	$scope.getWholesalers = function() {
		return getItems('wholesalers');
	};

	var searchItems = function(str, type) {
		var matches = [];
	
		$scope[type].forEach(function(item) {
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || 
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

		return matches;
	};

	$scope.searchBrands = function(str) {
		return searchItems(str, 'brands');
	}

	$scope.searchWholesalers = function(str) {
		return searchItems(str, 'wholesalers');
	}

	$scope.brandSelected = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.campaign.brand = brand;
	};

	$scope.wholesalerSelected = function(selected){
		var wholesaler = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.campaign.wholesaler = wholesaler;
	};

	$scope.updateShop = function(){
		$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
	}

}]);