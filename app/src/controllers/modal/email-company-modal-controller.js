prismanoteApp.controller("emailCompanyController", ['$scope', '$rootScope', 'data', '$api', '$stateParams', '$uibModalInstance', '$state', 'prompt', 'Upload', '$retailer', '$uibModal',
    function ($scope, $rootScope, data, $api, $stateParams, $uibModalInstance, $state, prompt, Upload, $retailer, $uibModal) {
        $scope.wholesalersNames =[];
        $scope.wholesalersEmails=[];
        $scope.wholesalersNameSlug=[];
        $scope.newEmailTemplate = "<p><title>Notificatie</title></p><!-- Notification 6--><table class='full2' width= '100%' border= '0' cellpadding= '0' cellspacing= '0' bgcolor= '#303030' style= 'text-align: center;' > <tbody><tr><td id='not6' style='background-color: #333436;text-align: center;'><!-- Mobile Wrapper--><table class='mobile2' width='100%' border='0' cellpadding='0' cellspacing='0' style='text-align: center;'><tbody><tr><td width='100%' style='text-align: center;'><div class='sortable_inner ui-sortable'><!-- Space--><table class='full2' width='600' border='0' cellpadding='0' cellspacing='0' style='text-align: center;'><tbody><tr><td width='600' height='50'></td></tr></tbody></table><!-- End Space--><!-- Space--><table class='full2' width='600' border='0' cellpadding='0' cellspacing='0' style='text-align: center;'><tbody><tr><td width='600' height='50'></td></tr></tbody></table><!-- End Space--><!-- Start Top--><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#4edeb5' style='text-align: center;'><tbody><tr><td class='logo' width='600' style='text-align: center;'><!-- Header Text--><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' height='30'></td></tr><tr><td width='100%'><img src='http://rocketway.net/themebuilder/template/templates/notify/images/not6_icon75px.png' width='75' alt='' border='0'></td></tr><tr><td width='100%' height='30'></td></tr></tbody></table></td></tr></tbody></table></div><!-- Mobile Wrapper--><table class='full2' width='600' border='0' cellpadding='0' cellspacing='0' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;' bgcolor='#ffffff'><div class='sortable_inner ui-sortable'><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' height='30'></td></tr></tbody></table></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' style='text-align: left;font-size: 23px;color: #3f4345;'><!-- [if !mso] <!--><!-- <![endif]-->Hallo Jolmer van Ekeren,<!-- [if !mso] <!--><!-- <![endif]--></td></tr></tbody></table></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' height='30'></td></tr></tbody></table></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' style='text-align: left;font-size: 14px;color: #3f4345;'><!-- [if !mso] <!--><!-- <![endif]-->I'm a paragraph. Click here to add your own text and edit me. I�m a great place for you to tell a story and let your users know a little more about you.<br><br><span style='font-size: 14px;text-align: left;float: none;'><b style='font-size: 14px;text-align: left;'><!--EndFragment--><br><br></span><img src='https://s3-eu-west-1.amazonaws.com/prismanotetest/uploads/prismanote-vliegtuigje.png'><br><!-- [if !mso] <!--><!-- <![endif]--></td></tr></tbody></table></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' height='40'></td></tr></tbody></table></td></tr></tbody></table><!-- --------------- Button Center -----------------><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td><table border='0' cellpadding='0' cellspacing='0' style='text-align: left;'><tbody><tr><td height='45' bgcolor='#4edeb5' style='text-align: center;'><b><!-- [if !mso] <!--><!-- <![endif]--><a href='https://juwelierbos.nl' style='color: #ffffff;font-size: 15px;width: 100%;'>Bezoek onze website</a><!-- [if !mso] <!--><!-- <![endif]--></b></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><!-- --------------- End Button Center -----------------><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' height='35'></td></tr></tbody></table></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' style='text-align: left;font-size: 14px;color: #3f4345;'><!-- [if !mso] <!--><!-- <![endif]-->Kunnen wij u ergens anders mee helpen?Bel ons direct op : +31 345631776 en wij zorgen dat u tevreden bent!<br><br><b>Contact gegevens:</b><br>Juwelier Bos<br>Prins Bernhardweg 4 <br>9166SH Schiermonnikoog<br>niek@excellent-electronics.nl<!-- [if !mso] <!--><!-- <![endif]--></td></tr></tbody></table></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='text-align: center;'><tbody><tr><td width='600' style='text-align: center;'><table class='fullCenter2' width='540' border='0' cellpadding='0' cellspacing='0' style='text-align: center;text-align: center;'><tbody><tr><td width='100%' height='50'></td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table><table class='full2' width='600' border='0' cellpadding='0' cellspacing='0' style='text-align: center;'><tbody><tr><td width='600' height='30'></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' style='text-align: center;'><tbody><tr><td width='600' style='text-align: left;font-size: 13px;color: #ffffff;'><i><!-- [if !mso] <!--><!-- <![endif]-->Copyright Juwelier Bos<!-- <![endif]--></i></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' style='text-align: center;'><tbody><tr><td width='600' height='30'></td></tr></tbody></table><table class='mobile2' width='600' border='0' cellpadding='0' cellspacing='0' style='text-align: center;'><tbody><tr><td width='600' height='29'></td></tr><tr><td width='600' height='1' style='font-size: 1px;'>&nbsp;</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><!-- End Notification 6-->";

        $scope.data = data;
        $scope.loading = false;
        $scope.direct = false;
        $scope.company = {};

        $scope.date = new Date();
        //$scope.price = data.price;



        $scope.repairStatus="To RepairMan";


     

        // if(data && data.body){
        //     $scope.body = data.body;
        // }else{
        //     console.log("DTA", $scope.data);
        //     $scope.loading = true;
        //     $api.get('preview-email', $scope.data)
        //     .then(function(response) {
        //         $scope.loading = false;
        //         $scope.body = response.data.html;
        //     })
        //     .catch(function(reason) {
        //         $scope.loading = false;
        //         console.log(reason);
        //     })

        // }

        // $api.get('crmusers', { filter: { 'email': $scope.data.email }, 'sort': { '_id': 'desc' } })

        //     .then(function (response) {
        //         $scope.emailTemplate = response.data.crmUsers[0].plannedEmail[$scope.data.index].emailTemplate;
                
        //         if ($scope.emailTemplate == undefined  || $scope.emailTemplate == '') {
        //             $scope.emailTemplate = $scope.body;
        //         }
        //         $scope.loading = false;
        //     })

        //     .catch(function (reason) {
        //         console.log(reason);
        //     });

       

        // $scope.cancel = function() {
        //     $uibModalInstance.dismiss('cancel');
        // }

        // $scope.update = function () {
        //     //alert('update call:' + $scope.data.index);

        //     $api.post('emailtemplateupdate', { emailIndex: $scope.data.index, userId: $scope.data.userId, template: $scope.emailTemplate })

        //     .then(function (response) {
        //         $uibModalInstance.dismiss('cancel');
        //         $scope.loading = false;
        //     })

        //     .catch(function (reason) {
        //         $scope.loading = false;
        //         console.log(reason);
        //     });
        // }

        // $scope.create = function () {
        //     $api.post('add-new-planned-email', { 
        //         date: $scope.direct ? null : $scope.date, 
        //         subject: $scope.subject, 
        //         userId: $scope.data.userId, 
        //         template: $scope.newEmailTemplate,
        //         shopId: $scope.currentShop._id
        //     })

        //     .then(function (response) {
        //         $uibModalInstance.dismiss('cancel');

        //         $scope.loading = false;
        //     })

        //     .catch(function (reason) {
        //         $scope.loading = false;
        //         console.log(reason);
        //     });
        // }

        // $scope.sendRepairEmailWithPic = function () {
        //     //alert($scope.data.userId);
        //     $scope.upload = true;
          
        //     Upload.upload({
        //         url: 'api/user/loyalty-photo-upload',
        //         data: {                
        //             crmUserId: $scope.data.userId,
        //             file: $scope.customer.photo, //file input field
        //             filename: $scope.customer.photo,
        //             friendlyFileName: true
        //         }
        //     })
        //     .then(function (res) {
        //         // Api call
        //         $api.post('repairready-withpic-email', { userId: $scope.data.userId, fileName: res.data.file, price: $scope.price })
        //         .then(function (response) {
        //             console.log(response);
        //                 $uibModalInstance.dismiss('cancel');
        //         })

        //         .catch(function (reason) {
        //             console.log(reason);
        //         });

        //         //wanneer uploaden gelukt
        //         $scope.customer = null;
        //         $scope.alert = {
        //             type: "success",
        //             msg: "Uw gegevens zijn verzonden!"
        //         }
        //     }, function (res) {
        //         //wanneer uploaden mislukt
        //         console.log("Error: ", res.status);
        //         $scope.alert = {
        //             type: "danger",
        //             msg: "upload error: " + res.status
        //         }
        //     }, function (evt) {
        //         //tijdens upload
        //         var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        //         $scope.uploadProgress = progressPercentage;
        //         console.log("Progress: " + progressPercentage + '%' + evt.config.data);
        //     });
        //     $scope.upload = false;
        // }

        $scope.sendRepairEmailCompany = function (fileName) {
            // Api call
            console.log("data", $scope.data);
            // console.log(" $scope.company.email.title", $scope.company.email.title);
            var emailCmpny=$scope.company.email.title.split(" ");
            var emailnameSlug=emailCmpny[2];

            console.log("$scope.company.email.title",emailCmpny[0]);
             $api.post('repairready-email-company', { userId: $scope.data.userId,repairStatus: $scope.repairStatus,email: emailCmpny[0], fileName: $scope.data.photoname, price: $scope.price })
            .then(function (response) {
                console.log(response);
                $uibModalInstance.dismiss('cancel');
            })

            .catch(function (reason) {
                $scope.alert = {
                    type: "danger",
                    msg: "upload error: " + reason.status
                }
                console.log(reason);
            });
		}
		
		$scope.searchCompanyEmail = function () {
            $scope.wholesalershh = null;
            $scope.wholesalersList=[];
            $api.get('wholesalers')
        
            .then(function(response) {
                $scope.loading = false;
                $scope.wholesalershh = response.data.wholesalers;
                console.log($scope.wholesalershh);
                for(var i=0;i< $scope.wholesalershh.length;i++){
                    $scope.wholesalersNames.push($scope.wholesalershh[i].name);
                    $scope.wholesalersEmails.push($scope.wholesalershh[i].email);
                    $scope.wholesalersNameSlug.push($scope.wholesalershh[i].nameSlug);
                 }
                 $scope.wholesalershh = null;
                 console.log(" $scope.wholesalersNames",$scope.wholesalersNames);
                 console.log(" $scope.wholesalersEmails",$scope.wholesalersEmails);
               
                 for(var j=0;j<$scope.wholesalersNames.length;j++){
                     $scope.getEachwholesalerDetails={};
                     $scope.getEachwholesalerDetails.name= $scope.wholesalersNames[j];
                     $scope.getEachwholesalerDetails.email=$scope.wholesalersEmails[j];
                     $scope.getEachwholesalerDetails.nameSlug=$scope.wholesalersNameSlug[j];
                     
                     $scope.wholesalersList.push( $scope.getEachwholesalerDetails);
                     // console.log("$scope.sdd",$scope.sdd);
                     
                    
                  }
                
                  console.log("$scope.wholesalersList",$scope.wholesalersList);
            })
            
            .catch(function(reason) {
                console.log(reason);
            });
            
        }

        $scope.searchCompanyEmail();

        $scope.openWholesalerDetails = function (data) {
            console.log("data.....",data);
            var modalInstance = $uibModal.open({
                templateUrl: '../views/modal/wholesaler-details-modal.html',
                controller: 'wholesalerDetailsController',
                size: 'lg',
                resolve: {
                    data: function(){
                        return data;
                    }
                }
            });
    
            modalInstance.result.then(function(){
    
            }, function(){
                //dismissed
            })
        }
            
            
        
        $scope.getCompanyEmail = function (email) {
            console.log("email...."+email);
            $scope.company.email = email;
            $scope.wholesalershh = null;
            
        }
        
    $scope.viewDetails = function () {
     console.log("Inside view details");
     var emailCmpny=$scope.company.email.title.split(" ");
     console.log("emailCmpny",emailCmpny);
     var emailnameSlug=emailCmpny[2];
     console.log("emailnameSlug",emailnameSlug);
     $scope.openWholesalerDetails(emailnameSlug);

    }

	}
]);