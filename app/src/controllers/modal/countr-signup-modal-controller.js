prismanoteApp.controller("countrSignupController", ['$scope', '$rootScope', '$uibModalInstance', '$api', '$http', '$state', '$retailer', 'prompt', '$countr',
	function($scope, $rootScope, $uibModalInstance, $api, $http, $state, $retailer, prompt, $countr) {

    $scope.countr = {};

    $scope.createCountrObject = function(){
        var shop = $rootScope.currentShop;

        var phoneNumber = function(type){
            var countryCode = shop.phone.countryCode ? shop.phone.countryCode : null;

            if(shop.phone[type]){
                return countryCode + shop.phone[type];
            }else{
                return null;
            }
        }

        var contactDetails = {
            address1: shop.address.street,
            number: shop.address.houseNumber,
            number_ext: shop.address.houseNumberSuffix,
            city: shop.address.city,
            country: shop.address.country,
            zip: shop.address.postalCode,
            first_name: shop.address.attn.firstName,
            last_name: shop.address.attn.lastNamePrefix,
            middle_name: shop.address.attn.lastName,
            phone: phoneNumber('landLine'),
            mobile: phoneNumber('mobilePhone'),
            email: shop.email
        }

        var billingFields = {
            vat: shop.vatNumber ? shop.vatNumber : null,
            registration: shop.pvc ? shop.pvc : null,
            iban: shop.bankAccountNumber ? shop.bankAccountNumber : null,
            organization: shop.accountHolder ? shop.accountHolder : null,
        }

        var billingDetails = angular.extend(angular.copy(contactDetails), billingFields);

        $scope.countr = {
            email: shop.email,
            username: shop.email,
            signup_source: 'reseller',
            contact: contactDetails,
            billing_info: billingDetails
        }
    }

    $scope.step1 = function(){
        $scope.showSteps = true;
    }

    $scope.step2 = function(){
        $scope.connected = false;
        $scope.countrMode = null;
        $scope.addTax = false;

        $scope.activeTab = 1;
        $scope.createCountrObject();
        $scope.alert = null;
    }

    $scope.step3 = function(){
        $scope.alert = null;
        $scope.loading = false;
        $scope.activeTab = 2;
    }

    $scope.login = function(valid){
        $scope.alert = null;
        if(!valid){
            $scope.alert = {
                type: 'warning',
                msg: "Niet alle verplicht velden zijn ingevuld"
            };
            return;
        }

        $scope.loading = true;
        $countr.connect($scope.countr.email, $scope.countr.password)
        	.then(function(response) {
                $scope.loading = false;
                
                $scope.getCountrShops();
                $scope.getTaxes();
                $scope.connected = true;
            })
            .catch(function(reason) {
                console.error(reason);
                $scope.loading = false;

                $scope.alert = {
                    type: 'danger',
                    msg: reason
                }
            })
    }

    $scope.getCountrShops = function(){
        $countr.getShops()
        .then(function(response) {
            $scope.countrShops = response;
        })
        .catch(function(reason) {
            console.error(reason);
            $scope.alert = {
                type: 'danger',
                msg: reason
            }
        })
    }

    $scope.setCountrId = function(){
        $retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
    }

    $scope.getTaxes = function(){
        $api.get('countr/taxes', {shopId: $rootScope.currentShop._id})
        .then(function(response) {
            $scope.taxes = response.data;

            if((typeof $rootScope.currentShop.countr.taxId == "undefined" || $rootScope.currentShop.countr.taxId == "") && $scope.taxes.length > 0){
                $rootScope.currentShop.countr.taxId = $scope.taxes[0]._id;
                $retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
            }
        })
        .catch(function(reason) {
            console.error(reason);
        })
        
    }

    $scope.deleteTax = function(id, index){
        prompt({
            title: 'Belastingtarief verwijderen?',
            message: 'Weet u zeker dat u dit belastingtarief wilt verwijderen?'
        }).then(function() {
            $countr.deleteTax(id)
            .then(function(response) {
                $scope.taxes.splice(index, 1);

                if(id == $rootScope.currentShop.countr.taxId && $scope.taxes.length > 0){
                    //Set new default tax rate to the first
                    $rootScope.currentShop.countr.taxId = $scope.taxes[0]._id;
                    $retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
                }

            })
            .catch(function(reason) {
                console.error(reason);
                $scope.countrAlert = {
                    type: 'danger',
                    msg: reason
                }
            })
        });
    }

    $scope.defaultTax = function(taxId){
        $countr.setDefaultTax(taxId);
    }

    $scope.addNewTax = function(){
        $countr.newTax($scope.newTax.name, $scope.newTax.rate, $scope.newTax.default)
        .then(function(response) {
            $scope.taxes.push({
                name: $scope.newTax.name,
                rate: $scope.newTax.rate,
                _id: response.tax._id
            })

            $scope.addTax = !$scope.addTax;
            $scope.newTax = {};
            
        })
        .catch(function(reason) {
            console.error(reason);
            $scope.countrAlert = {
                type: 'danger',
                msg: reason
            }
        })
    }

    var oldMode;
    $scope.changeMode = function(mode){
        $scope.modeAlert = null;
        $scope.alert = null;

        if(!$rootScope.currentShop.address.attn || !$rootScope.currentShop.address.attn.firstName || !$rootScope.currentShop.address.attn.lastName){

            $scope.modeAlert = {
                type: 'danger',
                msg: 'Controleer de waarden bij "post ter attentie van" bij uw winkel instellingen.'
            }
            return;
        }

        if(oldMode != mode){
            $scope.createCountrObject();
        }
        if(mode == 'login'){
            $scope.countrMode = 'login';
        }else if(mode == 'register'){
            $scope.countrMode = 'register';
            
        }
        oldMode = mode;
        
    }

    $scope.signUp = function(valid){
        console.log("signup", valid);
        $scope.alert = null;
        if(!valid){
            $scope.alert = {
                type: 'warning',
                msg: "Niet alle verplicht velden zijn ingevuld"
            }
            return;
        }
        
        $scope.countr.username = $scope.countr.email;

        $api.post('countr/signup', {
            countr: $scope.countr,
            shopId: $rootScope.currentShop._id,
            language: $rootScope.language
        })
        .then(function(response) {
            $scope.activeTab = 2;
            $scope.alert = {
                type: 'success',
                msg: response.data.message
            }

        })
        .catch(function(reason) {
            $scope.alert = {
                type: 'danger',
                msg: reason
            }
            console.error(reason);
        })
    }
    

    $scope.getBrands = function() {
        $api.get('brands')
        
        .then(function(response) {
            $scope.brands = response.data.brands;
        })
        
        .catch(function(reason) {
            console.error(reason);
        });
    };

    $scope.searchBrands = function(str) {
        var matches = [];
        $scope.brands.forEach(function(brand){
            if((brand.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
            (brand.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
                matches.push(brand);
            }
        })
        return matches;
    };

    var loadProductsFromBrand = function(brand, callback) {
        $api.get('products', {'aggregate': {'brand.nameSlug': brand.nameSlug}})
        
            .then(function(response) {
                return callback(null,response.data);
            })
            
            .catch(function(reason) {
                console.error(reason);
                return callback(reason, null);
            });
    };

    $scope.brandSelected = function(selected){
        var brand = {
            _id: selected.originalObject._id,
            name: selected.title,
            nameSlug: selected.originalObject.nameSlug,
            description: selected.originalObject.description,
            images: selected.originalObject.images,
            restricted: selected.originalObject.restricted
        }

        $rootScope.currentShop.brands.push(brand);
        $retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);

        if(($rootScope.currentShop.isPremium || (!$rootScope.currentShop.isPremium && !brand.restricted)) && $rootScope.currentShop.webshopActive) {
            
            loadProductsFromBrand(brand, function(err, result) {
                if(err) console.error("Error: ", err); return 
                $scope.progress = true;
                $scope.productCount = result.products.length;
                $scope.count = 0;
                for(var i = 0; i < $scope.productCount; i++) {
                    $scope.count += 1;
                    var product = {
                        _id: result.products[i]._id,
                        stock: 0,
                        price: result.products[i].suggestedRetailPrice,
                        dropshippingPrice: result.products[i].suggestedRetailPrice,
                        discount: 0,
                        dateAdded: new Date(),
                        show: true,
                        isBestseller: false,
                        isFeatured: false
                    };
                    $rootScope.currentShop.products.push(product);
                    
                    if($scope.count == result.products.length) {
                        $scope.progress = false;
                        $retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
                    }
                }
            });
        }
        getSocialPortal(function(err, portal){
            if(err){
                console.err("err", err);
                return;
            }
            var index = _.findIndex($scope.user.socialPortal.brands, {'_id': brand._id});
            if(index <0){
                prompt({
                    title: 'Merk ook voor promoties gebruiken?',
                    message: 'Wilt u ook promoties van dit merk zien en gebruiken?'
                }).then(function() {		
                    var portalBrand = {
                        _id: brand._id,
                        name: brand.name
                    }
                    $scope.user.socialPortal.brands.push(portalBrand);
                    $scope.saveSocialPortal();
    
                })
            }
        })			
    };

    function getSocialPortal(callback){
        $api.get('user-social-portal', {userId: $rootScope.user._id})

        .then(function(response){
            $scope.user.socialPortal = response.data.result;
            return callback(null, response.data.result)
        })
        .catch(function(reason){
            return callback(reason)
        })
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }





}]);