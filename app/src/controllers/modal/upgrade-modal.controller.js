prismanoteApp.controller("upgradeModalController", ['$scope', '$uibModalInstance', 'upgrade', 'shop', '$api',
function($scope, $uibModalInstance, upgrade, shop, $api) {

	console.log("MODULES", shop.modules);

	$scope.upgrade = upgrade;

	$scope.options = [];
	
	$scope.getOptions = function(){
		if(!$scope.upgrade.choosen || typeof $scope.upgrade.choosen == 'undefined' || Object.keys($scope.upgrade.choosen).length < 1) return;

		for(var key in $scope.upgrade.choosen){
			if(!$scope.upgrade.choosen.hasOwnProperty(key)) continue;

			var code = $scope.upgrade.choosen[key].code;
			$scope.upgrade.options.forEach(function(option){
				option.options.forEach(function(opt){
					if(opt.code == code){
						$scope.options.push({
							price: opt.price,
							option: opt.name,
							name: option.name,
							code: opt.code
						})
					}
				})
			})
		}
	}

	$scope.startUpgrade = function() {
		$uibModalInstance.close($scope.upgrade);
	}

	$scope.cancel = function() {
		$uibModalInstance.dismiss();
	}
	
}]);