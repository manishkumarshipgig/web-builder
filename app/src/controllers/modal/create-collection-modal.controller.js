prismanoteApp.controller("createCollectionModalController", ['$scope', '$uibModalInstance', '$api', 'collection',
function($scope, $uibModalInstance, $api, collection) {

	if(collection){
		$scope.collection = collection;
		$scope.editMode = true;
	}else{
    	$scope.collection = {};
	}

    $scope.closeModal = function(){
        $uibModalInstance.close($scope.collection);
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
	}
	
	$scope.deleteCollection = function(){
		$scope.collection.delete = true;
		$uibModalInstance.close($scope.collection);
	}

	$scope.removeCategory = function(index){
		$scope.collection.categories.splice(index, 1);
	}

	var getItems = function(type){
		$api.get(type)
		
			.then(function(response) {
				$scope[type] = response.data[type];
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

	$scope.getBrands = function() {
		return getItems('brands');
	};

	$scope.getWholesalers = function() {
		return getItems('wholesalers');
	};

	var searchItems = function(str, type) {
		var matches = [];
	
		$scope[type].forEach(function(item) {
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || 
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

		return matches;
	};

	$scope.searchBrands = function(str) {
		return searchItems(str, 'brands');
	}

	$scope.searchWholesalers = function(str) {
		return searchItems(str, 'wholesalers');
	}

	$scope.brandSelected = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.collection.brand = brand;
	};

	$scope.wholesalerSelected = function(selected){
		var wholesaler = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.collection.wholesaler = wholesaler;
	};

	$scope.updateShop = function(){
		$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
	}

	$scope.addToCollectionCategories = function(category){
		if(!$scope.collection.categories){
			$scope.collection.categories = [];
		}
		$scope.collection.categories.push(category)
	}

}]);