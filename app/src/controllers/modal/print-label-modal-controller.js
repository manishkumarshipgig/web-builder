prismanoteApp.controller("printLabelModalController", ['$scope', '$uibModalInstance', '$state', 'product', '$api', '$stateParams', 'FileSaver', '$http',
function($scope, $uibModalInstance, $state, product, $api, $stateParams, FileSaver, $http) {
    $scope.product = product;

    $scope.label = {
        quantity: 1
    };

    $scope.changeLabelType = function(){
        if($scope.label.type == 'etiket1'){
            $scope.label.line1 = $scope.product.nl.name;
            $scope.label.price = '€' + Math.round($scope.product.sellingPrice * 100) /100;
            $scope.label.line2 = $scope.product.variants[0].productNumber;
            $scope.label.line3 = $scope.product.brand.name;
            //line 4 t/m 8
        }
        if($scope.label.type == 'etiket2' ){
            $scope.label.line1 = $scope.product.nl.name;
            $scope.label.barcode = $scope.product.variants[0].ean;
            $scope.label.line3 = $scope.product.brand.name;
        }
        if($scope.label.type == 'etiket3'){
            $scope.label.line1 = $scope.product.variants[0].productNumber;
            $scope.label.barcode = $scope.product.variants[0].ean;
        } 
    }

    $scope.print = function(){

        $http({
            method: 'POST',
            url: '/api/download-label',
            data: {
                label: $scope.label,
            },
            responseType : 'arraybuffer'
        })
        .then(function(response){
            var file = new Blob([response.data], {type: 'application/zip'});
            FileSaver.saveAs(file, "etiket.pme");
            $scope.close();
        }, function(response){
            console.log("ERROR", response);
        })
    }



    if($scope.product.stock > 0) {
        $scope.inStock = true;
    } else {
        $scope.inStock = false;
    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.close = function() {
        $uibModalInstance.close();
    };

}]);
