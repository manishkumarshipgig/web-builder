prismanoteApp.controller("retailerProductModalController", ['$scope', '$uibModalInstance', '$state', 'product', '$api', '$stateParams', '$uibModal', '$rootScope',
	function($scope, $uibModalInstance, $state, product, $api, $stateParams, $uibModal, $rootScope) {
		$scope.product = product;
		$scope.productUpdateLoading = false;
		$scope.productUpdateError = false;
		$scope.getTheCollectionName = function(collection){
			// console.log("Getting Collection Name = ",collection);
			var collectionName = "";

			if(collection.hasOwnProperty($scope.language))
				collectionName = collection[$scope.language].name;
			else if(collection.hasOwnProperty("en.name"))
				collectionName = collection.en.name;
			else if(collection.hasOwnProperty("name") && collection.name != "")
				collectionName = collection.name;
			else
				collectionName = "NO NAME";


			return collectionName;
		}


		console.log("PRODUCT STOCK TEST = ",$scope.product.stock);
		if($scope.product.stock >= 0) {
			$scope.inStock = true;
		} else {
			$scope.inStock = false;
		}
		$scope.collection = {
			brand : {}
		}
		function activate(){
			// Getting All Brands
			$api.get('brands').then(function(success){
				$scope.brands = success.data.brands;
				console.log("$scope.brands = ",$scope.brands);
			},function(err){
				console.log(err);
			})
		}
		activate();
		$scope.countrModule = $rootScope.checkModule('countr');
		$scope.deleteProductFromShop = function(product){
			console.log("Product to be deleted from shop ",product);
			$api.post('shops/delete-product-from-shop/',{
				shop : $rootScope.currentShop._id,
				product: product._id
			}).then(function(success){
				$scope.cancel();
				alert("Product Deleted Successfully");
			},function(err){
				alert("There seems to be some problem. Please try again later.")
			})
		}
		// this function is needed because of the blocking of show button to prevent the user switching the show button on while the product has not completed filter info
		$api.get('get-specific-product-from-shop', {productId: product._id, shopId: $rootScope.currentShop._id})
		.then(function(response) {
			$scope.product = response.data;
			// $scope.productGeneral = response.data.product;
			// if(!$scope.product.images){
			// 	$scope.product.images = $scope.productGeneral.images;
			// }
			// if($scope.product[$rootScope.language] == undefined || !$scope.product[$rootScope.language].name){
			// 	$scope.product[$rootScope.language] = {
			// 		name: $scope.productGeneral[$rootScope.language].name
			// 	}
			// }
			// if(!$scope.product.variants || $scope.products.variants.length < 0){
			// 	$scope.product.variants = $scope.productGeneral.variants;
			// }

			// if(!$scope.product.en || !$scope.product.en.nameSlug){
			// 	$scope.product.en = {
			// 		nameSlug: $scope.productGeneral.en.nameSlug
			// 	}
			// }
		})


		productPriceStuff();

		$scope.$watch('product.sellingPrice',function(){
			productPriceStuff();
		})
		function productPriceStuff(){
			console.log("Altering with product price stuff");
			console.log("Product price = ",$scope.price);
			if($scope.product.price != null && $scope.product.price > 0) {
				$scope.product.sellingPrice = Math.round(($scope.product.price * 1.21) * 100) /100;
			} 
			else {
				// if the product price is 0, and the suggested price has a value, this will automatically populate the product.price field on the view.
				// In case you want this to not to happen, a condition for $scope.product.price shall be added before the if condition.
				if($scope.product.suggestedRetailPrice != null && $scope.product.suggestedRetailPrice > 0) {
					$scope.product.sellingPrice = Math.round($scope.product.suggestedRetailPrice * 100) / 100;
					$scope.product.price = Math.round(($scope.product.sellingPrice / 1.21) * 100) / 100;
				} else {
					$scope.product.sellingPrice = 0;
					$scope.product.price = 0;
				}
			}

			if($scope.product.discount != null) {
				if($scope.product.discount > 0) {
					$scope.onSale = true;
				} else {
					$scope.onSale = false;
				}
			} else {
				$scope.product.discount = 0;
			}

			$scope.inWebshop = false;

			$scope.discount = $scope.product.discount;

		}




		$scope.setDiscount = function() {
			if($scope.usePercentage) {
				$scope.product.discount = ($scope.discount / 100) * $scope.product.sellingPrice;
			} else {
				$scope.product.discount = $scope.discount;
			}
		}

		$scope.cancel = function() {
			// $uibModalInstance.dismiss('cancel');
			$scope.addNewCollectionForm = false;
		}

		$scope.close = function() {
			if(!$scope.inStock) {
				$scope.product.stock = 0;
			}
			if(!$scope.onSale) {
				$scope.product.discount = 0;
			}
			$uibModalInstance.close($scope.product);
		}

		$scope.saveProduct = function() {
			console.log("SAVING PRODUCT = ",$scope.product);

			delete $scope.product.sellingPrice;
			$scope.productUpdateLoading = true;

			$api.post("shop/save-shop-product", {
				shop : $rootScope.currentShop._id,
				product: $scope.product
			})
			.then(function(res){
				if($rootScope.currentShop.countr && $rootScope.currentShop.countr.username && $rootScope.currentShop.countr.accessToken){
					$api.put('countr/products', {
						shopId: $rootScope.currentShop._id,
						product: $scope.product,
						language: $rootScope.language
					}).then(function(response) {
						$scope.productUpdateLoading = false;
						$scope.close();
					})
					.catch(function(reason) {
						console.log(reason);
					})
				}else{
					$scope.productUpdateLoading = false;
					$scope.close();
				}
			},function(err){
				console.error(err);
				$scope.productUpdateError = true;
				$scope.productUpdateLoading = false;

			})
			
		}

		$scope.openPrintLabelModal = function() {
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/retailer-print-label-modal.html',
				controller: 'printLabelModalController',
				resolve: {
					product: function(){
						return product;
					}
				}
			});

			modalInstance.result.then(function(result){
				if(result){
					//ready
				}
			}, function(){
				//dismissed
			})

		}

		var getItems = function (type) {
			$api.get(type)

			.then(function (response) {
				$scope[type] = response.data[type];
			})

			.catch(function (reason) {
				console.log(reason);
			});
		};

		$scope.getCollections = function() {
			return getItems('collections');
		};

		var searchItems = function(str, type) {
			var matches = [];
			console.log("Test ",$scope[type]);

			var addNewCollectionItem = {
				en: {
					name: " ➕ Add New Collection"
				},
				nl: {
					name: " ➕ Voeg nieuwe collectie toe"
				},
				es: {
					name: " ➕ Añadir colección"
				},
				fr: {
					name: " ➕ Ajouter une collection"
				},
				de: {
					name: " ➕ Kollektion hinzufügen"
				}
			};
			
			matches.push(addNewCollectionItem)



			$scope[type].forEach(function(item) {
				console.log("Item Language ",item[$rootScope.language])
				if(type == "brand"){
					console.log("Search For BRAND");
					console.log("$scope.brands",$scope.brands)
					if(
						(item != undefined)
						&& (
							(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
							||
							(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
							)
						) {
						matches.push(item);
				}
			}
			else{
				if(
					(item[$rootScope.language] != undefined && item[$rootScope.language].name != undefined)
					&& (
						(item[$rootScope.language].name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
						||
						(item[$rootScope.language].nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
						)
					) {
					matches.push(item);
			}
		}

	});


			return matches;
		};

		$scope.searchCollections = function(str) {
			if(str != "") $scope.searchStr = str;
			return searchItems(str, 'collections');
		}

		$scope.collectionSelected = function(selected){
			$scope.addNewCollectionForm = false;
			if(selected.title === " ➕ Add New Collection" || selected.title === " ➕ Kollektion hinzufügen" || selected.title === " ➕ Voeg nieuwe collectie toe" || selected.title === " ➕ Añadir colección" || selected.title === " ➕ Ajouter une collection"){
				console.log("Selected", selected);

				$scope.addNewCollectionForm = true;
				$("#collection-"+$rootScope.language+"-name").val($scope.searchStr);
			}
			else{
				var collection = selected.originalObject;
				if(!$scope.product.collections){
					$scope.product.collections = [];
				}
				$scope.product.collections.push(collection);
				$api.put('products/'+$scope.product[$rootScope.language].nameSlug,$scope.product)
				.then(function(success){
					console.log("Product has been updated with new collection.")
				},function(err){
					console.error(err);
				})
				console.log("product", $scope.product);
			}
		};

		$scope.stockStatusChange = function(inStock){
			console.log("InStock = ",inStock);
			if (inStock == false) {
				$scope.product.stock = 0;
			}
			// if(!$scope.product.stock){
			// 	$scope.product.stock = 1;

			// }
		}
		$scope.deleteCollection = function(index) {
			$scope.product.collections.splice(index, 1);
			$api.put('products/'+$scope.product[$rootScope.language].nameSlug,$scope.product)
			.then(function(success){
				console.log("Product has been updated with new collection.")

			},function(err){
				console.error(err);
			})

		};

		$scope.getCountrProduct = function(){
			$scope.counter = false;
			if($rootScope.checkModule('countr') && $scope.product.countrId){
				$api.get('countr/products', {
					shopId: $rootScope.currentShop._id,
					productId: $scope.product.countrId
				})
				.then(function(response) {
					$scope.product.countr = response.data.product;
					$scope.counter = true;
				})
				.catch(function(reason) {
					console.log(reason);
				})
			}
		}









		// Collection Functions

		$scope.closeModal = function(){
			$uibModalInstance.close($scope.collection);
		}

		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		}

		// $scope.deleteCollection = function(){
		// 	$scope.collection.delete = true;
		// 	$uibModalInstance.close($scope.collection);
		// }

		$scope.removeCategory = function(index){
			$scope.collection.categories.splice(index, 1);
		}

		var getItems2 = function(type){
			$api.get(type)
			.then(function(response) {
				$scope[type] = response.data[type];
			})
			.catch(function(reason) {
				console.log(reason);
			});
		};

		$scope.getBrands = function() {
			console.log("Getting Brands from getItems2()");
			var x = getItems2('brands');
			return x;
		};

		$scope.getWholesalers = function() {
			return getItems2('wholesalers');
		};

		var searchItemsBrands = function(str, type) {
			var matches = [];

			$scope[type].forEach(function(item) {
				if(
					(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
					(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

			return matches;
		};

		$scope.searchBrands = function(str) {
			return searchItemsBrands(str, 'brands');
		}

		$scope.searchWholesalers = function(str) {
			return searchItems(str, 'wholesalers');
		}

		$scope.brandSelected = function(selected){
			var brand = {
				_id: selected.originalObject._id,
				name: selected.title,
				nameSlug: selected.originalObject.nameSlug,
				description: selected.originalObject.description,
				images: selected.originalObject.images
			}

			$scope.collection.brand = brand;
		};

		$scope.wholesalerSelected = function(selected){
			var wholesaler = {
				_id: selected.originalObject._id,
				name: selected.title,
				nameSlug: selected.originalObject.nameSlug,
				description: selected.originalObject.description,
				images: selected.originalObject.images
			}

			$scope.collection.wholesaler = wholesaler;
		};

		$scope.updateShop = function(){
			$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
		}

		$scope.addToCollectionCategories = function(category){
			if(!$scope.collection.categories){
				$scope.collection.categories = [];
			}
			$scope.collection.categories.push(category)
		}

		$scope.addCollection = function(collection){
			console.log("Adding Collection : ",collection);
			$api.post('collections',collection).then(function(success){
				console.log("Collection added successtully ",success.data);
				$scope.collections.push(success.data.collection)
				var collToBeSelected = {
					originalObject: success.data.collection,
					title: success.data.collection[$rootScope.language].name
				};
				$scope.collectionSelected(collToBeSelected);

			},function(err){
				console.log("Error adding collection : ",err);
			})
		}

	}]);
