prismanoteApp.controller('tableOptionsController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload', '$retailer',
'data',
    function ($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt, Upload, $retailer,data) {

        $scope.nameSlug = $rootScope.getShopType() == 'proshop' ? localStorage.getItem('nameSlug') : $stateParams.nameSlug;
      
        $scope.selectedOptions=$rootScope.user.tableOptions;
        $scope.showName =true;
        $scope.showPhoneNumber =true;
        $scope.showProductStatus =true;
        $scope.showProductKind =true;
        $scope.showProductMaterial =true;
        $scope.showCustomTag =true;
        $scope.showComments =true;
        $scope.showFillinDate =true;
        $scope.showRepairDate =true;
        $scope.showRetailerComment =true;
        $scope.showPrice =true;
        $scope.showEndWarranty =true;
        $scope.showAddress =true;
        $scope.showBrandName =true;
        $scope.showHouseNumber=true;
        $scope.showZipCode =true;
        
        
        
        
        
        if($rootScope.user.tableOptions.length>0){
            $scope.count=$rootScope.user.tableOptions.length;
            } else {
                $scope.count=0;
            }

        $scope.marketingCustomers= data;
        console.log("$rootScope.user",$rootScope.user.tableOptions);

        $scope.tabOptions= ['Name','Phone Number','Product Status','Product Kind','Product Material','Custom Tag','Comments','Fillin Date','Repair Date','Retailer Comment','Price','End Warranty','Address','Brand Name','Zipcode','HouseNumber'];

        
        $scope.updateUser = function(){
            //$scope.user.userDetails.push($scope.user);
            console.log("DDahdasfd",$scope.user);
            if($scope.user.phone&&$scope.user.address){
                alert("Phone and Address added!!");
            } else {
                alert("Either the phone or the address is missing!!");
                return;
            }
		}


        $scope.updateTableOptions = function() {
            console.log("fgdfgdfgdfgdfgd",$scope.selectedOptions);
            var data = {
                _id : $rootScope.user._id,
                tableOptions: $scope.selectedOptions,
            }

            $api.post('shop/loyalty-repair/settings/tableOptions', { data: data })
			
				.then(function(response) {
					console.log("tableOptions",response);
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
        }

        $scope.deleteTableOptions = function(index,selectOpts){
            console.log("index",index+"   "+selectOpts);
            prompt({
                title: 'Adres verwijderen?',
                message: 'Delete this product kind from the list?'
            }).then(function() {
                $scope.selectedOptions.splice(index, 1);
                if(selectOpts=='Name'){
                    $scope.showName =true;
                }
                if(selectOpts=='Phone Number'){
                    $scope.showPhoneNumber =true;
                }
                if(selectOpts=='Product Status'){
                    $scope.showProductStatus =true;
                }
                if(selectOpts=='Product Kind'){
                    $scope.showProductKind =true;
                }
                if(selectOpts=='Product Material'){
                    $scope.showProductMaterial =true;
                }
                if(selectOpts=='Custom Tag'){
                    $scope.showCustomTag =true;
                }
                if(selectOpts=='Comments'){
                    $scope.showComments =true;
                }
                if(selectOpts=='Fillin Date'){
                    $scope.showFillinDate =true;
                }
                if(selectOpts=='Repair Date'){
                    $scope.showRepairDate =true;
                }
                if(selectOpts=='Retailer Comment'){
                    $scope.showRetailerComment =true;
                } 
                if(selectOpts=='Price'){
                    $scope.showPrice =true;
                }
                if(selectOpts=='End Warranty'){
                    $scope.showEndWarranty =true;
                }
                if(selectOpts=='Address'){
                    $scope.showAddress =true;
                }
                if(selectOpts=='Brand Name'){
                    $scope.showBrandName =true;
                }
                if(selectOpts=='Zipcode'){
                    $scope.showZipCode =true;
                }
                if(selectOpts=='HouseNumber'){
                    $scope.showHouseNumber =true;
                }
                $scope.updateTableOptions();
            });
        }

        $retailer.getShop($scope.nameSlug)
            .then(function (shop) {
                $scope.currentShop = shop;

                $api.get('crmuserscount', { filter: { 'shopSlug': $scope.nameSlug } })
                    .then(function (response) {
                        console.log("response", response);
                        if ((!shop.proShop && parseInt(response.data.crmUsers) >= shop.userLimit) || (shop.proShop && parseInt(response.data.crmUsers) >= shop.userLimit)) {
                            $scope.alert = {
                                type: "danger",
                                msg: "Limiet overschreden. Het is momenteel niet mogelijk om uw garantie te registreren of uw aankoopbon naar uw emailadres te sturen. Onze excuses voor het ongemak. U kunt nog wel uw mening geven op onze Review pagina."
                            }
                            $scope.buttonDisabled = true
                        } else {
                            $scope.buttonDisabled = false
                        }
                    });

            })
            .catch(function (reason) {
                $scope.alert = {
                    type: "danger",
                    msg: reason
                }
            });


            $scope.addTabOptions=function(data) {
                //alert(data);
                var f=0;
                
                if($scope.selectedOptions.length>=5){
            
                    alert("Max 5 Options Allowed!!");
                    return;
                }
                if($scope.count==0){
                    if(data=='Name'){
                        $scope.showName =false;
                    }
                    if(data=='Phone Number'){
                        $scope.showPhoneNumber =false;
                    }
                    if(data=='Product Status'){
                        $scope.showProductStatus =false;
                    }
                    if(data=='Product Kind'){
                        $scope.showProductKind =false;
                    }
                    if(data=='Product Material'){
                        $scope.showProductMaterial =false;
                    }
                    if(data=='Custom Tag'){
                        $scope.showCustomTag =false;
                    }   if(data=='Name'){
                        $scope.showName =false;
                    }
                    if(data=='Phone Number'){
                        $scope.showPhoneNumber =false;
                    }
                    if(data=='Product Status'){
                        $scope.showProductStatus =false;
                    }
                    if(data=='Product Kind'){
                        $scope.showProductKind =false;
                    }
                    if(data=='Product Material'){
                        $scope.showProductMaterial =false;
                    }
                    if(data=='Custom Tag'){
                        $scope.showCustomTag =false;
                    }
                    if(data=='Comments'){
                        $scope.showComments =false;
                    }
                    if(data=='Fillin Date'){
                        $scope.showFillinDate =false;
                    }
                    if(data=='Repair Date'){
                        $scope.showRepairDate =false;
                    }
                    if(data=='Retailer Comment'){
                        $scope.showRetailerComment =false;
                    } 
                    if(data=='Price'){
                        $scope.showPrice =false;
                    }
                    if(data=='End Warranty'){
                        $scope.showEndWarranty =false;
                    }
                    if(data=='Comments'){
                        $scope.showComments =false;
                    }
                    if(data=='Fillin Date'){
                        $scope.showFillinDate =false;
                    }
                    if(data=='Repair Date'){
                        $scope.showRepairDate =false;
                    }
                    if(data=='Retailer Comment'){
                        $scope.showRetailerComment =false;
                    } 
                    if(data=='Price'){
                        $scope.showPrice =false;
                    }
                    if(data=='End Warranty'){
                        $scope.showEndWarranty =false;
                    }
                    if(data=='Address'){
                        $scope.showAddress =false;
                    }
                    if(data=='Brand Name'){
                        $scope.showBrandName =false;
                    }
                    if(data=='Zipcode'){
                        $scope.showZipCode =false;
                    }
                    if(data=='HouseNumber'){
                        $scope.showHouseNumber =false;
                    }
                $scope.selectedOptions.push(data);
                $scope.count++;
                //alert("asdfsafsdg!!");
                return;
                }
                console.log(' $scope.selectedOptions', $scope.selectedOptions);
                for(var sOptions in $scope.selectedOptions){
                    if($scope.selectedOptions[sOptions]==data){
                        f=1;
                        alert("Exists!!");
                        break;
                    }
                }
                if(f==0){
                    if(data=='Name'){
                        $scope.showName =false;
                    }
                    if(data=='Phone Number'){
                        $scope.showPhoneNumber =false;
                    }
                    if(data=='Product Status'){
                        $scope.showProductStatus =false;
                    }
                    if(data=='Product Kind'){
                        $scope.showProductKind =false;
                    }
                    if(data=='Product Material'){
                        $scope.showProductMaterial =false;
                    }
                    if(data=='Custom Tag'){
                        $scope.showCustomTag =false;
                    }
                    if(data=='Comments'){
                        $scope.showComments =false;
                    }
                    if(data=='Fillin Date'){
                        $scope.showFillinDate =false;
                    }
                    if(data=='Repair Date'){
                        $scope.showRepairDate =false;
                    }
                    if(data=='Retailer Comment'){
                        $scope.showRetailerComment =false;
                    } 
                    if(data=='Price'){
                        $scope.showPrice =false;
                    }
                    if(data=='End Warranty'){
                        $scope.showEndWarranty =false;
                    }
                    if(data=='Address'){
                        $scope.showAddress =false;
                    }
                    if(data=='Brand Name'){
                        $scope.showBrandName =false;
                    }
                    if(data=='Zipcode'){
                        $scope.showZipCode =false;
                    }
                    if(data=='HouseNumber'){
                        $scope.showHouseNumber =false;
                    }
                    
                    $scope.selectedOptions.push(data);
                    $scope.count++;
                }

                $scope.updateTableOptions();
                
            }
            $scope.tablecolumns= function(){
                for(var i=0;i<$scope.selectedOptions.length;i++){
                    if($scope.selectedOptions[i]=='Name'){
                        $scope.showName =false;
                    }
                    if($scope.selectedOptions[i]=='Phone Number'){
                        $scope.showPhoneNumber =false;
                    }
                    if($scope.selectedOptions[i]=='Product Status'){
                        $scope.showProductStatus =false;
                    }
                    if($scope.selectedOptions[i]=='Product Kind'){
                        $scope.showProductKind =false;
                    }
                    if($scope.selectedOptions[i]=='Product Material'){
                        $scope.showProductMaterial =false;
                    }
                    if($scope.selectedOptions[i]=='Custom Tag'){
                        $scope.showCustomTag =false;
                    }
                    if($scope.selectedOptions[i]=='Comments'){
                        $scope.showComments =false;
                    }
                    if($scope.selectedOptions[i]=='Fillin Date'){
                        $scope.showFillinDate =false;
                    }
                    if($scope.selectedOptions[i]=='Repair Date'){
                        $scope.showRepairDate =false;
                    }
                    if($scope.selectedOptions[i]=='Retailer Comment'){
                        $scope.showRetailerComment =false;
                    } 
                    if($scope.selectedOptions[i]=='Price'){
                        $scope.showPrice =false;
                    }
                    if($scope.selectedOptions[i]=='End Warranty'){
                        $scope.showEndWarranty =false;
                    }
                    if($scope.selectedOptions[i]=='Address'){
                        $scope.showAddress =false;
                    }
                    if($scope.selectedOptions[i]=='Brand Name'){
                        $scope.showBrandName =false;
                    }
                    if($scope.selectedOptions[i]=='Zipcode'){
                        $scope.showZipCode =false;
                    }
                    if($scope.selectedOptions[i]=='HouseNumber'){
                        $scope.showHouseNumber =false;
                    }
                }
            }
    
        
}])