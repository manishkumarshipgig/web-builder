prismanoteApp.controller("previewNewsItemModalController", ['$scope', '$uibModalInstance', 'newsItem', '$api', '$http', '$rootScope',
function($scope, $uibModalInstance, newsItem, $api, $http, $rootScope) {
    
    $scope.newsItem = newsItem;
    

    $scope.confirm = function(){
        $uibModalInstance.close(true);
    }

}]);