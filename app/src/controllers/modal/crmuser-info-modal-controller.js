prismanoteApp.controller("crmUserInfoModalController", ['$scope', '$uibModalInstance', 'customer', '$api', '$http', '$rootScope','campaignFact', '$state', '$uibModal',
function($scope, $uibModalInstance, customer, $api, $http, $rootScope,campaignFact, $state, $uibModal) {
	
	console.log("crmUser", customer)
	$scope.crmUser = customer;
	
	$scope.itemsRegistration = 0;
	$scope.itemsRepair = 0;
	$scope.itemsWebshopOrder = 0;
	$scope.itemsTransactions = 0;
	$scope.mailResult = null;

	$scope.loadUser = function(crmUser) {

		$api.post('user-for-crm', {crmUser: crmUser})
		
			.then(function(response) {
				if(response.data){
					$scope.user = response.data.user;
				}else{
					$scope.alert = {
						type: "warning",
						msg: "Deze klant heeft nog geen account op uw webshop, als u op opslaan klikt krijgt de klant automatisch een email of deze zijn garantiebewijs, reparatie status, etc in wil zien via uw website."
					}

					var crmUserData = {
						username: $scope.crmUser.email,
						email: $scope.crmUser.email,
						gender: $scope.crmUser.gender
					}

					$scope.user = crmUserData;
				}
			})
			
			.catch(function(reason) {
				console.log(reason);
			});

			console.log("$scope.userkv",	$scope.user);
	};

    $scope.confirm = function(){
		$uibModalInstance.close(true);
	}

    $scope.resetActivities = function(){
		$scope.crmUser.items = activitiesWithoutOrders;
	}

	$scope.openAddress = function(index){
		$scope.resetSelected();
		$scope.addNewAddress = false;
		$scope.currentAddress = $scope.user.address[index];
		$scope.currentAddress.index = index;
	}

	$scope.newAddress = function() {
		$scope.resetSelected();
		$scope.addNewAddress = true;
	}

	$scope.deleteAddress = function(index){
		prompt({
			title: 'Adres verwijderen?',
			message: 'Weet u zeker dat u dit adres wilt verwijderen?'
		}).then(function() {
			$scope.user.address.splice(index, 1);
			$scope.resetSelected();
		});
	}

	$scope.pushAddress = function() {
		$scope.user.address.push($scope.currentAddress);
		$scope.addNewAddress = false;
	}

	$scope.cancelNewAddress = function() {
		$scope.addNewAddress = false;
		$scope.resetSelected();
	}


	$scope.openPhone = function(index){
			$scope.resetSelected();
			$scope.currentPhone = $scope.user.phone[index];
			$scope.currentPhone.index = index;
	}

	$scope.deletePhone = function(index){
			prompt({
					title: 'Telefoonnummer verwijderen?',
					message: 'Weet u zeker dat u dit telefoonnummer wilt verwijderen?'
			}).then(function() {
					$scope.user.phone.splice(index, 1);
					$scope.resetSelected();
			});
	}

	$scope.newPhone = function(index){
			$scope.resetSelected();
			$scope.addNewPhone = true;
	}

	$scope.pushPhone = function() {
		$scope.user.phone.push($scope.currentPhone);
		$scope.addNewPhone = false;
	}

	$scope.cancelNewPhone = function() {
			$scope.addNewPhone = false;
			$scope.resetSelected();
	}

		$scope.resetSelected = function() {

		$scope.currentAddress = null;
		$scope.currentPhone = null;
	}

	$scope.loadUserWebshopOrders = function(crmUser) {
		$api.post('user-for-crm', {crmUser: crmUser})
		
			.then(function(response) {
				if(response.data){
					console.log("This user has an account");
					$scope.user = null;
					$scope.user = response.data.user;

					$scope.loading = true;

					$api.get('orders', {filter: {'user._id' : $scope.user._id}, 'sort': {'_id': 'desc'}})
		
					.then(function(response) {
						$scope.orders = null;
						$scope.orders = response.data.orders;
						
						//-adding orders to crmUser.items
						var counter = 0;
						for(var i=0; i < $scope.orders.length; i++){
							if($scope.crmUser.items.indexOf($scope.orders[i].number)){
								var item = { 
									productTypeId: 5,
									number: response.data.orders[i].number,
									comment: response.data.orders[i].comment,
									fillindate: response.data.orders[i].date,
									products: response.data.orders[i].items
								}
								$scope.crmUser.items.push(item)
							}
							counter++;
							if(counter == $scope.orders.length){
								//- fill Pie chart
								var counter = 0;
								$scope.totalPriceTransactions = 0;
								$scope.totalWebshopOrders = 0;
								$scope.totalPriceRegistrations = 0;
								$scope.totalPriceRepairs = 0;
		
								for(var i =0; i < $scope.crmUser.items.length; i++){
									if($scope.crmUser.items[i].productTypeId === 1 || $scope.crmUser.items[i].productTypeId === 0){
										$scope.itemsRegistration ++
										$scope.totalPriceRegistrations = $scope.totalPriceRegistrations + $scope.crmUser.items[i].priceRange
									}
									if($scope.crmUser.items[i].productTypeId === 2){
										$scope.itemsRepair ++
										$scope.totalPriceRepairs = $scope.totalPriceRepairs + $scope.crmUser.items[i].priceRange
									}
									if($scope.crmUser.items[i].productTypeId === 4){
										$scope.itemsTransactions ++
										$scope.totalPriceTransactions = $scope.totalPriceTransactions + $scope.crmUser.items[i].priceRange
									}
									if($scope.crmUser.items[i].productTypeId === 5){
										$scope.itemsWebshopOrder ++
										$scope.totalWebshopOrders = $scope.totalWebshopOrders + $scope.crmUser.items[i].priceRange
									}
									counter++
								}
								if(counter == $scope.crmUser.items.length){
									pieChart();
								}
							}
						}			

						for(var i =0; i < $scope.orders.length; i++){
							var total = 0;
							for(var t=0; t < $scope.orders[i].items.length; t++){
								total = total + ($scope.orders[i].items[t].price * $scope.orders[i].items[t].quantity);
							}
							$scope.orders[i].total = total;
							$scope.orders[i].currentStatus = angular.lowercase($scope.orders[i].status[$scope.orders[i].status.length-1].status);
		
							// $scope.orders[i].paid = false;
							// for(var p=0; p < $scope.orders[i].status.length; p++){
							// 	if(angular.lowercase($scope.orders[i].status[p].status) == 'paid'){
							// 		$scope.orders[i].paid = true;
							// 		break;
							// 	}
							// }

							$scope.loading = false;
						}
					})
					
					.catch(function(reason) {
						console.log(reason);
						var counter = 0;
						$scope.totalPriceTransactions = 0;
						$scope.totalWebshopOrders = 0;
						$scope.totalPriceRegistrations = 0;
						$scope.totalPriceRepairs = 0;

						for(var i =0; i < $scope.crmUser.items.length; i++){
							if($scope.crmUser.items[i].productTypeId === 1 || $scope.crmUser.items[i].productTypeId === 0){
								$scope.itemsRegistration ++
								$scope.totalPriceRegistrations = $scope.totalPriceRegistrations + $scope.crmUser.items[i].priceRange
							}
							if($scope.crmUser.items[i].productTypeId === 2){
								$scope.itemsRepair ++
								$scope.totalPriceRepairs = $scope.totalPriceRepairs + $scope.crmUser.items[i].priceRange
							}
							if($scope.crmUser.items[i].productTypeId === 4){
								$scope.itemsTransactions ++
								$scope.totalPriceTransactions = $scope.totalPriceTransactions + $scope.crmUser.items[i].priceRange
							}
							if($scope.crmUser.items[i].productTypeId === 5){
								$scope.itemsWebshopOrder ++
								$scope.totalWebshopOrders = $scope.totalWebshopOrders + $scope.crmUser.items[i].priceRange
							}
							counter++
						}
						if(counter == $scope.crmUser.items.length){
							pieChart();
						}
						
					});

				}else{
					$scope.alert = {
						type: "warning",
						msg: "Deze klant heeft nog orders op uw webshop geplaatst."
					}
					$scope.loading = false;
					var counter = 0;
					$scope.totalPriceTransactions = 0;
					$scope.totalWebshopOrders = 0;
					$scope.totalPriceRegistrations = 0;
					$scope.totalPriceRepairs = 0;

					for(var i =0; i < $scope.crmUser.items.length; i++){
						if($scope.crmUser.items[i].productTypeId === 1 || $scope.crmUser.items[i].productTypeId === 0){
							$scope.itemsRegistration ++
							$scope.totalPriceRegistrations = $scope.totalPriceRegistrations + $scope.crmUser.items[i].priceRange
						}
						if($scope.crmUser.items[i].productTypeId === 2){
							$scope.itemsRepair ++
							$scope.totalPriceRepairs = $scope.totalPriceRepairs + $scope.crmUser.items[i].priceRange
						}
						if($scope.crmUser.items[i].productTypeId === 4){
							$scope.itemsTransactions ++
							$scope.totalPriceTransactions = $scope.totalPriceTransactions + $scope.crmUser.items[i].priceRange
						}
						if($scope.crmUser.items[i].productTypeId === 5){
							$scope.itemsWebshopOrder ++
							$scope.totalWebshopOrders = $scope.totalWebshopOrders + $scope.crmUser.items[i].priceRange
						}
						counter++
					}
					if(counter == $scope.crmUser.items.length){
						pieChart();
					}
				}
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

	campaignFact.getsocialPortal().then(function (res) {

	})
	function pieChart() {
		Highcharts.chart('dealerDonut', {
			chart: {
				type: 'pie'
			},
			title: {
				verticalAlign: 'top',
				align: 'center',
				x: 15,
				y: 10,
				text: ''
			},
			credits: {
				enabled: false
			},
			exporting: {
				enabled: false
			},
			plotOptions: {
				pie: {
					shadow: false
				}
			},
			tooltip: {
				formatter: function () {
					return '<b>' + this.point.name + '</b>: ' + this.y ;
				}
			},
			series: [{
				// name: 'Browsers',
				size: '100%',
				innerSize: '80%',
				showInLegend: false,
				dataLabels: {
					enabled: false
				},
				data: [
					{
						y: $scope.itemsTransactions,
						name: "Transacties €" + $scope.totalPriceTransactions,
						color: "#16936c"
					},
					{
						y: $scope.itemsWebshopOrder,
						name: "Webshop orders €" + $scope.totalWebshopOrders,
						color: "#000331"
					},
					{
						y: $scope.itemsRegistration,
						name: "Aankoop registraties €" + $scope.totalPriceRegistrations,
						color: "#6094c2"
					},
					{
						y: $scope.itemsRepair,
						name: "Reparaties €" + $scope.totalPriceRepairs,
						color: "#337ab7"
					},
				]
			}]
		});
	}

	$scope.disableMail = function(index, userId){
		for(var i =0; i < $scope.crmUser.plannedEmail.length; i++){
			$scope.crmUser.plannedEmail[i].enabled = false;
			$scope.savePlanned(i, false, userId);
		}
	}

	$scope.savePlanned = function(index, value, userId){
		$api.post('crmusers', {emailIndex: index, userId: userId, enabled: value})

		.then(function(response) {
			console.log(response);
		})

		.catch(function(reason) {
			console.log(reason);
		});
	}

	$scope.openPreviewMailModal = function(data){
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/preview-email-modal.html',
			controller: 'previewEmailModalController',
			size: 'lg',
			resolve: {
				data: function(){
					return data;
				}
			}
		});

		modalInstance.result.then(function(){

		}, function(){
			//dismissed
		})

	}

	$scope.openEditMailModal = function (data) {
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/edit-email-modal.html',
			controller: 'previewEmailModalController',
			size: 'lg',
			resolve: {
				data: function () {
					return data;
				}
			}
		});

		modalInstance.result.then(function () {

		}, function () {
			//dismissed
		})

	}


	$scope.openAddMailModal = function (data) {
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/create-email-modal.html',
			controller: 'previewEmailModalController',
			size: 'lg',
			resolve: {
				data: function () {
					return data;
				}
			}
		}).closed.then(function () {
			//window.alert('Modal closed');

			$api.get('crmusers', { filter: { 'shopSlug': $scope.currentShop.nameSlug }, 'sort': { '_id': 'desc' } })

				.then(function (response) {
					console.log(response.data.crmUsers);
					$scope.marketingCustomers = response.data.crmUsers;
					$scope.loading = false;
				})

				.catch(function (reason) {
					console.log(reason);
					$scope.loading = false;

				});
		});

		modalInstance.result.then(function () {
		   
		}, function () {
			//dismissed
		})

	}

	$scope.openRepairPhotoModal = function (data) {
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/repair-photo-upload.html',
			controller: 'previewEmailModalController',
			size: 'lg',
			resolve: {
				data: function () {
					return data;
				}
			}
		}).closed.then(function () {
			//window.alert('Modal closed');

			$api.get('crmusers', { filter: { 'shopSlug': $scope.currentShop.nameSlug }, 'sort': { '_id': 'desc' } })

				.then(function (response) {
					console.log(response.data.crmUsers);
					$scope.marketingCustomers = response.data.crmUsers;
					$scope.loading = false;
				})

				.catch(function (reason) {
					console.log(reason);
					$scope.loading = false;

				});
		});

		modalInstance.result.then(function () {

		}, function () {
			//dismissed
		})

	}

	$scope.openAddMailModal = function (data) {
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/create-email-modal.html',
			controller: 'previewEmailModalController',
			size: 'lg',
			resolve: {
				data: function () {
					return data;
				}
			}
		}).closed.then(function () {
			//window.alert('Modal closed');

			$api.get('crmusers', { filter: { 'shopSlug': $scope.currentShop.nameSlug }, 'sort': { '_id': 'desc' } })

				.then(function (response) {
					console.log(response.data.crmUsers);
					$scope.marketingCustomers = response.data.crmUsers;
					$scope.loading = false;
				})

				.catch(function (reason) {
					console.log(reason);
					$scope.loading = false;

				});
		});

		modalInstance.result.then(function () {

		}, function () {
			//dismissed
		})

	}

	$scope.updateCrmUser = function (data) { // contains data.customer._id
    // console.log("inside updateCrmUser");
		// var modalInstance = $uibModal.open({
		// 	templateUrl: '../views/modal/crm-user-details-modal.html',
		// 	controller: 'emailCompanyController',
		// 	size: 'lg',
		// 	resolve: {
		// 		data: function () {
		// 			return data;
		// 		}
		// 	}
		// }).closed.then(function () {

		alert("Details Saved Successfully.");
		
			var crmUser = $scope.crmUser;

			if($scope.crmUser.retailerComments.length == 0){
				var comment = {
					comment: '',
					date: Date.now()
				}
				crmUser.retailerComments.push(comment)
			}
			var retailerComment = {
				comment: $scope.crmUser.retailerComments[0].comment,
				date: new Date(),
				user: $rootScope.user
			}
	
			crmUser.retailerComments = [];
			crmUser.retailerComments.push(retailerComment)
	
			$api.put('crmusers', {crmUser: crmUser})
			
				.then(function(response) {
					$state.go('retailer.loyalty-portal');
					console.log("response", response);
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		
			
		
	//	});


	}

	$scope.popover = function(item){
		return '<img src="' + item.photo.src + '" alt="'+ item.photo.alt +'" height: 200px />';;
	}

	$scope.sendTransactionMail = function(transaction){
		$api.post('countr/send-transaction-mail', {
			shopId: $rootScope.currentShop._id,
			transactionId: transaction.countrId,
			to: $scope.crmUser.email
		})
		.then(function(response) {
			$scope.mailResult = {
				type: 'success',
				msg: 'Overzicht is gemaild naar ' + $scope.crmUser.email
			};
		})
		.catch(function(reason) {
			console.log(reason);
			$scope.mailResult = {
				type: 'danger',
				msg: reason
			};
		})
	}

	$scope.updateUser = function () { 
		$api.put('user/' + $scope.user._id, {user: $scope.user})
		
			.then(function(response) {
				console.log("response", response);
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	}

	$scope.openEmailCompanyEmail = function (data) {
		//alert("openEmailCompanyEmail");
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/email-company-modal.html',
			controller: 'emailCompanyController',
			size: 'lg',
			resolve: {
				data: function () {
					return data;
				}
			}
		}).closed.then(function () {
			//window.alert('Modal closed');

			$api.get('crmusers', { filter: { 'shopSlug': $scope.currentShop.nameSlug }, 'sort': { '_id': 'desc' } })

				.then(function (response) {
					console.log(response.data.crmUsers);
					$scope.marketingCustomers = response.data.crmUsers;
					$scope.loading = false;
				})

				.catch(function (reason) {
					console.log(reason);
					$scope.loading = false;

				});
		});

		modalInstance.result.then(function () {

		}, function () {
			//dismissed
		})
	
	}

}]);