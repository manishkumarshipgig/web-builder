prismanoteApp.controller("removeProductsModalController", ['$scope', '$uibModalInstance', 
	function($scope, $uibModalInstance) {
		
		$scope.close = function(options) {
				$uibModalInstance.close(options);
		}

		$scope.cancel = function(reason) {
				$uibModalInstance.dismiss(reason);
		}

}]);