prismanoteApp.controller("createStoreModalController", ['$scope', '$uibModalInstance', '$countr', '$retailer',
	function($scope, $uibModalInstance, $countr, $retailer) {

        console.log("createStoreModalController");

        $scope.loadModal = function(){
            $retailer.getShop()
            .then(function(shop) {
                $scope.store = {
                    name: shop.name,
                    street: shop.address.street,
                    number: shop.address.houseNumber,
                    numbersuffix: shop.address.houseNumberSuffix,
                    zip: shop.address.postalCode,
                    city: shop.address.city,
                    country: shop.address.country
                }
            })
        }

        

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.close = function(){
            $uibModalInstance.close($scope.store);
        }

        $scope.submit = function(valid){
            if(!valid){
                return;
            }
            $scope.loading = true;
            var shop = $scope.store;
            var store = {
                name: shop.name,
                address: {
                    address1: shop.street + " " + shop.number + " " +(shop.numbersuffix ? shop.numbersuffix : ""),
                    city: shop.city,
                    country: "nl",
                    zip: shop.zip
                },
                currency: "EUR",
                timezone: "CET"
            }

            $countr.createShop(store)
            	.then(function(response) {
                    $scope.loading = false;
                    $uibModalInstance.close(response.store);
                })
                .catch(function(reason) {
                    console.error(reason);
                    $scope.alert = {
                        type: 'danger',
                        msg: reason
                    }
                })
        }

		
	}
]);