prismanoteApp.controller("chooseShopModalController", ['$scope', '$uibModalInstance', 'item',
	function($scope, $uibModalInstance, item) {

		// Demo content while no function exists in the backend yet to actually find relevant shops.
		$scope.shops = [
			{
				name: 'Juwelier Bos',
				nameSlug: 'juwelier-bos',
				price: 119.99
			},
			{
				name: 'Horloge Magazijn',
				nameSlug: 'horloge-magazijn',
				price: 129.99
			}
		];

		$scope.chooseShop = function(shop) {
			item.shop = {name: shop.name, nameSlug: shop.nameSlug};
			item.price = shop.price;
			$uibModalInstance.close(item);
		};

		$scope.item = item;
	}
]);