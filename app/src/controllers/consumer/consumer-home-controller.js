prismanoteApp.controller('consumerHomeController', ['$scope', '$rootScope', '$api', '$translate', function($scope, $rootScope, $api, $translate) {
    
        $rootScope.breadcrumb = ['Home'];
        $rootScope.pageTitle = 'PrismaNote';
        
        $api.get('home')
    
        	.then(function(response) {
        		$rootScope.homeBrandCount = response.data.brandCount;
        		$rootScope.homeProductCount = response.data.productCount;
        		$rootScope.homeShopCount = response.data.shopCount;
        	})
            
        	.catch(function(reason) {
        		console.log(reason);
        	})
            
        	.finally(function() {
    
        		$api.get('products', {filter: {isVerified: true}, sort: {uploadDate: 'desc'}, limit: 3})
                
        			.then(function(response) {
        				$scope.newProducts = response.data.products;
        			})
                    
        			.catch(function(reason) {
        				console.log(reason);
        			})
                    
        			.finally(function() {
    
        				$api.get('products', {filter: {isVerified: true}, sort: {views: 'desc'}, limit: 3})
                        
        					.then(function(response) {
        						$scope.popularProducts = response.data.products;
        					})
                            
        					.catch(function(reason) {
        						console.log(reason);
        					})
                            
        					.finally(function() {
    
        						$api.get('products', {filter: {isFeatured: true, isVerified: true}, sort: {views: 'desc'}, limit: 3})
                                
        							.then(function(response) {
        								$scope.featuredProducts = response.data.products;
        							})
                                    
        							.catch(function(reason) {
        								console.log(reason);
        							})
                                    
        							.finally(function() {
    
        								$api.get('news', {sort: {publicationDate: 'desc'}, limit: 3})
                                        
        									.then(function(response) {
        										$scope.news = response.data.newsItems;
        									})
                                            
        									.catch(function(reason) {
        										console.log(reason);
        									})
                                            
        									.finally(function() {
    
        										$api.get('brands', {filter: {isFeatured: true}, limit: 4})
                                                
        											.then(function(response) {
        												$scope.featuredBrands = response.data.brands;
        											})
                                                    
        											.catch(function(reason) {
        												console.log(reason);
        											});
        									});
        							});
        					});
        			});
        	});
    
    }]);
    