prismanoteApp.controller('shopController', ['$scope', '$rootScope', '$q', '$cart', '$state', '$stateParams', '$api', '$window', '$transitions', '$uibModal', '$translate', '$timeout',
	function($scope, $rootScope, $q, $cart, $state, $stateParams, $api, $window, $transitions, $uibModal, $translate, $timeout) {

		$scope.nameSlug = $rootScope.getShopType() == 'proshop' ? localStorage.getItem('nameSlug') : $stateParams.nameSlug;
		
		doActivate();
		function doActivate(){

			$api.get('webshop-products-categories/' + $scope.nameSlug,{})
			.then(function(response){
				if(!response.data.cats) return; 
				$scope.shopCats = response.data.cats;
				$scope.setCategory($scope.shopCats[0]);

				console.log();
			},function(err){
				console.error("ERROR WHILE GETTING SHOP CATS ",err);
			});

		}

		// Array with all currently displayed items in /products
		$scope.products = [];

		$scope.productCategory = [];

		$scope.hideFilters = true;

		$scope.noProductsFound = false;

		if($stateParams.category && typeof $stateParams.category == 'string' && ['WATCH', 'STRAP', 'JEWEL', 'OTHER'].includes($stateParams.category)) {
			$scope.category[$stateParams.category.toLowerCase()] = true;
			$scope.productCategory = [$stateParams.category];
		} else {
			$scope.productCategory = ['WATCH', 'STRAP', 'JEWEL', 'OTHER'];
		}


		// Featured products in homepage block
		$scope.featuredProducts = [];

		$scope.sortShopProducts = {};

		$scope.limit = 24;

		$scope.setLimit = function(limit) {
			$scope.limit = limit;
		}

		// Parse gender state params to $scope so that the filters will be updated on load
		$scope.gents = $stateParams.gents;
		$scope.ladies = $stateParams.ladies;
		$scope.boys = $stateParams.boys;
		$scope.girls = $stateParams.girls;

		$scope.activeSlide = 0;

		$scope.yPos = 0;
		$scope.yTreshold = 400;

		$scope.today = new Date();

		$scope.isShop = true;

		$scope.range = function(num) {
			var range = []
			for(var i = 0; i < num; i++) {
				range.push(i);
			}
			return range;
		}

		$scope.productCategory = 'WATCH';
		$scope.filtersWatch = true;

		$scope.setCategory = function(category) {
			console.log("Calling SetCategory() for category = ",category);
			$scope.products = [];

			$scope.productCategory = [category];

			if($scope.productCategory == 'WATCH'){
				$scope.filtersWatch = true;
				$scope.filtersStrap = false;
				$scope.filtersJewel = false;
			}else if ($scope.productCategory == 'STRAP'){
				$scope.filtersWatch = false;
				$scope.filtersStrap = true;
				$scope.filtersJewel = false;
			}else if($scope.productCategory == 'JEWEL'){
				$scope.filtersWatch = false;
				$scope.filtersStrap = false;
				$scope.filtersJewel = true;
			}else if($scope.productCategory == 'OTHER'){
				$scope.filtersWatch = false;
				$scope.filtersStrap = false;
				$scope.filtersJewel = true;
			}
			$scope.getProducts({filter: {category: $scope.productCategory}}, {reset: true})

			.catch(function(reason) {
				console.log(reason);
			});
		}

		// format kids, male and female booleans into a translated and formatted gender string
		var formatGender = function(male, female, kids) {
			return $q(function(resolve) {

				if(kids == false) {
					if(male == true && female == true) {
						$translate('GENTS').then(function(gents) {
							$translate('LADIES').then(function(ladies) {
								resolve(gents + ' / ' + ladies);
							});
						});
					} else if(male == true && female == false) {
						$translate('GENTS').then(function(gents) {
							resolve(gents);
						});
					} else if(male == false && female == true) {
						$translate('LADIES').then(function(ladies) {
							resolve(ladies);
						});
					}
				} else {
					if(male == true && female == true) {
						$translate('BOYS').then(function(boys) {
							$translate('GIRLS').then(function(girls) {
								resolve(boys + ' / ' + girls);
							});
						});
					} else if(male == true && female == false) {
						$translate('BOYS').then(function(boys) {
							resolve(boys);
						});
					} else if(male == false && female == true) {
						$translate('GIRLS').then(function(girls) {
							resolve(girls);
						});
					}
				}
			})
		};

		// Async function to add/format gender, shop name and possibly other additional properties later on
		var formatProduct = function(product) {
			return $q(function(resolve, reject) {

				formatGender(product.male, product.female, product.kids)

				.then(function(formattedGender) {
					product.shop = {name: $scope.shop.name, nameSlug: $scope.nameSlug};
					product.gender = formattedGender;
					resolve(product);
				})

				.catch(function(reason) {
					reject(reason);
				});
			});
		};
		$scope.setLimit = function(limit) {
			if(limit != null) {
				$scope.limit = limit;
			} else {
				$scope.limit = 24;
			}
		}

		// Get new products from the database and add them to the $scope
		$scope.getProducts = function(params, options) {

			return $q(function(resolve, reject) {

				if(!params) {
					params = {};
				}
				if(!params.sort) {
					params.sort = {};
				}
				if(!params.filter) {
					params.filter = {};
				}
				if(!params.filter._id) {
					params.filter._id = {};
				}

				if(params.filter.category && params.filter.category.$in) {
					$scope.productCategory = params.filter.category.$in;
				} else {
					$scope.productCategory = [];
				}

				if(params.sort.price == 'asc' || params.sort.price == 'desc') {
					$scope.sortShopProducts.price = params.sort.price;
				} else if(params.sort != {}) {
					$scope.sortShopProducts = {};
				}

				// Delete the loaded products and load them again with the new apiParams
				console.log("RESET")
				if(options != null && typeof options === 'object') {
					console.log("RESET FOUND")
					if(options.reset === true) {
						console.log("Blanking $scope.products")
						$scope.products = [];
					}
				}


				params.filter.includeUnverified = true;
				params.offset = $scope.products.length;

				params.limit = $scope.limit;
				params.filter.containsFilterInfo = true;

				params.shopFilter = {
					inStock : true
				}

				var apiRequest = function() {
					console.log("Sending request from shop-controller : ",params)
					$api.get('webshop-products/' + $scope.nameSlug, params)
					.then(function(response){

						

						for(var i = 0; i < response.data.products.length; i++) {
							

							formatProduct(response.data.products[i])
							.then(function(formattedProduct) {
								


								var matchedShopProduct = _.find($scope.shop.products, {_id: formattedProduct._id});
								var mergedProduct = Object.assign(formattedProduct, matchedShopProduct);

								// mergedProduct.price = matchedShopProduct.price;
								if(options != null && typeof options === 'object' && options.featured === true) {
									$scope.featuredProducts.push(mergedProduct);
								} else {
									
									$scope.products.push(mergedProduct);
								}

								// if($scope.products.length == params.offset + response.data.products.length) {
									if($scope.sortShopProducts != null && typeof $scope.sortShopProducts.price === 'string') {
										if($scope.sortShopProducts.price == 'asc') {
											$scope.products.sort(function(a,b) { return a.price - b.price; });
										} else if($scope.sortShopProducts.price == 'desc') {
											$scope.products.sort(function(a,b) { return b.price - a.price; });
										}
									}
								// }
							})

							.catch(function(reason) {
								reject(reason);
							});
						}

						$scope.noProductsFound = false;

						resolve();

					})

					.catch(function(reason) {

						$scope.noProductsFound = true;

						reject(reason);
					});
				}



				apiRequest();
			});
		};

		var onScroll = function() {
			$scope.yPos = document.body.scrollTop || window.scrollY;
			$scope.$digest();
		};

		var toggleScroll = function() {
			if($state.$current.name == $rootScope.getShopType() + '.home') {
				angular.element($window).on('scroll', onScroll);
				$scope.home = true;
			} else {
				angular.element($window).off('scroll', onScroll);
				$scope.home = false;
			}
		};

		$transitions.onSuccess(true, function(transition) {
			$scope.state = transition.$to().name;
			$scope.showMenu = false;
			$rootScope.top();
			toggleScroll();
		});

		toggleScroll();

		var stateApiParams = {};

		if($stateParams.filter != null && typeof $stateParams.filter === 'object') {
			stateApiParams.filter = $stateParams.filter;
		}

		if($stateParams.sort != null && typeof $stateParams.sort === 'object') {
			stateApiParams.sort = $stateParams.sort;
		}

		if($stateParams.limit != null && typeof $stateParams.limit === 'number') {
			stateApiParams.limit = $stateParams.limit;
		}

		if($stateParams.offset != null && typeof $stateParams.offset === 'number') {
			stateApiParams.offset = $stateParams.offset;
		}

		$api.setProductParams(stateApiParams)
		.catch(function(reason) {
			console.log(reason);
		});


		// SHOP PORTION
		$api.get('shops/' + $scope.nameSlug)
		.then(function(response) {

			$scope.shop = response.data.shop;
			$scope.campaigns = [];

			if(response.data.portal && response.data.portal.campaigns && response.data.portal.campaigns.length > 0){
				for(var c =0; c < response.data.portal.campaigns.length; c++){
					for(var t=0; t < response.data.portal.campaigns[c].tasks.length; t++){
						if(response.data.portal.campaigns[c].tasks[t] && response.data.portal.campaigns[c].tasks[t].type == "facebook" && response.data.portal.campaigns[c].tasks[t].completed){
							$scope.campaigns.push(response.data.portal.campaigns[c].tasks[t]);
						}
					}
				}
				delete response.data.portal;
			}

			for(var i =0; i < $scope.shop.news.length; i++){
				if($scope.shop.news[i].isPublished){
					$scope.shop.news[i].news = true;
					$scope.campaigns.push($scope.shop.news[i]);
				}
			}

			$scope.brands = $scope.shop.brands;
			delete $scope.shop.brands;

			var productIds = [];
			var featuredProductIds = [];

			for(var i = 0; i < $scope.shop.products.length; i++) {
				productIds.push($scope.shop.products[i]._id);
				if(featuredProductIds.length < 4 && $scope.shop.products[i].isFeatured == true) {
					featuredProductIds.push($scope.shop.products[i]._id);
				}
			}

			$scope.productCount = productIds.length;

			if($scope.shop.isPremium === true) {

				$rootScope.pageTitle = $scope.shop.name;

					// Get featured products for proshop homepage filter: {}
					$scope.getProducts({sort: {views: 'desc'}, filter: {category: {$in: $scope.productCategory}}, limit: 4}, {featured: true})

					.catch(function(reason) {
						console.log(reason);
					})

					.finally(function() {

						$scope.getProducts({limit: $scope.limit}, {reset: true})

						.catch(function(reason) {
							console.log(reason);
						});
					});

					if($scope.home.spotlight) {
						// Find two items to fill the spotlight block
						$scope.spotlight = [];
						var randomBrand = $rootScope.randomNumber(0, $scope.brands.length - 1);
						$scope.spotlight[0] = $scope.brands[randomBrand];
						$scope.spotlight[0].type = 'brand';
						if($scope.shop.tasks.length > 0) {
							$scope.spotlight[1] = $scope.shop.tasks[$rootScope.randomNumber(0, $scope.shop.tasks.length - 1)];
							$scope.spotlight[1].type = 'campaign';
						} else if($scope.brands.length > 1) {
							if(typeof $scope.brands[randomBrand - 1] !== 'undefined') {
								$scope.spotlight[1] = $scope.brands[randomBrand - 1];
							} else {
								$scope.spotlight[1] = $scope.brands[randomBrand + 1];
							}
							$scope.spotlight[1].type = 'brand';
						}
						if($scope.spotlight[0]){
							$scope.spotlight[0].description = $rootScope.truncateText($scope.spotlight[0].description);
						}
						if($scope.spotlight[1]){
							$scope.spotlight[1].description = $rootScope.truncateText($scope.spotlight[1].description);
						}
					}


				// Not a proshop
			} else {
				$rootScope.pageTitle = 'PrismaNote | ' + $scope.shop.name;
				$scope.getProducts({filter: {_id: {$in: productIds}}}, {reset: true})

				.catch(function(reason) {
					console.log(reason);
				});
			}
			if($scope.shop.address){
				var houseNumberWithSuffix = "";
				if($scope.shop.address.houseNumber != null){
					houseNumberWithSuffix = $scope.shop.address.houseNumber;

					if($scope.shop.address.houseNumberSuffix != null) {
						houseNumberWithSuffix += $scope.shop.address.houseNumberSuffix;
					}
				}

				$scope.googleMapsUrl = 'https://www.google.com/maps?q=' + window.encodeURIComponent($scope.shop.address.street + ' ' + houseNumberWithSuffix + ' ' + $scope.shop.address.city) + '&output=embed';
			}

		})
		.catch(function(reason) {
			console.log(reason);
		});



		$scope.searching = false;
		$scope.searchMode = false;

		$scope.searchProducts = function(search){
			$scope.hits = [];
			console.log('searchProducts', search);
			if(!search || search.length < 3){
				$scope.searching = false;
				return;
			}

			$scope.searching = true;
			$scope.searchMode = true;

			var params = {
				limit: 10,
				offset: 0,
				short: true,
				filter: {
					'name': {
						regex: {
							text: search,
							flags: 'i'
						}
					}
				}
			};

			console.log("PARAMS", params);

			$api.get('webshop-products/' + $scope.shop.nameSlug, params)
			.then(function(response) {
				$scope.hits = response.data.products;
				console.log("ht", $scope.hits);
				$scope.searching = false;
			})
			.catch(function(reason) {
				console.log(reason);
				$scope.searching = false;
			})
		}
		$scope.clearSearchHits = function(){
				//timeout for 100 ms to make the items clickable (otherwise the hits array will be emptied on click)
				$timeout(function(){
					$scope.hits = null;
					$scope.searchMode = false;
				}, 100)

			}

		}]);
