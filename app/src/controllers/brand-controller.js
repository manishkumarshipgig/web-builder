prismanoteApp.controller('brandController', ['$scope', '$rootScope', '$state', '$stateParams', '$api', '$translate', 
	function($scope, $rootScope, $state, $stateParams, $api, $translate) {

		$stateParams.brandNameSlug ? this.nameSlug = $stateParams.brandNameSlug : this.nameSlug = $stateParams.nameSlug

		var formatGender = function(male, female, kids) {
			if(kids == false) {
				if(male == true && female == true) {
					$translate('GENTS').then(function(gents) {
						$translate('LADIES').then(function(ladies) {
							return gents + ' / ' + ladies;
						});
					});
				} else if(male == true && female == false) {
					$translate('GENTS').then(function(gents) {
						return gents;
					});
				} else if(male == false && female == true) {
					$translate('LADIES').then(function(ladies) {
						return ladies;
					});
				}
			} else {
				if(male == true && female == true) {
					$translate('BOYS').then(function(boys) {
						$translate('GIRLS').then(function(girls) {
							return boys + ' / ' + girls;
						});
					});
				} else if(male == true && female == false) {
					$translate('BOYS').then(function(boys) {
						return boys;
					});
				} else if(male == false && female == true) {
					$translate('GIRLS').then(function(girls) {
						return girls;
					});
				}
			}
		};

		$api.get('brands/' + this.nameSlug)
		
			.then(function(response) {
				$scope.brand = response.data.brand;
				$rootScope.pageTitle = 'PrismaNote | ' + $scope.brand.name;
				$rootScope.breadcrumb = ['Merken', $scope.brand.name];
				$scope.brandProducts = new Array;

				if(!$state.includes('shop') && !$state.includes('proshop')) {

					$api.get('products', {'sort':{'views': 'desc'}, 'filter': {'brand.nameSlug': $scope.brand.nameSlug}, 'limit': 12})
					
						.then(function(response) {
							for(var i = 0; i < response.data.products.length; i++) {
								var product = response.data.products[i];
								$scope.brandProducts.push(product);
							}
						},
						$scope.shops = [
							{
								"name": "Juwelier Bos",
								"nameSlug": "juwelier-bos",
								"logoDark": "/images/demo-juwelier-logo-zwart.png",
								"price": 129.99,
								"distance": "11 km",
								"delivery": "1 werkdag",
								"bestBuy": true,
								"priceRating": 4,
								"distanceRating": 5,
								"deliveryRating": 3
							},
							{
								"name": "Juwelier Prisma",
								"nameSlug": "juwelier-demo",
								"logoDark": "/images/demo-juwelier-logo-zwart.png",
								"price": 119.99,
								"distance": "70 km",
								"delivery": "1 werkdag",
								"leastExpensive": true,
								"priceRating": 5,
								"distanceRating": 2,
								"deliveryRating": 3
							},
							{
								"name": "Horloge Magazijn",
								"nameSlug": "horloge-magazijn",
								"logoDark": "/images/demo-juwelier-logo-zwart.png",
								"price": 129.99,
								"distance": "36 km",
								"delivery": "1 werkdag",
								"alternative": true,
								"priceRating": 4,
								"distanceRating": 3,
								"deliveryRating": 3
							}
						]
						)

						
						.catch(function(reason) {
							console.log(reason);
						});

				}
			})
			
			.catch(function(reason) {
				console.log(reason);
			});

}]);
