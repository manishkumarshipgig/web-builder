prismanoteApp.controller('shopNewsController', ['$scope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload',
	function($scope, $api, $stateParams, $uibModal, $state, prompt, Upload) {

		$scope.getNewsItem = function() {
		$api.get('shop/news/',{newsItemSlug:  $stateParams.newsItemSlug, shopId: $scope.shop._id})
		
			.then(function(response) {
				$scope.newsItem = response.data.newsItem;
				console.log($scope.newsItem);
				// if($scope.newsItem.tasks.length > 0) {
				// 	$scope.activeTask = $scope.newsItem.tasks[0].name;
				// }
				// $scope.taskLanguage = $rootScope.language;
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

	$scope.getNewsItem();

}]);




