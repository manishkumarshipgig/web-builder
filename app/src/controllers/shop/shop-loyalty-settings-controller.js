prismanoteApp.controller('loyaltySettingsController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload', '$retailer',
    function ($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt, Upload, $retailer) {
      
        $scope.productStatus= $rootScope.user.productStatus;
        $scope.productKind= $rootScope.user.productKind;
        $scope.productMaterial= $rootScope.user.productMaterial;
        $scope.customTagss= $rootScope.user.customTagss;

        $scope.sentForRepair = function () {
            // New field
            $scope.customer.productType = 0;
            $scope.customer.productTypeId = 2;
            $scope.customer.dateOfBirth = undefined; 
            $scope.customer.address = $scope.user.address;
            $scope.customer.phone = $scope.user.phone;
            $scope.customer.firstName = $scope.user.firstName;
            $scope.customer.insertion = $scope.user.insertion;
            $scope.customer.lastName = $scope.user.lastName;
            $scope.customer.helpedBy = $scope.user.helpedBy;
            $scope.customer.emailOfficial = $scope.user.email;
            console.log("User:KV ",$scope.customer);
            $scope.postData();
        }
        $scope.addStatus = function(){
            if($scope.productStatus.length>=5){
                alert("Max number allowed=5, Please delete previous to add new");
                document.getElementById('repairSettingsStatus').value = "";
                return;
            }
            for (status in $scope.productStatus){
            if($scope.productStatus[status].toUpperCase()==$scope.repairSettings.status.toUpperCase()){
                alert("Tag Exists, try A different one");
                document.getElementById('repairSettingsStatus').value = "";
                return;
            }
            }
            $scope.productStatus.push($scope.repairSettings.status);
            console.log("PKP",$rootScope.user._id);
            console.log("fgsdgfsdfsdifids",$scope.productStatus);
            $scope.postDataStatus();
            document.getElementById('repairSettingsStatus').value = "";
        }
        $scope.addProductKind = function(){
            if($scope.productKind.length>=5){
                alert("Max number allowed=5, Please delete previous to add new");
                document.getElementById('repairSettingsProductKind').value = "";
                return;
            }
            for(productKind in $scope.productKind){
                // console.log(productKind);
                if($scope.productKind[productKind].toUpperCase()==$scope.repairSettings.productKind.toUpperCase()){
                    alert("Tag Exists, try A different one");
                    document.getElementById('repairSettingsProductKind').value = "";
                    return;
                }
                }
            $scope.productKind.push($scope.repairSettings.productKind);
            $scope.postDataProductKind();
            document.getElementById('repairSettingsProductKind').value = "";
        }
        $scope.addProductMaterial = function(){
            if($scope.productMaterial.length>=5){
                alert("Max number allowed=5, Please delete previous to add new");
                document.getElementById('repairSettingsProductMaterial').value = "";
                return;
            }
            for(productMaterial in $scope.productMaterial){
                if($scope.productMaterial[productMaterial].toUpperCase()==$scope.repairSettings.productMaterial.toUpperCase()){
                    alert("Tag Exists, try A different one");
                    document.getElementById('repairSettingsProductMaterial').value = "";
                    return;
                }
                }
            $scope.productMaterial.push($scope.repairSettings.productMaterial);
            $scope.postDataProductMaterial();
            document.getElementById('repairSettingsProductMaterial').value = "";
            
        }
        $scope.addCustomTags = function(){
            if($scope.customTagss.length>=5){
                alert("Max number allowed=5, Please delete previous to add new");
                document.getElementById('repairSettingsCustomTags').value = "";
                return;
            }
            for(customTagss in $scope.customTagss){
                if($scope.customTagss[customTagss].toUpperCase()==$scope.repairSettings.customTagss.toUpperCase()){
                    alert("Tag Exists, try A different one");
                    document.getElementById('repairSettingsCustomTags').value = "";
                    return;
                }
                }
            $scope.customTagss.push($scope.repairSettings.customTagss);
            $scope.postDataCustomTags();
            document.getElementById('repairSettingsCustomTags').value = "";
        }
        
        $scope.saveUserDetails = function () {
          
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/create-retailer-extradetails-modal.html',
				controller: 'loyaltyController',
				size: 'lg',
			});
			//console.log('user login data is:', $rootScope.user);
        }

        $scope.openAddress = function(index){
            $scope.resetSelected();
            $scope.addNewAddress = false;
            $scope.currentAddress = $scope.user.address[index];
            $scope.currentAddress.index = index;
        }
    
        $scope.newAddress = function() {
            //$scope.resetSelected();
            $scope.addNewAddress = true;
        }
    
        $scope.deleteStatus = function(index){
            prompt({
                title: 'Adres verwijderen?',
                message: 'Delete this status from the list?'
            }).then(function() {
                $scope.productStatus.splice(index, 1);
                $scope.postDataStatus();
            });
        }

        $scope.deleteProductKind = function(index){
            prompt({
                title: 'Adres verwijderen?',
                message: 'Delete this product kind from the list?'
            }).then(function() {
                $scope.productKind.splice(index, 1);
                $scope.postDataProductKind();
            });
        }

        $scope.deleteProductMaterial = function(index){
            prompt({
                title: 'Adres verwijderen?',
                message: 'Delete this product material from the list?'
            }).then(function() {
                $scope.productMaterial.splice(index, 1);
                $scope.postDataProductMaterial();
            });
        }

        $scope.deleteCustomTagss = function(index){
            prompt({
                title: 'Adres verwijderen?',
                message: 'Delete this custom tag from the list?'
            }).then(function() {
                $scope.customTagss.splice(index, 1);
                $scope.postDataCustomTags();
            });
        }
    
        $scope.pushAddress = function() {
            $scope.user.address.push($scope.currentAddress);
            $scope.addNewAddress = false;
        }
    
        $scope.cancelNewAddress = function() {
            $scope.addNewAddress = false;
            $scope.resetSelected();
        }
    
    
        $scope.openPhone = function(index){
                $scope.resetSelected();
                $scope.currentPhone = $scope.user.phone[index];
                $scope.currentPhone.index = index;
        }
    
        $scope.deletePhone = function(index){
                prompt({
                        title: 'Telefoonnummer verwijderen?',
                        message: 'Weet u zeker dat u dit telefoonnummer wilt verwijderen?'
                }).then(function() {
                        $scope.user.phone.splice(index, 1);
                        $scope.resetSelected();
                });
        }
    
        $scope.newPhone = function(index){
                //$scope.resetSelected();
                $scope.addNewPhone = true;
        }
    
        $scope.pushPhone = function() {
            $scope.user.phone.push($scope.currentPhone);
            $scope.addNewPhone = false;
        }
    
        $scope.cancelNewPhone = function() {
                $scope.addNewPhone = false;
                $scope.resetSelected();
        }

		$scope.sentWatchWarranty = function() {

            $scope.customer.productType = 1;
            $scope.customer.productTypeId = 1;
			$scope.customer.dateOfBirth=0;
			$scope.postData();
		}

		$scope.sentOtherPurchase = function(){
            $scope.customer.productType = 0;
            $scope.customer.productTypeId = 0;
			$scope.postData();
        }
        
        $scope.updateUser = function(){
            //$scope.user.userDetails.push($scope.user);
            console.log("DDahdasfd",$scope.user);
            if($scope.user.phone&&$scope.user.address){
                alert("Phone and Address added!!");
            } else {
                alert("Either the phone or the address is missing!!");
                return;
            }
		}

        // $scope.postDataStatus = function () {
        //     $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
        //         .then(function (response) {
        //             if ((!$scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit) || ($scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit)) {
        //                 if($rootScope.user.role == 'retailer'){
        //                     var message = 'Limiet overschreden door gebruiker. Bestel de CRM module via uw portaal om meer aankopen te registreren.'
        //                 }else{
        //                     var message = 'Deze email kan helaas niet verstuurd worden. Onze excuses voor het ongemak.'
        //                 }
        //                 $scope.alert = {
        //                     type: "danger",
        //                     msg: message
        //                 }
        //             } else {
        //                 console.log("PKP",$rootScope.user._id);
        //                 var data = {
        //                     id : $rootScope.user._id,
        //                     repairProductStatus: $scope.repairSettings.status,
        //                 }
        //                 $api.post('shop/loyalty-repair/settings/status', { data: data })

        //                     .then(function (response) {
        //                         console.log("response.data", response.data);

        //                     })

        //                     .catch(function (reason) {
        //                         console.log(reason);
        //                     });
        //             }

        //         })
        //         .catch(function (reason) {
        //             $scope.alert = {
        //                 type: "danger",
        //                 msg: reason
        //             }
        //             console.log(reason);
                    

        //         });
        // }



        $scope.postDataProductKind = function() {
            console.log("asdfasdasd",$scope.repairSettings);
            var data = {
                _id : $rootScope.user._id,
                productKind: $scope.productKind,
            }

            $api.post('shop/loyalty-repair/settings/productkind', { data: data })
			
				.then(function(response) {
					console.log("response",response);
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
        }
        
        $scope.postDataStatus = function() {
            console.log("asdfasdasd",$scope.repairSettings);
            var data = {
                _id : $rootScope.user._id,
                repairProductStatus: $scope.productStatus,
            }

            $api.post('shop/loyalty-repair/settings/status', { data: data })
			
				.then(function(response) {
					console.log("response",response);
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}


        $scope.postDataProductMaterial = function() {
            console.log("asdfasdasd",$scope.repairSettings);
            var data = {
                _id : $rootScope.user._id,
                repairProductStatus: $scope.productMaterial,
            }

            $api.post('shop/loyalty-repair/settings/productmaterial', { data: data })
			
				.then(function(response) {
					console.log("response",response);
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}


        $scope.postDataCustomTags = function() {
            console.log("asdfasdasd",$scope.repairSettings);
            var data = {
                _id :  $rootScope.user._id,
                repairCustomTagss: $scope.customTagss,
            }

            $api.post('shop/loyalty-repair/settings/customtags', { data: data })
			
				.then(function(response) {
					console.log("response",response);
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}



        // $scope.postDataProductMaterial = function () {
        //     $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
        //         .then(function (response) {
        //             if ((!$scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit) || ($scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit)) {
        //                 if($rootScope.user.role == 'retailer'){
        //                     var message = 'Limiet overschreden door gebruiker. Bestel de CRM module via uw portaal om meer aankopen te registreren.'
        //                 }else{
        //                     var message = 'Deze email kan helaas niet verstuurd worden. Onze excuses voor het ongemak.'
        //                 }
        //                 $scope.alert = {
        //                     type: "danger",
        //                     msg: message
        //                 }
        //             } else {
        //                 console.log("PKP", $rootScope.user._id);
        //                 var data = {
        //                     id : $rootScope.user._id,
        //                     repairProductStatus: $scope.repairSettings.productMaterial,
        //                 }
        //                 $api.post('shop/loyalty-repair/settings/productmaterial', { data: data })

        //                     .then(function (response) {
        //                         console.log("response.data", response.data);

        //                     })

        //                     .catch(function (reason) {
        //                         console.log(reason);
        //                     });
        //             }

        //         })
        //         .catch(function (reason) {
        //             $scope.alert = {
        //                 type: "danger",
        //                 msg: reason
        //             }
        //             console.log(reason);
                    

        //         });
        // }


        // $scope.postDataCustomTags = function () {
        //     $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
        //         .then(function (response) {
        //             if ((!$scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit) || ($scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit)) {
        //                 if($rootScope.user.role == 'retailer'){
        //                     var message = 'Limiet overschreden door gebruiker. Bestel de CRM module via uw portaal om meer aankopen te registreren.'
        //                 }else{
        //                     var message = 'Deze email kan helaas niet verstuurd worden. Onze excuses voor het ongemak.'
        //                 }
        //                 $scope.alert = {
        //                     type: "danger",
        //                     msg: message
        //                 }
        //             } else {
        //                 console.log("PKP", $rootScope.user._id);
        //                 var data = {
        //                     _id :  $rootScope.user._id,
        //                     repairProductStatus: $scope.repairSettings.customTagss,
        //                 }
        //                 $api.post('shop/loyalty-repair/settings/customtags', { data: data })

        //                     .then(function (response) {
        //                         console.log("response.data", response.data);

        //                     })

        //                     .catch(function (reason) {
        //                         console.log(reason);
        //                     });
        //             }

        //         })
        //         .catch(function (reason) {
        //             $scope.alert = {
        //                 type: "danger",
        //                 msg: reason
        //             }
        //             console.log(reason);
                    

        //         });
		// }

		$scope.closeAlert = function() {
			$scope.alert = 0
        }

        $retailer.getShop($stateParams.nameSlug)
            .then(function (shop) {
                $scope.currentShop = shop;

                $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
                    .then(function (response) {
                        console.log("response", response);
                        if ((!shop.proShop && parseInt(response.data.crmUsers) >= shop.userLimit) || (shop.proShop && parseInt(response.data.crmUsers) >= shop.userLimit)) {
                            $scope.alert = {
                                type: "danger",
                                msg: "Limiet overschreden. Het is momenteel niet mogelijk om uw garantie te registreren of uw aankoopbon naar uw emailadres te sturen. Onze excuses voor het ongemak. U kunt nog wel uw mening geven op onze Review pagina."
                            }
                            $scope.buttonDisabled = true
                        } else {
                            $scope.buttonDisabled = false
                        }
                    });

            })
            .catch(function (reason) {
                $scope.alert = {
                    type: "danger",
                    msg: reason
                }
            });
    
        
}])