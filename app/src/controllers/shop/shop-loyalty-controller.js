prismanoteApp.controller('loyaltyController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload', '$retailer','$location','$timeout',
    function ($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt, Upload, $retailer,$location,$timeout) {
        $scope.showFilters=true;
        $rootScope.newUser={};
        $rootScope.currentAddress ={};
        $rootScope.currentPhone ={};
        $scope.brands=[];
        $scope.getEachBrand={};
        $scope.crmUsersNames =[];
        $scope.crmUsersEmail =[];
        $scope.crmUsers={};
        $scope.customer={};
        $scope.currentShop={};
        // $scope.productStatus =['Taken in ','Send to repair company','Back in shop','Ready to collect',' Picked up'];
        $scope.productKind=['Watch','Jewelery','Other'];
        $scope.productMaterial =['Gold','Silver','Non-Noble'];
        $scope.customTagss=['10% discount'];
        $scope.productStatus = {
        "type": "select", 
        "name": "Status",
        "value": "Taken in", 
        "values": ['Taken in','Send to repair company','Back in shop','Ready to collect',' Picked up'] 
    };
        
       console.log("$rootScope.user",$rootScope.user);
        $scope.selectedBrand ='';
        // $scope.getBrandList();
        console.log("$scope.brands", $scope.verifiedBrands);
        // $scope.productKind= $rootScope.user.productKind;
        // $scope.productMaterial= $rootScope.user.productMaterial;
        // $scope.customTagss= $rootScope.user.customTagss;
        console.log("$scope.productKind",JSON.stringify($rootScope.user)+' '+ $rootScope.user.productKind);
        if($rootScope.user.productStatus!='' && $rootScope.user.productStatus!=[]){
            // $scope.productStatus= $rootScope.user.productStatus;
            $scope.hideProductStatus= false;
        } else {
            $scope.hideProductStatus= true;
        }
        if($rootScope.user.productKind!='' && $rootScope.user.productKind!=[]){
            // $scope.productKind= $rootScope.user.productKind;
            $scope.hideProductKind=false;
        } else {
            $scope.hideProductKind= true;
        }
        if($rootScope.user.productMaterial!='' && $rootScope.user.productMaterial!=[]){
            // $scope.productMaterial= $rootScope.user.productMaterial;
            $scope.hideProductMaterial= false;
        } else {
            $scope.hideProductMaterial= true;
        }
        if($rootScope.user.customTagss!='' && $rootScope.user.customTagss!=[]){
            // $scope.customTagss= $rootScope.user.customTagss;
            $scope.hideCustomTagss= false;
        } else {
            $scope.hideCustomTagss= true;
        }
        
        $scope.selectedProductStatus="Taken in";
        $scope.selectedProductKind=null;
        $scope.selectedProductMaterial=null;
        $scope.selectedCustomTagsss=null;
     

        console.log("...$scope.selectedProductStatus",$scope.selectedProductStatus);
        $scope.saveUserDetails = function () {
          
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/create-retailer-extradetails-modal.html',
				controller: 'loyaltyController',
				size: 'lg',
            });
            

            //console.log('user login data is:', $rootScope.user);
	
			modalInstance.result.then(function(result){
				if(result){
                    console.log(result);
					// $scope.user.socialPortal.campaigns.push(campaign);

					// $scope.saveSocialPortal();
					// $scope.getPortalCampaigns();

					// $scope.alert = {
					// 	type: 'success',
					// 	msg: "De promotie " + campaign.name + " is toegevoegd bij Mijn promoties. <a href='/retailer/campaigns/" + campaign.nameSlug + "'> Bekijk de campagne</a>"
					// }
				}
			}, function(){
				//dismissed
			})
		
        }
        $scope.getCurrentShop = function() {
            $retailer.getShop($stateParams.nameSlug)
            .then(function (shop) {
                console.log("shop123......",shop);
                $scope.currentShop = shop;
                console.log("   $scope.currentShop",   $scope.currentShop);

                $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
                    .then(function (response) {
                        console.log("response", response);
                        if ((!shop.proShop && parseInt(response.data.crmUsers) >= shop.userLimit) || (shop.proShop && parseInt(response.data.crmUsers) >= shop.userLimit)) {
                            $scope.alert = {
                                type: "danger",
                                msg: "Limiet overschreden. Het is momenteel niet mogelijk om uw garantie te registreren of uw aankoopbon naar uw emailadres te sturen. Onze excuses voor het ongemak. U kunt nog wel uw mening geven op onze Review pagina."
                            }
                            $scope.buttonDisabled = true
                        } else {
                            $scope.buttonDisabled = false
                        }
                    });

            })
            .catch(function (reason) {
                // $scope.alert = {
                //     type: "danger",
                //     msg: reason
                // }
                console.log("kv123",reason);
            });
        }

        $scope.settings = function () {
            $state.go("loyalty-repair-settings");
        }

        $scope.openAddress = function(index){
            $scope.resetSelected();
            $scope.addNewAddress = false;
            $scope.currentAddress = $scope.user.address[index];
            $scope.currentAddress.index = index;
        }
    
        $scope.newAddress = function() {
            //$scope.resetSelected();
            console.log("Super User: ",  $rootScope.user);
            console.log("selectedStatus: ",$scope.selectedStatus);
            console.log("selectedKind: ",$scope.selectedKind);
            console.log("selectedMaterial: ",$scope.selectedMaterial);
            console.log("selectedCustom: ",$scope.selectedCustom);
            $scope.addNewAddress = true;
        }
    
        $scope.deleteAddress = function(index){
            prompt({
                title: 'Adres verwijderen?',
                message: 'Weet u zeker dat u dit adres wilt verwijderen?'
            }).then(function() {
                $scope.user.address.splice(index, 1);
                $scope.resetSelected();
            });
        }
    
        $scope.pushAddress = function() {
            $scope.user.address.push($scope.currentAddress);
            $scope.addNewAddress = false;
        }
    
        $scope.cancelNewAddress = function() {
            $scope.addNewAddress = false;
            $scope.resetSelected();
        }
    
    
        $scope.openPhone = function(index){
                $scope.resetSelected();
                $scope.currentPhone = $scope.user.phone[index];
                $scope.currentPhone.index = index;
        }
    
        $scope.deletePhone = function(index){
                prompt({
                        title: 'Telefoonnummer verwijderen?',
                        message: 'Weet u zeker dat u dit telefoonnummer wilt verwijderen?'
                }).then(function() {
                        $scope.user.phone.splice(index, 1);
                        $scope.resetSelected();
                });
        }
    
        $scope.newPhone = function(index){
                //$scope.resetSelected();
                $scope.addNewPhone = true;
        }
    
        $scope.pushPhone = function() {
            $scope.user.phone.push($scope.currentPhone);
            $scope.addNewPhone = false;
        }
    
        $scope.cancelNewPhone = function() {
                $scope.addNewPhone = false;
                $scope.resetSelected();
        }

		$scope.sentWatchWarranty = function() {

            $scope.customer.productType = 1;
            $scope.customer.productTypeId = 1;
            $scope.customer.dateOfBirth=0;
      
			$scope.postDataWarranty();
		}

		$scope.sentOtherPurchase = function(){
            $scope.customer.productType = 0;
            $scope.customer.productTypeId = 0;
			$scope.postData();
        }
        
        $scope.updateUser = function(){
            //$scope.user.userDetails.push($scope.user);
            console.log("DDahdasfd",$scope.user);
            if($scope.user.phone&&$scope.user.address){
               
                $scope.alert = {
                    type: "success",
                    msg: "Phone and Address added!! " 
                }
            } else {
             
                $scope.alert = {
                    type: "danger",
                    msg: "Either the phone or the address is missing!! " 
                }
                return;
            }
        }
        
        $scope.sentForRepair = function () {
            // New field
            console.log("$scope.newUser.dateOfBirth",$rootScope.newUser);
            $scope.customer.productType = 0;
            $scope.customer.productTypeId = 2;
            $scope.customer.dateOfBirth = $rootScope.newUser.dateOfBirth; 
            $scope.customer.address =$rootScope.currentAddress;
            $scope.customer.phone = $rootScope.currentPhone;
            $scope.customer.firstName = $rootScope.newUser.firstName;
            $scope.customer.insertion = $rootScope.newUser.insertion;
            $scope.customer.lastName = $rootScope.newUser.lastName;
            $scope.customer.helpedBy = $rootScope.user.firstName;
            $scope.customer.emailOfficial = $rootScope.newUser.email;
            console.log("User:KV ",$scope.customer);
            // if($scope.selectedProductStatus==null){
               
            //     $scope.alert = {
            //         type: "danger",
            //         msg: "Please select all the fields!! " 
            //     }
            //     return;
            // } else {

                $scope.customer.productStatus = $scope.selectedProductStatus;
                $scope.customer.productKind = $scope.selectedProductKind;
                $scope.customer.productMaterial = $scope.selectedProductMaterial;
                $scope.customer.customTags = $scope.selectedCustomTagsss;
                $scope.customer.brandName = $scope.selectedBrand.title;

           // }
            $scope.postData();
        }

        $scope.postData = function () {
            if($scope.tempEmail!=$scope.customer.email&&$scope.tempEmail!= undefined){
                $scope.updateCrmUser();
                   
            }else{
            $retailer.getShop($stateParams.nameSlug)
            .then(function (shop) {
                console.log("shop......",shop);
                $scope.currentShop = shop;
            $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
                .then(function (response) {
                    if ((!$scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit) || ($scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit)) {
                        if($rootScope.user.role == 'retailer'){
                            var message = 'Limiet overschreden door gebruiker. Bestel de CRM module via uw portaal om meer aankopen te registreren.'
                        }else{
                            var message = 'Deze email kan helaas niet verstuurd worden. Onze excuses voor het ongemak.'
                        }
                        $scope.alert = {
                            type: "danger",
                            msg: message
                        }
                    } else {
                        $scope.customer.fillinLanguage = $rootScope.language;
                        var data = {
                            customer: $scope.customer,
                            shopSlug: $stateParams.nameSlug,
                            fileName: $scope.customer.photo.name
                        }
                        console.log("$scope.customer",$scope.customer);
                        console.log("$scope.shopSlug",$stateParams.nameSlug);
                        console.log("fileName",$scope.customer);

                        // $api.get('crmuseremail/' +$scope.customer.email)
                    
                        // .then(function(response) {
                        //     console.log("khatarnaak", response.data.crmUser);
                        //     $scope.crmUser = response.data.crmUser;

                        //     console.log("$scope.crmUser",$scope.crmUser);
                        
                       

                        $api.post('shop/loyalty-watches/', { data: data })

                            .then(function (response) {
                                console.log("response.data loyalty-watches", response.data);
                                $scope.upload = true;
                                console.log("response.data.fileName", response.data.fileName);
                                Upload.upload({
                                    url: 'api/user/loyalty-photo-upload',
                                    data: {
                                        fileName: response.data.fileName,
                                        crmUserId: response.data.user._id,
                                        file: $scope.customer.photo //file input field
                                    }
                                })
                                    .then(function (res) {
                                        //Check if this must be send to Countr

                                        $scope.customer = null;
                                        $scope.alert = {
                                            type: "success",
                                            msg: "Uw gegevens zijn verzonden!"
                                        }




                                    }, function (res) {
                                        //wanneer uploaden mislukt
                                        console.log("Error: ", res.status);
                                        $scope.alert = {
                                            type: "danger",
                                            msg: "upload error: " + res.status
                                        }
                                    }, function (evt) {
                                        //tijdens upload
                                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                        $scope.uploadProgress = progressPercentage;
                                        console.log("Progress: " + progressPercentage + '%' + evt.config.data);
                                    });

                                $scope.upload = false;

                            })

                            .catch(function (reason) {
                                console.log(reason);
                                if (reason.indexOf('email_1 dup key') > -1) {
                                    $scope.alert = {
                                        type: "danger",
                                        msg: "Gebruiker bestaat al met dit e-mailadres. Gebruik een ander e-mailadres."
                                    }
                                } else {
                                    $scope.alert = {
                                        type: "danger",
                                        msg: "Het lukte niet om uw gegevens op te slaan " + $rootScope.userreason
                                    }
                                }
                                $scope.upload = false;
                            });
                                 //$scope.getSocialPortal();
                        // })
                        
                        // .catch(function(reason) {
                        //     $scope.alert = {
                        //         type: "danger",
                        //         msg: "Please enter the email address you filled at the registration time"
                        //     }
                        //     console.log(reason);
                        // });
                    }

                })
                .catch(function (reason) {
                    $scope.alert = {
                        type: "danger",
                        msg: reason
                    }
                    console.log(reason);
                    $scope.loading = false;

                });
            });
        }
        }
        $scope.postDataWarranty= function () {
            $retailer.getShop($stateParams.nameSlug)
            .then(function (shop) {
                console.log("shop......",shop);
                $scope.currentShop = shop;
            $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
                .then(function (response) {
                    console.log("$scope.currentShop",$scope.currentShop);
                    if ((!$scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit) || ($scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit)) {
                        if($rootScope.user.role == 'retailer'){
                            var message = 'Limiet overschreden door gebruiker. Bestel de CRM module via uw portaal om meer aankopen te registreren.'
                        }else{
                            var message = 'Deze email kan helaas niet verstuurd worden. Onze excuses voor het ongemak.'
                        }
                        $scope.alert = {
                            type: "danger",
                            msg: message
                        }
                    } else {
                        console.log("$rootScope.language",$rootScope.language);
                        console.log("$scope.customer",   $scope.customer);
                        $scope.customer.fillinLanguage = $rootScope.language;
                        var data = {
                            customer: $scope.customer,
                            shopSlug: $stateParams.nameSlug,
                            fileName: $scope.customer.photo.name
                        }
                        console.log("$scope.customer",$scope.customer);
                        console.log("$scope.shopSlug",$stateParams.nameSlug);
                        console.log("fileName",$scope.customer);
                        // if(($scope.customer.productType ==0)&&($scope.customer.productTypeId == 2)){

                        $api.get('crmuseremail/' +$scope.customer.emailName)
                    
                        .then(function(response) {
                            console.log("khatarnaak", response.data.crmUser);
                            $scope.crmUser = response.data.crmUser;

                        //     console.log("$scope.crmUser",$scope.crmUser);
                        
                       

                        $api.post('shop/loyalty-watches/', { data: data })

                            .then(function (response) {
                                console.log("response.data loyalty-watches", response.data);
                                $scope.upload = true;
                                console.log("response.data.fileName", response.data.fileName);
                                Upload.upload({
                                    url: 'api/user/loyalty-photo-upload',
                                    data: {
                                        fileName: response.data.fileName,
                                        crmUserId: $scope.crmUser._id,
                                        file: $scope.customer.photo //file input field
                                    }
                                })
                                    .then(function (res) {
                                        //wanneer uploaden gelukt
                                        $scope.customer = null;
                                        $scope.alert = {
                                            type: "success",
                                            msg: "You succesfully e-mailed the guarantee card!"
                                        }
                                        $timeout( function(){
                                            $location.path('/home');
                                        }, 4000 );
                                        
                                    }, function (res) {
                                        
                                        //wanneer uploaden mislukt
                                        console.log("Error: ", res.status);
                                        $scope.alert = {
                                            type: "danger",
                                            msg: "upload error: " + res.status
                                        }
                                    }, function (evt) {
                                        //tijdens upload
                                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                        $scope.uploadProgress = progressPercentage;
                                        console.log("Progress: " + progressPercentage + '%' + evt.config.data);
                                        if( $scope.uploadProgress>=100){
                                            $scope.upload = false;
                                        }
                                    });

                                $scope.upload = false;
                                  //alert("You have successfully uploaded the gurantee Card")
                                  $scope.alert = {
                                    type: "success",
                                    msg: "You have successfully uploaded the gurantee Card."
                                }

                            })

                            .catch(function (reason) {
                                console.log(reason);
                                if (reason.indexOf('email_1 dup key') > -1) {
                                    $scope.alert = {
                                        type: "danger",
                                        msg: "Gebruiker bestaat al met dit e-mailadres. Gebruik een ander e-mailadres."
                                    }
                                } else {
                                    $scope.alert = {
                                        type: "danger",
                                        msg: "Het lukte niet om uw gegevens op te slaan " + $rootScope.userreason
                                    }
                                }
                                $scope.upload = false;
                            });
                                 //$scope.getSocialPortal();
                       })
                    
                        
                        .catch(function(reason) {
                            console.log(reason);
                        });


                   // }
                }

                })
                .catch(function (reason) {
                    $scope.alert = {
                        type: "danger",
                        msg: reason
                    }
                    console.log(reason);
                    $scope.loading = false;

                });
            });
        }
        $scope.updateCrmUser = function () {
            console.log("if condition running...");
            $api.get('crmuseremail/' +$scope.tempEmail)
                    
            .then(function(response) {
                console.log("khatarnaak", response.data.crmUser);
                var crmUser = response.data.crmUser;
                console.log("crmUser.email1",crmUser.email);
                crmUser.email = $scope.customer.email;
                console.log("crmUser.email2",crmUser.email);
            
            $api.put('crmusers',{crmUser:crmUser})
            
                .then(function(response){
                 console.log(response);
                }) .catch(function(reason) {
                        $scope.alert = {
                            type: "danger",
                            msg: "Please enter the email address you filled at the registration time"
                        }
                        console.log(reason);
                    });
                })
                $retailer.getShop($stateParams.nameSlug)
                .then(function (shop) {
                    console.log("shop......",shop);
                    $scope.currentShop = shop;
                $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
                    .then(function (response) {
                        if ((!$scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit) || ($scope.currentShop.proShop && parseInt(response.data.crmUsers) >= $scope.currentShop.userLimit)) {
                            if($rootScope.user.role == 'retailer'){
                                var message = 'Limiet overschreden door gebruiker. Bestel de CRM module via uw portaal om meer aankopen te registreren.'
                            }else{
                                var message = 'Deze email kan helaas niet verstuurd worden. Onze excuses voor het ongemak.'
                            }
                            $scope.alert = {
                                type: "danger",
                                msg: message
                            }
                        } else {
                            $scope.customer.fillinLanguage = $rootScope.language;
                            var data = {
                                customer: $scope.customer,
                                shopSlug: $stateParams.nameSlug,
                                fileName: $scope.customer.photo.name
                            }
                            console.log("$scope.customer",$scope.customer);
                            console.log("$scope.shopSlug",$stateParams.nameSlug);
                            console.log("fileName",$scope.customer);
    
                            $api.get('crmuseremail/' +$scope.customer.email)
                        
                            .then(function(response) {
                                console.log("khatarnaak", response.data.crmUser);
                                $scope.crmUser = response.data.crmUser;
    
                                console.log("$scope.crmUser",$scope.crmUser);
                            
                           
    
                            $api.post('shop/loyalty-watches/', { data: data })
    
                                .then(function (response) {
                                    console.log("response.data loyalty-watches", response.data);
                                    $scope.upload = true;
                                    console.log("response.data.fileName", response.data.fileName);
                                    Upload.upload({
                                        url: 'api/user/loyalty-photo-upload',
                                        data: {
                                            fileName: response.data.fileName,
                                            crmUserId: $scope.crmUser._id,
                                            file: $scope.customer.photo //file input field
                                        }
                                    })
                                        .then(function (res) {
                                            //wanneer uploaden gelukt
                                            $scope.customer = null;
                                            $scope.alert = {
                                                type: "success",
                                                msg: "Uw gegevens zijn verzonden!"
                                            }
                                        }, function (res) {
                                            //wanneer uploaden mislukt
                                            console.log("Error: ", res.status);
                                            $scope.alert = {
                                                type: "danger",
                                                msg: "upload error: " + res.status
                                            }
                                        }, function (evt) {
                                            //tijdens upload
                                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                            $scope.uploadProgress = progressPercentage;
                                            console.log("Progress: " + progressPercentage + '%' + evt.config.data);
                                        });
    
                                    $scope.upload = false;
    
                                })
    
                                .catch(function (reason) {
                                    console.log(reason);
                                    if (reason.indexOf('email_1 dup key') > -1) {
                                        $scope.alert = {
                                            type: "danger",
                                            msg: "Gebruiker bestaat al met dit e-mailadres. Gebruik een ander e-mailadres."
                                        }
                                    } else {
                                        $scope.alert = {
                                            type: "danger",
                                            msg: "Het lukte niet om uw gegevens op te slaan " + $rootScope.userreason
                                        }
                                    }
                                    $scope.upload = false;
                                });
                                     //$scope.getSocialPortal();
                            })
                            
                            .catch(function(reason) {
                                $scope.alert = {
                                    type: "danger",
                                    msg: "Please enter the email address you filled at the registration time"
                                }
                                console.log(reason);
                            });
                        }
    
                    })
                    .catch(function (reason) {
                        $scope.alert = {
                            type: "danger",
                            msg: reason
                        }
                        console.log(reason);
                        $scope.loading = false;
    
                    });
                });
            


        }

        //$scope.postData();

		$scope.closeAlert = function() {
			$scope.alert = 0
        }

        $retailer.getShop($stateParams.nameSlug)
            .then(function (shop) {
                $scope.currentShop = shop;

                $api.get('crmuserscount', { filter: { 'shopSlug': $stateParams.nameSlug } })
                    .then(function (response) {
                        console.log("response", response);
                        if ((!shop.proShop && parseInt(response.data.crmUsers) >= shop.userLimit) || (shop.proShop && parseInt(response.data.crmUsers) >= shop.userLimit)) {
                            $scope.alert = {
                                type: "danger",
                                msg: "Limiet overschreden. Het is momenteel niet mogelijk om uw garantie te registreren of uw aankoopbon naar uw emailadres te sturen. Onze excuses voor het ongemak. U kunt nog wel uw mening geven op onze Review pagina."
                            }
                            $scope.buttonDisabled = true
                        } else {
                            $scope.buttonDisabled = false
                        }
                    });

            })
            .catch(function (reason) {
                $scope.alert = {
                    type: "danger",
                    msg: reason
                }
            });

            $scope.getBrandList = function() {
                $scope.brandsList=[];
                $api.get('brands',{filter: {isVerified:true}, limit: 0})
                    .then(function(response) {
                        console.log("response",response);
                        // $scope.loading = false;

                        $scope.verifiedBrands = response.data.brands;
                        for(var i=0;i<$scope.verifiedBrands.length;i++){
                           $scope.brands.push( $scope.verifiedBrands[i].name);
                        }
                       
                        console.log("$scope.brands",$scope.brands);
                        for(var j=0;j<$scope.brands.length;j++){
                            $scope.getEachBrand={};
                            $scope.getEachBrand.name=$scope.brands[j];
                            $scope.brandsList.push( $scope.getEachBrand);
                            // console.log("$scope.sdd",$scope.sdd);
                            
                           
                         }
                         console.log("$scope.brandsd",$scope.brandsList);
                    })
                    .catch(function(reason) {
                        console.log(reason);
                    });
        }

        $scope.customerSelected = function(customer){
            if (typeof $scope.customer == "undefined" || !$scope.customer || $scope.customer == ''){
                $scope.customer = {};
            }
            console.log("customer", $scope.customer, typeof $scope.customer)
            $scope.tempEmail = customer.description;
            $scope.customer.email = customer.description;

        }

        $scope.searchCrmUsers = function() {
            $scope.crmUsersDetails =[];
            $api.get('crmusers')
                .then(function(response) {
                    $scope.crmUsers = response.data.crmUsers;
                    console.log(" $scope.crmUsers", $scope.crmUsers);
                    for(var i=0;i< $scope.crmUsers.length;i++){
                        $scope.crmUsersNames.push($scope.crmUsers[i].emailName);
                        $scope.crmUsersEmail.push($scope.crmUsers[i].email);
                     }
                    
                     console.log(" $scope.crmUsersNames",$scope.crmUsersNames);
                     console.log(" $scope.crmUsersEmail",$scope.crmUsersEmail);
                   
                     for(var j=0;j<$scope.crmUsersNames.length;j++){
                         $scope.getEachCrmUser={};
                         $scope.getEachCrmUser.name= $scope.crmUsersNames[j];
                         $scope.getEachCrmUser.email=$scope.crmUsersEmail[j];
                         $scope.crmUsersDetails.push( $scope.getEachCrmUser);
                         // console.log("$scope.sdd",$scope.sdd);
                         
                        
                      }
                      console.log("$scope.crmUsersDetails",$scope.crmUsersDetails);
                 console.log("response $scope.searchCrmUsers",response);
                   
                })
                .catch(function(reason) {
                    console.log(reason);
                });
            
    }
    $scope.searchCrmUsers();

    $scope.findCustomer= function(email) {

       console.log("email",email);
    }
    $scope.focusOut= function() { 
       console.log("crmUsersDetails",$scope.crmUsersDetails);

    }

}])