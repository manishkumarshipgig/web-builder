prismanoteApp.controller('shopReviewController', ['$scope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload',
	function($scope, $api, $stateParams, $uibModal, $state, prompt, Upload) {

	// Default options for fetching new items: 12 items with featured items first, sorted by views.
	var defaultApiOptions = {'sort': {'startDate': 'desc'}, 'limit': 12};

	// currently active API request params/options. Set to default options initially, may be changed or reset to default options later.
	var apiOptions = defaultApiOptions;

	// General setter to change the Api HTTP request options/params. Some wrapper functions are available to make the code more intuitive.
	var setApiOptions = function(options, type, append) {
		if(typeof options === 'object') {
			if(typeof append !== 'boolean' || append === false) {
				apiOptions[type] = options;
			} else {
				apiOptions[type] = _.extend(apiOptions[type], options);
			}
		} else if(options == null) {
			apiOptions = options;
		}
		$scope.products = new Array;
		$scope.getProducts();
	};

	$scope.reviews = new Array;

	$scope.getReviewItems = function() {
		// var numberOfReviewPosts = $scope.reviews.length;
		// apiOptions.offset = numberOfReviewPosts;

		// $api.get('shop/reviews', apiOptions)
		
		// 	.then(function(response) {
		// 		// Add reviewPosts to the $scope.
		// 		for(var i = 0; i < response.data.reviewsItems.length; i++) {
		// 			var reviewPost = response.data.reviewsItems[i];
		// 			//console.log(reviewPost);
		// 			//if(reviewPost.update == true) {
		// 			$scope.reviews.push(reviewPost);
		// 			//} else {
		// 			//	console.log("er ging iets niet goed met de aanvraag naar de backend");
		// 			//}
		// 		}
		// 	})
			
		// 	.catch(function(reason) {
		// 		console.log(reason);
		// 	})
	
		console.log("reviews", $scope.shop	);

	};

	$scope.addReview = function(review) {
		//console.log("score", review.score, typeof(review.score));
		//review.content = review.textarea;
		review.author = {
			name: review.name
		};
		//review.rating = review.score;
		review.creationDate = new Date();
		
		$api.post('shop/reviews', {review:$scope.review, shopId:$scope.shop._id})
		.then(function(response){
			$scope.shop.reviews.push(review);
		})
		.catch(function(reason){
			console.log("FOUT!", reason);
		})

		$scope.succes = 'showme=true'

	}

	$scope.addReviewItem = function(){
		var data = {
			reviewItem: $scope.reviewItem,
		}

		console.log("voor de data");
		console.log("data", data);
		$api.post('shop/reviews', {data:data})

			.then(function(response) {
				//wanneer opslana gelukt
				console.log("RESPONSE!", response);

				console.log("komt de controller hier? Nee? Dan krijgt hij geen response");
				$scope.upload = true;
					Upload.upload({
							url: 'api/reviews/reviews-photo-upload',
							data: {
									reviewItemId: response.data.reviewItem._id,
									file: $scope.reviewItem.photo //file input field
							}
					})
					console.log(response.data.reviewItem._id)
					.then(function (res){
								//wanneer uploaden gelukt
								console.log("uploaden gelukt")
								$scope.reviewItem = null;
								$scope.alert = {
									type: "success",
									msg: "Uw gegevens zijn verzonden!"
								}
					}, function (res){
						//wanneer uploaden mislukt
							console.log("Error: ", res.status);
							$scope.alert = {
									type: "danger",
									msg: "upload error: " + res.status
								}
					}, function(evt){
							//tijdens upload
							var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
							$scope.uploadProgress = progressPercentage;
							console.log("Progress: " + progressPercentage + '%' + evt.config.data);
					});
					$scope.upload = false;
				
			})

			.catch(function(reason) {
				console.log(reason);
				$scope.alert = {
					type: "danger",
					msg: "Het lukte niet om uw gegevens op te slaan " + reason
				}
				$scope.upload = false;
			});
	}

	$scope.closeAlert = function() {
		$scope.alert = 0
	}


	var getItems = function(type){
		$api.get(type)
		
			.then(function(response) {
				$scope[type] = response.data[type];
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

// merken
	$scope.getBrands = function() {
		console.log("Deze doet het")
		return getItems('brands');
	};

	$scope.getWholesalers = function() {
		return getItems('wholesalers');
	};

	var searchItems = function(str, type) {
		var matches = [];
	
		$scope[type].forEach(function(item) {
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || 
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

		return matches;
	};

	$scope.searchBrands = function(str) {
		return searchItems(str, 'brands');
	}

	$scope.searchWholesalers = function(str) {
		return searchItems(str, 'wholesalers');
	}

	$scope.brandSelected = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.reviewItem.brand = brand;
	};

	$scope.wholesalerSelected = function(selected){
		var wholesaler = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.reviewItem.wholesaler = wholesaler;
	};


}]);




