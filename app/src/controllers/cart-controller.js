prismanoteApp.controller("cartController", ['$scope', '$rootScope', '$state', '$cart', '$uibModal', 'prompt',  function($scope, $rootScope, $state, $cart, $uibModal, prompt) {
	
	$scope.isDesktop = function(){
		return $rootScope.userDeviceType == 'desktop';
	}

	var openAddToCartModal = function(item) {
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/add-to-cart-modal.html',
				controller: 'addToCartModalController',
				resolve: {
					item: function() {
						return item;
					}
				}
			});
			modalInstance.result.then(function() {
				if($state.includes('shop') || $state.includes('proshop')) {
					$state.go($rootScope.getShopType() + '.cart');
				} else {
					$state.go('layout.cart');
				}
			}, function() {
				// Modal is dismissed
			})
		};

		var openChooseShopModal = function(item) {
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/choose-shop-modal.html',
				controller: 'chooseShopModalController',
				resolve: {
					item: function() {
						return item;
					}
				}
			});
			// The callback uses the chooseShopModal result param item instead of the initial item param, because it also includes shop and price.
			modalInstance.result
			
				.then(function(item) {
					$cart.add(item, 1, $rootScope.cart)
				
						.then(function(cart) {
							$rootScope.cart = cart;
							getTotal(cart);
							openAddToCartModal(item);
							getShops(cart);
						})
						
						.catch(function(reason) {
							console.log(reason);
						});

			}, function() {
				//Modal is dismissed
			})
		};
	
	// getTotal doesnt have to be accessible from the $rootScope because it is called when necessary and updates $rootScope.total, which should always be up-to-date this way.
	var getTotal = function(cart) {

		$cart.getTotal(cart, false)
			
			.then(function(total) {
				$rootScope.total = total;
			})

			.catch(function(reason) {
				console.log(reason);
			});
	}


	var getShops = function(cart) {
		$cart.getShops(cart)

			.then(function(shops) {
				$rootScope.shops = shops;
			})

			.catch(function(reason) {
				console.log(reason);
			});
	}

	var getCart = function(force) {

		$cart.get(force)
			
			.then(function(cart) {
				$rootScope.cart = cart;
				$rootScope.total = getTotal(cart);

				// Also update the shops if the cart is fetched from the server.
				$cart.getShops(cart)

					.then(function(shops) {
						$rootScope.shops = shops;
					})

					.catch(function(reason) {
						console.log(reason);
					});
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	}

	$scope.addToCart = function(item) {
		if($state.includes('shop') || $state.includes('proshop')) {

			$cart.add(item, 1, $rootScope.cart)
			
				.then(function(cart) {

					$rootScope.cart = cart;
					getTotal(cart);
					openAddToCartModal(item);
					getShops(cart);
				
				})
				
				.catch(function(reason) {
					console.log(reason);
				});

		} else {

				openChooseShopModal(item);

		}
	}

	$scope.removeFromCart = function(index) {
		$cart.remove(index, $rootScope.cart)
			
			.then(function(cart) {
				$rootScope.cart = cart;
				getShops(cart);
				getTotal(cart);
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	}


	$scope.emptyCart = function(){

		prompt({
			title: 'Winkelmandje legen',
			message: 'Weet u zeker dat u uw winkelmandje wilt legen?'
		}).then(function() {
			$cart.empty()
				.then(function(cart) {
					$rootScope.cart = cart;
				})
		});
	}

	getCart(true);

	$scope.changeQuantity = function(mode, index){
		if(mode){
			$rootScope.cart[index].quantity++;
			$rootScope.cart[index].tax = ($rootScope.cart[index].price * $rootScope.cart[index].quantity) * 0.21;
		}else{
			$rootScope.cart[index].quantity--;
			$rootScope.cart[index].tax = ($rootScope.cart[index].price * $rootScope.cart[index].quantity) * 0.21;
			if($rootScope.cart[index].quantity < 1){
				$scope.removeFromCart(index);
				return;
			}
		}

		getShops($rootScope.cart);
		getTotal($rootScope.cart);

	}

	$scope.increaseQuantity = function(index) {
        $cart.increaseQuantity(index, 1, $rootScope.cart)
        
            .then(function(cart) {
                $rootScope.cart = cart;
                getShops(cart);
                getTotal(cart);
            })
            
            .catch(function(reason) {
                console.log(reason);
            });
    }

    $scope.decreaseQuantity = function(index) {
        $cart.decreaseQuantity(index, 1, $rootScope.cart)
        
            .then(function(cart) {
                $rootScope.cart = cart;
                getShops(cart);
                getTotal(cart);
            })
            
            .catch(function(reason) {
                console.log(reason);
            });
    }


	
}]);