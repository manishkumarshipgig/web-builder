prismanoteApp.controller('productController', ['$scope', '$rootScope', '$state', '$stateParams', '$api', '$cart', function($scope, $rootScope, $state, $stateParams, $api, $cart) {

	$stateParams.productNameSlug ? this.nameSlug = $stateParams.productNameSlug : this.nameSlug = $stateParams.nameSlug;

	$scope.range = function(num) {
		var range = []
		for(var i = 0; i < num; i++) {
			range.push(i);
		}
		return range;
	}


	$api.get('products/' + this.nameSlug)
		
		.then(function(response) {

			$api.get('shops/' + $stateParams.nameSlug)
				.then(function(shop) {
					$scope.shop = shop.data.shop;

					$scope.product = response.data.product;
					$scope.product.shop = shop.data.shop;

					if($state.includes('shop') || $state.includes('proshop')) {
						$scope.active=1;

						function searchProduct(product){
							return product._id._id === $scope.product._id;
						}

						var shopProduct = $scope.shop.products.find(searchProduct);

						$scope.product.price = shopProduct.price;
						$scope.product.stock = shopProduct.stock;

					} else {
						$scope.active=0;
					}
					$rootScope.pageTitle = 'PrismaNote | ' + $scope.product.name;
					$rootScope.breadcrumb = ['Producten', $scope.product.name];

					// Temporary solution for displaying the product details nicely.
					// TODO: Generate these details automatically based on other properties in the db.
					if($scope.product[$rootScope.language].shortDescription && $scope.product[$rootScope.language].longDescription) {
						$scope.product[$rootScope.language].shortDescription = $scope.product[$rootScope.language].shortDescription
							.split("\n").slice(2).join("\n");
						$scope.product[$rootScope.language].shortDescription = $scope.product[$rootScope.language].shortDescription
							.replace("Algemeen","<strong>Algemeen</strong>")
							.replace("Voordelen", "<strong>Voordelen</strong>");
						$scope.product[$rootScope.language].longDescription = $scope.product[$rootScope.language].longDescription
							.replace("Details", "")
							.replace("Kast", "<strong>Kast</strong>")
							.replace("Wijzerplaat", "<strong>Wijzerplaat</strong>")
							.replace("Meters","<strong>Meters</strong>")
							.replace("Ketting", "<strong>Ketting</strong>")
							.replace("Band","<strong>Band</strong>")
							.replace("Uurwerk", "<strong>Uurwerk</strong>")
							.replace("Overig", "<strong>Overig</strong>");
					}

					$api.put('products/' + $scope.product.nameSlug, {$inc: {views: 1}})
					
					.catch(function(reason) {
						console.log(reason);
					});
				})
			.catch(function(reason) {
				//
				console.error("error getting shop", reason);
			})			
		})
		
	.catch(function(reason) {
		console.error("error getting product", reason);
	});
}]);
