prismanoteApp.controller('homeController', ['$scope', '$rootScope', '$api','$uibModal', '$translate', '$state',
function($scope, $rootScope, $api, $uibModal, $translate, $state) {

		if($rootScope.user){
			if($rootScope.user.role == 'retailer'){
				$state.go('retailer.home');
			}else if($rootScope.user.role == 'wholesaler'){
				$state.go('brand.home');
			}
		}

		$scope.softwareSlider = [
			{
				src: 'https://prismanote.s3.amazonaws.com/general/home.PNG',
				alt: 'home'
			},
			{
				src: 'https://prismanote.s3.amazonaws.com/general/kassa.PNG',
				alt: 'kassa'
			},
			{
				src: 'https://prismanote.s3.amazonaws.com/general/voorraadbeheer.PNG',
				alt: 'voorraadbeheer'
			},
			{
				src: 'https://prismanote.s3.amazonaws.com/general/artikeloverzicht.PNG',
				alt: 'artikeloverzicht'
			},
			{
				src: 'https://prismanote.s3.amazonaws.com/general/orderoverzicht.PNG',
				alt: 'orderoverzicht'
			},
			{
				src: 'https://prismanote.s3.amazonaws.com/general/reparatieoverzicht.PNG',
				alt: 'reparatieoverzicht'
			},
			{
				src: 'https://prismanote.s3.amazonaws.com/general/medewerkers.PNG',
				alt: 'medewerkers'
			},
			{
				src: 'https://prismanote.s3.amazonaws.com/general/Leveranciers.PNG',
				alt: 'leveranciers'
			},
		]
		$scope.getCampaigns = function(){
			$api.get('campaigns')
			.then(function(response) {
				$scope.campaigns = response.data.campaigns;
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
		}
		
		$scope.openPreviewCampaignModal = function(campaign){
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/preview-campaign-modal.html',
				controller: 'previewCampaignModalController',
				size: 'lg',
				resolve: {
					campaign: function() {
						return campaign;
					}
				}
			});
	
			modalInstance.result.then(function(result){
				if(result){
					$scope.user.socialPortal.campaigns.push(campaign);

					$scope.saveSocialPortal();
					$scope.getPortalCampaigns();

					$scope.alert = {
						type: 'success',
						msg: "De promotie " + campaign.name + " is toegevoegd bij Mijn promoties. <a href='/retailer/campaigns/" + campaign.nameSlug + "'> Bekijk de campagne</a>"
					}
				}
			}, function(){
				//dismissed
			})
		}

		$scope.getShopList = function() {
			$api.get('shops')
			
				.then(function(response) {
					$scope.loading = false;
					$scope.shops = response.data.shops;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}
		
		//accordion
		$scope.oneAtATime = true;
		
			$scope.groups = [
			{
				title: 'Dynamic Group Header - 1',
				content: 'Dynamic Group Body - 1'
			},
			{
				title: 'Dynamic Group Header - 2',
				content: 'Dynamic Group Body - 2'
			}
			];
		
			$scope.items = ['Item 1', 'Item 2', 'Item 3'];
		
			$scope.addItem = function() {
			var newItemNo = $scope.items.length + 1;
			$scope.items.push('Item ' + newItemNo);
			};
		
			$scope.status = {
				isCustomHeaderOpen: false,
				isFirstOpen: false,
				isFirstDisabled: true,
			};
		
			$scope.translateDiscoverSolutions = {
				nl: 'Ontdek oplossingen',
				en: 'Discover solutions',
				de: 'Lösungen entdecken',
				fr: 'Découvrez les solutions',
				es: 'Descubra soluciones'
			}
			$scope.selectedTestApp = $scope.translateDiscoverSolutions[$rootScope.language];

		$scope.showForm = false;

		$scope.openLoginRegisterModal = function(mode){
			var tab, data;
			if($scope.activeTab) {
				tab = $scope.activeTab;
			}else if(!$scope.activeTab && $scope.selectedTestApp != "Ontdek oplossingen"){
				//tab 0 is default 'null'
				tab = 5;
			}
			var redirect = true;
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/login-modal.html',
				controller: 'loginModalController',
				resolve: {
					mode: function() {
						return mode;
					},
					tab: function() {
						return tab;
					},
					redirect: function(){
						return redirect;
					},
					data: function(){
						return data
					}
				}
			});			
			modalInstance.result.then(function(result){
				$scope.initShoppingCart();
			}, function(){
				
			})

		}

		$scope.total = 115;
		$scope.calculate = function(method, amount){
			if(method){
				$scope.total += amount;
			}else{
				$scope.total -= amount;
			}
		}

}]);