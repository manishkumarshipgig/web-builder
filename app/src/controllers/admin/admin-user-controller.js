prismanoteApp.controller('usersController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 
	function($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt) {
		$scope.loading = true;
		$scope.totalDisplayed = 20;

		

		$scope.getUsers = function() {
			var params = {
				sort: {lastLogin: 'desc'}
			}
			if($stateParams.userType != '' && typeof $stateParams.userType != 'undefined'){
				localStorage.setItem('userType',$stateParams.userType);				
			}

			$api.get('users', params)
			
				.then(function(response) {
					$scope.users = [];
					
					var userData = response.data.users;
					var getLocalState = localStorage.getItem('userType');

					if(getLocalState != null && typeof getLocalState != 'undefined'){

						angular.forEach(userData,function(val){
							if(val.role == getLocalState){
								$scope.users.push(val);
							}
						})
					}else{
						$scope.users = userData;
					}
					
					
					$scope.loading = false;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}

		$scope.resetSelected = function() {
			$scope.currentShop = null;
			$scope.currentAddress = null;
			$scope.currentPhone = null;
			$scope.currentWholesaler = null;
		}

		$scope.loadMore = function() {
			$scope.totalDisplayed += 20;
		}

		$scope.loadUser = function() {
			$api.get('user/' + $stateParams.userId)
			
				.then(function(response) {

					$scope.user = response.data.user;
					$scope.getPhoneNumber();
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}

		$scope.getPhoneNumber = function(){
			if($scope.user.phone){
				var phone = $scope.user.phone;

				if(phone.mobilePhone){
					$scope.user.phonenumber = phone.mobilePhone;
				}else{
					$scope.user.phonenumber = '+' + phone.countryCode + phone.landLine;
				}
			}
		}

		$scope.updateUser = function(role) {
			$api.put('user/' + $stateParams.userId, {user: $scope.user})
				.then(function(response) {
					$state.go('admin.users',{'userType':role});
				})
				.catch(function(reason) {
					console.log(reason);
				});
		}

		$scope.openResetPasswordModal = function(userId) {
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/admin-reset-password-modal.html',
			controller: 'adminResetPasswordModalController',
			resolve: {
				userId: function() {
						return userId;
				}
			}
		});
		modalInstance.result.then(function() {
		}, function() {
			//Modal is dismissed
		})
	};

		$scope.openAddress = function(index){
			$scope.resetSelected();
			$scope.addNewAddress = false;
			$scope.currentAddress = $scope.user.address[index];
			$scope.currentAddress.index = index;
		}

		$scope.newAddress = function() {
			$scope.resetSelected();
			$scope.addNewAddress = true;
		}

		$scope.deleteAddress = function(index){
			prompt({
				title: 'Adres verwijderen?',
				message: 'Weet u zeker dat u dit adres wilt verwijderen?'
			}).then(function() {
				$scope.user.address.splice(index, 1);
				$scope.resetSelected();
			});
		}

		$scope.pushAddress = function() {
			$scope.user.address.push($scope.currentAddress);
			$scope.addNewAddress = false;
		}

		$scope.cancelNewAddress = function() {
			$scope.addNewAddress = false;
			$scope.resetSelected();
		}

		$scope.openShop = function(index){
			$scope.resetSelected();
			$scope.currentShop = $scope.user.shops[index];
			$scope.currentShop.index = index;
		}

		$scope.openWholesaler = function(index){
			$scope.resetSelected();
			$scope.currentWholesaler = $scope.user.wholesalers[index];
			$scope.wholesalers.index = index;
		}

		$scope.getShops = function() {
			$api.get('shops')
			
				.then(function(response) {
					$scope.shops = response.data.shops;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}

		$scope.getWholesalers = function() {
			$api.get('wholesalers')
			
				.then(function(response) {
					$scope.wholesalers = response.data.wholesalers;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}

		$scope.searchShops = function(str) {
			var matches = [];
			$scope.shops.forEach(function(shop){
				if((shop.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
				(shop.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(shop);
				}
			});
			return matches;
		}

		$scope.searchWholesalers = function(str) {
			var matches = [];
			$scope.wholesalers.forEach(function(wholesaler){
				if((wholesaler.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
				(wholesaler.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(wholesaler);
				}
			});
			return matches;
		}

		$scope.searchUsers = function(str){
			var matches = [];
			$scope.users.forEach(function(user){
				if((user.email.toLowerCase().indexOf(str.toString().toLowerCase()) >=0 ) ||
					(user.firstName && user.firstName.toLowerCase().indexOf(str.toString().toLowerCase()) >=0) ||
					(user.lastName && user.lastName.toLowerCase().indexOf(str.toString().toLowerCase()) >=0 ) ||
					(user.username.toLowerCase().indexOf(str.toString().toLowerCase()) >=0 )) 
					{
						matches.push(user);
					}
			})
			return matches;
		}

		$scope.searchBrands = function(str) {
			var matches = [];
			$scope.brands.forEach(function(brand){
				if((brand.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
				(brand.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(brand);
				}
			})
			return matches;
		};

		$scope.shopSelected = function(selected){
			var shop = {
				_id: selected.originalObject._id,
				name: selected.title,
				nameSlug: selected.originalObject.nameSlug,
				type: 'shop',
				//TODO: Adres van shop
			}
			$scope.user.shops.push(shop);
			$scope.currentShop = $scope.user.shops[$scope.user.shops.length-1];
		};

		$scope.wholesalerSelected = function(selected){
			var wholesaler = {
				_id: selected.originalObject._id,
				name: selected.title,
				nameSlug: selected.originalObject.nameSlug,
			}
			if(!$scope.user.wholesalers){
				$scope.user.wholesalers = [];
			}
			$scope.user.wholesalers.push(wholesaler);
			$scope.currentWholesaler = $scope.user.wholesalers[$scope.user.wholesalers.length-1];
		};

		$scope.userSelected = function(selected){
			var user = {
				_id: selected.originalObject._id,
				name: selected.originalObject.firstName + " " + (selected.originalObject.lastNamePrefix ? selected.originalObject.lastNamePrefix : "") + " " + selected.originalObject.lastName,
				email: selected.originalObject.email
			}
			
			var index = _.findIndex($scope.user.socialPortal.users, {'_id': user._id})
			console.log("index", index);
			if(index < 0){
				$scope.user.socialPortal.users.push(user);
			}

			$scope.saveSocialPortal();
		}

		$scope.brandSelected = function(selected){
			var brand = {
				_id: selected.originalObject._id,
				name: selected.title
			}
			var index = _.findIndex($scope.user.socialPortal.brands, {'_id': brand._id})
			
			if(index < 0){
				$scope.user.socialPortal.brands.push(brand);
			}

			$scope.saveSocialPortal();
		}

		$scope.deleteShop = function(index){
			prompt({
					title: 'Winkel verwijderen?',
					message: 'Weet u zeker dat u deze winkel wilt verwijderen?'
			}).then(function() {
					$scope.user.shops.splice(index, 1);
					$scope.resetSelected();
			});
		}

		$scope.deleteWholesaler = function(index){
			prompt({
					title: 'Distribiteur verwijderen?',
					message: 'Weet u zeker dat u deze distribiteur wilt verwijderen?'
			}).then(function() {
					$scope.user.wholesalers.splice(index, 1);
					$scope.resetSelected();
			});
		}

		$scope.openPhone = function(index){
			$scope.resetSelected();
			$scope.currentPhone = $scope.user.phone[index];
			$scope.currentPhone.index = index;
		}

		$scope.deletePhone = function(index){
			prompt({
					title: 'Telefoonnummer verwijderen?',
					message: 'Weet u zeker dat u dit telefoonnummer wilt verwijderen?'
			}).then(function() {
					$scope.user.phone.splice(index, 1);
					$scope.resetSelected();
			});
		}

		$scope.newPhone = function(index){
			$scope.resetSelected();
			$scope.addNewPhone = true;
		}

		$scope.pushPhone = function() {
			$scope.user.phone.push($scope.currentPhone);
			$scope.addNewPhone = false;
		}

		$scope.cancelNewPhone = function() {
			$scope.addNewPhone = false;
			$scope.resetSelected();
		}

		$scope.getBrands = function() {
			$api.get('brands')
			
				.then(function(response) {
					$scope.brands = response.data.brands;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		};
	
		$scope.getSocialPortal = function(){

			$api.get('user-social-portal', {userId: $stateParams.userId})
				.then(function(response){
					console.log("RESPONSE", response);
					$scope.user.socialPortal = response.data.result;
					$scope.loading = false;
					console.log("sp", $scope.user);
				})
				.catch(function(reason){
					$scope.loading = false;
					if(reason.indexOf('not found') !== -1){

						//there is no Social Media Portal for this user, lets create one
						$api.post('user-social-portal', {user: $scope.user})
						.then(function(response){
							$scope.getSocialPortal();
						})
						.catch(function(reason){
							console.log("fout bij aanmaken portal", reason);
						})
					}
				})
		}

		$scope.deleteBrandFromSocialPortal = function(index){
			prompt({
				title: 'Merk verwijderen?',
				message: 'Weet u zeker dat u dit merk wilt verwijderen uit het social media portaal van deze gebruiker?'
			}).then(function() {
				$scope.user.socialPortal.brands.splice(index, 1);
				$scope.saveSocialPortal();
			});
		}
		$scope.upgradeCreditSocialPortal = function(index, credit){
			$scope.user.socialPortal.brands.splice(index, 1, $scope.user.socialPortal.brands[index]);
			console.log("brand log", $scope.user.socialPortal.brands[index]);
			$scope.saveSocialPortal();
		}

		$scope.deleteShopFromSocialPortal = function(index){
			prompt({
				title: 'Shop verwijderen?',
				message: 'Weet u zeker dat u deze winkel wilt verwijderen uit het social media portaal van deze gebruiker?'
			}).then(function() {
				$scope.user.socialPortal.shops.splice(index, 1);
				$scope.saveSocialPortal();
			});
		}

		$scope.deleteWholesalerFromSocialPortal = function(index){
			prompt({
				title: 'Distribiteur verwijderen?',
				message: 'Weet u zeker dat u deze distribiteur wilt verwijderen uit het social media portaal van deze gebruiker?'
			}).then(function() {
				$scope.user.socialPortal.wholesalers.splice(index, 1);
				$scope.saveSocialPortal();
			});
		}

		$scope.deleteUserFromSocialPortal = function(index){
			prompt({
				title: 'Gebruiker verwijderen?',
				message: 'Weet u zeker dat u deze gebruiker wilt verwijderen uit het social media portaal van deze gebruiker?'
			}).then(function() {
				$scope.user.socialPortal.users.splice(index, 1);
				$scope.saveSocialPortal();
			});
		}

		$scope.addWholesalerToSocialPortal = function(){
			var nameSlug = $scope.user.wholesalers[$scope.socialPortalWholesaler].nameSlug;
			$api.get('wholesalers/' + nameSlug)
				.then(function(response) {

					var wholesaler = response.data.wholesaler;
					var newWholesaler = {
						_id: wholesaler._id,
						name: wholesaler.name
					}
					if($scope.user.socialPortal.wholesalers.length == 0){
						$scope.user.socialPortal.wholesalers.push(newWholesaler);
					}else{
						var index = _.findIndex($scope.user.socialPortal.wholesalers, {'_id': newWholesaler._id})
						if(index < 0){
							$scope.user.socialPortal.wholesalers.push(newWholesaler);
						}
					}
					
					wholesaler.brands.forEach(function(brand){
						var newBrand = {
							_id: brand._id,
							name: brand.name
						}
						$scope.user.socialPortal.brands.push(newBrand);
						var result = _.map(_.uniqBy($scope.user.socialPortal.brands,'_id'), function(item){
							return {
								_id: item._id,
								name: item.name
							}
						});
						$scope.user.socialPortal.brands = result;
					})
					$scope.saveSocialPortal();
				})
				.catch(function(reason) {
					console.log(reason);
				})
		}

		$scope.addShopToSocialPortal = function(){
			var nameSlug = $scope.user.shops[$scope.socialPortalShop].nameSlug;
			$api.get('shops/' + nameSlug)
				.then(function(response) {

					var shop = response.data.shop;
					var newShop = {
						_id: shop._id,
						name: shop.name
					}
					console.log('in shops socila portal',$scope.user);
					// $scope.user.socialPortal={
					// 	brands:[],
					// 	shops:[],
					// 	users:[],
					// 	_id : $scope.user._id
					// };
					
					 var length=  $scope.user.socialPortal.shops.length;
					if(length == 0){
						$scope.user.socialPortal.shops.push(newShop);
					}else{
						var index = _.findIndex($scope.user.socialPortal.shops, {'_id': newShop._id})
						if(index < 0){
							$scope.user.socialPortal.shops.push(newShop);
						}
					}
					// var user = {
					// 	_id: $scope.user._id,
					// 	name: $scope.user.firstName + " " + ($scope.user.lastNamePrefix ? $scope.userlastNamePrefix : "") + " " + $scope.user.lastName,
					// 	email: $scope.user.email
					// }
				
					// var index = _.findIndex($scope.user.socialPortal.users, {'_id': user._id})
					// if(index < 0){
					// 	$scope.user.socialPortal.users.push(user);
					//}
					
					shop.brands.forEach(function(brand){
						var newBrand = {
							_id: brand._id,
							name: brand.name
						}
						$scope.user.socialPortal.brands.push(newBrand);
						var result = _.map(_.uniqBy($scope.user.socialPortal.brands,'_id'), function(item){
							return {
								_id: item._id,
								name: item.name
							}
						});
						$scope.user.socialPortal.brands = result;
					})
					$scope.saveSocialPortal();
				})
				.catch(function(reason) {
					console.log(reason);
				})
		}

		$scope.saveSocialPortal = function(){
			$api.put('user-social-portal', {socialPortal: $scope.user.socialPortal})
				.then(function(response) {
					
				})
				.catch(function(reason) {
					console.log(reason);
				})
		}
}])
