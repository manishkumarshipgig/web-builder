prismanoteApp.controller('adminArticleBaseController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload', '$http', 'FileSaver', 
function($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt, Upload, $http, FileSaver) {

	$scope.version = "1.1.0";

	$scope.getBrands = function() {
        $api.get('brands')
		
			.then(function(response) {
				$scope.brands = response.data.brands;
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
    };

    $scope.searchBrands = function(str) {
		var matches = [];
	
		$scope.brands.forEach(function(item) {
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || 
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

		return matches;
	};

    $scope.brandSelected = function(selected){
        $scope.selectedBrand = selected.title;
        $scope.brandSlug = selected.originalObject.nameSlug;
    };
    
    $scope.generateBaseFile = function(){
		$scope.alert = null;

        if(!$scope.brandSlug){
            $scope.alert = {
				type: 'danger',
				msg: "Geen merk gekozen"
			}
			return;
		}
		if(!$scope.selectedLanguge){
			$scope.alert = {
				type: 'danger',
				msg: "Geen taal gekozen"
			}
			return;
		}
		$scope.loading = true;

		$http({
			method: 'GET',
			url: '/api/cs/article-base',
			params: {
				lang: $scope.selectedLanguge,
				brand: $scope.brandSlug,
				version: $scope.version
			},
			responseType: 'arraybuffer'
		})
		.then(function(response) {
			console.log("res", response);
			$scope.alert = {
				type: 'success',
				msg: 'Bestand is gegenereerd'
			}
			$scope.loading = false;
			var file = new Blob([response.data], {type: 'text/plain'})
			var filename = "GENERAL_EXEC_NL_" + $scope.selectedLanguge + "_EUR.DAT";
			FileSaver.saveAs(file, filename);

		}, function(response){
			$scope.loading = false;
			if(response.status == 422){
				$scope.alert = {
					type: "danger",
					msg: "Er zijn geen producten gevonden om te exporteren"
				}
				return;
			}
			$scope.alert = {
				type: 'danger',
				msg: 'Er is een fout opgetreden: ' + response.statusText + "(" + response.status + ")"
			}
		})
	}
}])