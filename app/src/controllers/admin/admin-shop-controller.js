prismanoteApp.controller('shopsController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload', 
	function($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt, Upload) {
		
		$scope.totalDisplayed = 20;
		$scope.upload = false;

		$scope.getShopList = function() {
			$scope.loading = true;
			$scope.shops = {};
			$api.get('shops')
			
				.then(function(response) {
					$scope.loading = false;
					$scope.shops = response.data.shops;
					$scope.shopsFilter = [];
					$scope.shops.forEach(function(val){
						$scope.shopsFilter.push({'name':val.name,'nameSlug' : val.nameSlug});
					})
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}

		$scope.resetSelected = function() {
			$scope.currentSocialMedia = null;
			$scope.currentHome = null;
			$scope.currentSlide = null;
		}

		$scope.loadMore = function() {
			$scope.totalDisplayed += 20;
		}

		$scope.loadShop = function() {
			$api.get('shops/' + $stateParams.nameSlug)
			
				.then(function(response) {
					$scope.shop = response.data.shop;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}

		$scope.updateShop = function () {
			$api.put('shops/' + $stateParams.nameSlug, { shop: $scope.shop })

				.then(function (response) {
					if (response.data.shop) {
						$state.go('admin.users');
					}
				})

				.catch(function (reason) {
					console.log(reason);
				});
		}

		$scope.openSocialMedia = function(index){
				$scope.resetSelected();
				$scope.addNewSocialMedia = false;
				$scope.currentSocialMedia = $scope.shop.socialMedia[index];
				$scope.currentSocialMedia.index = index;
		}

		$scope.newSocialMedia = function() {
				$scope.resetSelected();
				$scope.addNewSocialMedia = true;
		}

		$scope.deleteSocialMedia = function(index){
				prompt({
						title: 'Link verwijderen?',
						message: 'Weet u zeker dat u deze link wilt verwijderen?'
				}).then(function() {
						$scope.shop.socialMedia.splice(index, 1);
						$scope.resetSelected();
				});
		}

		$scope.pushSocialMedia = function(currentSocialMedia) {
				$scope.currentSocialMedia = currentSocialMedia;
				$scope.shop.socialMedia.push($scope.currentSocialMedia);
				$scope.addNewSocialMedia = null;
				$scope.resetSelected();
		}

		$scope.cancelSocialMedia = function() {
				$scope.currentSocialMedia = null;
				$scope.addNewSocialMedia = false;
				$scope.resetSelected();
		}

		$scope.getBrands = function() {
			$api.get('brands')
			
				.then(function(response) {
					$scope.brands = response.data.brands;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		}
	 
		$scope.searchBrands = function(str) {
				var matches = [];
				$scope.brands.forEach(function(brand){
						if((brand.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
						(brand.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
										matches.push(brand);
								}        
				})
				return matches;
		}

		$scope.brandSelected = function(selected){
				var brand = {
						_id: selected.originalObject._id,
						name: selected.title,
						nameSlug: selected.originalObject.nameSlug,
						description: selected.originalObject.description,
						images: selected.originalObject.images
				}
				$scope.shop.brands.push(brand);
		};

		 $scope.deleteBrand = function(index){
				prompt({
						title: 'Merk verwijderen?',
						message: 'Weet u zeker dat u dit merk van deze winkel wilt verwijderen?'
				}).then(function() {
						$scope.shop.brands.splice(index, 1);
						$scope.resetSelected();
				});
		}

		$scope.openHome = function(index) {
				$scope.resetSelected();
				$scope.currentHome = $scope.shop.home[index];
		}

		$scope.openSlide = function(index){
			//hier geen resetSelected doen omdat anders currentHome leeg is
			$scope.currentSlide = null;
			$scope.currentSlide = $scope.currentHome.slides[index];
			// refresh option list
			$scope.generateInternalLinks()
		}

		$scope.uploadSlidePhoto = function() {
				if($scope.addNewSlide){
						$scope.currentHome.slides.push($scope.currentSlide);
				}
				if($scope.currentSlide.sliderPhoto){
						
						$scope.upload = true;
						Upload.upload({
							url: 'api/shop/uploadsliderphoto',
							data: {
									shopId: $scope.shop._id,
									file: $scope.currentSlide.sliderPhoto
							}
						})
						.then(function (res){
								$scope.currentSlide.src = res.data.file;
								$scope.currentSlide.sliderPhoto = null;
								$scope.upload = false;
								$scope.updateShop(false);
						}, function (res){
								console.log("Error: ", res.status);
						}, function(evt){
								var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
								$scope.uploadProgress = progressPercentage;
								console.log("Progress: " + progressPercentage + '%' + evt.config.data);
						});
				}else{
						$scope.updateShop(false);
				}
		}

		$scope.newSlide = function() {
				$scope.currentSlide = null;
				$scope.addNewSlide = true;  
		}

		$scope.addSlide = function() {
			
						var newSlide = {
							title: '',
							buttonText: '',
							buttonLink: '',
							alt: '',
							src: ''
						}
						$scope.currentHome.slides.push(newSlide)
						$scope.currentSlide = newSlide	
		}

		$scope.deleteSlide = function(index){
			//hier geen resetSelected doen omdat anders currentHome leeg is
			$scope.currentHome.slides.splice(index, 1)
			
			// $scope.currentSlide = null;
			// $scope.currentSlide = $scope.currentHome.slides[index];
		}

		$scope.openItem = function(index){
			//hier geen resetSelected doen omdat anders currentHome leeg is
			$scope.currentItem = null;
			$scope.currentItem = $scope.currentHome.items[index];
			// refresh option list
			$scope.generateInternalLinks()
		}

		$scope.uploadItemPhoto = function() {
			if($scope.currentItem.itemGridPhoto){
					$scope.upload = true;
					Upload.upload({
						url: 'api/shop/uploaditemgridphoto',
						data: {
								shopId: $scope.shop._id,
								file: $scope.currentItem.itemGridPhoto
						}
					})
					.then(function (res){
							$scope.currentItem.src = res.data.file;
							$scope.currentItem.itemGridPhoto = null;
							$scope.upload = false;
							$scope.updateShop(false);
					}, function (res){
							console.log("Error: ", res.status);
					}, function(evt){
							var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
							$scope.uploadProgress = progressPercentage;
							console.log("Progress: " + progressPercentage + '%' + evt.config.data);
					});
			}else{
					$scope.updateShop(false);
			}
		}

		$scope.addNewItem = function() {

			var newItem = {
				title: '',
				content: '',
				alt: '',
				src: '',
				url: '',
				external: ''
			}
			$scope.currentHome.items.push(newItem)
			$scope.currentItem = newItem

		}

		$scope.deleteItem = function(index){
			$scope.currentHome.items.splice(index, 1)
		}

		$scope.openCreateShopModal = function(){
			$scope.alert = null;

			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/create-shop-modal.html',
				controller: 'createShopModalController',
				size: 'lg',
			});

			modalInstance.result.then(function(result){

				for(var i = result.length - 1; i >= 0; i--) {
					if(array[i] === tasks) {
						array.splice(i, 1);
					}
				}

				result.nameSlug = $rootScope.slugify(result.name);
				result.isPublished = false;

				$api.post('shops', { shop: result })

					.then(function (response) {
						$api.get('shops')

						.then(function (response) {

							$scope.alert = {
								type: 'success',
								msg: response.data.message
							}
							
							$scope.getShopList();

						})

						.catch(function (reason) {
							console.log(reason);
						});
					})
					
					.catch(function(reason) {
						console.log(reason);
						$scope.alert = {
							type: 'danger',
							msg: reason
						}
					});

			}, function(){

			})
		}

		$scope.checkForWebshop = function(){
			if($scope.shop && !$scope.shop.webshopActive){
				$scope.shop.webshopActive = true;
			}
		}
		$scope.generateInternalLinks = function(){

			var internalLinks = [
				{
					nlName: 'Home',
					enName: 'Home',
					deName: 'Home',
					frName: 'Home',
					spName: 'Home',
					link: '.home'
				},
				{
					nlName: 'Producten',
					enName: 'Products',
					deName: 'Producten',
					frName: 'Produits',
					spName: 'Productos',
					link: '.products'
				},
				{
					nlName: 'Merken',
					enName: 'Brands',
					deName: 'Marken',
					frName: 'Marques',
					spName: 'Marcas',
					link: '.brands'
				},
				{
					nlName: 'Acties',
					enName: 'Actions',
					deName: 'Aktionen',
					frName: 'Actions',
					spName: 'Acciones',
					link: '.actions'
				},
				{
					nlName: 'Recensies',
					enName: 'Reviews',
					deName: 'Rezensionen',
					frName: 'Commentaires',
					spName: 'Opiniones',
					link: '.reviews'
				},
				{
					nlName: 'Contact',
					enName: 'Contact',
					deName: 'Kontakt',
					frName: 'Contact',
					spName: 'Contacto',
					link: '.contact'
				},
			];

			console.log("internalLinks", internalLinks)
			console.log("brands", $scope.shop.brands);
			for (i = 0; i < $scope.shop.brands.length; i++) {
				console.log("lenght", $scope.shop.brands.length);
				console.log("name of brand", $scope.shop.brands[i].name);
				var newBrand = {
					nlName: $scope.shop.brands[i].name,
					enName: $scope.shop.brands[i].name,
					deName: $scope.shop.brands[i].name,
					frName: $scope.shop.brands[i].name,
					spName: $scope.shop.brands[i].name,
					link: ".brand({brandNameSlug:'" + $scope.shop.brands[i].nameSlug + "',nameSlug:shop.nameSlug})"
				}
				internalLinks.push(newBrand);
			}
			
			console.log("lenght", $scope.shop.news.length);
			for (i = 0; i < $scope.shop.news.length; i++) {
				console.log("lenght", $scope.shop.news.length);
				console.log("name of newsItem", $scope.shop.news[i].name);
				var newNewsItem = {
					nlName: $scope.shop.news[i].name,
					enName: $scope.shop.news[i].name,
					deName: $scope.shop.news[i].name,
					frName: $scope.shop.news[i].name,
					spName: $scope.shop.news[i].name,
					link: ".news({newsItemSlug:'" + $scope.shop.news[i].nameSlug + "',nameSlug:shop.nameSlug})"
					//-link: ".news({nameSlug:shop.nameSlug,newsItemSlug:" + $scope.shop.news[i].nameSlug + "})"
				}
				internalLinks.push(newNewsItem);
			}

			$scope.linkOptions = internalLinks;
			console.log("internalLinks after loop", internalLinks)
			//- collecties & producten
		}

		$scope.logoUpload = function(type) {
			console.log($scope.shop);
			console.log($scope.shop.logoDark);
			console.log($scope.shop.logoLight);
			if(type == "dark"){
				var shopLogo = $scope.shop.logoDark
			}else{
				var shopLogo = $scope.shop.logoLight
			}
			console.log("shopLogo", shopLogo);
			if(shopLogo){
					
					$scope.upload = true;
					Upload.upload({
						url: 'api/shop/uploadlogo',
						data: {
								shopId: $scope.shop._id,
								file: shopLogo
						}
					})
					.then(function (res){
							console.log("res.data.file", res.data.file);
							shopLogo.src = res.data.file;
							shopLogo = null;
							$scope.upload = false;
							$scope.updateShop(false);
					}, function (res){
							console.log("Error: ", res);
					}, function(evt){
							var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
							$scope.uploadProgress = progressPercentage;
							console.log("Progress: " + progressPercentage + '%' + evt.config.data);
					});
			}else{
					$scope.updateShop(false);
			}
	}

	$scope.initCountr = function(){
		console.log("initCountr", $scope.shop);
		$scope.countr = {};
		$scope.getCountrStores();
		$scope.getWebhooks();
		
	}

	$scope.getCountrStores = function(){
		$api.get('countr/shops', {
			shopId: $scope.shop._id
		})
			.then(function(response) {
				
				$scope.countr.stores = response.data;
			})
			.catch(function(reason) {
				console.error(reason);
			})
	}

	$scope.getWebhooks = function(){
		$scope.countr.webhooks = {};
		$api.get('countr/webhooks', {
			shopId: $scope.shop._id
		})
		.then(function(response) {
			console.log("RES", response);
			$scope.countr.webhooks = response.data;
			$scope.countr.webhooks.forEach(function(hook){
				console.info(hook.topic, hook.url);
			})
		})
		.catch(function(reason) {
			console.error(reason);
		})
	}

	$scope.deleteHook = function(id, index){
		$api.delete('countr/webhooks/' + id + '/' + $scope.shop._id)
		.then(function() {
			$scope.countr.webhooks.splice(index, 1);                    
		})
		.catch(function(reason) {
			console.error(reason);
			$scope.countrAlert = {
				type: 'danger',
				msg: reason
			}
		})
	}

	$scope.webhookSetup = function(){
		var counter = 0, webhooks = $scope.countr.webhooks.length;

		if(webhooks > 0){
			$scope.countr.webhooks.forEach(function(hook, i){
				$scope.deleteHook(hook._id, i);
				counter++;

				if(counter == webhooks){
					setup();
				}
			})
		}else{
			setup();
		}

		function setup(){
			console.info("Starting creating hooks")
			$api.post('countr/webhook-setup', {shopId: $scope.shop._id})
			.then(function(response) {
				$scope.getWebhooks();
			})
			.catch(function(reason) {
				console.log(reason);
			})
		}
	}

	$scope.deleteAllProducts = function(storeId){
		$scope.result = null;
		prompt({
			title: 'ALLE producten verwijderen?',
			message: 'Weet u zeker dat u ALLE producten van deze winkel wilt verwijderen? Per keer zullen er maximaal 1000 producten verwijderd worden'
	}).then(function() {
		$api.post('countr/delete-all-products', {
			shopId: $scope.shop._id,
			storeId: storeId
		})
			.then(function(response) {
				$scope.result = {
					type: 'success',
					msg: response.data.message
				}
			})
			.catch(function(reason) {
				$scope.result = {
					type: 'danger',
					msg: reason
				}
			})
	});
	}

	$scope.deleteAllCategories = function(storeId){
		$scope.result = null;
		prompt({
			title: 'ALLE categorieën verwijderen?',
			message: 'Weet u zeker dat u ALLE categorieën van deze winkel wilt verwijderen? Per keer zullen er maximaal 1000 categorieën verwijderd worden'
	}).then(function() {
		$api.post('countr/delete-all-categories', {
			shopId: $scope.shop._id,
			storeId: storeId
		})
			.then(function(response) {
				$scope.result = {
					type: 'success',
					msg: response.data.message
				}
			})
			.catch(function(reason) {
				$scope.result = {
					type: 'danger',
					msg: reason
				}
			})
	});
	}
}])