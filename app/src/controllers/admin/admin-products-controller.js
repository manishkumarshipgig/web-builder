prismanoteApp.controller("adminProductsController", ['$scope', '$rootScope', '$stateParams', '$api', '$q', '$translate',
	function($scope, $rootScope, $stateParams, $api, $q, $translate) {

		if($stateParams.category && typeof $stateParams.category == 'string' && ['WATCH', 'STRAP', 'JEWEL'].includes($stateParams.category)) {
			$scope.category[$stateParams.category.toLowerCase()] = true;
		}

		// Array with all currently displayed items in /products
		$scope.products = [];

		$scope.selection = [];

		$scope.productCategory = [];

		$scope.isVerified = true;

		$scope.isNotVerified = true;

		$scope.convertDate = function(date) {
			var d = new Date(date);
			// return d.toUTCString();
			return d.toLocaleString()
		}
		
		$scope.verificationsChanged = function(){
			var params = {
				filter : {
					isVerified: false
				},
				offset: 0
			}
			$scope.getProducts(params);

		}

		$scope.changeSuggestion = function(fields) {
			console.log("fields", fields);
		}

		$scope.selectItem = function(nameSlug, index) {
			var indexOf = $scope.selection.indexOf(nameSlug);
			if(indexOf != -1) {
				$scope.selection.splice(indexOf, 1);
			} else {
				$scope.selection.push(nameSlug);
			}
			$scope.products[index].selected = !$scope.products[index].selected;
		}

		$scope.limit = 24;

		$scope.setLimit = function(limit) {
			if(limit != null) {
				$scope.limit = limit;
			} else {
				$scope.limit = 24;
			}
		}

		// format kids, male and female booleans into a translated and formatted gender string
		var formatGender = function(male, female, kids) {
			return $q(function(resolve,reject) {
				if(kids == false) {
					if(male == true && female == true) {
						$translate('GENTS').then(function(gents) {
							$translate('LADIES').then(function(ladies) {
								resolve(gents + ' / ' + ladies);
							});
						});
					} else if(male == true && female == false) {
						$translate('GENTS').then(function(gents) {
							resolve(gents);
						});
					} else if(male == false && female == true) {
						$translate('LADIES').then(function(ladies) {
							resolve(ladies);
						});
					}
				} else {
					if(male == true && female == true) {
						$translate('BOYS').then(function(boys) {
							$translate('GIRLS').then(function(girls) {
								resolve(boys + ' / ' + girls);
							});
						});
					} else if(male == true && female == false) {
						$translate('BOYS').then(function(boys) {
							resolve(boys);
						});
					} else if(male == false && female == true) {
						$translate('GIRLS').then(function(girls) {
							resolve(girls);
						});
					}
				}
			})
		};

		// Async function to add/format gender and possibly other additional properties later on
		var formatProduct = function(product) {
			return $q(function(resolve, reject) {
				//console.log('product....',product);
				formatGender(product.male, product.female, product.kids)
				
				.then(function(formattedGender) {
					//console.log('formattedGender....',formattedGender);
					product.gender = formattedGender;
					resolve(product);
				})

				.catch(function(reason) {
					reject(reason);
				});
			});
		};

		var getProductCount = function() {
			return $q(function(resolve, reject) {

				$api.getCount('products')

				.then(function(productCount) {
					$scope.productCount = productCount;
					resolve(productCount);
				})

				.catch(function(reason) {
					reject(reason);
				});
			});
		};

		// Get new products from the database and add them to the $scope
		$scope.getProducts = function(params, options) {
			//console.log("IN GET PRODUCTS CONTROLLER !");

			console.log("-------------params controller---------------",params);
			


			if(!params) {
				params = {};
			}

			if(!params.filter) {
				params.filter = {};
			}
			return $q(function(resolve, reject) {

				$scope.loading = true;

				if(!params) {
					params = {};
				}

				if(!params.filter) {
					params.filter = {};
				}

				var apiRequest = function() {
					params.sort = {priorityKey: -1};
					if(params.offset != 0) {
						params.offset = $scope.products.length;
						params.limit = $scope.limit;
					}

					// Below if else condition add by akib
					
					if ($scope.products.length < params.limit && params.offset == 0) {
						params.offset = 0;
					} else {
						params.offset = $scope.products.length + params.limit;
					}

					if($scope.isVerified){
						params.filter.isVerified = true;
					}
					if($scope.isNotVerified){
						params.filter.isVerified = false;
					}

					if($scope.isVerified && $scope.isNotVerified){
						//console.log("Is Verified And Not Verified.");
						params.filter.isVerified = "showAll"
					}
					if($scope.isVerified == false && $scope.isNotVerified == false){
						//console.log("Is Verified And Not Verified.");
						params.filter.isVerified = "showAll";
					}

					// if(typeof params.filter.isVerified !== 'boolean') {
					// 	params.filter.isVerified = false;
					// }
					// params.filter.isVerified = $scope.isVerified;

					if(params.filter.category && params.filter.category.$in) {
						$scope.productCategory = params.filter.category.$in;
					} else {
						$scope.productCategory = [];
					}



					getProductCount()
					
					.then(function(res) {

						$api.get('products', params)
						.then(function(response) {
							$scope.products = [];
									// Loop through newly fetched products and add them to the $scope.
									for(var i = 0; i < response.data.products.length; i++) {
										$scope.products.push(response.data.products[i]);

										if($scope.products.length == params.offset + response.data.products.length) {
											$scope.loading = false;
										}
										formatProduct(response.data.products[i])
										.then(function(formattedProduct) {
											//console.log("Products = ",formattedProduct);
											$scope.products.push(formattedProduct);

											if($scope.products.length == params.offset + response.data.products.length) {
												$scope.loading = false;
											}
										})

										.catch(function(reason) {
											reject(reason);
										});
									}
									
									$scope.noProductsFound = false;
									resolve();

								})

						.catch(function(reason) {
							$scope.noProductsFound = true;

							reject(reason);
						});
					})
				}
				
				// Delete the loaded products and load them again with the new apiParams
				if(options != null && typeof options === 'object') {
					
					if(options.reset === true) {
						$scope.products = [];
					}
				}

				apiRequest();
			});
		};
		$scope.getProducts();
	}]);
