prismanoteApp.controller('adminNewsController', ['$scope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload', '$state',
	function($scope, $api, $stateParams, $uibModal, $state, prompt, Upload, $state) {

	// Default options for fetching new items: 12 items with featured items first, sorted by views.
	var defaultApiOptions = {'sort': {'startDate': 'desc'}};

	// currently active API request params/options. Set to default options initially, may be changed or reset to default options later.
	var apiOptions = defaultApiOptions;

	// General setter to change the Api HTTP request options/params. Some wrapper functions are available to make the code more intuitive.
	var setApiOptions = function(options, type, append) {
		if(typeof options === 'object') {
			if(typeof append !== 'boolean' || append === false) {
				apiOptions[type] = options;
			} else {
				apiOptions[type] = _.extend(apiOptions[type], options);
			}
		} else if(options == null) {
			apiOptions = options;
		}
		$scope.products = new Array;
		$scope.getProducts();
	};

	$scope.news = new Array;

	$scope.getNewsItems = function() {
		var numberOfNewsPosts = $scope.news.length;
		apiOptions.offset = numberOfNewsPosts;

		$api.get('news', apiOptions)
		
			.then(function(response) {
				// Add newsPosts to the $scope.
				for(var i = 0; i < response.data.newsItems.length; i++) {
					var newsPost = response.data.newsItems[i];
					//console.log(newsPost);
					//if(newsPost.update == true) {
					$scope.news.push(newsPost);
					//} else {
					//	console.log("er ging iets niet goed met de aanvraag naar de backend");
					//}
				}
			})
			
			.catch(function(reason) {
				console.log(reason);
			})
	};

	$scope.addNewsItem = function(){
		$scope.alert = null;

		$api.post('news/', {newsItem:$scope.newsItem})

			.then(function(response) {
				$scope.upload = true;
					Upload.upload({
							url: 'api/news/news-photo-upload',
							data: {
									newsItemId: response.data.newsItem._id,
									file: $scope.newsItem.photo //file input field
							}
					})
					.then(function (res){
						//wanneer uploaden gelukt
						console.log("klaar!");
						$state.go('admin.news');
					}, function (res){
						//wanneer uploaden mislukt
						console.log("Error: ", res);
						$scope.alert = {
							type: "danger",
							msg: "Fout bij uploaden: " + res.status
						}

					}, function(evt){
							//tijdens upload
							var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
							$scope.uploadProgress = progressPercentage;
							console.log("Progress: " + progressPercentage + '%' + evt.config.data);
					});
					$scope.upload = false;
				
			})
			.catch(function(reason) {
				console.log(reason);
				$scope.alert = {
					type: "danger",
					msg: "Het lukte niet om uw gegevens op te slaan " + reason
				}
				$scope.upload = false;
			});
	}

	$scope.closeAlert = function() {
		$scope.alert = 0
	}


	var getItems = function(type){
		$api.get(type)
		
			.then(function(response) {
				$scope[type] = response.data[type];
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

// merken
	$scope.getBrands = function() {
		return getItems('brands');
	};

	$scope.getWholesalers = function() {
		return getItems('wholesalers');
	};

	var searchItems = function(str, type) {
		var matches = [];
	
		$scope[type].forEach(function(item) {
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || 
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

		return matches;
	};

	$scope.searchBrands = function(str) {
		return searchItems(str, 'brands');
	}

	$scope.searchWholesalers = function(str) {
		return searchItems(str, 'wholesalers');
	}

	$scope.brandSelected = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.newsItem.brand = brand;
	};

	$scope.wholesalerSelected = function(selected){
		var wholesaler = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.newsItem.wholesaler = wholesaler;
	};


}]);




