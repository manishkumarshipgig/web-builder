prismanoteApp.controller('adminCampaignController', ['$scope', '$rootScope', '$api', '$state', '$stateParams', 
	function($scope, $rootScope, $api, $state, $stateParams) {

	// $scope.tab = $stateParams.tab;
	
	$scope.getCampaign = function() {
		$api.get('campaigns/' + $stateParams.nameSlug)
		
			.then(function(response) {
				$scope.campaign = response.data.campaign;
				if($scope.campaign.tasks.length > 0) {
					$scope.activeTask = $scope.campaign.tasks[0].name;
				}
				$scope.taskLanguage = $rootScope.language;
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

	var getItems = function(type){
		$api.get(type)
		
			.then(function(response) {
				$scope[type] = response.data[type];
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

	$scope.approveCampaign = function(){
		console.log("approveCampaign");
		$api.post('approve-campaign', {campaign: $scope.campaign})
			.then(function(response) {
				$scope.campaign = response.data.campaign;
			})
			.catch(function(reason) {
				console.log(reason);
			})
	}

	$scope.getBrands = function() {
		return getItems('brands');
	};

	$scope.getWholesalers = function() {
		return getItems('wholesalers');
	};

	var searchItems = function(str, type) {
		var matches = [];
		
		$scope[type].forEach(function(item) {
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || 
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

		return matches;
	};

	$scope.searchBrands = function(str) {
		return searchItems(str, 'brands');
	}

	$scope.searchWholesalers = function(str) {
		return searchItems(str, 'wholesalers');
	}

	$scope.brandSelected = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.campaign.brand = brand;
	};

	$scope.wholesalerSelected = function(selected){
		var wholesaler = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.campaign.wholesaler = wholesaler;
	};

	$scope.updateCampaign = function(redirect){
		$api.put('campaigns/' + $stateParams.nameSlug, {campaign: $scope.campaign})
		
			.then(function() {
				if(redirect === true) {
					$state.go('admin.campaigns');
				}
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

}]);