prismanoteApp.controller("adminProductController", ['$scope','$parse', '$rootScope', '$stateParams','$state', '$api', '$q', '$translate',
	function($scope,$parse, $rootScope, $stateParams,$state, $api, $q, $translate) {
//
var verifyMode = false;
var nameSlug;
		// alert("Product Controller For Admin");
		//$scope.suggestionChanged = function(obj){
		//	console.log("all the products in suggestion " , $scope.product.suggestions);
		//
		//	console.log("CHANGED!");
		//	console.log('suggested product',obj);
		//	setTimeout(function(){
		//	console.log('test suggestion', $scope.product);
		//	},500)

		//	console.log('hi1');
			//console.log($scope.showProduct.watch)
		//}
		$scope.$watch("showProduct",function(item){
			console.log("ShowProduct Changed = ",$scope.showProduct);
		});
		$scope.convertDate = function(date) {
			var d = new Date(date);
			d.setHours( d.getHours() - 2 );
			// var dateStr = d.getFullYear() +'-'+ (d.getMonth()+1) +'-'+d.getDate()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
			console.log("New Date", d);
			return d.toDateString() +' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
			// return d.toUTCString();
		}
		$scope.deleteProductFromPrismaNote = function(){

			$api.delete('product/delete-suggestion?productId='+ $scope.product._id)
			.then(function(success){
				alert("Deletion of the product has been scheduled.")
			},function(err){
				alert("There seems to be some error. Please try again later.")
			})
		}

		$scope.convertToUpperAndSpace = function(str){
			var oldStr = str;
			str = str.toUpperCase();
			str = str.replace(" ","_");
			console.log(oldStr, "is converted to  ",str);
			return str;
		}
		$scope.getValueChangedIfSuggestionClick = function(sugvalue,prodfeildname){
			//for testing
			//sugvalue = "123456";
			console.log("prodfeildname",prodfeildname);	
			// Get the model
			var model = $parse(prodfeildname);
			console.log("old value",$scope[prodfeildname]);
			// Assigns a value to it
			model.assign($scope, sugvalue);			
			console.log("new value",$scope[prodfeildname]);
			
			//$scope[prodfeildname] = sugvalue;
			//console.log("value changed successfully ",prodfeildname,sugvalue);
		}

		$scope.getValueChangedIfSuggestionClickForIndex = function(sugvalue,prodfeildname,index){
			var finalprodfeildname = prodfeildname.replace("$index", index);
			//for testing
			//sugvalue = "123456";
			
			console.log("finalprodfeildname",prodfeildname , index);	
			// Get the model
			var model = $parse(finalprodfeildname);
			console.log("old value",$scope[finalprodfeildname]);
			// Assigns a value to it
			model.assign($scope, sugvalue);			
			console.log("new value",$scope[finalprodfeildname]);
			
			//$scope[prodfeildname] = sugvalue;
			//console.log("value changed successfully ",prodfeildname,sugvalue);
		}

		$scope.getClassForSuggestion = function(sug,prod){
			//console.log("getclasscalled" , sug , prod);
			//var colorCode = "redColorClass" ; // for testing
			var colorCode;

			//console.log("suggestion",sug);
			//console.log("product",prod);
			if((sug == undefined || sug == "") && (prod != undefined && prod != "") ){
				colorCode = "redColorClass"
			}
			else if((prod == undefined || prod == "") && (sug != undefined && sug != "")){
				colorCode="greenColorClass";
			}
			else if(sug != prod){
				colorCode="orangeColorClass";
			}
			else if(sug == prod){
				colorCode=" ";	
			}
			//console.log("color code selected because of suggestion and production selected ",colorCode ,sug,prod);

			return colorCode;
		}

		$scope.showProductChange = function(obj) {
			console.log("Suggestion Change", obj);
			$scope.showProduct = obj;
			console.log("ShowProduct final value = ", $scope.showProduct);

			// $scope.productCategory = value;
			// $scope.setProductCategory($scope.productCategory);
			// //$scope[changeLocation] = value;

		}

		$scope.dialColors = [{
			key: 'SILVER',
			hex: '#CACFD2'
		},
		{
			key: 'GREY',
			hex: '#4D5656'
		},
		{
			key: 'BLUE',
			hex: '#2980B9'
		},
		{
			key: 'WHITE',
			hex: '#FFF8DC'
		},
		{
			key: 'RED',
			hex: '#922B21'
		},
		{
			key: 'BLACK',
			hex: '#000000'
		},
		{
			key: 'ROSE_GOLD',
			hex: '#FFA07A'
		},
		{
			key: 'GOLD',
			hex: '#B8860B'
		},
		{
			key: 'MOTHER_OF_PEARL',
			hex: '#BC8F8F'
		},
		{
			key: 'BROWN',
			hex: '#8B4513'
		},
		{
			key: 'BEIGE',
			hex: '#CD853F'
		},
		{
			key: 'MOTHER_OF_PEARL_COLOURED',
			hex: '#BC8F8F'
		},
		{
			key: 'GOLDEN',
			hex: '#B8860B'
		},
		{
			key: 'GREEN',
			hex: '#228B22'
		},
		{
			key: 'PINK',
			hex: '#FF69B4'
		},
		{
			key: 'PURPLE',
			hex: '#663399'
		},
		{
			key: 'ORANGE',
			hex: '#FF5733'
		},
		{
			key: 'YELLOW',
			hex: '#FFD700'
		},
		{
			key: 'CREME',
			hex: '#F5DEB3'
		},
		{
			key: 'TAUPE',
			hex: '#D2B48C'
		},
		{
			key: 'BRASS',
			hex: '#CD853F'
		}];

		$scope.strapColors = [
		{
			key: 'SILVER',
			hex: '#CACFD2'
		},
		{
			key: 'GREY',
			hex: '#4D5656'
		},
		{
			key: 'BLUE',
			hex: '#2980B9'
		},
		{
			key: 'WHITE',
			hex: '#FFF8DC'
		},
		{
			key: 'RED',
			hex: '#922B21'
		},
		{
			key: 'BLACK',
			hex: '#000000'
		},
		{
			key: 'ROSE_GOLD',
			hex: '#FFA07A'
		},
		{
			key: 'GOLD',
			hex: '#B8860B'
		},
		{
			key: 'BROWN',
			hex: '#8B4513'
		},
		{
			key: 'BEIGE',
			hex: '#CD853F'
		},
		{
			key: 'GOLDEN',
			hex: '#B8860B'
		},
		{
			key: 'GREEN',
			hex: '#228B22'
		},
		{
			key: 'PINK',
			hex: '#FF69B4'
		},
		{
			key: 'PURPLE',
			hex: '#663399'
		},
		{
			key: 'ORANGE',
			hex: '#FF5733'
		},
		{
			key: 'YELLOW',
			hex: '#FFD700'
		},
		{
			key: 'CREME',
			hex: '#F5DEB3'
		},
		{
			key: 'TAUPE',
			hex: '#D2B48C'
		},
		{
			key: 'BRASS',
			hex: '#CD853F'
		}
		];

		$scope.$watch("product",function(){
			console.log("PRODUCT OBJECT CHANGED ",$scope.product);
		})
		// FOR COLLECTIONS
		$scope.searchCollections = function(str) {
			console.log("SEARCH COLLECTION CALLED");
			return searchItems(str, 'collections');
		}

		$scope.getCollections = function() {
			console.log("GETTING COLLECTIONS");
			return getItems('collections');
		};

		$scope.collectionSelected = function(selected){
			console.log("Selected", selected);
			console.log("Language = ",$scope.language);
			var collection = selected.originalObject;
			console.log("To be pushed in collections array ",collection);

			if(!$scope.product.collections){
				$scope.product.collections = [];
			}
			$scope.product.collections.push(collection);
			console.log("product", $scope.product);
		};

		// $scope.$watch('collectionSelected',function(){
		// 	console.log("COLLECTION SELECTED!!!" , $scope.collectionSelected);
		// 	if($scope.product.collections == undefined || $scope.product.collections.length==0)
		// 		$scope.product.collections = []

		// 	$scope.product.collections.push($scope.collectionSelected.title);
		// 	console.log("PROD COLLECTIONS = ", $scope.product.collections);
		// 	console.log("saveProduct", nameSlug, $scope.product, $stateParams.nameSlug);
		// })



		$scope.setProductCategory = function(category) {
			if(category == 'WATCH') {
				$scope.product.strap = $scope.product.jewel = {};
				$scope.product.watch = {case: {}, strap: {}, dial: {}};
			} else if(category == 'STRAP') {
				$scope.product.watch = $scope.product.jewel = {};
			} else if(category == 'JEWEL') {
				$scope.product.strap = $scope.product.watch = {};
			}
			$scope.product.category = category;
		}

		$scope.setCaseColor = function(color) {
			$scope.product.watch.case.color = [color.key];
		}
		$scope.setCaseColorInitially = function(color) {
			console.log("SET CASE COLOR CALLED FOR COLOR INITIALLY: ",color);
			if(color==null || color==undefined || color=="") return;

			color = color[0].toUpperCase();
			console.log("CASE COLOR SENT FOR INITIALLY SETTING : ",color)
			for(var i = 0; i < $scope.caseColors.length; i++){
				if($scope.caseColors[i].key == color){
					$scope.caseColor = $scope.caseColors[i];
					console.log("MATCH FOUND !",$scope.caseColor);
					$scope.setCaseColor(color);
					// $scope.product.watch.dial.color = color.key;
					break;
				}
				else{
					console.log("CASE COLOR");
					console.log($scope.caseColors[i].key +"!="+ color);
				}
			}
			if(!$scope.caseColor) console.log("NO CASECOLOR MATCHED TO SET INITIALLY")
		}
	$scope.setDialColor = function(color) {
		console.log("SET DIAL COLOR CALLED FOR COLOR : ",color)
		$scope.product.watch.dial.color = [color.key];
	}

	$scope.setDialColorInitially = function(color) {
		console.log("SET DIAL COLOR CALLED FOR COLOR INITIALLY: ",color);
		if(color==null || color==undefined || color=="") return;
		color = color[0].toUpperCase();
		console.log("COLOR SENT FOR INITIALLY SETTING : ",color)
		for(var i = 0; i < $scope.dialColors.length; i++){
			if($scope.dialColors[i].key == color){
				$scope.dialColor = $scope.dialColors[i];
				console.log("MATCH FOUND !",$scope.dialColor);
				$scope.setDialColor(color);
					// $scope.product.watch.dial.color = color.key;
					break;
				}
				else{
					console.log($scope.dialColors[i].key +"!="+ color);
				}
			}
			if(!$scope.dialColor) console.log("NO COLOR MATCHED TO SET INITIALLY")
		}


	$scope.setStrapColor = function(color) {
		if($scope.product.category == 'WATCH') {
			$scope.product.watch.strap.color = [color.key];
		} else if($scope.product.category == 'STRAP') {
			$scope.product.strap.color = [color.key];
		}
	}
	$scope.setStrapColorInitially = function(color) {
		console.log("SET STRAP COLOR CALLED FOR COLOR INITIALLY: ",color);
		if(color==null || color==undefined || color=="") return;

		color = color[0].toUpperCase();
		console.log("strap COLOR SENT FOR INITIALLY SETTING : ",color)
		for(var i = 0; i < $scope.strapColors.length; i++){
			if($scope.strapColors[i].key == color){
				$scope.strapColor = $scope.strapColors[i];
				console.log("MATCH FOUND !",$scope.strapColor);
				$scope.setStrapColor(color);
					// $scope.product.watch.dial.color = color.key;
					break;
				}
				else{
					console.log("Strap COLOR");
					console.log($scope.strapColors[i].key +"!="+ color);
				}
			}
			if(!$scope.caseColor) console.log("NO CASECOLOR MATCHED TO SET INITIALLY")
		}
	$scope.caseColors = $scope.jewelColors = Object.assign($scope.dialColors, $scope.strapColors);

	var getItems = function(type){
		$api.get(type)

		.then(function(response) {
			$scope[type] = response.data[type];
		})

		.catch(function(reason) {
			console.log(reason);
		});
	};

	$scope.getBrands = function() {
		return getItems('brands');
	};

	var searchItems = function(str, type) {
		var matches = [];

		$scope[type].forEach(function(item) {
			if(type != "brands")
			{
				if(
					(item[$rootScope.language] != undefined)
					&& (
						(item[$rootScope.language].name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
						||
						(item[$rootScope.language].nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
						)
					) {
					matches.push(item);
			}
		}
		else{
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
				||
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
				)
			{
				matches.push(item);
			}
		}
	});

		return matches;
	};

	$scope.searchBrands = function(str) {
		return searchItems(str, 'brands');
	}

	$scope.brandSelected = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.product.brand = brand;
	};

	$scope.addProductVariant = function() {
		/* TODO: remove collapse property on save */
		$scope.product.variants.push({collapse: true});
	}

	$scope.removeProductVariant = function(index) {
		$scope.product.variants.splice(index, 1);
	}
	$scope.customGetTargetAudience = function(){
		console.log("GETTING TARGET AUDIENCE from scope to native.");
		getTargetAudience();
	}
	var getTargetAudience = function() {
		console.log("SETTING TARGET AUDIENCE");
		if($scope.product.male) {
			if($scope.product.female) {
				if($scope.product.kids) {
					$scope.targetAudience = 'KIDS'
				} else {
					$scope.targetAudience = 'UNISEX'
				}
			} else {
				if($scope.product.kids) {
					$scope.targetAudience = 'BOYS'
				} else {
					$scope.targetAudience = 'GENTS'
				}
			}
		} else {
			if($scope.product.female) {
				if($scope.product.kids) {
					$scope.targetAudience = 'GIRLS'
				} else {
					$scope.targetAudience = 'LADIES'
				}
			}
		}
	}

	$scope.getSexAudience = function(prod) {
		var targetAudience = "";
		if(prod.male) {
			if(prod.female) {
				if(prod.kids) {
					targetAudience = 'KIDS'
				} else {
					targetAudience = 'UNISEX'
				}
			} else {
				if(prod.kids) {
					targetAudience = 'BOYS'
				} else {
					targetAudience = 'GENTS'
				}
			}
		} else {
			if(prod.female) {
				if(prod.kids) {
					targetAudience = 'GIRLS'
				} else {
					targetAudience = 'LADIES'
				}
			}
		}
		return targetAudience;
	}

	$scope.setGender = function(targetAudience) {
		switch(targetAudience) {
			case 'GENTS':
			$scope.product.male = true;
			$scope.product.female = false;
			$scope.product.kids = false;
			break;
			case 'LADIES':
			$scope.product.male = false;
			$scope.product.female = true;
			$scope.product.kids = false;
			break;
			case 'UNISEX':
			$scope.product.male = true;
			$scope.product.female = true;
			$scope.product.kids = false;
			break;
			case 'BOYS':
			$scope.product.male = true;
			$scope.product.female = false;
			$scope.product.kids = true;
			break;
			case 'GIRLS':
			$scope.product.male = false;
			$scope.product.female = true;
			$scope.product.kids = true;
			break;
			case 'KIDS':
			$scope.product.male = true;
			$scope.product.female = true;
			$scope.product.kids = true;
			break;
		}
	}

	$scope.saveProduct = function() {
		var nameSlug = $stateParams.nameSlug;

		if(nameSlug == null) {

			var product = $scope.product;

			for(var i = 0; i < product.variants.length; i++) {
				delete product.variants[i].collapse;
			}

			if(product.category == 'WATCH') {
				delete product.strap;
				delete product.jewel;
			}

			if(product.category == 'STRAP') {
				delete product.watch;
				delete product.jewel;
			}

			if(product.category == 'JEWEL') {
				delete product.strap;
				delete product.watch;
			}

			product.isVerified = true;

			product.verifyProduct = verifyMode || false;

			$api.post('products', product)
			.then(function(response) {
				nameSlug = response.product.nameSlug || response.product.en.nameSlug;
			})

			.catch(function(reason) {
				console.log(reason);
			})

		} else {

			/* Don't create a new product twice, but update the product instead. */
			var product = $scope.product;

				// product.collections = $scope.collectionSelected.title;
				console.log("PRODUCT TO BE ADDED  = ",product);
				// console.log("Collections = ",$scope.collectionSelected.title);
				// console.log("In $scope.Product =",$scope.product.collections);
				for(var i = 0; i < product.variants.length; i++) {
					delete product.variants[i].collapse;
				}

				if(product.category == 'WATCH') {
					delete product.strap;
					delete product.jewel;
				}

				if(product.category == 'STRAP') {
					delete product.watch;
					delete product.jewel;
				}

				if(product.category == 'JEWEL') {
					delete product.strap;
					delete product.watch;
				}

				product.isVerified = true;

				if(verifyMode == true) {

					$api.put('products/' + nameSlug, product)

					.then(function(response) {
						console.log("res", response);
						console.log('Product updated.');
						$state.go('admin.products');
					})

					.catch(function(reason) {
						console.log(reason);
					})
				} else {
					$api.put('products/' + nameSlug, product)

					.then(function(response) {
						console.log("res", response);
						console.log('Product updated.');
					})

					.catch(function(reason) {
						console.log(reason);
					})
				}
			}
		}

		if($stateParams.nameSlug) {
			$scope.currentMode='edit';
			$api.get('products/' + $stateParams.nameSlug)

			.then(function(response) {
				console.log('res from name slug',response);
				verifyMode = true;

				$scope.product = response.data.product;
				getTargetAudience();

				// console.log("SUGGESTIONS : ",$scope.product.suggestions[0]);
				$scope.productCategory = $scope.product.category;
				$scope.image = $scope.product.images;

				if(!$scope.product.nl) {
					$scope.product.nl = {views: 0}
				}
				if(!$scope.product.en) {
					$scope.product.en = {views: 0}
				}
				if(!$scope.product.de) {
					$scope.product.de = {views: 0}
				}
				if(!$scope.product.fr) {
					$scope.product.fr = {views: 0}
				}
				if(!$scope.product.es) {
					$scope.product.es = {views: 0}
				}

				$scope.setDialColorInitially($scope.product.watch.dial.color);
				$scope.setCaseColorInitially($scope.product.watch.case.color);
				$scope.setStrapColorInitially($scope.product.watch.strap.color);
				$scope.product.watch.case.material =  $scope.convertToUpperAndSpace($scope.product.watch.case.material);
				$scope.product.watch.case.glassMaterial =  $scope.convertToUpperAndSpace($scope.product.watch.case.glassMaterial);
				$scope.product.watch.case.shape =  $scope.convertToUpperAndSpace($scope.product.watch.case.shape);
				$scope.product.watch.strap.model =  $scope.convertToUpperAndSpace($scope.product.watch.strap.model);
				$scope.product.watch.strap.material =  $scope.convertToUpperAndSpace($scope.product.watch.strap.material);
				console.log($scope.product.watch.case.material,$scope.product.watch.case.glassMaterial,$scope.product.watch.case.shape)

			})

			.catch(function(reason) {
				console.log(reason);
			});
		} else {
			$scope.currentMode='add';
			$scope.targetAudience = 'MALE';
			$scope.product = {
				variants: [{collapse: false}],
				brand: {},
				category: 'WATCH',
				views: 0,
				nl: {views: 0},
				en: {views: 0},
				de: {views: 0},
				fr: {views: 0},
				es: {views: 0},
				strap: {},
				jewel: {},
				watch: {
					case: {},
					strap: {},
					dial: {}
				}
			};
		}

		console.log("CURRENT MODE = ",$scope.currentMode);
		$scope.getVersionValues = function(property){
			if(!$scope.product || !$scope.product.versions || $scope.product.versions.length == 0) return;
			var propertyValues = [];
			for(var i=0; i < $scope.product.versions.length; i++){
				var value = _.get($scope.product.versions[i].modifiedProperties[0], property);
				if(value){
					propertyValues.push(value);
				}
			}
			return propertyValues;
		}

		$scope.deleteProduct = function(){
			console.log($scope.product);
			$api.delete('products', {productId: $scope.product._id})
			.then(function(response) {
				$state.go('admin.products');
			})
			.catch(function(reason) {
				console.log(reason);
			})
		}
	}]);
