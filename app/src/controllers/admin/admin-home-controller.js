prismanoteApp.controller("adminHomeController", ['$scope', '$rootScope', '$retailer', '$api', '$translate', '$filter','$facebook','facebookService',
    function ($scope, $rootScope, $retailer, $api, $translate, $filter, $facebook,facebookService) {
        
        
        $scope.isLoggedIn = false;
        $scope.login = function() {
            console.log('called login');
          $facebook.login().then(function(data) {
              console.log('rrrrrrrr',data);
              var accessToken = data.authResponse;
              facebookService.getAdminAccessToken(accessToken).then(function(res){
                console.log('res>>>',res);
              })
            refresh();
          });
        }
        function refresh() {
            $facebook.api("/me").then( 
              function(response) {
                  console.log('welcome response facebook',response);
                //$scope.welcomeMsg = "Welcome " + response.name;
                $scope.isLoggedIn = true;
              },
              function(err) {
                console.log('welcome err facebook',err);
                  
                //$scope.welcomeMsg = "Please log in";
              });
          }
          
          refresh();
        
      
    }]);
    prismanoteApp.factory('facebookService',['$http', function($http) {
        return {
            getAdminAccessToken: function(obj) {
                console.log('data for post',obj);
                return $http({
                    headers: { 'Content-Type': 'application/json' },
                    url: '/api/get-admin-fbToken',
                    method: "POST",
                    data: obj,
                })
               .then(function(res){
                   console.log('res from long live token',res);
                   return res.data;
               })
            }
        }
    }]);


