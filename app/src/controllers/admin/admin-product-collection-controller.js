prismanoteApp.controller('adminProductCollectionsController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload', '$http', 'FileSaver', 
function($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt, Upload, $http, FileSaver) {

	$scope.loading = false;
    $scope.isVerified = 'all';

    // Verification Filter
	$scope.verificationFilter = function(x){
		return ($scope.isVerified == 'all') ? true : x.isVerified == ($scope.isVerified === 'true');
	}


    $scope.getCollections = function() {
        $scope.loading = true;
        $api.get('collections')
        
		.then(function(response) {
			$scope.collections = response.data.collections;
			$scope.loading = false;
		})
		
		.catch(function(reason) {
			$scope.loading = false;
			console.log(reason);
		});
	}
	
	$scope.closeAlert = function(){
		$scope.alert = null;
	}

	$scope.getCollection = function() {
        var filter = {_id: $stateParams.id};
        $api.get('collections/', {filter: filter})
		
		.then(function(response) {
			$scope.collection = response.data.collections[0];
			if(!$scope.collection.isVerified){
				$scope.alert = {
					type: 'warning',
					msg: "Deze collectie is nog niet goedgekeurd"
				}
			}
		})
		
		.catch(function(reason) {
			console.log(reason);
		});
    };

    $scope.searchBrands = function(str) {
		var matches = [];
	
		$scope.brands.forEach(function(item) {
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || 
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

		return matches;
	};

    $scope.brandSelected = function(selected){
        $scope.selectedBrand = selected.title;
        $scope.brandSlug = selected.originalObject.nameSlug;
	};

	$scope.addCategory = function(cat){
		$scope.collection.categories.push(cat);
	}

	$scope.deleteCategory = function(index){
		$scope.collection.categories.splice(index, 1);
	}

	$scope.cancel = function(){
		$state.go('admin.product-collections');
	}

	$scope.saveCollection = function(){
		$api.put('collections', {collection: $scope.collection})
			.then(function(response) {
				$state.go('admin.product-collections');
			})
			.catch(function(reason) {
				console.log(reason);
				$scope.alert = {
					type: 'danger',
					msg: reason
				}
			})
	}
	
	$scope.deleteCollection = function(){
		$api.delete('delete-collection/' + $scope.collection._id)
			
		$state.go('admin.product-collections');
	}

}])