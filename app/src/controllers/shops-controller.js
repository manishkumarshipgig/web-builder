prismanoteApp.controller('shopsController', ['$scope', '$rootScope', '$location', '$api', function($scope,	$rootScope, $location, $api) {

	$rootScope.breadcrumb = ['Juweliers'];
	
	$scope.getShopList = function() {
		$api.get('shops')
		
			.then(function(response) {
				$scope.loading = false;
				$scope.shops = response.data.shops;
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	}

}]);