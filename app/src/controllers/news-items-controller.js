prismanoteApp.controller('newsItemsController', ['$scope', '$rootScope', '$api', function($scope,	$rootScope, $api) {

	$rootScope.breadcrumb = ['Nieuws'];

	$api.get('news', {'sort': {'publicationDate': 'desc'}, 'limit': 12})
	
		.then(function(response) {
			$scope.newsItems = response.data.newsItems;
		})
		
		.catch(function(reason) {
			console.log(reason);
		});
}]);