prismanoteApp.controller('welcomeRetailerController', ['$scope', '$rootScope', '$location', '$api', '$state', '$stateParams',
function($scope, $rootScope, $location, $api, $state, $stateParams) {
    $scope.loading = true;
    $scope.addToShop = false;
    $scope.readyToGo = false;

    $scope.url = window.location.pathname;

    $scope.loadPage = function(){
        
        $api.post('load-retailer-welcome', {hashedData: $stateParams.hashedData})
        	.then(function(response) {
                $scope.data = response.data.data;
                $scope.campaign = response.data.campaign;

                if(response.status == 207){
                    $scope.loading = false;
                    $scope.accessDenied = true;
                    $scope.username = $scope.data.email;
                    $scope.newUser = {
                        email: $scope.data.email
                    }
                    //not logged in, we are done yet
                }else if(response.status == 203){
                    if($scope.user && $scope.user.role != 'retailer'){
                        $state.transitionTo("layout.access-denied" , {to: 'layout.welcome-retailer'});
                    }else{
                        $scope.loading = false;
                        $scope.differentUser = true;
                    }
                }else{
                    //200 OK
                    $scope.checkBeforeExecute();
                    $scope.addToShop = true;
                    $scope.loading = false;
                }
            })
            .catch(function(reason) {
                console.log(reason);
            })
    }

    $scope.checkBeforeExecute = function(){
        if($scope.user.facebook && $scope.user.facebook.postTo == 'page'){
            $scope.execute();
        }else{
            if($scope.user.facebook && $scope.user.facebook.profileId){
                $scope.getFacebookPages();
            }
        }
    }

    $scope.continue = function(){
        $scope.addToShop = true;
        $scope.differentUser = false;
        $scope.checkBeforeExecute();
        $scope.readyToGo = true;
    }


    $scope.getFacebookPages = function(){
        if($scope.user && $scope.user.facebook && $scope.user.facebook.profileId){
            $api.get('facebook/get-pages')
            
            .then(function(response) {
                $scope.pages = response.data.pages;
            })
            .catch(function(reason) {
                console.log(reason);
            })
        }
    }

    $scope.setFacebookPage = function(selectedPage){
        $scope.user.facebook.postTo = 'page';
        $scope.user.facebook.pageToken = selectedPage.access_token;
        $scope.user.facebook.pageId = selectedPage.id
        $scope.user.facebook.pageName = selectedPage.name;

        $scope.saveUser();
        $scope.readyToGo = true;
    }

    $scope.saveUser = function(){
        $api.put('user/' + $scope.user._id, {user: $scope.user})
        .then(function() {

        })
        .catch(function(reason) {
            console.log(reason);
        });
    }

    $scope.execute = function(){
        console.log("execute");
        $api.post('add-campaign-to-shop', {campaign: $scope.campaign._id})
        	.then(function(response) {
                console.log("ex res", response);
               $state.go('retailer.campaign', {nameSlug:$scope.campaign.nameSlug})
            })
            .catch(function(reason) {
                console.log("ex reas", reason);
                if(reason.status == 206){
                    $state.go('retailer.campaign', {nameSlug:$scope.campaign.nameSlug})
                }
                $scope.alert = {
                    type: "warning",
                    msg: reason
                }
                $scope.readyToGo = false;
            })
    }
}]);