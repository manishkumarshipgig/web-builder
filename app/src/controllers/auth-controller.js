prismanoteApp.controller("authController", ['$scope', '$rootScope', '$uibModal', '$stateParams', '$api', '$http', '$state', 
	function($scope, $rootScope, $uibModal, $stateParams, $api, $http, $state) {
		$scope.error = false;
		
		$scope.openLoginModal = function(mode, redirect, data) {

			if(!mode || mode == '' || $rootScope.user){
				var mode = 'login';
			}
			if(redirect && redirect == 'reload'){
				var reloadState = true;
				redirect = false;
			}else if(!redirect){
				if(!redirect){
					redirect = false;
				}
			}					
			

			if(!data){
				data = null;
			}

			var tab;
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/login-modal.html',
				controller: 'loginModalController',
				resolve: {
					mode: function() {
						return mode;
					},
					tab: function(){
						return tab;
					},
					redirect: function(){
						return redirect;
					},
					data: function(){
						return data;
					}
				}
			});
			modalInstance.result.then(function() {
				if($stateParams.to){
					$state.go($stateParams.to);
				}else if(reloadState){
					$state.reload();
				}
				
			}, function() {
				//Modal is dismissed
			})
		};

		$scope.activateAccount = function() {
			// $api.post('activate-user', {id: $stateParams.id, code: $stateParams.code})
			
			// 	.then(function(response) {
			// 		console.log('Success: ' + response);
			// 	})
				
			// 	.catch(function(reason) {
			// 		console.log(reason);
			// 	});

			$http.post('/api/activate-user', {id: $stateParams.id, code: $stateParams.code})
				.then(function successCallback (response) {
					$scope.message = response.data.message;
					$rootScope.user = response.data.user;
				}, function errorCallback (response){
					console.log("Error: ", response);
					$scope.message = response.data.message || response.statusText;
					$scope.error = true;
				})
		}

		$scope.resetPassword = function(activate){
			
			$scope.alert = null;
			console.log("resetPassword", $stateParams);
			if($scope.resetPasswordForm.$invalid){
				$scope.alert = {
					type: "danger",
					msg: "Controleer invoer"
				}
				return;
			}
			$scope.data.id = $stateParams.id;
			$scope.data.code = $stateParams.code;
			$scope.data.activate = activate;
			$api.post('change-password', {data: $scope.data})
			.then(function(response){
				console.log("resp", response);
				$scope.alert = {
					type: "success",
					msg: response.data.message
				}
			})
			.catch(function(reason){
				$scope.alert = {
					type: 'danger',
					msg: reason
				}
			})
		}

		$scope.closeAlert = function(){
			$scope.alert = null;
		}

		$scope.logout = function(redirect) {
			if(!redirect){
				redirect = false;
			}
			$api.get('logout')
				.then(function() {
					$scope.alert = null;
					$rootScope.user = null;
					if(redirect){
						$state.go('layout.logout');
					}
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		};
}]);