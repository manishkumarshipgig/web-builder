prismanoteApp.controller('retailerNewsController', ['$scope', '$api', '$stateParams', '$uibModal', '$state', 'prompt', 'Upload','$retailer', '$rootScope',
	function($scope, $api, $stateParams, $uibModal, $state, prompt, Upload, $retailer, $rootScope) {

	$scope.getNewsItems = function() {
		$api.get('news', {'sort': {'startDate': 'desc'}, 'limit': 24})
		
			.then(function(response) {
				$scope.news = response.data.newsItems;
				$scope.reloadShop();
			})
			
			.catch(function(reason) {
				console.log(reason);
			})
	};

	$scope.closeAlert = function() {
		$scope.alert = 0
	}

    $scope.openPreviewNewsItemModal = function(newsItem){
		console.log("[preview modal geopend]")
        var modalInstance = $uibModal.open({
            templateUrl: '../views/modal/preview-newsItem-modal.html',
            controller: 'previewNewsItemModalController',
            size: 'lg',
            resolve: {
                newsItem: function() {
                    return newsItem;
                }
            }
        });

        modalInstance.result.then(function(result){
            if(result){
                $scope.currentShop.news.push(newsItem);
				$scope.updateShop();
				$scope.reloadShop();
				$scope.activeTab=1;                
            }
        }, function(){
            //dismissed
        })
	}

	$scope.updateShop = function(){
		$retailer.setShop($rootScope.selectedShop, $scope.currentShop);
	}

	$scope.reloadShop = function(){
		$retailer.getShop()
		.then(function(shop) {	
			$scope.currentShop = shop;	
			console.log("shop", $scope.currentShop.news);

		})
		.catch(function(reason) {
			console.log(reason);
		});
	}

	$scope.$on('changeSelectedShop', function() {
		$scope.reloadShop();
		$scope.activeTab =1;
	});

	$scope.activeTab =1;

}]);