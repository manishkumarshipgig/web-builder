prismanoteApp.controller('retailerCrmUserItemController', ['$scope', '$rootScope', '$api', '$stateParams', '$uibModal', '$state', 'FileSaver','prompt',  '$http',
function($scope, $rootScope, $api, $stateParams, $uibModal, $state, prompt,FileSaver, $http) {
  $scope.showDownloadLink = false;
  $scope.flag =0;
  $scope.disabledUserDetails = true;
  $scope.isRepairFinished = false;
  $scope.selectedBrand ='';
  $scope.brands=[];
  //$scope.crmUser={};
  console.log("rootScope.user",$rootScope.user);
  console.log("item",$scope.item);
  $scope.repairFinishFinalDate ;
  console.log('repairFinishFinalDate', $scope.repairFinishFinalDate);
    $scope.loadUser = function() {
        console.log("$stateParams.crmUserId", $stateParams.crmUserId);

        $api.get('crmuser/' + $stateParams.crmUserId)
        
            .then(function(response) {
                console.log("response.data.crmUser", response.data.crmUser);
                $scope.crmUser = response.data.crmUser;

                console.log("$scope.crmUser",$scope.crmUser);
                $scope.validAddress = false;
                for(var i = 0; i < $scope.crmUser.items.length; i++){
                    console.log("$stateParams.crmUserItemId", $stateParams.crmUserItemId);
                    if($stateParams.crmUserItemId == $scope.crmUser.items[i]._id){
                        $scope.item = $scope.crmUser.items[i];
                        console.log("$scope.selectedBrand.title",$scope.selectedBrand.title);
                        $scope.item.brandName =  $scope.selectedBrand.title;

                    if($scope.item.address.street){
                      $scope.validAddress = true;
                        }


                    console.log("$scope.item",$scope.item);

                    if($scope.item.status=='readyToPickUp'){
                        $scope.isRepairFinished = true;
                        $scope.item.repairFinishDateFinal =new Date().toLocaleDateString();
                          }
                        console.log(" $scope.item.repairFinishDateFinal",  $scope.item.repairFinishDateFinal);
                    }
                }
                //$scope.getSocialPortal();
            })
            
            .catch(function(reason) {
                console.log(reason);
            });
    }

    $scope.showComment = true;
    $scope.showSalesData = false;
    $scope.showPurchaseData = true;

    $scope.showCommentFunction = function() {
        if(!$scope.showComment){
            $scope.showComment = true;
        }else{
            $scope.showComment = false;
        }
    }

    $scope.showSalesDataFunction = function() {
        if(!$scope.showSalesData){
            $scope.showSalesData = true;
            $scope.showPurchaseData = false;
        }else{
            $scope.showSalesData = false;
            $scope.showPurchaseData = true;
        }
    }

    $scope.showPurchaseDataFunction = function() {
        if(!$scope.showPurchaseData){
            $scope.showPurchaseData = true;
            $scope.showSalesData = false;
        }else{
            $scope.showPurchaseData = false;
            $scope.showSalesData = true;
        }
    }

    $scope.showAllFields = function (){
        $scope.showPurchaseData = false;
        $scope.showSalesData = true;
        $scope.showComment = true;
    }

    $scope.print = function(item,crmUser) {
        $scope.flag= 1;
        // window.print();

        console.log("$rootScope.user.shops.nameSlug",$rootScope.user.shops["0"]);
        $http({
            method: 'POST',
            url: '/api/generate-pdf',
            data: {
                items: $scope.item,
                shop_slug:$rootScope.user.shops["0"],
                crmUser:crmUser,
                showSalesData:$scope.showSalesData,
                showComment:$scope.showComment,
                showPurchaseData:$scope.showPurchaseData,
                flag: $scope.flag
            },
            responseType : 'arraybuffer'
        })
        .then(function(response){
            console.log("response",response);
            var file = new Blob([response.data], {type: 'application/pdf'});
            $scope.url = window.URL.createObjectURL(file);
            var link=document.createElement('a');
            link.href = $scope.url;
            link.download = $scope.url.substr($scope.url.lastIndexOf('/') + 1);
            link.click();

            console.log("url", $scope.url);
            var rec = document.getElementById('receipt');
            rec.style = "display: inline;margin-left:10px;";
            $scope.showDownloadLink = true;
        }, function(response){
            console.log("ERROR", response);
        })
          
        
    }
    $scope.printCustomer = function(item,crmUser) {
        // window.print();
        $scope.flag =0;
        console.log("$scope.item",$scope.item);
        console.log("$scope.flag",$scope.flag);
        console.log("$rootScope.user.shops.nameSlug",$rootScope.user.shops["0"]);
        $http({
            method: 'POST',
            url: '/api/generate-pdf',
            data: {
                items: $scope.item,
                shop_slug:$rootScope.user.shops["0"],
                crmUser:crmUser,
                showSalesData:$scope.showSalesData,
                showComment:$scope.showComment,
                showPurchaseData:$scope.showPurchaseData,
                flag: $scope.flag
            },
            responseType : 'arraybuffer'
        })
        .then(function(response){
            console.log("response",response);
            var file = new Blob([response.data], {type: 'application/pdf'});
            $scope.url = window.URL.createObjectURL(file);
            var link=document.createElement('a');
            link.href = $scope.url;
            link.download = $scope.url.substr($scope.url.lastIndexOf('/') + 1);
            link.click();

            console.log("url", $scope.url);
            var rec = document.getElementById('receipt');
            rec.style = "display: inline;margin-left:10px;";
            $scope.showDownloadLink = true;
        }, function(response){
            console.log("ERROR", response);
        })
          
        
    }
    $scope.printPdf = function(){
            if($scope.showSalesData && $scope.showComment){
                var printItem =  $scope.item;

            }
            if($scope.showSalesData && !$scope.showComment){
                console.log("Remove comments");
                var printItem =  $scope.item;
            }
            if($scope.showPurchaseData && $scope.showComment){
                console.log("Repair Price and comment fields and removed customer details");
 
             }
            if($scope.showPurchaseData && !$scope.showComment){
               console.log("Repair Price and  remove comment fields and removed customer details");

            }
            if(!$scope.showPurchaseData && !$scope.showComment){
                console.log("Repair Price and  customer details and  remove comment fields  ");
 
             }  
    }

	$scope.updateCrmUser = function () { // contains data.customer._id
        $scope.crmUser.items.brandName = $scope.selectedBrand.title;
        var crmUser = $scope.crmUser;

        console.log("crmUser", crmUser);

        $api.put('crmusers', {crmUser: crmUser})
        
            .then(function(response) {
                console.log("response", response);
                //$state.reload();
                $scope.loadUser();
            })
            
            .catch(function(reason) {
                console.log(reason);
            });
    }
    $scope.getBrandList = function() {
        $scope.brandsList=[];
        $api.get('brands',{filter: {isVerified:true}, limit: 0})
            .then(function(response) {
                console.log("response",response);
                // $scope.loading = false;

                $scope.verifiedBrands = response.data.brands;
                for(var i=0;i<$scope.verifiedBrands.length;i++){
                   $scope.brands.push( $scope.verifiedBrands[i].name);
                }
               
                console.log("$scope.brands",$scope.brands);
                for(var j=0;j<$scope.brands.length;j++){
                    $scope.getEachBrand={};
                    $scope.getEachBrand.name=$scope.brands[j];
                    $scope.brandsList.push( $scope.getEachBrand);
                    // console.log("$scope.sdd",$scope.sdd);
                    
                   
                 }
                 console.log("$scope.brandsd",$scope.brandsList);
            })
            .catch(function(reason) {
                console.log(reason);
            });
}

}])