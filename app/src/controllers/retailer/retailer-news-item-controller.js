prismanoteApp.controller('retailerNewsItemController', ['$scope', '$rootScope', '$api', '$state', 'Upload', '$stateParams', '$retailer', 'prompt',
	function($scope, $rootScope, $api, $state, Upload, $stateParams, $retailer, prompt) {

	$scope.newItem = function(){
		$scope.newsItem = {
			brand: {},
			author: {
				name: $rootScope.user.firstName + ($rootScope.user.lastNamePrefix ? ' ' + $rootScope.user.lastNamePrefix + ' ' : ' ') + $rootScope.user.lastName
			},
			isPublished: true,
			publicationDate: new Date()
		};
	}

	$scope.getNewsItem = function() {
		console.log("getNewsItem", $stateParams.nameSlug, $rootScope.currentShop._id);
		$api.get('shop/news', {
			newsItemSlug: $stateParams.nameSlug, 
			shopId: $rootScope.currentShop._id
		})
			.then(function(response) {
				console.log("response", response);
				$scope.newsItem = response.data.newsItem;
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

	var getItems = function(type){
		$api.get(type)
		
			.then(function(response) {
				$scope[type] = response.data[type];
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	};

	$scope.getBrands = function() {
		return getItems('brands');
	};

	var searchItems = function(str, type) {
		var matches = [];
	
		$scope[type].forEach(function(item) {
			if(
				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) || 
				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(item);
			}
		});

		return matches;
	};

	$scope.searchBrands = function(str) {
		return searchItems(str, 'brands');
	}

	$scope.brandSelected = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.newsItem.brand = brand;
	};

	$scope.updateShop = function(){
		$retailer.setShop($rootScope.selectedShop, $scope.currentShop);
	}

	$scope.uploadOtherPhoto = function(newsItem){
		if(newsItem.photo === "") {
			} else {
			$scope.upload = true;
				Upload.upload({
					url: 'api/news/news-photo-upload',
					data: {
							newsItemId: newsItem._id,
							file: $scope.newsItem.photo //file input field
					}
				})
				.then(function (res){
					//wanneer uploaden gelukt
					console.log("uploaden gelukt")
					$scope.newsItem = null;
					$scope.alert = {
						type: "success",
						msg: "Uw gegevens zijn verzonden!"
					}
				}, function (res){
					//wanneer uploaden mislukt
					console.log("Error: ", res.status);
					$scope.alert = {
						type: "danger",
						msg: "upload error: " + res.status
						}
				}, function(evt){
					// //tijdens upload
					// var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					// $scope.uploadProgress = progressPercentage;
					// console.log("Progress: " + progressPercentage + '%' + evt.config.data);
				});
				$scope.upload = false;
		}
	};

	$scope.makeSlug = function(){
		if($scope.newsItem.name){
			$api.get('shop/newsItemSlug', {name: $scope.newsItem.name})
			.then(function(response) {
				$scope.newsItem.nameSlug = response.data.nameSlug;
			})
			.catch(function(reason) {
				console.log(reason);
			})
		}
	}

	$scope.reloadShop = function(callback){
		$retailer.getShop()
		.then(function(shop) {	
			$scope.currentShop = shop;	
			if(callback){
				return callback(shop);
			}
		})
		.catch(function(reason) {
			console.log(reason);
		});
	}


	$scope.addNewsItem = function(){
		//Upload the selected file and save the item to the current shop
		$scope.reloadShop();

		$scope.image = $scope.newsItem.photo;
		$scope.currentShop.news.push($scope.newsItem);

		$scope.updateShop();
		$scope.reloadShop(function(shop){
			$scope.currentShop = shop;

			$scope.newsItem = $scope.currentShop.news[$scope.currentShop.news.length-1];

			uploadImage();

			$state.go('retailer.news');
		});
	}

	function uploadImage(){
		console.log("uploadImage", $scope.newsItem);
		Upload.upload({
			url: 'api/shop/news-image',
			data: {
				file: $scope.image,
				newsItemId: $scope.newsItem._id,
				shopId: $scope.currentShop._id,
			}
		})
		.then(function (res){
			console.log('res!?', res)
			//wanneer uploaden gelukt
			$scope.newsItem.images = [{src: res.data.file}];

		}, function (res){
			//wanneer uploaden mislukt
			console.log("Error: ", res.status);
			$scope.alert = {
				type: "danger",
				msg: "upload error: " + res.status
			}
		}, function(evt){
				//tijdens upload
				// var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				// $scope.uploadProgress = progressPercentage;
				// console.log("Progress: " + progressPercentage + '%' + evt.config.data);
		});
		$scope.upload = false;
	}

	$scope.closeAlert = function() {
		$scope.alert = 0
	}

	$scope.updateNewsItem = function(){
	
		if($scope.newsItem.photo){
			//Als er een foto is geupload moet deze worden gewijzigd
			Upload.upload({
				url: 'api/shop/news-image',
				data: {
					file: $scope.newsItem.photo,
					newsItemId: $scope.newsItem._id,
					shopId: $rootScope.currentShop._id,
				}
			})
			.then(function (res){
				//photo uploaded
			}, function (res){
				//wanneer uploaden mislukt
				console.log("Error: ", res.status);
				$scope.alert = {
					type: "danger",
					msg: "upload error: " + res.status
				}
			}, function(evt){
				//progress
			});
		}
		
		delete $scope.newsItem.photo;

		var data = {
			newsItem: $scope.newsItem,
			shopId: $rootScope.currentShop._id
		}

		$api.put('shop/news', {data: data})
			.then(function(response) {
				$scope.alert = {
					type: "success",
					msg: "Nieuwsbericht is aangepast!"
				}
				$state.go('retailer.news');
			})
			.catch(function(reason) {
				console.log("update news failed", reason);
			})
		.catch(function(reason) {
			console.log("upload failed: ", reason);
		})
	}

	$scope.closeAlert = function() {
		$scope.alert = 0
	}

	$scope.$on('changeSelectedShop', function() {
		$scope.reloadShop();
	});

	$scope.removeNewsItem = function(){
		console.log("remove", $scope.currentShop.news, $scope.newsItem);
		$scope.reloadShop();
		prompt({
			title: 'Bericht verwijderen',
			message: 'Weet u zeker dat u dit bericht wilt verwijderen?'
		}).then(function() {

			var index = _.findIndex($rootScope.currentShop.news, {'_id': $scope.newsItem._id});
			console.log("index", index);
			if(index >= 0){
				console.log("Delete");
				$scope.currentShop.news.splice(index,1);
				$scope.updateShop();	
			}
			$state.go('retailer.news');
		})	
	}
}]);