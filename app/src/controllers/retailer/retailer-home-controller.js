prismanoteApp.controller("retailerHomeController", ['$scope', '$rootScope', '$retailer', '$api', '$translate', '$filter', '$countr',
    function ($scope, $rootScope, $retailer, $api, $translate, $filter, $countr) {
       
        $scope.getShop = function (nameSlug) {
            $retailer.getShop(nameSlug)

                .then(function (shop) {
                    $scope.currentShop = shop;
                })

                .catch(function (reason) {
                    console.log(reason);
                });
        }   

        //check social portal
        $scope.checkSocialPortal = function () {
            $api.get('check-social-portal', { userId: $rootScope.user._id })
                .then(function (response) {
                    $scope.user.socialPortal = response.data.socialPortal
                    $scope.getOpenTasksCount();
                },function(err){
                    $scope.alert = {
                        type: "danger",
                        msg: "No social portal has been found for this user. Please contact support."
                    }
                })
        }
        $scope.checkSocialPortal();

        $scope.getOpenTasksCount = function () {
            $scope.openTasks = 0;
            if ($scope.user.taskList) {
                for (var i = 0; i < $scope.user.socialPortal.taskList.length; i++) {
                    if (!$scope.user.socialPortal.taskList[i].completed) {
                        $scope.openTasks++;
                    }
                }
            }
        }

        $scope.getOpenOrdersCount = function () {
            $scope.openOrders = 0;
            $retailer.getShop()
                .then(function (shop) {
                    $scope.currentShop = shop;

                    $api.get('retailer-order-list', { nameSlug: $scope.currentShop.nameSlug, status: 'Paid' })
                        .then(function (response) {
                            $scope.openOrders = response.data.orders.length;
                        })
                        .catch(function (reason) {
                            console.log(reason);

                        });
                })
                .catch(function (reason) {
                    console.log(reason);
                });

        }
        if ($scope.user.shops && $scope.user.shops.length > 0) {
            $scope.getOpenOrdersCount();
        }


        $scope.$on('changeSelectedShop', function () {
            $scope.getOpenOrdersCount();
        });

        $scope.generaltourOnclick = function () {
            localStorage.setItem("type", "generaltour");
            gentour.restart();
        }

        $scope.marketingtourOnclick = function () {
            localStorage.setItem("type", "marketingtour");
            marketingTour.restart();
        }

        $scope.webshoptourOnclick = function () {
            localStorage.setItem("type", "webshoptour");
            webshopTour.restart();
        }

        $scope.emailmarketingtourOnclick = function () {
            localStorage.setItem("type", "emailmarketingtour");
            localStorage.setItem("nameSlug", $rootScope.currentShop.nameSlug);
            console.log($rootScope.currentShop);
            console.log($rootScope.currentShop.nameSlug);
            emailMarketingTour.restart();
        }



		$scope.openCountrSignupModal = function(){
            $countr.signupModal()
            	.then(function(response) {
                    console.log("response");
                })
                .catch(function(reason) {
                    console.error(reason);
                })
		}

    }]);


