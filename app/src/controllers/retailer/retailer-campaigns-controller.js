prismanoteApp.controller('retailerCampaignsController', ['$scope', '$rootScope', '$stateParams', '$state', '$q', '$retailer', '$api', '$uibModal', 'prompt', '$window', 'campaignFact',
	function ($scope, $rootScope, $stateParams, $state, $q, $retailer, $api, $uibModal, prompt, $window, campaignFact) {

		$scope.facebookId = null;
		$scope.activeTab = 4;
		$scope.orderByDate = function (log) {
			var date = new Date(log.date);
			return date;
		}
		$scope.backTOPromotion = function () {
			$scope.activeTab = 4;
		}
		$scope.sendRequest = false;
		$scope.sendFbRequest = function () {
			$scope.sendRequest = true;
			window.open('https://facebook.com/ajeetweb', '_blank');
		}
		$scope.assignPermission = function () {
			campaignFact.updatePermission().then(function (res) {
				console.log('res from updte permission', res);

			})
		}

		$scope.getSocialPortal = function () {
			if ($scope.user.isMarketingUser && $scope.user.isMarketingUser == false) {
				var idUser = $scope.user;
			} else {
				var idUser = $scope.user;
			}
			$api.get('user-social-portal', { userId: idUser._id })

				.then(function (response) {
					$scope.user.socialPortal = response.data.result;
					// if($scope.user.isMarketingUser && $scope.user.isMarketingUser == true){
					// 	$scope.user.socialPortal.campaigns = $scope.user.socialPortal.campaigns.filter(function(val){
					// 		return val.credit == true;
					// 	})
					// }else{

					// }

					$scope.loading = false;
					$scope.getPortalCampaigns();
					$scope.getOpenTasksCount();

					// get unique brands and contributions totals for retailer/suppliers
					var socialBrands = $scope.user.socialPortal.brands
					var uniqueBrands = socialBrands.reduce(function (a, b) {
						if (a.indexOf(b) < 0) a.push(b);
						return a;
					}, []);
					//console.log(uniqueBrands, socialBrands)

					$scope.uniqueBrands = uniqueBrands;
					//console.log("$scope.uniqueBrands", $scope.uniqueBrands);

					$scope.brandsWithCampaigns = [];
					var counter = 0;
					for (var i = 0; i < $scope.uniqueBrands.length; i++) {
						for (var j = 0; j < $scope.user.socialPortal.campaigns.length; j++) {
							//console.log("$scope.uniqueBrands[i].name", $scope.uniqueBrands[i].name);
							//console.log("$scope.user.socialPortal.campaigns[j].brand.name", $scope.user.socialPortal.campaigns[j].brand.name);
							if ($scope.uniqueBrands[i].name == $scope.user.socialPortal.campaigns[j].brand.name || $scope.uniqueBrands[i].credit) {
								//console.log("PUSH!");
								var brandName = {
									name: $scope.uniqueBrands[i].name,
									credit: $scope.uniqueBrands[i].credit,
									totalAllAmounts: 0,
									allAmounts: [],
									brandContribution: 0,
									retailerContribution: 0
								}
								$scope.brandsWithCampaigns.push(brandName);
								break
							}
						}
						counter++
						if (counter == $scope.uniqueBrands.length) {
							//console.log("lijst is gevuld: brandsWithCampaigns", $scope.brandsWithCampaigns);
							for (var k = 0; k < $scope.brandsWithCampaigns.length; k++) {
								for (var l = 0; l < $scope.user.socialPortal.campaigns.length; l++) {
									if ($scope.brandsWithCampaigns[k].name == $scope.user.socialPortal.campaigns[l].brand.name) {
										//console.log("$scope.user.socialPortal.campaigns[l].tasks[0].contribution", $scope.user.socialPortal.campaigns[l].tasks[0]);
										if ($scope.user.socialPortal.campaigns[l].tasks[0].contribution) {
											$scope.brandsWithCampaigns[k].allAmounts.push($scope.user.socialPortal.campaigns[l].tasks[0].contribution.maxAmount)
											$scope.brandsWithCampaigns[k].totalAllAmounts = $scope.brandsWithCampaigns[k].totalAllAmounts + $scope.user.socialPortal.campaigns[l].tasks[0].contribution.maxAmount;

											$scope.brandsWithCampaigns[k].brandContribution = $scope.brandsWithCampaigns[k].brandContribution + ($scope.user.socialPortal.campaigns[l].tasks[0].contribution.maxAmount * ($scope.user.socialPortal.campaigns[l].tasks[0].contribution.percent / 100));
										}
										$scope.brandsWithCampaigns[k].retailerContribution = $scope.brandsWithCampaigns[k].totalAllAmounts - $scope.brandsWithCampaigns[k].brandContribution;
									}
								}
							}
						}
					}


				})
				.catch(function (reason) {
					$scope.loading = false;
					console.log("reason", reason);

					var diff = Math.abs(new Date() - new Date($scope.user.creationDate));
					var minutes = Math.floor(diff / 1000) / 60;

					if (minutes <= 5) {
						$scope.alert = {
							type: "warning",
							msg: "Klik opnieuw op de link in de e-mail of herlaad de vorige pagina. (Deze pagina kunt u nu sluiten.)"
						}
					} else {
						$scope.alert = {
							type: "danger",
							msg: "Er is geen Social Portal gevonden voor deze gebruiker. Neem contact op met support."
						}
					}


				})
		}

		$scope.getPortalCampaigns = function () {
			$scope.activeTab = 4;
			$scope.campaigns = new Array;
			$scope.updates = new Array;
			$api.get('portal-campaigns', { portalId: $scope.user.socialPortal._id })
				.then(function (response) {
					for (var i = 0; i < response.data.campaigns.length; i++) {
						var campaign = response.data.campaigns[i];
						if (campaign.update == true) {
							$scope.updates.push(campaign);
						} else {
							$scope.campaigns.push(campaign);
						}
					}
				})
				.catch(function (reason) {
					console.log("REASON", reason);
				})
		}

		$scope.getFacebookPages = function () {
			$api.get('facebook/get-pages')

				.then(function (response) {
					$scope.pages = response.data.pages;
				})
				.catch(function (reason) {
					console.log(reason);
				})
		}

		$scope.getOpenTasksCount = function () {
			$scope.openTasks = 0;
			for (var i = 0; i < $scope.user.socialPortal.taskList.length; i++) {
				if (!$scope.user.socialPortal.taskList[i].completed) {
					$scope.openTasks++;
				}
			}
		}

		$scope.saveUser = function () {
			$api.put('user/' + $scope.user._id, { user: $scope.user })
				.then(function () {

				})

				.catch(function (reason) {
					console.log(reason);
				});
		}

		$scope.setFacebook = function (method, save) {
			if (method == 'wall') {
				$scope.user.facebook.postTo = 'wall';
				if ($scope.user.socialPortal) {
					$scope.addLogItem({
						title: "Facebook ingesteld op profiel",
						type: "facebook",
						user: $scope.user.firstName + " " + ($scope.user.lastNamePrefix || '') + " " + $scope.user.lastName
					}, true);
				}
				if (save) {
					$scope.saveUser();
				}
			} else {
				$scope.user.facebook.postTo = 'page';
				$scope.getFacebookPages();
			}
		}

		$scope.setFacebookPage = function (selectedPage) {
			$scope.user.facebook.pageToken = selectedPage.access_token;
			$scope.user.facebook.pageId = selectedPage.id
			$scope.user.facebook.pageName = selectedPage.name;
			$scope.addLogItem({
				title: "Facebookpagina ingesteld op " + selectedPage.name,
				type: "facebook",
				user: $scope.user.firstName + " " + ($scope.user.lastNamePrefix || '') + " " + $scope.user.lastName
			}, false);
			$scope.saveUser();
		}

		$scope.saveFacebookSettings = function (facebookId) {
			//Check if 'own profile' or a page is selected
			//console.log("saveFacebookSettings", facebookId, $scope.facebookId, $scope.user.facebook.profileId, $scope.facebookId == $scope.user.facebook.profileId);
			if ($scope.facebookId == $scope.user.facebook.profileId) {
				//Own profile
				$scope.user.facebook.postTo = "wall";
			} else {
				//Facebook page
				$scope.user.facebook.pageId = $scope.facebookId;
				$scope.user.facebook.postTo = "page";
				for (var i = 0; i < $scope.pages.length; i++) {
					if ($scope.pages[i].id == $scope.facebookId) {
						$scope.user.facebook.pageToken = $scope.pages[i].access_token;
					}
				}
			}

			$api.put('user/' + $scope.user._id, { user: $scope.user })
				.then(function (res) {
					console.log("User saved.", res);
				})

				.catch(function (reason) {
					console.log(reason);
				});
		}

		$scope.testFacebook = function () {
			campaignFact.getFacebookInfo().then(function (res) {
				console.log('response from fcebook', res.data);
				if (res.status == 200) {
					$scope.testResult = {
						type: 'success',
						msg: "gebruikers login succesvol getest"
					}
				} else {
					$scope.testResult = {
						type: "warning",
						msg: "gebruiker is niet ingelogd"
					}
				}

			})
			// $scope.facebookResponse = null;
			// $api.get("facebook/test")
			// 	.then(function(response) {
			// 		$scope.post_id = response.data.result.post_id;
			// 		$scope.testResult = {
			// 			type: 'success',
			// 			msg: "Bericht geplaatst! <a href='https://facebook.com/" + $scope.post_id + "' target='_blank'>Bekijk het bericht</a>"
			// 		}
			// 		$scope.addLogItem({
			// 			title: "Test bericht geplaatst op Facebook",
			// 			type: "facebook",
			// 			user: $scope.user.firstName + " " + ($scope.user.lastNamePrefix || '') + " " + $scope.user.lastName
			// 		}, true);
			// 	})	

			// 	.catch(function(reason) {
			// 		console.log(reason);
			// 		$scope.testResult = {
			// 			type: "warning",
			// 			msg: "Fout tijdens plaatsen testbericht: " + reason
			// 		}
			// 		$scope.addLogItem({
			// 			title: "Fout bij plaatsen testbericht op Facebook: " + reason,
			// 			type: "facebook",
			// 			user: $scope.user.firstName + " " + ($scope.user.lastNamePrefix || '') + " " + $scope.user.lastName
			// 		}, true);
			// 	})
		}

		$scope.openPreviewCampaignModal = function(campaign){
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/preview-campaign-modal.html',
				controller: 'previewCampaignModalController',
				size: 'lg',
				resolve: {
					campaign: function() {
						return campaign;
					}
				}
			});
	
			modalInstance.result.then(function(result){
				if(result){
					$scope.user.socialPortal.campaigns.push(campaign);
					$scope.saveSocialPortal();
					$scope.getPortalCampaigns();
					$scope.alert = {
						type: 'success',
						msg: "De promotie " + campaign.name + " is toegevoegd bij Mijn promoties. <a href='/retailer/campaigns/" + campaign.nameSlug + "'> Bekijk de campagne</a>"
					}
				}
			}, function(){
				//dismissed
			})
		}

		$scope.saveSocialPortal = function(){
			console.log('$scope.user.socialPortal',$scope.user.socialPortal);
			$api.put('user-social-portal', {socialPortal: $scope.user.socialPortal})
			.then(function(response) {
				console.log('response in user social portal',response);
				$rootScope.user.socialPortal = response.data.result;
			})
			.catch(function(reason) {
				console.log(reason);
			})
		}

		$scope.closeAlert = function(){
			$scope.alert = null;
		}

		$scope.getPinterestBoards = function(){
			$api.get('pinterest/get-boards')
				.then(function(response) {
					$scope.boards = response.data.boards;
				})
				.catch(function(reason) {
					console.log(reason);
				})
		}

		$scope.savePinterestSettings = function(){
			$api.put('user/' + $scope.user._id, {user: $scope.user})
				.then(function(res) {
					console.log("User saved.",res);
				})

				.catch(function(reason) {
					console.log(reason);
				});
		}

		$scope.testPinterest = function(){
			var pin = {
				note: "Testbericht vanaf PrismaNote",
				image: 'https://s3-eu-west-1.amazonaws.com/prismanote/notenews/2157f5d1-ef67-4565-8df3-e04354c66269.png'
			}
			$scope.pinterestResponse = null;
			$api.post("pinterest/create-pin", {pin: pin})
				.then(function(response) {
					$scope.post_id = response.data.result.post_id;
					$scope.pinterestResponse = {
						type: 'success',
						msg: "Bericht geplaatst! <a href='https://facebook.com/" + $scope.post_id + "' target='_blank'>Bekijk het bericht</a>"
					}
				})	
				
				.catch(function(reason) {
					console.log(reason);
					$scope.pinterestResponse = {
						type: "warning",
						msg: "Fout tijdens plaatsen testbericht: " + reason
					}
				})
		}

		$scope.completeTask = function(task){
			if(task.type == "order"){
				$window.open(task.url);
			}

			prompt({
				title: 'Taak uitvoeren',
				message: 'Is uitvoeren van taak ' + task.title + ' gelukt?'
			}).then(function() {
				task.completed = true;
				task.dateCompleted = new Date();

				$scope.addLogItem({
					title: task.title + " uitgevoerd",
					type: "task",
					taskId: task._id,
					user: $scope.user.firstName + " " + ($scope.user.lastNamePrefix || '') + " " + $scope.user.lastName
				}, false);

				$scope.saveSocialPortal();
				$scope.getOpenTasksCount();
			});
		}

		$scope.markOpen = function(task){
			task.completed = false;
			$scope.addLogItem({
				title: task.title + " als openstaand gemarkeerd",
				type: "task",
				taskId: task._id,
				user: $scope.user.firstName + " " + ($scope.user.lastNamePrefix || '') + " " + $scope.user.lastName
			}, false);
			$scope.saveSocialPortal();
		}

		$scope.addLogItem = function(item, save){
			var logItem = {
				title: item.title,
				date: new Date(),
				type: item.type,
				taskId: item.taskId,
				user: item.user
			};
			$scope.user.socialPortal.log.push(logItem);

			if(save){
				$scope.saveSocialPortal();
			}
		}
		$scope.deleteFacebook = function(){
			prompt({
				title: 'Undo Facebook link',
				message: 'Are you sure you want to unlink with Facebook?'
			}).then(function() {
				campaignFact.unLinkFacebook().then(function(res){
					console.log('unlink successfully',res);
					if(res.data.fbLogout == true){
						$scope.user.facebook = {};
						$scope.addLogItem({
							title: "Facebook link removed",
							type: "facebook",
							user: $scope.user.firstName + " " + ($scope.user.lastNamePrefix || '') + " " + $scope.user.lastName
						}, true);
						$scope.saveUser();
					}
				})

				//$window.location.reload();
			});
		}


		$scope.openTaskModal = function(task){

			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/view-task-modal.html',
				controller: 'viewTaskModalController',
				size: 'lg',
				resolve: {
					task: function() {
						return task;
					}
				}
			});
	
			modalInstance.result.then(function(result){
				$scope.saveSocialPortal();
				$scope.getOpenTasksCount();
			}, function(){

			})
		}

		$scope.openCreateCampaignModal = function(){
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/create-campaign-modal.html',
				controller: 'createCampaignModalController',
				size: 'lg',
			});

			modalInstance.result.then(function(result){
				//console.log("MODALRESULT", result);
				
				for(var i = result.length - 1; i >= 0; i--) {
					if(array[i] === tasks) {
					array.splice(i, 1);
					}
				}

				// niet invullen maar standaard waarde
				result.strategy = "store-promotion";
				//result.number = $rootScope.randomNumber(); // edit this because ofcourse this is not logic
				result.nameSlug = $rootScope.slugify(result.name);
				//result.nameSlug = "hoe-genereer-je-een-slug";

				result.update = false;
				result.images = [];
				var language = $rootScope.language;
				var task = {
					type: 'facebook',
					mandatory: true,
					defaultChecked: true,
					images: [],
					fbPromotionSettings: {
						alsoOnInsta: true
					}
				};
				task[language] = {
					name: result.name
				}
				result.tasks = [task];
				var image = {
					src: 'https://prismanotevoorjuweliers.nl/wp-content/uploads/2017/07/prismanote-vliegtuigje.png', 
					alt: 'Standard My-Store-Promotion-campaign'
				}
				task.images.push(image);
				result.images.push(image);

				//console.log("Tasks", result);
				$scope.user.socialPortal.campaigns.push(result);

				$scope.saveSocialPortal();
				$scope.getPortalCampaigns();

			}, function(){

			})
		}
		$scope.searchBrands = function(str) {
			var matches = [];
			$scope.brands.forEach(function(brand){
				if((brand.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
				(brand.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(brand);
				}
			})
			return matches;
		};
		$scope.brandSelected = function(selected){
			var brand = {
				_id: selected.originalObject._id,
				name: selected.title
			}
			var index = _.findIndex($scope.user.socialPortal.brands, {'_id': brand._id})
			
			if(index < 0){
				$scope.user.socialPortal.brands.push(brand);
			}

			//check if brand exists on shop to ask if it must be added to their shop too
			if($scope.user.shops && $scope.user.shops.length > 0){
				var brandIndex = _.findIndex($scope.currentShop.brands, {'_id' : brand._id});
				if(brandIndex < 0){
					prompt({
						title: 'Ook op uw winkel toevoegen?',
						message: 'Het merk is toegevoegd, wilt u deze ook toevoegen op uw winkel (' + $scope.currentShop.name + ') ?'
					}).then(function() {
						$scope.currentShop.brands.push(brand);

						$retailer.setShop($rootScope.selectedShop, $scope.currentShop);
					})
				}
			}

			$scope.saveSocialPortal();
		}
		$scope.getBrands = function() {
			$api.get('brands')
			
				.then(function(response) {
					$scope.brands = response.data.brands;
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		};
		$scope.deleteBrandFromSocialPortal = function(index){
			var brand = $scope.user.socialPortal.brands[index];
			prompt({
				title: 'Merk verwijderen?',
				message: 'Weet u zeker dat u dit merk wilt verwijderen?'
			}).then(function() {
				$scope.user.socialPortal.brands.splice(index, 1);
				$scope.saveSocialPortal();
				if($scope.user.shops && $scope.user.shops.length > 0){
					//check if brand also exists on shop to ask if the brand must be removed there too
					var brandIndex = _.findIndex($scope.currentShop.brands, {'_id' : brand._id});

					if(brandIndex >= 0){
						prompt({
							title: 'Ook van winkel verwijderen?',
							message: 'Het merk is verwijderd, wilt u deze ook verwijderen van uw winkel (' + $scope.currentShop.name + ') ?'
						}).then(function() {
							$scope.currentShop.brands.splice(brandIndex, 1);

							$retailer.setShop($rootScope.selectedShop, $scope.currentShop);
						})
					}
				}
			});
		}
	
}]);