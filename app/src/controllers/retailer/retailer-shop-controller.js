prismanoteApp.controller("retailerShopController", ['$scope','$q', '$rootScope', '$uibModal', '$api', 'prompt', '$retailer', '$countr',
	function($scope,$q, $rootScope, $uibModal, $api, prompt, $retailer, $countr) {
		$scope.addTax = false;
		$scope.processing = false;
		$scope.syncResult = null;
		$scope.countr = {};

		$scope.collectionsSynced = false;


		$scope.submitShop = function(valid) {
			if(valid) {
				$rootScope.currentShop.isPublished = $rootScope.currentShop.isShopActive;
				$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
			}
		};
		$scope.addBrands = function(data) {
			console.log("Inside addBrands");
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/add-brands-modal.html',
				controller: 'addBrandsModalController',
				size: 'lg',
				resolve: {
					data: function() {
						return data;
					}
				}
			});

			modalInstance.result.then(function(result){
				if(result){
					console.log("result",result);
				// 				$scope.currentShop.news.push(newsItem);
				// $scope.updateShop();
				// $scope.reloadShop();
				// $scope.activeTab=1;                
			}
		}, function(){
						//dismissed
					})
		};

		$scope.getBrands = function() {
			$api.get('brands')
			
			.then(function(response) {
				$scope.brands = response.data.brands;
			})

			.catch(function(reason) {
				console.log(reason);
			});
		};

		$scope.searchBrands = function(str) {
			var matches = [];
			$scope.brands.forEach(function(brand){
				if((brand.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
					(brand.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )) {
					matches.push(brand);
			}
		})
			return matches;
		};

		var loadProductsFromBrand = function(brand, callback) {
			console.log("loadProductsFromBrand", brand.nameSlug, brand);
			$api.get('products', {'aggregate': {'brand.nameSlug': brand.nameSlug}})
			
			.then(function(response) {
				return callback(null, response.data);
			})

			.catch(function(reason) {
				console.log(reason);
				return callback(reason, null);
			});
		};

		$scope.brandSelected = function(selected){
			var brand = {
				_id: selected.originalObject._id,
				name: selected.title,
				nameSlug: selected.originalObject.nameSlug,
				description: selected.originalObject.description,
				images: selected.originalObject.images,
				restricted: selected.originalObject.restricted
			}
			// console.log('$rootScope.currentShop',$rootScope.currentShop);
			var existing = false;
			angular.forEach($rootScope.currentShop.brands,function(val){
				if(val._id == brand._id){
					existing = true;
				}
			});
			if(existing == true){
				alert('this brand has already added');
				return;
			}


			if(($rootScope.currentShop.isPremium || (!$rootScope.currentShop.isPremium && !brand.restricted)) && $rootScope.currentShop.webshopActive || true) {
				console.log("Calling API for adding in brand")
				$api.post('add-brand-to-shop',{
					brand: brand,
					shopId : $rootScope.currentShop
				}).then(function(success){
					alert("Successfully Added brand to the shop.");
					$rootScope.currentShop.brands.push(brand);
					// $retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
					$retailer.getShop().then(function(shop) {		
						$rootScope.currentShop = shop;
					})

				},function(err){
					alert("Sorry. Couldn't be added.")
				});

				// loadProductsFromBrand(brand, function(err, result) {
				// 	if(err) return console.log("Error: ", err);
				// 	$scope.progress = true;
				// 	$scope.productCount = result.products.length;
				// 	$scope.count = 0;
				// 	for(var i = 0; i < $scope.productCount; i++) {
				// 		$scope.count += 1;
				// 		var product = {
				// 			_id: result.products[i]._id,
				// 			stock: 0,
				// 			price: result.products[i].suggestedRetailPrice,
				// 			dropshippingPrice: result.products[i].suggestedRetailPrice,
				// 			discount: 0,
				// 			dateAdded: new Date(),
				// 			show: true,
				// 			isBestseller: false,
				// 			isFeatured: false
				// 		};
				// 		$rootScope.currentShop.products.push(product);

				// 		if($scope.count == result.products.length) {
				// 			$scope.progress = false;
				// 			$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
				// 		}
				// 	}
				// });
			}
			else{
				console.log("Will not call API.")
			}
			getSocialPortal(function(err, portal){
				if(err){
					console.log("err");
					return;
				}
				var index = _.findIndex($scope.user.socialPortal.brands, {'_id': brand._id});
				if(index <0){
					prompt({
						title: 'Merk ook voor promoties gebruiken?',
						message: 'Wilt u ook promoties van dit merk zien en gebruiken?'
					}).then(function() {		
						var portalBrand = {
							_id: brand._id,
							name: brand.name
						}
						$scope.user.socialPortal.brands.push(portalBrand);
						$scope.saveSocialPortal();

					})
				}
			})			
		};

		function getSocialPortal(callback){
			$api.get('user-social-portal', {userId: $scope.user._id})

			.then(function(response){
				$scope.user.socialPortal = response.data.result;
				return callback(null, response.data.result)
			})
			.catch(function(reason){
				return callback(reason)
			})
		}

		$scope.saveSocialPortal = function(){
			$api.put('user-social-portal', {socialPortal: $scope.user.socialPortal})
			.then(function(response) {

			})
			.catch(function(reason) {
				console.log(reason);
			})
		}

		$scope.removeBrandProducts = function(brand, options) {
			var defer = $q.defer();

			if(brand != null && typeof brand === 'object' && brand != {}){
				$api.post('remove-brand-from-shop', {"brand": brand,"shopId":$rootScope.currentShop._id})
				.then(function(success){
					defer.resolve()
				},function(error){
					alert("There seems to be some error. Please try again later or contact the admin.")
					console.error(err);
					defer.reject();
				})
			}
			else{
				defer.reject();
			}
			return defer.promise;


			// if(brand != null && typeof brand === 'object' && brand != {}) {
			// 	$scope.progress = false;

			// 	if(options == null || typeof options !== 'object') {
			// 		options = {};
			// 	}

			// 	var ids = [];
			// 	for(var i = 0; i < $rootScope.currentShop.products.length; i++) {
			// 		// Add the shop product, remove if it does not match the filters.
			// 		if($rootScope.currentShop.products[i].stock == 0 || (options.all != null && options.all === true)) {
			// 			ids.push($rootScope.currentShop.products[i]._id);
			// 		}
			// 	}
			// 	$api.post('get-specific-shop-products', {short: true, filter: {_id: {$in: ids}, "brand.nameSlug": brand.nameSlug}, limit: undefined, offset: 0})
			// 	.then(function(response) {
			// 		if(response.data.products.length <= 0 ){
			// 			return;
			// 		}
			// 		for(var i = 0; i < response.data.products.length; i++) {
			// 			var product = response.data.products[i];
			// 			$rootScope.currentShop.products.splice($rootScope.currentShop.products.findIndex(function(element) { return element._id == product._id; }), 1);
			// 		}

			// 		$retailer.setShop($retailer.selectedShop, $rootScope.currentShop);
			// 	})
			// 	.catch(function(reason) {
			// 		console.log(reason);
			// 	});
			// }
		};

		$scope.deleteBrand = function(index) {
			var brand = $rootScope.currentShop.brands[index];
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/remove-products-modal.html',
				controller: 'removeProductsModalController'
			});

			modalInstance.result
			.then(function(options) {
				$scope.removeBrandProducts(brand, options).then(function(success){
					alert("Removed the brand Successfully");
					// $rootScope.currentShop.brands.splice(index, 1);
					// $api('shops/'+$rootScope.currentShop.nameSlug).then(function(shop) {		
					// 	$rootScope.currentShop = shop;
					// })

					$retailer.getShop().then(function(shop) {		
						$rootScope.currentShop = shop;
						getSocialPortal(function(err, portal){
							if(err){
								console.log("err", err);
								return;
							}
							var index = _.findIndex($scope.user.socialPortal.brands, {'_id': brand._id});
							if(index >= 0){
								prompt({
									title: 'Ook promoties stoppen?',
									message: 'Wilt u ook stoppen met het voeren van de promoties voor dit merk?'
								}).then(function() {		
									$scope.user.socialPortal.brands.splice(index, 1);
									$scope.saveSocialPortal();				
								})
							}
						})
					})

				},function(err){
					alert("Couldn't remove the brand")
				});



			})

			.catch(function(reason) {
				console.log(reason);
			});
			
		};

		// check IBAN
		$scope.alertValidIBAN = function(iban) {
			if(isValidIBANNumber(iban) == 1){
				alert("The IBAN is valid")
			} else {
				alert("The IBAN is invalid")
			}
		}
		//- isValidIBANNumber(document.getElementById("iban").value)	
		/*
		* Returns 1 if the IBAN is valid 
		* Returns FALSE if the IBAN's length is not as should be (for CY the IBAN Should be 28 chars long starting with CY )
		* Returns any other number (checksum) when the IBAN is invalid (check digits do not match)
		*/
		function isValidIBANNumber(input) {
			var CODE_LENGTHS = {
				AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
				CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
				FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
				HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
				LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
				MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
				RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
			};
			var iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
					code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
					digits;
			// check syntax and length
			if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
				return false;
			}
			// rearrange country code and check digits, and convert chars to ints
			digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function (letter) {
				return letter.charCodeAt(0) - 55;
			});
			// final check
			return mod97(digits);
		}
		function mod97(string) {
			var checksum = string.slice(0, 2), fragment;
			for (var offset = 2; offset < string.length; offset += 7) {
				fragment = String(checksum) + string.substring(offset, offset + 7);
				checksum = parseInt(fragment, 10) % 97;
			}
			return checksum;
		}
		
		$scope.logo = function(type){
			if($rootScope.currentShop && $rootScope.currentShop[type]){
				if($rootScope.currentShop[type].src && $rootScope.currentShop[type].src.indexOf('images') !== -1){
					return $rootScope.currentShop[type].src;
				}else{
					return $rootScope.awsUrl + $rootScope.currentShop[type].src;
				}
			}
		}

		var generalTour = new Tour({
			steps: [
			{
				element: '#generalTourFillInInformation',
				title: 'Nieuwe klant',
				content: 'Vul uw gegevens in',
				path: '/retailer/shop'
			},
			{
				element: '#generalTourBrands',
				title: 'Nieuwe klant',
				content: 'Geef aan  welke merken u voert.',
				path: '/retailer/shop'
			},
			{
				element: '#generalTourWebshopSettings',
				title: 'Nieuwe klant',
				content: 'Bepaal de instellingen voor uw webshop',
				path: '/retailer/shop'
			},
				// {
				// 	element: '#generalTourProShop',
				// 	title: 'Nieuwe klant',
				// 	content: 'Wilt een een professionele shop op PrismaNote? Hier betaald u 25 euro voor. We gaan nu terug naar uw overzicht.',
				// 	path: '/retailer/shop'
				// },
				{
					element: '#generalTourMyProfile',
					title: 'Nieuwe klant',
					content: 'Klik op Mijn profiel om uw persoonlijk profiel te beheren.',
					path: '/retailer/home'
				},
				{
					element: '#generalTourShopsToManage',
					title: 'Nieuwe klant',
					content: 'Neem contact op met de beheerder om een winkel aan uw account toe te voegen.',
					path: '/retailer/user'
				},
				]
			});

		$scope.startGeneralTour = function(){
			console.log(generalTour);
			generalTour.init();
			generalTour.restart();
		}

		$scope.countrModule = $rootScope.checkModule('countr');

		$scope.$on('changeSelectedShop', function() {
			$retailer.getShop()
			.then(function(shop) {		
				if($rootScope.checkModule('countr')){
					$rootScope.currentShop = shop;
					$scope.checkCountrStatus();		
				}			
			})
			.catch(function(reason) {
				console.log(reason);
			});
		});

		$scope.getTaxes = function(){
			if($scope.countrStatus){
				$api.get('countr/taxes', {shopId: $rootScope.currentShop._id})
				.then(function(response) {
					$scope.taxes = response.data;

					if((typeof $rootScope.currentShop.countr.taxId == "undefined" || $rootScope.currentShop.countr.taxId == "") && $scope.taxes.length > 0){
						$rootScope.currentShop.countr.taxId = $scope.taxes[0]._id;
						$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
					}
				})
				.catch(function(reason) {
					console.log(reason);
				})
			}
		}

		$scope.deleteTax = function(id, index){
			prompt({
				title: 'Belastingtarief verwijderen?',
				message: 'Weet u zeker dat u dit belastingtarief wilt verwijderen?'
			}).then(function() {
				$countr.deleteTax(id)
				.then(function(response) {
					$scope.taxes.splice(index, 1);

					if(id == $rootScope.currentShop.countr.taxId && $scope.taxes.length > 0){
						//Set new default tax rate to the first
						$rootScope.currentShop.countr.taxId = $scope.taxes[0]._id;
						$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
					}

				})
				.catch(function(reason) {
					console.log(reason);
					$scope.countrAlert = {
						type: 'danger',
						msg: reason
					}
				})
			});
		}

		$scope.defaultTax = function(taxId){
			$countr.setDefaultTax(taxId);
		}

		$scope.addNewTax = function(){
			$countr.newTax($scope.newTax.name, $scope.newTax.rate, $scope.newTax.default)
			.then(function(response) {
				$scope.taxes.push({
					name: $scope.newTax.name,
					rate: $scope.newTax.rate,
					_id: response.tax._id
				})

				$scope.addTax = !$scope.addTax;
				$scope.newTax = {};
				
			})
			.catch(function(reason) {
				console.error(reason);
				$scope.countrAlert = {
					type: 'danger',
					msg: reason
				}
			})
		}

		$scope.setCountrId = function(){
			$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
		}

		$scope.getCountrShops = function(){
			if($scope.countrStatus){
				$countr.getShops()
				.then(function(response) {
					$scope.countrShops = response;
				})
				.catch(function(reason) {
					console.error(reason);
					$scope.countrAlert = {
						type: 'danger',
						msg: reason
					}
				})
			}
		}

		$scope.checkCountrStatus = function(force){
			$scope.alert = {};
			$scope.newTax = {};
			if(force){
				$retailer.getShop()
				.then(function(shop) {
					$rootScope.currentShop = shop;					
					checkStatus();
				})
			}else{
				checkStatus();
			}

			function checkStatus() {
				$scope.countrStatus = $countr.connectionStatus();

				if($scope.countrStatus && ($rootScope.currentShop.countr && $rootScope.currentShop.countr.username)){
					$scope.countr.username = $rootScope.currentShop.countr.username
				}
				
				
				$scope.getCountrShops();
				$scope.getTaxes();
			}
		}

		$scope.connectCountr = function(valid){
			$scope.processing = true;
			$scope.countrAlert = null;
			$scope.alert = null;

			if(valid){
				$countr.connect($scope.countr.username, $scope.countr.password)
				.then(function() {
					$scope.processing = false;
					$scope.checkCountrStatus(true);		
					$countr.webhookSetup();			
				})
				.catch(function(reason) {
					console.error(reason);
					$scope.processing = false;
					$scope.alert = {
						type: 'danger',
						msg: reason
					}
				})
			}
		}

		$scope.sync = function(data, reverse){
			
			$scope.syncResult = null;

			if(typeof reverse == 'undefined'){
                reverse = false;
            }

			if(reverse){
                prompt({
                    title: 'Producten van Countr naar PrismaNote synchroniseren',
                    message: 'Dit zal alle bestaande producten van uw winkel verwijderen, wilt u doorgaan?'
                }).then(function() {    
                    runSync(data, true);
                })
            }else{
                runSync(data, false);
            }
		}

		function runSync(data, reverse){
			$scope.processing = true;
			$countr.sync(data, reverse)
			.then(function(response) {
				$scope.syncResult = {
					type: 'success',
					msg: response.message
				}
				if(data == 'collections'){
					$scope.collectionsSynced = true;
				}
				$scope.processing = false;
			})
			.catch(function(reason) {
				console.error(reason);
				$scope.processing = false;
				$scope.syncResult = {
					type: 'danger',
					msg: reason
				}
			})
		}

		$scope.openCreateStoreModal = function(){
			var countrData = $rootScope.currentShop.countr;
			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/countr-create-store-modal.html',
				controller: 'createStoreModalController',
				size: 'lg'
			});

			modalInstance.result.then(function(result){
				if(result){
					$scope.getCountrShops();
				}
			}, function(){
					//dismissed
				})
		}

		$scope.breakConnection = function(){

			prompt({
				title: 'Countr connectie ongedaan maken',
				message: 'Weet u zeker dat u de Countr koppeling wilt verwijderen? '
			}).then(function() {

				$rootScope.currentShop.countr = {};
				$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
				$scope.checkCountrStatus();
				
				$scope.countrStatus = false;
				
				delete $scope.taxes;
				delete $scope.countrShops;
			})
		}


	}]);
