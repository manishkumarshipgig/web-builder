prismanoteApp.controller("retailerProductController", ['$state','$scope', '$rootScope', '$api', '$q', '$translate', '$retailer', '$uibModal', 'Upload', '$stateParams',
	function($state, $scope, $rootScope, $api, $q, $translate, $retailer, $uibModal, Upload, $stateParams) {
		var nameSlug;
		$scope.collection = {};		
		$scope.updateMode = false;
		$scope.updatedImages = [];
		$scope.dialColors = [{key: 'SILVER',hex: '#CACFD2'},{key: 'GREY',hex: '#4D5656'},{key: 'BLUE',hex: '#2980B9'},{key: 'WHITE',hex: '#FFF8DC'},{key: 'RED',hex: '#922B21'},{key: 'BLACK',hex: '#000000'},{key: 'ROSE_GOLD',hex: '#FFA07A'},{key: 'GOLD',hex: '#B8860B'},{key: 'MOTHER_OF_PEARL',hex: '#BC8F8F'},{key: 'BROWN',hex: '#8B4513'},{key: 'BEIGE',hex: '#CD853F'},{key: 'MOTHER_OF_PEARL_COLOURED',hex: '#BC8F8F'},{key: 'GOLDEN',hex: '#B8860B'},{key: 'GREEN',hex: '#228B22'},{key: 'PINK',hex: '#FF69B4'},{key: 'PURPLE',hex: '#663399'},{key: 'ORANGE',hex: '#FF5733'},{key: 'YELLOW',hex: '#FFD700'},{key: 'CREME',hex: '#F5DEB3'},{key: 'TAUPE',hex: '#D2B48C'},{key: 'BRASS',hex: '#CD853F'}];
		$scope.strapColors = [{ key: 'SILVER',hex: '#CACFD2'},{key: 'GREY',hex: '#4D5656'},{key: 'BLUE',hex: '#2980B9'},{key: 'WHITE',hex: '#FFF8DC'},{key: 'RED',hex: '#922B21'},{key: 'BLACK',hex: '#000000'},{key: 'ROSE_GOLD',hex: '#FFA07A'},{key: 'GOLD',hex: '#B8860B'},{key: 'BROWN',hex: '#8B4513'},{key: 'BEIGE',hex: '#CD853F'},{key: 'GOLDEN',hex: '#B8860B'},{key: 'GREEN',hex: '#228B22'},{key: 'PINK',hex: '#FF69B4'},{key: 'PURPLE',hex: '#663399'},{key: 'ORANGE',hex: '#FF5733'},{key: 'YELLOW',hex: '#FFD700'},{key: 'CREME',hex: '#F5DEB3'},{key: 'TAUPE',hex: '#D2B48C'},{key: 'BRASS',hex: '#CD853F'}];

		// function activate(){
		// 	getItems('brands');
		// }
		// activate();
		$scope.addNewCollectionForm = false;
		$scope.collectionSelected = function(selected){
			$scope.addNewCollectionForm = false;
			if(selected.title === " ➕ Add New Collection" || selected.title === " ➕ Kollektion hinzufügen" || selected.title === " ➕ Voeg nieuwe collectie toe" || selected.title === " ➕ Añadir colección" || selected.title === " ➕ Ajouter une collection"){
				console.log("Selected", selected);

				$scope.addNewCollectionForm = true;
				$("#collection-"+$rootScope.language+"-name").val($scope.searchStr);
			}
			else{
				var collection = selected.originalObject;
				if(!$scope.product.collections){
					$scope.product.collections = [];
				}
				$scope.product.collections.push(collection);
				// $api.put('products/'+$scope.product[$rootScope.language].nameSlug,$scope.product)
				// .then(function(success){
				// 	console.log("Product has been updated with new collection.")
				// },function(err){
				// 	console.error(err);
				// })
				console.log("product", $scope.product);
			}
		};


		$scope.sendDeleteSuggestion = function(){
			$api.post('product/delete-suggestion',{productId: $scope.product._id})
			.then(function(success){
				alert("Request for Deleting this product has been sent to admin. !")
			},function(err){
				alert("There seems to be some error. Please try again later.")
			});
		}

		$scope.searchCollections = function(str) {
			console.log("SEARCH COLLECTION CALLED");
			return searchItems(str, 'collections');
		}

		$scope.getCollections = function() {
			console.log("GETTING COLLECTIONS");
			return getItems('collections');
		};



		$scope.caseColors = $scope.jewelColors = Object.assign($scope.dialColors, $scope.strapColors);

		if($stateParams.nameSlug != null) {
			$scope.updateMode = true;

			$scope.images = [];

			$api.get('products/' + $stateParams.nameSlug)

			.then(function(response) {

				$scope.product = response.data.product;

				nameSlug = $scope.product.en.nameSlug || $scope.product.nameSlug;

				$scope.productCategory = $scope.product.category;

				if($scope.productCategory == 'WATCH') {

					if($scope.product.watch.dial.color) {
						$scope.dialColor = $scope.dialColors.find(function(color) { return color.key == $scope.product.watch.dial.color});
					}

					if($scope.product.watch.strap.color) {
						$scope.strapColor = $scope.strapColors.find(function(color) { return color.key == $scope.product.watch.strap.color});
					}

					if($scope.product.watch.case.color) {
						$scope.caseColor = $scope.caseColors.find(function(color) { return color.key == $scope.product.watch.case.color});
					}

				} else if($scope.productCategory == 'JEWEL') {
					$scope.jewelColor = $scope.jewelColors.find(function(color) { return color.key == $scope.product.jewel.color});

				} else if($scope.productCategory == 'STRAP') {
					$scope.strapColor = $scope.strapColors.find(function(color) { return color.key == $scope.product.strap.color});

					// Enable or Disable this?
					// } else if($scope.productCategory == 'OTHER') {
					// 	$scope.otherColor = $scope.otherColors.find(function(color) { return color.key == $scope.product.other.color});
				}

				$scope.setTargetAudience($scope.product.male, $scope.product.female, $scope.product.kids);

			})

			.catch(function(err) {
				console.log(err);
			});
		} else {
			// ADD PRODUCT CONF
			$scope.updateMode = false;

			$scope.product = {
				variants: [{collapse: false}],
				brand: {},
				category: 'WATCH',
				views: 0,
				nl: {views: 0},
				en: {views: 0},
				de: {views: 0},
				fr: {views: 0},
				es: {views: 0},
				strap: {},
				jewel: {},
				watch: {
					case: {},
					strap: {},
					dial: {}
				}
			};

			$scope.images = [];
		}

		$scope.setProductCategory = function(category) {
			if(category == 'WATCH') {
				$scope.product.strap = $scope.product.jewel = {};
				$scope.product.watch = {case: {}, strap: {}, dial: {}};
			} else if(category == 'STRAP') {
				$scope.product.watch = $scope.product.jewel = {};
			} else if(category == 'JEWEL') {
				$scope.product.strap = $scope.product.watch = {};
			// Enable or Disable this?
			// } else if(category == 'OTHER') {
			// 	$scope.product.other = $scope.product.watch = {};
		}
		$scope.product.category = category;
	}

	$scope.setCaseColor = function(color) {
		$scope.product.watch.case.color = color.key;
	}

	$scope.setDialColor = function(color) {
		$scope.product.watch.dial.color = color.key;
	}

	$scope.setStrapColor = function(color) {
		if($scope.product.category == 'WATCH') {
			$scope.product.watch.strap.color = color.key;
		} else if($scope.product.category == 'STRAP') {
			$scope.product.strap.color = color.key;
		}
	}

	var getItems = function(type){
		console.log("GETTING ",type);
		$api.get(type)

		.then(function(response) {
			$scope[type] = response.data[type];
		})

		.catch(function(reason) {
			console.log(reason);
		});
	};

	$scope.getBrands = function() {
		return getItems('brands');
	};
	var searchItems = function(str, type) {
		var matches = [];
		console.log("Test ",type);
		if(type == "collections"){		
			var addNewCollectionItem = {
				en: {
					name: " ➕ Add New Collection"
				},
				nl: {
					name: " ➕ Voeg nieuwe collectie toe"
				},
				es: {
					name: " ➕ Añadir colección"
				},
				fr: {
					name: " ➕ Ajouter une collection"
				},
				de: {
					name: " ➕ Kollektion hinzufügen"
				}
			};
			matches.push(addNewCollectionItem)
		}


		$scope[type].forEach(function(item) {
			if(type == "brands"){
				console.log("Search For BRAND");
				console.log("$scope.brands",$scope.brands)
				if(
					(item != undefined)
					&& (
						(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
						||
						(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
						)
					) {
					matches.push(item);
			}
		}
		else{
			if(
				(item[$rootScope.language] != undefined && item[$rootScope.language].name != undefined)
				&& (
					(item[$rootScope.language].name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
					||
					(item[$rootScope.language].nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
					)
				) {
				matches.push(item);
		}
	}

});


		return matches;
	};
	// var searchItems = function(str, type) {
	// 	var matches = [];
	// 	console.log("SEARCH ITEM CALLED FOR PARAMS : str = ",str," type = ",type);
	// 	console.log("Searching from : ",$scope[type]);
	// 	if(type != "brands"){
	// 		$scope[type].forEach(function(item) {
	// 			if(
	// 				(item[$rootScope.language] != undefined && item[$rootScope.language].name != undefined)
	// 				&& (
	// 					(item[$rootScope.language].name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
	// 					||
	// 					(item[$rootScope.language].nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
	// 					)
	// 				) {
	// 				matches.push(item);
	// 		}
	// 	}
	// 	);

	// 		// $scope[type].forEach(function(item) {
	// 		// 	if((item[$rootScope.language] != undefined)
	// 		// 		&& (
	// 		// 			(item[$rootScope.language].name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
	// 		// 			||
	// 		// 			(item[$rootScope.language].nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
	// 		// 			)
	// 		// 		)
	// 		// 	{
	// 		// 		matches.push(item);
	// 		// 	}
	// 		// 	else{
	// 		// 		console.log("item.language = ",item[$rootScope.language])
	// 		// 	}
	// 		// });
	// 	}
	// 	else{
	// 		$scope[type].forEach(function(item) {
	// 			if(
	// 				(item.name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)
	// 				||
	// 				(item.nameSlug.toLowerCase().indexOf(str.toString().toLowerCase()) > 0 )
	// 				)

	// 			{
	// 				matches.push(item);
	// 			}
	// 			else{
	// 				console.log("item.language = ",item[$rootScope.language])
	// 			}
	// 		});
	// 	}

	// 	return matches;
	// };

	$scope.searchBrands = function(str) {
		console.log("Searching Brand",$scope.brands);
		return searchItems(str, 'brands');
	}

	$scope.brandSelected = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}

		$scope.product.brand = brand;
	};

	$scope.brandSelectedForCollection = function(selected){
		var brand = {
			_id: selected.originalObject._id,
			name: selected.title,
			nameSlug: selected.originalObject.nameSlug,
			description: selected.originalObject.description,
			images: selected.originalObject.images
		}
		$scope.collection['brand'] = brand;

	};

	$scope.addProductVariant = function() {
		$scope.product.variants.push({collapse: true});
	}

	$scope.removeProductVariant = function(index) {
		$scope.product.variants.splice(index, 1);
	}

	$scope.addProductImage = function() {
		$scope.images.push({});
	}

	$scope.removeProductImage = function(mode, index) {
		if(mode == 'scope'){
			$scope.images.splice(index, 1);
		}else{
			$scope.product.images.splice(index, 1);
		}
	}

	$scope.setGender = function(targetAudience) {
		switch(targetAudience) {
			case 'GENTS':
			$scope.product.male = true;
			$scope.product.female = false;
			$scope.product.kids = false;
			break;
			case 'LADIES':
			$scope.product.male = false;
			$scope.product.female = true;
			$scope.product.kids = false;
			break;
			case 'UNISEX':
			$scope.product.male = true;
			$scope.product.female = true;
			$scope.product.kids = false;
			break;
			case 'BOYS':
			$scope.product.male = true;
			$scope.product.female = false;
			$scope.product.kids = true;
			break;
			case 'GIRLS':
			$scope.product.male = false;
			$scope.product.female = true;
			$scope.product.kids = true;
			break;
			case 'KIDS':
			$scope.product.male = true;
			$scope.product.female = true;
			$scope.product.kids = true;
			break;
		}
	}

	$scope.setTargetAudience = function(male, female, kids) {
		if(male) {
			if(female) {
				if(kids) {
					$scope.targetAudience = 'KIDS';
				} else {
					$scope.targetAudience = 'UNISEX';
				}
			} else {
				if(kids) {
					$scope.targetAudience = 'BOYS';
				} else {
					$scope.targetAudience = 'GENTS';
				}
			}
		} else {
			if(female) {
				if(kids) {
					$scope.targetAudience = 'GIRLS';
				} else {
					$scope.targetAudience = 'LADIES';
				}
			}
		}
	}

	$scope.openProductModal = function(product) {
		return $q(function(resolve, reject) {

			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/retailer-product-modal.html',
				controller: 'retailerProductModalController',
				size: 'lg',
				resolve: {
					product: function() {
						product.shippingCosts = $rootScope.currentShop.shippingCosts;
						return product;
					}
				}
			});

			modalInstance.result

			.then(function(response) {
				resolve(response);
			})

			.catch(function(reason) {
				reject(reason);
			});
		});
	};

	$scope.testRedirect = function(){
		console.log("Calling state.go")
		$state.go('retailer.assortment',{modalFor: $scope.product})
	}
	function deepen(o) {
			//convert the dot notation to an nested object
			var oo = {}, t, parts, part;
			for (var k in o) {
				t = oo;
				parts = k.split('.');
				var key = parts.pop();
				while (parts.length) {
					part = parts.shift();
					t = t[part] = t[part] || {};
				}
				t[key] = o[k]
			}
			return oo;
		}

		function getModifiedProperties(callback){
			var modifiedProperties = {};

			angular.forEach($scope.addProductForm, function(value, key) {
				if(key[0] == '$') return;
				var splittedKey = key.split('.');
				if(splittedKey.length > 0){
					if (splittedKey[0].includes('[language]')) {
						splittedKey[0] = $scope.language;
					}
					key = splittedKey.join('.');
				}

				if(value.$dirty){
					modifiedProperties[key] = value.$$lastCommittedViewValue;
					value.$setPristine();
				}
			});

			return callback(deepen(modifiedProperties));
		}

		$scope.saveProduct = function() {

			if($scope.addProductForm.$error.required == null) {
				$scope.product.containsFilterInfo = true
			} else {
				$scope.product.containsFilterInfo = false
			}

			if($scope.updateMode == false) {

				var product = $scope.product;

				for(var i = 0; i < product.variants.length; i++) {
					delete product.variants[i].collapse;
				}

				if(product.category == 'WATCH') {
					delete product.strap;
					delete product.jewel;
					delete product.other;
				}

				if(product.category == 'STRAP') {
					delete product.watch;
					delete product.jewel;
					delete product.other;
				}

				if(product.category == 'JEWEL') {
					delete product.strap;
					delete product.watch;
					delete product.other;
				}

				if(product.category == 'OTHER') {
					delete product.strap;
					delete product.watch;
					delete product.jewel;
				}

				product.isVerified = false;
				$api.post('products', product)

				.then(function(response) {

					var productId = response.data.product._id;
					alert("Product Added. Uploading Images.")
					if($scope.images.length > 0){
						$scope.upload = true;

						Upload.upload({
							url: 'api/product-image-upload',
							data: {
								productId: productId,
										files: $scope.images //file input field
									}
								})
						.then(function (res) {
								//wanneer uploaden gelukt
								$scope.alert = {
									type: "success",
									msg: "Uw gegevens zijn verzonden!"
								}

								response.data.product.images = angular.copy(res.data.files.images);
								$scope.openProductModal(response.data.product)
								.then(function(shopProduct) {

									$scope.updateMode = true;

									$rootScope.currentShop.products.unshift(shopProduct);
									$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
									alert("Uploaded Images Successfully");
									$state.go('retailer.assortment',{modalFor: $scope.product})

								})

								.catch(function(reason) {
									console.log(reason);
								});
							}, function (res) {
								//wanneer uploaden mislukt
								console.log("Error: ", res.status);
								$scope.alert = {
									type: "danger",
									msg: "upload error: " + res.status
								}
							}, function(evt) {
									//tijdens upload
									var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
									$scope.uploadProgress = progressPercentage;
									console.log("Progress: " + progressPercentage + '%' + evt.config.data);
								});

						$scope.upload = false;
					}


				})

				.catch(function(reason) {
					console.log(reason);
					$scope.alert = {
						type: "danger",
						msg: "Het lukte niet om uw gegevens op te slaan " + reason
					}
					$scope.upload = false;
				})
			} else {
				/* Don't save twice, but update the existing product. */
				// AS IT IS THE RETAILER, DO NOT UPDATE THE PRODUCT. JUST ADD THE PRODUCT AS SUGGESTION TO THE EXISTING PRODUCT
				$api.update('suggest-update-product',{product: $scope.product, shop: $rootScope.currentShop})
				.then(function(success){
					console.log(success.data)
					alert("Suggestion Sent to the admin.!");
				},function(err){
					console.error(err)
				})

				/*getModifiedProperties(function(modifiedProperties){
									var product = angular.copy($scope.product);

									if(modifiedProperties){
										var version = {
											author: {
												name: $rootScope.user.firstName + ($rootScope.user.lastNamePrefix ? " " + $rootScope.user.lastNamePrefix + " " : " ") + $rootScope.user.lastName,
												_id: $rootScope.user._id
											},
											date: new Date(),
											concept: true,
											history: {
												author: {
													name: $rootScope.user.firstName + ($rootScope.user.lastNamePrefix ? " " + $rootScope.user.lastNamePrefix + " " : " ") + $rootScope.user.lastName,
													_id: $rootScope.user._id
												},
												status: "Versie aangemaakt",
												date: new Date()
											}
										}
										product.modifiedProperties = modifiedProperties;
										product.versions.push(version);

										console.log("product", product);
									}

									for(var i = 0; i < product.variants.length; i++) {
										delete product.variants[i].collapse;
									}

									if(product.category == 'WATCH') {
										delete product.strap;
										delete product.jewel;
									}

									if(product.category == 'STRAP') {
										delete product.watch;
										delete product.jewel;
									}

									if(product.category == 'JEWEL') {
										delete product.strap;
										delete product.watch;
									}

									product.isVerified = false;
									product.currentShop = $rootScope.currentShop;

									$api.update('products/' + nameSlug, product)
									.then(function(response) {
										var productId = response.data.updatedProduct._id;
										if($scope.images.length > 0){
											$scope.upload = true;
											Upload.upload({
												url: 'api/product-image-upload',
												data: {
													productId: productId,
													files: $scope.images
												}
											})
											.then(function () {
														//wanneer uploaden gelukt
														$scope.alert = {
															type: "success",
															msg: "Uw gegevens zijn verzonden!"
														}
													}, function (res) {
														//wanneer uploaden mislukt
														console.log("Error: ", res.status);
														$scope.alert = {
															type: "danger",
															msg: "upload error: " + res.status
														}
													}, function(evt) {
															//tijdens upload
															var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
															$scope.uploadProgress = progressPercentage;
															console.log("Progress: " + progressPercentage + '%' + evt.config.data);
														});

											$scope.upload = false;
										}
									})
									.catch(function(reason) {
										console.log(reason);
									})
								})*/
							}
						}
					}]);
