prismanoteApp.controller("retailerLoyaltyPortalController", ['$scope', '$rootScope', '$uibModal', '$api', '$translate', '$http', 'FileSaver', 'Blob', '$retailer', '$stateParams', '$state','$filter',
			'$window',
	function($scope, $rootScope, $uibModal, $api, $translate, $http, FileSaver, Blob, $retailer, $stateParams, $state, $filter,$window) {
	
		$scope.crmUserBindValues =[0,0,0,0,0,0,0,0,0,0,0,0,0];
		$scope.space='  ';
		$scope.repairStatus == '';
		$scope.childFilter = [];
		$scope.hNflag=0;
		$scope.zCflag=0;
		$scope.zc =[];
		$scope.zcc =[];
		$scope.houseNumberArray =[];
		$scope.tabOptions= ['Phone Number','Comments','Product Status','Product Kind','Product Material','Custom Tag','Fillin Date','Repair Date','Retailer Comment','Price','End Warranty','Address','Brand Name','Zipcode,HouseNumber'];
		// $scope.temP();
		// $scope.filteredTodos = [],
		// $scope.currentPage = 1,
		// $scope.numPerPage = 10,
		// $scope.maxSize = 5;
		$scope.currentPage = 0;
		$scope.val;
    	$scope.pageSize = 10;
    	//$scope.data = [];
    	//$scope.q = '';
		$scope.tableOptionschildFilter = [];
		$scope.searchquery = '';
		$scope.childFilterTable='';
		$scope.showFilters=true;
		$scope.showFiltersChld=true;
		$scope.filterTable="";
		$scope.marketingCustomers=[];
		$scope.isFilterDataFl=0;
		$rootScope.houseNumber=[];
		$rootScope.zipcode=[];
		$scope.getData = function () {
			// needed for the pagination calc
			// https://docs.angularjs.org/api/ng/filter/filter
			return $filter('filter')($scope.marketingCustomers, $scope.searchquery)
		   /* 
			 // manual filter
			 // if u used this, remove the filter from html, remove above line and replace data with getData()
			 
			  var arr = [];
			  if($scope.q == '') {
				  arr = $scope.data;
			  } else {
				  for(var ea in $scope.data) {
					  if($scope.data[ea].indexOf($scope.q) > -1) {
						  arr.push( $scope.data[ea] );
					  }
				  }
			  }
			  return arr;
			 */
		  }
		  
		  $scope.numberOfPages=function(){
			  return Math.ceil($scope.getData().length/$scope.pageSize);                
		  }
	  
	  //We already have a limitTo filter built-in to angular,
	  //let's make a startFrom filter

		$scope.getCrmusers = function(){
			$scope.marketingCustomers = null;
            $scope.loading = true;
            
            if ($scope.currentShop != undefined) {
                $api.get('crmusers', { filter: { 'shopSlug': $scope.currentShop.nameSlug }, 'sort': { '_id': 'desc' } })

                    .then(function (response) {
                        console.log(response.data.crmUsers);
                        $scope.marketingCustomers = response.data.crmUsers;
                        $scope.loading = false;
                    })

                    .catch(function (reason) {
                        console.log(reason);
                        $scope.loading = false;

                    });
            }
		}    

		$scope.$on('changeSelectedShop', function() {
			$retailer.getShop()
				.then(function(shop) {		
					$scope.currentShop = shop;
					$scope.getCrmusers();
					
				})
				.catch(function(reason) {
					console.log(reason);
				});
		});

		$scope.saveShop = function(){
			$retailer.setShop($rootScope.selectedShop, $scope.currentShop);
        }

        $scope.exportCSV = function () {
            
            var data = $scope.MakeRequiredJson($scope.marketingCustomers);
            JSONToCSVConvertor(data, "User List", true);
        }

        $scope.MakeRequiredJson = function (data) {
            var pluginArrayArg = new Array();

            for (var i = 0; i < data.length; i++) {

                var jsonArg1 = new Object();
                jsonArg1.Name = data[i].emailName;
                jsonArg1.FilledInOn = data[i].fillindate;
                jsonArg1.Email = data[i].emailName;
                jsonArg1.WarrantyUpto = data[i].endDateWarranty;
                jsonArg1.DateOfBirth = '';
                jsonArg1.ProductType = data[i].watchOrJewel ? 'WATCH' : 'JEWEL';
                jsonArg1.AdvertisingElementGender = data[i].gender ? 'MAN' : 'WOMAN';
                jsonArg1.AdvertisingElementProductType = data[i].watchOrJewel ? 'WATCH' : 'JEWEL';
                jsonArg1.AdvertisingElementPriceInclVat = data[i].priceRange;
                jsonArg1.ImageURL = data[i].photo[0] != undefined ? $rootScope.awsUrl + data[i].photo[0].src : '';
                jsonArg1.SentMail = data[i].sentMail.length;
                jsonArg1.ScheduledMail = data[i].plannedEmail.length;
                jsonArg1.ExportDate = moment().format('DD-MM-YYYY');


                pluginArrayArg.push(jsonArg1);
            }
            return JSON.stringify(pluginArrayArg);
        }


		$scope.openCrmUserInfoModal = function(index, customer){
            console.log("hallo openCrmUserInfoModal controller2", customer);


            //- dit werkt maar even meegegeven index veranderen
            
            console.log("customer", customer);

            // var crmUser = $scope.marketingCustomers[index];


			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/crmuser-info-modal.html',
				controller: 'crmUserInfoModalController',
				size: 'lg',
				resolve: {
					index: function() {
						return index;
					},
					customer: function() {
						return customer;
					}
				}
			});
	
			modalInstance.result.then(function(result){
				if(result){
					$scope.user.socialPortal.campaigns.push(campaign);

					$scope.saveSocialPortal();
					$scope.getPortalCampaigns();

					$scope.alert = {
						type: 'success',
						msg: "De promotie " + campaign.name + " is toegevoegd bij Mijn promoties. <a href='/retailer/campaigns/" + campaign.nameSlug + "'> Bekijk de campagne</a>"
					}
				}
			}, function(){
				//dismissed
			})
		}

		$scope.getSelectProduct = function() {

			$scope.showFiltersChld=false;

			// $scope.selectOpt=selectOpt;
			console.log("sdgfdsgdfgdfgfdgfdgdfgdfg",JSON.stringify($scope.filterTable));
			if($scope.filterTable==''){
			
			}

			if($scope.filterTable=='Product Status'){
				$scope.childFilter = $rootScope.user.productStatus;
			console.log("sdfhhsdffdf",JSON.stringify($rootScope.user));
			}

			if($scope.filterTable=='Product Material') {
				$scope.childFilter = $rootScope.user.productMaterial;
			console.log("sdfhhsdffdf",JSON.stringify($rootScope.user));
			}

			if($scope.filterTable=='Product Kind') {
				$scope.childFilter = $rootScope.user.productKind;
			console.log("sdfhhsdffdf",JSON.stringify($rootScope.user.productKind));
			}

			// if($scope.filterTable=='Zipcode'){
			// 	$scope.childFilter =$rootScope.zipcode;
			// 	console.log("$rootScope.zipcode",JSON.stringify($rootScope.zipcode)); 
			// }
			// if($scope.filterTable=='HouseNumber'){
			// 	$scope.childFilter =$rootScope.houseNumber ;
			// 	console.log("$rootScope.houseNumber",JSON.stringify($rootScope.houseNumber));
			// }


		}

		$scope.commonFilterTable = function() {
			$scope.searchquery=$scope.childFilterTable;
		}

		$scope.getTableOptions = function (data) {
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/create-retailer-tableoptions-modal.html',
			controller: 'tableOptionsController',
			size: 'lg',
			resolve: {
				data: function () {
					return data;
				}
			}
		}).closed.then(function () {
			//	window.alert('Modal closed');
			//$window.location.reload();
			$scope.temP();
			$scope.dropdownValues ($scope.currentShop.nameSlug);
			console.log('$rootScope.user.tableOptions',$rootScope.user.tableOptions);
			$window.location.reload();
			
		
		});

	}

		$scope.repairOrder = function(item){
			if(item.items && item.items.length > 0 && $scope.repairStatus){
				for(var i =0; item.items.length; i++){
					if(item.items[i] && item.items[i].productTypeId === 2 && item.items[i].status && item.items[i].status == $scope.repairStatus){
						return 0;
					}else{
						return 1;
					}
				}
			}
		}
		$scope.refresh = function(){
			
			$window.location.reload(); 

		}
		// $scope.dropdownValues = function(currentShopNameSlug){
			
		// 	$api.get('crmusers', { filter: { 'shopSlug': currentShopNameSlug }, 'sort': { '_id': 'desc' } })
    //    	.then(function (response) {
		// 			console.log("hey");
		// 			console.log("kratika vaid",response.data.crmUsers);
					
		// 			$scope.marketingCustomers = response.data.crmUsers;
		// 			console.log("	$scope.marketingCustomers",	$scope.marketingCustomers);
		// 			for(var i=0;i<$scope.marketingCustomers.length;i++){
		// 				for(var j=0;j<$scope.marketingCustomers[i].items.length;j++){
		// 					console.log( $scope.marketingCustomers[i].items[j].address);
						
							
		// 					 if(($scope.marketingCustomers[i].items[j].address.length != 0)&&($scope.marketingCustomers[i].items[j].address[0].houseNumber !=undefined)){

		// 						$scope.houseNumberArray.push($scope.marketingCustomers[i].items[j].address[0].houseNumber);
							
		// 					 }	
		// 					 console.log("	$scope.houseNumberArray ",		$scope.houseNumberArray );
		// 					if(($scope.marketingCustomers[i].items[j].address.length != 0)&&($scope.marketingCustomers[i].items[j].address[0].postalCode!=undefined)){
							
		// 							$scope.zcc.push($scope.marketingCustomers[i].items[j].address[0].postalCode);
		// 						}
							
		// 						console.log("	$scope.zcc",	$scope.zcc);
						
					
		// 				}
		// 			}
		// 			$rootScope.houseNumber.push($scope.houseNumberArray[0]);
		// 			for (var i=0;i<$scope.houseNumberArray.length;i++){
		// 				for(var j=i+1;j<$scope.houseNumberArray.length;j++){
    //           if($scope.houseNumberArray[i]==$scope.houseNumberArray[j]){
							
		// 						$scope.hNflag++;
							
		// 					}else{
		// 						$rootScope.houseNumber.push($scope.houseNumberArray[j]);
		// 						break;
		// 					}
		// 					break;
		// 				}
		// 				if($scope.hNflag==0)
		// 				$rootScope.houseNumber.push($scope.houseNumberArray[i]);
		// 			}
		// 			console.log("	$rootScope.houseNumber",	$rootScope.houseNumber);

					
		// 		$rootScope.zipcode.push($scope.zcc[0]);
		// 			for (var i=0;i<$scope.zcc.length;i++){
		// 				for(var j=i+1;j<$scope.zcc.length;j++){
    //           if($scope.zcc[i]==$scope.zcc[j]){
							
		// 						$scope.zCflag++;
							
		// 					}else{
		// 						$rootScope.zipcode.push($scope.zcc[j]);
		// 						break;
		// 					}
		// 					break;
		// 				}
		// 				if($scope.zCflag==0)
		// 				$rootScope.zipcode.push($scope.zcc[i]);
		// 			}
		// 			console.log("	$rootScope.zipcode",$rootScope.zipcode);
		// 			$scope.loading = false;
		// 		})

		// 		.catch(function (reason) {
		// 			console.log(reason);
		// 			$scope.loading = false;

		// 		});

		// }

		$scope.temP = function(){
			var flagdd=0;
			var i=0;

			//$scope.showFiltersChld=false;

			for(var tOpt in $rootScope.user.tableOptions) {
				flagdd=0;
				i=0;

				for(var oldTOpt in $scope.tabOptions){
						if($rootScope.user.tableOptions[tOpt]==$scope.tabOptions[oldTOpt]){
							flagdd=1;
							break;
						}
						i++;
				}
				if(flagdd==1){
				console.log($scope.tabOptions[i]);
				}
			}


			for(var tOpt in $rootScope.user.tableOptions) {
				flagdd=0;
				i=0;

				for(var oldTOpt in $scope.tabOptions){
						if($rootScope.user.tableOptions[tOpt]==$scope.tabOptions[oldTOpt]){
							$scope.crmUserBindValues[i]=1; 
							break;
						}
						i++;
				}
				
			}
		
			if($scope.isFilterDataFl==0){
			if($scope.crmUserBindValues[2]==1)
			$scope.tableOptionschildFilter.push('Product Status');
			if($scope.crmUserBindValues[3]==1)
			$scope.tableOptionschildFilter.push('Product Kind');
			if($scope.crmUserBindValues[4]==1)
			$scope.tableOptionschildFilter.push('Product Material');
			// if($scope.crmUserBindValues[13]==1)
			// $scope.tableOptionschildFilter.push('Zipcode,HouseNumber');
			// if($scope.crmUserBindValues[14]==1)
			// $scope.tableOptionschildFilter.push('HouseNumber');
			$scope.isFilterDataFl=1;



			}
			if(($scope.crmUserBindValues[2]!=1)&&($scope.crmUserBindValues[3]!=1)&&($scope.crmUserBindValues[4]!=1)){
			$scope.showFilters=true;
			$scope.isFilterDataFl=0;
			}

			if($scope.tableOptionschildFilter.length>0) {
				$scope.showFilters=false;
				}

      

		}

}]);

prismanoteApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});