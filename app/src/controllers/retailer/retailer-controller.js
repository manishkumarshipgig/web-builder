prismanoteApp.controller("retailerController", ['$scope', '$state', '$rootScope', '$retailer', '$location', '$api',
	function($scope, $state, $rootScope, $retailer, $location, $api) {

		$rootScope.shopSaved = false;

		$scope.closeAlert = function() {
			$rootScope.shopSaved = false;
		};

		$scope.getShop = function(nameSlug) {
			$rootScope.selectedShop = nameSlug;

			$retailer.getShop(nameSlug)

				.then(function(shop) {
					$scope.$broadcast('changeSelectedShop', nameSlug);
					$rootScope.currentShop = shop;
					console.log("$rootScope.currentShop", $rootScope.currentShop);
				})
				
				.catch(function(reason) {
					console.log(reason);
				});
		};
		if($rootScope.user.shops && $rootScope.user.shops.length > 0){
			$scope.getShop($rootScope.user.shops[0].nameSlug);
		}

		//Get active modules for this shop
		$retailer.getShop()
		.then(function(shop) {
			$api.get('get-shop-modules', {shopId: $rootScope.currentShop._id})
			.then(function(response) {
				$rootScope.currentShop.modules = {};
				var modules = response.data.modules;
				for(var i=0; i < modules.length; i++){
					$rootScope.currentShop.modules[modules[i].name] = modules[i].active;
				}
			})
			.catch(function(reason) {
				console.log(reason);
			})
		})
		.catch(function(reason) {
			
			
		})

}]);
