prismanoteApp.controller("retailerOrdersController", ['$scope', '$rootScope', '$uibModal', '$api', '$translate', '$http', 'FileSaver', 'Blob', '$retailer', 'prompt','campaignFact',
function($scope, $rootScope, $uibModal, $api, $translate, $http, FileSaver, Blob, $retailer, prompt,campaignFact) {

	$scope.loadOrders = function(filter){
		$scope.loading = true;
		$scope.orders = null;
		console.log($scope.currentShop);
		$api.get('retailer-order-list', {nameSlug: $scope.currentShop.nameSlug, status: filter})
			.then(function(response) {
				$scope.orders = response.data.orders;
				console.log("$scope.orders", $scope.orders);
				$scope.loading = false;
			})
			.catch(function(reason) {
				console.log(reason);
				$scope.loading = false;
			});
	}
	
	$scope.downloadPackageSlip = function(order){
		//Dit doen we even met $htpp omdat de $api service vooralsnog geen responseType ondersteund.
		$http({
			method: 'POST',
			url: '/api/package-slip',
			data: {
				orderId: order._id
			},
			responseType : 'arraybuffer'
		})
		.then(function(response){
			var file = new Blob([response.data], {type: 'application/pdf'});
			FileSaver.saveAs(file, "Order " + order.number + ".pdf");
		}, function(response){
			console.log("ERROR", response);
		})
	}

	$scope.openReturnOrderModal = function(order){
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/return-order-modal.html',
			controller: 'returnOrder-modalController',
			resolve: {
				order: function(){
					return order;
				}
			}
		});

		modalInstance.result.then(function(result){
			if(result){
				//ready
			}
		}, function(){
			//dismissed
		})
	};

	$scope.openSendOrderModal = function(order){
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/send-order-modal.html',
			controller: 'sendOrder-modalController',
				resolve: {
					order: function() {
						return order;
					}
				}
		});

		modalInstance.result.then(function(result){
			if(result){
				// $scope.changeOrderFilter();
			}
		}, function() {
			//Modal is dismissed
		});
	}

	$retailer.getShop()
		.then(function(shop) {	
			$scope.currentShop = shop;
			$scope.loadOrders('Paid');
			$scope.activeTab=0;
			
		})
		.catch(function(reason) {
			console.log(reason);
		});

	$scope.$on('changeSelectedShop', function() {
		$retailer.getShop()
			.then(function(shop) {		
				$scope.currentShop = shop;
				$scope.loadOrders('Paid');
				$scope.activeTab=0;
				
			})
			.catch(function(reason) {
				console.log(reason);
			});
	});

	$scope.cancelOrder = function(order){
		//TODO: Create a model where the retailer should enter an reason for the cancellation
		$scope.alert = null;
		prompt({
			title: 'Order annuleren',
			message: 'Weet u zeker dat u deze order wilt annuleren?'
		}).then(function() {
			$api.post('cancel-order', {orderNumber: order.number, shopRequest: true})
			.then(function(response){
				$scope.alert = {
					type: 'success',
					msg: "Order cancelled"
				}
			})
			.catch(function(reason){
				console.log("reason", reason);
				$scope.alert = {
					type: 'danger',
					msg: "Error while cancelling the order: " + reason
				}
			})
		});
	}

	$scope.closeAlert =  function(){
		$scope.alert = null;
	}
	
}]);

