prismanoteApp.controller("retailerAssortmentController", ['$stateParams','$scope', '$rootScope', '$uibModal', '$api', '$q', '$translate', '$retailer', 'prompt',
	function($stateParams, $scope, $rootScope, $uibModal, $api, $q, $translate, $retailer, prompt) {
		
		doActivate();
		function doActivate(){

			$api.get('webshop-products-categories/' + $rootScope.currentShop.nameSlug,{})
			.then(function(response){
				$scope.shopCats = response.data.cats;
				$scope.setCategory($scope.shopCats[0]);
				if($stateParams.modalFor != undefined)
					$scope.updateShopProduct($stateParams.modalFor, true)
			},function(err){
				console.error("There was an error while getting the categories for this shop",err);
			});

		}


		$scope.currentPage = 0;
		// Array with all currently displayed items in /products
		$scope.products = [];
		$scope.collections = [];

		$scope.selection = [];

		$scope.productCategory = [];

		$scope.searchMode = "exact";

		$scope.hideFilters = true;
		$scope.filtersShow = function(){
			if($scope.hideFilters){
				$scope.hideFilters = false;
			}else{
				$scope.hideFilters = true;
			}
		}

		$scope.selectItem = function(nameSlug, index) {
			var indexOf = $scope.selection.indexOf(nameSlug);
			if(indexOf != -1) {
				$scope.selection.splice(indexOf, 1);
			} else {
				$scope.selection.push(nameSlug);
			}
			$scope.products[index].selected = !$scope.products[index].selected;
		}

		$scope.limit = 24;
		$scope.sortShopProducts = {};

		$scope.setLimit = function(limit) {
			if(limit != null) {
				$scope.limit = limit;
			} else {
				$scope.limit = 24;
			}
		}

		$scope.productCategory = 'WATCH';
		$scope.filtersWatch = true;
		$scope.filtersStrap = false;
		$scope.filtersJewel = false;

		$scope.setCategory = function(category) {

			$scope.products = [];

			$scope.productCategory = [category];

			if($scope.productCategory == 'WATCH'){
				$scope.filtersWatch = true;
				$scope.filtersStrap = false;
				$scope.filtersJewel = false;
			}else if ($scope.productCategory == 'STRAP'){
				$scope.filtersWatch = false;
				$scope.filtersStrap = true;
				$scope.filtersJewel = false;
			}else if($scope.productCategory == 'JEWEL'){
				$scope.filtersWatch = false;
				$scope.filtersStrap = false;
				$scope.filtersJewel = true;
			}else if($scope.productCategory == 'OTHER'){
				$scope.filtersWatch = false;
				$scope.filtersStrap = false;
				$scope.filtersJewel = false;
			}

			$scope.getProducts({filter: {category: $scope.productCategory}}, {reset: true})
			.catch(function(reason) {
				console.error(reason);
			});
		}

		// format kids, male and female booleans into a translated and formatted gender string
		var formatGender = function(male, female, kids) {
			return $q(function(resolve) {

				if(kids == false) {
					if(male == true && female == true) {
						$translate('GENTS').then(function(gents) {
							$translate('LADIES').then(function(ladies) {
								resolve(gents + ' / ' + ladies);
							});
						});
					} else if(male == true && female == false) {
						$translate('GENTS').then(function(gents) {
							resolve(gents);
						});
					} else if(male == false && female == true) {
						$translate('LADIES').then(function(ladies) {
							resolve(ladies);
						});
					}
				} else {
					if(male == true && female == true) {
						$translate('BOYS').then(function(boys) {
							$translate('GIRLS').then(function(girls) {
								resolve(boys + ' / ' + girls);
							});
						});
					} else if(male == true && female == false) {
						$translate('BOYS').then(function(boys) {
							resolve(boys);
						});
					} else if(male == false && female == true) {
						$translate('GIRLS').then(function(girls) {
							resolve(girls);
						});
					}
				}
			})
		};

		// Async function to add/format gender, shop name and possibly other additional properties later on
		var formatProduct = function(product) {
			return $q(function(resolve, reject) {

				formatGender(product.male, product.female, product.kids)

				.then(function(formattedGender) {
					product.shop = {name: $rootScope.currentShop.name, nameSlug: $scope.nameSlug};
					product.gender = formattedGender;
					resolve(product);
				})

				.catch(function(reason) {
					reject(reason);
				});
			});
		};

		// $scope.filterChecker[''] = [];
    // Loading Filter For Classes
		$scope.filterChecker = {
			brandsInProds : []
		};

		$scope.loadCurrentProductsFilter = function(){
			$scope.filterChecker.brandsInProds = $.unique($scope.products.map(function (d) {return d.brand._id;}));
		}

		$scope.getTheCollectionName = function(collection){
			var collectionName = "";

			if(collection.hasOwnProperty($scope.language))
				collectionName = collection[$scope.language].name;
			else if(collection.hasOwnProperty("en.name"))
				collectionName = collection.en.name;
			else if(collection.hasOwnProperty("name") && collection.name != "")
				collectionName = collection.name;
			else
				collectionName = "NO NAME";


			return collectionName;
		}
		// Get new products from the database and add them to the $scope
		$scope.getProducts = function(params, options) {
			return $q(function(resolve, reject) {
				$scope.loading = true;
				
				if(!params) {
					params = {};
				}
				if(!params.sort) {
					params.sort = {dateLastModified: 'desc'};
				}
				if(!params.filter) {
					params.filter = {};
				}
				if(!params.filter._id) {
					params.filter._id = {};
				}

				if(params.sort.price == 'asc' || params.sort.price == 'desc') {
					$scope.sortShopProducts.price = params.sort.price;
				} else if(params.sort.dateAdded == 'desc' || params.sort.dateAdded == 'asc') {
					$scope.sortShopProducts.dateAdded = params.sort.dateAdded;
				} else if(params.sort != {}) {
					$scope.sortShopProducts = {};
				}

				params.filter.includeUnverified = true;
				params.filter.isVerified = undefined;

				// Delete the loaded products and load them again with the new apiParams
				if(options != null && typeof options === 'object') {

					if(options.reset === true) {
						$scope.products = [];
					}
				}

				params.offset = $scope.products.length;

				params.limit = $scope.limit;

				var ids = [];

				for(var i = 0; i < $rootScope.currentShop.products.length; i++) {
					// Add the shop product, remove if it does not match the filters.
					ids.push($rootScope.currentShop.products[i]._id);

					if(params.filter.price && (params.filter.price.$gte != null || params.filter.price.$lte != null) && ($rootScope.currentShop.products[i].price < params.filter.price.$gte || $rootScope.currentShop.products[i].price > params.filter.price.$lte)) {
						ids.pop();
					} else if(params.filter.discount == true && $rootScope.currentShop.products[i].discount == 0) {
						ids.pop();
					} else if(params.filter.inStock == true && $rootScope.currentShop.products[i].stock == 0) {
						ids.pop();
					} else if(params.filter.isBestseller == true && $rootScope.currentShop.products[i].isBestseller != true) {
						ids.pop();
					} else if(params.filter.show == true && $rootScope.currentShop.products[i].show != true) {
						ids.pop();
					}
				}
				delete params.filter.price;
				delete params.filter.discount;
				delete params.filter.inStock;
				delete params.filter.isBestseller;
				delete params.filter.show;
				//params.filter._id.$in = ids;

				$scope.productCount = ids.length;

				if(params.filter.category && params.filter.category.$in) {
					$scope.productCategory = params.filter.category.$in;
				} else {
					$scope.productCategory = [];
				}
				$api.get('webshop-products/' + $rootScope.currentShop.nameSlug, params)
				.then(function(response){
					if($scope.products.length == 0 && response.data.products.length == 0)
						$scope.noSuchProductsFound = true;
					else
						$scope.noSuchProductsFound = false;

					if(response.data.products.length < $scope.limit)
						$scope.showloadmore = false;
					else
						$scope.showloadmore = true;

					for(var i = 0; i < response.data.products.length; i++) {
						formatProduct(response.data.products[i])
						.then(function(formattedProduct) {
							var matchedShopProduct = _.find($rootScope.currentShop.products, {_id: formattedProduct._id});
							var mergedProduct = Object.assign(formattedProduct, matchedShopProduct);
							// mergedProduct['price'] = matchedShopProduct.price;

							if(options != null && typeof options === 'object' && options.featured === true) {
								$scope.featuredProducts.push(mergedProduct);
							} else {
								if(checkIfAlreadyExist(mergedProduct._id))
									$scope.products.push(mergedProduct);
							}

							function checkIfAlreadyExist(id){
								for(var j = 0; j < $scope.products.length; j++){
									if($scope.products[j]._id == id){
										return false;
									}
								}
								return true;
							}

							$scope.loadCurrentProductsFilter($scope.products);
							if($scope.products.length == params.offset + response.data.products.length) {
								if($scope.sortShopProducts != null) {
									if(typeof $scope.sortShopProducts.price === 'string') {
										if($scope.sortShopProducts.price == 'asc') {
											$scope.products.sort(function(a,b) { return a.price - b.price; });
										} else if($scope.sortShopProducts.price == 'desc') {
											$scope.products.sort(function(a,b) { return b.price - a.price; });
										}
									} else if(typeof $scope.sortShopProducts.dateAdded === 'string') {
										if($scope.sortShopProducts.dateAdded == 'asc') {
											$scope.products.sort(function(a,b) { return a.dateAdded - b.dateAdded; });
										} else if($scope.sortShopProducts.dateAdded == 'desc') {
											$scope.products.sort(function(a,b) { return b.dateAdded - a.dateAdded; });
										}
									}
								}
								$scope.loading = false;
							}
						})
						.catch(function(reason) {
							console.error(reason);
							reject(reason);
						});
					}

					$scope.noProductsFound = false;

					resolve();

				})

				.catch(function(reason) {

					$scope.noProductsFound = true;

					reject(reason);
				});
			});
};

var updateShop = function(result) {
	$scope.products[$scope.products.findIndex(function(element) { return element._id == result._id; })] = result;
    $rootScope.currentShop.products[$rootScope.currentShop.products.findIndex(function(element) { return element._id == result._id; })] = {
		_id: result._id,
		stock: result.stock,
		price: result.price,
		dropshippingPrice: result.price,
		discount: result.discount,
		show: result.show,
		isBestseller: result.isBestseller,
		isFeatured: result.isFeatured,
		collections: result.collections,
		countrId: result.countrId
	};
	$retailer.setShopProducts($rootScope.selectedShop, $rootScope.currentShop.products);
};

$scope.updateShopProduct = function(result, modal) {
	if(result != null) {
		if(modal === true) {
			$scope.openProductModal(result)
			.then(function(result) {

				$scope.products[$scope.products.findIndex(function(element) { return element._id == result._id; })] = result;
				$rootScope.currentShop.products[$rootScope.currentShop.products.findIndex(function(element) { return element._id == result._id; })] = {
					_id: result._id,
					stock: result.stock,
					price: result.price,
					dropshippingPrice: result.price,
					discount: result.discount,
					show: result.show,
					isBestseller: result.isBestseller,
					isFeatured: result.isFeatured,
					collections: result.collections,
					countrId: result.countrId
				};
			})

			.catch(function(reason) {
				console.error('Modal dismissed. Reason: ' + reason);
			});
		} else {
			updateShop(result);
		}
	}
};

$scope.addShopProduct = function(product) {

	var productIndex = $rootScope.currentShop.products.findIndex(function(element) { return element._id == product._id; });
			// If the product does not exist yet in the shop products db:
			if(productIndex == -1) {

				$scope.openProductModal(product)
				.then(function(shopProduct) {

					$rootScope.currentShop.products.unshift(shopProduct);
					var mergedProduct = Object.assign(product, shopProduct);
					$scope.products.unshift(mergedProduct);
					$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
					$scope.products.pop();
				})

				.catch(function(reason) {
					console.error(reason);
				});

			// Product already exists in shop products db.
		} else {
			$scope.updateShopProduct($rootScope.currentShop.products[productIndex], true);
		}
	}

	$scope.deleteShopProduct = function(id) {
		var productBrand = $scope.products[$scope.products.findIndex(function(element) { return element._id == id; })].brand;
		var brand = $rootScope.currentShop.brands.find(function(brand) {return brand.nameSlug == productBrand.nameSlug}) || null;
		if(brand != null && brand.restricted) {
			prompt({
				title: 'Product verwijderen?',
				message: 'U bent officieel dealer van dit merk, dus het product blijft in uw assortiment. U kunt wel besluiten dit product niet meer te verkopen door deze onzichtbaar te maken voor bezoekers en de voorraad op 0 zetten. Wilt u het product inderdaad niet meer verkopen?'
			}).then(function() {
				var scopeIndex = $scope.products.findIndex(function(element) { return element._id == id; });
				var product = $scope.products[scopeIndex];
				product.stock = 0;
				product.show = false;
				$scope.products[scopeIndex] = product;

				var rootScopeIndex = $rootScope.currentShop.products.findIndex(function(element) { return element._id == id; });
				$rootScope.currentShop.products[rootScopeIndex] = product;
				$scope.deleteProduct(product);
			})

		} else {
			prompt({
				title: 'Product verwijderen?',
				message: 'Weet u zeker dat u dit product uit uw assortiment wilt verwijderen? U kunt dit product helemaal uit uw assortiment verwijderen óf het product onzichtbaar maken voor bezoekers en de voorraad op 0 zetten.',
				buttons: [
				{label:'Product verwijderen', primary: true, id: 0},
				{label:'Verbergen en voorraad op 0 zetten', primary: true, id: 1},
				{label:'Annuleren', primary: false, cancel: true, id: 2}
				]
			}).then(function(value) {
				if(value.id === 0) {
					$scope.products.splice($scope.products.findIndex(function(element) { return element._id == id; }), 1);
					$rootScope.currentShop.products.splice($rootScope.currentShop.products.findIndex(function(element) { return element._id == id; }), 1);
					$retailer.setShopProducts($rootScope.selectedShop, $rootScope.currentShop.products);
				} else if(value.id === 1) {
					var scopeIndex = $scope.products.findIndex(function(element) { return element._id == id; });
					var product = $scope.products[scopeIndex];
					product.stock = 0;
					product.show = false;
					$scope.products[scopeIndex] = product;

					var rootScopeIndex = $rootScope.currentShop.products.findIndex(function(element) { return element._id == id; });
					$rootScope.currentShop.products[rootScopeIndex] = product;
					$scope.deleteProduct(product);
				}

			});
		}
	}

	$scope.deleteProduct = function(product){
		console.log("selected", Products);
		//TODO: complete this function to delete the product from the shop
		//TODO: delete product in Countr when needed
	}


	$scope.openProductModal = function(product) {
		return $q(function(resolve, reject) {

			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/retailer-product-modal.html',
				controller: 'retailerProductModalController',
				size: 'lg',
				resolve: {
					product: function() {
						product.shippingCosts = $rootScope.currentShop.shippingCosts;
						return product;
					}
				}
			});

			modalInstance.result

			.then(function(response) {
				resolve(response);
			})

			.catch(function(reason) {
				reject(reason);
			});
		});
	};

	$scope.getShopProducts = function(nameSlug) {
		$retailer.getShopProducts(nameSlug, null)

		.then(function(shopProducts) {
			$rootScope.currentShop.products = shopProducts;

			$scope.getProducts(null, {reset: true});
		})

		.catch(function(reason) {

			console.error(reason);
		});
	}

	$scope.$on('changeSelectedShop', function() {
		$scope.getShopProducts($rootScope.selectedShop, null);
		$scope.resetCollectionFilter();
	});

	$scope.updateShop = function(){
		$retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
	}

	$scope.openCreateCollectionModal = function(){
		collection = null;
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/create-collection-modal.html',
			controller: 'createCollectionModalController',
			size: 'lg',
			resolve: {
				collection: function() {
						// collection.shippingCosts = $rootScope.currentShop.shippingCosts;
						// return collection;
					}
				}
			});

		modalInstance.result.then(function(result){

			for(var i = result.length - 1; i >= 0; i--) {
				if(array[i] === tasks) {
					array.splice(i, 1);
				}
			}

			//if($rootScope.checkModule('countr')){
			if($rootScope.currentShop.countr && $rootScope.currentShop.countr.username && $rootScope.currentShop.countr.accessToken){
				//Sync this collection with Countr
				$api.put('countr/categories', {
					shopId: $rootScope.currentShop._id,
					category: result,
					language: $rootScope.language
				}).then(function(response) {
					result.countrId = response.data.category._id;
					$scope.addShopCollection(result);
				})
				.catch(function(reason) {
					$scope.addShopCollection(result);
					console.error(reason);
				})
			}else{
				$scope.addShopCollection(result);
			}


		}, function(){

		})
	}

		// Get collections from the database and add them to the $scope
		$scope.getCollections = function(params, options) {
			return $q(function(resolve, reject) {

				$scope.loading = true;

				if(!params) {
					params = {};
				}

				$scope.collections = [];

				params.offset = $scope.collections.length;
				params.limit = $scope.limit;

				var ids = [];

				$api.get('collections', params)

				.then(function(response) {
					$scope.assortmentShop = {
						collections : []
					};
					response.data.collections.forEach(function(val){
						if(val.isVerified == true){
							$scope.assortmentShop.collections.push(val);
						}else{
						}
					})
						// Loop through newly fetched collections and add them to the $scope.
						for(var i = 0; i < response.data.collections.length; i++) {

							formatCollection(response.data.collections[i])

							.then(function(formattedCollection) {

								var matchedShopCollection = _.find($rootScope.currentShop.collections, {_id: formattedCollection._id});
								var mergedCollection = Object.assign(formattedCollection, matchedShopCollection);
								mergedCollection.price = matchedShopCollection.price;

								if(options != null && typeof options === 'object' && options.featured === true) {
									$scope.featuredCollections.push(mergedCollection);
								} else {
									$scope.collections.push(mergedCollection);
								}

								if($scope.collections.length == params.offset + response.data.collections.length) {
									if($scope.sortShopCollections != null) {
										if(typeof $scope.sortShopCollections.price === 'string') {
											if($scope.sortShopCollections.price == 'asc') {
												$scope.collections.sort(function(a,b) { return a.price - b.price; });
											} else if($scope.sortShopCollections.price == 'desc') {
												$scope.collections.sort(function(a,b) { return b.price - a.price; });
											}
										} else if(typeof $scope.sortShopCollections.dateAdded === 'string') {
											if($scope.sortShopCollections.dateAdded == 'asc') {
												$scope.collections.sort(function(a,b) { return a.dateAdded - b.dateAdded; });
											} else if($scope.sortShopCollections.dateAdded == 'desc') {
												$scope.collections.sort(function(a,b) { return b.dateAdded - a.dateAdded; });
											}
										}
									}
									$scope.loading = false;
								}
							})

							.catch(function(reason) {
								reject(reason);
							});
						}
						resolve();

					})

				.catch(function(reason) {
					$scope.noCollectionsFound = true;

					reject(reason);
				});
			});
		};
		$scope.getCollections();

		$scope.addShopCollection = function(collection) {
			$api.post('collections', collection)
			.then(function(response) {
				if(!$rootScope.currentShop.collections){
					$rootScope.currentShop.collections = [];
				}
				$rootScope.currentShop.collections.push(response.data.collection);

				$scope.updateShop();
			})
		}

		$scope.openCollectionModal = function(collection) {
			return $q(function(resolve, reject) {

				var modalInstance = $uibModal.open({
					templateUrl: '../views/modal/create-collection-modal.html',
					controller: 'createCollectionModalController',
					size: 'lg',
					resolve: {
						collection: function() {
							// collection.shippingCosts = $rootScope.currentShop.shippingCosts;
							// return collection;
						}
					}
				});

				modalInstance.result

				.then(function(response) {
					resolve(response);
				})

				.catch(function(reason) {
					reject(reason);
				});
			});
		};

		$scope.updateShopCollection = function(result, modal) {
			if(result != null) {
				if(modal === true) {
					$scope.openCollectionModal(result)

					.then(function(result) {
						updateShop(result);

						//if($rootScope.checkModule('countr')){
						if($rootScope.currentShop.countr && $rootScope.currentShop.countr.username && $rootScope.currentShop.countr.accessToken){
							//Sync this collection with Countr
							$api.put('countr/categories', {
								shopId: $rootScope.currentShop._id,
								category: result,
								language: $rootScope.language
							}).then(function(response) {
								result.countrId = response.data.category._id;
								updateShop(result);
							})
							.catch(function(reason) {
								console.error(reason);
							})
						}
					})
					.catch(function(reason) {
						console.error('Modal dismissed. Reason: ' + reason);
					});
				} else {
					updateShop(result);
				}
			}
		};

		$scope.filterFn = function (item) {
			// must have array, and array must be empty
			return item.categories && item.categories.length ===0;
		};

		$scope.editCollection = function(collection) {
			var index = _.findIndex($scope.currentShop.collections, {'_id': collection._id});

			var modalInstance = $uibModal.open({
				templateUrl: '../views/modal/create-collection-modal.html',
				controller: 'createCollectionModalController',
				resolve: {
					collection: function() {
						return collection;
					}
				}
			});

			modalInstance.result.then(function(result){
				if(result){
					if(result.delete){
						$scope.currentShop.collections.splice(index, 1);
						$retailer.setShop($rootScope.selectedShop, $scope.currentShop);
						if(result.countrId){
							$api.delete('countr/categories/' + result.countrId + '/' + $rootScope.currentShop._id, {})
							.then(function(response) {

							})
							.catch(function(reason) {
								console.error(reason);
							})
						}
						return
					}

					//if($rootScope.checkModule('countr')){
					if($rootScope.currentShop.countr && $rootScope.currentShop.countr.username && $rootScope.currentShop.countr.accessToken){
					//Sync this collection with Countr
					$api.put('countr/categories', {
						shopId: $rootScope.currentShop._id,
						category: result,
						language: $rootScope.language
					}).then(function(response) {
						result.countrId = response.data.category._id;
						$scope.currentShop.collections[index] = result;
						$retailer.setShop($rootScope.selectedShop, $scope.currentShop);
					})
					.catch(function(reason) {
						console.error(reason);
					})
				}else{
					$scope.currentShop.collections[index] = result;
					$retailer.setShop($rootScope.selectedShop, $scope.currentShop);
				}
			}
		}, function() {
				//Modal is dismissed
			});
		}

		$scope.collectionId = null;

		$scope.collectionFilter = function(item){
			if($scope.collectionId === undefined || !$scope.collectionId){
				return true;
			}
			var match = false;

			angular.forEach(item.collections, function(collection){
				if(collection._id === $scope.collectionId){
					match = true;
				}
			})

			return match;
		}

		$scope.setCollectionId = function(collection){
			$scope.collectionId = collection._id;
		}

		$scope.resetCollectionFilter = function(){
			$scope.collectionId = null;
		}
	}]);
