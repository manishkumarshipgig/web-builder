prismanoteApp.controller("retailerUpgradeController", ['$scope', '$rootScope', '$retailer', '$api', '$timeout', '$state', 'prompt', '$window', '$stateParams', '$uibModal','$countr',
function($scope, $rootScope, $retailer, $api, $timeout, $state, prompt, $window, $stateParams, $uibModal, $countr) {
    
    if($stateParams.return && $stateParams.orderid && !$scope.callbackCalled){
        //Mollie redirect, handle some things
        $scope.orderId = $stateParams.orderid;
        $scope.callbackCalled = true;
        mollieCallback();
    }

    $scope.package = {};

    $scope.packages = [
        {
            name: 'Social Media',
            desc: 'Gebruik maken van marketing acties van uw leveranciers en kassa',
            price: 0.00,
            suggested: false,
            contents: [
                'Social Media onbeperkt (betaling aan Facebook)',
                'Leveranciers',
                'CRM Tool tot 15 klanten',
                'Demo website/shop',
                'Labels printen'
            ],
            totalPrice: 0.00,
            code: 'FREE',
            options: [
                {
                    name: 'Countr Kassa',
                    options: [
                        {
                            name: 'Basic',
                            price: 49.00,
                            code: "COUNTRB"
                        },{
                            name: 'Product synchronisatie',
                            price: 59.00,
                            code:'COUNTRP'
                        }
                    ]
                },
                {
                    name: "Marketing ondersteuning",
                    options: [
                        {
                            name: '30 minuten',
                            price: 25,
                            code: 'MARKT30'
                        },
                        {
                            name: '60 minuten',
                            price: 50,
                            code: 'MARKT60'
                        },
                        {
                            name: '120 minuten',
                            price: 100,
                            code: 'MARKT120'
                        },
                        {
                            name: '240 minuten',
                            price: 200,
                            code: 'MARKT240'
                        }
                    ]
                }
            ]
        }, {
            name: 'Winkel basis',
            desc: 'Website, kassa, reparaties en leveranciers',
            price: 24.95,
            suggested: false,
            contents: [
                'Social Media onbeperkt (betaling aan Facebook)',
                'Leveranciers en inkoop',
                'CRM Tool tot 100 klanten',
                'Website',
                'Labels printen'
            ],
            totalPrice: 0.00,
            code: 'STOREB',
            options: [
                {
                    name: 'Countr Kassa',
                    options: [
                        {
                            name: 'Basic',
                            price: 49.00,
                            code: 'COUNTRB'
                        },{
                            name: 'Product synchronisatie',
                            price: 59.00,
                            code: 'COUNTRP'
                        },{
                            name: 'CRM + Product synchronisatie',
                            price: 99,
                            code: 'COUNTRC'
                        }
                    ]
                },
                {
                    name: "Marketing ondersteuning",
                    options: [
                        {
                            name: '30 minuten',
                            price: 25,
                            code: 'MARKT30'
                        },
                        {
                            name: '60 minuten',
                            price: 50,
                            code: 'MARKT60'
                        },
                        {
                            name: '120 minuten',
                            price: 100,
                            code: 'MARKT120'
                        },
                        {
                            name: '240 minuten',
                            price: 200,
                            code: 'MARKT240'
                        }
                    ]
                }
            ]
        },{
            name: 'Professioneel',
            desc: 'Webshop, Kassa en reparaties',
            price: 49.95,
            suggested: true,
            contents: [
                'Social Media onbeperkt (betaling aan Facebook)',
                'Leveranciers en inkoop',
                'CRM Tool tot 300 klanten',
                'Website en webshop',
                'Labels printen'
            ],
            totalPrice: 0.00,
            code: 'STOREP',
            options: [
                {
                    name: 'Countr Kassa',
                    options: [
                        {
                            name: 'Basic',
                            price: 49.00,
                            code: 'COUNTRB'
                        },{
                            name: 'Product synchronisatie',
                            price: 59.00,
                            code: 'COUNTRP'
                        },{
                            name: 'CRM + Product synchronisatie',
                            price: 99,
                            code: 'COUNTRC'
                        }
                    ]
                },
                {
                    name: "Marketing ondersteuning",
                    options: [
                        {
                            name: '30 minuten',
                            price: 25,
                            code: 'MARKT30'
                        },
                        {
                            name: '60 minuten',
                            price: 50,
                            code: 'MARKT60'
                        },
                        {
                            name: '120 minuten',
                            price: 100,
                            code: 'MARKT120'
                        },
                        {
                            name: '240 minuten',
                            price: 200,
                            code: 'MARKT240'
                        }
                    ]
                }
            ]
        },
        {
            name: 'Winkel expert',
            desc: 'Onbeperkt klanten en producten toevoegen',
            price: 99.99,
            suggested: false,
            contents: [
                'Social Media onbeperkt (betaling aan Facebook)',
                'Leveranciers en inkoop',
                'CRM Tool onbeperkt',
                'Website en webshop',
                'Labels printen'
            ],
            totalPrice: 0.00,
            code: 'STOREE',
            options: [
                 {
                    name: 'Countr Kassa',
                    options: [
                        {
                            name: 'Basic',
                            price: 49.00,
                            code: 'COUNTRB'
                        },{
                            name: 'Product synchronisatie',
                            price: 59.00,
                            code: 'COUNTRP'
                        },{
                            name: 'CRM + Product synchronisatie',
                            price: 99,
                            code: 'COUNTRC'
                        }
                    ]
                },
                {
                    name: "Marketing ondersteuning",
                    options: [
                        {
                            name: '30 minuten',
                            price: 25,
                            code: 'MARKT30'
                        },
                        {
                            name: '60 minuten',
                            price: 50,
                            code: 'MARKT60'
                        },
                        {
                            name: '120 minuten',
                            price: 100,
                            code: 'MARKT120'
                        },
                        {
                            name: '240 minuten',
                            price: 200,
                            code: 'MARKT240'
                        }
                    ]
                }
            ]
        }
    ]

    $scope.upgrade = {};
    $scope.initial = true;

    // $scope.prices = {
    //     countr: $scope.levelCashRegister,
    //     marketing: 25,
    //     marketingPerMinute: 25/30,
    //     socialmedia: 0,
    //     crm: 5,
    //     webshop1: 24.95,
    //     webshop2: 49.95,
    //     labels: 0,
    //     basicCashRegister: 45,
    //     crmConnectedCashRegister: 75,
    //     productConnectedCashRegister: 99,
    // }
    // $scope.minutes = "30";
    // var marketingPrice = function(){
    //     return $scope.minutes * $scope.prices.marketingPerMinute
    // }
    // $scope.prices.marketing = marketingPrice();

    $scope.$on('changeSelectedShop', function() {
        $scope.alert = null;
        $scope.getShopModules();
        $retailer.getShop()
            .then(function(shop) {		
                $rootScope.currentShop = shop;    
            })
            .catch(function(reason) {
                console.log(reason);
            });
    });

    $scope.getShopModules = function(){
        return;
        $scope.loading = true;
        $scope.upgrade = {};
        $scope.initial = true;
        $scope.originalTotal = 0;
        $scope.total =0;
        $retailer.getShop()
		.then(function(shop) {	
			$scope.currentShop = shop;
			$api.get('get-shop-modules', {shopId: $scope.currentShop._id})
        	.then(function(response) {
                var counter =0;
                for(var i =0; i < response.data.modules.length; i++){
                    var mod = response.data.modules[i];
                    
                    if(mod.active){
                        if(mod.name === "website"){
                            $scope.upgrade["webshop"] = 1;
                        }else if(mod.name === "webshop"){
                            $scope.upgrade["webshop"] = 2;
                        }else{
                            $scope.upgrade[mod.name] = true;
                        }
                        if(mod.name == "marketing"){
                            $scope.minutes = mod.value;
                            $scope.prices.marketing = marketingPrice();
                        } 
                    }
                    counter++;
                    if(counter == response.data.modules.length){
                        $scope.getCurrentValues();
                        //use timout to avoid double prices in the calculation
                        $timeout(function(){
                            $scope.initial = false;
                            $scope.loading = false;
                        }, 1000)
                    }
                }
                
            })
            .catch(function(reason) {
                $scope.initial = false;
                $scope.loading = false;
                console.log(reason);
            })
			
		})
		.catch(function(reason) {
            $scope.loading = false;
			console.log(reason);
		});
    }

    $scope.getShopModules();

    // function getPrices(callback){
    //     var modules = [];
    //     var counter =0;
    //     angular.forEach($scope.upgrade, function(value, key){
    //         function getKey(key){
    //             if(key == "webshop" && value == 1){
    //                 return "website";
    //             }else if(key == "webshop" && value == 2){
    //                 return "webshop";
    //             }else{
    //                 return key;
    //             }
    //         }
    //         function getValue(key){
    //             if(key == "marketing"){
    //                 return $scope.minutes;
    //             }else{
    //                 return null;
    //             }
    //         }
    //         function getPrice(key, value){
    //             if(key == "webshop" && (value == 1 || value == 2)){
    //                 console.log(key, value);
    //                 return $scope.prices[key+value];
    //             }else{
    //                 return $scope.prices[key];
    //             }
    //         }

    //         if((key == "webshop" && typeof(value) != 'boolean') || (key !== "webshop" && typeof(value) == 'boolean' && value === true)){
    //             var mod = {
    //                 name: getKey(key),
    //                 amount: getPrice(key, value),
    //                 value: getValue(key)
    //             };
    //             modules.push(mod);
    //         }
    //         counter++;
    //         if(counter == Object.keys($scope.upgrade).length){
    //             return callback(modules);
    //         }
    //     })
    // }

    $scope.cancelSubscription = function(){
        console.log("cancelSubscription");
        $scope.loading = true;
        $retailer.getShop()
		.then(function(shop) {
            $api.post('cancel-subscription', {shopId: shop._id})
            	.then(function(response) {
                    console.log("response", response);
                    $scope.loading = false;
                    $scope.alert = {
                        type: "success",
                        msg: "Uw abonnement is geanuleerd"
                    }
                })
                .catch(function(reason) {
                    console.log(reason);
                    $scope.loading = false;
                    $scope.alert = {
                        type: "danger",
                        msg: err
                    }
                })
        })
        .catch(function(reason) {
            console.log(reason);
        })
        
    }


    $scope.getCurrentValues = function(){
        angular.forEach($scope.upgrade, function(value, key){
            if(key == "webshop"){
                key = key + value;
                value = value == 1 || value == 2;
            }
            $scope.calculate(value, $scope.prices[key]);
        })
    }  

    // $scope.$watch('upgrade.webshop', function(newValue, oldValue){ 
    //     if(!$scope.initial){
    //         if(newValue == oldValue){ return; }
    //         if(!newValue){
    //             var oldPrice = $scope.prices['webshop' + oldValue];
    //             $scope.calculate(false, oldPrice);
    //         }else{
    //             var oldPrice = $scope.prices['webshop' + oldValue];
    //             var newPrice = $scope.prices['webshop' + newValue];
    //             if(!oldValue){
    //                 $scope.calculate(true, newPrice);
    //             }else{
    //                 $scope.calculate(false, oldPrice);
    //                 $scope.calculate(true, newPrice);
    //             }
    //         }
    //     }
    // })

    // $scope.total =0;
    // $scope.originalTotal=0;
    // $scope.calculate = function(method, amount){
    //     if(method){
    //         $scope.total += amount;
    //         if($scope.initial){
    //             $scope.originalTotal += amount;
    //         }
    //     }else{
    //         $scope.total -= amount;
    //         if($scope.initial){
    //             $scope.originalTotal -= amount;
    //         }
    //     }
    // }

    // start tours
    $scope.marketingtourOnclick = function () {
        localStorage.setItem("type", "marketingtour");
        marketingTour.restart();
    }

    $scope.webshoptourOnclick = function () {
        localStorage.setItem("type", "webshoptour");
        webshopTour.restart();
    }

    $scope.emailmarketingtourOnclick = function () {
        localStorage.setItem("type", "emailmarketingtour");
        emailMarketingTour.restart();
    }

    function mollieCallback(){
        console.log("mollieCallback");
        //The user has now grant us to create an recurring payment, so let's handle that and active the modules
        $api.post('finish-upgrade', {orderId: $scope.orderId})
        	.then(function(response) {
                console.log("mollieCallback", response);
                $scope.loading = false;
                $scope.shop = response.data.shop;
                $rootScope.shop = response.data.shop;
                $rootScope.selectedShop = response.data.shop.nameSlug;

                $scope.alert = {
                    type: "success",
                    msg: "Uw abbonnement is succesvol gewijzigd!"
                }
                $timeout(function(){
                    $scope.originalTotal = 0;
                    $scope.total =0;
                    $scope.getShopModules();
                }, 1000)
                
            })
            .catch(function(reason) {
                console.log(reason);
                $scope.loading = false;
                $scope.alert = {
                    type: "danger",
                    msg: reason
                }
            })
    }

    $scope.openCountrSignupModal = function(){
        $countr.signupModal()  
        	.then(function(response) {
                console.log("RESPONSE", response);
            })
            .catch(function(reason) {
                console.error(reason);
            })
    }

    $scope.startUpgrade = function(package){
        var userPackage = angular.copy($scope.packages[package]), counter =0;

        if(!userPackage.choosen || typeof userPackage.choosen == 'undefined' || Object.keys(userPackage.choosen).length < 1){
            $scope.openUpgradeModal(userPackage);
            return;
        }

        for(var key in userPackage.choosen){
            if(!userPackage.choosen.hasOwnProperty(key)) continue;

            var option = userPackage.choosen[key];
            try{
                option = JSON.parse(option);
                if(option && option.price){
                    userPackage.choosen[key] = option;
                }
            }catch(err){
                //Use try catch to prevent errors for already parsed object
            }
            counter++;
            if(counter == Object.keys(userPackage.choosen).length){
                $scope.openUpgradeModal(userPackage);
            }
        }
    }

    $scope.getPackageTotalPrice = function(index){
        var package = $scope.packages[index];
        var totalPrice = package.price;

        if(package.choosen && Object.keys(package.choosen).length > 0){
            var counter = 0;

            for(var key in package.choosen){
                if(!package.choosen.hasOwnProperty(key)) continue;
                var option = package.choosen[key];
                try{
                    option = JSON.parse(option);
                    if(option && option.price){
                        totalPrice += option.price;
                    }
                }catch(err){
                    //Use try catch to prevent errors for already parsed object
                }
                counter++;
                if(counter == Object.keys(package.choosen).length){
                    package.totalPrice = totalPrice;
                } 
            }
        }else{
            package.totalPrice = totalPrice;
        }
    }

    $scope.openUpgradeModal = function(upgrade){
		var modalInstance = $uibModal.open({
			templateUrl: '../views/modal/upgrade-modal.html',
			controller: 'upgradeModalController',
			resolve: {
				upgrade: function(){
					return upgrade;
                },
                shop: function(){
                    return $rootScope.currentShop
                }
			}
		});

		modalInstance.result.then(function(result){
            console.log("result", result);
			if(result){
				$scope.processUpgrade(result);
			}
		}, function(){
			//dismissed
		})
    }
    
    $scope.processUpgrade = function(package){
        console.log("processUpgrade");
        $scope.loading = true;

        if(typeof $rootScope.currentShop.accountHolder == 'undefined' || !$rootScope.currentShop.accountHolder || $rootScope.currentShop.accountHolder == ""){
            console.error("Not all financial data is known");
            $scope.loading = false;
            $scope.alert = {
                type: 'danger',
                msg: "Controleer en vul uw bankgegevens aan in Mijn winkel > Financieel"
            }
            return;
        }
        console.log("hier");

        $api.post('start-upgrade', {
            package: package,
            shopId: $rootScope.currentShop._id
            })
        	.then(function(response) {
                console.log("response", response);
                if(response.status == 200){
                    $scope.loading = false;
                    $window.location.href = response.data.payment.links.paymentUrl;
                    return;
                }
                if(response.status == 201){
                    $scope.orderId = response.data.order._id.toString();
                    mollieCallback();
                    return;
                }
            })
            .catch(function(reason) {
                $scope.loading = false;
                console.error(reason);
                $scope.alert = {
                    type: 'danger',
                    msg: reason
                }
            }) 
    }

}]);