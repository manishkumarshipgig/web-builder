prismanoteApp.controller('productsController', ['$scope', '$rootScope', '$api', '$q', '$cart', '$state', '$stateParams', '$translate',
	function($scope, $rootScope, $api, $q, $cart, $state, $stateParams, $translate) {
		alert("hi!");
		$scope.gents = $stateParams.gents;
		$scope.ladies = $stateParams.ladies;
		$scope.boys = $stateParams.boys;
		$scope.girls = $stateParams.girls;

		$scope.category = {};
		$scope.$watch('products',function(){
			console.log("$scope.products Changed ",$scope.products);
		})		
		if($stateParams.category && typeof $stateParams.category == 'string' && ['WATCH', 'STRAP', 'JEWEL', 'OTHER'].includes($stateParams.category)) {

			$scope.productCategory = [$stateParams.category];
			$scope.category[$stateParams.category.toLowerCase()] = true;
			// $scope.category = $scope.productCategory.reduce(function(categories, category) { 
			// 	categories[category.toLowerCase()] = true;
			// 	return categories;
			// }, {});
		} else {
			$scope.productCategory = ['WATCH', 'STRAP', 'JEWEL', 'OTHER'];
		}

		// Array with all currently displayed items
		$scope.products = [];

		$scope.limit = 24;

		$scope.setLimit = function(limit) {
			$scope.limit = limit;
		}

		$api.resetApiParams('products')

		.catch(function(reason) {
			console.log(reason);
		});

		// format kids, male and female booleans into a translated and formatted gender string
		var formatGender = function(male, female, kids) {
			return $q(function(resolve) {

				if(kids == false) {
					if(male == true && female == true) {
						$translate('GENTS').then(function(gents) {
							$translate('LADIES').then(function(ladies) {
								resolve(gents + ' / ' + ladies);
							});
						});
					} else if(male == true && female == false) {
						$translate('GENTS').then(function(gents) {
							resolve(gents);
						});
					} else if(male == false && female == true) {
						$translate('LADIES').then(function(ladies) {
							resolve(ladies);
						});
					}
				} else {
					if(male == true && female == true) {
						$translate('BOYS').then(function(boys) {
							$translate('GIRLS').then(function(girls) {
								resolve(boys + ' / ' + girls);
							});
						});
					} else if(male == true && female == false) {
						$translate('BOYS').then(function(boys) {
							resolve(boys);
						});
					} else if(male == false && female == true) {
						$translate('GIRLS').then(function(girls) {
							resolve(girls);
						});
					}
				}
			})
		};

		// Async function to add/format gender, shop name and possibly other additional properties later on
		var formatProduct = function(product) {
			return $q(function(resolve, reject) {

				formatGender(product.male, product.female, product.kids)
				
				.then(function(formattedGender) {
					product.gender = formattedGender;
					resolve(product);
				})
				
				.catch(function(reason) {
					reject(reason);
				});
			});
		};

		var getProductCount = function() {
			return $q(function(resolve, reject) {

				$api.getCount('products')
				
				.then(function(productCount) {
					$scope.productCount = productCount;
					resolve(productCount);
				})
				
				.catch(function(reason) {
					reject(reason);
				});
			});
		};



		// NEW CODE
				// Get new products from the database and add them to the $scope
				$scope.getProducts = function(params, options) {
					return $q(function(resolve, reject) {
						$scope.loading = true;

						if(!params) {
							params = {};
						}
						if(!params.sort) {
							params.sort = {dateLastModified: 'desc'};
						}
						if(!params.filter) {
							params.filter = {};
						}
						if(!params.filter._id) {
							params.filter._id = {};
						}

						if(params.sort.price == 'asc' || params.sort.price == 'desc') {
							$scope.sortShopProducts.price = params.sort.price;
						} else if(params.sort.dateAdded == 'desc' || params.sort.dateAdded == 'asc') {
							$scope.sortShopProducts.dateAdded = params.sort.dateAdded;
						} else if(params.sort != {}) {
							$scope.sortShopProducts = {};
						}

						params.filter.includeUnverified = true;
						params.filter.isVerified = undefined;
						
						
				// Delete the loaded products and load them again with the new apiParams
				if(options != null && typeof options === 'object') {
					
					if(options.reset === true) {
						$scope.products = [];
					}
				}

				params.offset = $scope.products.length;
				console.log("this is seller offset", params.offset);
				
				params.limit = $scope.limit;
				console.log("this is seller limit", params.limit);
				

				var ids = [];

				for(var i = 0; i < $rootScope.currentShop.products.length; i++) {
					// Add the shop product, remove if it does not match the filters.
					ids.push($rootScope.currentShop.products[i]._id);
					
					if(params.filter.price && (params.filter.price.$gte != null || params.filter.price.$lte != null) && ($rootScope.currentShop.products[i].price < params.filter.price.$gte || $rootScope.currentShop.products[i].price > params.filter.price.$lte)) {
						ids.pop();
					} else if(params.filter.discount == true && $rootScope.currentShop.products[i].discount == 0) {
						ids.pop();
					} else if(params.filter.inStock == true && $rootScope.currentShop.products[i].stock == 0) {
						ids.pop();
					} else if(params.filter.isBestseller == true && $rootScope.currentShop.products[i].isBestseller != true) {
						ids.pop();
					} else if(params.filter.show == true && $rootScope.currentShop.products[i].show != true) {
						ids.pop();
					}
				}
				delete params.filter.price;
				delete params.filter.discount;
				delete params.filter.inStock;
				delete params.filter.isBestseller;
				delete params.filter.show;
				//params.filter._id.$in = ids;

				$scope.productCount = ids.length;

				if(params.filter.category && params.filter.category.$in) {
					$scope.productCategory = params.filter.category.$in;
				} else {
					$scope.productCategory = [];
				}

				$api.get('webshop-products/' + $rootScope.currentShop.nameSlug, params)
				.then(function(response){
					
					for(var i = 0; i < response.data.products.length; i++) {

						formatProduct(response.data.products[i])

						.then(function(formattedProduct) {

							var matchedShopProduct = _.find($rootScope.currentShop.products, {_id: formattedProduct._id});
							var mergedProduct = Object.assign(formattedProduct, matchedShopProduct);
							mergedProduct.price = matchedShopProduct.price;

							if(options != null && typeof options === 'object' && options.featured === true) {
								$scope.featuredProducts.push(mergedProduct);
							} else {
								console.log("Its a regular product");
								if(checkIfAlreadyExist(mergedProduct._id))
									$scope.products.push(mergedProduct);
								else
									console.log(mergedProduct._id,"Already exists in $scope.products");
							}

							function checkIfAlreadyExist(id){
								console.log("Checking for ID = ",id);
								for(var j = 0; j < $scope.products.length; j++){
									if($scope.products[j]._id == id){
										// console.log("Match Found");
										return false;
									} 
								}
								return true;
							}

							if($scope.products.length == params.offset + response.data.products.length) {
								if($scope.sortShopProducts != null) {
									if(typeof $scope.sortShopProducts.price === 'string') {
										if($scope.sortShopProducts.price == 'asc') {
											$scope.products.sort(function(a,b) { return a.price - b.price; });
										} else if($scope.sortShopProducts.price == 'desc') {
											$scope.products.sort(function(a,b) { return b.price - a.price; });
										}
									} else if(typeof $scope.sortShopProducts.dateAdded === 'string') {
										if($scope.sortShopProducts.dateAdded == 'asc') {
											$scope.products.sort(function(a,b) { return a.dateAdded - b.dateAdded; });
										} else if($scope.sortShopProducts.dateAdded == 'desc') {
											$scope.products.sort(function(a,b) { return b.dateAdded - a.dateAdded; });
										}
									}
								}
								$scope.loading = false;
							}
						})

						.catch(function(reason) {
							reject(reason);
						});
					}
					
					$scope.noProductsFound = false;

					resolve();

				})

				.catch(function(reason) {

					$scope.noProductsFound = true;

					reject(reason);
				});
			});
};

var checkIfAlreadyExist = function(item){
	console.log("LOADMORE -- Checking if ",item.id ,"==", $scope.compareId, "RESULT = ",(item.id == $scope.compareId));
	return (item.id == $scope.compareId);
}

		// OLD CODE
		// Get new products from the database and add them to the $scope
		// $scope.getProducts = function(params, options) {
		// 	return $q(function(resolve, reject) {

		// 		// Delete the loaded products and load them again with the new apiParams
		// 		if(options != null && typeof options === 'object') {
			
		// 			if(options.reset === true) {
		// 				$scope.products = [];
		// 			}
		// 		}

		// 		if(params == null || typeof params !== 'object') {
		// 			params = {};
		// 		}

		// 		params.offset = $scope.products.length;
		// 		params.limit = $scope.limit;

		// 		if(params.filter.category && params.filter.category.$in) {
		// 			$scope.productCategory = params.filter.category.$in;
		// 		} else {
		// 			$scope.productCategory = [];
		// 		}

		// 		getProductCount()
		
		// 			.then(function() {

		// 				$api.get('products', params)

		// 					.then(function(response) {

		// 						// Loop through newly fetched products and add them to the $scope.
		// 						for(var i = 0; i < response.data.products.length; i++) {

		// 							formatProduct(response.data.products[i])

		// 								.then(function(formattedProduct) {
		// 									$scope.products.push(formattedProduct);
		// 								})

		// 								.catch(function(reason) {
		// 									reject(reason);
		// 								});
		// 						}
		// 						resolve();

		// 					})

		// 					.catch(function(reason) {
			
		// 						$scope.noProductsFound = true;

		// 						reject(reason);
		// 					});
		// 			})
		
		// 			.catch(function(reason) {
		// 				console.log(reason);
		// 			});
		// 	});
		// };

		$scope.getProducts({sort: {views: 'desc'}, filter: {category: {$in: $scope.productCategory}}, limit: 4})

		.catch(function(reason) {
			console.log(reason);
		});


	}]);
