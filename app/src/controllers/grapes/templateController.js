prismanoteApp.controller('grapeTemplateController', ['$scope', 'factory', '$state', 'grapeFactory', function ($scope, factory, $state, grapeFactory) {
  $scope.editorHide = false;

  $scope.templateLoader = function (templateName, pageName) {
    
    var localData = JSON.parse(localStorage.getItem('factory'));
    //console.log('factory...',localData);
    if (localData.websiteName == undefined) {
      alert('please select a Website name');
      $state.go('builderhome');
    } else {
      factory.templateName = templateName;
      factory.pageName = pageName;
      factory.websiteName = localData.websiteName;
      factory.logoFile = [];
      var obj = {};
      obj.websiteName = factory.websiteName;
      obj.pageName = factory.pageName;
      obj.templateName = factory.templateName;
      localStorage.setItem("factory", JSON.stringify(factory));
      grapeFactory.templateChooser(obj).then(function (response) {
        console.log('get response',response);
        if (response.data == true) {
          $state.go('buildereditor');
        }
      })
    }
  };

}]).config(['$qProvider', function ($qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
}]);