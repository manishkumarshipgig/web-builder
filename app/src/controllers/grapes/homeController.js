prismanoteApp.controller('grapeHomeController',['$scope', '$state', 'factory','grapeFactory','Upload', function ($scope, $state, factory, grapeFactory,Upload) {
  
  $scope.websiteName = $scope.user.shops[0].name;

  grapeFactory.getWildCard().then(function (response) {
    $scope.wildCards=response.data;
  });

 // $scope.cruise = cruises;
  $scope.limit = 4;

  $scope.loadMore = function() {
    var increamented = $scope.limit + 4;
    $scope.limit = increamented > $scope.wildCards.length ? $scope.wildCards.length : increamented;
  };
  $scope.uploadlogoWebsiteName = function(file){
    file = Upload.rename(file, "logo.png");
    Upload.upload({
      url: 'api/grape/site-name',
      data: {websiteName:$scope.websiteName,file: file},
    }).then(function(res){
      if(res.data==true){
              alert("This Domain's name is already exists");
              $state.go('buildereditor');
            }
            else{
              $state.go('buildertemplate');
            }
    })
   
    factory.websiteName = $scope.websiteName;
    factory.logoFile=file;
    factory.websiteName = $scope.websiteName;
    localStorage.setItem("factory",JSON.stringify(factory));
  };

  $scope.wildCardViewFunc=function(wildcard){
    factory.websiteName = wildcard;
    factory.pageName = 'index';
    localStorage.setItem("factory", JSON.stringify(factory));
    if(wildcard){
      $scope.websiteName=factory.websiteName;
      $state.go('buildereditor');
    }

  };
  
  $scope.GetFileName=function(){
    var fileInput = document.getElementById('logoFile');
    $scope.fileName = fileInput.value.split(/(\\|\/)/g).pop();
  }
  
}]);
prismanoteApp.config(['$qProvider', function ($qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
}]);