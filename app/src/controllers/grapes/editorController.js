prismanoteApp.controller('grapeEditorController', ['$scope', '$http', 'grapeFactory', 'factory', '$state', '$uibModal','Upload', function ($scope, $http, grapeFactory, factory, $state, $uibModal,Upload) {
  $scope.editorHide = false;
  $scope.websiteName = '';
  var editor;
  var oldHTMLContent = '';
  var oldCSSContent = '';
  var currentHTMLContent = '';
  var currentCSSContent = '';
 
  grapeFactory.getAllNewImage().then(function (response) {
    images=response.data;
  });
  var loadEditor = function (page) {

    if (factory.pageName == 'campaigns' || factory.pageName == 'brands') {
      flagScript = 0;
    } else {
      flagScript = 1;
    }
  
    //var images = ['https://s3-eu-west-1.amazonaws.com/prismanote/social-media/citizen.jpg', 'https://s3-eu-west-1.amazonaws.com/prismanote/social-media/DKNY.jpg', 'https://s3-eu-west-1.amazonaws.com/prismanote/social-media/smile_solar.jpg', 'https://s3-eu-west-1.amazonaws.com/prismanote/social-media/royce-concept.jpg', 'https://s3-eu-west-1.amazonaws.com/prismanote/social-media/breil-lady-chrono.png'];
    editor = grapesjs.init({
      height: '200%',
      showOffsets: 1,
      noticeOnUnload: 0,
      storageManager: {
        autoload: 0
      },
      components: '<div>Hello world!</div>',
      container: '#gjs',
      fromElement: true,
      allowScripts: flagScript,

      plugins: [
        'Prismanote Blocks', 'grapesjs-lory-slider', 'gjs-blocks-basic', 'gjs-preset-webpage'
      ],
      pluginOpts: {
        'grapesjs-lory-slider': {},
      },
      canvas: {
        styles: [
          'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.c" +
          "ss"
        ],
        scripts: ['https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', "https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"]
      },
      // assetManager: {
      //   upload: api_path + 'image-upload',
      //   uploadName: factory.websiteName
      // }
      assetManager: {
        assets: images,
        upload: 'api/grapes/image-upload',
        uploadFile: function (e) {
          var files = e.dataTransfer ? e.dataTransfer.files : e.target.files;
          var formData = new FormData();
          for (var i in files) {
            formData.append('file-' + i, files[i]) //containing all the selected images from local
          }
          var file = e.target.files[0];
          file.upload = Upload.upload({
            url: 'api/grapes/image-upload',
            data: {file: file},
          });
         file.upload.then(function (response) {
  
              editor.AssetManager.add(response.data); 
            
          });
        },
      },
    });


    editor
      .getComponents()
      .reset();
    editor.addComponents(page);
    editor
      .getComponents()
      .add('<style>label.checkbox-label { padding: 7px; display: inline-flex;}input.checkbox { padding: 7px; display: inline-flex;}input.input{ height: 40px !important; padding: 10px !important; border-radius: 2px !important; transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important; color: #555 !important;}.form-group { height: 40px; padding-left: 10px !important;}input.input:focus { border-color: #66afe9 !important; outline: 0 !important; -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6) !important; box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6) !important;}label.label { margin-right: 16px;}  .darkLink{background:#222;color:white}.darkLink:hover{background:#0ccfa8;color:white }  .cell {height:auto;min-height: 75px !important;flex-grow: 1 !important;flex-basis: 100% !important;padding-right: 10px;padding-left: 12px;}.row {display: flex !important;margin: 0 auto !important;}@media (max-width: 768px){.row {display: block !important;margin:0 auto !important;}.cell{height: auto;}}</style>');

    oldHTMLContent = editor.getHtml();
    oldCSSContent = editor.getCss();
  }
  /*-----to change template-----*/
  $scope.chooseTemplate = function () {

    var modalInstance = $uibModal.open({
      templateUrl: '../views/modal/builder-changetheme-modal.html',
      controller: 'grapeEditorController',
    })
    modalInstance.result.then(function (result) {
      console.log('result', result);
    });
  }

  $scope.modalok = function () {
    var obj = {};
    obj.websiteName = factory.websiteName;
    obj.pageName = factory.pageName;
    obj.templateName = factory.templateName;
    $state.go('buildertemplate');
    grapeFactory.postChangeTemplate(obj)
      .then(function (response) {
        $scope.page = response.data;

        $scope.editorHide = true;
      });
  };
  $scope.modalcancel = function () {
    $state.go('builderhome');
  };

  $scope.addCustomPage = function () {
    //$scope.customPageName='';
    var modalInstance = $uibModal.open({
      templateUrl: '../views/modal/custom-page-modal.html',
      controller: 'grapeEditorController',
    })
    modalInstance.result.then(function (result) {
      console.log('result', result);
    });
  };

  $scope.customOk = function (customPageName) {
    factory.customPageName = customPageName;
    var obj = {};
    obj.websiteName = factory.websiteName;
    obj.customPageName = factory.customPageName;
    grapeFactory.postCustomPage(obj)
      .then(function (response) {
        alert(response.data);
      });
  };

  $scope.customCancel = function () {
    $state.go('editor');
  };

  /*-----to change template-----*/

  $scope.templateLoader = function (pageLoadButtonID) {

    if (editor) {
      $('#gjs').empty();
    }

    if (factory != undefined) {
      if (factory.templateName == undefined || factory.pageName == undefined) {
        var factory = JSON.parse(localStorage.getItem('factory'));
        if (factory == null) {
          $state.go('builderhome');
        } else {
          $scope.websiteName = factory.websiteName;
        }

      } else {
        var factory = JSON.parse(localStorage.getItem('factory'));
        if (factory == null) {
          $state.go('builderhome');
        } else {
          $scope.websiteName = factory.websiteName;
        }
      }
    } else {
      var factory = JSON.parse(localStorage.getItem('factory'));
      if (factory == null) {
        $state.go('builderhome');
      } else {
        $scope.websiteName = factory.websiteName;
      }
    }
    if (pageLoadButtonID == 'homePage') {
      factory.pageName = 'index';
    }
    //console.log(factory);
    var obj = {};
    obj.websiteName = factory.websiteName;
    obj.pageName = factory.pageName;
    obj.templateName = factory.templateName;
    grapeFactory.postTemplate(obj)
      .then(function (response) {
        console.log('response in template', response);
        $scope.page = response.data;
        loadEditor($scope.page);
        $scope.editorHide = true;
      });

  }

  $scope.templateLoader('homePage');

  $scope.path = function (pageName, pageLoadButtonID) {

    currentHTMLContent = editor.getHtml();
    currentCSSContent = editor.getCss();

    if (oldHTMLContent != '' || currentHTMLContent != '') {
      if (oldHTMLContent != currentHTMLContent || oldCSSContent != currentCSSContent) {
        if (confirm('Current page contains unsaved changes, click ok to leave this page or cancel to ' +
          'stay on current page.')) {
          factory.pageName = pageName;
          localStorage.setItem("factory", JSON.stringify(factory));
          setActivePage(pageLoadButtonID);
          $scope.templateLoader();
        }
      } else {
        factory.pageName = pageName;
        localStorage.setItem("factory", JSON.stringify(factory));
        setActivePage(pageLoadButtonID);
        $scope.templateLoader();
      }
    } else {
      $scope.templateLoader('homePage');
    }

  }

  function setActivePage(pageLoadButtonID) {

    var homePageButton = document.getElementById('homePage');
    var productsPageButton = document.getElementById('productsPage');
    var brandsPageButton = document.getElementById('brandsPage');
    var campaignsPageButton = document.getElementById('campaignsPage');
    var reviewsPageButton = document.getElementById('reviewsPage');
    var contactPageButton = document.getElementById('contactPage');

    var clickedButton = document.getElementById(pageLoadButtonID);

    var arrayBtn = [
      homePageButton,
      productsPageButton,
      brandsPageButton,
      campaignsPageButton,
      reviewsPageButton,
      contactPageButton
    ];

    for (var i = 0; i < arrayBtn.length; i++) {
      if (arrayBtn[i] == clickedButton) {
        clickedButton
          .classList
          .remove("btn-primary");
        clickedButton
          .classList
          .add("btn-default");
      } else {
        arrayBtn[i]
          .classList
          .remove("btn-default");
        arrayBtn[i]
          .classList
          .add("btn-primary");
      }
    }

  }

  $scope.exit = function () {
    localStorage.clear();
    $state.go('retailer.home');
  }

  $scope.save = function () {

    if (editor) {

      var reviewCss = '';

      if (pageName == 'reviews') {
        reviewCss = '.star-yellow{color:orange;}'
        alert('css');
      }
      var c = '<!doctype html><html ng-app="editorApp"><head><link rel="stylesheet" href="https://maxcdn.bootstrap' +
        'cdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><link rel="stylesheet" href="http' +
        's://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">' +
        '<link href="lib/angular-slider.css"></script>' +
        '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">' +
        '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css">' +
        '<link rel="stylesheet" href="/styles/style.css">' +
        '<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script></script>' +
        '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>' +
        '<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min.js"></script>' +
        '<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-route.min.js"></script>' +
        '</script><script src="https://rawgit.com/prajwalkman/' +
        'angular-slider/master/angular-slider.js"></script><script src="https://angula' +
        'r-ui.github.io/bootstrap/ui-bootstrap-tpls-0.6.0.js" type="text/javascript"></script>' +
        '<script src="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>' +
        '<script src="//cdnjs.cloudflare.com/ajax/libs/angular-slick-carousel/3.1.7/angular-slick.min.js"></script>' +
        '<script src="https://m-e-conroy.github.io/angular-dialog-service/javascripts/dialogs.min.js" type="text/javascript">' +
        '</script><script>var app = angular.module("editorApp", ["uiSlider","ngFileUpload","slickCarousel"]);</script><style>' + editor.getCss() + reviewCss + '</style></head>';
      var h = editor.getHtml();
      // var j = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"><' +
      //     '/script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstra' +
      //     'p.min.js"></script><script src="https://ajax.googleapis.com/ajax/libs/angularjs/' +
      //     '1.6.4/angular.min.js"></script>';
      var html = c + h;// + j;
      var pageName = factory.pageName;
      var websiteName = factory.websiteName;

      if (pageName == 'brands') {
        var brandScript = "<script>" +
          "var appBrand = angular.module('editorApp');" +
          "appBrand.controller('myBrandCtrl', function($scope, $http,$q) {" +
          "$scope.brandDetailArray={};" +
          "document.getElementById('viewBrand').style.display ='none';" +
          "document.getElementById('angularBrand').style.display ='block';" +
          "$scope.brandDetailViewFlag = false;" +
          "$http.get('https://goedenoot.nl/api/get-brandlist-for-grpah').then(function (response) {" +
          "$scope.brands = response.data;" +
          "return $http.get('https://goedenoot.nl/api/get-branddetail-for-grpah', response.data.result);" +
          "}).then(function (response) {" +
          "$scope.brandDetailArray = response.data;" +
          "}).catch(function (error) {" +
          "});" +
          "$scope.showBrand = function(id) {" +
          "$scope.brandDetailViewFlag = true;" +
          "$scope.brandDetail =  brandDetailArray.filter(function(brand){" +
          "return brand.id == id;" +
          "})[0];" +
          "};" +
          "$scope.brandList = function(){" +
          "$scope.brandDetailViewFlag = false;" +
          "};" +
          "});" +
          "</script>"
        html = html + brandScript;
      }

      if (pageName == 'campaigns') {
        var campaignScript =
          "<script>" +
          "var app = angular.module('editorApp');" +
          "app.controller('campaignCtrl', function ($scope, $http) {" +

          "document.getElementById('viewCampaign').style.display ='none';" +
          "document.getElementById('angularCampaign').style.display ='block';" +
          "$http({" +
          "method: 'GET'," +
          " url: 'https://goedenoot.nl/api/allcampaigns/grpahapi'," +

          "}).then(function(response) {" +
          " $scope.campaigns=response.data;" +
          " }, function (err) {" +
          "});" +
          "});" +
          "</script>";
        html = html + campaignScript;
      }
      if (pageName == 'products') {

        var productsScript =
          "<script>" +
          "var app = angular.module('editorApp',['slickCarousel']);" +
          "app.controller('productsCtrl', function ($scope, $http, $filter, $window) {" +
          "$scope.servicedomain = 'https://s3-eu-west-1.amazonaws.com/prismanote/';" +
          "document.getElementById('grapesFeaturedProducts').style.display ='none';" +
          "document.getElementById('angularfeaturedProductsID').style.display ='block';" +
          "document.getElementById('viewProducts').style.display ='none';" +
          // "document.getElementById('searchBoxID').style.display ='none';" +
          "document.getElementById('angularProducts').style.display ='block';" +
          // "document.getElementById('angularSearchBox').style.display ='block';" +
          " $scope.reviewPageRedirect=function(){" +
          '$window.location = "reviews.html";' +
          " };" +
          " $scope.openNav=function () {" +
          'document.getElementById("mySidenav").style.width = "250px";' +
          'document.getElementById("main").style.marginLeft = "250px";' +
          "};" +
          "$scope.closeNav=function() {" +
          'document.getElementById("mySidenav").style.width = "0";' +
          'document.getElementById("main").style.marginLeft= "0";' +
          'document.body.style.backgroundColor = "white";' +
          "};" +
          "$http({" +
          " method: 'GET'," +
          "url: 'https://goedenoot.nl/api/get-products/grpahapi'," +
          "}).then(function(response) {" +
          "$scope.products = response.data;" +
          "$scope.items2 = $scope.products;" +
          "$scope.$watch('search', function(val){" +
          "$scope.products = $filter('filter')($scope.items2, val);" +
          "});" +
          "}, function (err) {" +
          " });" +
          "$scope.lower_price_bound = 0;" +
          "$scope.upper_price_bound = 1200;" +
          "$scope.priceRange = function(item) {" +
          " return (parseInt(item['suggestedRetailPrice']) <= $scope.upper_price_bound);" +
          " };" +
          " $scope.slickConfigLoaded = true;" +
          "$scope.slickCurrentIndex1 = 0;" +
          "$scope.slickCurrentIndex2 = 0;" +
          "$scope.slickDots = true;" +
          "$scope.slickInfinite = true;" +
          " $scope.slickConfig2 = {" +
          "autoplay: true," +
          "dots: $scope.slickDots," +
          "enabled: true," +
          "focusOnSelect: true," +
          "infinite: $scope.slickInfinite," +
          "initialSlide: 0," +
          "slidesToShow: 4," +
          "slidesToScroll: 1," +
          "vertical:false," +
          "method: {}," +
          "event: {" +
          "afterChange: function (event, slick, currentSlide, nextSlide) {" +
          "$scope.slickCurrentIndex2 = currentSlide;" +
          "}," +
          "init: function (event, slick) {" +
          "slick.slickGoTo($scope.slickCurrentIndex2);" + // slide to correct index when init
          " }" +
          "}" +
          "};" +
          " $scope.slickConfigLoaded = true;" +

          "}).filter('ordinal', function(){" +
          "return function(number){" +
          "if(isNaN(number) || number < 1){" +
          "return number;" +
          "} else {" +
          "var lastDigit = number % 10;" +
          "if(lastDigit === 1){" +
          "return number + 'st'" +
          "} else if(lastDigit === 2){" +
          "return number + 'nd'" +
          "} else if (lastDigit === 3){" +
          "return number + 'rd'" +
          "} else if (lastDigit > 3){" +
          "return number + 'th'" +
          "}" +
          " }" +
          " }" +
          "}).filter('unique', function() {" +
          "return function(collection, keyname) {" +
          "var output = [], " +
          "keys = [];" +
          "angular.forEach(collection, function(item) {" +
          "var key = item[keyname];" +
          "if(keys.indexOf(key) === -1) {" +
          " keys.push(key); " +
          "output.push(item);" +
          "}" +
          "});" +
          "return output;" +
          "};" +
          "});" +
          "</script>";
        html = html + productsScript;
      }
      if (pageName == 'reviews') {
        var reviewScript =
          "<script>" +
          "var app = angular.module('editorApp');" +
          "app.controller('reviewCtrl', function($scope, $http) {" +

          "document.getElementById('viewReview').style.display ='none';" +
          "document.getElementById('angularReview').style.display ='block';" +
          "$scope.reviewPageRedirect=function(){" +
          '$window.location = "reviews.html";' +
          "};" +

          "$scope.errorFlag = false; " +

          " $scope.reviews = [" +
          " {" +
          " name:'Thomas Dingemanse'," +
          "ratings : [{current: 1,max: 5}]," +
          "time:'1397490980837'," +
          "content:'Geweldige service. Toen mijn horloge het ca. 2 maanden na aankoop al niet meer deed, bood dhr Bos aan om deze gelijk kostenloos te repareren! Ik kon mijn (weer perfect werkende) horloge gelijk de volgende dag weer ophalen.'" +

          " }," +
          "{" +
          "name:'Test'," +
          " ratings : [{current: 2,max: 5}]," +
          " time:'1397490980837'," +
          "content:'Geweldige service. Toen mijn horloge het ca. 2 maanden na aankoop al niet meer deed, bood dhr Bos aan om deze gelijk kostenloos te repareren! Ik kon mijn (weer perfect werkende) horloge gelijk de volgende dag weer ophalen.'" +

          " }" +
          " ];" +

          " $scope.review={};" +
          " $scope.review.ratings = [{" +
          " current: 1," +
          "max: 5" +
          "}];" +

          " $scope.getSelectedRating = function (rating) {" +
          "alert(rating);" +
          "}" +
          " $scope.saveReview = function(reviews){" +

          " $scope.review.time = Date.now();" +
          "$scope.reviews.push($scope.review);" +
          " $scope.review={};" +

          "}" +
          "})" +
          ".directive('starRating', function () {" +
          "return {" +
          "restrict: 'A'," +
          "template: '<ul class='rating'>'" +
          '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
          '\u2605' +
          '</li>' +
          '</ul>' +
          "scope: {" +
          "ratingValue: '='," +
          "max: '='," +
          "onRatingSelected: '&'" +
          "}," +
          "link: function (scope, elem, attrs) {" +

          "var updateStars = function () {" +
          "scope.stars = [];" +
          "for (var i = 0; i < scope.max; i++) {" +
          "scope.stars.push({" +
          "filled: i < scope.ratingValue" +
          "});" +
          " }" +
          " };" +

          "scope.toggle = function (index) {" +
          "scope.ratingValue = index + 1;" +
          "scope.onRatingSelected({" +
          "rating: index + 1" +
          "});" +
          "};" +

          " scope.$watch('ratingValue', function (oldVal, newVal) {" +
          "if (newVal) {" +
          "updateStars();" +
          "}" +
          "});" +
          "}" +
          "}" +
          "})" +
          ".directive('starRatingNew', function () {" +
          "return {" +
          "restrict: 'A'," +
          "template: '<ul class='rating'>'" +
          '<li ng-repeat="star in stars" ng-class="star">' +
          '\u2605' +
          '</li>' +
          '</ul>' +
          "scope: {" +
          "ratingValue: '='," +
          "max: '='," +
          "onRatingSelected: '&'" +
          "}," +
          "link: function (scope, elem, attrs) {" +

          "var updateStars = function () {" +
          "scope.stars = [];" +
          "for (var i = 0; i < scope.max; i++) {" +
          "scope.stars.push({" +
          "filled: i < scope.ratingValue" +
          "});" +
          " }" +
          " };" +

          "scope.toggle = function (index) {" +
          "scope.ratingValue = index + 1;" +
          "scope.onRatingSelected({" +
          "rating: index + 1" +
          "});" +
          "};" +

          " scope.$watch('ratingValue', function (oldVal, newVal) {" +
          "if (newVal) {" +
          "updateStars();" +
          "}" +
          "});" +
          "}" +
          "}" +
          "})" +

          "</script>"
        html = html + reviewScript;
      }

      if (pageName == 'index') {
        var indexPageScript =
          "<script>" +
          "var app = angular.module('editorApp');" +
          "app.controller('indexCtrl', function($scope, $http, $window,$dialogs) {" +
          "document.getElementById('viewNavbar').style.display ='none';" +
          "document.getElementById('angularNavbar').style.display ='block';" +
          "$scope.reviewPageRedirect=function(){" +
          ' $window.location = "reviews.html";' +
          " };" +
          "$http({" +
          "method: 'GET'," +
          "url: 'https://goedenoot.nl/api/allcampaigns/grpahapi'," +
          "}).then(function(response) {" +
          "$scope.campaigns=response.data;" +
          "}, function (err) {" +
          "});" +
          " });" +
          "</script>"
        html = html + indexPageScript;
      }
      if (pageName == 'index' | pageName == 'products' | pageName == 'campaigns' | pageName == 'brands' | pageName == 'reviews') {

        var allPageScript =
          "<script>" +
          "app.controller('topBtnCtrl', function($scope, $http, $window) {" +
          "document.getElementById('myBtn').style.display ='none';" +
          "$window.onscroll = function() {scrollFunction()};" +
          "function scrollFunction() {" +
          "if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {" +
          'document.getElementById("myBtnAngular").style.display = "block";' +
          "} else {" +
          'document.getElementById("myBtnAngular").style.display = "none";' +
          " }" +
          "}" +
          "$scope.topFunction=function() {" +
          "document.body.scrollTop = 0;" +
          "document.documentElement.scrollTop = 0;" +
          "};" +
          "});" +
          "</script>";
        html = html + allPageScript;
      }
      if (pageName == 'index' | pageName == 'products' | pageName == 'campaigns' | pageName == 'brands' | pageName == 'reviews') {

        var footerScript =
          "<script>" +

          "app.controller('footerCtrl', function($scope,$window) {" +


          'document.getElementById("angularFooterID").style.display = "block";' +
          'document.getElementById("footerID").style.display = "none";' +

          '$scope.reviewPageRedirect=function(){' +
          '$window.location = "reviews.html";' +
          '};' +

          ' });' +
          "</script>";
        html = html + footerScript;
      }
      var data = {
        html: html + '</html>',
        pageName: pageName,
        websiteName: websiteName
      };
      if (data.pageName == undefined) {
        var local = JSON.parse(localStorage.getItem('factory'));
        data.pageName = local.pageName;
        data.websiteName = local.websiteName;
      }
      console.log('data of save page', data);
      grapeFactory.saveGrapeData(data).then(function (response) {
        // console.log(response);
        alert("Page Saved!!!");
        $scope.templateLoader();
      });
    }

  }

  $scope.publish = function () {
    if (factory == undefined) {
      var factory = JSON.parse(localStorage.getItem('factory'));
    }
    //console.log('factory.websiteName', factory);
    var data = {
      websiteName: factory.websiteName,
      pages: [
        'index',
        'products',
        'brands',
        'campaigns',
        'reviews',
        'contact'
      ]
    };


    grapeFactory.postCanPublish(data).then(function (response1) {
      if (response1.data.success) {
        grapeFactory.postPublish(data).then(function (response2) {
          alert("Website Published!!!");
          var url = location.origin + '/prismanote/' + factory.websiteName + '/production/index.html';
          window.open(url, '_blank');
        });

      } else {
        alert("Please save remaining pages : " + JSON.stringify(response1.data.pages));
      }

    });

  };

}]);
prismanoteApp.config(['$qProvider', function ($qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
}]);