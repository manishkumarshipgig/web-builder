var prismanoteApp = angular.module('prismanoteApp',[
	'ngAnimate',
	'angularUtils.directives.dirPagination',
	'ui.bootstrap',
	'ui.router',
	'angular.filter',
	'angularUtils.directives.dirPagination',
	'validation.match',
	'ngSanitize',
	'ngCookies',
	'pascalprecht.translate',
	'toggle-switch',
	'angucomplete-alt',
	'cgPrompt',
	'dndLists',
	'ngFileUpload',
	'ui.router.breadcrumbs',
	'angular-bind-html-compile',
	'rzModule', // range slider (i.e. for price filter)
	'ngFileSaver',
	'angular-input-stars',
	'textAngular',
	'ui.bootstrap.datetimepicker',
	'mm.iban',
	'angular-intro',
	'colorpicker.module',
	'uiCropper',
	//'pubnub.angular.service',
	'isoCurrency',
	'ngFacebook'
]);

prismanoteApp.constant('_', _);

prismanoteApp.config(['$provide', '$facebookProvider', '$httpProvider', function($provide, $facebookProvider, $httpProvider){

	$httpProvider.interceptors.push('apiInterceptor');

	$facebookProvider.setAppId('184018705493073');
	$facebookProvider.setPermissions("manage_pages, email, public_profile, publish_pages, user_posts, publish_actions, ads_management, read_insights, ads_read, rsvp_event, instagram_basic,user_events");
	$facebookProvider.setVersion("v2.11");

	$provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$uibModal', function (taRegisterTool, taOptions, $uibModal) {
		taRegisterTool('uploadImage', {
			buttontext: 'Image/Afbeelding toevoegen',
			iconclass: "fa fa-image",
			action: function (deferred,restoreSelection) {

				var modalInstance = $uibModal.open({
					templateUrl: '../views/modal/textAngular-upload-image-modal.html',
					controller: 'textAngularTextUploadModalController',
				});
				modalInstance.result.then(function(result) {
					restoreSelection();
					document.execCommand('insertImage', true, result);
					deferred.resolve();
				}, function() {
					deferred.resolve();
				})
				return false;
			}
		});
		taOptions.toolbar[3].push('uploadImage');
		return taOptions;
	}]);
}]);
// prismanoteApp.config(['$facebookProvider', function( $facebookProvider ) {
// 	$facebookProvider.setAppId('184018705493073');
// 	$facebookProvider.setPermissions("manage_pages, email, public_profile, publish_pages, user_posts, publish_actions, ads_management, read_insights, ads_read, rsvp_event, instagram_basic,user_events");
// 	$facebookProvider.setVersion("v2.11");
// }]);

prismanoteApp.run(['$rootScope', '$window', '$location', '$stateParams', '$api', '$translate', '$transitions', '$state', '$filter',
	function($rootScope, $window, $location, $stateParams, $api, $translate, $transitions, $state, $filter) {
	
	var lang = "";

	//detect user window size on resize
	angular.element($window).on('resize', function () {
		$rootScope.setWindowSize($window.innerWidth);
	});
	
	$rootScope.setWindowSize = function(width){
		$rootScope.innerWidth = width;

		if(width < 768){
			$rootScope.userDeviceType = 'mobile';
			$rootScope.bootstrapSize = 'xs'
		}else if(width >= 768 && width < 992){
			$rootScope.userDeviceType = 'tablet';
			$rootScope.bootstrapSize = 'sm';
		}else if(width >= 992 && width < 1200){
			$rootScope.userDeviceType = 'desktop';
			$rootScope.bootstrapSize = 'md';
		}else if(width >= 1200){
			$rootScope.userDeviceType = 'desktop';
			$rootScope.bootstrapSize = 'lg';
		}
	}

	$rootScope.setWindowSize($window.innerWidth);

	$rootScope.awsUrl = "https://prismanote.s3.amazonaws.com/";
	if (window.location.hostname == 'localhost' || window.location.hostname == "127.0.0.1" || window.location.hostname == "goedenoot.nl"){
		$rootScope.awsUrl = "https://prismanotetest.s3.amazonaws.com/";
	}

	//$rootScope.language = $translate.use().substring(0,2);
	$rootScope.currency = 'eur';

	$rootScope.guest = false;
	this.titleBarControllers = ['productController', 'productsController', 'brandController', 'brandsController', 'newsItemController', 'newsItemsController'];

	$transitions.onSuccess({to: "layout.**"}, function(transition) {
		if(transition.$to().data && transition.$to().data.pageTitle) {
			$rootScope.pageTitle = 'PrismaNote | ' + transition.$to().data.pageTitle;
		} else {
			$rootScope.pageTitle = 'PrismaNote';
		}
	});

	$transitions.onSuccess(true, function() {
		$rootScope.top();
		$api.resetApiParams();
	});

	$transitions.onStart({to: "**"}, function(transition){		
		if(transition.$to().data && transition.$to().data.restricted) {
			checkLogin(true, function(err, user){
				if (err) {
					//No user, or error during login
					return $state.transitionTo("layout.access-denied", {to: transition.$to().name});
				}
				if(transition.$to().data.role && transition.$to().data.role != ""){
					//specific role required
					if(!user || user.role != transition.$to().data.role){
						return $state.transitionTo("layout.access-denied", {to: transition.$to().name});
					}
				}else{
					//only login is required
					if(!user){
						return $state.transitionTo("layout.access-denied", {to: transition.$to().name});
					}
				}
			})
		}
	})

	$rootScope.getBreadcrumbPath = function(item) {
		var currentPath = $location.path().split('/');
		var breadcrumbPath = new Array();
		for(var i = 0; i <= $rootScope.breadcrumb.indexOf(item); i++) {
			breadcrumbPath.push(currentPath[i + 1]);
		}
		return breadcrumbPath.join('/');
	};

	$rootScope.top = function() {
		$window.scrollTo(0,0);
	};

	$rootScope.nl2br = function(str, is_xhtml) {
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
		return (str + '').trim().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
	};

	$rootScope.truncateText = function(text, length) {
		if(!text){
			return;
		}
		if(!length) {
			length = 140;
		}
		if(text.indexOf(' ', length) == -1) {
			return text;
		} else {
			return text.substring(0, text.indexOf(' ', length)) + ' ...';
		}
	};

	$rootScope.formatDate = function(dateString, short) {
		// TODO make a multilingual formatDate function using angular-translate
		var result = "";
		var monthNames = [
			"januari", "februari", "maart",
			"april", "mei", "juni", "juli",
			"augustus", "september", "oktober",
			"november", "december"
		];

		if(dateString) {
			var date = new Date(dateString);
			if(short === true) {
				result = '' + date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear();
			} else {
				result = '' + date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear() + ' om ' + date.getHours() + ":" + ('0' + date.getMinutes()).slice(-2) + " uur";
			}
		}

		return result;
	};

	$rootScope.randomNumber = function(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	};

	$rootScope.checkLogin = function(reload, callback){
		return checkLogin(reload, callback);
	}

	function checkLogin(reloadData, callback) {
		$api.get('checklogin')
		
			.then(function(response) {
				if(response.data.user != null) {
					if(reloadData){
						$rootScope.user = response.data.user;
					}
					
					return callback(null, response.data.user);					
				} else{
					return callback(response.data.message, null);
				}
			})
			
			.catch(function(reason) {
				console.log(reason);
				return callback(reason, null);
			});
	}

	$rootScope.changeLanguage = function (langKey) {
		if(langKey){
			if(langKey.length == 2) {
				$translate.use(langKey.toLowerCase()+"_"+langKey.toUpperCase());
				$rootScope.language = langKey;
			} else if(langKey.length == 5) {
				$translate.use(langKey);
				$rootScope.language = langKey.substring(0,2);
			}
		}
	};

	$rootScope.$on('$translateChangeSuccess', function () {
		if($translate.use()){
			$rootScope.language = $translate.use().substring(0,2);
			localStorage.setItem("language", $rootScope.language);
			lang = $rootScope.language;
			if($state.$current && !$state.$current.self.abstract) {
				$state.reload();
			}
		}
	});
	$rootScope.changeLanguage($translate.use());

	
	checkLogin(true,function(err, user){ // OPTIONAL: do something with the data
	});

	$rootScope.getShopType = function() {
		
		var hostnames = ['prismanote.com', 'nieuw.prismanote.com', '127.0.0.1', 'localhost', 'prismanote.ngrok.io', 'goedenoot.nl'];
		
		if (hostnames.indexOf(window.location.hostname) == -1) {
			return "proshop";
		} else {
			return "shop";
		}
	}

	$rootScope.testing = function(){
		var test = false;
		var hostnames = ['goedenoot.nl'];

		test = hostnames.indexOf(window.location.hostname) != -1;

		return test;
	}

	$rootScope.getOrderStatus = function(order, returnType){
		if(returnType && returnType == 'object'){
			return order.status[order.status.length -1];
		}else{
			return order.status[order.status.length -1].status;
		}
	}

	$rootScope.slugify = function(text) {
    	return text.toString().toLowerCase()
    		.replace(/\s+/g, '-')           // Replace spaces with -
        	.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        	.replace(/\-\-+/g, '-')         // Replace multiple - with single -
        	.replace(/^-+/, '')             // Trim - from start of text
        	.replace(/-+$/, '');            // Trim - from end of text
	}

	$rootScope.getSpecificOrderStatus = function(order, status){
		for(var i =0; i < order.status.length; i++){
			if(order.status[i].status == status){
				return order.status[i];
			}
		}
	}
	
    $rootScope.consumerHeaderHide = false;
	if($location.$$path=='/welcome-retailer/' || $location.$$path=='/home/'){
		$rootScope.consumerHeaderHide = true;
	}

    $rootScope.checkModule = function(mod, shop){
		var result = false;	

		if(!shop){
			shop = $rootScope.currentShop;
		}

        if(shop && typeof shop.modules != 'undefined'){
            if(typeof shop.modules[mod] != 'undefined' && shop.modules[mod]){
                result = true;
            }
        }
        return result;
    }

	//#region route definitions
		
	//#endregion
	
	//#region route functions
		
	//$rootScope.startGeneralTour = function(){
	//	initTour('general', $rootScope.language);
	//	generalTour.init();
	//	generalTour.start(true);
	//	generalTour.restart();
	//}

	//$rootScope.startMarketingTour = function(){
	//	initTour('marketing');
	//	marketingTour.init();
	//	marketingTour.start(true);
	//	marketingTour.restart();
	//}

	//$rootScope.startWebshopTour = function(){
	//	initTour('webshop');
	//	webshopTour.init();
	//	webshopTour.start(true);
	//	webshopTour.restart();
	//}

	//$rootScope.startEmailMarketingTour = function(){
	//	initTour('emailMarketing');
	//	emailMarketingTour.init();
	//	emailMarketingTour.start(true);
	//	emailMarketingTour.restart();
	//}

	//#endregion
		if (document.getElementById('facebook-jssdk')) {return;}
		var firstScriptElement = document.getElementsByTagName('script')[0];
		var facebookJS = document.createElement('script'); 
		facebookJS.id = 'facebook-jssdk';
		facebookJS.src = 'https://connect.facebook.net/en_US/sdk.js';
		firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
}]);
prismanoteApp.config(['$qProvider', function ($qProvider) {
	$qProvider.errorOnUnhandledRejections(false);
}])
