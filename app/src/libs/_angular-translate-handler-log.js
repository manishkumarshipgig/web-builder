/*!
 * angular-translate - v2.9.0 - 2016-01-24
 * 
 * Copyright (c) 2016 The angular-translate team, Pascal Precht; Licensed MIT
 */
(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define([], function () {
      return (factory());
    });
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory();
  } else {
    factory();
  }
}(this, function () {

angular.module('pascalprecht.translate')

/**
 * @ngdoc object
 * @name pascalprecht.translate.$translateMissingTranslationHandlerLog
 * @requires $log
 *
 * @description
 * Uses angular's `$log` service to give a warning when trying to translate a
 * translation id which doesn't exist.
 *
 * @returns {function} Handler function
 */
.factory('$translateMissingTranslationHandlerLog', ['$http', '$log', function( $http, $log){

  return function (translationId) {
    $log.warn('Translation for ' + translationId + ' doesn\'t exist');


  //  $rootScope.missingTranslations.push(translationId);

      $http({
          method: 'PUT',
          url: '/api/translation',
          data: {keyword: translationId}
      }).then(function successCallback(result){
          
        }, function errorCallback(result){
            //console.log("error", result)
        })
  };

}])

return 'pascalprecht.translate';

}));