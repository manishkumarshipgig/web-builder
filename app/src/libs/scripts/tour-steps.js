var gentour, marketingTour, webshopTour, emailMarketingTour;
var lang;

function getTourTranslation(key){
    var step = _.get(steps, key);
    if(step){
        return step[lang];
    }else{
        return key;
    }
}

function initTour() {
    // init tour
    if (localStorage.getItem("type") == "marketingtour") {
        marketingTour.init();
    } else if (localStorage.getItem("type") == "webshoptour") {
        webshopTour.init();
    } else if (localStorage.getItem("type") == "emailmarketingtour") {
        emailMarketingTour.init();
    } else if (localStorage.getItem("type") == "generaltour") {
        gentour.init();
    }
}

$(function () {
    setTimeout(function () {

        lang = localStorage.getItem("language");

        gentour = new Tour({
            debug: true,
            onEnd: function (tour) {
                if (window.location.href != window.location.origin + "/retailer/home") {
                    //-window.location = '/retailer/home'
                }
            },
            //storage: window.localStorage,
            steps: [
                // {
                //     element: '#generalTourHome',
                //     title: getTourTranslation('START_WITH_PRISMANOTE'),
                //     content: getTourTranslation('GENERALTOUR_WELCOME'),
                //     path: '/retailer/home'
                // },
                {
                    element: '#generalTourMyShopMenu',
                    title: getTourTranslation('MORE_SHOPS'),
                    content: getTourTranslation('HERE_YOU_WILL_FIND_YOUR_SHOPS'),
                    path: '/retailer/home'
                },
                {
                    element: '#generalTourMyShopIcon',
                    title: getTourTranslation('SHOP_DATA'),
                    content: getTourTranslation('CLICK_ON_SHOP_ICON'),
                    path: '/retailer/home'
                },
                {
                    element: '#generalTourFillInInformation',
                    title: getTourTranslation('FILL_IN_DATA'),
                    content: getTourTranslation('FILL_IN_YOUR_DATA'),
                    path: '/retailer/shop'
                },
                {
                    element: '#generalTourBrands',
                    title: getTourTranslation('SHOP_BRANDS'),
                    content: getTourTranslation('INDICATE_WHICH_BRANDS_YOU_ARE_USING'),
                    path: '/retailer/shop'
                },
                {
                    element: '#generalTourWebshopSettings',
                    title: getTourTranslation('WEBSHOP_SETTINGS'),
                    content: getTourTranslation('DETERMINE_SETTINGS_FOR_WEBHSOP'),
                    path: '/retailer/shop'
                },
                // {
                //     element: '#generalTourProShop',
                //     title: getTourTranslation('PAID_WEBHSOP'),
                //     content: getTourTranslation('WANT_A_PRO_SHOP_YOU_PAY_THIS'),
                //     path: '/retailer/shop'
                // },
                {
                    element: '#generalTourMyProfile',
                    title: getTourTranslation('USER_PROFILE'),
                    content: getTourTranslation('CLICK_ON_USER_PROFILE'),
                    path: '/retailer/home'
                },
                {
                    element: '#generalTourShopsToManage',
                    title: getTourTranslation('SHOPS_YOU_MANAGE'),
                    content: getTourTranslation('CONTACT_ADMIN_TO_ADD_STORE'),
                    path: '/retailer/user'
                },
            ]
        });

        marketingTour = new Tour({
            debug: true,
            onEnd: function (tour) {
                if (window.location.href != window.location.origin + "/retailer/home") {
                    //-window.location = '/retailer/home'
                }
            },
            //storage: window.localStorage,
            steps: [
                {
                    element: '#marketingTourHome',
                    title: getTourTranslation('MARKETING_PORTAL'),
                    content: getTourTranslation('ORGANISE_AND_MANAGE_ACTIONS_SOCIAL_MEDIA_CALENDER_CLICK'),
                    path: '/retailer/home'
                },
                {
                    element: '#marketingTourSettings',
                    title: getTourTranslation('FACEBOOK_CONNECTION'),
                    content: getTourTranslation('TAB_SETTING_CONNECTION_FACEBOOK'),
                    path: '/retailer/campaigns'
                },
                {
                    element: '#marketingTourCampaigns',
                    title: getTourTranslation('UPDATES'),
                    content: getTourTranslation('TAB_UPDATES_TOUR'),
                    path: '/retailer/campaigns'
                },
                {
                    element: '#marketingTourAddCampaign',
                    title: getTourTranslation('ADD_A_PROMOTION_YOURSELF'),
                    content: getTourTranslation('ADD_A_PROMOTION_TO_YOUR_ACCOUNT'),
                    path: '/retailer/campaigns'
                },
                {
                   element: '#marketingTourPromotions',
                   title: getTourTranslation('PAID_PROMOTIONS'),
                   content: getTourTranslation('TAB_PAID_PROMOTIONS'),
                   path: '/retailer/campaigns'
                },
                //{ // element is currently located at target-search campaign type
                //    element: '#marketingTourAddTasks',
                //    title: getTourTranslation('SWITCHING_ON_OFF'),
                //    content: getTourTranslation('THIS_CAMPAIGN_GOT_TASKS_DETERMINE_IF_YOU_WANT_TO_SWITCH_THEM_ON_OFF'),
                //    path: '/retailer/campaigns/nameSlug'
                //},
                //{
                //    element: '#marketingTourAddControl',
                //    title: getTourTranslation('CHECK'),
                //    content: getTourTranslation('CHECK_TEXT_AND_PHOTO_FOR_FACEBOOK'),
                //    path: '/retailer/campaigns/nameSlug'
                //},
                //{
                //    element: '#marketingTourAddExecuteModal',
                //    title: getTourTranslation('PLACE_THIS_NOW'),
                //    content: getTourTranslation('CLICK_BUTTON_TO_CHECK_POST_CAMPAIGN'),
                //    path: '/retailer/campaigns/nameSlug'
                //},
            ]
        });

        webshopTour = new Tour({
            debug: true,
            onEnd: function (tour) {
                if (window.location.href != window.location.origin + "/retailer/home") {
                    //-window.location = '/retailer/home'
                }
            },
            steps: [
                {
                    element: '#webshopTourAssortiment',
                    title: getTourTranslation('EDIT_ASSORTIMENT'),
                    content: getTourTranslation('MANAGE_AND_EDIT_ASSORTIMENT'),
                    path: '/retailer/home'
                },
                {
                    element: '#webshopTourCategory',
                    title: getTourTranslation('CATEGORIES'),
                    content: getTourTranslation('HERE_YOU_FIND_CATEGORIES'),
                    path: '/retailer/assortment'
                },
                {
                    element: '#webshopTourAddProduct',
                    title: getTourTranslation('ADD_PRODUCT'),
                    content: getTourTranslation('CLICK_ON_ADD_PRODUCT_TO_ADD_PRODUCTS'),
                    path: '/retailer/assortment'
                },
                {
                    element: '#add-to-assortment',
                    title: getTourTranslation('SEARCH_ARTICLE'),
                    content: getTourTranslation('TYPE_IN_ARTICLE_NUMBER_TO_ADD_PRODUCT'),
                    path: '/retailer/assortment'
                },
                {
                    element: '#webshopTourOpenEditModal',
                    title: getTourTranslation('EDIT_PRODUCT'),
                    content: getTourTranslation('CLICK_ON_A_PRODUCT_TO_CHANGE_ITS_DATA'),
                    path: '/retailer/assortment'
                },
                {
                    element: '#webshopTourOpenEditModal',
                    title: getTourTranslation('STOCK_PRICE_ETC'),
                    content: getTourTranslation('DETERMINE_STOCK_PRICE'),
                    path: '/retailer/assortment'
                },
                { // element is currently located at target-search campaign type
                    element: '#webshopTourOpenEditModal',
                    title: getTourTranslation('SHARING_PRODUCT_INFO'),
                    content: getTourTranslation('EXPLAIN_CENTRAL_DB_SHARING_SO_EDIT_TO_MAKE_VISIBLE'),
                    path: '/retailer/assortment'
                },
                {
                    element: '#webshopTourOrderIcon',
                    title: getTourTranslation('VIEW_ORDERS'),
                    content: getTourTranslation('CLICK_ON_THE_ORDER_ICON_TO_VIEW_ORDERS'),
                    path: '/retailer/home'
                },
                {
                    element: '#webshopTourCoupon',
                    title: getTourTranslation('SENT_ORDERS'),
                    content: getTourTranslation('FIND_YOUR_ORDERS_HERE_SHIP_WITH_PACKING_SLIP_ETC'),
                    path: '/retailer/orders'
                },
                {
                    element: '#webshopTourCompleted',
                    title: getTourTranslation('HANDLING_OF_RETURNS'),
                    content: getTourTranslation('SENT_ORDERS_ARE_HERE_CLICK_BUTTON_WHEN_YOU_GOT_RETURNS'),
                    path: '/retailer/orders'
                },
                {
                    element: '#webshopTourFinancial',
                    title: getTourTranslation('REVENUES'),
                    content: getTourTranslation('CLICK_ON_FINANCIAL_ICON_TO_SEE_YOUR_PAYMENTS_FROM_PRISMANOTE'),
                    path: '/retailer/home'
                },
                {
                    element: '#webshopTourAccountHolder',
                    title: getTourTranslation('ACCOUNT_HOLDER'),
                    content: getTourTranslation('CHECK_ACCOUNT_HOLDER_YOUR_MONEY_YOU_RECEIVE_WITHIN_21_28_DAYS'),
                    path: '/retailer/financial'
                },
                {
                    element: '#webshopTourPaymentsOverview',
                    title: getTourTranslation('PAYMENT_OVERVIEW'),
                    content: getTourTranslation('VIEW_YOUR_PAYOUTS_HERE'),
                    path: '/retailer/financial/payments'
                },
            ]
        });

        var nameSlug = localStorage.getItem("nameSlug");

        emailMarketingTour = new Tour({
            debug: true,
            onEnd: function (tour) {
                if (window.location.href != window.location.origin + "/retailer/home") {
                    //-window.location = '/retailer/home'
                }
            },
            steps: [
                {
                    element: '#emailMarketingTourHome',
                    title: getTourTranslation('EMAIL_MARKETING'),
                    content: getTourTranslation('CLICK_ON_PREMIUM_SERVICE_ICON_TO_USE_EMAILMARKETING'),
                    path: '/retailer/home'
                },
                {
                    element: '#emailMarketingTourManualMobile',
                    title: getTourTranslation('A_WEB_APP_VIA_THE_INTERNET'),
                    content: getTourTranslation('THIS_APP_IS_BEST_USED_FOR_MOBILE_PHONES_CLICK_HERE_FOR_MANUAL_CLICK_NEXT_TO_VIEW_ON_DESKTOP'),
                    path: '/retailer/loyalty-portal'
                },
                {
                    element: '#emailMarketingTourNewClient',
                    title: getTourTranslation('NEW_CUSTOMER'),
                    content: getTourTranslation('CLICK_TO_REGISTER_NEW_CUSTOMER_VIA_PN_PROFILE_WEBSITE'),
                    path: '/retailer/loyalty-portal'
                },
                {
                   element: '#emailMarketingTourPurchaseRegistration',
                   title: getTourTranslation('PURCHASE_REGISTRATION'),
                   content: getTourTranslation('RECORD_PURCHASE_DATA_ON_THIS_PAGE_CHOOSE_WATCH_OR_JEWEL'),
                   path: '/shop/' + nameSlug + '/loyalty-program'
                },
                {
                   element: '#emailMarketingTourFillIn',
                   title: getTourTranslation('PICTURE_OF_WARRANTY_CERTIFICATE_AND_FILL_IN'),
                   content: getTourTranslation('CLICK_ON_GREY_BOS_AND_FILL_IN'),
                   path: '/shop/' + nameSlug + '/loyalty-watches'
                },
                {
                   element: '#emailMarketingTourSent',
                   title: getTourTranslation('SEND_EMAIL'),
                   content: getTourTranslation('SEND_MY_WARRANTY_CERTIFICATE_TO_CUSTOMER_EMAIL_WE_LINK_YOU_TO_OVERVIEW_OF_DATA'),
                   path: '/shop/' + nameSlug + '/loyalty-watches'
                },
                {
                    element: '#emailMarketingTourHome',
                    title: getTourTranslation('EMAIL_MARKETING_CLIENT_DATA'),
                    content: getTourTranslation('CLICK_ON_PREMIUM_SERVICE_TO_VIEW_CUSTOMER_DATA'),
                    path: '/retailer/home'
                },
                {
                    element: '#emailMarketingTourClickOnCustomer',
                    title: getTourTranslation('VIEWING_DATA'),
                    content: getTourTranslation('CLICK_ON_CUSTOMER_TO_SEE_DETAILS_AND_PHOTO'),
                    path: '/retailer/loyalty-portal'
                },
                {
                    element: '#emailMarketingTourCustomerScheduledMails',
                    title: getTourTranslation('SCHEDULED_EMAILS'),
                    content: getTourTranslation('CUSTOMER_WILL_AUTOMATICALLY_RECEIVE_EMAILS_CLICK_ON_ARROW'),
                    path: '/retailer/loyalty-portal'
                },
            ]
        });

        initTour();
        
    }, 1500);


});

