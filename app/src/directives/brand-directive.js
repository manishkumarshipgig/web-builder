prismanoteApp.directive('brand', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/brand.html'
	};
});
