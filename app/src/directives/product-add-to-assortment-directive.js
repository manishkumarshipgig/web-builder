prismanoteApp.directive('productAddToAssortment', ['$rootScope',function($rootScope) {
	return {
		restrict: 'E',
		replace: 'true',
		scope: false,
		templateUrl: '../views/directives/product-add-to-assortment.html',
		controller: ['$scope', '$api', function($scope, $api) {
			$(document).ready(function(){
				var typingTimer;
				var doneTypingInterval = 2000;

				$(document).on('keyup', '#add-to-assortment', function(){
					console.log("In Timer")
					clearTimeout(typingTimer);
					console.log("Value in search = ",$('#add-to-assortment').val())
					if ($('#add-to-assortment').val()) {
						typingTimer = setTimeout(doneTyping, doneTypingInterval);
					}
				});
				// $('#add-to-assortment').keyup(function(){
				// });
				function doneTyping () {
					alert("Done Typing")
				}
			})
			$scope.findProducts = function(value, searchMode) {

				$scope.hits = [];
				var params = {};

				if(value) {
					if(value.length < 5) return;
					// if(searchMode == "exact"){
						// params = {
						// 	limit: 6,
						// 	offset: 0,
						// 	short: true,
						// 	filter: {
						// 		'variants.productNumber' : {
						// 			regex: { 
						// 				text: value,
						// 				flags: "i" // case insensitive search
						// 			}, 
						// 		},								
						// 		_id: undefined,
						// 		$text: undefined,
						// 		isVerified: undefined
						// 	}
						// }
						// params = {
						// 	productNumber : value
						// }

					// }
					// else if(searchMode == "contains"){
					// 	params = {
					// 		limit: 10, 
					// 		offset: 0, 
					// 		short: true, 
					// 		filter: {
					// 			'variants.productNumber': undefined,
					// 			_id: undefined, 
					// 			$text: {
					// 				$search: value
					// 			}, 
					// 			isVerified: true
					// 		}
					// 	}
					// }

					$api.get('webshop-products-search/'+$rootScope.currentShop.nameSlug+"/"+searchMode+"/"+value, {}, {reset: true})
					.then(function(response) {
						$scope.hits = response.data.products;
					})

					.catch(function(reason) {
						console.log(reason);
					});

					$scope.setLimit();
				}
			};

			$scope.addProductToAssortment = function(product) {	
				product.stock = 0;
				product.discount = 0;
				$api.get('get-specific-product', {productId: product._id})
				.then(function(response) {	
					$scope.productGeneral = response.data.product;
					if($scope.productGeneral.product.containsFilterInfo == false){
						$scope.product.show = false;
					} else {
						product.show = true;
					}
				}
				)
				product.isFeatured = false,
				product.isBestseller = false;
				product.price = product.suggestedRetailPrice;
				$scope.addShopProduct(product);
			}
		}]
	};
}]);
