prismanoteApp.directive('newsItemSmall', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/news-item-small.html'
	};
});
