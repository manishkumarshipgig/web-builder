prismanoteApp.directive('campaignAdminBlock', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: function(elem, attrs) {
			return attrs.modal && attrs.modal == 'true' ? '../views/directives/campaign-admin-block-modal.html' : '../views/directives/campaign-admin-block.html';
		}
	};
});
