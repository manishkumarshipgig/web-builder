prismanoteApp.directive('newsItemBlockRetailerPortal', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: function(elem, attrs) {
			return attrs.modal && attrs.modal == 'true' ? '../views/directives/news-item-block-retailer-portal-modal.html' : '../views/directives/news-item-block-retailer-portal.html';
		}
	};
});
