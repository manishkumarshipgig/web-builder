prismanoteApp.directive('campaignRetailerBlock', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: function(elem, attrs) {
			return attrs.modal && attrs.modal == 'true' ? '../views/directives/campaign-retailer-block-modal.html' : '../views/directives/campaign-retailer-block.html';
		}
	};
});
