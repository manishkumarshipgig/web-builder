prismanoteApp.directive('shopBlock', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/shop-block.html'
	};
});
