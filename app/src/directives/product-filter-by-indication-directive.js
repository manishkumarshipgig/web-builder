prismanoteApp.directive('productFilterByIndication', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-filter-by-indication.html',
		scope: false,
		controller: ['$scope', function($scope) {

			$scope.setIndication = function() {
				var filter = {'watch.indication': {}};
				var indications = [];

				if($scope.indication) {
					if($scope.indication.chronoMulti) {
						indications.push('CHRONO_MULTI');
					} else if(indications.indexOf('CHRONO_MULTI') != -1) {
						indications.splice(indications.indexOf('CHRONO_MULTI'));
					}

					if($scope.indication.analog) {
						indications.push('ANALOG');
					} else if(indications.indexOf('ANALOG') != -1) {
						indications.splice(indications.indexOf('ANALOG'));
					}

					if($scope.indication.digital) {
						indications.push('DIGITAL');
					} else if(indications.indexOf('DIGITAL') != -1) {
						indications.splice(indications.indexOf('DIGITAL'));
					}

					if($scope.indication.analogDigital) {
						indications.push('ANALOG_DIGITAL');
					} else if(indications.indexOf('ANALOG_DIGITAL') != -1) {
						indications.splice(indications.indexOf('ANALOG_DIGITAL'));
					}
				}

				var filter;

				if(indications.length == 0) {
					indications = ['CHRONO_MULTI', 'ANALOG', 'DIGITAL', 'ANALOG_DIGITAL'];
				}

				filter['watch.indication'] = {$in: indications};

				$scope.getProducts({filter: filter}, {reset: true})

					.catch(function(reason) {
						console.log(reason);
					});
			}
		}]
	};
});
