prismanoteApp.directive('productFilterByShopProperties', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-filter-by-shop-properties.html',
		scope: false,
		controller: ['$scope', function($scope) {

			$scope.setShopProperties = function() {
				var filter = {};

				if($scope.shopProperties) {
					if($scope.shopProperties.discount) {
						filter.discount = true;
					} else {
						filter.discount = undefined;
					}

					if($scope.shopProperties.inStock) {
						filter.inStock = true;
					} else {
						filter.inStock = undefined;
					}

					if($scope.shopProperties.isBestseller) {
						filter.isBestseller = true;
					} else {
						filter.isBestseller = false;
					}

					if($scope.shopProperties.show) {
						filter.show = true;
					} else {
						filter.show = false;
					}

					$scope.getProducts({filter: filter}, {reset: true})

						.catch(function(reason) {
							console.log(reason);
						});
				}
			}
		}]
	};
});
