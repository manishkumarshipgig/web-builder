prismanoteApp.directive('productFilterByGender', function() {
	var filter = {};
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-filter-by-gender.html',
		scope: false,
		controller: ['$scope', function($scope) {

			$scope.setGender = function() {

				if($scope.gents) {

					if($scope.ladies) {

						if($scope.boys) {

							// gents, ladies, boys, girls
							if($scope.girls) {

								// Empty filter: select all
								filter = {}

							// gents, ladies, boys
							} else {

								filter = {
									'$or': [
										{
											kids: false
										},
										{
											male: true
										}
									]
								};

							}
						} else {

							// gents, ladies, girls
							if($scope.girls) {

								filter = {
									'$or': [
										{
											kids: false
										},
										{
											female: true
										}
									]
								};

							// gents, ladies
							} else {

								filter = {kids: false};

							}
						}
					} else {

						if($scope.boys) {

							// gents, boys, girls
							if($scope.girls) {

								filter = {
									'$or': [
										{
											kids: true
										},
										{
											male: true
										}
									]
								};

							// gents, boys
							} else {

								filter = {male: true};

							}
						} else {

							// gents, girls
							if($scope.girls) {

								filter = {
									'$or': [
										{
											kids: true,
											female: true
										},
										{
											kids: false,
											male: true
										}
									]
								};

							// gents
							} else {

								filter = {male: true, kids: false};

							}
						}
					}
				} else {

					if($scope.ladies) {

						if($scope.boys) {

							// ladies, boys, girls
							if($scope.girls) {

								filter = {
									'$or': [
										{
											kids: true,
										},
										{
											female: true
										}
									]
								};

							// ladies, boys
							} else {

								filter = {
									'$or': [
										{
											kids: false,
											female: true
										},
										{
											kids: true,
											male: true
										}
									]
								};

							}
						} else {

							// ladies, girls
							if($scope.girls) {

								filter = {female: true};

							// ladies
							} else {

								filter = {female: true, kids: false};

							}
						}
					} else {

						if($scope.boys) {

							// boys, girls
							if($scope.girls) {

								filter = {kids: true};

							// boys
							} else {

								filter = {kids: true, male: true};

							}
						} else {

							// girls
							if($scope.girls) {

								filter = {kids: true, female: true};

							// nothing selected, default to: gents, ladies, boys, girls.
							} else {

								filter = {};

							}
						}
					}
				}

				if(!filter.male) {
					filter.male = undefined;
				}
				if(!filter.female) {
					filter.female = undefined;
				}
				if(!filter.kids) {
					filter.kids = undefined;
				}

				$scope.getProducts({filter: filter}, {getProducts: true, reset: true})

					.catch(function(reason) {
						console.log(reason);
					});
			}
		}]
	};
});
