prismanoteApp.directive('productSort', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-sort.html',
		scope: false,
		controller: ['$scope', function($scope) {

			$scope.default = {isFeatured:'desc', views:'desc'};

			// TODO fix price asc/desc in retailer portal
			$scope.sortProducts = function() {
				if($scope.sortBy != null && typeof $scope.sortBy === 'object') {
					var sortBy = angular.copy($scope.sortBy);
					$scope.getProducts({sort: sortBy}, {reset: true})
					
						.catch(function(reason) {
							console.log(reason);
						});
				}
			}
		}]
	};
});
