prismanoteApp.directive('campaignBlock', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/campaign-block.html'
	};
});
