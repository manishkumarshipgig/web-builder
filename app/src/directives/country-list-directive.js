prismanoteApp.directive('countryList', function() {
	return {
		scope: {
            required: '=ngRequired',
            model: '=ngModel'
        },
        templateUrl: '../views/directives/country-list.html'
	};
});
