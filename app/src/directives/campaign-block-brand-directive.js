prismanoteApp.directive('campaignBrandBlock', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: function(elem, attrs) {
			return attrs.modal && attrs.modal == 'true' ? '../views/directives/campaign-brand-block-modal.html' : '../views/directives/campaign-brand-block.html';
		}
	};
});
