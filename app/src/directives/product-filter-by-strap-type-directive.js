prismanoteApp.directive('productFilterByStrapType', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-filter-by-strap-type.html',
		scope: false,
		controller: ['$scope', function($scope) {

			$scope.setStrapType = function() {
				var filter = {'watch.strap.model': {}};
				var strapTypes = [];

				if($scope.strapType) {
					if($scope.strapType.standardModel) {
						strapTypes.push('STANDARD_MODEL');
					} else if(strapTypes.includes('STANDARD_MODEL')) {
						strapTypes.splice(strapTypes.indexOf('STANDARD_MODEL'));
					}

					if($scope.strapType.milaneseMesh) {
						strapTypes.push('MILANESE_MESH');
					} else if(strapTypes.includes('MILANESE_MESH')) {
						strapTypes.splice(strapTypes.indexOf('MILANESE_MESH'));
					}

					if($scope.strapType.nato) {
						strapTypes.push('NATO');
					} else if(strapTypes.includes('NATO')) {
						strapTypes.splice(strapTypes.indexOf('NATO'));
					}

					if($scope.strapType.expandableStretch) {
						strapTypes.push('EXPANDABLE_STRETCH_STRAP');
					} else if(strapTypes.includes('EXPANDABLE_STRETCH_STRAP')) {
						strapTypes.splice(strapTypes.indexOf('EXPANDABLE_STRETCH_STRAP'));
					}

					if($scope.strapType.select) {
						strapTypes.push('SELECT_BAND');
						strapTypes.push('SELECT_BAND_FOR_POCKET_WATCH');
					} else if(strapTypes.includes('SELECT_BAND')) {
						strapTypes.splice(strapTypes.indexOf('SELECT_BAND'));
						strapTypes.splice(strapTypes.indexOf('SELECT_BAND_FOR_POCKET_WATCH'));
					}

					if(strapTypes.length == 0) {
						strapTypes = ['EXPANDABLE_STRETCH_STRAP', 'NATO', 'MILANESE_MESH', 'STANDARD_MODEL', 'SELECT_BAND', 'SELECT_BAND_FOR_POCKET_WATCH', 'CHAIN_OF_A_POCKET_WATCH'];
					}

					filter['watch.strap.model'] = {$in: strapTypes};

					$scope.getProducts({filter: filter}, {reset: true})

						.catch(function(reason) {
							console.log(reason);
						});
				}
			}
		}]
	};
});
