prismanoteApp.directive('productBlockRetailerPortal', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-block-retailer-portal.html',
		scope: false
	};
});
