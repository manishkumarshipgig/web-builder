prismanoteApp.directive('productBlock', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-block.html'
	};
});
