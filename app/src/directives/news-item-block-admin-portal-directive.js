prismanoteApp.directive('newsItemBlockAdminPortal', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/news-item-block-admin-portal.html'
	};
});
