prismanoteApp.directive('productSmall', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-small.html'
	};
});
