prismanoteApp.directive('newsItemBlockShop', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/news-item-block-shop.html'
	};
});
