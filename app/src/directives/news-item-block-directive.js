prismanoteApp.directive('newsItemBlock', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/news-item-block.html'
	};
});
