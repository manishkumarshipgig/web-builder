prismanoteApp.directive('taskBlock', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/task-block.html'
	};
});
