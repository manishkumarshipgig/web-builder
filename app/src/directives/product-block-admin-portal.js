prismanoteApp.directive('productBlockAdminPortal', function() {
	return {
		restrict: 'E',
		replace: 'true',
		templateUrl: '../views/directives/product-block-admin-portal.html',
		scope: false
	};
});
