
prismanoteApp.factory("grapeFactory",['$http',function($http){
 return {
    postTemplate : function(obj){
        return $http({
            headers: { 'Content-Type': 'application/json' },
            url: '/api/grape/template',
            method: "POST",
            data: obj,
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    },
    saveGrapeData : function(obj){
        return $http({
            headers: { 'Content-Type': 'application/json' },
            url: '/api/grape/save',
            method: "POST",
            data: obj,
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    },
    postCanPublish : function(obj){
        return $http({
            headers: { 'Content-Type': 'application/json' },
            url: '/api/grape/can-publish',
            method: "POST",
            data: obj,
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    },
    postPublish : function(obj){
        return $http({
            headers: { 'Content-Type': 'application/json' },
            url: '/api/grape/publish',
            method: "POST",
            data: obj,
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    },
    postSiteName : function(obj){
        return $http({
            headers: { 'Content-Type': 'application/json' },
            url: '/api/grape/site-name',
            method: "POST",
            data: obj,
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    },
    postCustomPage : function(obj){
        return $http({
            headers: { 'Content-Type': 'application/json' },
            url: '/api/grape/custom-page',
            method: "POST",
            data: obj,
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    },
    templateChooser : function(obj){
        return $http({
            headers: { 'Content-Type': 'application/json' },
            url: '/api/grape/template-chooser',
            method: "POST",
            data: obj,
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    },
    getWildCard : function(){
        return $http({
            url: '/api/grape/wildcards',
            method: "GET"
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    },
    getAllNewImage : function(){
        return $http({
            url: '/api/grapes/getAllNewImage',
            method: "GET"
        })
            .then(function (res) {
                return res;
            }, function (err) {
                return err;
            });
    }
 };
}]);

prismanoteApp.factory("factory",function(){
    var obj = {};
    return obj;
   })