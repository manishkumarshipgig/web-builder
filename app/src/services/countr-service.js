prismanoteApp.factory('$countr', ['$http', '$q', '$api', '$rootScope', '$retailer', '$uibModal',
	function($http, $q, $api, $rootScope, $retailer, $uibModal) {

    
	function connectionStatus(){    

        if(
            !$rootScope.currentShop.countr ||
            !$rootScope.currentShop.countr.accessToken ||
            !$rootScope.currentShop.countr.refreshToken ||
            !$rootScope.currentShop.countr.username
        ){
            return false;
        }else{
            return true;
        }
    }

    function connect(username, password){
        
        return $q(function(resolve, reject){
            $api.post('countr/token', {
                username: username,
                password: password,
                shopId: $rootScope.currentShop._id
            })
            .then(function(response) {
                resolve(response.data);
            })
            .catch(function(reason) {
                reject(reason);
            })
        })
    }

    function getTaxes(){
        return $q(function(resolve, reject){
            $api.get('countr/taxes', {
                shopId: $rootScope.currentShop._id
            })
            .then(function(response) {
                if(
                    (typeof $rootScope.currentShop.countr.taxId == "undefined") ||
                    $rootScope.currentShop.countr.taxId == "" &&
                    response.data.length > 0
                ){
                    $rootScope.currentShop.countr.taxId = response.data[0]._id;
                    $retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
                }
                resolve(response.data);
            })
            .catch(function(reason) {
                reject(reason);
            })
        })
    }

    function setDefaultTax(taxId){
        $rootScope.currentShop.countr.taxId = taxId;
        $retailer.setShop($rootScope.selectedShop, $rootScope.currentShop);
    }

    function newTax(name, rate, defaultTax){
        return $q(function(resolve, reject){
            $api.post('countr/taxes', {
                taxName: name,
                taxRate: rate,
                shopId: $rootScope.currentShop._id,
                default: defaultTax
            })
            .then(function(response) {
                if(defaultTax){
                    setDefaultTax(response.data.tax._id);
                }
                resolve(response.data);
            })
            .catch(function(reason) {
                reject(reason);
            })
        })
    }

    function deleteTax(taxId){
        return $q(function(resolve, reject){
            $api.delete('countr/taxes/' + taxId + '/' + $rootScope.currentShop._id)
            .then(function(response) {
                resolve(response.data);
            })
            .catch(function(reason) {
                reject(reason);
            })
        })
    }

    function getWebhooks(){
        return $q(function(resolve, reject){
            $api.get('countr/webhooks', {
                shopId: $rootScope.currentShop._id
            })
            .then(function(response) {
                resolve(response.data);
            })
            .catch(function(reason) {
                reject(reason);
            })
        })
    }

    function webhookSetup(){
        return $q(function(resolve, reject){
            $api.post('countr/webhook-setup', {
                shopId: $rootScope.currentShop._id
            })
            	.then(function(response) {
                    resolve(response.data);
                })
                .catch(function(reason) {
                    reject(reason)
                })
        })
    }


    function deleteWebhook(webhookId){
        return $q(function(resolve, reject){
            $api.delete('countr/webhooks' + webhookId + '/' + $rootScope.currentShop._id)
            	.then(function(response) {
                    resolve(response.data);
                })
                .catch(function(reason) {
                    reject(reason);
                })
        })
    }

    function sync(data, reverse){
        return $q(function(resolve, reject){
            if(!reverse){
                $api.post('countr/sync-' + data, {
                    shopId: $rootScope.currentShop._id,
                    lang: $rootScope.language
                })
                    .then(function(response) {
                        resolve(response.data);
                    })
                    .catch(function(reason) {
                        reject(reason);
                    })
            }else{
                $api.get('countr/get-products-from-countr', {
                    shopId: $rootScope.currentShop._id,
                    language: $rootScope.language
                })
                	.then(function(response) {
                        resolve(response.data);
                    })
                    .catch(function(reason) {
                        reject(reason);
                    })
            }
            
        })
    }

    function getShops(){
        return $q(function(resolve, reject){
            $api.get('countr/shops', {
                shopId: $rootScope.currentShop._id
            })
            	.then(function(response) {
                    resolve(response.data);
                })
                .catch(function(reason) {
                    reject(reason);
                })
        })
    }

    function createShop(shopData){
        return $q(function(resolve, reject){
            $api.post('countr/shops', {
                shopData: shopData,
                shopId: $rootScope.currentShop._id
            })
            	.then(function(response) {
                    resolve(response.data);
                })
                .catch(function(reason) {
                    reject(reason);
                })
        })
    }


    function signupModal(){
        return $q(function(resolve, reject){
            var modalInstance = $uibModal.open({
                templateUrl: '../views/modal/countr-signup-modal.html',
                controller: 'countrSignupController'
            });
    
            modalInstance.result
                .then(function(result) {
                   resolve(reject);
                })
                
                .catch(function(reason) {
                    reject(reason);
                });
        })   
    }


    return {
        connectionStatus : connectionStatus,
        connect : connect,
        getTaxes : getTaxes,
        setDefaultTax : setDefaultTax,
        newTax : newTax,
        deleteTax : deleteTax,
        getWebhooks: getWebhooks,
        webhookSetup: webhookSetup,
        deleteWebhook: deleteWebhook,
        sync: sync,
        getShops: getShops,
        createShop: createShop,
        signupModal : signupModal
    }

}]);
