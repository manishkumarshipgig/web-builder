var steps  = {
    START_WITH_PRISMANOTE : {
        nl: "Starten met PrismaNote",
        en: "Starting with PrismaNote",
        de: "Mit PrismaNote starten",
        fr: "Commencer avec PrismaNote",
        es: "Comenzando con PrismaNote"
    },
    GENERALTOUR_WELCOME: {
        nl: "Welkom! Op deze plek kunt u uw winkel beheren en andere functies op PrismaNote gebruiken.",
        en: "Welcome! Here you can manage your store and use other functions on PrismaNote.",
        de: "Willkommen! Hier können Sie Ihren Shop verwalten und weitere Funktionen auf PrismaNote nutzen.",
        fr: "Bienvenue! Ici, vous pouvez gérer votre magasin et utiliser d'autres fonctions sur PrismaNote.",
        es: "Bienvenido! Aquí puede gestionar su tienda y utilizar otras funciones de PrismaNote."
    },
    MORE_SHOPS: {
        nl: "Meerdere winkels",
        en: "Multiple shops",
        de: "Mehrere Geschäfte",
        fr: "Magasins multiples",
        es: "Múltiples tiendas"
    },
    HERE_YOU_WILL_FIND_YOUR_SHOPS: {
        nl: "Hier vindt u uw winkels.",
        en: "Here you will find your shops.",
        de: "Hier finden Sie Ihre Shops.",
        fr: "Ici vous trouverez vos boutiques.",
        es: "Aquí encontrará sus tiendas."
    },
    SHOP_DATA: {
        nl: "Winkelgegevens",
        en: "Shop data",
        de: "Shop-Daten",
        fr: "Données de la boutique",
        es: "Datos de la tienda"
    },
    CLICK_ON_SHOP_ICON: {
        nl: "Klik op het Mijn Winkel icoon om uw winkel gegevens aan te passen.",
        en: "Click on the My Store icon to change your store data.",
        de: "Klicken Sie auf das My Store-Symbol, um Ihre Daten zu ändern.",
        fr: "Cliquez sur l'icône My Store pour modifier les données de votre magasin.",
        es: "Haga clic en el icono Mi tienda para cambiar los datos de su tienda."
    },
    FILL_IN_DATA: {
        nl: "Gegevens invullen",
        en: "Fill in data",
        de: "Daten eingeben",
        fr: "Remplir les données",
        es: "Rellene los datos"
    },
    FILL_IN_YOUR_DATA: {
        nl: "Vul uw gegevens in",
        en: "Fill in your data",
        de: "Geben Sie Ihre Daten ein",
        fr: "Remplissez vos données",
        es: "Rellene sus datos"
    },
    SHOP_BRANDS: {
        nl: "Winkel merken",
        en: "Shop brands",
        de: "Shop-Marken",
        fr: "Marques de commerce",
        es: "Marcas de tienda"
    },
    INDICATE_WHICH_BRANDS_YOU_ARE_USING: {
        nl: "Geef aan welke merken u voert.",
        en: "Indicate which brands you are using.",
        de: "Geben Sie an, welche Marken Sie verwenden.",
        fr: "Indiquez les marques que vous utilisez.",
        es: "Indique las marcas que está usando."
    },
    WEBSHOP_SETTINGS: {
        nl: "Webshop instellingen",
        en: "Webshop settings",
        de: "Webshop Einstellungen",
        fr: "Paramètres de boutique en ligne",
        es: "Configuración de la tienda web"
    },
    DETERMINE_SETTINGS_FOR_WEBHSOP: {
        nl: "Bepaal de instellingen voor uw webshop",
        en: "Determine the settings for your webshop",
        de: "Legen Sie die Einstellungen für Ihren Webshop fest",
        fr: "Déterminez les paramètres de votre boutique en ligne",
        es: "Determinar la configuración de su tienda web"
    },
    PAID_WEBHSOP: {
        nl: "Betaalde webshop",
        en: "Paid webshop",
        de: "Bezahlter Webshop",
        fr: "Magasin en ligne payant",
        es: "Tienda pagada"
    },
    WANT_A_PRO_SHOP_YOU_PAY_THIS: {
        nl: "Wilt een een professionele shop op PrismaNote? Hier betaald u 25 euro voor. We gaan nu terug naar uw overzicht.",
        en: "Want a professional shop on PrismaNote? You pay 25 euros for this. We now go back to your overview.",
        de: "Sie möchten einen professionellen Shop auf PrismaNote? Dafür zahlen Sie 25 Euro. Wir gehen nun zurück zu Ihrer Übersicht.",
        fr: "Vous voulez un magasin professionnel sur PrismaNote? Vous payez 25 euros pour ça. Nous revenons maintenant à votre vue d'ensemble.",
        es: "¿Quieres una tienda profesional en PrismaNote? Pagas 25 euros por esto. Volvemos ahora a su visión general."
    },
    USER_PROFILE: {
        nl: "Persoonlijk profiel",
        en: "User profile",
        de: "Persönliches Profil",
        fr: "Profil personnel",
        es: "Perfil personal"
    },
    CLICK_ON_USER_PROFILE: {
        nl: "Klik op Mijn profiel om uw persoonlijk profiel te beheren.",
        en: "Click on My Profile to manage your personal profile.",
        de: "Klicken Sie auf Mein Profil, um Ihr persönliches Profil zu verwalten.",
        fr: "Cliquez sur Mon profil pour gérer votre profil personnel.",
        es: "Haga clic en Mi Perfil para gestionar su perfil personal."
    },
    SHOPS_YOU_MANAGE: {
        nl: "Winkels die u beheert",
        en: "Shops you manage",
        de: "Geschäfte, die Sie verwalten",
        fr: "Magasins que vous gérez",
        es: "Tiendas que maneja"
    },
    CONTACT_ADMIN_TO_ADD_STORE: {
        nl: "Neem contact op met het PrismaNote team om een winkel aan uw account toe te voegen.",
        en: "Contact the PrismaNote team to add a store to your account.",
        de: "Wenden Sie sich an das PrismaNote-Team, um einen Speicher zu Ihrem Konto hinzuzufügen.",
        fr: "Contactez l'équipe PrismaNote pour ajouter un magasin à votre compte.",
        es: "Póngase en contacto con el equipo de PrismaNote para agregar una tienda a su cuenta."
    },
    // marketing tour
    MARKETING_PORTAL: {
        nl: "Marketing portal",
        en: "Marketing portal",
        de: "Marketing Portal",
        fr: "Portail marketing",
        es: "Portal de marketing"
    },
    ORGANISE_AND_MANAGE_ACTIONS_SOCIAL_MEDIA_CALENDER_CLICK: {
        nl: "Organiseer en beheer acties door via Social Media door op deze kalender te klikken.",
        en: "Organise and manage actions through Social Media by clicking on this calendar.",
        de: "Organisieren und verwalten Sie Aktionen über Social Media, indem Sie auf diesen Kalender klicken.",
        fr: "Organisez et gérez des actions via les médias sociaux en cliquant sur ce calendrier.",
        es: "Organiza y gestiona acciones a través de Social Media haciendo clic en este calendario."
    },
    FACEBOOK_CONNECTION: {
        nl: "Facebook koppeling",
        en: "Facebook connection",
        de: "Anmeldung in Facebook",
        fr: "Connectez-vous Facebook",
        es: "Iniciar sesión en Facebook"
    },
    TAB_SETTING_CONNECTION_FACEBOOK: {
        nl: "Klik op de tab: Instellingen. U staat in verbinding met Facebook. Koppel uw Facebook en selecteer de pagina die u door PrismaNote wilt laten beheren.",
        en: "Click on the tab: Settings. You are connected to Facebook. Link your Facebook and select the page you want PrismaNote to manage.",
        de: "Klicken Sie auf den Reiter: Einstellungen. Sie sind mit Facebook verbunden. Verlinken Sie Ihr Facebook und wählen Sie die Seite aus, die PrismaNote verwalten soll.",
        fr: "Cliquez sur l'onglet Paramètres. Vous êtes connecté à Facebook. Liez votre Facebook et sélectionnez la page que vous voulez que PrismaNote gère.",
        es: "Haga clic en la pestaña Configuración. Estás conectado a Facebook. Enlaza tu Facebook y selecciona la página que quieras que gestione PrismaNote."
    },
    UPDATES: {
        nl: "Gratis updates",
        en: "Free updates",
        de: "Kostenlose Updates",
        fr: "Mises à jour gratuites",
        es: "Actualizaciones gratuitas"
    },
    TAB_UPDATES_TOUR: {
        nl: "Gratis Updates, hier ziet u de updates die uw leveranciers hebben klaargezet. Ziet u weinig updates? Controleer dan of u alle merken hebt toegevoegd bij uw instellingen.",
        en: "Free Updates, here you can see the updates your suppliers have prepared. Do you see little updates? Make sure you have added all brands in the setting section.",
        de: "Kostenlose Updates, hier sehen Sie die Updates, die Ihre Lieferanten vorbereitet haben. Sehen Sie kleine Updates? Stellen Sie sicher, dass Sie alle Marken zu Ihren Einstellungen hinzugefügt haben.",
        fr: "Mises à jour gratuites, ici vous pouvez voir les mises à jour que vos fournisseurs ont préparées. Assurez-vous d'avoir ajouté toutes les marques à vos paramètres.",
        es: "Actualizaciones gratuitas, aquí puede ver las actualizaciones que sus proveedores han preparado. ¿Ves pequeñas actualizaciones? Asegúrate de haber agregado todas las marcas a tu configuración."
    },
    PAID_PROMOTIONS: {
        nl: "Betaalde promoties",
        en: "Paid promotions",
        de: "Bezahlte Aktionen",
        fr: "Promotions payées",
        es: "Promociones pagadas"
    },
    TAB_PAID_PROMOTIONS: {
        nl: "Promoties, hier ziet u de campagnes die uw leveranciers hebben klaargezet. Anders dan updates zijn promoties betaalde updates waar u betaald aan Facebook om VEEL MEER mensen te bereiken.",
        en: "Promotions, here you can see the campaigns that your suppliers have prepared. Unlike updates, promotions are paid updates where you pay Facebook to reach more people.",
        de: "Promotions, hier sehen Sie die Kampagnen, die Ihre Lieferanten vorbereitet haben. Im Gegensatz zu Updates sind Promotions bezahlte Updates, bei denen du Facebook bezahlst, um mehr Leute zu erreichen.",
        fr: "Promotions, vous pouvez voir ici les campagnes que vos fournisseurs ont préparées. Contrairement aux mises à jour, les promotions sont payantes et vous payez Facebook pour atteindre plus de gens.",
        es: "Promociones, aquí puedes ver las campañas que tus proveedores han preparado. A diferencia de las actualizaciones, las promociones son actualizaciones pagadas donde se paga a Facebook para llegar a más personas."
    },
    ADD_A_PROMOTION_YOURSELF: {
        nl: "Zelf een promotie toevoegen",
        en: "Add a promotion yourself",
        de: "Eigene Promotion hinzufügen",
        fr: "Ajoutez votre propre promotion",
        es: "Añade tu propia promoción"
    },
    ADD_A_PROMOTION_TO_YOUR_ACCOUNT: {
        nl: "Voeg hier uw campagne toe aan uw account.",
        en: "Add your campaign to your account here.",
        de: "Fügen Sie hier Ihre Kampagne zu Ihrem Konto hinzu.",
        fr: "Ajoutez votre campagne à votre compte ici.",
        es: "Añade tu campaña a tu cuenta aquí."
    },
    SWITCHING_ON_OFF: {
        nl: "In-/Uitschakelen",
        en: "Switching on/off",
        de: "Ein-/Ausschalten",
        fr: "Activation/désactivation",
        es: "Encendido/apagado"
    },
    THIS_CAMPAIGN_GOT_TASKS_DETERMINE_IF_YOU_WANT_TO_SWITCH_THEM_ON_OFF: {
        nl: "Deze campagne heeft één of meerdere taken. Bepaal met behulp van deze switch of u deze taak wel of niet wilt uitvoeren.",
        en: "This campaign has one or more tasks. Use this switch to determine whether or not you want to perform this task.",
        de: "Diese Kampagne hat eine oder mehrere Aufgaben. Mit diesem Schalter können Sie festlegen, ob Sie diese Aufgabe ausführen möchten oder nicht.",
        fr: "Cette campagne a une ou plusieurs tâches. Utilisez ce commutateur pour déterminer si vous voulez ou non exécuter cette tâche.",
        es: "Esta campaña tiene una o más tareas. Utilice este indicador para determinar si desea o no realizar esta tarea."
    },
    CHECK: {
        nl: "Controleren",
        en: "Check",
        de: "Kontrolle",
        fr: "Vérifier",
        es: "Comprobar"
    },
    CHECK_TEXT_AND_PHOTO_FOR_FACEBOOK: {
        nl: "Controleer de tekst en foto die op Facebook komen te staan.",
        en: "Check the text and photo that appear on Facebook.",
        de: "Überprüfen Sie den Text und das Foto, die auf Facebook angezeigt werden.",
        fr: "Vérifiez le texte et la photo qui apparaissent sur Facebook.",
        es: "Comprueba el texto y la foto que aparecen en Facebook."
    },
    PLACE_THIS_NOW: {
        nl: "Plaatsen!",
        en: "Place this now!",
        de: "Platziere das jetzt!",
        fr: "Placez ça maintenant!",
        es: "¡Pon esto ahora!"
    },
    CLICK_BUTTON_TO_CHECK_POST_CAMPAIGN: {
        nl: "Klik op deze knop om uw campagne te controleren en te plaatsen.",
        en: "Click this button to check and post your campaign.",
        de: "Klicken Sie auf diese Schaltfläche, um Ihre Kampagne zu überprüfen und zu posten.",
        fr: "Cliquez sur ce bouton pour vérifier et afficher votre campagne.",
        es: "Haga clic en este botón para comprobar y publicar su campaña."
    },
    EDIT_ASSORTIMENT: {
        nl: "Assortiment bewerken",
        en: "Edit assortment",
        de: "Sortiment bearbeiten",
        fr: "Modifier assortiment",
        es: "Editar surtido"
    },
    MANAGE_AND_EDIT_ASSORTIMENT: {
        nl: "Beheer en bewerk hier uw assortiment",
        en: "Manage and edit your assortment here",
        de: "Sortimente hier verwalten und bearbeiten",
        fr: "Gérer et éditer votre assortiment ici",
        es: "Gestione y edite su surtido aquí"
    },
    CATEGORIES: {
        nl: "Categorien",
        en: "Categories",
        de: "Kategorien",
        fr: "Catégories",
        es: "Categorías"
    },
    HERE_YOU_FIND_CATEGORIES: {
        nl: "Hier vindt u verschillende categorieën van uw producten.",
        en: "Here you will find different categories of your products.",
        de: "Hier finden Sie verschiedene Kategorien Ihrer Produkte.",
        fr: "Ici vous trouverez différentes catégories de vos produits.",
        es: "Aquí encontrará diferentes categorías de sus productos."
    },
    ADD_PRODUCT: {
        nl: "Product toevoegen",
        en: "Add product",
        de: "Produkt hinzufügen",
        fr: "Ajouter produit",
        es: "Añadir producto"
    },
    CLICK_ON_ADD_PRODUCT_TO_ADD_PRODUCTS: {
        nl: "Klik op Product toevoegen om producten toe te voegen.",
        en: "Click on Add Product to add products.",
        de: "Klicken Sie auf Produkt hinzufügen, um Produkte hinzuzufügen.",
        fr: "Cliquez sur Ajouter un produit pour ajouter des produits.",
        es: "Haga clic en Añadir producto para añadir productos."
    },
    SEARCH_ARTICLE: {
        nl: "Artikel zoeken",
        en: "Search article",
        de: "Artikel suchen",
        fr: "Article de recherche",
        es: "Buscar artículo"
    },
    TYPE_IN_ARTICLE_NUMBER_TO_ADD_PRODUCT: {
        nl: "Zoek op het artikelnummer om dit product toe te voegen.",
        en: "Search the article number to add this product.",
        de: "Durchsuchen Sie die Artikelnummer, um dieses Produkt hinzuzufügen.",
        fr: "Recherchez le numéro d'article pour ajouter ce produit.",
        es: "Buscar el número de artículo para añadir este producto."
    },
    EDIT_PRODUCT: {
        nl: "Product bewerken",
        en: "Edit product",
        de: "Einstellen Produkt",
        fr: "Modifier produit",
        es: "Ajustar producto"
    },
    CLICK_ON_A_PRODUCT_TO_CHANGE_ITS_DATA: {
        nl: "Klik op een product om de gegevens hiervan te wijzigen.",
        en: "Click on a product to change its data.",
        de: "Klicken Sie auf ein Produkt, um seine Daten zu ändern.",
        fr: "Cliquez sur un produit pour modifier ses données.",
        es: "Haga clic en un producto para cambiar sus datos."
    },
    STOCK_PRICE_ETC: {
        nl: "Voorraad, Prijs, etc.",
        en: "Stock, Price, etc.",
        de: "Vorrat, Preis, etc.",
        fr: "Stock, prix, etc.",
        es: "Stock, Precio, etc."
    },
    DETERMINE_STOCK_PRICE: {
        nl: "Bepaal hier het aantal items wat u op voorraad hebt. U kunt in de andere velden gegevens zoals Prijs etc. aanpassen.",
        en: "Determine the number of items you have in stock. You can adjust data such as Price etc. in the other fields.",
        de: "Bestimmen Sie die Anzahl der Artikel, die Sie auf Lager haben. In den anderen Feldern können Sie Daten wie Preis usw. anpassen.",
        fr: "Déterminez le nombre d'articles que vous avez en stock. Vous pouvez corriger des données telles que Prix, etc. dans les autres zones.",
        es: "Determine el número de artículos que tiene en stock. Puede ajustar datos como Precio, etc. en los otros campos."
    },
    SHARING_PRODUCT_INFO: {
        nl: "Productinformatie delen",
        en: "Sharing product information",
        de: "Gemeinsame Nutzung von Produktinformationen",
        fr: "Partage d'informations sur les produits",
        es: "Compartir información sobre productos"
    },
    EXPLAIN_CENTRAL_DB_SHARING_SO_EDIT_TO_MAKE_VISIBLE: {
        nl: "PrismaNote werkt met een centrale database. Dit betekent dat productinformatie wordt gedeeld tussen winkeliers. Informatie moet compleet zijn om het product zichtbaar te maken.",
        en: "PrismaNote works with a central database. This means that product information is shared between retailers. Information must be complete to make the product visible.",
        de: "PrismaNote arbeitet mit einer zentralen Datenbank. Dies bedeutet, dass Produktinformationen zwischen Einzelhändlern ausgetauscht werden. Die Angaben müssen vollständig sein, um das Produkt sichtbar zu machen.",
        fr: "PrismaNote fonctionne avec une base de données centrale. Cela signifie que les informations sur les produits sont partagées entre les détaillants. L'information doit être complète pour rendre le produit visible.",
        es: "PrismaNote trabaja con una base de datos central. Esto significa que la información del producto se comparte entre los minoristas. La información debe estar completa para que el producto sea visible."
    },
    VIEW_ORDERS: {
        nl: "Orders bekijken",
        en: "View orders",
        de: "Bestellungen anzeigen",
        fr: "Voir les commandes",
        es: "Ver pedidos"
    },
    CLICK_ON_THE_ORDER_ICON_TO_VIEW_ORDERS: {
        nl: "Klik op het orders icoon om uw orders op uw webshop te bekijken.",
        en: "Click on the order icon to view your orders on your webshop.",
        de: "Klicken Sie auf das Bestellsymbol, um Ihre Bestellungen in Ihrem Webshop einzusehen.",
        fr: "Cliquez sur l'icône de commande pour visualiser vos commandes sur votre boutique en ligne.",
        es: "Haga clic en el icono de pedido para ver sus pedidos en su tienda web."
    },
    SENT_ORDERS: {
        nl: "Order verzenden",
        en: "Send order",
        de: "Bestellung absenden",
        fr: "Envoyer commande",
        es: "Enviar pedido"
    },
    FIND_YOUR_ORDERS_HERE_SHIP_WITH_PACKING_SLIP_ETC: {
        nl: "Hier vindt u uw orders. Wilt u een product versturen? Klik hier om de pakbon te downloaden en klik op verzenden",
        en: "Here you will find your orders. Do you want to ship a product? Click here to download the packing list and click on Send.",
        de: "Hier finden Sie Ihre Bestellungen. Möchten Sie ein Produkt versenden? Klicken Sie hier, um die Packliste herunterzuladen und klicken Sie auf Senden.",
        fr: "Ici vous trouverez vos commandes. Voulez-vous expédier un produit? Cliquez ici pour télécharger la liste d'emballage et cliquez sur Envoyer.",
        es: "Aquí encontrará sus pedidos. ¿Desea enviar un producto? Haga clic aquí para descargar la lista de embalaje y haga clic en Enviar."
    },
    HANDLING_OF_RETURNS: {
        nl: "Retour afhandelen",
        en: "Handling of returns",
        de: "Retourenabwicklung",
        fr: "Traitement des retours",
        es: "Tramitación de las devoluciones"
    },
    SENT_ORDERS_ARE_HERE_CLICK_BUTTON_WHEN_YOU_GOT_RETURNS: {
        nl: "Bekijk hier de verzonden orders. Het de consument het pakket terug gestuurd? Klik op de knop om de klant een nieuw product op te sturen of de klant terug te betalen.",
        en: "View the sent orders here. It sends the package back to the consumer? Click on the button to send a new product to the customer or to refund the customer.",
        de: "Hier können Sie die versendeten Bestellungen einsehen. Es schickt das Paket zurück zum Verbraucher? Klicken Sie auf den Button, um ein neues Produkt an den Kunden zu senden oder den Kunden zu erstatten.",
        fr: "Voir les commandes envoyées ici. Il renvoie le colis au consommateur? Cliquez sur le bouton pour envoyer un nouveau produit au client ou pour le rembourser.",
        es: "Ver los pedidos enviados aquí. Envía el paquete al consumidor? Haga clic en el botón para enviar un nuevo producto al cliente o para devolverle el dinero."
    },
    REVENUES: {
        nl: "Inkomsten",
        en: "Revenues",
        de: "Einnahmen",
        fr: "Revenus",
        es: "Ingresos"
    },
    CLICK_ON_FINANCIAL_ICON_TO_SEE_YOUR_PAYMENTS_FROM_PRISMANOTE: {
        nl: "Klik op het Financieel icoon om uw te kijken welke betalingen u ontvangt.",
        en: "Click on the Financial icon to see what payments you receive.",
        de: "Klicken Sie auf das Symbol Finanzen, um zu sehen, welche Zahlungen Sie erhalten.",
        fr: "Cliquez sur l'icône Financière pour voir quels paiements vous recevez.",
        es: "Haga clic en el icono Financiero para ver qué pagos recibe."
    },
    ACCOUNT_HOLDER: {
        nl: "Rekeninghouder",
        en: "Account holder",
        de: "Kontoinhaber",
        fr: "Titulaire du compte",
        es: "Titular de la cuenta"
    },
    CHECK_ACCOUNT_HOLDER_YOUR_MONEY_YOU_RECEIVE_WITHIN_21_28_DAYS: {
        nl: "Betaalde orders zullen via PrismaNote binnen 21 tot 28 dagen worden uitbetaald op uw rekening. Bekijk alstublieft of u uw gegevens juist hebt ingevuld.",
        en: "Orders paid for will be paid via PrismaNote within 21 to 28 days on your account. Please check if you have entered your details correctly.",
        de: "Bezahlte Bestellungen werden über PrismaNote innerhalb von 21 bis 28 Tagen auf Ihr Konto bezahlt. Bitte überprüfen Sie, ob Sie Ihre Daten korrekt eingegeben haben.",
        fr: "Les commandes payées seront réglées via PrismaNote dans un délai de 21 à 28 jours sur votre compte. Veuillez vérifier si vous avez entré vos coordonnées correctement.",
        es: "Los pedidos pagados se pagarán a través de PrismaNote en un plazo de 21 a 28 días en su cuenta. Por favor, compruebe si ha introducido sus datos correctamente."
    },
    PAYMENT_OVERVIEW: {
        nl: "Betalingsoverzicht",
        en: "Payment overview",
        de: "Aperçu des paiements",
        fr: "Zahlungsübersicht",
        es: "Resumen de pagos"
    },
    VIEW_YOUR_PAYOUTS_HERE: {
        nl: "Bekijk hier uw overzicht van uitbetalingen op uw account.",
        en: "View your payout overview on your account here.",
        de: "Hier können Sie Ihre Auszahlungsübersicht auf Ihrem Konto einsehen.",
        fr: "Visualisez ici votre aperçu des paiements sur votre compte.",
        es: "Vea aquí su resumen de pagos en su cuenta."
    },
    EMAIL_MARKETING: {
        nl: "Emailmarketing",
        en: "Email Marketing",
        de: "E-Mail-Marketing",
        fr: "Marketing par courriel",
        es: "Email Marketing"
    },
    CLICK_ON_PREMIUM_SERVICE_ICON_TO_USE_EMAILMARKETING: {
        nl: "Klik op het Premium Service icoon om de Emailmarketing/CRM app te gebruiken.",
        en: "Click on the Premium Service icon to use the email marketing/CRM app.",
        de: "Klicken Sie auf das Premium Service Symbol um die E-Mail Marketing/CRM App zu nutzen.",
        fr: "Cliquez sur l'icône Premium Service pour utiliser l'application email marketing / CRM.",
        es: "Haga clic en el icono Servicio Premium para utilizar la aplicación de marketing por correo electrónico/CRM."
    },
    A_WEB_APP_VIA_THE_INTERNET: {
        nl: "Een WEB-App?! Via het internet",
        en: "A WEB App! Via the Internet",
        de: "Eine WEB App! Über das Internet",
        fr: "Une application WEB! Via Internet",
        es: "A WEB App! A través de Internet"
    },
    THIS_APP_IS_BEST_USED_FOR_MOBILE_PHONES_CLICK_HERE_FOR_MANUAL_CLICK_NEXT_TO_VIEW_ON_DESKTOP: {
        nl: "Eigenlijk is deze App het best te gebruiken op mobiel. Hiervoor kunt u het beste op uw Ipad of Smartphone inloggen en de tour opnieuw starten. Klik volgende, om verder te gaan op uw computer.",
        en: "In fact, this App is best used on mobile phones. For this, it is best to log in with your Ipad or Smartphone and restart the tour. Click next, to continue on your computer.",
        de: "Diese App ist am besten für Mobiltelefone geeignet. Zu diesem, ist es am besten, sich mit Ihrem Ipad oder Smartphone anzumelden und die Tour neu zu starten. Klicken Sie auf Weiter, um auf Ihrem Computer fortzufahren.",
        fr: "En fait, cette application est mieux utilisée sur les téléphones mobiles. Pour ce faire, il est préférable de vous connecter avec votre Ipad ou Smartphone et de redémarrer la visite. Cliquez sur Suivant, pour continuer sur votre ordinateur.",
        es: "De hecho, esta aplicación se utiliza mejor en los teléfonos móviles. Para hacer esto, es preferible iniciar sesión con su Ipad o Smartphone y reiniciar la visita. Haga clic en Siguiente, para continuar en el equipo."
    },
    NEW_CUSTOMER: {
        nl: "Nieuwe klant",
        en: "New customer",
        de: "Neuer Kunde",
        fr: "Nouveau client",
        es: "Nuevo cliente"
    },
    CLICK_TO_REGISTER_NEW_CUSTOMER_VIA_PN_PROFILE_WEBSITE: {
        nl: "Klik hier om een nieuwe klant te registreren. Dit kan via uw PrismaNote profiel/website.",
        en: "Click here to register a new customer. You can do this via your PrismaNote profile/website.",
        de: "Klicken Sie hier, um einen neuen Kunden zu registrieren. Sie können dies über Ihr PrismaNote-Profil/Website tun.",
        fr: "Cliquez ici pour enregistrer un nouveau client. Vous pouvez le faire via votre profil/site web PrismaNote.",
        es: "Haga clic aquí para registrar un nuevo cliente. Puede hacerlo a través de su perfil/sitio web PrismaNote."
    },
    PURCHASE_REGISTRATION: {
        nl: "Aankoopregistratie",
        en: "Purchase registration",
        de: "Kaufregistrierung",
        fr: "Enregistrement des achats",
        es: "Registro de compra"
    },
    RECORD_PURCHASE_DATA_ON_THIS_PAGE_CHOOSE_WATCH_OR_JEWEL: {
        nl: "Op deze pagina kunnen klanten hun Aankoop vastleggen na de aankoop. Klik of uw klant een horloge, een sieraad heeft gekocht. Klik op volgende als de klant een horloge heeft gekocht.",
        en: "On this page, customers can record their Purchase after the purchase. Click whether your customer has bought a watch, a piece of jewelry. Click on next if the customer has bought a watch.",
        de: "Auf dieser Seite können Kunden nach dem Kauf ihre Bestellung aufzeichnen. Klicken Sie, ob Ihr Kunde eine Uhr, ein Schmuckstück gekauft hat. Klicken Sie auf Weiter, wenn der Kunde eine Uhr gekauft hat.",
        fr: "Sur cette page, les clients peuvent enregistrer leur achat après l'achat. Cliquez sur si votre client a acheté une montre, un bijou. Cliquez sur Suivant si le client a acheté une montre.",
        es: "En esta página, los clientes pueden registrar su compra después de la compra. Haga clic en si su cliente ha comprado un reloj, una pieza de joyería. Haga clic en Siguiente si el cliente ha comprado un reloj."
    },
    PICTURE_OF_WARRANTY_CERTIFICATE_AND_FILL_IN: {
        nl: "Foto van garantiebewijs en invullen klant gegevens",
        en: "Photo of guarantee certificate and customer data to be filled in",
        de: "Foto des Garantiescheins und auszufüllende Kundendaten",
        fr: "Photo du certificat de garantie et des données du client à compléter",
        es: "Foto del certificado de garantía y datos del cliente que deben cumplimentarse"
    },
    CLICK_ON_GREY_BOS_AND_FILL_IN: {
        nl: "Klik op het grijze vak om een foto te maken van zijn garantiebewijs, certificaat of kassabon. En vul de gegevens van de klant verder aan.",
        en: "Click on the grey box to take a picture of his warranty certificate, certificate or receipt. And complete the customer's details.",
        de: "Klicken Sie auf das graue Kästchen, um ein Foto von seinem Garantieschein, seinem Zertifikat oder seiner Quittung zu machen. Und vervollständigen Sie die Kundendaten.",
        fr: "Cliquez sur la case grise pour prendre une photo de son certificat de garantie, certificat ou reçu. Et complétez les coordonnées du client.",
        es: "Haga clic en la casilla gris para tomar una foto de su certificado de garantía, certificado o recibo. Y completar los detalles del cliente."
    },
    SEND_EMAIL: {
        nl: "Email verzenden",
        en: "Send email",
        de: "E-Mail senden",
        fr: "Envoyer un email",
        es: "Enviar email"
    },
    SEND_MY_WARRANTY_CERTIFICATE_TO_CUSTOMER_EMAIL_WE_LINK_YOU_TO_OVERVIEW_OF_DATA: {
        nl: "Klik op Stuur mijn garantiebewijs om een email naar de klant zijn adres te sturen. Nadat de email verstuurd is naar uw klant kunt u terug naar u overzicht gaan om de ingevulde klantgegevens te bekijken. Klik op volgende om naar uw overzicht te worden doorgelinkt",
        en: "Send my warranty certificate to send an email to the customers address. After the email has been sent to your customer, you can go back to your overview to view the customer data you entered. Click on the following to be linked to your overview",
        de: "Klicken Sie auf Garantiezertifikat senden, um eine E-Mail an die Adresse des Kunden zu senden. Nachdem die E-Mail an Ihren Kunden versendet wurde, können Sie in der Übersicht die von Ihnen eingegebenen Kundendaten einsehen. Klicken Sie auf die folgenden Links, um zu Ihrer Übersicht zu gelangen",
        fr: "Cliquez sur Envoyer mon certificat de garantie pour envoyer un email à l'adresse du client. Une fois l' e-mail envoyé à votre client, vous pouvez revenir à votre aperçu pour afficher les données client que vous avez saisies. Cliquez sur les liens suivants pour accéder à votre aperçu",
        es: "Haga clic en Enviar mi certificado de garantía para enviar un correo electrónico a la dirección del cliente. Después de que el correo electrónico se haya enviado a su cliente, puede volver a la vista general para ver los datos de cliente que ha introducido. Haga clic en los siguientes enlaces para acceder a su vista general"
    },
    EMAIL_MARKETING_CLIENT_DATA: {
        nl: "Emailmarketing/klantgegevens",
        en: "Email marketing / customer data",
        de: "E-Mail-Marketing / Kundendaten",
        fr: "Email marketing / données clients",
        es: "Email marketing / datos del cliente"
    },
    CLICK_ON_PREMIUM_SERVICE_TO_VIEW_CUSTOMER_DATA: {
        nl: "Klik op het Premium Service icoon om de ingevulde klantgegevens in de Emailmarketing/CRM app te bekijken.",
        en: "Click on the Premium Service icon to view the customer data entered in the email marketing /CRM app.",
        de: "Klicken Sie auf das Premium Service-Symbol, um die Kundendaten in der E-Mail-Marketing /CRM-App zu sehen.",
        fr: "Cliquez sur l'icône Premium Service pour visualiser les données clients saisies dans l'application email marketing /CRM.",
        es: "Haga clic en el icono Servicio Premium para ver los datos del cliente introducidos en la aplicación de marketing /CRM."
    },
    VIEWING_DATA: {
        nl: "Gegevens bekijken",
        en: "Viewing data",
        de: "Daten ansehen",
        fr: "Visualisation des données",
        es: "Visualización de datos"
    },
    CLICK_ON_CUSTOMER_TO_SEE_DETAILS_AND_PHOTO: {
        nl: "Klik op de klant om zijn gegevens te zien en de genomen foto.",
        en: "Click on the customer to see his details and the photo taken.",
        de: "Klicken Sie auf den Kunden, um seine Daten und das aufgenommene Foto zu sehen.",
        fr: "Cliquez sur le client pour voir ses coordonnées et la photo prise.",
        es: "Haga clic en el cliente para ver sus datos y la foto tomada."
    },
    SCHEDULED_EMAILS: {
        nl: "Emails inplannen",
        en: "Scheduled emails",
        de: "Email Planung",
        fr: "Courriels programmés",
        es: "Emails programados"
    },
    CUSTOMER_WILL_AUTOMATICALLY_RECEIVE_EMAILS_CLICK_ON_ARROW: {
        nl: "De klant krijgt nu automatisch emails op vooraf ingeplande data. Bekijk de emails door op het pijltje te klikken.",
        en: "The customer will now automatically receive emails on pre-planned data. View the emails by clicking on the arrow.",
        de: "Der Kunde erhält nun automatisch E-Mails über vorgeplante Daten. Klicken Sie auf den Pfeil, um die E-Mails anzuzeigen.",
        fr: "Le client recevra automatiquement des courriels sur les données préprogrammées. Affichez les courriels en cliquant sur la flèche.",
        es: "El cliente recibirá ahora automáticamente correos electrónicos con los datos planificados previamente. Vea los correos electrónicos haciendo clic en la flecha."
    }
}