# Server commands
## PM2 environment
Several command to restart processes on the production server
The server use pm2 to ensure that there some processes are running continuous.

Currently those processes are running with pm2:

1. Prismanote Node
2. Auto-deploy coffee script

Both processes are loaded via an ecosystem which can be found in `~/ecosystem.config.js`
When PM2 for some reason is not running, or not properly, you can use the follow commands to start, reload or stop the processes:

```
pm2 start ~/ecosystem.config.js

pm2 reload ~/ecosystem.config.js

pm2 stop ~/ecosystem.config.js

```
When the ecosystem file is edited, you must run the reload command to let pm2 now to load the new settings.
By default the Prismanote Node app is running in production environiment via PM2. 

The autodeploy script is triggered by the bitbucket webhook. When triggered this script does a `git pull master` and restarts the node process. This process must be running otherwise the webhook will fail.

```
pm2 start --interpreter coffee ~/admin/bin/auto-deploy.coffee
```

In [Bitbucket](https://bitbucket.org/excellentelectronics/prismanote-2/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin) you can see the status/log from the webhooks.

## MongoDB backup
To create an full backup of the database you can use `mongodump`. This command will create an full export in the given folder. Change the folder name to the current date. We use sudo to be sure that the folder can be created.

`sudo mongodump --db prismanote --out ~/mongodump/14122017`