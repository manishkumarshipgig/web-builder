const knox = require('knox');
const s3 = require('s3');
const fs = require('fs');
const uuid = require('uuid');
const path = require('path');
const http = require('follow-redirects').http;
const https = require('follow-redirects').https;
const Url = require('url');
const stringService = require(path.join(__dirname, '..', 'services', 'string.service'));

module.exports.uploadFile = uploadFile;
module.exports.deleteFile = deleteFile;
module.exports.uploadStream = uploadStream;
module.exports.getFile = getFile;


function createClient(type){
    if(!type || type == "knox"){
        var client = knox.createClient({
            key: settings.s3Bucket.key,
            secret: settings.s3Bucket.secret,
            bucket: settings.s3Bucket.bucket,
            region: settings.s3Bucket.region
        });
        return client;
    }else if(type == "s3"){
        var client = s3.createClient({
            s3Options: {
                accessKeyId: settings.s3Bucket.key,
                secretAccessKey: settings.s3Bucket.secret
            },
            region: settings.s3Bucket.region
        });
        return client;
    }
}

function uploadFile(file, folder, options, callback){
    
    if (!file)
    {
        return callback("No file");
    }

    var filename = path.basename(file.originalFilename, path.extname(file.originalFilename));

    if(options){
        //option to override the filename
        if (options.fileName) {
            filename = options.fileName;
        }
    }

    filename = stringService.slugify(filename) + path.extname(file.originalFilename);

    fs.readFile(file.path, function(err, data){
        if (err) {
            return callback(err);
        }
        var client = createClient();

        var headers = {
            'Content-Length' : data.length,
            'Content-Type': file.type
        }
        console.log("Folder Name", folder);
        
        client.putBuffer(data, folder+"/"+filename, headers, function(err, result){
            if (err) {
                return callback(err);
            }else{
                return callback(null, folder+"/"+filename);
            }
        })
    })
}

function deleteFile(file){

}

function uploadStream(stream, folder, options, callback){
    var url = Url.parse(stream);

    if(!stream){
        console.log("no stream");
        return callback("No stream!")
    }

    if(stream.indexOf("http://") == -1 && stream.indexOf("https://") == -1){
        console.log("invalid url");
        return callback("Invalid URL!");
    }

    var originalFile = path.basename(url.pathname);
    var filename = stringService.slugify(originalFile);

    if(options){
        //option to override the filename
        if (options.fileName) {
            filename = options.fileName;
        }
    }

    filename = stringService.slugify(filename) + path.extname(originalFile);

    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    var protocol = url.protocol == "https:" ? https : http;

    var request = protocol.get(stream, function(response){

        if(response.statusCode == 200){
            var url = Url.parse(stream);
            
            var headers = {
                'Content-Length' : response.headers['content-length'],
                'Content-Type' : response.headers['content-type'],
                'x-amz-acl': 'public-read'
            }
            var client = createClient();
           
            client.putStream(response, folder+"/"+filename, headers, function(err, result){
                if(!err && result.statusCode == 200){
                    return callback(null, folder+"/"+filename);
                }else{                 
                    var error = {
                        "statusCode": result && result.statusCode ? result.statusCode : "null",
                        "statusMessage": result && result.statusMessage ? result.statusMessage : "null",
                        "error": err ? err: "null",
                        "url": stream
                    };
                    request.abort();
                    return callback(error);
                }
            })
       }else{

            var result = {
                "statusCode": response.statusCode ? response.statusCode : "null",
                "statusMessage": response.statusMessage ? response.statusMessage : "null",
                "error": "null",
                "url": stream
            }
            request.abort();
            return callback(result);
       }
    }).on('error', function(ex){
        console.error("error http/https get", ex);
        var result = {
            "statusCode": "null",
            "statusMessage": ex,
            "error" :  "null",
            "url" : stream
        }
        request.abort();
        return callback(result)
    });

    // request.setTimeout(50000, function(){
    //     var error = {
    //         "statusCode": "null",
    //         "statusMessage": "request timed out",
    //         "error": "null",
    //         "url": stream
    //     };
    //     request.abort();
    //     return callback(error);
    // })
}

function getFile(file, options, callback){
    if(!file){
        return callback("No file provided!")
    }

    var localFolder = 'download';
    var fileName = path.basename(file);

    if(options){
        if(options.localFolder){
            localFolder = options.localFolder
        }
        if(options.localFileName){
            fileName = options.localFileName
        }
    }
    var localFile = "tmp/" + localFolder + "/" + fileName;

    var client = createClient("s3");

    var params = {
        localFile: localFile,
        s3Params: {
            Bucket: settings.s3Bucket.bucket,
            Key: file
        }
    }

    var downloader = client.downloadFile(params);

    downloader.on('error', function(err){
        console.error("Unable to download:", err.stack)
        return callback(err);
    });
    downloader.on('progress', function(){
        //console.log("progress", downloader.progressAmount, downloader.progressTotal);
    });
    downloader.on('end', function(){
        return callback(null, localFile);
    })

    

}