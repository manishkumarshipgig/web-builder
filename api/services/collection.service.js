// DO NOT CHANGE THE CODE OF THIS FILE IN CASE YOU WANT SOME CHANGES. In such case, please contact Tirthraj Barot, Rajkot-Team.

const mongoose = require('mongoose');
const Shop = mongoose.model('Shop');
const Product = mongoose.model('Product');
const _ = require('lodash');
const Q = require('q');

module.exports.getUniqueCollectionsFromTheProductsInShop = getUniqueCollectionsFromTheProductsInShop;
module.exports.getUniqueCollectionsFromProductsArray = getUniqueCollectionsFromProductsArray;
module.exports.getNewCollectionsForShopByProductsArray = getNewCollectionsForShopByProductsArray;
module.exports.updateShopWithProperCollections = updateShopWithProperCollections;
module.exports.updateShopWithProperCollectionsByShopId = updateShopWithProperCollectionsByShopId;
module.exports.loadShopCollections = loadShopCollections;



function getUniqueCollectionsFromTheProductsInShop(param){
	var defer = Q.defer();
	Shop.findOne(param)
	.populate("products._id")
	.lean()
	.exec(function(err,shop){
		(err) ? defer.reject(err) : defer.resolve(getUniqueCollectionsFromProductsArray(shop.products));
	})
	return deffered.promise;
}



function loadShopCollections(shop){
	for(var i = 0 ; i < shop.collections.length; i++)
		shop.collections[i] = Object.assign(shop.collections[i],shop.collections[i]._id)
	return shop.collections;
}



function getUniqueCollectionsFromProductsArray(p_ara){
	var colIds = [];
	for(var i = 0; i < p_ara.length; i++)
		if(p_ara[i] != null && p_ara[i]._id != null && p_ara[i]._id.collections)
			for(j = 0; j < p_ara[i]._id.collections.length;j++){
				if(p_ara[i]._id.collections[j] == null){
					console.log("Null Collection Found. Therefore skipping");
					continue;
				}
				colIds.push(p_ara[i]._id.collections[j]._id);
			}
			return [... new Set(colIds)];
		}


function getNewCollectionsForShopByProductsArray(shop,product){

	var shopUniqCollections = _.uniq(getUniqueCollectionsFromProductsArray(shop.products));
	shopUniqCollections = _.uniqWith(shopUniqCollections, _.isEqual);
	console.log("Unique Collections : ",shopUniqCollections.length);
	if(shopUniqCollections.length == 0) 
		return [];


	var toBeAdded = [];
	var toBeKept = [];
	var toBeAddedFinal = [];
	var toBeKeptFinal = [];

	// Adding
	for(var i = 0; i < shopUniqCollections.length; i++){
		if(shopUniqCollections[i] == undefined) continue;
		if(shopUniqCollections[i]._id == null) continue;

		if(existsInArray(shopUniqCollections[i]._id, shop.collections))
			toBeKept.push(shopUniqCollections[i]);
		else
			toBeAdded.push(shopUniqCollections[i]);
	}

	
	for(var i = 0; i < toBeKept.length; i++)
		toBeKeptFinal.push({
			_id: toBeKept[i]._id,
			countrId: getCountrValue(toBeKept[i]._id,shop.collections)	
		})
	

	for(var i = 0; i < toBeAdded.length; i++)
		toBeAddedFinal.push({
			_id: toBeAdded[i]._id,
			countrId: null
		})
	
	// REMOVING ALL THE COLLECTIONS
	shop.collections = [];
	shop.collections = [...toBeAddedFinal,...toBeKeptFinal];
	
	return _.uniqBy(shop.collections,'_id');
}

function removeCollectionById(collections,id){
	// DO NOT USE THIS FUNCTION ANYWHERE. THIS IS CODED FOR VERY SPECIFIC FUNCTION.
	for(var i = 0; i < collections.length; i++)
		if(collections[i]._id == id)
			collections.splice(i,1);
		return collections;
}



function updateShopWithProperCollections(shop){
	var defer = Q.defer();
	shop.collections = getNewCollectionsForShopByProductsArray(shop);
	Shop.findByIdAndUpdate(shop._id,shop,{new:true,upsert:true})
	.exec(function(err,finalShop){
		if(err) defer.reject(err);
		return defer.resolve(finalShop);
	})
	return defer.promise;
}



function updateShopWithProperCollectionsByShopId(shopId){
	
	var defer = Q.defer();
	Shop.findById(shopId)
	.populate("products._id")
	.exec(function(err,shop){
		updateShopWithProperCollections(shop)
		.then((finalShop)=>defer.resolve(finalShop),(err)=>defer.reject(err));
	})
	return defer.promise;
}



function existsInArray(needle,hay){
	if(needle == null) return false;
	for(var i = 0; i < hay.length; i++) 
		if(hay[i]._id.toString() == needle.toString()) 
			return true;
		return false;
}

function getCountrValue(needle,hay){
	for(var i = 0; i < hay.length; i++) 
		if(hay[i]._id.toString() == needle.toString()) 
			return (hay[i].countrId == undefined) ? null : hay[i].countrId;
		return false;
}
