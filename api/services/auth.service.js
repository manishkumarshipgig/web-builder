const mongoose = require('mongoose');
const externalApiToken = mongoose.model('externalApiToken');


module.exports = function ensureAuthenticated(role) {
    return function(req,res,next) {
        if(typeof role == "undefined" || role.length == 0 || !req.user) {
            if(req.isAuthenticated()) {
               return next();
            }else{
                var token = req.headers['x-access-token'];
                var externalToken = '';

                if (!token){
                    //disabled this since users aren't allowd to register or log in
                    return next();
                    //return res.status(403).send({ auth: false, message: 'No token provided.' });
                }else{
                    externalApiToken.findOne({token: token}, function(err, token){
                        if(err || !token){
                            return res.status(401).json({message: 'no rights to visit this page'});
                        }else{
                            return next();
                        }
                    }) 
                }
            }
        }else{
            for(var i=0; i<role.length; i++){
                if (role[i] == req.user.role){
                    if(req.isAuthenticated()) {
                        return next();
                     }else{
                        res.status(401).json({message: 'no rights to visit this page'});
                     }
                }
            }
        }
    }
}