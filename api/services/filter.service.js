var path = require('path');

var mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
// Helper functions for parsing the provided filters from the queryString (parameter 'filters')
var str  = require(path.join(__dirname, 'string.service'))
var obj = require(path.join(__dirname, 'object.service'));

module.exports = function(filters) {
	var filterBy = {};

	if(typeof filters === 'string') {
		filters = JSON.parse(filters);
	}

	for(var i = 0; i < obj.objectLength(filters); i++) {
		var filterName = Object.keys(filters)[i];

		if(Object.keys(filters[filterName])[0] == '$in') {
			if(filterName == '_id') {
				var ids = new Array;
				for(var j = 0; j < filters[filterName]['$in'].length; j++) {
					ids.push(mongoose.Types.ObjectId(filters[filterName]['$in'][j]));
				}
				filterBy[filterName] = {$in: ids};
			} else {
				filterBy[filterName] = {$in: filters[filterName]['$in']};
			}
		}
		//Parse regex to a real RegExp object
		else if(Object.keys(filters[filterName])[0] == 'regex') {
			var regex = new RegExp(filters[filterName].regex.text, filters[filterName].regex.flags);
			filterBy[filterName] = {
				$regex: regex
			}
		}

		// Check if filter name starts with 'min_' and its value is numeric. If so, use Mongoose's $gt (greater than)
		else if(filterName.substr(0,4) == 'min_' && !isNaN(parseInt(filters[filterName]))) {
			filterBy[filterName.slice(4)] = { "$gt": parseInt(filters[filterName]) - 1 };

		// Check if filter name starts with 'max_' and its value is numeric. If so, use Mongoose's $lt (greater than)
		} else if(filterName.substr(0,4) == 'max_' && !isNaN(parseInt(filters[filterName]))) {

			filterBy[filterName.slice(4)] = { "$lt": parseInt(filters[filterName]) + 1 };

		// If the filter value is a boolean (strict = true to make sure 0 and 1 values are ignored), convert it to Mongo's 1 / -1 notation.
		} else if(str.toBoolean(filters[filterName], true) === true) {
			filterBy[filterName] = true;
		} else if(str.toBoolean(filters[filterName], true) === false) {
			filterBy[filterName] = false;
		} else if(typeof filters[filterName] === 'boolean') {
			filterBy[filterName] = filters[filterName];
		}		
		// If the filter is an array, it represents multiple possible/allowed values for a certain field in the model.
		else if(Array.isArray(filters[filterName]) && filters[filterName].length > 0) {
			filterBy[filterName] = { "$in": filters[filterName]};
		}
		
		//If the filter is an objectId, convert it to an ObjectId
		else if(validateObjectId(filters[filterName])){
			filterBy[filterName] = ObjectId(filters[filterName]);
		}

		// Default to only returning items with the provided value for this field.
		else if(!isNaN(parseInt(filters[filterName]))) {
			filterBy[filterName] = parseInt(filters[filterName]);
		}

		else {
			filterBy[filterName] = filters[filterName];
		}

	}
	// This will still be an empty Object if the provided Object has a length of 0. Otherwise, it will contain the filtering parameters for Mongoose.
	return filterBy;
}


function validateObjectId(id)
{
    var bool=false; 
    if(id.length==24) bool=/[a-f]+/.test(id);
    return bool;
}
