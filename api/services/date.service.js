module.exports.formatDate = function(dateString) {
	var result = "";
	var monthNames = [
		"januari", "februari", "maart",
		"april", "mei", "juni", "juli",
		"augustus", "september", "oktober",
		"november", "december"
	];

	if(dateString) {
		var date = new Date(dateString);
		result = '' + date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear() + ' om ' + date.getHours() + ":" + ('0' + date.getMinutes()).slice(-2) + " uur";
	}

	return result;
};