var geocoder = require('google-geocoder');
var geo = geocoder({key: settings.geoCoder.apiKey});

module.exports.code = function(address, countrycode, callback) {

	var formattedAddress, coordinates = Array;
	if(countrycode == 'nl') {
		formattedAddress = address.street + ' ' + address.houseNumber + ', ' + address.postalCode + ' ' + address.city + ', ' + address.country;
	} else {
		formattedAddress = address.houseNumber + ' ' + address.street + ', ' + address.postalCode + ' ' + address.city + ', ' + address.country;
	}
	geo.find(formattedAddress, function(err, res) {
		if(!err) {
			coordinates = [res[0].location.lat, res[0].location.lng];
		} else {
			console.log(err);
		}
		callback({'type': 'Point', 'coordinates': coordinates});
	});
}