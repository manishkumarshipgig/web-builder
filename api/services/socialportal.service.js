const path = require('path');
const mongoose = require('mongoose');
const SocialPortal = mongoose.model('Socialportal');
const create = require(path.join(__dirname, '..', 'services', 'create.service'));

module.exports.getSocialPortal = getSocialPortal;
module.exports.getUserSocialPortal = getUserSocialPortal;
module.exports.createSocialPortal = createSocialPortal;
module.exports.saveSocialPortal = saveSocialPortal;

function getSocialPortal(portalId, callback){
    SocialPortal.findOne({_id: req.params.portalId}).exec(function(err, socialPortal){
        if(err) {
            return callback({code: 400, msg: "Invalid portalId: " + err})
        } else if (!socialPortal) {
            return callback({code: 404, msg: "Portal not found"})
        } else {
            return callback(null, socialPortal)
        }
    })
}

function getUserSocialPortal(user, callback){
  
    //if(user.userId == undefined){
    if(user == undefined){
        return callback({code: 404, msg: "Portal not found"})
    }else{
        var userID = user;
    }
    
        
        SocialPortal.findOne({'users._id': {$in: [userID.toString()]}}).exec(function(err, socialPortal){
            if(err) {
                return callback({code: 400, msg: "Invalid UserId: " + err})
            } else if (!socialPortal) {
                return callback({code: 404, msg: "Portal not found"})
            
            } else {
                if(socialPortal.campaigns && socialPortal.campaigns.length > 0){
                    
                    //remove the old campaigns from the socialportal
                    var counter =0, originalLength = socialPortal.campaigns.length;
                    for(var i=0; i< originalLength; i++){
                        var today = new Date();
                        if(socialPortal.campaigns[i] && socialPortal.campaigns[i].endDate < today){
                            socialPortal.campaigns.splice(i, 1);
                        }
                        counter++;
                        if(counter == originalLength){
                            return callback(null, socialPortal);
                        }
                    }
                }else{
                    return callback(null, socialPortal)
                }
            }
        })
    }

function createSocialPortal(user, callback){
    var socialPortal = {};
    socialPortal.users = [];
    socialPortal.tasks = [];

    var user = {
        _id: user._id.toString(),
        name: user.firstName + ' ' + user.lastNamePrefix + ' ' + user.lastName,
        email: user.email
    }
    socialPortal.users.push(user);

    var task = {
        title: "Koppel uw Facebook pagina",
        date: Date.now(),
        status: "Open",
        completed: false
    }
    socialPortal.tasks.push(task);
    create(socialPortal, function(newPortal){
        if(newPortal){
            SocialPortal.create(newPortal, function(err, portal){
                if(err) {
                    return callback({code: 400, msg:'Portal could not be added. Error message: ' + err})
                } else if(!portal) {
                    return callback({code: 404, msg: 'Portal not found'})
                } else {
                    return callback(null, portal)
                }
            })
        }
    })
}

function saveSocialPortal(portalId, data, callback){
    console.log("saveSocialPortal");
    SocialPortal.findByIdAndUpdate(portalId, {$set: data}, {new: true}, function (err, portal) {
        if(err) {
            return callback({code: 400, msg: 'Error  updating portal. Error message: ' + err})
        } else if(!portal) {
            return callback({code: 404, msg: 'Portal not found'})
        } else {
            return callback(null, portal)     
        }
    })
}