const mongoose = require('mongoose');
const path = require('path');
const async = require('async');
const DateDiff = require('date-diff');
const _ = require('lodash');
const moment = require('moment');
const PhoneNumber = require('awesome-phonenumber');
const chalk = require('chalk');
const request = require('request');
const uuidv3 = require('uuid/v3');

const Shop = mongoose.model('Shop');
const ObjectId = require('mongodb').ObjectId;

const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));

module.exports.getToken = getToken;
module.exports.refreshToken = refreshToken;
module.exports.getAccessToken = getAccessToken;
module.exports.updateCategory = updateCategory;
module.exports.getSpecificStoreItem = getSpecificStoreItem;
module.exports.deleteTax = deleteTax;
module.exports.findProduct = findProduct;
module.exports.updateProduct = updateProduct;
module.exports.callApi = callApi;
module.exports.getRandomColor = getRandomColor;
module.exports.getUncategorizedCategory = getUncategorizedCategory;
module.exports.registerDeviceAndSaveMerchant = registerDeviceAndSaveMerchant;
module.exports.accessToken = accessToken;

const header = {
    "Content-Type" : "application/x-www-form-urlencoded"
}

var requestOptions = function(method, url, access_token, form){
    var requestObject = {
        url: settings.countr.apiUrl + url,
        method: method.toUpperCase(),
        header: header,
        auth: access_token ? {
            bearer : access_token
        } : null,
        form: form && method != 'GET' ? form : null,
        qs: method == 'GET' && form ? form: null
    }
    return requestObject;
};

/**
 * Callback for returning the Countr API result
 * 
 * @callback countrCallback
 * @param {err} err - if exists: the error from the request
 * @param {body} body - body from the request
 */


function getRandomColor(callback) {

    var colors = [
        'greyblue',
        'lightblue',
        'blue',
        'lightgreen',
        'darkgreen',
        'purple',
        'pink',
        'red',
        'yellow',
        'brown'
    ];

    var random = Math.floor(Math.random() * colors.length);
    var color = colors[random];
    
    return callback(color);
}


/**
 * Get an accesstoken
 * @param {string} username - The username from countr
 * @param {string} password - The password from countr
 * @param {countrCallback} callback - The callback with error, response and body
 */
function getAccessToken(username, password, callback){
    var data = {
        "username": username,
        "password": password,
        "client_id" : settings.countr.clientId,
        "client_secret" :  settings.countr.clientSecret,
        "grant_type" : "password",
    }

    var options = {
        url: settings.countr.apiUrl + 'oauth/token',
        method: 'POST',
        form: data,
        header: header
    };

    request(options, function(err, response, body){
        console.log("getAccessToken2");
        console.log(err, response, body);
        if(err){
            return callback(err);
        }

        return callback(null, response, JSON.parse(body));
    })
}
 
/**
 * 
 * @param {object} url - Object with optional method (default GET) and url properties
 * @param {object} data - Optional object with data to be processed
 * @param {string} access_token - The countr access_token to use the Api
 * @param {countrCallback} callback - Callback with error and body
 */
function callApi(url, data, access_token, callback){
    data = data ? data : null;
    var method =  url.method ? url.method.toUpperCase() : "GET";
    if(settings.countr.debug){
        console.log("Sending an", chalk.blue(method), "to", chalk.blue(url.url), "With data:", data);
    }
    request(
        requestOptions(
            method,
            url.url,
            access_token,
            data
        ), function(err, response, body){
            if(err || (response && 
                (response.statusCode < 200 || 
                (response.statusCode >= 300 && response.statusCode != 404)
                )
            )){
                if(settings.countr.debug){
                    console.error("There was an error during a request to Countr");
                    console.error(err, response, body);
                }
                
                if(response && response.statusCode != 200){
                    return callback(JSON.parse(response.body));
                }
                return callback(JSON.parse(body));
            }
            body = JSON.parse(body);
            if(settings.countr.debug){
                console.log("Result from Countr", body);
            }
            if(body.error){
                return callback(body);
            }
            
            return callback(null, body);
        }
    )
}


/**
 * Refresh the access token
 * @param {string} refresh_token - The refresh token for this user
 * @param {countrCallback} callback - The callback with error, response and body
 */
function refreshToken(refresh_token, callback){
    var data = {
        "grant_type" : "refresh_token",
        "client_id" : settings.countr.clientId,
        "client_secret" : settings.countr.clientSecret,
        "refresh_token": refresh_token
    };

    var options = {
        url: settings.countr.apiUrl + 'oauth/refresh',
        method: "POST",
        form: data,
        header: header
    };
    request(options, function(err, response, body){
        if(err){
            console.error("Error while refreshing accessToken", err)
            return callback(err);

        }

        return callback(null, response, JSON.parse(body));
    })
}

function getUncategorizedCategory(language, acces_token, callback){
    var categoryName = "Uncategorized";
    language = language.toLowerCase();
    if(language == 'nl'){
        categoryName = "Geen categorie";
    }else if(language == 'de'){
        categoryName = 'Unkategorisiert';
    }else if(language == 'fr'){
        categoryName = 'Non catégorisé';
    }else if(language == 'sp'){
        categoryName = 'Sin categoría';
    }
    callApi({
        url: 'categories/search'
    }, {
        limit: 1,
        sort: 'created_at',
        text: categoryName
    }, acces_token, function(err, result){
        if(err){
            return callback(err);
        }
        if(!result || result.length == 0 || result[0].name != categoryName){
            //Create category
            getRandomColor(function(color){
                callApi({
                    url: 'categories',
                    method: 'POST'
                }, {
                    name: categoryName,
                    visible: true,
                    color: color
                }, acces_token, function(err, result){
                    if(err){
                        return callback(err);
                    }
                    return callback(null, result);
                })
            })
        }else{
            return callback(null, result[0])
        }
    })
}

/**
 * Wrapper function to get an accessToken and, if needed, to refresh it
 * @param {string} username - The countr username
 * @param {string} password - The counter password
 * @param {countrCallback} callback - The callback with error, response and body
 */
function getToken(username, password, callback){
    getAccessToken(username, password, function(err, response, body){
        if(err){
            console.error(err);
            //TODO: if error == invalid_token then refresh the token and return the new token
            return callback(err);
        }    
        return callback(null, response, body);
    })
}

function registerDeviceAndSaveMerchant(shopId, callback){
    Shop.findOne({_id: ObjectId(shopId)}).exec(function(err, shop){
        accessToken(shopId, null, null, function(err, tokens){
            callApi({
                url: 'me'
            }, null, tokens.accessToken, function(err, me){
                if(err) return callback(err);

                callApi({
                    url: 'devices',
                    method: "POST"
                }, {
                    merchant: me._id,
                    store: shop.countr.countrId,
                    name: "PrismaNote",
                    uuid: uuidv3(shop.name + '-' + shop.countr.countrId, settings.countr.uuid),
                }, tokens.accessToken, function(err, deviceResult){
                    if(err) return callback(err);
                    shopService.updateDeviceOrMerchantIdOnShop(shopId, deviceResult._id, me._id, function(err, result){
                        if(err) return callback(err);
                        return callback(null, deviceResult);
                    })
                })
            })
        })
    })   
}

function accessToken(shopId, username, password, callback){
    Shop.findOne({_id: ObjectId(shopId)}).exec(function(err, shop){
        if(err){
            return callback({code: 500, message: err});
        }
        if(!shop){
            return callback({code: 404, message: "Shop not found"});
        }
       
        var refreshExpiryDiff = new DateDiff(new Date(shop.countr.refreshTokenExpiry), new Date);
        console.log("Refresh token is expiring in", chalk.blue(refreshExpiryDiff.days()), "days");
        if(shop.countr && shop.countr.accessToken && (refreshExpiryDiff && refreshExpiryDiff.days() >= 5) && (!username && !password)){
            //The shop does have already some details from Countr, let's check the accessToken expiry date
            var diff = new DateDiff(new Date(shop.countr.accessTokenExpiry), new Date());
            
            if(typeof shop.countr.accessTokenExpiry == 'undefined' || diff.hours() <= 2){
                refreshAccessToken(shop.countr.refreshToken, function(err, result){
                    console.log(chalk.blueBright("refreshAccessToken"), err, result);
                    if(err && err.error == 'invalid_grant'){
                        if(username && password){
                            return accessToken(shopId, username, password, callback)
                        }
                        
                        return callback({code: 500, error:err.error, message: err.message})
                    }

                    var accessTokenExpiry = new Date(result.access_token_expiry);
                    var refreshTokenExpiry = new Date(moment(accessTokenExpiry).add(1, 'month'));

                    
                    shop.countr.accessToken = result.access_token,
                    shop.countr.accessTokenExpiry =  result.access_token_expiry,                                                  
                    shop.countr.refreshToken = result.refresh_token,
                    shop.countr.refreshTokenExpiry = refreshTokenExpiry

                    Shop.findOneAndUpdate({_id: shopId}, shop, {new: true}).exec(function(err, shop){
                        if(err){
                            return callback({code: 500, message: err});
                        }
                        return callback(null, shop.countr);
                    })
                })
            }else{
                return callback(null, shop.countr);
            }
        }else{
            //There are no details know from Countr, let's grab some!
            getAccessToken(username, password, function(err, result){
                if(err){
                    return callback({code: 500, message: err});
                }
                if(result.error){
                    return callback({code: 500, error:result.error, message: result.message});
                }

                var accessTokenExpiry = new Date(result.access_token_expiry);
                var refreshTokenExpiry = new Date(moment(accessTokenExpiry).add(1, 'month'));

                shop.countr = {
                    username: username,
                    accessToken: result.access_token,
                    accessTokenExpiry: accessTokenExpiry,
                    refreshToken: result.refresh_token,
                    refreshTokenExpiry: refreshTokenExpiry
                }
                Shop.findOneAndUpdate({_id: shopId}, shop, {new: true}).exec(function(err, shop){

                    if(err){
                        return callback({code: 500, message: err});
                    }
                    return callback(null, shop.countr);
                })
            })
        }

        function getAccessToken(username, password, callback){
            countr.getAccessToken(username, password, function(err, respone, body){
                if(err){
                    return callback(err);
                }
                if(body.err){
                    return callback({error: body.error, message: body.message});
                }
                return callback(null, body);
            })
        }

        function refreshAccessToken(refresh_token, callback){
            countr.refreshToken(refresh_token, function(err, response, body){
                if(err){
                    return callback(err);
                }
                if(body.error){
                    return callback({error: body.error, message: body.message});
                }
                return callback(null, body);
            })
        }
    })
}

/**
 * OBSOLETE: Update a specific category
 * @param {string} id - The Countr ID of the category
 * @param {object} data - The object with the data which should be updated
 * @param {countrCallback} callback - The callback with error, response and body
 */
function updateCategory(id, data, access_token, callback){
    console.error("The function updateCategory is absolete and will be removed in the future");
    request(requestOptions('PATCH', 'categories/' + id, access_token, data), function(err, response, body){
        if(err){
            return callback(err);
        }
        return callback(null, response, JSON.parse(body));
    })
}


/**
 * OBSOLETE: Get specific store categories
 * @param {string} storeId - The store id from Countr
 * @param {string} item - The store item you want from Countr
 * @param {string} access_token - The access token from Countr
 * @param {countrCallback} callback - The callback with error, response and body
 */
function getSpecificStoreItem(storeId, item, access_token, callback){
    console.error("The function getSpecificStoreItem is absolete and will be removed in the future");
    request(requestOptions('GET', 'stores/' + storeId + '/' + item, access_token, null), function(err, response, body){
        if(err){
            return callback(err);
        }
        return callback(null, response, JSON.parse(body));
    })
}


/**
 * OBSOLETE: Delete a tax with the given id
 * @param {string} id - The ID of the tax
 * @param {string} access_token - The access token from Countr
 * @param {countrCallback} callback - The callback with error, response and body
 */
function deleteTax(id, access_token, callback){
    console.error("The function deleteTax is absolete and will be removed in the future");
    request(requestOptions('DELETE', 'taxes/' + id, access_token, null), function(err, response, body){
        if(err){
            return callback(err);
        }
        return callback(null, response, JSON.parse(body));
    })
}

/**
 * OBSOLETE: Find a specific product with a given id
 * @param {string} id - The ID of the product
 * @param {string} access_token - The access token from Contr
 * @param {countrCallback} callback - The callback with error, response and body
 */
function findProduct(id, access_token, callback){
    console.error("The function findProduct is absolete and will be removed in the future");
    request(requestOptions('GET', 'products/'+ id, access_token, null), function(err, response, body){
        if(err){
            return callback(err);
        }
        return callback(null, response, JSON.parse(body));
    })
}

/**
 * OBSOLETE: Update a specific product with a given id
 * @param {string} id - The ID of the product
 * @param {object} data - The product object following the API
 * @param {string} acces_token - The access token from Countr
 * @param {countrCallback} callback - The callback with error, response and body
 */
function updateProduct(id, data, acces_token, callback){
    console.error("The function updateProduct is absolete and will be removed in the future");
    request(requestOptions('PATCH', 'products/'+id, acces_token, data), function(err, response, body){
        if(err){
            return callback(err);
        }
        return callback(null, response, JSON.parse(body));
    })
}