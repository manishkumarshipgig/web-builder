exports.nl2br = function(str, is_xhtml) {
	var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
	return (str + '').trim().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

exports.toBoolean = function(str, strict) {
	// Check for optional parameter strict (allow only "true" or "false") and set default if necessary.
	if(typeof strict === 'undefined' || (strict !== false && strict !== true)) {
		strict = false;
	}
	
	// check if the value of the provided string represents a Boolean. If so, convert to an actual Boolean. Otherwise, return null;
	if(str === 'true') {
		return true;
	} else if(str === 'false') {
		return false;
	} else if(strict == false && str === '1') {
		return true;
	} else if(strict == false && str === '0') {
		return false;
	} else {
		return null;
	}
}

module.exports.truncateText = function(str, length) {
	if(str.indexOf(' ', length) == -1) {
		return str;
	} else {
		return str.substring(0, str.indexOf(' ', length));
	}
};

module.exports.generateNameSlug = function(str) {
	return str.trim().toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
}

module.exports.generateTranslationTag  = function(str) {
	return str.trim().toUpperCase().replace(/ /g, '_').replace(/[^\w-]+/g, '_');
}

module.exports.slugify = function(text) {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

module.exports.capitalizeFirstLetter = function(value){
    return value.charAt(0).toUpperCase() + value.slice(1);
}