const path = require('path');
const geo = require(path.join(__dirname, 'geo.service'));

module.exports = function(query, callback) {
	if(typeof query === 'string') {
		query = JSON.parse(query);
	}
	// When creating a new object in de database, a nameSlug should be generated as well (contrary to the update() service). At least for all objects that have a name property.
	// if(query.name && typeof query.name === 'string' && query.name.length > 0) {
	// 	query.nameSlug = str.generateNameSlug(query.name);
	// }

	if(query.address) {
		var address;
		if(query.address.length > 1){
			address = query.address[0]
		}else{
			address = query.address;
		}
		geo.code(address, 'nl', function(geoloc) {
			query.geoloc = geoloc;
		});
	}
	callback(query);
}