const path = require('path');
const async = require('async');
const request = require('request');

const mongoose = require('mongoose');
const Setting = mongoose.model('Setting');


module.exports.getSetting = getSetting;
module.exports.updateSetting = updateSetting;
module.exports.createSetting = createSetting;
module.exports.removeSetting = removeSetting;


function getSetting(name, callback){
    Setting.findOne({name: name}).exec(function(err, setting){
        if(err){
            return callback(err);
        }
        if(!setting){
            return callback("Setting not found");
        }
        return callback(null, setting);
    })
}

function updateSetting(name, value, callback){
    Setting.findOne({name: name}).exec(function(err, setting){
        if(err){
            return callback(err);
        }
        if(!setting){
            return callback("Setting not found");
        }
        setting.value = value;
        setting.date = new Date();

        setting.save(function(err, result){
            if(err){
                return callback(err);
            }
            return callback(null, result);
        })
    })
}

function createSetting(name, value, callback){
    var setting = {
        name: name,
        value: value,
    };
    Setting.create(setting, function(err, result){
        if(err){
            return callback(err);
        }
        return callback(null, result);
    })
}

function removeSetting(name, callback){
    Setting.remove({name: name}).exec(function(err, result){
        if(err){
            return callback(err);
        }
        return callback(null, result);
    })
}