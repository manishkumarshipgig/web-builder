const Mollie = require('mollie-api-node');

const mollie = new Mollie.API.Client;

module.exports.createCustomer = createCustomer;
module.exports.createPayment = createPayment;
module.exports.getMandates = getMandates;
module.exports.createSubscription = createSubscription;
module.exports.checkSubscriptions = checkSubscriptions;
module.exports.cancelSubscription = cancelSubscription;
module.exports.getPayment = getPayment;
module.exports.getSubscription = getSubscription;

mollie.setApiKey(settings.mollie.apiKey);


function createCustomer(name, email, callback){
    mollie.customers.create({
        name: name,
        email: email
    }, function(customer){
        if(customer){
            return callback(null, customer);
        }
        
    })
}

function createPayment(payment, callback){
    mollie.payments.create(payment, function(result){
        return callback(null, result);
    })
}

function getMandates(customerId, callback){
    mollie.customers_mandates.withParentId(customerId).all(function(mandates){
        return callback(mandates);
    })
}
//mollie.customers_payments.withParent(customer).create({ ??
function createSubscription(customerId, subscription, callback){
    mollie.customers_subscriptions.withParentId(customerId).create(subscription, function(result){
        return callback(result);
    })
}

function checkSubscriptions(customerId, callback){
    mollie.customers_subscriptions.withParentId(customerId).all(function(subscriptions){
        return callback(subscriptions);
    })
}

function cancelSubscription(customerId, subscriptionId, callback){
    mollie.customers_subscriptions.withParentId(customerId).delete(subscriptionId, function(result){
        console.log("CANCEL", result);
        return callback(result);
    })
}

function getPayment(paymentId, callback){
    mollie.payments.get(paymentId, function(result){
        return callback(result);
    })
}

function getSubscription(subscriptionId, customerId, callback){
    mollie.customers_subscriptions.withParentId(customerId).get(subscriptionId, function(subscription){
        return callback(subscription);
    })
}