var path = require('path');

var obj = require(path.join(__dirname, 'object.service'));

module.exports = function(q) {
	var sortBy = {};

	if(typeof q === 'string') {
		q = JSON.parse(q);
	}

	for(var i = 0; i < obj.objectLength(q); i++) {
		if(q[Object.keys(q)[i]] == 'asc') {
			sortBy[Object.keys(q)[i]] = 1;
		} else if(q[Object.keys(q)[i]] == 'desc') {
			sortBy[Object.keys(q)[i]] = -1;
		}
	}
	// This will still be an empty Object if the provided Object has a length of 0. Otherwise, it will contain the sorting parameters for Mongoose.
	return sortBy;
}