// seen on https://github.com/robert52/express-starter/blob/master/app/helpers/password.js
var LEN = 256;
var SALT_LEN = 64;
var ITERATIONS = 10000;
var DIGEST = 'sha256';

var crypto = require('crypto');

/**
 * Creates a hash based on a salt from a given password
 * if there is no salt a new salt will be generated
 * 
 * @param {String} password
 * @param {String} salt - optional
 * @param {function} callback
 */

module.exports.hash = hashPassword;
module.exports.verify = verify;

function hashPassword(password, salt, callback){
    var len = LEN /2;
    if (3 === arguments.length) {
        crypto.pbkdf2(password, salt, ITERATIONS, len, DIGEST, function(err, derivedKey) {
            if (err) {
                return callback(err);
            }

            return callback(null, derivedKey.toString('hex'));
        })
    } else {
        callback = salt;
        crypto.randomBytes(SALT_LEN / 2, function(err, salt) {
            if (err) {
                return callback(err);    
            }

            salt = salt.toString('hex');

            crypto.pbkdf2(password, salt, ITERATIONS, len, DIGEST, function(err, derivedKey) {
                if (err) { 
                    return callback(err);
                }
                return callback(null, derivedKey.toString('hex'), salt);
            })
        })
    }
}

/**
 * Verifies if a password match a hash by hasing the password
 * with a given salt
 * 
 * @param {String} password Plain text password
 * @param {String} hash Hashed Password of the user
 * @param {String} salt Salt of the hashed password
 * @param {!Function(?Error, !boolean)} callback
 */
function verify(password, hash, salt, callback) {

    hashPassword(password, salt, function(err, hashedPassword) {
        if (err) {
            return callback(err);
        }
        
        if(hashedPassword == hash) {
            callback(null, true);
        }else{
            callback(null, false);
        }
    })
}