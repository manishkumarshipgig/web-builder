const path = require('path');
const pdf = require('pdfkit');
const base64 = require('base64-stream');
const async = require('async');
const request = require('request');

const mongoose = require('mongoose');
const Order = mongoose.model('Order');
const FbOrder = mongoose.model('fbOrder');
const User = mongoose.model('User');
const Shop = mongoose.model('Shop');
const Payment = mongoose.model('Payment');
const UpgradeOrder = mongoose.model('UpgradeOrder');
const ObjectId = require('mongodb').ObjectId;

const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));
const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));
const upgradeShopService = require(path.join(__dirname, '..', 'services', 'upgradeShop.service'));

module.exports.completeOrder = completeOrder;
module.exports.createInvoice = createInvoice;
module.exports.generateAndSavePayId = generateAndSavePayId;
module.exports.updateOrderStatus = updateOrderStatus;
module.exports.updateOrderStatusById = updateOrderStatusById;
module.exports.getOrderStatus = getOrderStatus;
module.exports.getPayment = getPayment;
module.exports.createPayment = createPayment;
module.exports.getOrderTotal = getOrderTotal;
module.exports.updateOrderPayment = updateOrderPayment;
module.exports.getPaymentStatus = getPaymentStatus;
module.exports.handlePayment = handlePayment;
module.exports.createFbInvoice = createFbInvoice;
module.exports.createUpgradeInvoice = createUpgradeInvoice;

function completeOrder(payId, facebookOrder, resultCallback){
	var status, response, counter=0;
	var query;

	if(facebookOrder){
		query = FbOrder.find({payId: payId});
	}else{
		query = Order.find({payId: payId});
	}


    query.exec(function(err, orders){
		async.eachSeries(orders, function(order, callback){

			if(!order.handled || typeof order.handled == 'undefined'){
				createInvoice(order._id, function(err, result){
					if(err){
						console.error("ERR", err);
						status = 400;
						response = {message: 'error: ' + err};
					}else{
						//Factuur is gemaakt, dan kunnen we de voorraad gaan aanpassen en notificaties gaan sturen
						
						getShop(order, function(err, shop, logo){
							if((order && order.items) && (shop && shop.products)){
								async.eachSeries(order.items, function(item, itemDone){
									async.eachSeries(shop.products, function(product, productDone){
										if(product._id == item._id){
											product.stock = product.stock - item.quantity;
											shop.save();
											productDone();
										}
									})
									itemDone();
									//mail sturen naar winkelier
								})
							}

							//Send a mail to the customer or retailer
							var data = {
								shop: shop,
								order: order
							}
								
							var options = {
								to: shop.email,
								subject: 'Nieuwe bestelling voor ' + shop.name,
								template: 'new-order-for-shop'
							}

							mailService.mail(options, data, null, function(err, shopMailResult){
								//mail verzonden, nu factuur mailen naar koper
								
							})
							User.findOne({_id: order.user[0]._id}).exec(function(err, user){
								var data ={
									user: user,
									shop: shop,
									order: order
								}
								var attachment = [{
									filename: "Factuur " + order.number + ".pdf",
									content: result
								}];
								var options = {
									to: user.email,
									subject: 'Factuur ' + order.number + ' van uw bestelling bij ' + shop.name,
									template: 'invoice'
								}
								
								mailService.mail(options, data, attachment, function(err, invoiceMailResult){
									//result
								})
							})
							
						})//end getshop

						status = 200;
						response = {message: 'All done'};
					}
					callback();
				})//end createInvoice

				order.handled = true;
				order.save();
			}else{
				status = 200;
				response = {message: "Order already handled"};
				callback();
			}
		}, function(){
            resultCallback(status, response);
		})
	})//end order.find
}
function getLogoBase64(logo, result){
	request({url: logo, encoding: null}, (error, response, body) => {
		if(!error && response.statusCode == 200){
			var img = new Buffer(body, 'base64');
			return result(null, img);
		}
	})
}

function getShop(order, callback){
	if(typeof order.shopId != "undefined" && order.shopId){
		Shop.findOne({_id: order.shopId}).exec(function(err, shop){
			if(err){
				return callback(err);
			}
			if(shop){
				var logo = "https://prismanote.com/images/prismanote_logo_large.png";

				getLogoBase64(logo, function(err, img){
					if(err){
						return callback(err);
					}
					return callback(null, shop, img);
				})
			}
		})
	}
	else if(typeof order.shop[0].noShop != "undefined" && order.shop[0].noShop){
		var logo = "https://prismanote.com/images/prismanote_logo_large.png";

		getLogoBase64(logo, function(err, img){
			if(err){
				return callback(err);
			}
			return callback(null, order.shop[0], img);
		})
	}else{
		Shop.findOne({nameSlug: order.shop[0].nameSlug}).exec(function(err, shop){
			if(shop){
				shopService.getShopLogo(shop.nameSlug, 'Dark', null, function(err, logo){
					if(logo){
						return callback(null, shop, logo);
					}
				})
			}
		})
	}
}

function createFbInvoice(orderId, callback){
	FbOrder.findOne({_id: orderId}).exec(function(err, order){
		if(err){
			console.log("something went wrong", err);
			return callback("Something went wrong: " + err);
		}else if(!order){
			console.log("no order found");
			return callback("no order found");
		}else{
			getShop(order, function(err, shop, logo){
				if(shop){				
					doc = new pdf({autoFirstPage: false});
					var finalString = '';
					var stream = doc.pipe(base64.encode());

					doc.addPage({
						margin: 50
					})

					doc.image(logo, {height: 50});

					doc.fontSize(20);
					doc.text("Factuur", 400, 40, {align: 'right'});
					doc.fontSize(10);
					doc.moveDown(0.5);
					doc.text("Factuurnummer: " + order.number, {align: 'right'});
					doc.moveDown(0.3);
					var orderDate = new Date(order.date);
					doc.text("Datum: " + orderDate.getDate() + "-" + orderDate.getMonth() + "-" + orderDate.getFullYear(), {align: 'right'});

					doc.moveDown(2);
					doc.fontSize(10);

					doc.text(shop.name, 50);
					doc.moveDown(0.5);
					doc.text(shop.address.street + " " + shop.address.houseNumber + (shop.address.houseNumberSuffix ? shop.address.houseNumber : ""));
					doc.moveDown(0.3);
					doc.text(shop.address.postalCode)
					doc.moveDown(0.3);
					doc.text(shop.address.city);
					doc.moveDown(0.3);
					doc.text(shop.address.country);

					doc.moveUp(6.4);
					doc.text("PrismaNote", 300);
					doc.moveDown(0.3);
					doc.text("Middelweg 8d");
					doc.moveDown(0.3);
					doc.text("4147AV");
					doc.moveDown(0.3);
					doc.text("Asperen");
					doc.moveDown(0.3);
					doc.text("Nederland");
					doc.moveDown(3);

					doc.text("Facebook campagne: " + order.campaign.name, 50);
					doc.moveDown(0.3);
					doc.text("Totaalprijs:", 50);
					doc.moveUp();
					doc.text("€ " + order.amount, 100);

					doc.moveDown(0.3);
					var tax = Math.round((order.amount * 0.21) * 100) / 100;
					doc.text("Waarvan 21% BTW: € " + tax , 50);

					doc.moveDown(3);
					doc.text("Deze factuur is reeds voldaan.", 50);

					doc.end();
					stream.on('data', function(chunk){
						var a =0;
						finalString += chunk;
					})
					stream.on('end', function(){
						return callback(null, finalString);
					})
				}else{
					return callback("Something went wrong with the shop associated to this order");
				}
			})
		}

	})
}

function createInvoice(orderId, callback){
	console.log("createInvoice");
	//Gegevens opzoeken en factuur terugsturen in de callback als Base64
	Order.findOne({_id: orderId}).exec(function(err, order){
		
		if(err){
			return callback("Something went wrong:" + err);
		}else if(!order){
			return callback("No order found with given id");
		}else{	
			getShop(order, function(err, shop, logo){
					console.log("LOGO", typeof logo);
				if(shop){				
					doc = new pdf({autoFirstPage: false});
					var finalString = '';
					var stream = doc.pipe(base64.encode());

					doc.addPage({
						margin: 50
					})

					doc.image(logo, 40, 30, {fit: [250,100] });

					doc.fontSize(20);
					doc.text("Factuur", 400, 40, {align: 'right'});
					doc.fontSize(10);
					doc.moveDown(0.5);
					doc.text("Factuurnummer: " + order.number, {align: 'right'});
					doc.moveDown(0.3);
					var orderDate = new Date(order.date);
					doc.text("Datum: " + orderDate.getDate() + "-" + orderDate.getMonth() + "-" + orderDate.getFullYear(), {align: 'right'});

					doc.moveDown(2);
					doc.fontSize(10);

					doc.text(order.sendDetails[0].firstName + " " + ( order.sendDetails[0].lastNamePrefix ? order.sendDetails[0].lastNamePrefix : "") + " " + order.sendDetails[0].lastName, 50);
					doc.moveDown(0.5);
					doc.text(order.sendDetails[0].address[0].street + " " + order.sendDetails[0].address[0].houseNumber + order.sendDetails[0].address[0].houseNumberSuffix);
					doc.moveDown(0.3);
					doc.text(order.sendDetails[0].address[0].postalCode)
					doc.moveDown(0.3);
					doc.text(order.sendDetails[0].address[0].city);
					doc.moveDown(0.3);
					doc.text(order.sendDetails[0].address[0].country);

					doc.moveUp(7.4);
					doc.text("Afzender", 300);
					doc.moveDown(0.5);
					doc.text(shop.name);
					doc.moveDown(0.3);
					doc.text(shop.address.street + " "+ shop.address.houseNumber + shop.address.houseNumberSuffix);
					doc.moveDown(0.3);
					doc.text(shop.address.postalCode);
					doc.moveDown(0.3);
					doc.text(shop.address.city);
					doc.moveDown(0.3);
					doc.text(shop.address.country);
					doc.moveDown(3);

					doc.text("Aantal", 50);
					doc.moveUp();
					doc.text("Artikel", 90);
					doc.moveUp();
					doc.text("Stuksprijs",420);
					doc.moveUp();
					doc.text("Totaal",520);

					var total = 0;
					for(i = 0; i < order.items.length; i++){
						var item = order.items[i];
						total = total + item.price * item.quantity;
						doc.moveDown();
						doc.text(item.quantity, 50);
						doc.moveUp();
						doc.text(item.name, 90);
						doc.moveUp();
						doc.text("€" + Math.round(item.price* 100) / 100, 420);
						doc.moveUp();
						doc.text("€" + Math.round(item.price * item.quantity* 100) / 100, 520);
					}
					doc.moveDown(2);
					doc.text("Subtotaal", 420);
					doc.moveUp();
					doc.text("€" + Math.round(total * 100) / 100, 520);
					doc.moveDown(1);
					doc.text("Inclusief BTW (21%)", 420);
					doc.moveUp();
					doc.text("€" + Math.round((total * 0.21) * 100) / 100, 520);


					if(order.comment){
						doc.moveDown(3);
						doc.fontSize(18);
						doc.text("Opmerkingen", 50);
						doc.fontSize(10);
						doc.text(order.comment, 50);
					}

					doc.end();
					stream.on('data', function(chunk){
						var a =0;
						finalString += chunk;
					})
					stream.on('end', function(){
						return callback(null, finalString);
					})
				}else{
					return callback("Something went wrong with the shop associated to this order");
				}
			})
		}
	})
}


function generateAndSavePayId(orderNumber, callback) {
	var payId = new ObjectId();

	Order.findOne({number: orderNumber}).exec(function(err, order){
		if(order){
			order.payId = payId;
			order.save(function(err, result){
				if(err){
					return callback("Something went wrong while updating the order" + err);
				}
				callback(null, payId);
			})
		}else{
			return callback("No order found");
		}
	})
}

function updateOrderStatus(orderNumber, status, callback){	
	console.log("updateOrderStatus");
	if(typeof(status) != 'object') {
		return callback("no valid status provided");
	}

	Order.findOne({number: orderNumber}).exec(function(err, order){
		order.status.push(status);
		order.save(function(err, result){
			if(err){
				return callback(err);
			}else{
				return callback(null, result);
			}
		});
	})
}

function updateOrderStatusById(orderId, status){
	Order.findOne({_id: orderId}).exec(function(err, order){
		order.status.push(status);
		order.save();
	})
}

function getOrderStatus(order, returnType){
	if(returnType && returnType == 'object'){
		return order.status[order.status.length-1];
	}else{
		return order.status[order.status.length-1].status;
	}
}
function getPaymentStatus(payment, returnType){
	if(returnType && returnType == 'object'){
		return payment.status[payment.status.length-1];
	}else{
		return payment.status[payment.status.length-1].status;
	}
}

function getPayment(orderId, callback){
	Payment.findOne({orderId: orderId}).exec(function(err, payment){
		if(err){
			return callback(err);
		}
		return callback(null, payment);
	})
}

function createPayment(order, callback){
	console.log("createPayment");
	var orderAmount =0;
	var payment = {};

	function getOrderAmountAndDate(order, callback){
		if(order.items && order.items.length > 0){
			getOrderTotal(order.items, function(amount){
				orderAmount = amount;
				return callback(orderAmount, order.date, true);
			})
		}else if(order.amount){
			orderAmount = order.amount;
			return callback(orderAmount, order.date, false);
		}else{
			upgradeShopService.getUpgradeOrderTotal(order._id, function(err, total){
				orderAmount = total;
				upgradeShopService.getUpgradeOrderStatus(order._id, function(err, status){
					return callback(orderAmount, status.date, false);
				})
				
			})
		}
	}

	getShop(order, function(err, shop, logo){
		getOrderAmountAndDate(order, function(amount, date, webOrder){
			console.log("orderamoun result", amount, date, webOrder);
			payment.orderId = order._id;
			payment.amount = amount;
			payment.contribution = 0; //platform contribution
			payment.shippingCosts = 0;
			payment.data = date;
			
			//20 days to payout
			if(webOrder){
				//payout is only needed when it is an weborder
				var payoutDate = new Date(order.date);
				payoutDate.setDate(payoutDate.getDate() + 20);

				payment.payoutDate = payoutDate;
			}
			payment.shopId = shop._id ? shop._id : null;
			payment.status = {
				status: "Betaling aangemaakt",
				date: Date.now()
			},
			payment.returned = false;
			payment.completed = false;
			payment.exported = false;
			console.log("PAYMENT", payment);
			Payment.create(payment, function(err, result){
				if(err){
					console.log("error while saving payment", err);
					return callback(err);
				}
				return callback(null, result);
				
			})
		})
	})
}

function getOrderTotal(items, callback){
    var orderAmount = 0;

    async.forEach(items, function(product, orderDone){
        orderAmount = orderAmount + product.quantity * product.price;
        orderDone()
    }, function(){
        return callback(orderAmount);
    })
}

function updateOrderPayment(orderId, paymentStatus, callback){
	if(!paymentStatus){
		return callback("No payment details provided");
	}

	if(!paymentStatus.comment){
		paymentStatus.comment = null;
	}
	
	getPayment(orderId, function(err, payment){
		if(err)
			return callback(err);
		if(!payment){
			return callback("No payment found");
		}
		handlePayment(payment, paymentStatus, function(result){
			result.save(function(err, result){
				if(err){
					return callback(err);
				}
				console.log("payment saved");
				return callback(null, result);
			})
		})
	})
}

function handlePayment(payment, paymentStatus, callback){
	if (typeof payment == 'undefined' || !payment){
		payment = {};
	}
	if(paymentStatus.status){
		var status = {
			status: paymentStatus.status,
			comment: paymentStatus.comment,
			date: Date.now()
		}
		payment.status.push(status);
	}

	if(paymentStatus.generateInvoiceNumber){
		console.log("generateInvoiceNumber");
		var invoiceNumber =0;

		Payment.find({invoiceNumber: {$ne:null}}).sort({date: -1}).limit(1).exec(function(err, latestPaymentsWithNumber){

			if(latestPaymentsWithNumber && latestPaymentsWithNumber[0].invoiceNumber){
				invoiceNumber = latestPaymentsWithNumber[0].invoiceNumber.split("-")[1];
			}

			var date = new Date();
			payment.invoiceNumber = date.getFullYear().toString() + date.getMonth().toString() + "-" + (parseInt(invoiceNumber)+1);
			return callback(payment);
		})
	}

	if(paymentStatus.status == 'returned' || paymentStatus.status == 'cancelled'){
		payment.cancelled = true;
		payment.amount = 0;
		payment.payoutDate = null;
		return callback(payment);
	}

	if(paymentStatus.status == 'completed'){
		payment.completed = true;
		payment.exported = true;
		payment.payoutDate = null;	
		return callback(payment);
	}
}

function createUpgradeInvoice(orderId, callback){
	UpgradeOrder.findOne({_id: orderId}).exec(function(err, order){
		if(err){
			console.log("something went wrong", err);
			return callback("Something went wrong: " + err);
		}else if(!order){
			console.log("no order found");
			return callback("no order found");
		}else{
			getShop(order, function(err, shop, logo){ 
				

				upgradeShopService.getUpgradeOrderTotal(order._id, function(err, total){

					doc = new pdf({autoFirstPage: false});
					var finalString = '';
					var stream = doc.pipe(base64.encode());

					doc.addPage({
						margin: 50
					})

					doc.image(logo, {height: 50});

					doc.fontSize(20);
					doc.text("Factuur", 400, 40, {align: 'right'});
					doc.fontSize(10);
					doc.moveDown(0.5);
					doc.text("Factuurnummer: " + order.number, {align: 'right'});
					doc.moveDown(0.3);
					var orderDate = new Date(order.date);
					doc.text("Datum: " + orderDate.getDate() + "-" + orderDate.getMonth() + "-" + orderDate.getFullYear(), {align: 'right'});

					doc.moveDown(2);
					doc.fontSize(10);

					doc.text(shop.name, 50);
					doc.moveDown(0.5);
					doc.text(shop.address.street + " " + shop.address.houseNumber + (shop.address.houseNumberSuffix ? shop.address.houseNumber : ""));
					doc.moveDown(0.3);
					doc.text(shop.address.postalCode)
					doc.moveDown(0.3);
					doc.text(shop.address.city);
					doc.moveDown(0.3);
					doc.text(shop.address.country);

					doc.moveUp(6.4);
					doc.text("PrismaNote", 300);
					doc.moveDown(0.3);
					doc.text("Middelweg 8d");
					doc.moveDown(0.3);
					doc.text("4147AV");
					doc.moveDown(0.3);
					doc.text("Asperen");
					doc.moveDown(0.3);
					doc.text("Nederland");
					doc.moveDown(3);

					doc.text("Shop upgrade", 50);
					doc.moveDown(0.3);
					doc.text("Totaalprijs:", 50);
					doc.moveUp();
					doc.text("€ " + total, 100);

					doc.moveDown(0.3);
					var tax = Math.round((total * 0.21) * 100) / 100;
					doc.text("Waarvan 21% BTW: € " + tax , 50);

					doc.moveDown(3);
					doc.text("Deze factuur is reeds voldaan.", 50);

					doc.end();
					stream.on('data', function(chunk){
						var a =0;
						finalString += chunk;
					})
					stream.on('end', function(){
						return callback(null, finalString);
					})
				})//end get total
			})
		}
	})
}