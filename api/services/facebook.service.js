const path = require('path');
const graph = require('fbgraph');

extendAccessToken = function(accessCode, callback){
    graph.extendAccessToken({
        "access_token": accessCode,
        "client_id": settings.facebook.appId,
        "client_secret": settings.facebook.appSecret
    }, function(err, facebookRes){
        if (err)
            return callback(err);
        return callback(null, facebookRes);
    })

}

module.exports.extendAccessToken = extendAccessToken;