const path = require('path');

var geo = require(path.join(__dirname, 'geo.service'));

module.exports = function(query, callback) {
	if(query.address && query.address.length > 0) {
		geo.code(query.address[0], 'nl', function(geoloc) {
			query.geoloc = geoloc;
		
		});
	}
	callback(query);
}