const mongoose = require('mongoose');
const Blacklist = mongoose.model('Blacklist');
const Brand = mongoose.model('Brand');

module.exports.getEmail = getEmail;
module.exports.addBlacklistRule = addBlacklistRule;
module.exports.checkBrandBlacklist = checkBrandBlacklist;

function getEmail(email, callback){
    Blacklist.findOne({email: email}).exec(function(err, blacklist){
        if(err){
            return callback(err);
        }
        return callback(null, blacklist);
    })
}

function checkBrandBlacklist(email, brand, brandSlug, callback){
    var found = false; 

    Blacklist.findOne({email: email}).exec(function(err, blacklist){
        if(!blacklist || blacklist.blockedBrands.length <= 0){
            return callback(false);
        }
        var counter = 0;
        for(var i=0; i < blacklist.blockedBrands.length; i++){
            if(brand){
                if(blacklist.blockedBrands[i].name == brand){
                    found = true;
                }
            }else{
                if(blacklist.blockedBrands[i].nameSlug == brandSlug){
                    found = true;
                }
            }
            counter++;
            if(counter == blacklist.blockedBrands.length){
                return callback(found);
            }
        }
    })
}


function addBlacklistRule(email, options, callback){
    console.log("ADDBLACK", email, options);
    if(!options){
        return callback("No options given");
    }
    getEmail(email, function(err, blacklist){
        if(err){
            return callback(err);
        }else{
            if(!blacklist){
                let newRule = {
                    email: email,
                    blockedBrands: [],
                    newsletter: {}
                };
                
                //there is no rule with this email, so let's create a new rule with the given options
                if(options.brand){
                    Brand.findOne({nameSlug: options.brand.nameSlug}).exec(function(err, brand){
                        if(err){
                            return callback(err);
                        }
                        if(!brand){
                            return callback("No brand with nameslug " + options.brand.nameSlug + " found");
                        }
                        
                        let blockedBrand = {
                            name: brand.name,
                            nameSlug: brand.nameSlug
                        }
                        newRule.blockedBrands.push(blockedBrand);
                        createRule(newRule, function(err, result){
                            if(err){
                                return callback(err);
                            }else{
                                return callback(null, result);
                            }
                            
                        })
                    })
                }
                if(options.newsletter){
                    newRule.newsletter = {
                        block: true,
                        dateChanged: Date.now
                    }
                    createRule(newRule, function(err, result){
                        if(err){
                            return callback(err);
                        }else{
                            return callback(null, result);
                        }
                    });
                }

                function createRule(newRule, callback){
                    Blacklist.create(newRule, function(err, result){
                        if(err){
                            return callback(err);
                        }
                        return callback(null, result);
                    })
                }
            }else{
                //email is already found in the blacklist
                if(options.brand){
                    Brand.findOne({nameSlug: options.brand.nameSlug}).exec(function(err, brand){
                        if(err){
                            return callback(err);
                        }
                        if(!brand){
                            return callback("No brand with nameslug " + options.brand.nameSlug + " found");
                        }
                        let blockedBrand = {
                            name: brand.name,
                            nameSlug: brand.nameSlug
                        }
                        blacklist.blockedBrands.push(blockedBrand);
                        updateRule(blacklist, function(err, result){
                            if(err){
                                return callback(err);
                            }else{
                                return callback(null, result);
                            }
                        });
                    })
                }
                if(options.newsletter){
                    blacklist.newsletter = {
                        block: true,
                        dateChanged: Date.now
                    }
                    updateRule(blacklist, function(err, result){
                        if(err){
                            return callback(err);
                        }else{
                            return callback(null, result);
                        }
                    });
                }
            }

            function updateRule(rule, callback){
                rule.save(function(err, result){
                    if(err){
                        return callback(err);
                    }
                    return callback(null, result);
                })
            }
        }
    })
}

//TODO: create function to delete a rule