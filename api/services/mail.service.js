const mailer = require('nodemailer');
const ses = require('nodemailer-ses-transport');
const path = require('path');
const async = require("async");
const compiler = require(path.join(__dirname, 'pug.service'));

const mongoose = require('mongoose');
const Email = mongoose.model('Email');

module.exports.mail = mail;
module.exports.logMail = logMail;
module.exports.updateLog = updateLog;
module.exports.getLogs = getLogs;

function mail(options, data, attach, callback) {
    //Keys from the user SESMailer in the AWS Console. User has only Full API Access for the SES service
    var transporter = mailer.createTransport(ses({
        accessKeyId: settings.awsMailer.accessKeyId,
        secretAccessKey: settings.awsMailer.secretAccessKey,
        region: settings.awsMailer.region
    }));

    if(!data){
        data = {};
    }

    //somethimes we need the options like subject in our mails
    data.email = options;
    data.bucket = settings.s3Bucket.location;
    //Fill default options
    if (!options.from) {
        options.from = '"PrismaNote" <no-reply@prismanote.com>';
    }

    if (!options.type) {
        options.type = null
    }
    if(options.priority){
        var headers;
        if(options.priority == 'high'){
            headers = {
                "x-priority" : "1",
                "x-msmail-priority": "High",
                "importance": "high"
            }
        }else if(options.priority == 'low'){
            headers = {
                "x-priority": "5",
                "x-msmail-priority": "low",
                "importance": "low"
            }
        }

    }    
    function getHtml(options, callback){
        if (options.emailTemplate == undefined || !options.emailTemplate) {
            //compile html template
            var templatePath = path.join(__dirname, '..', 'mail-templates', options.template + '.pug');
            
            compiler(templatePath, data, function (err, html) {
                if (err) {
                    return callback(err);
                }
                return callback(null, html);
            })
        }else{
            return callback(null, options.emailTemplate);
        }
    }

    if (attach != null) {
        var attachments = [];

        async.eachSeries(attach, function (item, callback) {
            var attachment = {
                filename: item.filename,
                content: item.content,
                encoding: 'base64'
            }
            attachments.push(attachment);
            callback();
        }, function () {

        })
    }

    getHtml(options, function(err, html){
        
        transporter.sendMail({
            from: options.from,
            to: options.to,
            bcc: options.bcc ? options.bcc : null,
            subject: options.subject,
            html: html,
            attachments: attachments ? attachments : null,
            headers: headers ? headers: null
        }, function (err, result) {
            if (!err) {
                logMail(result.messageId.split("@")[0], options, html, function (err, result) {
                    if (err) {
                        console.log("err", err);
                    }
                })
            }
        })
        if (typeof callback === 'function') {
            return callback(err, data);
        }
    })
}


function logMail(messageId, options, body, callback) {
    var logItem = {
        messageId: messageId,
        to: options.to,
        from: options.from,
        typeMail: options.type,
        subject: options.subject,
        body: body,
        timestamp: new Date(),
    }

    Email.create(logItem, function (err, emaillog) {
        if (err) {
            return callback(err, null);
        } else {
            return callback(null, emaillog);
        }
    })
}

function updateLog(messageId, message, callback) {
    Email.findOne({ messageId: messageId }).exec(function (err, email) {
        if (email) {
            email.notificationType = message.notificationType;
            email.timestamp = message.timestamp;
            email.smtpResponse = message.smtpResponse;
            email.rawData = message.rawData;

            email.save(function (err, result) {
                if (err) {
                    return callback(err, null);
                } else {
                    return callback(null, result);
                }
            })
        } else {
            return callback("no logItem found");
        }
    })
}

function getLogs(options, callback) {
    console.log("getLogs");
    Email.find(options).exec(function (err, emails) {
        if (!emails) {
            return callback("No logs found");
        } else {
            return callback(null, emails);
        }
    })
}
