const request = require('request');
const path = require('path');

const apiUrl = 'https://api.pinterest.com/v1/';

getAccessToken = function(accessCode, code, callback){
    request.post({
        url: apiUrl + 'oauth/token',
        qs: {
            grant_type: accessCode,
            client_id: settings.pinterest.clientId,
            client_secret: settings.pinterest.clientSecret,
            code: code
        }
    }, function(err, response, body){
        console.log("BODY", body);
        var result = JSON.parse(body);
        return callback(result)
    })

}

module.exports.getAccessToken = getAccessToken;