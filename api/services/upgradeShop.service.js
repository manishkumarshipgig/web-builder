const mongoose = require('mongoose');
const UpgradeOrder = mongoose.model('UpgradeOrder');
const ObjectId = require('mongodb').ObjectId;

module.exports.getUpgradeOrder = getUpgradeOrder;
module.exports.getShopUpgradeOrders = getShopUpgradeOrders;
module.exports.getUpgradeOrderStatus = getUpgradeOrderStatus;
module.exports.createOrUpdateUpgradeOrder = createOrUpdateUpgradeOrder;
module.exports.addUpgradeOrderStatus = addUpgradeOrderStatus;
module.exports.getShopModules = getShopModules;
module.exports.getUpgradeOrderTotal = getUpgradeOrderTotal;
module.exports.activateUpgradeOrder = activateUpgradeOrder;
module.exports.cancelUpgradeOrder = cancelUpgradeOrder;
module.exports.getSubscriptionOrder = getSubscriptionOrder;

function getUpgradeOrder(orderId, callback){
    UpgradeOrder.findOne({_id: orderId}).exec(function(err, order){
        if(err){
            return callback(err);
        }
        if(!order){
            return callback("No order found with the given id");
        }
        return callback(null, order);
    })
}
function getShopUpgradeOrders(shopId, callback){
    UpgradeOrder.find({shopId: ObjectId(shopId), active: true}).exec(function(err, order){
        if(err){
            return callback(err);
        }
        if(!order){
            return callback("No order found with the given id");
        }
        return callback(null, order);
    })
}

function getUpgradeOrderStatus(orderId, callback){
    UpgradeOrder.findOne({_id: orderId}).exec(function(err, order){
        if(err){
            return callback(err);
        }
        if(!order){
            return callback("No order found with the given id");
        }
        if(order.status.length > 0){
            return callback(null, order.status[order.status.length -1]);
        }else{
            return callback("This order has no status yet");
        }
    })
}

function getSubscriptionOrder(subscriptionId, callback){
    console.log("getSubscriptionOrder", subscriptionId);
    UpgradeOrder.findOne({subscriptionId: subscriptionId}).exec(function(err, order){
        if(err){
            return callback(err);
        }
        if(!order){
            return callback("No order found with the given id");
        }
        return callback(null, order);
    })
}

function createOrUpdateUpgradeOrder(orderId, orderDetails, callback){
    //since we are using upsert: true, mongoose will create a new document when there is no match on _id.
    if(!orderId){
        orderId = new ObjectId();
    }
    UpgradeOrder.findOneAndUpdate(
        {_id: orderId}, 
        orderDetails, {
            upsert: true, 
            new: true
        }, function(err, result){
            if(err){
                return callback(err);
            }
            return callback(null, result);
        })
}

function addUpgradeOrderStatus(orderId, status, callback){
    UpgradeOrder.findOne({_id: orderId}).exec(function(err, order){
        if(err){
            return callback(err);
        }
        if(!order){
            return callback("No order found with the given id");
        }
        order.status.push(status);

        order.save(function(err, result){
            if(err){
                return callback(err);
            }
            return callback(null, result);
        })
    })
}

function getShopModules(shopId, callback){
    UpgradeOrder.findOne({shopId: ObjectId(shopId), active: true}).exec(function(err, order){
        if(err){
            return callback(err);
        }
        if(!order){
            return callback(null,[]);
        }
        return callback(null, order.modules);
    })
}

function getUpgradeOrderTotal(orderId, callback){
    UpgradeOrder.findOne({_id: orderId}).exec(function(err, order){
        if(err){
            return callback(err);
        }
        if(!order){
            return callback("No order found with the given id");
        }
        var total =0, counter=0;
        for(var i=0; i < order.modules.length; i++){
            total += order.modules[i].amount;
            counter++;
        }
        if(counter == order.modules.length){
            return callback(null, total);
        }
    })
}

function activateUpgradeOrder(orderId, callback){
    UpgradeOrder.findOne({_id: orderId}).exec(function(err, order){
        console.log("activateUpgradeOrder", err, order);
        if(err){
            return callback(err);
        }
        if(!order){
            return callback("No order found with the given id");
        }
        
        for(var i=0; i < order.modules.length; i++){
            order.modules[i].active = true;
        }
        order.active = true;
        order.save(function(err, result){
            if(err){
                return callback(err);
            }else{
                return callback(null, result);
            }
        })
    })
}

function cancelUpgradeOrder(subscriptionId, status, callback){
    UpgradeOrder.findOne({subscriptionId: subscriptionId}).exec(function(err, order){
        console.log("err", err, "order", order);
        if(err){
            return callback(err);
        }
        if(!order){
            return callback("No order found with the given id");
        }
        
        for(var i=0; i < order.modules.length; i++){
            order.modules[i].active = false;
        }
        order.active = false;
        order.status.push(status);
        order.save(function(err, result){
            console.log(err, result);
            if(err){
                return callback(err);
            }else{
                return callback(null, result);
            }
        })
    })
}
