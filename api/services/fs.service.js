/* Include file path methods like .join() and .resolve(). */
const path = require('path');

/* Include ES6 Promise-wrapped Node fs module for filesystem operations. This can be used with Promises and async/await. */
const fs = require('async-file');

/* Include custom error types and error handler. */
const errorService = require(path.join(__dirname, 'error.service'));

const File = module.exports.File = class File {
	constructor(path) {

		try {
			
			/* Validate path and create a path property for the file object if it's provided and valid. */
			if(path == null) {
				throw new errorService.MissingParamsError('path');
			} else if(typeof path !== 'string') {
				throw new TypeError('Parameter `path` should be in string format. ');
			} else {
				this.path = path;
			}

			this.stat = async function() {

				/* Return file stat() results asynchronously, so this function can be used as a getter for the most recent stats. */
				return await fs.lstat(this.path);
			}

		} catch(err) {
			errorService.handler(err);
		}
	}
}

const getFile = module.exports.getFile = function(options) {

	try {

		/* Check if a parameters object is provided and throw an error otherwise. */
		if(typeof options !== 'object') {
			throw new errorService.MissingParamsError();
		}

		/* Check if the filename is provided and if it is valid. */
		/* TODO: regex validation */
		if(!options.filename || options.filename == null) {
			throw new Error('No filename provided.');
		} else if(typeof options.filename !== 'string') {
			throw new TypeError('Parameter `filename` should be in string format.');
		}

		let filePath;

		/* Check if the directory name is provided as a parameter, otherwise use the API root directory. */
		if(options.dirname && typeof options.dirname === 'string') {
			filePath = path.resolve(global.path.root, options.dirname, options.filename);
		} else {
			filePath = path.resolve(global.path.root, options.filename);
		}

		return new File(filePath);

	} catch(err) {
		errorService.handler(err);
	}
};

const getFiles = module.exports.getFiles = async function(options) {

	try {

		async function getDirectoryContents() {

			let dirContents = [];

			for(let i = 0; i < options.filenames.length; i++) {

				let filename = options.filenames[i];

				let file = await getFile({filename: options.filenames[i], dirname: options.dirname});

				let stats = await file.stat();

				if(stats.isDirectory()) {
					
					/* Recursively call the getFiles() function if the `recursive` option is provided as a parameter. If no `recursive` option is set: do nothing to prevent an error message when trying to read a directory instead of a file. */
					if(options.recursive && options.recursive === true) {

						/* Append files in subfolder to file list by calling getFiles() on the subdirectory */
						dirContents = [].concat(dirContents, await getFiles({dirname: path.resolve(options.dirname, file.path)}));
					}
				} else {

					/* If it's a file, just add to the list of filenames that should be read. */
					dirContents.push(file);
				}
			}

			return dirContents;
		}

		/* Check if a parameters object is provided and throw an error otherwise. */
		if(typeof options !== 'object') {
			throw new errorService.MissingParamsError();
		}

		/* Check if the directory name is provided and if it is valid. */
		/* TODO: regex validation */
		if(!options.dirname || options.dirname == null) {
			throw new errorService.MissingParamsError('dirname');
		} else if(typeof options.dirname !== 'string') {
			throw new TypeError('Parameter `dirname` should be in string format.');
		}

		/* Check if a list of filenames is provided as a parameter. If that is the case, only read the specified files. If not, read all files in the specified directory. Concatenation is possible, but set to false by default. */
		if(options.filenames && Array.isArray(options.filenames) && options.filenames.length > 0) {
			
			/* Check if all filenames are strings and throw an error otherwise. */
			/* TODO: regex validation */
			if(options.filenames.some(filename => typeof filename !== 'string')) {
				throw new TypeError('All filenames should be strings.');
			}

		} else {

			/* Find all items in the specified directory (including directories if the `recursive` option is specified). */
			options.filenames = await fs.readdir(path.resolve(global.path.root, options.dirname));
		}

		const files = await getDirectoryContents();

		return files;

	} catch(err) {
		errorService.handler(err);
	}
};

/* Asynchronous function to read a single file and return its contents using a promise. */
const readFile = module.exports.readFile = async function(options) {

	try {

		/* Check if a parameters object is provided and throw an error otherwise. */
		if(typeof options !== 'object') {
			throw new errorService.MissingParamsError();
		}

		if(!options.file) {
			options.file = getFile({filename: options.filename, dirname: options.dirname});	
		}

		/* Use `encoding` parameter if provided, otherwise default to UTF-8. */
		if(!options.encoding || options.encoding == null) {
			options.encoding = 'utf8';
		} else if(typeof options.encoding !== 'string') {
			throw new TypeError('Parameter `encoding` should be in string format if provided.');
		}

		/* Read the file asynchronously and return if succesful. */
		const contents = await fs.readFile(options.file.path, options.encoding);

		// if(!contents) {
		// 	throw new errorService.FileNotFoundError(null, options.file.path);
		// } else if(contents.length === 0) {
		// 	throw new Error('File is empty. ');
		// }
		
		return contents;

	} catch(err) {
		errorService.handler(err);
	}
};

/* Asynchronous function that reads multiple files by calling readFile() for each file. A list of filenames may be specified to limit the files read to a subset of all files in the directory. Returns a promise that resolves if all files are successfully read and is rejected if any file read operation fails. */
const readFiles = module.exports.readFiles = async function(options) {

	try {

		/* Check if a parameters object is provided and throw an error otherwise. */
		if(typeof options !== 'object') {
			throw new errorService.MissingParamsError();
		}

		/* Use `encoding` parameter if provided, otherwise default to UTF-8. */
		if(!options.encoding || options.encoding == null) {
			options.encoding = 'utf8';
		} else if(typeof options.encoding !== 'string') {
			throw new TypeError('Parameter `encoding` should be in string format if provided.');
		}

		const files = await getFiles(options);

		/* If no files are found in the specified directory, throw an error. */
		if(files.length == 0) {
			throw new errorService.FileNotFoundError('The specified directory `' + options.dirname + '` is empty/invalid or the provided filenames ' + options.filenames + ' cannot be found there.');
		}

		let contents = [];

		/* For each file, read the contents with readFile() and return an array of promises that will return files. */
		for(i = 0; i < files.length; i++) {
			options.file = files[i];
			contents[i] = await readFile(options);
		}

		/* Optional: concatenate files before returning. */
		if(options.concat && options.concat === true) {

			let concatenatedContents = await Promise.all(contents);

			/* Return concatenated files. */
			return concatenatedContents.join('\n');

		} else {

			/* Return file contents as array. */
			return contents;
		}
	} catch (err) {
		errorService.handler(err);
	}
};
