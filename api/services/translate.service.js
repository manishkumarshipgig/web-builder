const mongoose = require('mongoose');
const Translation = mongoose.model('Translation');

module.exports.translate = translate;

function translate(keyword, lang, callback){
    if(!keyword){
        if(typeof callback === 'function'){
            return callback("No keyword provided");
        }
        return "No keyword provided";
    }else{
        Translation.findOne({keyword: keyword}).exec(function(err, translation){
            if(!translation){
                if(typeof callback === 'function'){
                    return callback("No translations found");
                }
                return "No translations found";
            }else{
                if(lang){
                    if(typeof callback === 'function'){
                        return callback(translation[lang])
                    }
                    return translation[lang];
                }else{
                    if(typeof callback == 'function'){
                        return callback(translation);
                    }else{
                        return translation;
                    }
                }
            }
        })
    }
}