var crypto = require("crypto");

/**
 * Generates a random value with letters and numbers
 * 
 * @param {string} len
 */
module.exports = function(len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex')
        .slice(0, len);
}