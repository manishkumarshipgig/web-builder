function objectLengthModern( object ) {
	return Object.keys(object).length;
}

function objectLengthLegacy( object ) {
	var length = 0;
	for( var key in object ) {
		if( object.hasOwnProperty(key) ) {
			++length;
		}
	}
	return length;
}

exports.objectLength = Object.keys ? objectLengthModern : objectLengthLegacy;
