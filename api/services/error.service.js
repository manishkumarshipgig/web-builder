/* For each of these custom error types: Create a new ES6 class, that prototypically inherits from the Error constructor. */
const CustomError = module.exports.CustomError = class extends Error {
	constructor(message, ...args) {
		
		super(message);
		
		this.name = this.constructor.name;
		this.message = message || 'An error has occurred, but no error message has been specified for this error type.';
		
		if (typeof Error.captureStackTrace === 'function') {
			Error.captureStackTrace(this, this.constructor);
		} else {
			this.stack = (new Error(...args)).stack;
		}
	}
};

/* ErrorHandler can return HTTP type errors as an HTTP response to the client. That is why subclasses of HTTPError have HTTP status codes. */
const HTTPError = module.exports.HTTPError = class extends CustomError {
	constructor(message, statusCode, ...args) {
		
		super(message || 'An HTTP error has occurred, but no error message has been specified for this error type.');
		
		/* Default to 500 Internal Server Error if no HTTP response code has been specified. */
		this.statusCode = statusCode || 500;
	}
};

const BadRequest = module.exports.BadRequest = class extends HTTPError {
	constructor(message) {
		
		super(message || 'Bad request. The HTTP request was incomplete or invalid and could not be processed. ', 400);
	}
};

/* Throw an error when someone is not allowed/authorized to acces certain resources or execute certain functions. */
const Unauthorized = module.exports.Unauthorized = class extends HTTPError {
	constructor(message) {
		
		super(message || 'You are not authorized to request this data. Log in or register and try again. ', 401);
	}
};

/* Throw an error when someone is not allowed/authorized to acces certain resources or execute certain functions. */
const PaymentRequired = module.exports.PaymentRequired = class extends HTTPError {
	constructor(message) {
		
		super(message || '. ', 402);
	}
};

const Forbidden = module.exports.Forbidden = class extends HTTPError {
	constructor(message) {
		
		super(message || 'You do not have the required permissions to access this resource. ', 403);
	}
};

const NotFound = module.exports.NotFound = class extends HTTPError {
	constructor(message) {

		super(message || 'The requested resource could not be found on the server. ', 404);
	}
};

const MethodNotAllowed = module.exports.MethodNotAllowed = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 405);
	}
};

const NotAcceptable = module.exports.NotAcceptable = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 406);
	}
};

const ProxyAuthenticationRequired = module.exports.ProxyAuthenticationRequired = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 407);
	}
};

const RequestTimeout = module.exports.RequestTimeout = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 408);
	}
};

const Conflict = module.exports.Conflict = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 409);
	}
};

const Gone = module.exports.Gone = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 410);
	}
};

const UnprocessableEntity = module.exports.UnprocessableEntity = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 422);
	}
};

const FailedDependency = module.exports.FailedDependency = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 424);
	}
};

const InternalServerError = module.exports.InternalServerError = class extends HTTPError {
	constructor(message) {

		super(message || 'Internal server error. ', 500);
	}
};

const NotImplemented = module.exports.NotImplemented = class extends HTTPError {
	constructor(message) {

		super(message || 'This feature has not been implemented yet. ', 501);
	}
};

const BadGateway = module.exports.BadGateway = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 502);
	}
};

const ServiceUnavailable = module.exports.ServiceUnavailable = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 503);
	}
};

const GatewayTimeout = module.exports.GatewayTimeout = class extends HTTPError {
	constructor(message) {

		super(message || 'Gateway timeout. The request from this server to an external data source, API or database has timed out. ', 504);
	}
};

const HTTPVersionNotSupported = module.exports.HTTPVersionNotSupported = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 505);
	}
};

const NetworkAuthenticationRequired = module.exports.NetworkAuthenticationRequired = class extends HTTPError {
	constructor(message) {

		super(message || '. ', 511);
	}
};

const FileError = module.exports.FileError = class extends CustomError {
	constructor(message, filePath) {

		super(message || 'An unknown filesystem operation (read/write) error has occurred at: ' + filePath + '. No detailed error message has been specified for this error type.');
		
		this.filePath = filePath;
	}
};

const FileNotFoundError = module.exports.FileNotFoundError = class extends FileError {
	constructor(message, filePath) {
		
		super(message || 'The requested file cannot be found on the server. File location: ', filePath);
	}
};

const FilePermissionsError = module.exports.FilePermissionsError = class extends FileError {
	constructor(message, filePath) {
		
		super(message || '.', filePath);
	}
};

const MissingParamsError = module.exports.MissingParamsError = class extends CustomError {
	constructor(params) {

		let message;

		/* Only one parameter is missing. */
		if(typeof params === 'string') {
			message = 'Parameter `' + params + '` is required. ';

		/* Multiple parameters are missing. */
		} else if(Array.isArray(params) && params.every(param => typeof param === 'string')) {
			message = 'Parameters ' + params + ' are all required, but were not provided. ';
		
		/* Otherwise (no valid param names provided). */
		} else {
			message = 'One or more parameters are required, but missing. ';
		}
		
		super(message);
	}
}

/* Generic handler for all error types that could be thrown somewhere in the application. Should also account for custom error types. */
module.exports.handler = function(err) {

	try {

		if(!(err instanceof Error)) {
			err = new TypeError('An error occurred in the error handler: argument passed is not an instance of Error. ');
		}

		switch(err.constructor) {
			
			/* TODO: specific handlers for default JS errors. */
			case EvalError || RangeError || ReferenceError || SyntaxError || TypeError || URIError:
				console.error(err);
				break;
			
			/* Generic file error handler if no subtype of FileError has been specified. Can be called if something goes wrong with a file, but no more specific info is available. */
			case FileError:
				console.error(err);
				break;
			
			/* Generic HTTP error handler if no subtype of HTTPError has been specified. Can be called to return the error to the user without a specific error status code. */
			case HTTPError:
				console.error(err)
				/* TODO: Send a response back to the frontend. */
				break;

			/* Other error types (for which no specific handler function has been defined here). */
			default:

				/* Node.js system errors (can be identified by their error code property, which other errors don't have). */
				if(err.code) {
					switch(err.code) {

						/* TODO: Handle Node.js system errors. */

						/* Fallback handler for otherwise uncaught Node.js system errors. */
						default:
							console.error(err);
					}
				}
				/* Errors that are generated with `new Error()` without using a predefined error type, or other unrecognized error types. */
				else {
					console.error(err);
				}
		}
		
	} catch(internalHandlerError) {
		console.error('An error occurred in the error handler: ' + internalHandlerError);
	}
};
