const path = require('path');
const chalk = require('chalk');
const request = require('request');
const _ = require('lodash');

const mongoose = require('mongoose');
const Shop = mongoose.model('Shop');
const SocialPortal = mongoose.model('Socialportal');
const ObjectId = require('mongodb').ObjectId;

const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const stringService = require(path.join(__dirname, '..', 'services', 'string.service'));
const upgradeShop = require(path.join(__dirname, '..', 'services', 'upgradeShop.service'));

module.exports.getShopUrl = getShopUrl;
module.exports.updateStock = updateStock;
module.exports.getItemPrice = getItemPrice;
module.exports.getShopSocialPortal = getShopSocialPortal;
module.exports.getShopFromSlug = getShopFromSlug;
module.exports.getShopLogo = getShopLogo;
module.exports.updateCountrIdOnShopProduct = updateCountrIdOnShopProduct;
module.exports.updateCountrIdOnShopCollection = updateCountrIdOnShopCollection;
module.exports.updateDeviceOrMerchantIdOnShop = updateDeviceOrMerchantIdOnShop;
module.exports.checkModule = checkModule;

function getShopUrl(searchParams, origin, callback){
    filterBy = filter(searchParams);

    Shop.findOne(filterBy).exec(function(err, shop){
        if(shop && shop.proShopUrl){
            return callback(shop.proShopUrl);
        }else if(shop && shop.website){
            return callback(shop.website);
        }else{
            return callback(origin + '/shop/' + shop.nameSlug);
        }
    })
}

/**
 * Set or Update the countrId on a ShopProduct
 * @param {ObjectId} shopId - ObjectId of the shop
 * @param {ObjectId} productId - ObjectId of the shopProduct
 * @param {string} countrId - The Id from Countr
 * @param {object} callback - Callback with err or result
 */
function updateCountrIdOnShopProduct(shopId, productId, countrId, callback){
    Shop.findOne({_id: ObjectId(shopId)}).exec(function(err, shop){
        if(err || !shop){
            return callback(err ? err : "No shop found")
        }
        var productIndex = _.findIndex(shop.products, {_id : productId});

        if(productIndex >= 0){
            shop.products[productIndex].countrId = countrId;

            shop.save(function(err, result){
                if(err) return callback(err);
                return callback(null, result);
            })
        }else{
            console.error("Shopproduct not found");
            return callback("Shopproduct not found");
        }
    })
}


/**
 * Set or Update the countrId on a ShopCollection
 * @param {ObjectId} shopId - ObjectId of the shop 
 * @param {ObjectId} collectionId - ObjectId of the shopCollection
 * @param {string} countrId - The Id from Countr 
 * @param {object} callback - Callback with err or result
 */
function updateCountrIdOnShopCollection(shopId, collectionId, countrId, callback){
    Shop.findOne({_id: ObjectId(shopId)}).exec(function(err, shop){
        if(err || !shop){
            return callback(err ? err : "No shop found");
        }
        var collectionIndex = _.findIndex(shop.collections, {_id: collectionId});
        if(collectionIndex >= 0){
            shop.collections[collectionIndex].countrId = countrId;
            shop.save(function(err, result){
                if(err) return callback(err);
                return callback(null, result);
            })
        }else{
            return callback("No collection found");
        }
    })
}

function updateDeviceOrMerchantIdOnShop(shopId, deviceId, merchantId, callback){
    Shop.findOne({_id: ObjectId(shopId)}).exec(function(err, shop){
        if(err || !shop){
            return callback(err ? err : "No shop found");
        }
        if(deviceId) shop.countr.deviceId = deviceId;
        if(merchantId) shop.countr.merchantId = merchantId;

        shop.save(function(err, result){
            return callback(err, result);
        })
    })
}

/**
 * Update stock for a specific product on a shop
 * @param {ObjectId} shopId - The ObjectId of the shop
 * @param {String} itemId - The string or ObjectId of the product
 * @param {Number} quantity - The value which has to be added or lowered
 * @param {Boolean} mode - true when stock most been lowered, false when adding stock
 */
function updateStock(shopId, itemId, quantity, mode){
    console.log("updateStock", quantity, mode, shopId.toString(), itemId.toString());

    Shop.aggregate([
        {$match: {_id: shopId}},
        {$project: {
            products: {$filter: {
                input: '$products',
                as: 'product',
                cond: {$eq: ['$$product._id', ObjectId(itemId)]}
            }}
        }}
    ]).exec(function(err, shop){
        var product = shop[0].products[0];

        var newStock;
        if(mode){
            if(product.stock <= 0 ){
                return;
            }
            newStock = product.stock - quantity;
        }else{
            newStock = product.stock + quantity;
        }

        Shop.update({
            _id: shopId, "products._id": itemId},
            {$set:
                {"products.$.stock" : newStock}},
            function(err, result){
                console.log(err, result);
                return;
            }
        )
    })
}

function getItemPrice(nameSlug, itemId, callback){
    //get the shop with only the specific product
    Shop.aggregate([
        {$match: {nameSlug: nameSlug}},
        {$project: {
            products: {$filter: {
                input: '$products',
                as: 'product',
                cond: {$eq: ['$$product._id', ObjectId(itemId)]}
            }}
        }}
    ]).exec(function(err, shop){
         if(err){
             return callback ("Error finding shop: " + err);
         }
         if(!shop || !shop[0] || !shop.products || !shop.products[0] && shop[0].products[0].length == 0){
             return callback("No shop or products found");
         }

        return callback(null, shop[0].products[0].price);
    })
}

function getShopSocialPortal(shopId, callback){
    if(shopId){
        SocialPortal.findOne({'shops._id': {$in: [shopId.toString()]}}).exec(function(err, socialPortal){
            var status, response;
            
            if(err) {
                console.log("error finding portal", err);
                return callback("Error finding Portal: " + err);
            } else if (!socialPortal) {
                return callback("No portal found");
            } else {
               return callback(null, socialPortal);
            }
        })
    }
}

function getShopFromSlug(slug, callback){
    Shop.findOne({nameSlug: slug}).exec(function(err, shop){
        if(err){
            return callback(err);
        }
        if(!shop){
            return callback("No shop found");
        }else{
            return callback(null, shop);
        }
    })
}

function getShopLogo(shopSlug, type, returnType, callback){
    if(!returnType || returnType == ""){
        returnType = 'base64';
    }

    var logo = "logo" + stringService.capitalizeFirstLetter(type);
  
    getShopFromSlug(shopSlug, function(err, shop){
        if(err){
            return callback(err);
        }

        function getLogoUrl(logo, result){
            if(shop[logo] && shop[logo].src){
                if(shop[logo].src.indexOf('images') !== -1){
                    return result("https://prismanote.com/" + shop[logo].src);
                }else{
                    return result(settings.s3Bucket.location + shop[logo].src);
                }
            }else{
                return result("https://prismanote.com/images/prismanote_logo_large.png");
            }
        }

        getLogoUrl(logo, function(result){
            if(returnType == 'base64'){
                request({url: result, encoding: null}, (error, response, body) => {
                    if(!error && response.statusCode == 200){
                        var img = new Buffer(body, 'base64');
                        return callback(null, img);
                    }
                })
            }else{
                return callback(null, result);
            }   
        })
    })
}

function checkModule(mod, shopId, callback){
    console.log("checkModule");
    let result = false;

    upgradeShop.getShopModules(shopId, function(err, modules){
        if(err){
            return false;
        }
        let counter = 0;
        for(var i=0; i < modules.length; i++){
            if(modules[i].name == mod){
                result = true;
            }
            counter++;
            if(counter == modules.length){
                console.log("result", result);
                return callback(result);
            }
        }
    })
}
