var mongoose = require('mongoose');
var User = mongoose.model('User');

var _ = require('lodash');

function findUserByEmail (email, callback){
    User.findOne({email: email}).exec(function(err, user){
        if (err){
            var err = {
                code: 500,
                err: err
            }
            return callback(err);
        }
        if(!user){
            var err = {
                code: 404,
                err: "No user found"
            }
            return callback(err);
        }
        if(user && !user.isVerified) {
            var err = {
                code: 409,
                err: "User is not activated"
            }
            return callback(err, user);
        }
        return callback(null, user);
    })
}

function updateUser(userId, data, callback){
    User.findByIdAndUpdate(userId, {$set: data}, {new: true}, function (err, user) {
        if(err) {
            return callback(err);
        } else if(!user) {
            return callback("User not found");
        } else {
            return callback(null, user);
        }
    })
}

module.exports.findUserByEmail = findUserByEmail;
module.exports.updateUser = updateUser;

