module.exports = function(contact) {
	var vcard = "BEGIN:VCARD\n" + "VERSION:2.1\n" + "N:;;;;\n" + "FN:" + contact.name + "\n" + "ORG:" + contact.name + "\n" + "TITLE:\n" + "PHOTO;\n";
	
	if (contact.phone != null && contact.phone.countryCode != null && contact.phone.landLine != null) {
		vcard += "TEL;TYPE=work,voice;VALUE=uri:tel:+" + contact.phone + "\n";
	}
	
	if(contact.address != null && contact.address.street != null && contact.address.houseNumber != null && contact.address.city != null && contact.address.postalCode != null && contact.address.country != null) {
		vcard += "ADR;INTL;PARCEL;WORK:;;" + contact.address.street + " " + contact.address.houseNumber + ";" + contact.address.city + ";;" + contact.address.postalCode + ";" + contact.address.country + "\n";
	}
	
	if(contact.email != null) {
		vcard += "EMAIL:" + contact.email + "\n";
	}

	if(contact.website != null) {
		vcard += "URL;TYPE=work;:" + contact.website + "\n";
	}

	vcard += "REV:" + new Date() + "\nEND:VCARD";

	return vcard;
};