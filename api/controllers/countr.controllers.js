const mongoose = require('mongoose');
const path = require('path');
const async = require('async');
const DateDiff = require('date-diff');
const _ = require('lodash');
const moment = require('moment');
const PhoneNumber = require('awesome-phonenumber');
const chalk = require('chalk');

const User = mongoose.model('User');
const Shop = mongoose.model('Shop');
const Product = mongoose.model('Product');
const Brand = mongoose.model('Brand');
const crmUser = mongoose.model('crmUser');
const ObjectId = require('mongodb').ObjectId;
const Collection = mongoose.model('Collection');

const countr = require(path.join(__dirname, '..', 'services', 'countr.service'));
const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));
const random = require(path.join(__dirname, '..', 'services', 'random.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const str  = require(path.join(__dirname, '..', 'services', 'string.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));

function bucketLocation() {
    return settings.s3Bucket.location;
}

//#region access

module.exports.getAccessToken = function(req, res){
    var shopId = req.body.shopId ? req.body.shopId : null;
    var username = req.body.username ? req.body.username : null;
    var password = req.body.password ? req.body.password : null;

    console.log("getAccessToken", shopId, username, password);

    countr.accessToken(shopId, username, password, function(err, result){
        if(err){
            console.error("Error while getting accessToken", err);
            return res.status(err.code).json(err);
        }
        return res.status(200).json({countr: result});
    })
}

/** Wrapper for accessToken */
module.exports.getTokens = countr.accessToken;


//#endregion

//#region Taxes

function getTaxesWrapper(id, shopId, callback){
    countr.accessToken(shopId, null, null, function(err, tokens){
        if(err){
            return callback(err);
        }
        if(id){
            countr.callApi({
                url: "taxes/" + id
            }, null, tokens.accessToken, function(err, tax){
                return callback(err, tax);
            })  
        }else{
            countr.callApi({
                url: "taxes"
            }, null, tokens.accessToken, function(err, taxes){
                return callback(err, taxes);
            }) 
        }
    })
}
module.exports.getTaxes = function(req, res){
    var id = req.params.id ? req.params.id : null;

    getTaxesWrapper(id, req.query.shopId, function(err, body){
        if(err){
            return res.status(500).json(err);
        }
        if(body.error){
            return res.status(400).json({error: body.error, message: body.message});
        }

        return res.status(200).json(body);
    })
}
function getTax(taxId, shopId, access_token, callback){
    getTaxesWrapper(taxId, shopId, function(err, body){
        if(err){
            return callback(err);
        }
        return callback(null, body);
    })
}
module.exports.deleteTax = function(req,res){
    countr.accessToken(req.params.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }
        countr.deleteTax(req.params.id, tokens.accessToken, function(err, response, body){
            if(err){
                console.error(err);
                return res.status(500).json(err);
            }
            if(body.error){
                console.error("Error deleting tax");
                return res.status(400).json({error: body.error, message: body.message});
            }

            return res.status(200).json(body);
        })
    })
}
module.exports.createNewTax = function(req, res){
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }
        newTax({
            name: req.body.taxName,
            rate: req.body.taxRate,
            default: req.body.default
        }, tokens.accessToken, function(err, tax){
            if(err){
                return res.status(500).json({message: err})
            }
            return res.status(200).json({tax: tax})
        })
    })
}

/**
 * Create a new tax
 * @param {Object} tax - Object with the name, rate and default setting for the new tax
 * @param {String} access_token - Access Token
 * @param {Object} taxCb - The callback with err or tax
 */
function newTax(tax, access_token, taxCb){
    countr.callApi({
        url: 'me'
    }, null, access_token, function(err, merchant){
        if(err){
            return taxCb(err);
        }

        var data = {
            merchant: merchant._id,
            name: tax.name,
            rules: [
                {
                    rate: tax.rate / 100,
                }
            ],
            default: tax.default
        }

        countr.callApi({
            url: 'taxes',
            method: "POST"
        }, data, access_token, function(err, result){
            return taxCb(err, result);
            
        }) 

    })
}
//#endregion

//#region Categories
function getCategoryWrapper(id, shopId, callback){
    countr.accessToken(shopId, null, null, function(err, tokens){
        if(err){
            callback(err);
        }
        if(id){
            countr.callApi({
                url: "categories/" + id + '/simple'
            }, null, tokens.accessToken, function(err, categorie){
                return callback(err, categorie);
            })  

        }else{
            countr.callApi({
                url: "categories"
            }, null, tokens.accessToken, function(err, categories){
                return callback(err, categories);
            }) 
        }
    })
}
module.exports.getCategories = function(req, res){
    console.log("getCategories", req.params);
    var id = req.params.id ? req.params.id : null;
    
    getCategoryWrapper(id, req.query.shopId, function(err, body){
        if(err){
            return res.status(500).json(err);
        }

        return res.status(200).json(body);
    })
}
/**
 * Create a new category
 * @param {string} name - The name of the new category
 * @param {string} access_token - The access token
 * @param {countrCallback} callback - Callback with err or category
 */
function createCategory(name, access_token, callback){
    countr.getRandomColor(function(color){
        countr.callApi(
            {
                url: 'categories', 
                method: 'POST'
            }, {
                name: name, 
                visible: true, 
                color: color 
            }, 
            access_token, 
            function(err, category){
                return callback(err, category);
            }
        )
    })
}

/**
 * Get the collections of a product in an array
 * @param {string} productId ProductId
 * @param {function} callback callback with error or result
 */
function getCountrCategories(shopId, access_token, callback){
    countr.callApi({
        url: 'stores/' + shopId + '/categories'
    }, {limit: 1000}, access_token, function(err, categories){
        return callback(err, categories);
    })
}


/**
 * Try to match the categories from Countr with the categories on the shop, if they not exists create the category and save back to the shop
 */
function getCategories(shop, productId, countrCategories, language, uncategorized, access_token, callback){
    //First get the product, to determine all the collections
    Product.findOne({_id: productId}).exec(function(err, product){
        if(err || !product){
            return callback(err ? err : "No product found");
        }
        var categories = [];
        
        //Collections are not required on a product, so if there a none collections, the product will be placed in the 'Uncategorized' category in Countr
        if(!product.collections || product.collections.length == 0){
            categories.push(uncategorized);
            return callback(null, categories);
        }

        //Loop through all collections of the product
        async.forEachOfSeries(product.collections, function(collection, key, collectionsDone){
            //Check if the collection exists on the shop to determine if they is synced already
            var shopCollection = _.find(shop.collections, {_id: ObjectId(collection._id)});
            if(shopCollection){

                Collection.findOne({_id: shopCollection._id}).exec(function(err, coll){
                    //When the product is already synced with Countr, get the category from Countr
                    if(shopCollection.countrId){
                        countr.callApi({
                            url: 'categories/' + shopCollection.countrId + '/simple'
                        }, null, access_token, function(err, countrCollection){

                            if(err){
                                if(err.error == 'not_found'){
                                    //Collection has an countrId, but doesn't exist in countr, we need to recreate it and save the new id
                                    createCategory(coll[language].name, access_token, function(createErr, result){
                                        if(createErr) return callback(createErr);
                                        
                                        shopService.updateCountrIdOnShopCollection(shop._id, shopCollection._id, result._id, function(updateErr, saveResult){
                                            if(updateErr) return callback(updateErr);
                                            categories.push(result);
                                            collectionsDone();
                                        })
                                    })
                                }else{
                                    return callback(err);
                                }
                                
                            }else{
                                categories.push(countrCollection);
                                collectionsDone();
                            }
                        })
                    }else{
                        //This collection is not synced with Countr yet, we need to do that now
                        //First grab the collection to get the name
                        createCategory(coll[language].name, access_token, function(err, result){
                            if(err) return callback(err);
                            //Save the id of the newly created category on the shop
                            shopService.updateCountrIdOnShopCollection(shop._id, shopCollection._id, result._id, function(err, saveResult){
                                if(err) return callback(err);
                                categories.push(result);
                                collectionsDone();
                            })
                        })
                    }
                })
            }else{
                console.log("Collection does not exists on shop");
                //category is not added to the shop yet, do nothing since this collection wil be added automatically with a cron job
                //For now this product has to be placed in the uncategorized category
                categories.push(uncategorized);
                return callback(null, uncategorized);
            }
        }, function(){
            //collectionsDone()
            return callback(null, categories);
        })
    })
}
module.exports.syncCollections = function(req, res){
    
    var lang = req.body.lang;

    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }
        if(err){
            return res.status(500).json(err);
        }
        syncCollections(req.body.shopId, lang, tokens.accessToken, function(err, result){
            if(err && err != 'nothing to do'){
                return res.status(err.code).json(err.msg)
            }
            return res.status(200).json({message: "Collections synced"});
        });
    })
    
}
function createOrUpdateCategory(collection, name, shopId, access_token, callback){

    if(collection.countrId){
        //countrId is present, we should check if there is an category that match an countr category
        countr.callApi({url: 'categories/' + collection.countrId + '/simple'}, null, access_token, function(err, category){
            if(err && err.error == 'not_found'){
                //no category found, we should create one
                createCategory(name, access_token, function(createErr, category){
                    if(category){
                        if(createErr) return callback(createErr);
                                        
                        shopService.updateCountrIdOnShopCollection(shopId, collection._id, category._id, function(updateErr, saveResult){
                            if(updateErr) return callback(updateErr);
                            return callback(null, category);
                        })

                    }else{
                        return callback(err);
                    }
                })
            }else if(err){
                console.error("error while getting category", err)
                return callback(err);
            
            }else{
                //check if it is needed to update the category
                if(category.name != name){
                    var data = {
                        _id: category._id,
                        name: name,
                        color: category.color,
                        visible: category.visible
                    };
                    var url = 'categories/' + category._id;
                    countr.callApi({
                        url: url,
                        method: "PATCH"
                    }, data, access_token, function(err, category){
                        if(category){
                            return callback(null, category);
                        }else{
                            return callback(err);
                        }
                    })
                }else{
                    return callback(null, "nothing to do");
                }
            }
        })
    }else{
        //create an category since there is none countrId known
        createCategory(name, access_token, function(createErr, category){
            if(createErr) return callback(createErr);
                                        
            shopService.updateCountrIdOnShopCollection(shopId, collection._id, category._id, function(updateErr, saveResult){
                if(updateErr) return callback(updateErr);
                return callback(null, category);
            })
        })
    }
}
/**
 * Sync collections as categories to Countr
 * @param {string} shopId - PrismanNote shopid 
 * @param {string} language - Languge to be synced (nl, en, fr, de, es)
 * @param {string} access_token - Access token to use the Countr API
 * @param {callback} callback - Callback with err or 'done'
 */
function syncCollections(shopId, language, access_token, callback){
    console.log("syncCollections");

    async.waterfall([
        getShop,
        collectionsLoop
    ], function(err, result){
        return callback(err, result);
    })

    function getShop(cb){
        Shop.findOne({_id: ObjectId(shopId)}).exec(function(err, shop){
            if(err || !shop){
                cb(err ? err : "No shop found");
            }else{
                cb(null, shop);
            }
        })
    }

    function collectionsLoop(shop, cb){

        async.forEachOfSeries(shop.collections, function(collection, key, collectionsDone){
            async.waterfall([
                collectionFindOne,
                createUpdateCategory,
                updateIdOnShop

            ], function(err, result){
                collectionsDone(err, result);
            })

            function collectionFindOne(cb){
                Collection.findOne({_id: collection._id}).exec(function(err, coll){
                    if(err || !coll){
                        cb(err ? err : "No collection found");
                    }else{
                        cb(null, coll);
                    }
                })
            }

            function createUpdateCategory(coll, cb){
                createOrUpdateCategory(collection, coll[language].name, shop._id, access_token, function(err, category){
                    if(err){
                        cb(err);
                    }else if(category && category._id){
                        cb(null, coll, category);
                    }else{
                        //If there is nothing to do, send the result as error
                        cb(category);
                    }
                })
            }

            function updateIdOnShop(coll, category, cb){
                shopService.updateCountrIdOnShopCollection(shop._id, coll._id, category._id, function(err, updateResult){
                    if(err){
                        cb(err);
                    }else{
                        cb(null, "done");
                    }
                })
            }


        }, function(err, result){
            return cb(err, result);
        })
    }
}

module.exports.updateCategory = function(req,res){
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            return res.status(err.code).json(err.message);
        }
        let category = req.body.category;
        let language = req.body.language;

        createOrUpdateCategory(category, category[language].name, req.body.shopId, tokens.accessToken, function(err, category){
            if(err){
                console.error("Error during createOrUpdateCategory", err)
                return res.status(500).json(err);
            }
            return res.status(200).json({message: "Collection synced", category})
        })
    })
}
module.exports.deleteCategory = function(req, res){
    countr.accessToken(req.params.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }
        countr.callApi({
            url: 'categories/' + req.params.id,
            method: "DELETE"
        }, null, tokens.accessToken, function(err, result){
            if(err){
                console.error("Error during deleting category", err);
                return res.status(500).json(err);
            }
            return res.status(200).json("Category deleted");
        })
    })
}
module.exports.deleteAllCategories = function(req, res){
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }

        countr.callApi({
            url: 'stores/' + req.body.storeId + '/categories'
        }, null, tokens.accessToken, function(err, categories){
            if(err){
                return res.status(500).json({message: "Error getting categories", err})
            }
            var counter = 0;

            categories.forEach(function(category){
                countr.callApi({
                    url: 'categories/' + category._id,
                    method: 'DELETE'
                }, {limit: 1000}, tokens.accessToken, function(err, result){
                    if(err){
                        console.log("Error while deleting product: ", product.name, err);
                        return;
                    }
                    console.log(category.name, chalk.red("deleted"));
                    counter++;
                    if(counter == categories.length){
                        return res.status(200).json({message: categories.length + " categories deleted!"})
                    }
                })
            })
        })
    })
}
//#endregion

//#region Stores
function getStoreWrapper(id, shopId, callback){
    countr.accessToken(shopId, null, null, function(err, tokens){
        if(err){
            return callback(err);
        }
        if(id){
            countr.callApi({
                url: "stores/" + id
            }, null, tokens.accessToken, function(err, store){
                return callback(err, store);
            })

        }else{
            countr.callApi({
                url: "stores"
            }, null, tokens.accessToken, function(err, stores){
                return callback(err, stores);
            })
        }
    })
}
module.exports.getStores = function(req,res){
    var id = req.params.id ? req.params.id : null;
    getStoreWrapper(id, req.query.shopId, function(err, body){
        if(err){
            return res.status(500).json(err);
        }
        if(body.error){
            return res.status(400).json({error: body.error, message: body.message});
        }

        return res.status(200).json(body);
    })
}

module.exports.createStore = function(req,res){
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }
        countr.callApi({
            url: 'stores/',
            method: "POST"
        }, req.body.shopData, tokens.accessToken, function(err, store){
            if(err){
                console.error("Error creating store", err);
                return res.status(500).json(err);
            }
            return res.status(200).json({message: "Shop created", store: store});
        })
    })
}

//#endregion

//#region Customers
function getCustomerWrapper(id, shopId, callback){
    countr.accessToken(shopId, null, null, function(err, tokens){
        if(err){
            return callback(err);
        }
        if(id){
            countr.callApi({
                url: "customers/" + id
            }, null, tokens.accessToken, function(err, customer){
                return callback(err, customer);
            })
        }else{
            countr.callApi({
                url: "customers"
            }, null, tokens.accessToken, function(err, customers){
                return callback(err, customers);
            })
        }
    })
}
module.exports.getCustomers = function(req, res){
    var id = req.params.id ? req.params.id : null;
    getCustomerWrapper(id, req.query.shopId, function(err, body){
        if(err){
            return res.status(500).json(err);
        }
        if(body.error){
            return res.status(400).json({error: body.error, message: body.message});
        }

        return res.status(200).json(body);
    })
}
/** Wrapper for createCustomer */
module.exports.newCustomer = createCustomer;

function createCustomer(customer, access_token, callback){
    countr.callApi({
        url: 'customers',
        method: "POST"
    }, customer, access_token, function(err, customer){
        return callback(err, customer);
    })
}

function createOrUpdateCustomer(customer, access_token, callback){
    if(customer.countrId){
        //countrId is present, we should check if there is really an customer with this id
        countr.callApi({
           url: 'customers/' + customer.countrId 
        }, null, access_token, function(err, countrCustomer){
            if(err && err.error == 'not_found'){
                
                createCustomer(customer, access_token, function(err, customer){
                    return callback(err, customer);
                })
            }else if(err){
                console.error("Error while getting customer", err);
                return callback(err);
            }else{
                //Update the exisiting customer
                countr.callApi({
                    url: 'customers/' + customer.countrId,
                    method: 'PATCH'
                }, customer, access_token, function(err, result){
                    return callback(err, result);
                })
            }
        })
    }else{
        delete customer.countrId;
        createCustomer(customer, access_token, function(err, customer){
            return callback(err, customer);
        })
    }
}

module.exports.syncCustomers = function(req, res){
    console.log("syncCustomers");

    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        Shop.findOne({_id: req.body.shopId}).exec(function(err, shop){
            if(err || !shop){
                var error = {
                    code: err ? 500 : 404,
                    msg: err ? err : "Shop not found"
                }
                return res.status(err).json({message: msg});
            }

            crmUser.find({shopSlug: shop.nameSlug}).exec(function(err, crmusers){
                if(err){
                    return res.status(500).json({message: err});
                }
                if(!crmusers){
                    return res.status(404).json({message: "No CRM Users found"})
                }
                countr.callApi({url: 'me'}, null, tokens.accessToken, function(err, merchant){                
                    async.forEachOf(crmusers, function(crmuser, key, userDone){

                        User.findOne({_id: crmuser.user.userId}).exec(function(err, user){
                            var countrCustomer = {
                                merchant: merchant._id,
                                first_name: crmuser.emailName,
                                email: crmuser.email,
                                countrId: crmuser.countrId ? crmuser.countrId : null
                            }

                            if(user){
                                if(user.firstName){
                                    countrCustomer.first_name = user.firstName;
                                }
                                if(user.lastNamePrefix){
                                    countrCustomer.middle_name = user.lastNamePrefix;
                                }
                                if(user.lastName){
                                    countrCustomer.last_name = user.lastName;
                                }
                                if(user.phone[0]){
                                    if(user.phone[0].landLine){
                                        countrCustomer.phone = user.phone[0].landLine;
                                    }
                                    if(user.phone[0].mobilePhone){
                                        countrCustomer.mobile = user.phone[0].mobilePhone;
                                    }
                                }
                            }

                            createOrUpdateCustomer(countrCustomer, tokens.accessToken, function(err, result){
                                if(result){
                                    crmuser.countrId = result._id;
                                    crmuser.save(function(err, crmuser){

                                    });
                                }
                                userDone();
                            })
                        })

                    }, function(){
                        return res.status(200).json({message: "Customers synced!"});
                    })
                })
            })
        })
    })
}
//#endregion

//#region Products
module.exports.getProductCount = function(req, res){
    countr.accessToken(req.query.shopId, null, null, function(err, tokens){
        if(err){
            callback(err);
        }
        countr.callApi({
            url: 'products/count'
        }, null, tokens.accessToken, function(err, count){
            if(err){
                return res.status(500).json({err})
            }
            return res.status(200).json({product_count: count})
        })
    })
}



function createProduct(shopProduct, product, shop, tax, uncategorized, merchant, lang, access_token, callback){
    async.waterfall([
        countrGetCategories,
        shopGetCategories,
        handleProduct,
        postProduct,
        patchProduct
    ], function(err, result){
        return callback(err, result);
    })

    function countrGetCategories(cb){
        getCountrCategories(shop.countr.countrId, access_token, function(err, countrCategories){
            cb(err, countrCategories);
        })
    }

    function shopGetCategories(countrCategories, cb){
        getCategories(shop, shopProduct._id, countrCategories, lang, uncategorized, access_token, function(err, shopCategories){
            cb(err, shopCategories);
        })
    }

    function handleProduct(shopCategories, cb){
        var variants = [], variantsCounter =0;

        for(var i=0; i < product.variants.length; i++){
            var variant = {
                name: product.variants[i].productNumber,
                price: (shopProduct.price - shopProduct.discount) * 1.21 || 0,
                ean: product.variants[i].ean,
                sku: product.variants[i].productNumber,
                tax: tax
            }
            variants.push(variant);
            variantsCounter++;

            if(variantsCounter == product.variants.length){
                var newProduct = {
                    merchant: merchant,
                    name: product[lang].name,
                    brand: product.brand.name,
                    image: product.images.length > 0 ? bucketLocation() + product.images[0].src : null,
                    visible: true,
                    ledger: product.category,
                    subledger: product.brand.name,
                    categories: shopCategories,
                    variants: variants,
                    options: {
                        stock: "self"
                    }
                }
                cb(null, newProduct);
            }
        }
    }

    function postProduct(product, cb){
        countr.callApi({
            url: 'products',
            method: 'POST'
        }, product, access_token, function(err, countrProduct){
            if(err){
                return cb(err);
            } 

            var inventory = [
                {
                    store: shop.countr.countrId,
                    units: {}
                }
            ]
            for(var i=0; i< countrProduct.variants.length; i++){

                inventory[0].units[countrProduct.variants[i]._id] = shopProduct.stock;

                if(Object.keys(inventory[0].units).length == countrProduct.variants.length){
                    
                    var updateProduct = {
                        inventory: inventory
                    }
                    return cb(null, updateProduct, countrProduct);
                }
            }

        })
    }

    function patchProduct(product, countrProduct, cb){
        countr.callApi({
            url : 'products/' + countrProduct._id,
            method: 'PATCH'
        }, product, access_token, function(err, updatedProduct){
            return cb(err, updatedProduct);
        })
    }
}

function createOrUpdateProduct(shopProduct, shop, tax, uncategorized, merchant, countrCategories, lang, access_token, callback){
    async.waterfall([
        findProduct,
        checkProduct,
    ], function(err, result){
        return callback(err, result);
    })

    function findProduct(cb1){
        Product.findOne({_id: shopProduct._id}).exec(function(err, product){
            if(err || !product){
                cb1(err ? err : "No product found");
            }else{
                cb1(null, product);
            }
        })
    }

    function checkProduct(product, cb1){
        console.log(chalk.green("checkProduct"), shopProduct.countrId);
        if(!shopProduct.countrId){
            createProduct(shopProduct, product, shop, tax, uncategorized, merchant, lang, access_token, function(err, countrProduct){
                if(err){
                    cb1(err);
                }else{
                    shopService.updateCountrIdOnShopProduct(shop._id, shopProduct._id, countrProduct._id, function(updateErr, result){
                       if(updateErr){
                           cb1(updateErr);
                       }else{
                           cb1(null, countrProduct);
                       }
                    })
                }
            })
        }else{
            async.waterfall([
                countrGetProduct,
                handleCountrProduct
            ], function(err, result){
                cb1(err, result);
            })

            function countrGetProduct(cb2){
                countr.callApi({
                    url: 'products/' + shopProduct.countrId
                }, null, access_token, function(err, countrProduct){
                    if(err && err.error == 'not_found'){
                        cb2(null, "not_found");
                    }else if(err){
                        cb2(err);
                    }else{
                        cb2(null, countrProduct);
                    }
                    
                })
            }

            function handleCountrProduct(countrProduct, cb2){
                if(countrProduct == "not_found"){
                    createProduct(shopProduct, product, shop, tax, uncategorized, merchant, lang, access_token, function(createErr, newProduct){
                        if(createErr){
                            cb2(createErr);
                        }else{
                            shopService.updateCountrIdOnShopProduct(shop._id, shopProduct._id, newProduct._id, function(updateErr, result){
                                if(updateErr){
                                    cb2(updateErr)
                                }else{
                                    cb2(null, newProduct);
                                }
                                
                            })
                        }                        
                    })
                }else{
                    //Update the product, start a new waterfall
                    async.waterfall([
                        categoriesGet,
                        variantsLoop,
                        patchProduct,
                        updateStock
                    ], function(err, result){
                        cb2(err, result);
                    })

                    function categoriesGet(cb3){
                        getCategories(shop, shopProduct._id, countrCategories, lang, uncategorized, access_token, function(err, categories){
                            if(err){ 
                                cb3(err)
                            }else{
                                cb3(null, categories)
                            }

                        })
                    }
                    function variantsLoop(categories, cb3){
                        var variants =[], variantsCounter = 0;
                        for(var i=0; i< product.variants.length; i++){
                           
                            var variant = {
                                name: product.variants[i].productNumber,
                                price: (shopProduct.price - shopProduct.discount) * 1.21,
                                ean: product.variants[i].ean,
                                sku: product.variants[i].productNumber,
                                tax: tax
                            }

                            variants.push(variant);
                            variantsCounter++;
                            if(variantsCounter == product.variants.length){
                                cb3(null, categories, variants);
                            }

                        }

                    }
                    function patchProduct(categories, variants, cb3){
                        var updatedProduct = {
                            _id: shopProduct.countrId,
                            merchant: merchant,
                            name: product[lang].name,
                            brand: product.brand.name,
                            image: bucketLocation() + product.images[0].src,
                            visible: true,
                            ledger: product.category,
                            subledger: product.brand.name,
                            categories: categories,
                            variants: variants,
                            options: {
                                stock: "self"
                            }
                        }

                        countr.callApi({
                            url: 'products/' + shopProduct.countrId,
                            method: 'PATCH'
                        }, updatedProduct, access_token, function(err, updatedProduct){
                            if(err){
                                cb3(err);
                            }else{
                                cb3(null, updatedProduct);
                            }
                        })
                    }

                    function updateStock(updatedProduct, cb3){
                        var stock = shopProduct.stock;
                        var countrId = shop.countr.countrId;

                        var inventory = [
                            {
                                store: countrId,
                                units: {}
                            }
                        ]
                        var variantStockCounter = 0;
                        for(var i=0; i< updatedProduct.variants.length; i++){

                            var variantStock = {}
                            variantStock[updatedProduct.variants[i]._id] = parseInt(stock);
                            
                            variantStockCounter++;
                            if(variantStockCounter == updatedProduct.variants.length){
                                inventory[0].units = Object.assign(variantStock, inventory[0].units);

                                updatedProduct.inventory = inventory

                                countr.callApi({url: 'products/'+ updatedProduct._id, method: 'PATCH'}, updatedProduct, access_token, function(err, updatedProduct){
                                    shopService.updateCountrIdOnShopProduct(shop.id, shopProduct._id, updatedProduct._id, function(err, result){ 
                                        cb3(err, updatedProduct);
                                    })                                                        
                                })
                            }
                        }
                    }
                }
            }
        }
    }
}

module.exports.syncProducts = function(req, res){
    var lang = req.body.lang;

    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }

        countr.callApi({
            url: 'me'
        },null, tokens.accessToken, function(err, merchant){
            if(err){
                return res.status(500).json({message: err});
            }


            res.status(200).json({message: "Product sync is started, this can take a while, but it's processing in the background."})

            productSync(lang, req.body.shopId, merchant, tokens.accessToken, function(err, result){
                console.log("PRODUCTSYNC");
                console.log(err, result);
            })
        })
    })
}

/**
 * Product synchronisation function
 * @param {string} language - The language (nl, en, de, fr or es)
 * @param {string} shopId - The shopId 
 * @param {object} merchant - The Countr Merchant Object
 * @param {string} access_token - The accessToken for Countr
 * @param {object} productCb - Callback with err or result
 */
function productSync(language, shopId, merchant, access_token, productCb){
    var merchantId = merchant._id;

    Shop.findOne({_id: ObjectId(shopId)}).exec(function(err,shop){
        if(err || !shop){
            return productCb(err ? err : "No shop found");
        }

        async.waterfall([
            getTax,
            countrGetUncategorized,
            shopCategories,
            productLoop
            
        ], function(err, result){
            if(err){
                return productCb(err);
            }else{
                return productCb(null, result);
            }
        })

        function getTax(cb){
            countr.callApi({
                url: 'taxes/' + shop.countr.taxId 
            }, null, access_token, function(err, tax){
                if (err){
                    cb(err)
                }else{
                    cb(null, tax);
                }
            })
        }

        function countrGetUncategorized(tax, cb){
            countr.getUncategorizedCategory(language, access_token, function(err, uncategorized){
                if(err){
                    cb(err)
                }else{
                    cb(null, tax, uncategorized);
                }
            })
        }

        function shopCategories(tax, uncategorized, cb){
            getCountrCategories(shop.countr.countrId, access_token, function(err, categories){
                if(err){
                    cb(err)
                }else{
                    cb(null, tax, uncategorized, categories);
                }
                
            })
        }

        function productLoop(tax, uncategorized, categories, cb){
            async.forEachOfSeries(shop.products, function(shopProduct, key, shopProductsDone){
                createOrUpdateProduct(shopProduct, shop, tax, uncategorized, merchantId, categories, language, access_token, function(err, productResult){
                    if(err){
                        shopProductsDone(err);
                    }else{
                        shopService.updateCountrIdOnShopProduct(shop._id, shopProduct._id, productResult._id, function(err, updateResult){
                            if(err){
                                shopProductsDone(err)
                            }else{
                                shopProductsDone(null, updateResult);
                            }
                        })
                    }
                })
            }, function(err, result){
                //shopProductsDone
                if(err){
                    cb(err);
                }else{
                    cb(null, result)
                }
            })
        }
    })             
}

module.exports.getSpecificStoreItem = function(req,res){
    console.log("getSpecificStoreItem");

    countr.accessToken(req.query.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }
        countr.getSpecificStoreItem(req.params.id, req.params.item, tokens.accessToken, function(err, response, body){
            
            if(err){
                console.error(err);
                return res.status(500).json(err);
            }
            if(body.error){
                console.error("Error getting " + req.params.item, body);
                return res.status(400).json({error: body.error, message: body.message, details: body.details ? body.details : null});
            }

            return res.status(200).json(body);
        })
    })
}

function makeProductObject(countrProduct, language, user, productCb){
    var product = {};

    function getBrand(brandName, brandCb){
        Brand.findOne({name: brandName}).exec(function(err, brand){
            if(err){
                return brandCb(err);
            }
            if(!brand){
                return brandCb("Brand not found");
            }
            return brandCb(null, brand);
        })
    }

    function determineProductCategory(name, categoryCb){
        //When one of these words is found in the productname, the product will be saved as 'watch'.
        //If not, the product will be saved as 'other'
        var watchNames = [
            'horloge',
            'watch',
            'aufpassen',
            'veille',
            'vigilar'
        ]

        var category = "OTHER", counter =0;

        for(var i=0; i < watchNames.length; i++){
            if(name.includes(watchNames[i])){
                category = "WATCH" 
            }

            counter++;
            if(counter == watchNames.length){
                return categoryCb(category);
            }
        }
    }

    function getProductVariants(countrVariants, variantCb){
        var variants = [], counter =0;;

        for(var i = 0; countrVariants.length; i++){
            var variant = {
                productNumber: countrVariants[i].sku,
                ean: countrVariants[i].ean,
            }
            variants.push(variant);
            counter++;

            if(counter == countrVariants.length){
                return variantCb(variants);
            }
        }
    }

    function handleImage(image, imageCb){
        upload.uploadStream(image, 'products', null, function(err, data){
           return imageCb(err, data); 
        })

    }

    var nameSlug = str.generateNameSlug(countrProduct.name)
    product.name = countrProduct.name;
    product.nameSlug = nameSlug;

    product[language] = {};
    product.en = {};
    product[language].name = countrProduct.name;
    product[language].nameSlug = nameSlug;  
    product.en.nameSlug = nameSlug;

    product.uploader = {
        _id: user._id,
        name: user.firstName + ' ' + (user.lastNamePrefix ? user.lastNamePrefix : '') + ' ' + user.lastName
    };

    product.suggestedRetailPrice = countrProduct.price;

    
    getBrand(countrProduct.brand, function(err, brand){
        if(err){
            return productCb(err);
        }
        product.brand = {
            _id: brand._id,
            name: brand.name,
            nameSlug: brand.nameSlug
        }

        determineProductCategory(countrProduct.name, function(category){
            product.category = category;

            getProductVariants(countrProduct.variants, function(variants){
                product.variants = variants;
                
                handleImage(countrProduct.image, function(err, result){
                    product.images = [
                        {
                            src: result,
                            alt: countrProduct.name
                        }
                    ]

                    return productCb(null, product);
                })
                
            }) 
        })
    })
}

function getOrCreateProduct(countrProduct, language, user, getCreateCb){
    function getStock(inventory, stockCb){

        var stock =0, counter =0;;

        for(var i=0; i < inventory.length; i++){
            for(var key in inventory[i].units){
                stock = stock + parseFloat(inventory[i].units[key]);
                
                counter++;
                if(counter == Object.keys(inventory[i].units).length){
                    return stockCb(stock);
                }
            }
        }
    }

    Product.findOne({'variants.ean': countrProduct.ean}).exec(function(err, product){

        if(err){
            return getCreateCb(err);
        }
        if(!product){
            makeProductObject(countrProduct, language, user, function(err, product){
                if(err){
                    return getCreateCb(err);
                }
                create(product, function(parsedProduct){
                    Product.create(parsedProduct, function (err, product) {
                         //The stock isn't saved on the product itself (only on shopproducts) but we are saving it here to easily get the stock when creating a shopproduct
                        getStock(countrProduct.inventory, function(stock){
                            product.stock = stock;
                            return getCreateCb(err, product);
                        })
                        
                    })
                })
            })
        }else{
            getStock(countrProduct.inventory, function(stock){
                product.stock = stock;
                return getCreateCb(null, product);
            })
        }
    
    })
}

//get products from countr and add to the pm shop
module.exports.addProductsFromCountr = function(req, res){
    console.log("addProductsFromCountr");
    countr.accessToken(req.query.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }

        Shop.findOne({_id: req.query.shopId}).exec(function(err, shop){
            if(err){
                return res.status(500).json({message: "Error getting shop or shop not found"})
            }
            countr.callApi({url: 'products'}, null, tokens.accessToken, function(err, products){
                if(err){
                    return res.status(500).json({message: err})
                }
                var shopProducts = [];

                async.forEachOfSeries(products, function(product, key, productsDone){
                    getOrCreateProduct(product, req.query.language, req.user, function(err, prod){
                       //TODO: check why new products aren't added to the shop
                        if(err){
                            console.error(err);
                            return productsDone();
                        }

                        if(!prod.suggestedRetailPrice){
                            prod.suggestedRetailPrice = 0;
                        }

                        shopProducts.push({
                            countrId: product._id, //Id from countrproduct
                            stock: prod.stock,
                            price: prod.suggestedRetailPrice,
                            show: true,
                            discount: 0,
                            _id: prod._id
                        });
                        productsDone();
                    })
                }, function(){
                    shop.products = shopProducts;

                    shop.save(function(err, shop){
                        return res.status(200).json({message: 'OK'});
                    })
                })
            })
        })
    })
}


module.exports.updateProduct = function(req, res){
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            console.error("Error getting tokens", err);
            return res.status(err.code).json(err.message)
        }
        var product = req.body.product;
        var language = req.body.language;

        Shop.findOne({_id: ObjectId(req.body.shopId)}).exec(function(err, shop){
            if(err || !shop){
                if(err){
                    return res.status(500).json({message: err});
                }else{
                    return res.status(404).json({message: "No shop found"});
                }
            }
            var shopProduct = _.find(shop.products, {'_id': ObjectId(product._id)});

            if(!shopProduct){
                return res.status(500).json({err: "error getting product"});
            }
            if(shopProduct.countrId){
                //Product is already synced

                async.waterfall([
                    countrGetUncategorized,
                    countrGetTaxes,
                    countrGetMe,
                    countrGetCategories,
                    createUpdateProduct
                ], function(err, product){
                    if(err){
                        return res.status(500).json({message: err})
                    }else{
                        return res.status(200).json({message: "Product synced", product});
                    }
                });

                function countrGetUncategorized(cb){
                    countr.getUncategorizedCategory(language, tokens.accessToken, function(err, uncategorized){
                        cb(err, uncategorized);
                    })
                }

                function countrGetTaxes(uncategorized, cb){
                    countr.callApi({url: 'taxes/' + shop.countr.taxId}, null, tokens.accessToken, function(err, tax){
                        cb(err, uncategorized, tax);
                    })
                }

                function countrGetMe(uncategorized, tax, cb){
                    countr.callApi({url: 'me'}, null, tokens.accessToken, function(err, me){
                        cb(err, uncategorized, tax, me);
                    })
                }
                
                function countrGetCategories(uncategorized, tax, me, cb){
                    getCountrCategories(shop.countr.countrId, tokens.accessToken, function(err, categories){
                        cb(err, uncategorized, tax, me, categories);
                    })
                }

                function createUpdateProduct(uncategorized, tax, me, categories, cb){
                    createOrUpdateProduct(shopProduct, shop, tax, uncategorized, me, categories, language, tokens.accessToken, function(err, product){
                        cb(err, product)
                    })
                }
            }else{
                //create product
                var shopProduct = _.find(shop.products, {_id: ObjectId(product._id)});

                if(shopProduct){
                    Product.findOne({_id: product._id}).exec(function(err, productResult){
                        countr.getUncategorizedCategory(language, tokens.accessToken, function(err, uncategorized){
                            
                            countr.callApi({url: 'taxes/' + shop.countr.taxId}, null, tokens.accessToken, function(err, tax){
                                
                                countr.callApi({url: 'me'}, null, tokens.accessToken, function(err, merchant){
                                
                                    createProduct(shopProduct, productResult, shop, tax, uncategorized, merchant, language, tokens.accessToken, function(err, result){
                                        if(err){
                                            return res.status(500).json({message: err})
                                        }else{
                                            var index = _.findIndex(shop.products, {_id: ObjectId(product._id)});
                                            shopService.updateCountrIdOnShopProduct(shop._id, shop.products[index]._id, result._id, function(err, updateResult){
                                                if(err){
                                                    return res.status(500).json({message: err})
                                                }else{
                                                    return res.status(200).json({message: "Product created", product: result});
                                                }
                                            })                                            
                                        }
                                    })
                                })// callApi: me
                            })// callApi: taxes
                        })// callApi: uncategorized
                    })//product findOne
                }else{
                    return res.status(500).json({err: 'Error getting product'});
                }
            }
        })//shop findOne
    })//accessToken
}

module.exports.getProduct = function(req, res){
    console.log("getProduct");
    countr.accessToken(req.query.shopId, null, null, function(err, tokens){

        if(err){
            return res.status(err.code).json(err.message)
        }

        countr.callApi({
            url: 'products/' + req.query.productId
        }, null, tokens.accessToken, function(err, product){
            if(err){
                return res.status(500).json(err)
            }
            return res.status(200).json({product});
        })
    })
}

/**
 * Deletes all products from a particular shop, only available for admin users
 */
module.exports.deleteAllProducts = function(req, res){
    console.log("[deleteAllProducts");
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            return res.status(err.code).json(err.message);
        }

        countr.callApi({
            url: 'stores/' + req.body.storeId + '/products'
        }, null, tokens.accessToken, function(err, products){
            if(err){
                return res.status(500).json(err);
            }
            var counter = 0;
            products.forEach(function(product){
                countr.callApi({
                    url: 'products/' + product._id,
                    method: 'DELETE'
                }, {limit: 1000}, tokens.accessToken, function(err, deleteResult){
                    if(err){
                        console.log("Error while deleting product: ", product.name, err);
                        return;
                    }
                    console.log(product.name, chalk.red("deleted"));
                    counter++;
                    if(counter == products.length){
                        return res.status(200).json({message: products.length + " products deleted!"})
                    }
                })
            })
        })
    })
}

//#endregion

//#region Webhooks
/**
 * Create a webhook 
 * @param {string} merchant - The merchant ID from Countr
 * @param {string} topic - The topic to which to subscribe
 * @param {sting} shopId - The PrismaNote shopId as a string 
 */
function newWebhook(merchantId, topic, shopId, access_token, callback){
    var webhook = {
        url:  settings.countr.hookUrl + topic + "/" + shopId,
        user: merchantId,
        topic: topic
    }
    countr.callApi({
        url: 'webhooks',
        method: 'POST'
    }, webhook, access_token, function(err, result){
        return callback(err, result);
    })
}

module.exports.createWebhook = function(req,res){
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            return res.status(500).json({message: "Error getting tokens", err});
        }
        countr.callApi({
            url: 'me'
        }, null, tokens.accessToken, function(err, merchant){
            newWebhook(merchant._id, req.body.topic, req.body.shopId.toString(), tokens.accessToken, function(err, webhook){
                if(err){
                    return res.status(500).json(err)
                }
                return res.status(200).json({message: "Webhook created", webhook})
            })
        })
    })
}

module.exports.getWebhooks = function(req, res){
    console.log("getWebhooks");
    countr.accessToken(req.query.shopId, null, null, function(err, tokens){
        if(err){
            return res.status(500).json({message: "Error getting tokens", err});
        }

        countr.callApi({
            url: 'webhooks'
        }, null, tokens.accessToken, function(err, webhooks){
            if(err){
                return res.status(500).json(err);
            }
            return res.status(200).json(webhooks);
        })
    })
}

module.exports.deleteWebhook = function(req, res){
    countr.accessToken(req.params.shopId, null, null, function(err, tokens){
        if(err){
            return res.status(500).json({message: "Error getting tokens", err});
        }
        countr.callApi({
            url: 'webhooks/' + req.params.id ,
            method: 'DELETE'
        }, null, tokens.accessToken, function(err, result){
            if(err){
                return res.status(500).json(err);
            }
            return res.status(200).json({message: "Webhook deleted"})
        })
    })
}

module.exports.webhookSetup = function(req,res){
    console.log("webhookSetup");
    //Set all webhooks 
    let shopId = req.body.shopId;
    countr.accessToken(shopId, null, null, function(err, tokens){
        if(err){
            return res.status(500).json({message: "Error getting tokens", err});
        }
        countr.callApi({
            url: 'me'
        }, null, tokens.accessToken, function(err, merchant){
            if(err){
                return res.status(500).json({message: "Error getting merchant", err});
            }
            var hooks = [
                'customer.created',
                'transaction.created'
            ]

            var results = [], errors = [], counter=0;
            for(var i = 0; i < hooks.length; i++){
                newWebhook(merchant._id, hooks[i], shopId, tokens.accessToken, function(err, webhook){
                    if(err){
                        errors.push(err);   
                    }else{
                        results.push(webhook);
                    }
                    counter++;
                    if(counter == hooks.length){
                        if(errors.length > 0 && results.length == 0){
                            return res.status(500).json(errors);
                        }else if(results.length > 0 && errors.length > 0){
                            return res.status(206).json(results, errors);
                        }else{
                            return res.status(200).json(results);
                        }
                    }
                })
            }
        })
    })
}
function checkTransaction(transaction, shopSlug, updateStock, callback){
    //check if there is no transaction with the receipt_id
    crmUser.findOne({'items.transactionDetails.receiptNumber': transaction.receipt_id}).exec(function(err, transactionResult){
        if(err){
            return callback(err);
        }
        if(!transactionResult){
            if(transaction.customer){
                handleNewTransaction(transaction, shopSlug, updateStock, function(err, result){
                    return callback(err, result);
                })
            }else{
                //No customer, but the stock must be updated when true
                if(updateStock){
                    var counter =0;
                    shopService.getShopFromSlug(shopSlug, function(err, shop){
                        if(shop){
                            for(var i =0; i < transaction.items.length; i++){
                                var transactionItem = transaction.items[i];

                                var product = _.find(shop.products, {countrId: transactionItem.product._id});
                                if(product){
                                    shopService.updateStock(shop._id, product._id, transactionItem.amount, true);
                                }

                                counter++;
                                if(counter == transaction.items.length){
                                    return callback(null, "Stock updated");
                                }
                            }
                        }
                    })                    
                }else{
                    return callback(null, "Nothing to do, because there isn't a customer and the stock doesn't have to be updated")
                }
            }
        }else{
            return callback(null, "nothing to do");
        }
    })
}

function handleNewTransaction(transaction, shopSlug, updateStock, callback){
    var items = [];
    var productId;
    shopService.getShopFromSlug(shopSlug, function(err, shop){
        for(var i=0; i < transaction.items.length; i++){
            var transactionItem = transaction.items[i];
            if(updateStock){
                if(shop){
                    var product = _.find(shop.products, {countrId: transactionItem.product._id});
                    if(product){
                        shopService.updateStock(shop._id, product._id, transactionItem.amount, true);
                        productId = product._id;
                    }
                }
            }
            var item = {
                productId: productId ? productId : null,
                amount: transactionItem.amount,
                name: transactionItem.product.name,
                price: transactionItem.product.current_variant.price,
                photo: {
                    src: transactionItem.product.image,
                    alt: transactionItem.product.name
                }
            }
            items.push(item);

            if(items.length == transaction.items.length){
                var activity = {
                    productTypeId: 4,
                    fillindate: Date.now(),
                    fillinLanguage: "nl",
                    priceRange: transaction.paid,
                    transactionDetails: {
                        countrId: transaction._id,
                        receiptNumber: transaction.receipt_id,
                        items: items
                    }
                }

                crmUser.findOne({countrId: transaction.customer._id}).exec(function(err, crmuser){
                    if(crmuser){
                        crmuser.items.push(activity);

                        crmuser.save(function(err, result){
                            return callback(err, result);
                        });
                    }else{
                        handleNewCustomerWebhook(transaction.customer, shopSlug, function(err, user, crmuser){
                            if(crmuser){
                                crmuser.items.push(activity);

                                crmuser.save(function(err, result){
                                    return callback(err, result);
                                })
                            }else{
                                console.error(err, user, crmuser);
                                return callback(err);
                            }
                        })
                    }
                    
                })
            }
        }
    })
}

function handleNewCustomerWebhook(user, shopSlug, callback){
    //First look if there is an user with this emailadress
    searchOrAddUser(user, function(err, userResult){
        if(err){
            return callback(err);
        }
        searchOrCreateCrmUser(userResult, user, shopSlug, function(err, crmResult){
            if(err){
                return callback(err);
            }

            return callback(null, userResult, crmResult)
        })
    })
}

function searchOrAddUser(countrUser, callback){

    User.findOne({email: countrUser.email}).exec(function(err, userResult){

        if(err){
            return callback(err);
        }
        if(!userResult){
            //create a user
            var address = [];
            if(countrUser.billing){
                var billing = {
                    street: countrUser.billing.address1,
                    houseNumber: countrUser.billing.number,
                    postalCode: countrUser.billing.zip,
                    city: countrUser.billing.city,
                    country: countrUser.billing.country
                };
                address.push(billing);
                if(countrUser.shipping && countrUser.shipping.address1 != countrUser.billing.address1){
                    var shipping = {
                        street: countrUser.shipping.address1,
                        houseNumber: countrUser.shipping.number,
                        postalCode: countrUser.shipping.zip,
                        city: countrUser.shipping.city,
                        country: countrUser.shipping.country
                    };
                    address.push(shipping);
                }
            }
            
            var phonenumber = [];
            if(countrUser.phone){
                var phone = new PhoneNumber(countrUser.phone, countrUser.billing.country.toUpperCase() || "NL");
                if(phone.isValid()){
                    phonenumber = [{
                        countryCode: phone.getRegionCode(),
                        mobilePhone: phone.isMobile() ? phone.getNumber() : null,
                        landLine: phone.isMobile() ? null : phone.getNumber(),
                        fax: null
                    }]
                }
            }

            var user = {
                firstName: countrUser.first_name,
                lastName: countrUser.last_name,
                password: random(10),
                email: countrUser.email,
                username: countrUser.email,
                address: address,
                phone: phonenumber
            }

            User.register(user, function(err, user){
                return callback(err, user);
            })
        }else{
            return callback(null, userResult);
        }
    })
}

function searchOrCreateCrmUser(user, customer, shopSlug, callback){
    crmUser.findOne({email: user.email, shopSlug: shopSlug}).exec(function(err, crmuser){
        if(err){
            return callback(err);
        }
        if(!crmuser){
            //create a crmuser
            var newUser = {
                user: {
                    userId: user._id,
                    name: user.firstName + " " + (user.lastNamePrefix ? user.lastNamePrefix : "") + " " + user.lastName,
                },
                shopSlug: shopSlug,
                email: user.email,
                emailName: user.firstName,
                countrId: customer._id
            }

            create(newUser, function(newCrmUserItem){
                crmUser.create(newCrmUserItem, function(err, crmuser){
                    return callback(err, crmuser);
                })
            })
        }else{
            //user already exists, let check and update some thing if needed
            if(crmuser.countrId && crmuser.countrId == customer._id){
                return callback(null, crmuser);
            }else{
                crmuser.countrId = customer._id;
                crmuser.save(function(err, result){
                    if(err){
                        return callback(err);
                    }
                    return callback(null, result);
                })
            }
        }
    })
}

module.exports.handleWebhook = function(req,res){
    var shopId = ObjectId(req.params.shopid);
    console.info("Countr webhook " + chalk.blueBright(req.params.topic) + " incoming", "params", req.params, "body", req.body);

    Shop.findOne({_id: shopId}).exec(function(err, shop){
        if(err || !shop){
            return sendResult();
        }
        switch (req.params.topic) {
            case 'customer.created':
                //new CRM user
                handleNewCustomerWebhook(req.body, shop.nameSlug, function(err, userResult, crmResult){
                    console.log(req.params.topic + " handled");
                    if(err){
                        console.error(err);
                    }
                    return sendResult();
                })
                break;
            case 'transaction.created': 
                var transaction = req.body;
                checkTransaction(req.body, shop.nameSlug, true, function(err, result){
                    console.log(req.params.topic + " handled");
                    if(err){
                        console.error(err);
                    }
                    return sendResult();
                })
                                
                break;
            default:
                console.error("Topic " + req.params.topic + " is not implemented yet!");
                return sendResult();
                break;
        }
    })
    
    function sendResult(){
        //always send OK to countr, because it can't handle errors
        return res.status(200).json({message: "OK"})
    }

}
//#endregion

//#region Transactions
module.exports.getTransactions = function(req, res){
    countr.accessToken(req.query.shopId, null, null, function(err, tokens){
        if(err){
            return res.status(500).json({message: "Error getting tokens", err});
        }

        countr.callApi({
            url: 'transactions'
        }, null, tokens.accessToken, function(err, transactions){
            if(err){
                return res.status(500).json(err);
            }
            return res.status(200).json(transactions);
        })
    })
}

module.exports.syncTransactions = function(req,res){
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            return res.status(500).json({message: "Error getting tokens", err});
        }
        Shop.findOne({_id: req.body.shopId}).exec(function(err, shop){
            if(err){
                return res.status(500).json({message: "Error getting shop", err});
            }
            countr.callApi({
                url: 'transactions'
            }, null, tokens.accessToken, function(err, transactions){
                if(err){
                    return res.status(500).json(err);
                }
                console.log("transactions", transactions);

                async.forEachOf(transactions, function(transaction, key, transactionDone){
                    checkTransaction(transaction, shop.nameSlug, false, function(err, result){
                        transactionDone();
                    })
                }, function(){
                    //transactionDone
                    console.log("transactionDOne");
                    return res.status(200).json({message: "Transactions synced"})
                })
            })
        })

    })
}

module.exports.sendTransactionMail = function(req, res){
    countr.accessToken(req.body.shopId, null, null, function(err, tokens){
        if(err){
            return res.status(500).json({message: 'Error getting tokens', err});
        }
        var body = {
            type: 'email',
            value: req.body.to
        }
        countr.callApi({
            url: 'transactions/' + req.body.transactionId + '/send',
            method: 'POST'
        }, body, tokens.accessToken, function(err, result){
            if(err){
                return res.status(500).json(err);
            }
            return res.status(200).json(result);
        })
    })
}
//#endregion

//#region Signup
module.exports.signup = function(req, res){
    var user = req.body.countr;
    var password = req.body.countr.password;

    var shopId = req.body.shopId;
    var language = req.body.language;

    //This URL doesn't need an accessToken or any other authentication
    countr.callApi({
        url: 'merchants',
        method: "POST"
    }, user, null, function(err, merchant){
        console.error("MERCHANT", merchant);
        if(err){
            var error = err;
            if(err.error == "duplicate_resource"){
                error = {
                    message: "Voor dit emailadres is al een account aanwezig bij Countr",
                    error: err.error
                }
            }
            return res.status(500).json(error);
        }
        //overwrite the hashed password with the plain text, because we need it for the email
        merchant.password = password;


        countr.accessToken(shopId, merchant.username, merchant.password, function(err, tokens){
            if(err){
                return res.status(500).json({message: err});
            }

            

            //since this is a new account, we need to create a shop
            Shop.findOne({_id: ObjectId(shopId)}).exec(function(err, shop){
                if(err){
                    return res.status(500).json({message: err});
                }

                var country = function(value){
                    var result = "nl";
                    switch(value.toLowerCase()){
                        case "nederland":
                        case "netherlands":
                        case "pays-bas":
                        case "paysbas":
                            result = "nl";
                            break;
                        case "belgie":
                        case "belgië":
                        case "belgium":
                        case "belgique":
                            result = "be";
                            break;
                        case "frankrijk":
                        case "france":
                            result = "fr";
                            break;
                        case "duitsland":
                        case "allemagne":
                        case "germany":
                            result = "de";
                            break;
                        case "spanje":
                        case "spain":
                        case "l'espagne":
                        case "l espagne":
                        case "l-espagne":
                            result = "es";
                    }
                    return result;
                }

                var store = {
                    merchant: merchant._id,
                    name: shop.name,
                    address: {
                        address1: shop.address.street + " " + shop.address.houseNumber + " " +(shop.address.houseNumberSuffix ? shop.address.houseNumberSuffix : ""),
                        city: shop.address.city,
                        country: country(shop.address.country),
                        zip: shop.address.postalCode
                    },
                    currency: "EUR",
                    timezone: "CET"
                }
                countr.callApi({
                    url: "stores",
                    method: "POST"
                }, store, tokens.accessToken, function(err, store){
                    if(err){
                        return res.status(500).json({message: err})
                    }
                    shop.countr.countrId = store._id;


                    //create default tax
                    newTax({
                        name: "BTW Hoog",
                        rate: 21,
                        default: true
                    }, tokens.accessToken, function(err, tax){
                        if(err){
                            return res.status(500).json({message: err});
                        }
                        shop.countr.taxId = tax._id;

                        shop.countr.trialExpiresAt = new Date(merchant.trial_expires_at);

                        shop.save(function(err, result){
                            if(err){
                                return res.status(500).json({message: err});
                            }
                            //All required information is know, start sync and send OK response
                            res.status(200).json({message: "Account created, synchronisation started"})

                            productSync(language, shop._id, merchant, tokens.accessToken, function(err, result){
                                //Products are synced, let the user know that everything is done!
                                var options = {
                                    to: merchant.email,
                                    type: 'Countr',
                                    bcc: 'info@prismanote.com'
                                }
                                if(err){
                                    //oh no! something went wrong!
                                    options.subject = "We hebben goed en slecht nieuws..."
                                    options.template = "countr/connection-complete-errors"
                                }else{
                                    options.subject = "Uw gloednieuwe koppeling is klaar!"
                                    options.template = "countr/connection-complete"
                                }

                                var data = {
                                    merchant: merchant,
                                }

                                mailService.mail(options, data, null, function(err, mailResult){
                                    console.log("err", err, "result", mailResult);
                                })


                            })
                        });
                    })
                })
            })
        })
    })
}

//#endregion












