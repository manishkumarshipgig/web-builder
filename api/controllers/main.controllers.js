const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const Brand = mongoose.model('Brand');
const Shop = mongoose.model('Shop');
// var Merchant = mongoose.model('Merchant');
const Translation = mongoose.model('Translation');

module.exports.getIndex = function(req, res) {
	console.log('[getIndex controller]');
	var status, response;

	Product.count({}, function(err, productCount) {
		// .aggregate({$unwind: '$shops'}, {$count: "shopCount"}, function(err, shopCount) {
		Shop.count({}, function(err, shopCount) {
			Brand.count({}, function(err, brandCount) {

				if(err) {
					status = 500;
					response = {message: 'Internal server error. ' + err};
				} else {
					status = 200;
					response = {message: 'OK', productCount: productCount, shopCount: shopCount, brandCount: brandCount};
				}
				
				console.log(status + ' ' + response.message);
				res.status(status).json(response);

			});
		});		
	});

};

module.exports.get501 = function(req, res) {
	console.log('[get501 controller]');

	var status = 501; 
	var response = {message: 'Not implemented. '};

	console.log(status + ' ' + response.message);
	res.status(status).json(response);
}

module.exports.get404 = function(req, res) {
	console.log('[get404 controller]');

	var status = 404;
	var response = {message: 'Page not found. '};

	console.log(status + ' ' + response.message);
	res.status(status).json(response);
};

module.exports.get401 = function(req, res) {
	console.log('[get401 controller]');

	var status = 401;
	var response = {message: 'Unauthorized. Please log in and try again. '};

	console.log(status + ' ' + response.message);
	res.status(status).json(response);
};

module.exports.getTranslations = function(req, res){
	var lang = req.query.lang;
	Translation.find({}, {keyword:1, [lang]:1}).exec(function(err, translations){
		if (translations){
			var result = [];
			for(var i=0; i<translations.length; i++){
				var trans = {
					[translations[i].keyword] : translations[i][lang]
				}
				result.push(trans);
			}
			if(result.length == translations.length) {
				res.status(200).json({message: 'OK', translations: result});
			}
		}
	})
}

module.exports.addMissingTranslation = function(req, res){
	console.log("addMissingTranslation", req.body);

	res.send("Ok");
	//translation.save(function(err, result){
	//	res.send(200);
//	})	
}
