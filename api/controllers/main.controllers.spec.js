const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;

const mainController = require('./main.controllers.js');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const Shop = mongoose.model('Shop');
const Brand = mongoose.model('Brand');
const Translation = mongoose.model('Translation');

describe('Main controllers', function () {

	beforeEach(function() {
		_jsonSpy = this.sandbox.spy()
		_statusStub = this.sandbox.stub().returns({json: _jsonSpy})

		this.res = {
			status: _statusStub,
			json: _jsonSpy
		};
	});
	
	describe('getIndex controller', function () {

		beforeEach(function() {
			this._productCountStub = this.sandbox.stub(Product, 'count').callsArgWith(1, null, 2000);
			this._shopCountStub = this.sandbox.stub(Shop, 'count').callsArgWith(1, null, 3000);
			this._brandCountStub = this.sandbox.stub(Brand, 'count').callsArgWith(1, null, 4000);

			mainController.getIndex(null, this.res);
		});

		it('should return HTTP status code 200: OK', function() {
			expect(_statusStub).to.have.been.calledWith(200);
		});

		it('should return a JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.be.an('object');
		});

		it('should include a status message in the JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.have.property('message').which.is.a('string');
		});

		it('should not send HTTP headers twice', function() {
			expect(_statusStub).to.have.been.calledOnce;
			expect(_jsonSpy).to.have.been.calledOnce;
		});

		it('should return the right product count', function () {
			expect(this._productCountStub).to.have.been.called;
			expect(_jsonSpy.args[0][0]).to.include({productCount: 2000});
		});

		it('should return the right shop count', function () {
			expect(this._shopCountStub).to.have.been.called;
			expect(_jsonSpy.args[0][0]).to.include({shopCount: 3000});
		});

		it('should return the right brand count', function () {
			expect(this._brandCountStub).to.have.been.called;
			expect(_jsonSpy.args[0][0]).to.include({brandCount: 4000});
		});
	});

	describe('get501 controller', function() {

		beforeEach(function() {
			mainController.get501(null, this.res);
		})

		it('should return HTTP status code 501: Not Implemented', function() {
			expect(_statusStub).to.have.been.calledWith(501);
		});

		it('should return a JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.be.an('object');
		});

		it('should include a status message in the JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.have.property('message').which.is.a('string');
		});
	});

	describe('get401 controller', function() {

		beforeEach(function() {
			mainController.get401(null, this.res);
		})

		it('should return HTTP status code 401: Unauthorized', function() {
			expect(_statusStub).to.have.been.calledWith(401);
		});

		it('should return a JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.be.an('object');
		});

		it('should include a status message in the JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.have.property('message').which.is.a('string');
		});
	});

	describe('get404 controller', function() {

		beforeEach(function() {
			mainController.get404(null, this.res);
		})

		it('should return HTTP status code 404: Not Found', function() {
			expect(_statusStub).to.have.been.calledWith(404);
		});

		it('should return a JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.be.an('object');
		});

		it('should include a status message in the JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.have.property('message').which.is.a('string');
		});
	});

	describe('getTranslations controller', function() {
		
		beforeEach(function() {

			const translations = [
				{keyword: 'KEYWORD_ONE', en: 'keyword one', nl: 'keyword één'},
				{keyword: 'KEYWORD_TWO', en: 'keyword two', nl: 'keyword twee'}
			];
			
			this._execStub = this.sandbox.stub().callsArgWith(0, null, translations);
			this._findTranslationStub = this.sandbox.stub(Translation, 'find').returns({exec: this._execStub});

			mainController.getTranslations({query: {lang: 'en'}}, this.res);
		});

		it('should return HTTP status code 200: OK', function() {
			expect(_statusStub).to.have.been.calledWith(200);
		});

		it('should return a JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.be.an('object');
		});

		it('should include a status message in the JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.have.property('message').which.is.a('string');
		});

		it('should try to get translations from the database with the right parameters', function() {
			expect(this._findTranslationStub.args[0][0]).to.deep.equal({});
			expect(this._findTranslationStub.args[0][1]).to.deep.equal({keyword: 1, en: 1});
		});

		it('should return an array of translations in the JSON response object', function() {
			expect(_jsonSpy.args[0][0]).to.have.property('translations').which.is.an('array').which.has.length(2);
			expect(_jsonSpy.args[0][0]['translations'][0]).to.be.an('object').which.has.property('KEYWORD_ONE').which.equals('keyword one');
		});
	});

	describe('addMissingTranslation controller', function() {

	});
});
