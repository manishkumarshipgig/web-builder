const path = require('path');
const import2 = require('./import2.controllers');
const parse = require('csv-parse');
const fs = require('fs');
const csvtojson = require('csvtojson')
const _ = require('lodash');

const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const User = mongoose.model('User');

const str  = require(path.join(__dirname, '..', 'services', 'string.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));

//Step 1: Parse the file, read the columns, and return the first 2 lines, the properties to link and the linecount
module.exports.parseFile = function(req, res){
    console.log("parseFile");

    fs.readFile(req.files.file.path, function(err, data){
        console.log("READING FILE COMPLETED")
        if(err){
            return res.status(500).json({message: 'Internal server error. ' + err});
        }
        parse(data, {delimiter: req.body.delimiter}, function(err, output){
            if(err){
                return res.status(500).json({message: 'Internal server error. ' + err});
            }

            if(checkArray(output[0])){
                console.log("Not a valid file!");
                return res.status(500).json({message: "This file isn't valid! (Check: If there are some rows without a heading, the file is uploaded in CSV format, or other extraordinarily situations)"});
            }else{
                var data = {
                    lines: output.length-1
                }

                if(data.lines > 10000){
                    console.log("File too big");
                    return res.status(500).json({message: "This file is too big! Maximum rows is 1000"});
                }

                var result = output[0];
                return res.status(200).json({message: 'File parsed!', headers: output[0], data: data})
            }
        })
    })
}

function checkArray(my_arr){
    for(var i=0;i<my_arr.length;i++){
        if(my_arr[i] === "")
           return true;
    }
    return false;
 }

//Step 2: Get values for columns which are not can be saved directly
module.exports.getValues = function(req, res){
    console.log("checkFile");
    var column = req.body.column;
    var result = [];
    csvtojson({delimiter: req.body.delimiter})
        .fromFile(req.files.file.path)
        .on('json', (jsonObj)=>{
            var value = _.get(jsonObj, column);

            var exist = result.some(function(data){
                return data.found === value;
            })

           if(!exist){
               result.push({found: value, value: null});
           }
        })
        .on('error', (err)=>{
            console.error("Error during file read:", err);
        })
        .on('done', (error)=>{
            res.status(200).json({message:'OK', result});
        })
}


//Step 3: Parse the file and link the columns to the properties with the given array
module.exports.fileImport = function(req, res){
    console.log("productImport");
    import2.fileImport2(req, res);
/*
    //send an result to the browser to avoid timeouts
    res.status(200).json({message: "Import started"});

    //Save the data
    var headers = req.body.headers;
    var category = req.body.cat;
    var delimiter = req.body.delimiter;
    var customData = req.body.customData;
    var product = req.body.product;
    var totalProducts = req.body.lines;
    var results = [];
    var productCounter =0;
    var productsSaved =0;

    csvtojson({delimiter: delimiter})
        .fromFile(req.files.file.path)
        .on('json', (jsonObj)=>{
            var newProduct = new Product();
            newProduct.en = {}, newProduct.nl = {}, newProduct.fr = {}, newProduct.de = {}, newProduct.es = {};
            newProduct.watch = {}, newProduct.strap, newProduct.jewel = {};
            var line =0;

            async.forEachOfSeries(product, function(value, key, save){
                if(key == "category"){
                    newProduct.category = value;
                    line++;
                    save();
                }else if(key == "gender"){
                    getValueFromCSV(jsonObj, value, key, customData, function(gender){
                        newProduct.male = gender.male;
                        newProduct.female = gender.female;
                        newProduct.kids = gender.kids;
                        line++;
                        save();
                    });
                }else if(key == "variants"){
                    //Fill variant 0 with the know details: EAN, productnumber, sku and size
                    if(typeof value[0] == 'object'){
                        var counter = 0;
                        var variant = {};

                        for(var prop in value[0]){
                            counter++;
                            var val = getValueFromCSV(jsonObj, value[0][prop], prop, customData);
                            variant[prop] = val ? val : null;

                            if(counter == Object.keys(value[0]).length){
                                newProduct.variants.push(variant);
                                line++;
                                save();
                            }
                        }
                    }
                }else if(key == "brand"){
                    var brandName = getValueFromCSV(jsonObj, value, key, customData);

                    Brand.findOne({name: brandName}).exec(function(err, brand){
                        line++;
                        if(err || !brand){
                            var log = err ? "Erroring during finding brand: " + err : "Brand not found: " + brandName;
                            results.push({line: productCounter+1, msg: log});
                            save();
                        }else{
                            newProduct.brand = {
                                _id: brand._id,
                                name: brand.name,
                                nameSlug: brand.nameSlug
                            }
                            save();
                        }
                    })
                }else if(key == "nl" || key == "en" || key == "fr" || key == "es" || key == "de"){
                    var lang = key;
                    var counter =0;
                    for(var prop in value){
                        counter++;
                        var val = getValueFromCSV(jsonObj, value[prop], prop, customData);
                        newProduct[lang][prop] = val;

                        if(counter == Object.keys(value).length){
                            line++;
                            save();
                        }
                    }

                }else if(key == "image1" || key == "image2" || key == "image3"){
                    var imageUrl = getValueFromCSV(jsonObj, value, key, customData);

                    if(imageUrl.indexOf("http") !== -1 || imageUrl.indexOf("https") !== -1){
                        if(imageUrl.indexOf("prismanote.com") !== -1){
                            var url = Url.parse(imageUrl);
                            imageUrl = "https://52.17.102.38" + url.pathname;
                        }

                        upload.uploadStream(imageUrl, 'products', null, function(err, data){
                            line++;
                            if(err){
                                if(err.error && err.error.message){
                                    var error = err.error.message;
                                }else{
                                    var error = err.statusMessage;
                                }

                                console.error("Error while download or upload photo: " + error);
                                results.push({line: productCounter+1, msg: "Error while download or upload photo: " + error});
                                return save();
                            }else{
                                newProduct.images.push({src:data});
                                save();
                            }
                        })
                    }
                }else if(key == "suggestedRetailPrice"){
                    var price = getValueFromCSV(jsonObj, value, key, customData);
                    if(price){
                        newProduct.suggestedRetailPrice = parseFloat(price.replace(',','.'));
                    }
                    line++;
                    save();
                }else if(key == "watch" || key == "jewel" || key == "strap" || key == "other"){
                    var counter =0;
                    for(var prop in value){
                        if(typeof value[prop] == 'object'){
                            for (var val in value[prop]){
                                var result;

                                function getValue(callback){
                                    if(val == "material"){
                                       return callback(getValueFromCSV(jsonObj, value[prop][val], prop + val, customData));
                                    }else{
                                       return callback(getValueFromCSV(jsonObj, value[prop][val], val, customData));
                                    }
                                }
                                getValue(function(result){
                                    Object.assign(newProduct, {
                                        [key]:{
                                            [prop] : {
                                                [val] : result
                                            }
                                        }
                                    })
                                    counter++;
                                })
                            }
                        }else{
                            var result = getValueFromCSV(jsonObj, value[prop], prop, customData);
                            Object.assign(newProduct, {
                                [key]:{
                                    [prop] : result
                                }
                            })
                            counter++;
                        }

                        if(counter == Object.keys(value[prop]).length){
                            line++;
                            save();
                        }
                    }
                }else{
                    newProduct[key] = getValueFromCSV(jsonObj, value, key, customData);
                    line++;
                    save();
                }
            }, function(){
                //save
                if(line == Object.keys(product).length){
                    newProduct.uploader = {
                        _id: req.user._id,
                        name:  req.user.firstName + (req.user.lastNamePrefix ? " " + req.user.lastNamePrefix + " " : " ") + req.user.lastName
                    }
                    //Fill default name
                    if(!newProduct.en.name && !newProduct.nl.name && !newProduct.fr.name && !newProduct.es.name && !newProduct.de.name){
                        newProduct.en.name = newProduct.brand.name + " " + newProduct.variants[0].productNumber;
                    }else{
                        if(newProduct.nl && newProduct.nl.name){
                            newProduct.en.name = newProduct.nl.name;
                        }else if(newProduct.fr && newProduct.fr.name){
                            newProduct.en.name = newProduct.fr.name;
                        }else if(newProduct.es && newProduct.es.name){
                            newProduct.en.name = newProduct.es.name;
                        }else if(newProduct.de && newProduct.de.name){
                            newProduct.en.name = newProduct.de.name;
                        }
                    }
                    if(!newProduct.brand.name || !newProduct.variants[0].productNumber || !newProduct.images[0]){
                        console.log("no brand, productnumber or image");
                        results.push({line: productCounter+1, msg: "No brand, productnumber or image!"});
                        productCounter++;
                        sendResults();
                    }else{
                        determineProductSlug(newProduct.en.name, 0, function(err, slug){
                            if(err){
                                results.push({line: productCounter+1, msg: "Error while generating nameslug: " + err});
                                productCounter++;
                                return sendResults();
                            }
                            newProduct.en.nameSlug = slug;
                            newProduct.save(function(err, res){
                                if(err){
                                    results.push({line: productCounter+1, msg: err});
                                    productCounter++;
                                    sendResults();
                                }else{
                                    productCounter++;
                                    productsSaved++;
                                    sendResults();
                                }
                            })
                        })
                    }
                }else{
                    productCounter++;
                    sendResults();
                }
            })

            function sendResults(){
                console.log("sendResults");
                if(productCounter == totalProducts){
                    console.log("all products done");
                    //All products are done
                    sendMail(results, productsSaved, req.user._id);
                }
            }
        })
    .on('error', (err)=>{
        console.error("Error during file read:", err);
    })
    .on('done', (error)=>{
        console.log("csv done");
    })
    */
}

function getValueFromCSV(jsonObj, value, key, custom, callback){
    try{
        value = JSON.parse(value);
        //check from fields which can not be saved directly and get the right value for the choosen value
        if(key == "gender"){
            var genders = custom.genders;
            var sex = _.get(jsonObj, value.name);
            var result = {
                male: false,
                female: false,
                kids: false
            };
            for(var i = 0; i < genders.length; i++){
                if(genders[i].found == sex){
                    var gen = genders[i].value;

                    if(gen == "GENTS" || gen == "BOYS"){
                        result.male = true;
                        result.kids = gen == "BOYS";
                    }
                    if(gen == "LADIES" || gen == "GIRLS"){
                        result.female = true;
                        result.kids = gen == "GIRLS";
                    }
                    if(gen == "UNISEX" || gen == "KIDS"){
                        result.male = true;
                        result.female = true;
                        result.kids = gen == "KIDS"
                    }
                }
                if(i == genders.length-1){
                    if(typeof callback === 'function'){
                        return callback(result);
                    }else{
                        return result;
                    }
                }
            }
        }else if(key == "casematerial"){
            var caseMat = custom.materials;
            var material = _.get(jsonObj, value.name);

            for(var i=0; i< caseMat.length; i++){
                if(caseMat[i].found == material){
                    return caseMat[i].value;
                }
            }
        }else if(key == "strapmaterial"){
            var strapMat = custom.strapMaterial;
            var material = _.get(jsonObj, value.name);

            for(var i=0; i< strapMat.length; i++){
                if(strapMat[i].found == material){
                    return strapMat[i].value;
                }
            }
        }else{
            return _.get(jsonObj, value.name);
        }
    }catch(e){
        return _.get(jsonObj, value);
    }
}

function determineProductSlug(name, count, callback){
	var nameSlug = str.slugify(name);
	if(count > 0){
		nameSlug = str.slugify(name) + '-' + count;
	}

	Product.find({'en.nameSlug': nameSlug}).sort({_id: -1}).exec(function(err, product){
		if(err){
			return callback(err);
		}
		if(product.length == 0){
			return callback(null, nameSlug);
		}else{
			//count = count+1;
			return determineProductSlug(name, count+1, callback);
		}
	})
}

function sendMail(results, products, userId){
    User.findOne({_id: userId}).exec(function(err, user){
        var data = {
            results: results,
            products: products
        };

        var options = {
            to: user.email,
            subject: 'Uw import is gereed',
            template: 'updates/import-result'
        };

        mailService.mail(options, data, null, function(err, mailResult){

        })
    })
}
