const path = require('path');
const mongoose = require('mongoose');
const async = require("async");
const Mollie = require('mollie-api-node');

const Order = mongoose.model('Order');
const fbOrder = mongoose.model('fbOrder');
const Shop = mongoose.model('Shop');
const User = mongoose.model('User');

const ObjectId = require('mongodb').ObjectId;
const orderService = require(path.join(__dirname, '..', 'services', 'order.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));
const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));

var mollie = new Mollie.API.Client;

mollie.setApiKey(settings.mollie.apiKey);

module.exports.createPayment = function(req, res){
    console.log("CreatePayment", req.body);
    var order = req.body.order;
    var shop = req.body.shop ? req.body.shop : null;
    
    function webhookUrl(facebookOrder){
        if(facebookOrder){
            return process.env.NODE_ENV == 'production' ? req.headers.origin + '/api/fbpayment-webhook' : settings.mollie.url + 'api/fbpayment-webhook';
        }else{
            return process.env.NODE_ENV == 'production' ? req.headers.origin + '/api/payment-webhook' : settings.mollie.url + 'api/payment-webhook';
        }
    }

    mollie.payments.create({
        //locale: 'nl_NL',
        amount: order.amount,
        description: order.description,
        //the redirecturl is based on the order
        redirectUrl: order.redirectUrl,
        //Use ngrok to make this url available to the world in development, otherwise mollie won't accept it
        webhookUrl: webhookUrl(req.body.facebookOrder),
        metadata: {
            orderID: order.payId
        }
    }, function(payment){
        if(payment.error){
            console.log("Error creating payment", payment, order);
            var data = {
                succes: false,
                error: payment.error
            }
            return res.json(data);
        }
        //set the paymentId to all orders with the given PayId, otherwise we are not able to set the order to paid later.
        Order.find({payId: ObjectId(order.payId)}).exec(function(err, orders){
            async.eachSeries(orders, function(order, callback){
                order.billingDetails[0].paymentId = payment.id;
                if(shop){
                    //when there is a 'custom' shop object use that instead of getting a real shop
                    //this is happen when a user without shops is placing an Facebook order.
                    //the user is asked for some details for the invoice
                    order.shop.noShop = true;
                    order.shop.name = shop.name;
                    order.shop.emailAddress = shop.email;
                    order.shop.address = shop.address;
                }
                
                order.save(function(err, result){
                    if(err){
                        console.log("error during updating order", err);
                        callback(err);
                    }else{
                        callback();
                    }
                })
            },function(err){
                if(err){
                    console.log("one or more orders are not updated", err);
                    var data = {
                        succes: false,
                        error: err
                    }
                    return res.json(data);
                }else{
                    var data = {
                        succes: true,
                        payment: payment
                    }
                    return res.json(data);
                }
            })
        })
    });
}

module.exports.paymentWebhook = function(req,res){
    //This function is called immediately after pressing the pay or cancel button in mollie, so this is the place to process the order
    console.log("paymentWebhook");
    res.send("OK"); //send http 200 to mollie

    var paymentId = req.body.id; //Mollie paymentId;
    var payId; //internal payId;
    mollie.payments.get(paymentId, function(payment){

        if(payment.status == 'refunded') {
            //full or partial refund for this payment
            //update order status and send mail to the owner
            //An refund is always for one order at a time
            Order.findOne({billingDetails: {$elemMatch:{paymentId: paymentId}}}).exec(function(err, order){
                
                if(order){
                    orderService.updateOrderStatus(order.number, {status: 'Refunded', date: new Date(), comment: null}, function(err, result){
                        if(err){
                            return res.json({message: err});
                        }else{
                            //find user and shop
                            User.findOne({_id: order.user[0]._id}).exec(function(err, user){
                                Shop.findOne({nameSlug: order.shop[0].nameSlug}).exec(function(err, shop){
                                    var data = {
                                        user: user,
                                        shop: shop,
                                        order: order
                                    };
                                    //send mail with the new status
                                    var options = {
                                        to: user.email,
                                        subject: 'Er is nieuws over bestelling ' + order.number +'!',
                                        template: 'order-update'
                                    };

                                    orderService.updateOrderPayment(order._id, {status: 'returned', comment: 'This order has been refunded'}, function(err, result){
                                        console.log("updateOrderPayment Result", err, result);
                                    })

                                    mailService.mail(options, data, null, function(err, orderSendResult){
                                    })
                                })
                            })
                        }
                    })
                }else{
                    //TODO: create method to look if there is an fbOrder with that payment id
                }
            })
            return;
        }

        var paymentStatus = payment.isPaid() ? "Paid" : "Payment cancelled";
        Order.find({billingDetails: {$elemMatch:{paymentId: paymentId}}}).exec(function(err, orders){

            async.eachSeries(orders, function(order, callback){
                payId = order.payId;

                var orderStatus = {
                    status: paymentStatus,
                    date: new Date(),
                    comment: ""
                }
                order.status.push(orderStatus);

                order.billingDetails[0].payMethod = payment.payMethod;
                order.billingDetails[0].payDetails = JSON.stringify(payment.details);
                
                order.save(function(err, result){
                    if(!err){
                        callback(result);
                    }else{
                        console.log("error while saving order", err);
                    }
                })
            }, function(order){
                if(paymentStatus == "Paid"){
                    orderService.completeOrder(payId, false, function(status, response){

                        orderService.updateOrderPayment(order._id, {status: 'paid', comment: 'This order is paid'}, function(err, result){
                            console.log("updateOrderPayment Result", err, result);
                        })
                        //Update stock for ordered items
                        Shop.findOne({nameSlug: order.shop[0].nameSlug}).exec(function(err, shop){
                            if(!err && shop){
                            
                                async.forEach(order.items, function(item, itemDone){
                                    shopService.updateStock(shop._id, item._id, item.quantity, true);
                                }, function(){
                                    return;
                                    //return res.status(200).json({message: response});
                                })                                
                            }
                        })
                    })
                }else{
                    return;
                    //return res.status(200).json({message: 'Payment not successfully'});
                }
            })
        })
    }) 
}

module.exports.fbPaymentWebhook = function(req,res){
    console.log("fbPaymentWebhook");
    var paymentId = req.body.id; //Mollie paymentId;
    res.send("OK"); //Mollie doesn't want to wait long, so send an 'OK' here.

    mollie.payments.get(paymentId, function(payment){
        var paymentStatus = payment.isPaid() ? "Paid" : "Payment cancelled";
        fbOrder.findOne({paymentid: paymentId}).exec(function(err, fbOrder){
            if(fbOrder){
                var orderStatus = {
                    status: paymentStatus,
                    date: new Date(),
                    comment: ""
                }
                fbOrder.status.push(orderStatus);
            
            if(payment.isPaid()){
                orderService.createPayment(fbOrder, function(err, payment){
                    //generate invoice and dat soort things
                    orderService.handlePayment(null, {generateInvoiceNumber:true}, function(payment2){
                        payment.invoiceNumber = payment2.invoiceNumber;
                        fbOrder.number = payment.invoiceNumber;

                        fbOrder.save();
                        payment.save(function(err, result){
                            if(err){
                                console.log("error while saving payment", err);
                                return;
                            }
                            orderService.createFbInvoice(fbOrder._id, function(err, result){
                                if(err){
                                    console.log("error creating invoice", err);
                                    return;
                                }

                                var data = {
                                    order: fbOrder,
                                    user: fbOrder.user[0]
                                }

                                var attachment = [{
                                    filename: "Factuur " + fbOrder.number + ".pdf",
                                    content: result
                                }];
                                    
                                var options = {
                                    to: fbOrder.user[0].email,
                                    bcc: 'info@prismanote.com',
                                    subject: 'Factuur ' + fbOrder.number + ' voor campagne ' + fbOrder.campaign.name,
                                    template: 'campaign-invoice'
                                }
    
                                mailService.mail(options, data, attachment, function(err, mailResult){
                                    //mail verzonden, klaar!
                                    
                                })
                            })
                        })
                    })
                })
            }
            }else{
                //no order found
                return;
            }
        })
    })
}

module.exports.fbCheckPayment = function(req,res){
    console.log("CheckPayment",req.params);
    var payId = req.params.payId;
    var result;
    //search for one order with the payId, to get the paymentId, which we need to verify the payment
    fbOrder.find({payId: payId}).exec(function(err, orders){
        console.log('fborder',orders);
        if(orders){
            if(orders[0].payId){
                var paymentId = orders[0].paymentid;
                mollie.payments.get(paymentId, function(payment){
                    if(payment.isPaid()){
                        result  = {
                            code: 200,
                            msg: "Thank you! Payment is successfully! <br> More information about your order will follow soon via email."
                        }
                        var query = {'payId':orders[0].payId}; 
                        fbOrder.update(query, { "$set": { "status.0.status": "success" }  }, function(err, doc){
                            if (err) return res.send(500, { error: err });
                            console.log("saved");
                            // return res.send("succesfully saved");
                            return res.status(200).json(result);
                        });
                    
                        
                    }else{
                        result = {
                            code: 400,
                            msg: "Your payment failed. <br> You can re-pay or cancel via your account."
                        }
                        var query = {'payId':orders[0].payId}; 
                        fbOrder.update(query, { "$set": { "status.0.status": "failed" } }, function(err, doc){
                            if (err) return res.send(500, { error: err });
                            console.log("saved");
                            // return res.send("succesfully saved");
                            return res.status(200).json(result);
                        });
                    }
                })
            }
        }else{
            res.status(404).json({message:'No orders found'});
        }
    })
}
module.exports.checkPayment = function(req,res){
    console.log("CheckPayment");
    var payId = req.params.payId;
    var result;
    //search for one order with the payId, to get the paymentId, which we need to verify the payment
    Order.find({payId: payId}).exec(function(err, orders){
        if(orders){
            if(orders[0].payId){
                var paymentId = orders[0].billingDetails[0].paymentId;
                mollie.payments.get(paymentId, function(payment){
                    if(payment.isPaid()){
                        result  = {
                            code: 200,
                            msg: "Thank you! Payment is successfully! <br> More information about your order will follow soon via email."
                        }
                        //clean the cart;
                        req.session.cart = [];
                        return res.status(200).json(result);
                    }else{
                        result = {
                            code: 400,
                            msg: "Your payment failed. <br> You can re-pay or cancel via your account."
                        }
                        return res.status(200).json(result);
                    }
                })
            }
        }else{
            res.status(404).json({message:'No orders found'});
        }
    })
}

module.exports.getPayment = function(req,res){
    var paymentId = req.body.paymentId;
    mollie.payments.get(paymentId, function(payment){
        if(payment.error){
            return res.json({message: payment.error.message})
        }else{
            return res.json({payment: payment});
        }
    })
}

module.exports.refundPayment = function(req,res){
    console.log("refundPayment");
    var paymentId = req.body.paymentId;
    var amount = req.body.amount;

    mollie.payments.get(paymentId, function(payment){
        if(payment){
            mollie.payments_refunds.withParent(payment).create({amount: amount}, function(refund){
                if(refund.error){
                    console.log("err", refund);
                    return res.status(405).json({message: refund.error.message})
                }

                return res.json({refund: refund});
            })
        }
    })
}

module.exports.getRefunds = function(req,res){
    var paymentId = req.body.paymentId;
    if(paymentId){

    mollie.payments_refunds.withParentId(paymentId).all(function(refunds){
        if(refunds.error){
            console.log("err",refunds);
            return res.json({message: 'error getting refunds' + refunds.error});
        }

        return res.json({refunds: refunds});
    })
    }else{
        return res.json({message: 'no payment id given'});
    }
}
