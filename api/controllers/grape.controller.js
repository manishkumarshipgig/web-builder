var express = require('express');
const path = require('path');
const mongoose = require('mongoose');
//const cors = require('cors');
const fs = require('fs');
const fse = require("fs-extra");
const formidable = require('formidable');
const prod = 'production/';
const dev = 'development/';
const imagePath = 'images/';
const videoPath = 'video/';
const logoPath = 'logo/';
const mv = require('mv');
var createHTML = require('create-html');
var app = express();
// app.use(cors({origin: clientAddress, credentials: true}));

module.exports.postGrapeTemplate = function (req, res) {
    var data = req.body;
    var websiteName = data.websiteName;
    var pageName = data.pageName;
    var templateName = data.templateName;

    var dir = path.join(__dirname, '..', '..', 'public', 'prismanote', websiteName, '/');
    var dir1 = path.join(__dirname, '..', '..', 'public');
    // var dir = __dirname + '/public/prismanote/' + websiteName + '/';
    if (fs.existsSync(dir + dev + pageName + '.html')) {
        res.sendFile(dir + dev + pageName + '.html')
    } else {
        res.sendFile(dir1 + '/default-template/' + templateName + pageName + '.html')
    }
};

module.exports.postGrapeSave = function (req, res) {
    
    var dir1 = path.join(__dirname, '..', '..', 'public');
    var data = req.body;
    var pageName = data.pageName;
    var html = data.html;
    var websiteName = data.websiteName;

    if (pageName) {
        var dir = dir1 + '/prismanote/' + websiteName + '/';
        
        saveTemplatesDev(dir, pageName,websiteName, html).then(function (data1) {
            res.send({ 'success': true });
        })
    }else{
        console.log('called post else con');
    }

};

module.exports.changeTemplate = function (req, res) {
    var data = req.body;
    console.log("data", data);
    var websiteName = data.websiteName;
    //var dir = __dirname + '/public/prismanote/' + websiteName + '/';
    var dir = path.join(__dirname, '..', '..', 'public', 'prismanote', websiteName, '/');
    if (fs.existsSync(dir)) {
        saveChangedTheme(dir, websiteName).then((data1) => {
            res.send({ 'success': true });
        });
        res.send(false);
    }
    else {
        res.send(true);
    }
}


module.exports.postGrapePublish = function(req, res) {

    var data = req.body;
    var websiteName = data.websiteName;

    if (websiteName) {
        var dir = path.join(__dirname, '..', '..', 'public','prismanote',websiteName,'/');
        //var dir = __dirname + '/public/prismanote/' + websiteName + '/';
        var source = dir + dev;
        var destination = dir + prod;
        console.log(destination);

        fse.copy(source, destination, {
            'overwrite': true
        }, function (err) {
            if (err) {
                console.log('An error occured while copying the folder.');
            } else {
                res.send({'success': true});
            }
        });

    }

};

module.exports.postGrapeCanPublish = function(req, res){

    var data = req.body;
    var websiteName = data.websiteName;
    var arrayPages = data.pages || [];

    if (websiteName && arrayPages.length > 0) {
        var dir = path.join(__dirname, '..', '..', 'public','prismanote',websiteName,dev);
        recursivelyCheckFile(dir,websiteName,arrayPages).then(function(data){
            res.send(data);
        },function(err){
            res.send(err);
        })
     

    }

};

module.exports.postGrapeImgUpload = function(req, res){
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
        var length = Object.keys(files)[0].length;
        var websiteName = Object
            .keys(files)[0]
            .substring(0, length - 2);
        var dir = path.join(__dirname, '..', '..', 'public','prismanote',websiteName,'images');
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        var oldpath = files[Object.keys(files)[0]].path;
        var newpath = dir + files[Object.keys(files)[0]].name;

        fs.rename(oldpath, newpath, function (err) {
            if (err) 
            if (err) 
                throw err;
            var data = {
                data: [
                    {
                        type: 'image',
                        src: dir +'/'+ files[Object.keys(files)[0]].name,
                        height: 350,
                        width: 250
                    }
                ]
            };
            res.send(data);
        });
    });

};
module.exports.uploadNewImage = function(req,res){
   // var logo = req.files.file;
    console.log("logo", logo.name);
        var dir = path.join(__dirname, '..', '..', 'public','images','/');;
                          
            if (!req.files) {
                res.send("no file uploaded");
            } else {
                var file = req.files.file;
                //console.log("req.files.imgAll",file);
                var extension = path.extname(file.name);
                //console.log("extension", logo.name);
                if (extension !== ".png" && extension !== ".jpg" && extension !== ".gif") {
                    res.send("only images are allowed");
                }
                else {
                    mv(file.path,dir + file.name, function (err) {
                        if (err) return console.error(err)
                       res.send('/public/images/'+file.name);
                    });
                }

            }
           
      
}
module.exports.getAllNewImage = function(req,res){
    var dir = path.join(__dirname, '..', '..', 'public','images');
    var uploadedImages = [];
    fs.readdir(dir, (err, files) => {
        files.forEach(file => {
            if(file.includes(".txt") == false){
                uploadedImages.push('/public/images/'+file);
            }
        });
        
         return res.send(uploadedImages);
      });
 }

module.exports.postSiteName = function(req, res){
    var data = req.body;
    var websiteName = data.websiteName;
    var logo = req.files.file;

    var dir = path.join(__dirname, '..', '..', 'public','prismanote',websiteName,'/');
    //var dir = __dirname + '/public/prismanote/' + websiteName + '/';
   
    // if(fs.existsSync(dir)){
    //         res.send(true);
    //     }else{
    //         res.send(false);
    //     }
    if (!fs.existsSync(dir)) {
            
        saveLogo(dir, logo, websiteName).then(function(data1) {
            res.send({ 'success': true });
          
        });
        if (!req.files) {
            res.send("no file uploaded");
        } else {
            var file = req.files.file;

            var extension = path.extname(logo.name);
            if (extension !== ".png" && extension !== ".jpg" && extension !== ".gif") {
                res.send("only images are allowed");
            }
            else {
                mv(file.path,dir + logoPath + file.name, function (err) {
                    if (err) return console.error(err)
                    var abc=dir+'logo';
                    app.use(express.static(dir + logoPath));

                });
            }

        }
        res.send(false);
    }
    else {
        res.send(true);
       
    }
}

module.exports.createCustomPage = function(req,res){

        var data = req.body;
        var websiteName = data.websiteName;
        var customPageName = data.customPageName;
        var customHtml = createHTML({
            title: 'example',
            lang: 'en',
            head: '<meta name="description" content="example">',
            body: '<p>example</p>',
           
          });

          var dir1 = path.join(__dirname, '..', '..', 'public','prismanote',websiteName,dev);
         
        fs.writeFile(dir1+customPageName+'.html', customHtml, function (err) {
            if (err) console.log(err)
            res.send(true);
          })
}

module.exports.templateChooser = function(req,res){
        var data = req.body;
        var websiteName = data.websiteName;
        var pageName = data.pageName;
        var templateName = data.templateName;
        var dir = path.join(__dirname, '..', '..', 'public','prismanote',websiteName,'/');
        if(templateName == 'template-1/'){
            var source = path.join(__dirname, '..', '..','public','default-template',templateName);
            var destination = dir + dev;
            fse.copy(source, destination, function (err) {
                if (err) {
                    console.error(err);
                } else {
                    console.log("success temp3!");
                    res.send(true);
                }
            });
    
        }
        else if(templateName == 'template-2/'){
            var source = path.join(__dirname, '..', '..','public','default-template',templateName);
            var destination = dir + dev;
            fse.copy(source, destination, function (err) {
                if (err) {
                    console.error(err);
                } else {
                    console.log("success temp2!");
                    res.send(true);
                }
            });
    
        }
        else if (templateName == 'template-3/'){
            var source = path.join(__dirname, '..', '..','public','default-template',templateName);
            var destination = dir + dev;
            fse.copy(source, destination, function (err) {
                if (err) {
                    console.error(err);
                } else {
                    console.log("success temp3!");
                    res.send(true);
                }
            });
        }
}



module.exports.wildcards = function (req, res)  {
    var dir = path.join(__dirname, '..', '..', 'public','prismanote');
    var wildCardNames = [];
    fs.readdir(dir, (err, files) => {
        files.forEach(file => {
            if(file.includes(".txt") == false){
                wildCardNames.push(file);
            }
        });
         return res.send(wildCardNames);
      });

};





// Functions



function recursivelyCheckFile(dir, websitename, arrayPages) {

    return new Promise((resolve, reject) => {
        var c = 0;
        var i = 0;
        var unsavedPages = [];
        for (var i = 0; i < arrayPages.length; i++) {
            if (!fs.existsSync(dir + arrayPages[i] + '.html')) {
                c = 1;
                unsavedPages.push(arrayPages[i]);
            }
            if (i == arrayPages.length - 1) {
                if (c == 1) {
                    reject({ 'success': false, 'pages': unsavedPages })
                } else {
                    resolve({ 'success': true });
                }
            }
        }
    });
}

function saveLogo(dir, logo, websiteName) {

    return new Promise(function(resolve, reject){

        var fnSave = function () {
            if (!fs.existsSync(dir + dev)) {

                fs.mkdirSync(dir + dev);
                fs.mkdirSync(dir + prod);
                if (!fs.existsSync(dir + logoPath)) {
                    fs.mkdirSync(dir + logoPath);
                }
                var source = path.join(__dirname, '..', '..', 'public', 'default-template', 'template-1');
                //var source = 'public/default-template/template-1';
                var destination = path.join(__dirname, '..', '..', 'public', 'prismanote', websiteName, dev);
                //var destination = 'public/prismanote/' + websiteName + '/' + dev;
                fse.copy(source, destination, function (err) {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log("success!");
                    }
                });


            }
        };

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
            fnSave();
        } else {
            fnSave();
        }

    });
}

function saveChangedTheme(dir, websiteName) {

    return new Promise((resolve, reject) => {

        var fnSave = function () {
            if (fs.existsSync(dir + dev) && fs.existsSync(dir + prod)) {
                var source = path.join(__dirname, '..', '..', 'public', 'default-template', 'template-1');
                //var source = 'public/default-template/template-1';
                var destination = path.join(__dirname, '..', '..', 'public', 'prismanote', websiteName, dev);
                //var destination = 'public/prismanote/' + websiteName + '/' + dev;
                fse.copy(source, destination, function (err) {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log("success theme!");
                    }
                });
            }
        };

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
            fnSave();
        } else {
            fnSave();
        }

    });
}

function saveTemplatesDev(dir, pageName,websiteName, html) {
    return new Promise((resolve, reject) => {

        var fnSave = function () {
            if (fs.existsSync(dir + dev)) {

                if (!fs.existsSync(dir + videoPath)) {
                    fs.mkdirSync(dir + videoPath);
                    var videoSource = path.join(__dirname, '..', '..', 'public', 'default-template', 'video');
                    var videoDestination =dir + videoPath;
                    app.use(express.static(dir + videoPath));
                    fse.copySync(videoSource, videoDestination,  /.*(.mp4|.webm)$/,function (err) {
                        if (err) {
                            console.error(err);
                        } else {
                            console.log("success! video");
                        }
                    });
                }else{
                    var videoSource = path.join(__dirname, '..', '..', 'public', 'default-template', 'video');
                    //var videoSource = __dirname+'/public/default-template/video';
                    
                    var videoDestination =dir + videoPath;
                    app.use(express.static(dir+ videoPath));
                    fse.copySync(videoSource, videoDestination,  /.*(.mp4|.webm)$/,function (err) {
                        if (err) {
                            console.error(err);
                        } else {
                            console.log("success! video");
                        }
                    });
                }
                fs.writeFile(dir + dev + pageName + '.html', html, function (err) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(true);
                        }
                    });
            } else {
                console.log('exist dev dir');
                fs.writeFile(dir + dev + pageName + '.html', html, function (err) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(true);
                        }
                    });
            }
        };

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
            fnSave();
        } else {
            fnSave();
        }

    });
}

