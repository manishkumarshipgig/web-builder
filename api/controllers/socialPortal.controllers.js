const path = require('path');
const fs = require('fs');
const passport=require('passport');
const async = require('async');
const request = require('request');
const graph = require('fbgraph');
const archiver = require('archiver');
const rimraf = require('rimraf');
const PDK = require('node-pinterest');
const DateDiff = require('date-diff');

const mongoose = require('mongoose');
const SocialPortal = mongoose.model('Socialportal');
const Campaigns = mongoose.model("Campaign");
const User = mongoose.model('User');
const ObjectId = require('mongodb').ObjectId;

const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));
const string = require(path.join(__dirname, '..', 'services', 'string.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));
const facebookService = require(path.join(__dirname, '..', 'services', 'facebook.service'));
const userService = require(path.join(__dirname, '..', 'services', 'user.service'));
const socialPortalService = require(path.join(__dirname, '..', 'services', 'socialportal.service'));


module.exports.getSocialPortal = function(req,res){    
    if(req && req.params && req.params.portalId) {
        socialPortalService.getSocialPortal(req.params.portalId, function(err, result){
            if(err){
                return res.status(err.code).json({message: err.msg})
            }
            return res.status(200).json({message: 'OK', result})
        })
    }
}

module.exports.getUserSocialPortal = function(req,res){
    if(req && req.query) {
        socialPortalService.getUserSocialPortal(req.query.userId, function(err, result){
            if(err){
                return res.status(err.code).json({message: err.msg})
            }
            return res.status(200).json({message: 'OK', result})
        })
    }
}

module.exports.createSocialPortal = function(req, res){
    if(req && req.body && req.body.user) {
        socialPortalService.createSocialPortal(req.body.user, function(err, result){
            if(err){
                return res.status(err.code).json({message: err.msg})
            }
            return res.status(200).json({message: "OK", result}) 
        })
    }else{
        res.status(500).json({message: 'no details provided'})
    }
}

module.exports.checkSocialPortal = function(req, res){
    if(req && req.query && req.query.userId){
        User.findOne({_id: req.query.userId}).exec(function(err, user){
            if(err || !user){
                return res.status(500).json({message: !user ? "No user found" : "Error while getting user: " + err})
            }
            socialPortalService.getUserSocialPortal(user._id, function(err, data){
                if(err && err.code == 404){
                    //res.send(err);
                    socialPortalService.createSocialPortal(user, function(err, data){
                        if(err){
                            return res.status(500).json({message: err})
                        }
                        return res.status(200).json({message: "OK", socialPortal: data})
                    })
                }else{
                    return res.status(200).json({message: "OK", socialPortal: data});
                }
            })
        })
    }
}

module.exports.saveSocialPortal = function(req,res){
    if(req && req.body.socialPortal){
        socialPortalService.saveSocialPortal(req.body.socialPortal._id, req.body.socialPortal, function(err, result){
            if(err){
                return res.status(err.code).json({message: err.msg})
            }
            return res.status(200).json({message: "OK", result}) 
        })
    }else{
        res.status(500).json({message: 'no details provided'})
    }
}

module.exports.getPortalCampaignsAndTasks = function(req, res){
    console.log("getPortalCampaigns");
    var status, response;
    if(req && req.query.portalId){
        SocialPortal.findOne({_id: ObjectId(req.query.portalId)}).exec(function(err, portal){
            if(err){
                res.status(500).json({message: 'Error finding portal:' + err});
            }
            else if(!portal){
                res.status(404).json({message: 'Portal not found'});
            }else{
                var brands = [];
                var campaigns = [];
                async.forEach(portal.campaigns, function(campaign, campaignsDone){                    
                    if(campaign._id ){
                        campaigns.push(ObjectId(campaign._id));
                    }
                    campaignsDone();
                }, function(){
                    async.forEach(portal.brands, function(brand, callback){
                        if(brand._id){
                            brands.push(ObjectId(brand._id));
                        }
                        callback();
                        
                    }, function(){
                        Campaigns.find(
                                	{
                                        $and: [
                                            {
                                                '_id' : {
                                                    $nin: campaigns
                                                }
                                            }
                                            ,{
                                                'brand._id' : {
                                                    $in: brands
                                                }
                                            },{
                                                "endDate" : {
                                                    $gte : new Date()
                                                }
                                            }
                                            ,{
                                                "isVerified": true
                                            }
                                        ]
                                    }).exec(function(err, campaigns){      //.sort({_id: -1})
                            if(err){
                                res.status(500).json({message: 'Error finding campaigns: '+ err});
                            }
                            else if(!campaigns){
                                res.status(404).json({message: "no campaigns found"});
                            }else{
                                res.status(200).json({message: "Campaigns found", campaigns})
                            }
                        })
                    })
                })
            }
        })
    }
}

function getUserAndAccesToken(userId, type, callback){
    //token and pagetoken must be set to select:false
    User.findOne({_id: userId}).select('+facebook.extendedToken +facebook.extendedPageToken +pinterest.accessCode').exec(function(err, user){
        if(err){
            console.log("error finding user", err);
            return callback("error finding user: " + err);
        }
        else if(!user){
            console.log("no user found");
            return callback("no user found");
        }else{
            //User found, now check if the accesstoken is not expired, otherwise, extend it via facebook
            if(user.facebook){

                if(type == null){
                    type = user.facebook.postTo == 'page' ? 'page' : 'wall';
                }

                getTheTokens(user, type, function(userWithTokens){

                    var diff = new DateDiff(new Date(), new Date(userWithTokens.accessTokenDate));

                    if(diff.days() >= 59 || !userWithTokens.accessTokenExtended){
                        var backupCallback = callback;
                        //time to extend
                        facebookService.extendAccessToken(userWithTokens.accessToken, function(err, result){
                            if (err)
                                return callback(err);

                            if(type == "page"){
                               userWithTokens.facebook.extendedPageToken = result.access_token;
                               userWithTokens.facebook.pageTokenDate = new Date();
                            }else{
                               userWithTokens.facebook.extendedToken = result.access_token;
                               userWithTokens.facebook.tokenDate = new Date();
                            }    

                            userService.updateUser(userWithTokens._id, userWithTokens, function(err, saveResult){
                                if (err)
                                    return callback(err)
                                //new tokens saved, get the results and return
                                getTheTokens(user, type, function(user){
                                    return callback(null, user);
                                })
                            })
                        })
                    }else{
                        return callback(null, user);
                    }
                })
            }else{
                return callback("No facebook settings found");
            }
        }
    })
}

getTheTokens = function(user, type, callback){
    //set the right token to an temp property of the user
    if(type == 'page'){
        user.accessToken = user.facebook.pageToken;
        user.accessTokenDate = user.facebook.pageTokenDate ? user.facebook.pageTokenDate : new Date();
        user.accessTokenExtended = user.facebook.extendedPageToken;
        user.pageId = user.facebook.pageId;
    }else if(type =='wall'){
        user.accessToken = user.facebook.token;
        user.accessTokenDate = user.facebook.tokenDate ? user.facebook.tokenDate : new Date();
        user.accessTokenExtended = user.facebook.extendedToken;
        user.pageId = user.facebook.profileId;
    }
    return callback(user);        
}

module.exports.facebookLogin = function(req,res, next) {
    console.log("facebookLogin");
    passport.authenticate('facebook', {scope: 'email'});
}

module.exports.facebookCallback = function(req,res, next) {
    console.log("facebookCallback");
    passport.authenticate('facebook', {
        successRedirect: '/retailer/home',
        failureRedirect: '/retailer'
    });
}

module.exports.getFacebookPages = function(req, res){
    console.log("getFacebookPages");

    getUserAndAccesToken(req.user._id, 'wall', function(err, user){
        if (err)
            return res.status(500).json({message: err});
        //To get the pages from the user, always use the acces token from the profile!

        graph.setAccessToken(user.facebook.extendedToken);
    
        graph.get(user.pageId + '/accounts', function(err, result){
            if(err){
                console.log("error getting pages from Facebook", err);
                return res.status(500).json({message: "Error getting data from Facebook: " + err})
            }
            var pages = result.data;
            res.status(200).json({message: "Data found", pages})
        })
    });
}

module.exports.sendTestPost = function(req, res){
    console.log("sendTestPost");

    getUserAndAccesToken(req.user._id, null, function(err, user){
        if (err)
            return res.status(500).json({message: err});

        graph.setAccessToken(user.accessTokenExtended);
        
        var post = {
            url: "https://prismanote.com/images/prismanote_logo_large.png",
            caption: "Dit is een testbericht via PrismaNote"
        }
        graph.post(user.pageId + '/photos', post, function(err, result){
            if(err){
                console.log("err", err);
                return res.status(500).json({message: "Error posting to Facebook: " + err.message})
            }
            res.status(200).json({message: "Successfully!", result})
        })
    });
    
}

module.exports.getFacebookInfo = function(req, res){
    console.log('user is',req.user);
    if(req.user.facebook){
        console.log('user profile',req.user.facebook.profileId);
        if(req.user.facebook.profileId){
            var profile = req.user.facebook.profileId;
            request.get({
                headers: { 'content-type': 'application/json' },
                url: 'https://graph.facebook.com/v3.0/' + profile + '?access_token=' + facebookApi.accessToken,
                method: 'GET'
            }, function (error, response, body) {
                if (error) {
                    res.status(500).json({error: error});
                } else {
                    if(body.error){
                        res.status(100).json({message : "User is not Logged In"});
                    }
                    res.status(200).json({"data": JSON.parse(body)})
                }
            });
        }else{
            res.status(500).json({message : "User is not Logged In"});
        }
    }else{
        res.status(500).json({message : "User is not Logged In"});
    }
    
}

bucketLocation = function(){
    return settings.s3Bucket.location + settings.s3Bucket.bucket
}

findTaskById = function(taskId, campaign, callback){
    for(var i=0; i < campaign.tasks.length; i++){
        if(campaign.tasks[i].id == taskId){
            return callback(campaign.tasks[i]);
        }
    }
}

module.exports.postUpdate = function(req, res){
    console.log("postUpdate");

    if(req && req.body.data){
        getUserAndAccesToken(req.user._id, null, function(err, user){
            if (err)
                return res.status(500).json({message: err});
            
            graph.setAccessToken(user.accessTokenExtended);

            var post = {
                url: req.body.data.url,
                caption: req.body.data.caption
            }

            if(req.body.data.scheduled){
                var date = new Date(req.body.data.date);
                var timestamp = date.getTime()/1000|0;
                
                post.published = false;
                post.scheduled_publish_time = timestamp;
            }

            graph.post(user.pageId + '/photos', post, function(err, result){
                if(err){
                    console.log("error posting to facebook", err);
                    return res.status(500).json({message: "Error posting to Facebook: " + err.message})
                }
                res.status(200).json({message: "Successfully!", result})
            })
        });
    }else{
        res.status(500).json({message: 'no details provided'})
    }
}

module.exports.downloadFiles = function(req,res){
    console.log("downloadFiles");

    if(req && req.query.campaignId && req.query.taskId && req.query.portalId){
        SocialPortal.aggregate([
            {$match: {_id: ObjectId(req.query.portalId)}},
            {$project: {
                campaigns: {$filter: {
                    input: '$campaigns',
                    as: 'campaign',
                    cond: {$eq: ['$$campaign._id', ObjectId(req.query.campaignId)]}
                }}
            }}
        ]).exec(function(err, portal){
            var campaign = portal[0].campaigns[0];

            if(err){
                console.log("err", err);
                return res.status(500).json({message: 'Error finding socialportal: ' + err});
            }
            if(!campaign){
                console.log("no campaign found");
                return res.status(404).json({message: "No campaign portal found"});
            }

            async.forEach(campaign.tasks, function(task, callback){
                if(task._id.toString() == req.query.taskId){
                    callback(task);
                }
            }, function(task){
                var files = [];

                if(task.images.length == 0){
                    console.log("no images in task");
                    return res.status(500).json({message: 'Task contains no images'});
                }
                for(var i =0; i < task.images.length; i++){
                    upload.getFile(task.images[i].src, {localFolder: campaign._id.toString()}, function(err, result){
                        if (err)
                            return res.status(500).json({message: "Error getting file: " + err})

                        files.push(result);
                        if(files.length == task.images.length){
                            //all files are downloaded, now we can create a textfile and a zipfile

                            fs.writeFile("tmp/" + campaign._id.toString() + "/tekst.txt", task.nl.description, function(err){
                                if(err){
                                    return console.error(err);
                                }
                            })

                            var archive = archiver('zip');
                            var directoryName = 'tmp/' + campaign._id.toString();

                            archive.on('error', function(err){
                                throw err;
                            });
        
                            archive.on('end', function(){
                                //console.log('Archive wrote %d bytes', archive.pointer());
                                //Delete the tmp folder for this campaign
                                rimraf(directoryName, function(){
                                    //folder deleted
                                })
                            });                   
        
                            res.attachment(string.slugify(task.nl.name)+ '.zip');
                            
                            archive.directory(directoryName, directoryName.replace('tmp/' + campaign._id.toString(), ''));

                            archive.finalize();                   
                            archive.pipe(res); 
                        }
                    })  
                }
            }) //end foreach
        })
    }else{
        return res.status(500).json({message: 'No details provided'});
    }
}

module.exports.getPortalCampaign = function(req, res){

    if(req && req.query.portalId && req.query.campaignSlug){
        SocialPortal.aggregate([
            {$match: {_id: ObjectId(req.query.portalId)}},
            {$project: {
                campaigns: {$filter: {
                    input: '$campaigns',
                    as: 'campaign',
                    cond: {$eq: ['$$campaign.nameSlug', req.query.campaignSlug]}
                }}
            }}
        ]).exec(function(err, portal){
            if(err)
                return res.status(500).json({message: 'Error finding campaign: ' + err});
            if(!portal){
                return res.status(404).json({message: 'Campaign not found'});
            }
            var campaign = portal[0].campaigns[0];
            return res.status(200).json({message: "Campaign found", campaign})
        })
    }else{
        return res.status(500).json({message: 'No details provided'});
    }
}
module.exports.getBrandCampaign = function(req, res){

    if(req  && req.query.campaignSlug){
        Campaigns.find({'nameSlug':req.query.campaignSlug}).exec(function(err, campaigns){
    if(err){
        res.status(500).json({message: 'Error finding campaigns: '+ err});
    }
    else if(!campaigns){
        res.status(404).json({message: "no campaigns found"});
    }else{
        res.status(200).json({message: "Campaigns found", campaigns})
    }
})
    }else{
        return res.status(500).json({message: 'No details provided'});
    }
}

module.exports.sendCampaignPromotion = function(req, res){
    console.log("sendCampaignPromotion");

    if(req && req.body.campaign){
        var data = {
            campaign: req.body.campaign,
            task: req.body.task,
            user: req.body.user,
        };
        var options = {
            //to: 'info@prismanote.com; marketing@prisma.watch; info@excellentwebshop.nl',
            to: 'info@prismanote.com',
            subject: 'Nieuwe campagne order',
            template: 'campaign-order'
        };
        mailService.mail(options, data, null, function(err, sendResult){
            if(err){
                return res.status(500).json({message: 'Error sending promotion: ' + err});
            }

            if(!sendResult){
                return res.status(404).json({message: 'Campaign not found'});
            }

            return res.status(200).json({message: "Order sent!", sendResult})
            
        })
    }
}

module.exports.uploadPromotionImages = function(req,res){
    console.log("uploadPromotionImages");
    //only used for jeweller promotion uploads
    if(req.body.campaign && req.body.task && req.body.user){
        var campaign = JSON.parse(req.body.campaign);
        var task = JSON.parse(req.body.task);
        var user = JSON.parse(req.body.user);  
    }
    
    console.log(req.files);
    var files = [];

    async.forEach(req.files.files, function(file, filesCompleted){
        upload.uploadFile(file, 'campaign-files', null, function(err, result){
            if(err)
                return res.status(500).json({message: 'Error uploading file: ' + err});
            var file = {
                name: result.split('/')[1],
                path:  result
            }
            files.push(file)
            filesCompleted();
        })
    }, function(){
        //filesCompleted
        //return the files so the frontend can add them to the scope and send an addionational save request
        return res.status(200).json({message: "files uploaded", files})
    })   
}
module.exports.uploadMarketingUserImage = function(req,res){
    console.log('request for upload file',req.files);
    var files = '';

    async.forEach(req.files.files, function(file, filesCompleted){
        upload.uploadFile(file, 'marketing-files', null, function(err, result){
            if(err)
                return res.status(500).json({message: 'Error uploading file: ' + err});
            var file = {
                name: result.split('/')[1],
                path:  result
            }
            files = file;
            filesCompleted();
        })
    }, function(){
        //filesCompleted
        //return the files so the frontend can add them to the scope and send an addionational save request
        return res.status(200).json({message: "files uploaded", files})
    })   
}

module.exports.getPinterestBoards = function(req, res){
    console.log("getPinterestBoards");

    getUserAndAccesToken(req, function(err, user){
        if(err)
            return res.status(500).json({message: "Error getting token", err});

        if(user){
            var pinterest = PDK.init(user.pinterest.accessCode);

            pinterest.api('me/boards').then(function(json){
                console.log("boards!", json);
                return res.status(200).json({message: "Successfull!", boards: json.data})
            })
        }
    })
}

module.exports.createPinterestPin = function(req, res){
    console.log("createPinterestPin");

    getUserAndAccesToken(req, function(err, user){
        if(err)
            return res.status(500).json({message: "Error getting token", err});

        if(user){
            var pinterest = PDK.init(user.pinterest.accessCode);

            pinterest.api('pins', {
                method: "POST",
                body: {
                    board: user.pinterest.board,
                    note: req.body.pin.note,
                    image_url: req.body.pin.image
                }
            }).then(function(json){
                console.log("createPinResult", json);
                return res.status(200).json({message: "Success!", result: json.data})
            })
            
        }
    })
}

module.exports.getFullTask = function(req, res){
    console.log("getFullTask");
    if(req && req.query.taskId && req.query.campaignId){
        SocialPortal.findOne({'campaigns._id': {$in: [req.query.campaignId]}}).exec(function(err, socialPortal){
            if(err){
                console.log("err", err);
                return res.status(500).json({message: 'Error finding portal: ' + err});
            }
            if(!socialPortal){
                console.log("portal not found");
                return res.status(404).json({message: 'No portal found'});
            }
            async.forEach(socialPortal.campaigns, function(campaign, callback){
                if(campaign._id == req.query.campaignId){
                    callback(campaign);
                }

            }, function(campaign){
                if(campaign){
                    findTaskById(req.query.taskId, campaign, function(task){
                        return res.status(200).json({message: 'OK', task, campaign});
                    })
                }
            })
        })
    }else{
        return res.status(500).json({message: 'No details provided'});
    }
}

module.exports.getSocialPortals = function(req, res){
    console.log('[getSocialPortals controller]');
    // default values, because offset and limit are required
    var offset = 0;
    var filterBy, sortBy, limit;
    
    if (req.query) {

        if (typeof req.query === 'string') {
            req.query = JSON.parse(req.query);
        }

        if (req.query.offset && !isNaN(parseInt(req.query.offset))) {
            offset = parseInt(req.query.offset);
            delete req.query.offset;
        }

        if (req.query.limit && !isNaN(parseInt(req.query.limit))) {
            limit = parseInt(req.query.limit);
            delete req.query.limit;
        }

        // if any filters are provided in the URL, parse them and add to the filtering params object.
        if (req.query.filter) {
            filterBy = filter(req.query.filter);
        }

        // if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
        if (req.query.sort) {
            sortBy = sort(req.query.sort);
        }

        if (req.query.lng && req.query.lat) {
            // TODO: find socialPortals close to the given coordinates. 
        }

    }

    SocialPortal.find(filterBy).sort(sortBy).skip(offset).limit(limit).exec(function (err, socialPortals) {
        var status, response;

        if (err) {
            status = 500;
            response = { message: 'Internal server error. ' + err };
        } else if (!socialPortals || socialPortals.length == 0) {
            status = 404;
            response = { message: 'No Social Portals found with the given search criteria. ' };
        } else {
            status = 200;
            response = { message: 'OK', socialPortals };
        }

        console.log(status + ' ' + response.message);
        res.status(status).json(response);

    });
};

module.exports.getImageUrl = function(req, res){
    var requests = require('request').defaults({ encoding: null });
    var imgurl = req.body.url;
    var url = settings.s3Bucket.location + imgurl;
    
    console.log('url of image',url);
    
    requests.get(url, function (error, response, body) {

    if (!error && response.statusCode == 200) {
        data = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
        res.status(200).json({'Url':data});
    }
});
}

