const path = require('path');

const passport=require('passport');
const parse = require('csv-parse');
const barcoder = require('barcoder');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const Collection = mongoose.model('Collection');
const User = mongoose.model('User');
const Brand = mongoose.model('Brand');
const fs = require('fs');
const request = require('request');
const async = require('async');
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));
const json2csv = require('json2csv');
const Url = require('url');
//const sleep = require('sleep');

// Helper functions to be used for CRUD (Create, Read, Update, Delete) operations.
const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const update = require(path.join(__dirname, '..', 'services', 'update.service'));
const remove = require(path.join(__dirname, '..', 'services', 'remove.service'));
const str  = require(path.join(__dirname, '..', 'services', 'string.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));

module.exports.getCollections = function(req, res) {
    console.log('[getCollections controller]');
    
    // default values, because offset and limit are required
    var filterBy, sortBy, offset = 0, limit, aggregate;

    if(req.query) {

        if(typeof req.query === 'string') {
            req.query = JSON.parse(req.query);
        }

        if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
            offset = parseInt(req.query.offset);
            delete req.query.offset;
        }

        if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
            limit = parseInt(req.query.limit);
            delete req.query.limit;
        }

        // if any filters are provided in the URL, parse them and add to the filtering or aggregate params object.
        if(req.query.aggregate) {
            aggregate = JSON.parse(req.query.aggregate);
        } else if(req.query.filter) {
            filterBy = filter(req.query.filter);
            // if(filterBy.isVerified == null && filterBy.includeUnverified != true) {
            //     filterBy.isVerified = true;
            // }
            // if(filterBy.includeUnverified) {
            //     delete filterBy.includeUnverified;
            // }
        }

        // if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
        if(req.query.sort) {
            sortBy = sort(req.query.sort);
        }

        if(req.query.lng && req.query.lat) {
            // TODO find collections close to the given coordinates. 
        }

    }
    if(aggregate) {
        console.log("Aggregate collections", aggregate);
        var query = Collection.aggregate().match(aggregate).allowDiskUse(true);
    } else{
        var query = Collection.find(filterBy).sort(sortBy);
    }
    
    if(limit){
        query.limit(limit);
    }

    query.skip(offset).exec(function(err, collections) {
        var status, response;

        if(err) {
            status = 500;
            response = {message: 'Internal server error. ' + err};
        } else if(!collections || collections.length == 0) {
            status = 404;
            response = {message: 'No collections found with the given search criteria. '};
        } else {
            status = 200;
            response = {message: 'OK', collections};
        }
        
        console.log(status + ' ' + response.message);
        res.status(status).json(response);

    });
};

module.exports.updateCollection = function(req, res){
    console.log("updateCollection");

    Collection.findByIdAndUpdate({_id: req.body.collection._id}, { $set: req.body.collection }, { new: true }, function (err, collection) {
        if(err){
            return res.status(500).json({message: err})
        }
        if(!collection){
            return res.status(404).json({message: "No collection found"})
        }
        return res.status(200).json({message: "OK", collection});

    })
}

module.exports.removeCollection = function(req, res){
    console.log("removeCollection", req.params, req.query, req.body);

    Collection.findByIdAndRemove({_id: req.params.id}, function(err, order){
        var status, response;
        if(err) {
            status = 400;
            response = {message: 'Invalid collection ID. ' + err};
        } else if(!order) {
            status = 404;
            response = {message: 'collection not found. '};
        } else {
            status = 204;
            response = {message: 'collection deleted. '};
        }

        console.log(status + ' ' + response.message);
        if(err) {
            console.log(err.stack);
        }
        // Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
        res.status(status);
    })
}

module.exports.addCollection = function(req, res) {
	console.log('[addCollection controller]');
    console.log(req.body);
	if(req.body) {

		// req.body.name = req.body.en.name; // Backwards compatibility

		create(req.body, function(newCollection) {
			if(newCollection) {

				if(newCollection.en.name != null && newCollection.en.name.length > 0) {
					newCollection.en.nameSlug = str.generateNameSlug(newCollection.en.name);
				}
				if(newCollection.fr.name != null && newCollection.fr.name.length > 0) {
					newCollection.fr.nameSlug = str.generateNameSlug(newCollection.fr.name);
				}
				if(newCollection.de.name != null && newCollection.de.name.length > 0) {
					newCollection.de.nameSlug = str.generateNameSlug(newCollection.de.name);
				}
				if(newCollection.nl.name != null && newCollection.nl.name.length > 0) {
					newCollection.nl.nameSlug = str.generateNameSlug(newCollection.nl.name);
				}
				if(newCollection.es.name != null && newCollection.es.name.length > 0) {
					newCollection.es.nameSlug = str.generateNameSlug(newCollection.es.name);
				}

				newCollection.uploader = {
					_id: req.user._id,
					name: req.user.firstName + ' ' + (req.user.lastNamePrefix || '') + ' ' + req.user.lastName
				}

				Collection.create(newCollection, function (err, collection) {
					var status, response

					if(err) {
						status = 400;
						response = {message: 'Collection could not be added. Error message: ' + err};
					} else if(!collection) {
						status = 404;
						response = {message: 'Collection not found. '};
					} else {
						status = 201;
						response = {message: 'Collection added. ', collection};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);

				});
			}
		});
	}
};