const path = require('path');
const pdf = require('pdfkit');
const async = require('async');
const _ = require('lodash');

const mongoose = require('mongoose');
const Order = mongoose.model('Order');
const fbOrder = mongoose.model('fbOrder');
const User = mongoose.model('User');
const Shop = mongoose.model('Shop');
const ObjectId = require('mongodb').ObjectId;
const PhoneNumber = require('awesome-phonenumber');

const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const update = require(path.join(__dirname, '..', 'services', 'update.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));
const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));
const orderService = require(path.join(__dirname, '..', 'services', 'order.service'));
const userService = require(path.join(__dirname, '..', 'services', 'user.service'));
const random = require(path.join(__dirname, '..', 'services', 'random.service'));

module.exports.getOrder = function(req, res) {
	console.log('[getOrder controller]');

	if(req && req.params && req.params.nameSlug) {

		Order.findOne({'nameSlug': req.params.nameSlug}).exec(function(err, order) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid order ID. ' + err};
			} else if (!order) {
				status = 404;
				response = {message: 'Order not found. '};
			} else {
				status = 200;
				response = {message: 'OK', order};
			}

			console.log(status + ' ' + response.message);
			res.status(status).json(response);

		});
	}
};

module.exports.getOrders = function(req, res) {
	console.log('[getOrders controller]');
	// default values, because offset and limit are required
	var offset = 0;
	var filterBy, sortBy, limit;

	if(req.query) {

		if(typeof req.query === 'string') {
			req.query = JSON.parse(req.query);
		}

		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			offset = parseInt(req.query.offset);
			delete req.query.offset;
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			limit = parseInt(req.query.limit);
			delete req.query.limit;
		}

		// if any filters are provided in the URL, parse them and add to the filtering params object.
		if(req.query.filter) {
			filterBy = filter(req.query.filter);
			//filterBy = req.query.filter;
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
		if(req.query.sort) {
			sortBy = sort(req.query.sort);
		}

	}

	Order.find(filterBy).sort(sortBy).skip(offset).limit(limit).exec(function(err, orders) {
		var status, response;

		if(err) {
			status = 500;
			response = {message: 'Internal server error. ' + err};
		} else if(!orders || orders.length == 0) {
			status = 204;
			response = {message: 'No orders found with the given search criteria. '};
		} else {
			//avoid that loggedin users (users and retailers) can see orders that are not theirs
			if(req.user.role == "retailer"){
				//-var nameSlug = filterBy['shop.nameSlug'];
				//-console.log("req.user.shops", req.user.shops);
				var shops = [];
				for(s = 0; s < req.user.shops.length; s++){
					shops.push(req.user.shops[s].nameSlug);
					nameSlug = req.user.shops[s].nameSlug;
				}
				if(shops.indexOf(nameSlug) < 0 ){
					status = 401;
					response = {message: "You have no rights to view this order"};
					return res.status(401).json(response);
				}

			}else if(req.user.role == "user"){
				for(i =0; i < orders.length; i++){
					if(orders[i].user[0]._id != req.user.id){
						status = 401;
						response = {message: "You have no rights to view this order"};
						return res.status(401).json(response);
					}
				}
			}
			status = 200;
			response = {message: 'OK', orders};
		}
		return res.status(status).json(response);
	});
};

module.exports.getRetailerOrderList = function(req, res){
	console.log("getRetailerOrderList");

	//extremly complex way to filter orders for the retailer. 
	//https://stackoverflow.com/questions/46362486/conditionally-include-aggregation-pipeline-stages
	var pipeline = [
		{ $match: 
			Object.assign(
			  { 'shop.nameSlug' : req.query.nameSlug },
			
			)
		},
		...(
		  (req.query.status)
			? [
				{
					"$project": {
						"_id" : 1,
						"number" : 1,
						"date":1,
						"comment":1,
						"status": {
							"$slice" : ['$status', -1]
						},
						"user":1,
						"billingDetails":1,
					}
				},
				{ 
					"$match": 
					{ 
						"status.status": (req.query.status instanceof Array)
				 		? { "$in": req.query.status } : req.query.status
					 }
				}
			]
			: [
				{
					"$project": {
						"_id" : 1,
						"number" : 1,
						"date":1,
						"comment":1,
						"status":1,
						"user":1,
						"billingDetails":1
					}
			}]
		  ),
		  {
			$sort: {
				number: -1,
				date: -1
			}
		}
	  ]; 
	Order.aggregate(pipeline).exec(function(err, orders){
		var status, response;

		if(err) {
			status = 500;
			response = {message: 'Internal server error. ' + err};
		} else if(!orders || orders.length == 0) {
			status = 404;
			response = {message: 'No orders found with the given search criteria. '};
		} else {
			//avoid that loggedin users (users and retailers) can see orders that are not theirs
			if(req.user.role == "retailer"){
				var nameSlug = req.query.nameSlug;
				var shops = [];
				for(s = 0; s < req.user.shops.length; s++){
					shops.push(req.user.shops[s].nameSlug);
				}
				if(shops.indexOf(nameSlug) < 0 ){
					status = 401;
					response = {message: "You have no rights to view this order"};
					return res.status(401).json(response);
				}

			}else if(req.user.role == "user"){
				for(i =0; i < orders.length; i++){
					if(orders[i].user[0]._id != req.user.id){
						status = 401;
						response = {message: "You have no rights to view this order"};
						return res.status(401).json(response);
					}
				}
			}
			status = 200;
			response = {message: 'OK', orders};
		}
		return res.status(status).json(response);
	});
	

}

module.exports.addOrder = function(req, res) {
	console.log('[addOrder controller]');

	if(req.body) {

		create(req.body, function(newOrder) {
			if(newOrder) {

				Order.create(newOrder, function (err, order) {
					var status, response

					if(err) {
						status = 400;
						response = {message: 'Order could not be added. Error message: ' + err};
					} else if(!order) {
						status = 404;
						response = {message: 'Order not found. '};
					} else {
						status = 201;
						response = {message: 'Order added. ', order};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);

				});

			}
		});

	}
};

module.exports.updateOrder = function(req, res) {
	console.log('[updateOrder controller]');

	if(req.params && req.params.id && req.body) {

		update(req.body, function(updatedOrder) {
			if(updatedOrder) {
		
				Order.findByIdAndUpdate(req.params.id, {$set: updatedOrder}, {new: true}, function (err, order) {
					var status, response;

					if(err) {
						status = 400;
						response = {message: 'Order could not be added. Error message: ' + err};
					} else if(!order) {
						status = 404;
						response = {message: 'Order not found. '};
					} else {
						status = 200;
						response = {message: 'Order updated. ', order};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);

				});

			}
		});
		
	}
};

module.exports.removeOrder = function(req, res) {
	console.log('[removeOrder controller]');

	if(req.params && req.params.id) {

		Order.findByIdAndRemove({_id: req.params.id}, function(err, order) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid order ID. ' + err};
			} else if(!order) {
				status = 404;
				response = {message: 'Order not found. '};
			} else {
				status = 204;
				response = {message: 'Order deleted. '};
			}

			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}
			// Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
			res.status(status);
		});
	
	}
};

module.exports.createPackageSlip = function(req, res){	
	Order.findOne({_id: req.body.orderId}).exec(function(err, order){
		if(order){
			Shop.findOne({nameSlug: order.shop[0].nameSlug}).exec(function(err, shop){
				if(shop){
					shopService.getShopLogo(shop.nameSlug, 'Dark', null, function(err, logo){
					
						doc = new pdf({autoFirstPage: false});

						doc.info['Title'] = 'Pakbon + retourformulier voor ' + order.number;
						doc.info['Author'] = 'Prismanote';

						var addressLine = order.sendDetails[0].address[0].street + " " + order.sendDetails[0].address[0].houseNumber;
						if(order.sendDetails[0].address[0].houseNumberSuffix){
							addressLine = addressLine + order.sendDetails[0].address[0].houseNumberSuffix;
						}
						var senderAddress = shop.address.street + " "+ shop.address.houseNumber;
						if(shop.address.houseNumberSuffix){
							senderAddress = senderAddress + + shop.address.houseNumberSuffix;
						} 

						doc.addPage({
							margin: 50
						})

						doc.image(logo, {height: 50});

						doc.fontSize(20);
						doc.text("Pakbon", 400, 40, {align: 'right'});
						doc.fontSize(10);
						doc.moveDown(0.5);
						doc.text("Ordernummer: " + order.number, {align: 'right'});
						doc.moveDown(0.3);
						var orderDate = new Date(order.date);
						doc.text("Orderdatum: " + orderDate.getDate() + "-" + orderDate.getMonth() + "-" + orderDate.getFullYear(), {align: 'right'});

						doc.moveDown(2);
						doc.fontSize(10);

						doc.text(order.sendDetails[0].firstName + " " + order.sendDetails[0].lastNamePrefix + " " + order.sendDetails[0].lastName, 50);
						doc.moveDown(0.5);
						doc.text(addressLine);
						doc.moveDown(0.3);
						doc.text(order.sendDetails[0].address[0].postalCode)
						doc.moveDown(0.3);
						doc.text(order.sendDetails[0].address[0].city);
						doc.moveDown(0.3);
						doc.text(order.sendDetails[0].address[0].country);

						doc.moveUp(7.4);
						doc.text("Afzender", 300);
						doc.moveDown(0.5);
						doc.text(shop.name);
						doc.moveDown(0.3);
						doc.text(senderAddress);
						doc.moveDown(0.3);
						doc.text(shop.address.postalCode);
						doc.moveDown(0.3);
						doc.text(shop.address.city);
						doc.moveDown(0.3);
						doc.text(shop.address.country);
						doc.moveDown(3);

						doc.fontSize(18);
						doc.text("Geleverd", 50);

						doc.fontSize(10);
						doc.moveDown();

						for(i = 0; i < order.items.length; i++){
							var item = order.items[i];
							doc.moveDown();
							doc.text(item.quantity, 50);
							doc.moveUp();
							doc.text("x", 60)
							doc.moveUp();
							doc.text(item.name, 70);
						}
						if(order.comment){
							doc.moveDown(3);
							doc.fontSize(18);
							doc.text("Opmerkingen", 50);
							doc.fontSize(10);
							doc.text(order.comment, 50);
						}
						
						//Retourpagina
						doc.addPage({
							margin: 50
						})

						doc.image(logo, {width: 300});

						doc.fontSize(25);
						doc.text("Retourformulier", 380, 40, {align: 'right'});
						

						doc.moveDown(2.5);
						doc.fontSize(12);
						doc.text("Datum: ....................../....................../......................", 50)
						doc.moveDown(2);
						doc.text(order.sendDetails[0].firstName + " " + order.sendDetails[0].lastNamePrefix + " " + order.sendDetails[0].lastName, 50);
						doc.moveDown(0.5);
						doc.text(addressLine);
						doc.moveDown(0.5);
						doc.text(order.sendDetails[0].address[0].postalCode)
						doc.moveDown(0.5);
						doc.text(order.sendDetails[0].address[0].city);
						doc.moveDown(0.5);
						doc.text(order.sendDetails[0].address[0].country);
						doc.moveDown(0.5);
						doc.text(order.billingDetails[0].email);

						doc.moveDown(3);
						doc.text("Ik wil graag")
						doc.moveDown(0.5);
						doc.lineWidth(2);
						doc.circle(60, 367, 7).stroke();
						doc.text("Mijn geld terug", 80);
						doc.moveDown(0.7);
						doc.circle(60, 390, 7).stroke();
						doc.text("Een vervangend model:", 80);
						doc.moveDown(0.7);
						doc.text("____________________________");

						doc.moveUp(6,4);
						doc.text("Ordernummer:", 300);
						doc.moveUp();
						doc.text(order.number, 400);
						doc.moveDown(0.3);
						var orderDate = new Date(order.date);
						doc.text("Orderdatum:", 300);
						doc.moveUp();
						doc.text(orderDate.getDate() + "-" + orderDate.getMonth() + "-" + orderDate.getFullYear(), 400);
						doc.moveDown(0.3);
						doc.text("Modelnummer:", 300);
						doc.moveUp();
						doc.text("_______________", 400);

						doc.moveDown(4);
						doc.text("Reden voor retour", 50);
						doc.moveDown(0.5);
						doc.circle(60, 470, 7).stroke();
						doc.text("Anders dan verwacht", 80);
						doc.moveDown(0.5);
						doc.circle(60, 493, 7).stroke();
						doc.text("De maat is niet goed", 80);
						doc.moveDown(0.5);
						doc.circle(60, 514, 7).stroke();
						doc.text("Andere reden", 80);
						doc.moveDown(0.5);
						doc.text("___________________________________________________________________");
						doc.moveDown(0.5);
						doc.text("___________________________________________________________________");
						doc.moveDown(0.5);
						doc.text("___________________________________________________________________");
						doc.moveDown(0.5);
						doc.text("___________________________________________________________________");

						doc.moveDown(3);
						doc.fontSize(8);
						doc.text("Retourbeleid:", 50)
						doc.moveDown(0.3);
						var terms = 'U heeft het recht om een ongebruikt en onbeschadigd product retour te sturen binnen 14 dagen na ontvangst van het product. ' +
						'Neemt u voor retour contact op met de verkoper. Het geretourneerde product moet in ongebruikte en onbeschadigde staat verkeren. ' +
						'Stuur het product terug in de orginele verpakking met dit Retourformulier. '+
						'U bent verantwoordelijk voor de verzending van het geretourneerde product naar het onderstaande retouradres. '+
						'Producten die kwijtraken gedurende verzending zullen niet vergoed worden. Retour met remboursverzending zal niet worden geaccepteerd. '+
						'Terugbetalingen zullen verwerkt worden binnen een week na het ontvangst van het product naar de rekening van de orginele aankoper. '+
						'Houdt er rekening mee dat extra verplichtingen, belastingen en terugstuurkosten niet gecompenseerd worden. '+
						'Voor landen binnen de Europese Unie zal ook het BTW bedrag teruggestort worden. Het omruilen is afhankelijk van de beschikbaarheid';
						doc.text(terms, {width: 500});

						doc.end();
						doc.pipe(res);
					})
				}
			})
		}
	})
}
module.exports.generatePdf = function(req, res){	

	console.log("hdgfhsd",req.body.items);
	 var showSalesData = req.body.showSalesData;
	 var showComment =req.body.showComment;
	 var showPurchaseData =req.body.showPurchaseData;
	 var flag = req.body.flag;
	 if(req.body.items.repairFinishDateFinal== undefined){
		req.body.items.repairFinishDateFinal ="Not Found";
	 }
	 if(req.body.items.status== undefined){
		req.body.items.status ="Not Found";
	 }
	 if(req.body.items.repairFinishDateFinal== undefined){
		req.body.items.repairFinishDateFinal ="Not Found";
	 }
	 if(req.body.items.priceRange== undefined){
		req.body.items.priceRange ="Not Found";
	 }
	 if(req.body.items.repairFinishDateFinal== undefined){
		req.body.items.repairFinishDateFinal ="Not Found";
	 }
	 if(req.body.items.priceRepairFinal== undefined){
		req.body.items.priceRepairFinal ="Not Found";
	 }
	 if(req.body.crmUser.emailName== undefined){
		req.body.crmUser.emailName ="Not Found";
	 }
	 if(req.body.crmUser.email== undefined){
		req.body.crmUser.email ="Not Found";
	 }
	//  if(req.body.crmUser.items[0].phone[1].mobilePhone== undefined){
	// 	req.body.crmUser.items[0].phone[1].mobilePhone ="Not Found";
	//  }
	//  if(req.body.crmUser.items[0].address[0].street== undefined){
	// 	req.body.crmUser.items[0].address[0].street ="Not Found";
	//  }
	//  if(req.body.crmUser.items[0].address[0].houseNumber== undefined){
	// 	req.body.crmUser.items[0].address[0].houseNumber ="Not Found";
	//  }
	//  if(req.body.crmUser.items[0].address[0].city== undefined){
	// 	req.body.crmUser.items[0].address[0].city ="Not Found";
	//  }
	//  if(req.body.crmUser.items[0].address[0].postalCode== undefined){
	// 	req.body.crmUser.items[0].address[0].postalCode ="Not Found";
	//  }
	 if(req.body.items.repairDate== undefined){
		req.body.items.repairDate ="Not Found";
	 }
	 if(req.body.items.priceRepairman== undefined){
		req.body.items.priceRepairman ="Not Found";
	 }
	 if(req.body.items.itemComment.comment== undefined){
		req.body.items.itemComment.comment ="Not Found";
	 }
	 if(req.body.items.priceRepairman== undefined){
		req.body.items.priceRepairman ="Not Found";
	 }
	 if(req.body.items.productKind== undefined){
		req.body.items.productKind ="Not Found";
	 }
	 if(req.body.items.productMaterial== undefined){
		req.body.items.productMaterial ="Not Found";
	 }
	 if(req.body.items.customTags== undefined){
		req.body.items.customTags ="Not Found";
	 }
	 if(req.body.items.brandName== undefined){
		req.body.items.brandName ="Not Found";
	 }


			Shop.findOne({nameSlug: req.body.shop_slug.nameSlug}).exec(function(err, shop){
				if(shop){
				// 	if(showSalesData && showComment){
					

				// }
			
				
			
				// if(!showPurchaseData && !showComment){
				// 		console.log("Repair Price and  customer details and  remove comment fields  ");
  
				//  }
					shopService.getShopLogo(shop.nameSlug, 'Dark', null, function(err, logo){
					
						doc = new pdf({autoFirstPage: false});

						doc.info['Title'] = 'Pakbon + retourformulier voor ';
						doc.info['Author'] = 'Prismanote';

				

						doc.addPage({
							margin: 50
						})

						doc.image(logo, {height: 30});
						doc.fontSize(10);
						doc.font('Helvetica');
						doc.text("SHOP NAME :" +req.body.shop_slug.nameSlug, 400, 40, {align: 'right'});
						doc.moveDown(0.5);
						doc.moveDown(0.3);
						doc.fontSize(10);
						doc.text(req.body.shop_slug.address,400,130 ,{align: 'right'});
						
						doc.fontSize(10);
						doc.moveDown(0.5);
						doc.moveDown(0.3);
					

						doc.font('Helvetica');
						doc.moveDown(10);
						doc.fontSize(15);
						doc.text('Product details ', 50 ,130,{align:'left'});
						doc.fontSize(10);
						doc.text('Item Status: ' + req.body.items.status, 50 ,150,{align:'left'});
						console.log("showSalesData",showPurchaseData);
						if((showPurchaseData)){
							// console.log("Repair Price and comment fields and removed customer details");
							doc.text("Price estimated: " + req.body.items.priceRange	,50 ,250,{align:'left'});
							doc.text("Price to Pay: " + req.body.items.priceRepairFinal,50 ,270,{align:'left'});
							doc.text("Contact Information Customer ", 50 ,320,{align:'left'});
							doc.moveDown(20);
							doc.fontSize(10);
							doc.text("Name: " + req.body.crmUser.emailName, 50 ,340,{align:'left'});
							doc.moveDown(0.3);
							doc.text("Email Add Customer: " + req.body.crmUser.email,50 ,360,{align:'left'});
							doc.moveDown(0.3);
							if(req.body.crmUser.items[0].phone.length!=0) {
								doc.text("Customer's Phoneno: " + req.body.crmUser.items[0].phone[1].mobilePhone,50 ,380,{align:'left'});
								doc.moveDown(0.3);
							}
							if(req.body.crmUser.items[0].address.length!=0){
								doc.text("Street: " + req.body.crmUser.items[0].address[0].street, 50 ,400,{align:'left'});
								doc.moveDown(0.3);
								doc.text("House no: " + req.body.crmUser.items[0].address[0].houseNumber,50 ,420,{align:'left'});
								doc.moveDown(0.3);
								doc.text("Place : " + req.body.crmUser.items[0].address[0].city ,50 ,440,{align:'left'});
								doc.moveDown(0.3);
								doc.text("Postal Code: " + req.body.crmUser.items[0].address[0].postalCode ,50 ,460,{align:'left'});
							}
							
					 }
					 doc.text("Date Ready Estimated: " + req.body.items.repairDate,50 ,190,{align:'left'});
					 doc.text("Date of repair really finished: " + req.body.items.repairFinishDateFinal, 50 ,210,{align:'left'});
						doc.text("Repair Price: " + req.body.items.priceRepairman,50 ,230,{align:'left'});
					
						if( showComment){
							doc.text("Comments Field: " + req.body.items.itemComment.comment,50 ,290,{align:'left'});
							doc.moveDown(20);
							doc.moveDown(0.5);
						
					}
					doc.fontSize(10);
					doc.text('Product Kind: ' + req.body.items.productKind, 50 ,310,{align:'left'});
					doc.fontSize(10);
					doc.text('Product Material: ' + req.body.items.productMaterial, 50 ,330,{align:'left'});
					doc.fontSize(10);
					doc.text('Custom Tags: ' + req.body.items.customTags, 50 ,350,{align:'left'});
					doc.fontSize(10);
					doc.text('Brand Name: ' + req.body.items.brandName, 50 ,370,{align:'left'});
					doc.text
					("___________________________________________________________________________",50,400);
					
						doc.font('Helvetica','Bold')
						doc.fontSize(15);
					if(flag==1){
						doc.text("----------------------------------------------------------------",50,480);
						doc.image(logo, {height: 50});
						doc.fontSize(10);
						doc.font('Helvetica');
						doc.text("SHOP NAME :" +req.body.shop_slug.nameSlug, 400, 500, {align: 'right'});
						doc.moveDown(0.5);
						doc.moveDown(0.3);
						doc.fontSize(10);
						doc.text(req.body.shop_slug.address,400,520 ,{align: 'right'});
						
						doc.fontSize(10);
						doc.moveDown(0.5);
						doc.moveDown(0.3);
					

						doc.font('Helvetica');
						doc.moveDown(10);
						doc.fontSize(15);
						doc.text('Product details ', 50 ,540,{align:'left'});
						doc.fontSize(10);
						doc.text('Item Status: ' + req.body.items.status, 50 ,560,{align:'left'});
						doc.text("Date Ready Estimated: " + req.body.items.repairDate,50 ,580,{align:'left'});
						doc.text("Date of repair really finished: " + req.body.items.repairFinishDateFinal, 50 ,210,{align:'left'});
						doc.text("Repair Price: " + req.body.items.priceRepairman,50 ,600,{align:'left'});
						doc.text("Price estimated: " + req.body.items.priceRange	,50 ,620,{align:'left'});
						doc.text("Price to Pay: " + req.body.items.priceRepairFinal,50 ,640,{align:'left'});
						doc.text("Comments Field: " + req.body.items.itemComment.comment,50 ,660,{align:'left'});
						doc.moveDown(20);
						doc.moveDown(0.5);
						doc.text("___________________________________________________________________________",50,680);
					
						doc.font('Helvetica','Bold')
						doc.fontSize(15);
						doc.text("Contact Information Customer ", 50 ,700,{align:'left'});
						doc.moveDown(20);
						doc.fontSize(10);
						doc.text("Name: " + req.body.crmUser.emailName, 50 ,720,{align:'left'});
						doc.moveDown(0.3);
						doc.text("Email Add Customer: " + req.body.crmUser.email,50 ,740,{align:'left'});
						doc.moveDown(0.3);
						if(req.body.crmUser.items[0].phone.length!=0) {
						doc.text("Customer's Phoneno: " + req.body.crmUser.items[0].phone[1].mobilePhone,50 ,760,{align:'left'});
						doc.moveDown(0.3);
						}
						if(req.body.crmUser.items[0].address.length!=0){
						doc.text("Street: " + req.body.crmUser.items[0].address[0].street, 50 ,780,{align:'left'});
						doc.moveDown(0.3);
						doc.text("House no: " + req.body.crmUser.items[0].address[0].houseNumber,50 ,800,{align:'left'});
						doc.moveDown(0.3);
						doc.text("Place : " + req.body.crmUser.items[0].address[0].city ,50 ,820,{align:'left'});
						doc.moveDown(0.3);
						doc.text("Postal Code: " + req.body.crmUser.items[0].address[0].postalCode ,50 ,840,{align:'left'});
						}
					}
					 
						doc.end();
						doc.pipe(res);
					})
				}
			})
}


module.exports.completeOrder = function(req,res){
	orderService.completeOrder(req.body.payId, false, function(status, response){
		res.status(status).json(response);
	})
}

module.exports.sendPackage = function(req,res){
	//Status verzonden aan order geven en gebruiker mail sturen dat order verzonden is
	Order.findOne({_id: req.body.orderId}).exec(function(err, order){
		if(order){
			var status = {
				status: "Send",
				date: new Date(),
				comment: "Send using [carrier] with tracking code: [code]"
			};

			order.status.push(status);

			order.save(function(err, result){
				console.log("save", result.user[0]);
				//order is opgeslagen dan mail sturen met status update
				User.findOne({_id: result.user[0]._id}).exec(function(err, user){
					if(!user){
						return res.status(500).json({message: "User not found"})
					}
					Shop.findOne({nameSlug: result.shop[0].nameSlug}).exec(function(err, shop){ 
						var data = {
							user: user,
							order: order,
							shop: shop
						};
						var options = {
							to: user.email,
							subject: 'Er is nieuws over bestelling ' + order.number + '!',
							template: 'order-update'
						};

						orderService.updateOrderPayment(order._id, {status: 'send'}, function(err, result){
							console.log("updateOrderPayment Result", err, result);
							if(err) {
								return res.status(500).json({message: err});
							}

							mailService.mail(options, data, null, function(err, orderSendResult){
								if(err){
									return res.status(500).json({message: err});
								}
							})
							return res.status(200).json({message: "Order status updated!"});				
						})
					})
				})
			})
		}
	})
}

var groupByShop = function(cart) {
    var shops = {};
    // Create a new shop item for each unique shop and add the corresponding items to it
    if(Array.isArray(cart)){
        for(i = 0; i < cart.length; i++) {
            if(!shops[cart[i].shop.nameSlug]) {
                shops[cart[i].shop.nameSlug] = {name: cart[i].shop.name, items: []};
            }

            if(shops[cart[i].shop.nameSlug].items.indexOf(cart[i]) == -1) {
                shops[cart[i].shop.nameSlug].items.push(cart[i]);
            }
        }
        return(shops);
    }
}

module.exports.createOrder = function(req,res){
    console.log("CreateOrder");
	var user, status=0;
	if(req.body.guest){
		//an guest checkout is done by a user which do not have an account. To see his order, cancel or, in case of an failed payment, try again to pay, the user
		//must have an account. So we will check if there is no emailadres registered with the entered email and create an account for the user with an default password which must be resetted
		findOrCreateUser(req.body.billTo, function(err, result){
			if(err){
				status = 500;
				console.log("error in aanroep", err);
				return res.status(err.code).json({message: err.err});
			}else{
				status = 200;
				user = result;
				processOrder(user, req.body, req.headers.origin, function(err, result){
					if(err){
						console.log("processOrderErr", err);
						return res.status(err.code).json({message: err.err})
					}else{
						return res.status(200).json(result);
					}
				})
			}
		})
	}else{
		status = 200;
		user = req.user;
		processOrder(user, req.body, req.headers.origin, function(err, result){
			if(err){
				console.log("processOrderErr", err);
				return res.status(err.code).json({message: err.err})
			}else{
				//clean the cart
				req.session.cart = [];
				return res.status(200).json(result);
			}
		})
	}
}

module.exports.createFbOrder = function(req,res){
		status = 200;
		user = req.user;
		processFbOrder(user, req.body, function(err, result){
			if(err){
				return res.status(err.code).json({message: err.err});
			}else{
				res.send(result);
			}
		})
	
}
processFbOrder = function(user,body,cb){
	var usrDetail = [];
	var fborders = [];
	saveFbOrder(0, body, user, function(err, newOrder){
		if(err){
			cb(err);
		}else{
			fborders.push(newOrder);
			cb(null,fborders);
		}
	})
	
}

processOrder = function(user, body, origin, cb){
	var orderDescription = "";
	var payId = new ObjectId(); //create one id, to pay multiple orders at one time   
	var shopList = [];
	var orders = [];
	var f = 0;
	if(body.cart != undefined){
		shopList = groupByShop(body.cart);
	}else{
		orders[0] = {};
		orders[0].payId = payId;
		shop = [];
		f = 1;
		saveOrder(payId, shop, key, body, user, function(err, newOrder){
			if(err){
				console.log("Err", err);
				callback(err);
			}else{
				//Create an payment to payout the retailer
				orderService.createPayment(newOrder, function(err, result){
					if (err){
						console.log("Error creating payment", err);
					}
				});
				orders.push(newOrder);
				callback();
			}
		})
	}
	
	
	var result = {};

	if(f == 0){
		//the cart is now sorted by shop, now we can make orders for each shop
		async.eachSeries(Object.keys(shopList), function(key, callback){

			var shop = shopList[key];

			saveOrder(payId, shop, key, body, user, function(err, newOrder){
				if(err){
					console.log("Err", err);
					callback(err);
				}else{
					console.log("newOrder", newOrder);
					//Create an payment to payout the retailer
					orderService.createPayment(newOrder, function(err, result){
						if (err){
							console.log("Error creating payment", err);
						}
					});
					orders.push(newOrder);
					callback();
				}
			})
		}, function(err){
			if(err){
				console.log("One or more orders are not saved:", err);
				var error = {
					code: 400,
					err: err
				}
				return cb(err);
			}else{
				console.log("All orders are processed");
				
				redirectUrl(orders, origin, function(url){
					result = {
						success: true,
						order: {
							payId: orders[0].payId,
							count: orders.length,
							redirectUrl: url
						}
					};
					return cb(null, result);
				})
			}		
		})
	} 
}


findOrCreateUser = function(billTo, callback){
	console.log("findOrCreateUser");
	var userData = _.pick(billTo, 'firstName', 'lastNamePrefix', 'lastName','email');
	var error;	
	userService.findUserByEmail(userData.email, function(err, user){
		if(err && err.code == 404){
			//no user found with this e-mailadres

			//fill default user fields
			userData.password = "empty"; //nobody can login with this pass, because it isn't hashed
			userData.passwordSalt = "empty";
			userData.mustResetPassword = true;
			userData.verificationCode = random(8);
			userData.username = userData.email;

			if(billTo.phone){
                var phone = new PhoneNumber(billTo.phone, billTo.country.toUpperCase() || "NL");
                if(phone.isValid()){
                    userData.phone = {
                        countryCode: phone.getRegionCode(),
                        mobilePhone: phone.isMobile() ? phone.getNumber() : null,
                        landLine: phone.isMobile() ? null : phone.getNumber(),
                        fax: null
                    }
                }
            }

			var address = {
				street: billTo.address,
				houseNumber: billTo.houseNumber,
				houseNumberSuffix: billTo.houseNumberSuffix || null,
				postalCode: billTo.zipcode,
				city: billTo.city,
				country: billTo.country,
				
			}
			userData.address = address;

			create(userData, function(newUser){
				if(newUser){
					User.create(newUser, function(err, addedUser){

						var status, response;
						if(err){
							error = {
								code: 500,
								err: 'User could not be added. Error message: ' + err
							}
							console.log("err", error);
							return callback(err)
						}else if(!addedUser){
							error = {
								code: 404,
								response: 'User not found'
							}
							return callback(err);
						}else{
							var options = {
								to: addedUser.email,
								subject: "Uw account op PrismaNote",
								template: "new-account-without-password"
							};

							mailService.mail(options, addedUser, null, function(err, data){
								if(err){
									error = {
										code: 500,
										err: 'error sending mail' + err
									}
									return callback(err);
								}
								//account is created and mail is send, continue with this order
								User.findOne({email: addedUser.email}).exec(function(err, user){
									if(user){
										return callback(null, user);
									}else{
										error = {
											code: 500,
											err: err
										};
										return callback(error);
									}	
								})
							})	
						}
						if(err){
							error = {
								code: 500,
								err: err
							}
							return callback(error);
						}
					})
				}
			})
		}else if(err && err.code == 409){
			var err = {
				msg: "Account is not activated",
				code: 409
			};
			error = {
				code: 200,
				err: err
			}
			return callback(error);
		}else if(err){
			return callback(err);
		}else if(user){
			var err = {
				msg: "E-mailadres already in use",
				code: 401
			}
			error = {
				code: 200,
				err: err
			}
			return callback(error);
		}
	})
}

redirectUrl = function(orders, origin, callback){

	origin = process.env.NODE_ENV != 'production' ? settings.mollie.redirect : origin;

	if(orders.length > 1){
		return callback(origin + '/payment'); //default page
	}else{
		if(!orders.shop){
			return callback(origin + '/payment');
		}else{
			shopService.getShopUrl({nameSlug: orders[0].shop[0].nameSlug}, origin, function(shopUrl){
				return callback(shopUrl + '/payment');
			})
		}
		
	}
}
saveFbOrder = function(payid, body, user, callback){
	var orderNumber = 0;
	var fborder = new fbOrder();
	var payId = new ObjectId();
	var date = new Date();
	console.log("SaveFbOrder",fborder,date);
	if(body.shop){
		fborder.shop = {
			noShop: true,
			name: body.shop.name,
			emailAddress: body.shop.email,
			address: body.shop.address,
		}
	}
	if(body.campaign){
		fborder.campaign = {
			_id: body.campaign._id,
			name: body.campaign.name
		}
	}
	//TODO: add shop to fbOrder when there is a normal shop to the user
	fborder.status = {
		status: "Created",
		comment: "",
		date: new Date()
	};
	fborder.user = {
		_id: user._id,
		firstName: user.firstName,
		lastName: user.lastName,
		email: user.email,
	}
	fborder.date = date;	fborder.date = date;
	fborder.payId = payId;
	fborder.save(function(err, result){
		if(err){
			console.log("Error during saving order", err);
			return callback(err);
		}else{
			
			return callback(null, result);
		}
	})
}
saveOrder = function(payId, shop, nameSlug, body, user, callback){
	console.log("SaveOrder");
    var orderNumber = 0;

    Order.findOne().sort({_id: -1}).exec(function(err, latestOrder){
        if(latestOrder && latestOrder.number){
            orderNumber = latestOrder.number.split("-")[1];
        }

        var order = new Order();
        var date = new Date();

        order.number = date.getFullYear().toString() + date.getMonth().toString() + "-" + (parseInt(orderNumber)+1);
        
        order.date = date;
        order.payId = payId;
        order.status = {
            status: "Created",
            comment: "",
            date: new Date()
        };
        order.comment = body.comment;

        order.shop = {
            name: shop.name,
            nameSlug: nameSlug
        }

        order.billingDetails = {
            //Paymethod is unknown at this time
            shippingprice: 0, //free shipping
            phone: body.billTo.phone,
            firstName: body.billTo.firstName,
            lastName: body.billTo.lastName,
            lastNamePrefix: body.billTo.lastNamePrefix,
            email: body.billTo.email,
            address: {
                street: body.billTo.address,
                houseNumber: body.billTo.houseNumber,
                houseNumberSuffix: body.billTo.houseNumberSuffix || null,
                postalCode: body.billTo.zipcode,
                city: body.billTo.city,
				country: body.billTo.country,
				phone: body.billTo.phone
            }
        }

        order.sendDetails = {
            firstName: body.sendTo.firstName,
            lastName: body.sendTo.lastName,
            lastNamePrefix: body.sendTo.lastNamePrefix,
            phone: body.sendTo.phone,
            address: {
                street: body.sendTo.address,
                houseNumber: body.sendTo.houseNumber,
                houseNumberSuffix: body.sendTo.houseNumberSuffix || null,
                postalCode: body.sendTo.zipcode,
                city: body.sendTo.city,
				country: body.sendTo.country,
				phone: body.sendTo.phone
            }
        }

        order.user = {
            _id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            lastNamePrefix: user.lastNamePrefix
		}
		
		if(user.address.length == 0 || ( (!user.phone || typeof user.phone == 'undefined') && body.billTo.phone)){
			//Add current sendTo address to the user
			User.findOne({_id: user._id}).exec(function(err, user){
				if(user){
					if(user.address.length == 0){
						var address = {
							street: body.sendTo.address,
							houseNumber: body.sendTo.houseNumber,
							houseNumberSuffix: body.sendTo.houseNumberSuffix || null,
							postalCode: body.sendTo.zipcode,
							city: body.sendTo.city,
							country: body.sendTo.country,
							phone: body.sendTo.phone
						};
	
						user.address.push(address);
					}
					if(user.phone.length == 0){
						var phone = new PhoneNumber(body.billTo.phone, body.billTo.country.toUpperCase() || "NL");
						if(phone.isValid()){
							user.phone = {
								countryCode: phone.getRegionCode(),
								mobilePhone: phone.isMobile() ? phone.getNumber() : null,
								landLine: phone.isMobile() ? null : phone.getNumber(),
								fax: null
							}
						}
					}
					
					user.save();
				}
			})
		}

        //Add items to the order
		var orderItems = [];
		if(shop != undefined){
			for(p=0; p<shop.items.length; p++){
				var product = shop.items[p];
				var orderItem = {
					_id: product._id,
					name: product.name,
					brand: product.brand && product.brand.length > 0 ? product.brand[0].name : "unknown",
					price: product.price,
					quantity: product.quantity
				}
				orderItems.push(orderItem);
			}
			order.items = orderItems;
		}
        

        order.save(function(err, result){
            if(err){
                console.log("Error during saving order", err);
                return callback(err);
            }else{
				
                return callback(null, result);
            }
        })
    })
}

module.exports.saveFbpaymentId = function(req, res){
	var paymentid = req.body.paymentid;
	var payID = req.body.payID;
	var amount = req.body.amount;

	fbOrder.findOneAndUpdate(
		{payId: payID}, {
			paymentid: paymentid,
			amount: amount
		}, {
			upsert: true, 
			new: true
		}, function (err, doc) {
		if (err) return res.send(500, { error: err });
		console.log("saved");
		return res.send("succesfully saved");
	});

}

module.exports.setNewPayId = function(req, res){
	//Generate an new PayId for one single order

	var orderNumber = req.body.orderNumber;

	orderService.generateAndSavePayId(orderNumber, function(err, result){
		if(err){
			return res.json({err: err});
		}

		res.json({payId: result});
	})
}

module.exports.cancelOrder = function(req,res){
	console.log("CancelOrder");
	//Set orderstatus to cancelled
	var orderNumber = req.body.orderNumber;
	//check if the user has cannceld the order or the retailer
	var comment = 'Cancelled on users request';
	var type = 'user';
	if(req.body.shopRequest){
		comment = 'The retailer has cannceled this order';
		type = 'shop';
	}

	if(orderNumber){
		orderService.updateOrderStatus(orderNumber, {status: 'Cancelled', date: new Date(), comment: comment}, function(err, result){
			if(err){
				return res.json({message: err});
			}else{
				if(type =='shop'){
					//send mail to the user
					Order.findOne({number: orderNumber}).exec(function(err, order){
						User.findOne({_id: order.user[0]._id}).exec(function(err, user){
							if(!user){
								return res.status(500).json({message: 'User not found'});
							}
							Shop.findOne({nameSlug: order.shop[0].nameSlug}).exec(function(err, shop){
							
								var data = {
									user: user,
									shop: shop,
									order: order
								};
								var options = {
									to: user.email,
									subject: 'Er is nieuws over bestelling ' + order.number + '!',
									template: 'order-update'
								};

								orderService.updateOrderPayment(order._id, {status: 'cancelled', comment: comment}, function(err, result){
									console.log("updateOrderPayment Result", err, result);
								})

								//send mail with the new status
								mailService.mail(options, data, null, function(err, orderSendResult){
									if(!err){
										return res.status(200).json({message: 'cancellation saved'})
									}
								})
							})
						})
					})
				}else{
					//send mail to shop owner?
				}
			}
		})
	}else{
		return res.status(404).json({message: 'no orderNumber given'});
	}
}

module.exports.returnOrder = function(req, res){
	var orderNumber = req.body.orderNumber;

	if(orderNumber){
		orderService.updateOrderStatus(orderNumber, {status: 'Returned', date: new Date(), comment: 'Replaced with new product'}, function(err, result){
			if(err){
				return res.status(500).json({message: err});
			}else{
				//Get complete order here, for the latest orderstatus
				Order.findOne({number: orderNumber}).exec(function(err, order){
					User.findOne({_id: order.user[0]._id}).exec(function(err, user){
						Shop.findOne({nameSlug: order.shop[0].nameSlug}).exec(function(err, shop){
							
							var data = {
								user: user,
								shop: shop,
								order: order
							};
							var options = {
								to: user.email,
								subject: 'Er is nieuws over bestelling ' + order.number + '!',
								template: 'order-update'
							};
							//send mail with the new status
							mailService.mail(options, data, null, function(err, orderSendResult){
								if(!err){
									return res.status(200).json({message: 'new order status saved'})
								}
							})
						})
					})
				})
			}
		})
	}else{
		return res.status(404).json({message: 'no orderNumber given'});
	}
}
