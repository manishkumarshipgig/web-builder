const path = require('path');
const mongoose = require('mongoose');
const Shop = mongoose.model('Shop');
const Order = mongoose.model('Order');
const Payment = mongoose.model('Payment');
const ObjectId = require('mongodb').ObjectId;

const async = require('async');
const DateDiff = require('date-diff');
const SEPA = require('sepa');
const pdf = require('pdfkit');

const orderService = require(path.join(__dirname, '..', 'services', 'order.service'));
const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));

module.exports.getFinancialDashboard = function(req,res){
    console.log("getFinancialDashboard");
    //orders ophalen
    //van alle verzonden en afgeronde orders de betaling ophalen uit payment.model

    if(req && req.query.shopSlug){
        shopService.getShopFromSlug(req.query.shopSlug, function(err, shop){
            var status, response;

            var outStandingOrders = 0;
            var nextPaymentDate = new Date();
            var nextPayment =0;
            var revenue = 0;
            var expectedRevenue = 0;
            var orderCount =0;

            if(err) {
                status = 500;
                response = {message: 'Internal server error. ' + err};
                return res.status(status).json(response);
            }

            Payment.find({shopId: shop._id}).exec(function(err, payments){
                if(err) {
                    status = 500;
                    response = {message: 'Internal server error. ' + err};
                    return res.status(status).json(response);
                }

                async.forEachOf(payments, function(payment, i, paymentDone){
                    parsePayment(payment, i, function(err, paymentRevenue, paymentExpectedRevenue){    
                        orderCount= i;  
                        if(err){
                            console.log("ERROR:", err);
                        }

                        if(!paymentRevenue && !paymentExpectedRevenue){
                            paymentDone();
                        }

                        if(paymentRevenue){
                            revenue = revenue + paymentRevenue;
                            paymentDone();
                        }

                        if(paymentExpectedRevenue){
                            expectedRevenue = expectedRevenue + paymentExpectedRevenue;
                            outStandingOrders++;
                            paymentDone();
                        }
                    })
                }, function(){
                    Payment.aggregate([
                        {
                            $lookup: {
                                from: "orders",
                                localField: "orderId",
                                foreignField: "_id",
                                as: "order"
                            }
                        },
                        {"$match": 
                            {
                                shopId: shop._id, 
                                payoutDate: {
                                    $ne: null,
                                    $gt: new Date(new Date().toISOString())
                                },
                                completed: {
                                    $eq: false
                                },
                                'order.status.status' : {$in : ['Send']}
                            }
                        },
                        {
                            $sort: {
                                payoutDate: -1,
                                _id: -1
                            }
                        },
                        {
                            $project: 
                            {
                                year: {$year: "$payoutDate"},
                                month: {$month: "$payoutDate"},
                                dayOfMonth: {$dayOfMonth: "$payoutDate"},
                                nextPayoutDate: {$dateToString: {format: '%d-%m-%Y', date: "$payoutDate"}},
                                amount: {$sum: '$amount'},
                            }
                        },  
                        {
                            $group: 
                            {
                                _id: {
                                    year: '$year',
                                    month: '$month',
                                    dayOfMonth: '$dayOfMonth'
                                },
                                amount: {
                                    $sum: '$amount'
                                },
                                count: {
                                    $sum: 1
                                },
                                nextPayoutDate: {
                                    $first: "$nextPayoutDate"
                                }
                            }
                        },                  
                        {
                            $limit: 1
                        }
                    ]).exec(function(err, payments){
                        var nextPaymentData = payments[0];

                        var dashboard = {
                            outStandingOrders: outStandingOrders,
                            nextPaymentData: nextPaymentData || null,
                            payment: nextPayment,
                            revenue: revenue,
                            expectedRevenue: expectedRevenue
                        }
                        
                        status = 200;
                        response = {message: 'OK', dashboard};
                        return res.status(status).json(response);
                   })

                    
                })
            })
        })
    }else{
        return res.status(500).json({message: 'No details provided'});
    }
}


function parsePayment(payment, i, callback){
    var revenue = 0, expectedRevenue =0;
    if(payment.amount > 0){
        if(payment.completed && payment.exported){
           return callback(null, payment.amount);
        }else{
            Order.findOne({_id: payment.orderId}).exec(function(err, order){
                if (err){
                    return callback(err);
                }
                if(orderService.getOrderStatus(order) == "Send"){
                    return callback(null, null, payment.amount);
                }else{
                    return callback(null, null, null);
                }
            })
        }
    }else{
        return callback(null, null, null);
    }
}

module.exports.getOrdersWithPayments = function(req,res){
    console.log("getOrdersWithPayments");
    if(req && req.query.shopSlug){
        Order.find(
            {
                $and: [
                    {'shop.nameSlug': req.query.shopSlug},
                    {'status.status': {"$in" : ['Send', 'Granted', "Returned"]}}
                ]

            }).exec(function(err, orders) {
            var status, response;
    
            if(err) {
                status = 500;
                response = {message: 'Internal server error. ' + err};
                return res.status(status).json(response);
            } else if(!orders || orders.length == 0) {
                status = 404;
                response = {message: 'No orders found with the given search criteria. '};
                return res.status(status).json(response);
            } else {
                //avoid that loggedin users (users and retailers) can see orders that are not theirs
                var nameSlug = req.query.shopSlug;
                var shops = [];
                for(s = 0; s < req.user.shops.length; s++){
                    shops.push(req.user.shops[s].nameSlug);
                }
                if(shops.indexOf(nameSlug) < 0 ){
                    status = 401;
                    response = {message: "You have no rights to view this order"};
                    return res.status(401).json(response);
                }

                var ordersWithPayment = [];

                async.forEach(orders, function(order, ordersDone){
                    var newOrder = {};
                    orderService.getPayment(order._id, function(err, payment){
                        if (err){
                            status = 500;
                            response = {message: 'Internal server error. ' + err};
                            return res.status(status).json(response);
                        }
                        
                        if(payment){
                            newOrder.payment = {
                                orderId: payment.orderId,
                                amount: payment.amount,
                                contribution: payment.contribution,
                                shippingCosts: payment.shippingCosts,
                                date: payment.date,
                                payoutDate: payment.payoutDate,
                                shopId: payment.shopId,
                                invoiceNumber: payment.invoiceNumber,
                                returned: payment.returned,
                                completed: payment.completed,
                                exported: payment.exported,
                                status: payment.status,
                                _id: payment._id
                            };
                        }
                        newOrder.status = order.status;
                        newOrder._id = order._id;
                        newOrder.date = order.date;
                        newOrder.shop = order.shop;
                        newOrder.paymentCreated = order.paymentCreated;
                        newOrder.number = order.number;
                        newOrder.handled = order.handled;

                        ordersWithPayment.push(newOrder);
                        
                        ordersDone();              
                    })                   
                }, function(){
                    //ordersDone
                    if(ordersWithPayment.length == orders.length){                            
                        status = 200;
                        response = {message: 'OK', ordersWithPayment};
                        return res.status(status).json(response);
                    }
                })
            }
        });
    }else{
        return res.status(500).json({message: 'No details provided'});
    }
}

module.exports.handlePayments = function(req, res){
    console.log("handlePayments");

    //Loop through all payments
    //create an invoice number
    //create an sepa transfer
    //add transfer to batch file
    //set payment to completed

    Payment.find().exec(function(err, payments){
        if (err) {
            console.log("error getting payments", err);
            return res.status(500).json({message: 'Erroring getting payments', err});
        }

        if(payments){

            var resultArray = [];
            var counter=0;

            var isoCountry = 'NL';
            var checkDigit = '60';
            var businessCode = 'ZZZ';
            var bankCode = 'ABNA';
            var kvkNumber = '11030402';
            var bankNumber = '0415788137'
            var bic = 'ABNANL2A';

            var iban = isoCountry + checkDigit + bankCode + bankNumber;
           
            if(!SEPA.validateIBAN(iban)){
                console.log("IBAN is not valid");
                return res.status(500).json({message: 'IBAN not valid: ' + iban});
            }

            var date = new Date();
            var doc = new SEPA.Document('pain.001.001.03');
            doc.grpHdr.id = "PM."+date.getFullYear().toString()+"."+date.getDate().toString();
            doc.grpHdr.created = date.toISOString();
            doc.grpHdr.initiatorName = "PrismaNote";

            var info = doc.createPaymentInfo();
            info.requestedExecutionDate = new Date();
            info.debtorIBAN = iban;
            info.debtorBIC = bic;
            info.debtorName = "Excellent Electronics";
            info.debtorId = isoCountry + checkDigit + businessCode + bankNumber;
            doc.addPaymentInfo(info);

            async.eachSeries(payments, function(payment, paymentDone){             
                var diff = new DateDiff(new Date(payment.payoutDate), new Date());
                if(payment.payoutDate && payment.amount > 0 && payment.completed == false && payment.exported == false && diff.days() <= 0){
                    //payment has amount, is not completed and is not exported, create an SEPA transfer, and set the payment to completed
                    //but first create an invoicenumber
                        orderService.updateOrderPayment(payment.orderId, {generateInvoiceNumber: true}, function(err, result){
                            if(err){
                                console.log("error updating payment", err);
                            }
                            result.save(function(err, payment){
                                console.log("payment invoice", payment.invoiceNumber);
                                if(err){
                                    console.log("error saving payment", err);
                                }

                                Shop.findOne({_id: payment.shopId}).exec(function(err, shop){
                                    if(shop && shop.bankAccountNumber && shop.accountHolder && shop.bicCode){
                                        var tx = info.createTransaction();
                                        tx.creditorName= shop.accountHolder;
                                        tx.creditorIBAN= shop.bankAccountNumber;
                                        tx.creditorBIC= shop.bicCode;
                                        tx.mandateId = "PM." + shop.nameSlug + date.getDate().toString();
                                        tx.mandateSignatureDate = new Date();
                                        tx.amount = Math.round(payment.amount * 100) /100;
                                        tx.remittanceInfo = "Prismanote uitbetaling: "+ payment.invoiceNumber;
                                        tx.end2endId = "PM." + shop.nameSlug +"."+payment.invoiceNumber;
                                        info.addTransaction(tx);
                                    }else{
                                        var result = {
                                            paymentID: payment._id,
                                            orderId: payment.orderId,
                                            status: payment.status,
                                            message: "shop does not have an iban, accountholder or biccode",
                                            shop: shop.nameSlug
                                        }
                                        resultArray.push(result);
                                        console.log("shop does not have an iban, accountholder or biccode", shop.nameSlug);
                                        paymentDone();
                                    }

                                    orderService.updateOrderPayment(payment.orderId, {status: 'completed'}, function(err, result){
                                        orderService.updateOrderStatusById(payment.orderId, {status: 'Granted', date: new Date(), comment: null});
                                        var result = {
                                            paymentID: payment._id,
                                            orderId: payment.orderId,
                                            status: payment.status,
                                            message: "payment exported",
                                        }
                                        resultArray.push(result);
                                        counter++;
                                        paymentDone();
                                    })
                                })
                            })    
                        })    
                }else{
                    var result = {
                        paymentID: payment._id,
                        orderId: payment.orderId,
                        status: payment.status,
                        message: "order already processed"
                    }
                    resultArray.push(result);
                    paymentDone();
                }
            }, function(){
                if(resultArray.length == payments.length){
                    //console.log("Result", resultArray);
                    if(counter == 0){
                        res.status(200).json({"message":"No orders to export", resultArray});
                    }else{
                        res.set({"Content-Disposition":"attachment; filename=\"sepa.xml\""});
                        res.send(doc.toString());
                    }
                }
             })
        }
    })
}


module.exports.generateInvoicePayment = function(req, res){
    console.log("generateInvoicePayment");

    if(req && req.query.paymentId){
        Payment.findOne({_id: ObjectId(req.query.paymentId)}).exec(function(err, payment){
            if (err){
                status = 500;
                response = {message: 'Internal server error. ' + err};
                return res.status(status).json(response);
            }
            if(payment){
                Shop.findOne({_id: payment.shopId}).exec(function(err, shop){
                    if (err){
                        status = 500;
                        response = {message: 'Internal server error. ' + err};
                        return res.status(status).json(response);
                    }
                    Order.findOne({_id: payment.orderId}).exec(function(err, order){
                        if (err){
                            status = 500;
                            response = {message: 'Internal server error. ' + err};
                            return res.status(status).json(response);
                        }

                        getPaymentDate(payment, function(paymentDate){   
                   
                            var logo = "app/dist/images/prismanote_logo_large.png";

                            doc = new pdf({autoFirstPage: false});

                            doc.info['Title'] = 'Factuur dienstverlening PrismaNote ' + payment.invoiceNumber;
                            doc.info['Author'] = 'PrismaNote';

                            doc.addPage({
                                margin: 50
                            })

                            doc.image(logo, {width: 300});
                            doc.fontSize(12);
                            doc.text("Middelweg 8d");
                            doc.moveDown(0.3);
                            doc.text("4147AV Asperen");

                            doc.fontSize(20);
                            doc.text("Creditfactuur", 400, 40, {align: 'right'});
                            doc.fontSize(10);
                            doc.moveDown(0.1);
                            doc.text("Dienstverlening PrismaNote", {align: 'right'});
                            doc.moveDown();
                            doc.text("Factuurnummer: " + payment.invoiceNumber, {align: 'right'});
                            doc.moveDown(0.3);
                            doc.text("Datum: " + paymentDate.getDate() + "-" + paymentDate.getMonth() + "-" + paymentDate.getFullYear(), {align: 'right'});
                            doc.moveDown();
                            doc.text("Betreft: order " + order.number, {align: 'right'});


                            doc.moveDown(2);
                            doc.fontSize(10);
                            doc.text(shop.name, 50);
                            doc.moveDown(0.3);
                            doc.text(shop.address.street + " " + shop.address.houseNumber + (shop.address.houseNumberSuffix ? shop.address.houseNumberSuffix : ""));
                            doc.moveDown(0.3);
                            doc.text(shop.address.postalCode)
                            doc.moveDown(0.3);
                            doc.text(shop.address.city);
                            doc.moveDown(0.3);
                            doc.text(shop.address.country);

                            doc.moveTo(50, 240).lineTo(570, 240).stroke();
                            
                            doc.moveDown(2.0);   
                            var total = 0;

                            for(i = 0; i < order.items.length; i++){
                                var item = order.items[i];
                                
                                doc.font('Helvetica-Bold').text(item.name, 50);
                                doc.moveDown(0.3);
                                doc.font('Helvetica').text("Aantal: ", 100);
                                doc.moveUp();
                                doc.text(item.quantity, 200);
                                doc.moveDown(0.3);
                                doc.text("Prijs per stuk: ", 100);
                                doc.moveUp();
                                doc.text("€ " + item.price.toFixed(2), 200);
                                doc.moveDown(0.3);

                                doc.text("Totaal te ontvangen: " , 100);
                                doc.moveUp();
                                var lineTotal = item.quantity * item.price;
                                total = total + lineTotal;
                                doc.text("€ " + lineTotal.toFixed(2), 200);
                                doc.moveDown();
                            }
                            if(payment.contribution || payment.shippingCosts){
                                doc.moveDown();
                                doc.font('Helvetica-Bold').text("Verder in te houden", 50);
                                doc.moveDown(0.3);

                                
                                if(payment.contribution != 0){
                                    doc.font('Helvetica').text("Bemiddelingsbijdrage: ");
                                    doc.moveUp();
                                    doc.text("€ " + payment.contribution, 160);
                                    doc.moveDown(0.3);
                                }
                                if(payment.shippingCosts != 0){
                                    doc.text("Verzendkosten: ", 50);
                                    doc.moveUp();
                                    doc.text("€ " + payment.shippingCosts, 160);
                                    doc.moveDown(0.3);
                                }
                                var involve = payment.contribution + payment.shippingCosts;
                                doc.text("Totaal: ", 50);
                                doc.moveUp();
                                doc.text("- € " + involve.toFixed(2), 154);
                            }

                            total = total - payment.contribution - payment.shippingCosts;
                            
                            doc.moveDown();
                            doc.text("Het te ontvangen bedrag (€ " + total.toFixed(2) + ") wordt gestort op " + shop.bankAccountNumber + " onder vermelding van factuurnummer " + payment.invoiceNumber + ".", 50);
                            doc.moveDown(2);
                            doc.text("Met vriendelijke groeten");
                            doc.text("PrismaNote");


                            doc.end();
                            doc.pipe(res);
                        })
                    })
                })
            }
        })
    }else{
        return res.status(500).json({message: 'No details provided'});
    }
}

function getPaymentDate(payment, callback){
    var i = payment.status.length;
    while(i--){
        if(payment.status[i].status == 'completed'){
            return callback(new Date(payment.status[i].date));
        }
    }
}



