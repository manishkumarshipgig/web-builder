const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const Brand = mongoose.model('Brand');
const Product = mongoose.model('Product');

const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const update = require(path.join(__dirname, '..', 'services', 'update.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));

module.exports.getBrand = function(req, res) {
	console.log('[getBrand controller]');

	if(req && req.params && req.params.nameSlug) {

		var query = Brand.findOne({'nameSlug': req.params.nameSlug})
		
		query.exec(function(err, brand) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid brand ID. ' + err};
			} else if (!brand) {
				status = 404;
				response = {message: 'Brand not found. '};
			} else {
				status = 200;
				response = {message: 'OK', brand};
			}

			console.log(status + ' ' + response.message);
			res.status(status).json(response);

		});
	}
};

module.exports.getBrands = function(req, res) {
	console.log('[getBrands controller]');
	// default values, because offset and limit are required
	var offset = 0;
	var filterBy = {}, sortBy = {}, limit = 0;
	console.log("req.query", req.query);
	if(req.query) {

		if(typeof req.query === 'string') {
			req.query = JSON.parse(req.query);
		}

		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			offset = parseInt(req.query.offset);
			delete req.query.offset;
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			limit = parseInt(req.query.limit);
			delete req.query.limit;
		}

		// if any filters are provided in the URL, parse them and add to the filtering params object.
		if(req.query.filter) {
			filterBy = filter(req.query.filter);
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
		if(req.query.sort) {
			sortBy = sort(req.query.sort);
		}

		if(req.query.lng && req.query.lat) {
			// TODO: find brands close to the given coordinates. 
		}
	}

	var query;

	if(req.query.hasProducts) {
		Product.distinct("brand.nameSlug")
			.then(function(brandsWithProducts) {
				if(filterBy.nameSlug && filterBy.nameSlug.$in) {
					filterBy.nameSlug.$in.filter(nameSlug => brandsWithProducts.includes(nameSlug));
				} else {
					filterBy.nameSlug = {$in: brandsWithProducts};
				}
				findBrands();
			})
			
			.catch(function(reason) {
				console.log(reason);
			});
	} else {
		findBrands();
	}


	function findBrands() {
		if(filterBy.nameSlug){
			if(filterBy.nameSlug.$in.length == 0)
				delete filterBy.nameSlug;
		}
		console.log("Brands FilterBy = ",filterBy);
		query = Brand.find(filterBy);

/*		if(req.query.short) {
			query.select("name nameSlug");
		}*/

		query.exec(function(err, brands) {
			var status, response;

			if(err) {
				status = 500;
				response = {message: 'Internal server error. ' + err};
			} else if(!brands || brands.length == 0) {
				status = 404;
				response = {message: 'No brands found with the given search criteria. '};
			} else {
				status = 200;
				response = {message: 'OK', brands};
			}
			
			console.log(status + ' ' + response.message);
			res.status(status).json(response);

		});
	}
};

module.exports.changeImage = function(req, res){
	if(req.body.brandId && req.files.file){
		upload.uploadFile(req.files.file, 'brands', null, function(err, result){
			if (err) {
				console.log('erro', err);
				return res.status(500).json({message: 'error uploading file: ' + err})
			}
			console.log("result", result);
			res.status(200).json({file: result});
		})
	}
}

module.exports.addImage = function (req, res) {
	if (req.body.crmUserId || req.body.userId) {
		if (req.body.friendlyFileName) {
				// this function is used for 'Repair ready' emails
				var filename = stringService.slugify(new Date().toISOString() + path.extname(req.files.file.originalFilename));
				var originalFileName = filename;
				var filename = originalFileName;
				var comment= req.body.comment;
		} else {
				// this function is used for ALL other emails with photo's
				var originalFileName = req.body.fileName;
				var filename = originalFileName;
				var comment= req.body.comment;
				console.log("filename (not friendly)", filename);
		}

		upload.uploadFile(req.files.file, 'loyality-photo', { fileName: filename }, function (err, result) {
			if (err) {
				return res.status(500).json({ message: 'error uploading file: ' + err })
			}
			crmUser.findOne({ _id: req.body.crmUserId }).exec(function (err, crmUser) {
				if (crmUser) {
					var photo = {
							src: result
					}

					crmUser.items[crmUser.items.length - 1].photo.src = "";
					crmUser.items[crmUser.items.length - 1].photo.push(photo);
					//crmUser.items[crmUser.items.length - 1].comment = comment;

					// will become obsolete
					crmUser.photo.push(photo);
					crmUser.comment = comment;

					console.log("crmUser", crmUser);

					crmUser.save(function (err, saveResult) {
						if (!err) {
								return res.status(200).json({ file: saveResult });
						} else {
								console.log("Error during updating crmUser: " + err);
								return res.status(200).json({ message: "Error during updating crmUser: " + err });
						}
					})
				} else {
					console.log("crmUser not found");
					return res.status(404).json({ message: "crmUser not found" });
				}
			})
		})
	} else {
		console.log("no user id found");
		return res.status(404).json({ message: "no user id found" });
	}
}

module.exports.changeBrandImages = function(req, res){
	//Download brand images and upload to Amazon S3 bucket
	console.log("changeBrandImages");

	var limit = parseInt(req.query.limit);
	var skip = parseInt(req.query.skip);

	console.log("limit", limit, "skip", skip);

	Brand.find().sort({id: -1}).skip(skip).limit(limit).exec(function(err, brands){
		
		console.log("aantal merken", brands.length);
		for(var i = 0; i < brands.length; i++){
			if(brands[i].images.length > 0){

				for(var b=0; b < brands[i].images.length; b++){
					var image = brands[i].images[b];
					
					getImageUrl(image.src, brands[i], i, b, function(err, result, brand, i, b){
						if(!err){
							upload.uploadStream(result, 'brands', {fileName: image.alt ? image.alt + "_" + b : brand.name + "_" + b}, function(err, data){
								if(err){
									console.log("error uploading", err);
									//resultArray.push(err);

									var data = err.statusCode + ";" + err.statusMessage + ";" + err.error + ";" + err.url + "\r\n";
									
									fs.appendFile('errors_brands.csv', data, function(err){
										if (err) throw err;
										
									})

								}else{
									console.log("image uploaded", i, b);
									brand.images[b].src = data;

									brand.save(function(err, result){
										if(err){
											console.log("error during saving", err);
										}else{
											console.log("updated!", brand.name);
										}
									})
								}
							})
						}else{
							console.log("error!", err);
						}
					})
				}
			}
		}
	})
	res.send("ok");
}

getImageUrl = function(src, brand, i, b, callback){
	if(!src){
		return callback("No src provided " + b.toString());
	}
	if(src.indexOf("brands/") !== -1){
		return callback("image already uploaded " + b.toString());
	}

	return callback(null, "https://s3-eu-west-1.amazonaws.com/prismanote" + src, brand, i, b)

}

module.exports.addBrand = function(req, res) {
	console.log('[addBrand controller]');

	if(req.body) {

		create(req.body.brand, function(newBrand) {
			if(newBrand) {

				Brand.create(newBrand, function (err, brand) {
					var status, response

					if(err) {
						status = 400;
						response = {message: 'Brand could not be added. Error message: ' + err};
					} else if(!brand) {
						status = 404;
						response = {message: 'Brand not found. '};
					} else {
						status = 201;
						response = {message: 'Brand added. ', brand};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);

				});

			}
		});

	}
};
module.exports.addBrands = function(req, res) {
	console.log('[addBrands controller]');

	if(req.body) {

		create(req.body.brand, function(newBrand) {
			if(newBrand) {

				Brand.create(newBrand, function (err, brand) {
					var status, response

					if(err) {
						status = 400;
						response = {message: 'Brand could not be added. Error message: ' + err};
					} else if(!brand) {
						status = 404;
						response = {message: 'Brand not found. '};
					} else {
						status = 201;
						response = {message: 'Brand added. ', brand};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);

				});

			}
		});

	}
};

module.exports.updateBrand = function(req, res) {
	console.log('[updateBrand controller]');
	if(req.params && req.body.brand._id && req.body.brand) {

		update(req.body.brand, function(updatedBrand) {
			if(updatedBrand) {

				Brand.findByIdAndUpdate(req.body.brand._id, {$set: updatedBrand}, {new: true}, function (err, brand) {
					var status, response;

					if(err) {
						status = 400;
						response = {message: 'Brand could not be added. Error message: ' + err};
					} else if(!brand) {
						status = 404;
						response = {message: 'Brand not found. '};
					} else {
						status = 200;
						response = {message: 'Brand updated. ', brand};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);

				});

			}
		});

	}
};

module.exports.removeBrand = function(req, res) {
	console.log('[removeBrand controller]');

	if(req.params && req.params.id) {

		Brand.findByIdAndRemove({_id: req.params.id}, function(err, brand) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid brand ID. ' + err};
			} else if(!brand) {
				status = 404;
				response = {message: 'Brand not found. '};
			} else {
				status = 204;
				response = {message: 'Brand deleted. '};
			}

			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}
			// Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
			res.status(status);
		});
	
	}
};


module.exports.importFromJson = function(req, res){
	var file = path.join(__dirname, '..', 'files', 'brands.json');
	const lines = require('lines-adapter');

	const stream = fs.createReadStream(file);
	lines(stream, 'utf8')
	.on('data', line => {
		var obj = JSON.parse(line);
		
		Brand.findOne({name: obj.name}).exec(function(err, brand) {
			if (brand) { return; }
			//We.Willen.GEEN.dubbele.MER-KEN!
		})
		var newBrand = {};
		newBrand.name = obj.name;
		if(obj.website != "undefined" || typeof(obj.website) != "undefined" || obj.website != "") {
			newBrand.website = obj.website;
		}
		if(obj.email != "undefined" || typeof(obj.email) != "undefined" || obj.email != "") {
			newBrand.email = obj.email;
		}

		newBrand.foundingYear = parseFloat(obj.foundingYear) || null;

		if(obj.about != "undefined" || typeof(obj.about) != "undefined" || obj.about != "") {
			newBrand.description = obj.about;
		}
		newBrand.isFeatured = obj.featured > 0 || false;
		var categories = [];
		if(obj.isCategoryJewelry) {
			categories.push("jewelry");
		}
		if(obj.isCategoryWatch) {
			categories.push("watches");
		}
		if(categories.length > 0){
			newBrand.categories = categories;
		}
		if(obj.manufacturer != "undefined" || typeof(obj.manufacturer) != "undefined" || obj.manufacturer != "") {
			newBrand.manufacturer = {
				name: obj.manufacturer
			}
		}
		var images = [];
		var image1Type;
		if(obj.image1 && obj.image1Type){
			if(obj.image1Type == "Herensieraad" || obj.image1Type == "Damenssieraad"){
				image1Type = "jewellery";
			}else if(obj.image1Type == "Herenhorloge") {
				image1Type = "gents";
			}else if(obj.image1Type == "Damenshorloge") {
				image1Type = "ladies";
			}else{
				image1Type = null;
			}
		}
		if(obj.image1) {
			var image = {
				src: obj.image1,
				alt: obj.name,
				type: image1Type
			}
			images.push(image);
		}
		if(obj.image2) {
			var image = {
				src: obj.image2,
				alt: obj.name,
			}
			images.push(image);
		}
		if(obj.image3) {
			var image = {
				src: obj.image3,
				alt: obj.name,
			}
			images.push(image);
		}
		if(obj.image4) {
			var image = {
				src: obj.image4,
				alt: obj.name,
			}
			images.push(image);
		}
		if(obj.image5) {
			var image = {
				src: obj.image5,
				alt: obj.name,
			}
			images.push(image);
		}
		if(obj.image6) {
			var image = {
				src: obj.image6,
				alt: obj.name,
			}
			images.push(image);
		}
		if(obj.image7) {
			var image = {
				src: obj.image7,
				alt: obj.name,
			}
			images.push(image);
		}
		if(obj.image8) {
			var image = {
				src: obj.image8,
				alt: obj.name,
			}
			images.push(image);
		}
		if(obj.image9) {
			var image = {
				src: obj.image9,
				alt: obj.name,
			}
			images.push(image);
		}
		if(obj.image10) {
			var image = {
				src: obj.image10,
				alt: obj.name,
			}
			images.push(image);
		}
		newBrand.images = images;
		
		create(newBrand, function(result) {
			if(result) {
				Brand.create(result, function (err, brand) {
					if (err) throw err;
					console.log("Brand added!");
				})
			}
		})
	}).on('end', () => {
		console.log("ready");
		res.send("ok");
	})
}



// function syncBrands(){
// 	Brand.update({},{$set:{isVerified: true}},{multi:true})
// 	.exec((err,docs)=>{
// 		if(err) console.error(err);
// 		else console.log(docs);
// 	});
// }

// syncBrands();