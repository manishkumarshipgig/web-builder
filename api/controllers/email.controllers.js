const path = require('path');
const mongoose = require('mongoose');
const Shop = mongoose.model('Shop');
const crmUser = mongoose.model('crmUser');
const ObjectId = require('mongodb').ObjectId;

const _ = require('lodash');
const async = require("async");
const DateDiff = require('date-diff');

const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));
const compiler = require(path.join(__dirname, '..', 'services', 'pug.service'));

function bucketLocation() {
    return settings.s3Bucket.location + settings.s3Bucket.bucket;
}

module.exports.getPlannedEmails = function (req, res) {
    console.log("getPlannedEmails");
    crmUser.find().exec(function (err, crmUsers) {
        async.eachSeries(crmUsers, function (user, callback) {
            async.eachSeries(user.plannedEmail, function (email, emailDone) {
                
                var diff = new DateDiff(new Date(email.date).setHours(12, 0, 0, 0), new Date().setHours(12, 0, 0, 0));
            
                if (email.enabled == true && diff.days() <= 0) {
                    Shop.findOne({ nameSlug: user.shopSlug }, function (err, shop) {
                        var data = {
                            shop: shop,
                            crmUser: user,
                        }

                        if(!user.fillinLanguage){
                            email.template = email.template + "-nl";
                        }

                        var options = {
                            to: user.email,
                            subject: email.subject,
                            template: email.template,
                            templatehtml: email.emailTemplate,
                            type: "CRM"
                        };

                        mailService.mail(options, data, null, function (err, result) {
                            if (!err) {
                                crmUser.update({ _id: user._id }, { $pull: { 'plannedEmail': { _id: email._id } } }).exec(function (err, res) {

                                })
                                emailDone();
                            }
                        })
                    }) //end shop findone
                } else {
                    emailDone();
                }

            }, function () {
                callback();
            })
        }, function () {
            res.send("ok");
        })
    })
}

module.exports.previewEmail = function (req, res) {
    console.log("previewEmail:" + req.query.index);

    if (req && req.query.emailId && req.query.userId) {
        crmUser.aggregate([
            { $match: { '_id': ObjectId(req.query.userId) } },
            {
                $project: {
                    shopSlug: 1,
                    emailName: 1,
                    email: 1,
                    endDateWarranty: 1,
                    fillindate: 1,
                    gender: 1,
                    watchOrJewel: 1,
                    plannedEmail: {
                        $filter: {
                            input: '$plannedEmail',
                            as: 'email',
                            cond: { $eq: ['$$email._id', ObjectId(req.query.emailId)] }
                        }
                    }
                }
            }
        ]).exec(function (err, crmUser) {
            if (err) {
                status = 500;
                response = { message: 'Internal server error. ' + err };
                return res.status(status).json(response);
            }
            if (!crmUser) {
                status = 404;
                response = { message: 'No CRM customer found' };
                return res.status(status).json(response);
            }

            Shop.findOne({ nameSlug: crmUser[0].shopSlug }).exec(function (err, shop) {
                if (err) {
                    status = 500;
                    response = { message: 'Internal server error. ' + err };
                    return res.status(status).json(response);
                }
                if (!shop) {
                    status = 404;
                    response = { message: 'No shop found' };
                    return res.status(status).json(response);
                }

                if (crmUser[0].plannedEmail[0].emailTemplate != undefined && crmUser[0].plannedEmail[0].emailTemplate != "") {

                    var html = crmUser[0].plannedEmail[0].emailTemplate;
                    res.status(200).json({ message: "Preview generated", html });

                } else {
                    var email = crmUser[0].plannedEmail[0];
                    var templatePath = path.join(__dirname, '..', 'mail-templates', email.template + '.pug');
                    var data = {
                        shop: shop,
                        crmUser: crmUser[0]
                    }

                    compiler(templatePath, data, function (err, html) {
                        if (err) {
                            status = 500;
                            response = { message: 'Internal server error. ' + err };
                            return res.status(status).json(response);
                        }
                        res.status(200).json({ message: "Preview generated", html });
                    })
                }
            })
        })
    } else {
        return res.status(500).json({ message: 'No details provided' });
    }
}

module.exports.previewEmailBrandsNewCampaign = function (req, res) {
    console.log("emailcontroller brandNewCampaign");
    var emailTemp = 'campaigns/shareCampaign-mail-a'
    var templatePath = path.join(__dirname, '..', 'mail-templates', emailTemp + '.pug');

    var data = {
        campaign: req.body.campaign,
        image: bucketLocation() + req.body.imageLinkPreview
    }

    compiler(templatePath, data, function (err, html) {
        if (err) {
            status = 500;
            response = { message: 'Internal server error. ' + err };
            return res.status(status).json(response);
        }
        res.status(200).json({ message: "Preview generated", html });
    }
    )
}