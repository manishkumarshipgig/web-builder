const path = require('path');
const DateDiff = require('date-diff');
const mongoose = require('mongoose');
const fs = require('fs');
const PhoneNumber = require('awesome-phonenumber');
const _ = require('lodash');
const async = require('async');
const uuidv4 = require('uuid/v4');

const User = mongoose.model('User');
const crmUser = mongoose.model('crmUser');
const Shop = mongoose.model('Shop');
const Brand = mongoose.model('Brand');

const countrController = require(path.join(__dirname, '..', 'controllers', 'countr.controllers'));

const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const update = require(path.join(__dirname, '..', 'services', 'update.service'));
const random = require(path.join(__dirname, '..', 'services', 'random.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));
const userService = require(path.join(__dirname, '..', 'services', 'user.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));
const stringService = require(path.join(__dirname, '..', 'services', 'string.service'));
const blacklistService = require(path.join(__dirname, '..', 'services', 'blacklist.service'));
const countrService = require(path.join(__dirname, '..', 'services', 'countr.service'));
const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));

module.exports.getUser = function (req, res) {
    console.log('[getUser controller]');

    if (req && req.params && req.params.userId) {

        User.findOne({ '_id': req.params.userId }).exec(function (err, user) {
            var status, response;

            if (err) {
                status = 400;
                response = { message: 'Invalid user ID. ' + err };
            } else if (!user) {
                status = 404;
                response = { message: 'User not found. ' };
            } else {
                status = 200;
                response = { message: 'OK', user };
            }

            console.log(status + ' ' + response.message);
            res.status(status).json(response);

        });
    }
};

module.exports.getUsers = function (req, res) {
    console.log('[getUsers controller]');
    // default values, because offset and limit are required
    var offset = 0;
    var filterBy, sortBy, limit;

    if (req.query) {

        if (typeof req.query === 'string') {
            req.query = JSON.parse(req.query);
        }

        if (req.query.offset && !isNaN(parseInt(req.query.offset))) {
            offset = parseInt(req.query.offset);
            delete req.query.offset;
        }

        if (req.query.limit && !isNaN(parseInt(req.query.limit))) {
            limit = parseInt(req.query.limit);
            delete req.query.limit;
        }

        // if any filters are provided in the URL, parse them and add to the filtering params object.
        if (req.query.filter) {
            filterBy = filter(req.query.filter);
        }

        // if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
        if (req.query.sort) {
            sortBy = sort(req.query.sort);
        }

        if (req.query.lng && req.query.lat) {
            // TODO: find users close to the given coordinates. 
        }

    }

    User.find(filterBy).sort(sortBy).skip(offset).limit(limit).exec(function (err, users) {
        var status, response;

        if (err) {
            status = 500;
            response = { message: 'Internal server error. ' + err };
        } else if (!users || users.length == 0) {
            status = 404;
            response = { message: 'No users found with the given search criteria. ' };
        } else {
            status = 200;
            response = { message: 'OK', users };
        }

        console.log(status + ' ' + response.message);
        res.status(status).json(response);

    });
};

module.exports.registerUser = function (req, res) {
    var status, response;
    var verifyUser = true;
    var MarketingUser = false;
    if (req.body.verify == false) {
        verifyUser = false;
    }
    if(req.body.user.isMarketingUser && req.body.user.brandId){
        MarketingUser = req.body.user.isMarketingUser;
        var brandID = req.body.user.brandId;
        var ismarketingUserImage = req.body.user.marketingUserImage;
        var ismission = req.body.user.mission;
    }

    if (req.body.userRole) {
        var userRole = req.body.userRole
    }

    if (req.body.user.password !== req.body.user.passwordCompare) {
        console.log("Passwords does not match!");
        response = { message: 'Passwords does not match!' };
    }

    var userData = _.pick(req.body.user, 'firstName', 'lastNamePrefix', 'lastName', 'email', 'password', 'gender');
    if (verifyUser) {
        userData.verificationCode = random(8);
    } else {
        userData.isVerified = true;
    }
    userData.gender = req.body.user.gender == 'Man';
    userData.verificationDate = new Date();
    userData.username = userData.email;
    userData.isMarketingUser = MarketingUser;
    userData.phone = [];
    if (userRole) {
        userData.role = userRole;
    }
    if(brandID && brandID != undefined && userData.isMarketingUser == true){
        userData.brandPortalId = brandID;
        userData.marketingUserImage = ismarketingUserImage;
        userData.marketingUserMission = ismission;
    }
    var phone = new PhoneNumber(req.body.user.phone, "NL");
    if (phone.isValid()) {
        var phoneNumber = {
            countryCode: phone.getRegionCode(),
            mobilePhone: phone.isMobile() ? phone.getNumber() : null,
            landLine: phone.isMobile() ? null : phone.getNumber(),
            fax: null
        }
        userData.phone.push(phoneNumber);
    } else {
        userData.phone = {};
    }

    User.register(userData, function (err, user) {
        if (err && (11000 == err.code || 11001 == err.code)) {
            console.log("E-mailadres already in use");
            return res.status(400).json({ message: 'E-mailadres already in use' });
        }

        if (err) {
            console.log("Something went terribly wrong...", 'https://youtu.be/t3otBjVZzT0 : ' + err);
            status = 500;
            response = { message: "Something went terribly wrong.. https://youtu.be/t3otBjVZzT0 :" + err };
            return res.status(status).json(response);
        }

        if (req.body.request) {
            //handle the request of account (this is different then registering!)
            var options = {
                to: "info@prismanote.com",
                subject: 'Nieuwe account aanvraag!',
                template: 'new-account-requested',
                priority: 'high'
            }
            var data = {
                user: user,
                interest: req.body.extraData
            }
            //Mail sturen met activatielink na aanmaken
            mailService.mail(options, data, null, function (err, mailResult) {
                var options = {
                    to: user.email,
                    subject: 'Uw accountaanvraag op PrismaNote',
                    template: 'new-account-pending'
                }

                var data = {
                    user: user,
                    interest: req.body.extraData
                }
                //Mail sturen met activatielink na aanmaken
                mailService.mail(options, data, null, function (err, data) {
                    if (err) {
                        status = 500;
                        response = err.message;
                        return res.status(status).json(response);
                    } else {
                        status = 200;
                        response = data;
                        return res.status(status).json(response);
                    }
                });
            });
        } else if (!verifyUser) {
            //account hoeft niet verifieerd te worden
            //TODO: mail sturen met info dat account is aangemaakt
            req.logIn(user, function (err) { });
            return res.status(200).json({ message: "User registered and activated", user: user });
        } else {
            var options = {
                to: user.email,
                subject: 'Uw account op PrismaNote',
                template: 'new-account'
            }
            //Mail sturen met activatielink na aanmaken
            mailService.mail(options, user, null, function (err, data) {
                if (err) {
                    status = 500;
                    response = err.message;
                    return res.status(status).json(response);
                } else {
                    status = 200;
                    response = data;
                    return res.status(status).json(response);
                }
            });
        }
    })
}

module.exports.addUser = function (req, res) {
    console.log('[addUser controller]');
    var newUser = {};

    if (req.body) {

        create(req.body, function (newUser) {
            if (newUser) {

                User.create(newUser, function (err, user) {
                    var status, response

                    if (err) {
                        status = 400;
                        response = { message: 'User could not be added. Error message: ' + err };
                    } else if (!user) {
                        status = 404;
                        response = { message: 'User not found. ' };
                    } else {
                        status = 201;
                        response = { message: 'User added. ', user };
                    }

                    console.log(status + ' ' + response.message);
                    if (err) {
                        console.log(err.stack);
                    }
                    res.status(status).json(response);

                });

            }
        });

    }
};

module.exports.updateUser = function (req, res) {
    console.log('[updateUser controller]', req.body);

    if (req.body.user) {
        if (req.user.role == "admin" || req.user._id == req.body.user._id) {

            update(req.body.user, function (updatedUser) {
                if (updatedUser) {
                    if(updatedUser.phonenumber && updatedUser.phonenumber != ""){
                        var country = new PhoneNumber(updatedUser.phonenumber).getRegionCode();
                        if(!country){
                            return res.status(500).json({message: "Invalid phonenumber"})
                        }
                        var phone = new PhoneNumber(updatedUser.phonenumber, country);
                            if (phone.isValid()) {
                                
                                var phoneNumber = {
                                    countryCode: PhoneNumber.getCountryCodeForRegionCode(phone.getRegionCode()),
                                    mobilePhone: phone.isMobile() ? phone.getNumber() : null,
                                    landLine: phone.isMobile() ? null : phone.getNumber('significant'),
                                    fax: null
                                }
                                console.log("VALID", phoneNumber);
                                updatedUser.phone = phoneNumber;
                            }
                    }
                    User.findByIdAndUpdate(
                        { _id: req.body.user._id },
                        { $set: updatedUser },
                        {
                            "fields": {
                                "facebook.token": 1,
                                "facebook.extendedToken": 1,
                                "facebook.pageToken": 1,
                                "facebook.extendedPageToken": 1,
                                "pinterest.accessCode": 1
                            },
                            "new": true
                        }).exec(function (err, user) {
                            var status, response;

                            if (err) {
                                status = 400;
                                response = { message: 'User could not be updated. Error message: ' + err };
                            } else if (!user) {
                                status = 404;
                                response = { message: 'User not found. ' };
                            } else {
                                status = 200;
                                response = { message: 'User updated. ', user };
                            }

                            console.log(status + ' ' + response.message);
                            if (err) {
                                console.log(err.stack);
                            }
                            res.status(status).json(response);

                        });

                }
            });
        } else {
            //Geen rechten of niet eigen account
            res.status(401).json({ message: 'Unauthorized' })
        }

    }
};

module.exports.removeUser = function (req, res) {
    console.log('[removeUser controller]');

    if (req.params && req.params.id) {

        User.findByIdAndRemove({ _id: req.params.id }, function (err, user) {
            var status, response;

            if (err) {
                status = 400;
                response = { message: 'Invalid user ID. ' + err };
            } else if (!user) {
                status = 404;
                response = { message: 'User not found. ' };
            } else {
                status = 204;
                response = { message: 'User deleted. ' };
            }

            console.log(status + ' ' + response.message);
            if (err) {
                console.log(err.stack);
            }
            // Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
            res.status(status);
        });

    }
};

module.exports.importFromJson = function (req, res) {
    var file = path.join(__dirname, '..', 'files', 'users.json');
    const lines = require('lines-adapter');

    const stream = fs.createReadStream(file);

    lines(stream, 'utf8')
        .on('data', line => {
            var obj = JSON.parse(line);

            User.findOne({ email: obj.email }).exec(function (err, user) {
                if (err) throw err;
                if (!user) {
                    var newUser = {};
                    newUser.role = obj.userRole != "undefined" ? obj.userRole : "user" || 'user';
                    newUser.password = random(10);
                    newUser.verificationCode = random(8);
                    newUser.mustResetPassword = true;
                    newUser.email = obj.email.replace(/\s/g, '');
                    newUser.username = obj.email.replace(/\s/g, '');
                    newUser.firstName = obj.firstName || null;
                    newUser.lastName = obj.lastName || null;

                    var addresses = [];
                    if (obj.street && obj.streetNumber && obj.city && obj.zip && obj.land) {
                        var country;
                        if (obj.land == "NL") {
                            country = "Nederland";
                        } else if (obj.land == "BE") {
                            country = "België";
                        } else if (obj.land.length == 0) {
                            country = "Nederland";
                        } else {
                            country = obj.land;
                        }
                        var number = obj.streetNumber.match(/[^\d]+|\d+/g);
                        var suffix;
                        if (number[1]) {
                            suffix = number[1];
                        }
                        if (number[2]) {
                            suffix = suffix + number[2];
                        }
                        var address = {
                            street: obj.street,
                            houseNumber: number[0],
                            houseNumberSuffix: suffix,
                            postalCode: obj.zip.replace(/\s/g, ''),
                            city: obj.city,
                            country: country
                        }
                        addresses.push(address);
                    }
                    newUser.address = addresses;
                    if (obj.phone && obj.land) {
                        var phoneNumbers = [];
                        var countryCode = obj.land == 'België' ? 'BE' : 'NL';
                        var phone = new PhoneNumber(obj.phone, countryCode);
                        if (phone.isValid()) {
                            var phoneNumber = {
                                countryCode: phone.getRegionCode(),
                                mobilePhone: phone.isMobile() ? phone.getNumber() : null,
                                landLine: phone.isMobile() ? null : phone.getNumber(),
                                fax: typeof (obj.fax) != "undefined" && obj.fax.length && obj.fax != "geen" > 1 ? obj.fax : null
                            }
                            phoneNumbers.push(phoneNumber);
                        }
                        newUser.phone = phoneNumbers;
                    }
                    Shop.find({ email: newUser.email }).exec(function (err, shops) {
                        if (err) throw err;
                        if (shops) {
                            var shops = [];
                            shops.forEach(function (shop) {
                                var shop = {
                                    _id: shop._id,
                                    name: shop.name,
                                    nameSlug: shop.nameSlug,
                                    type: shop.isPremium ? 'pro-shop' : 'shop',
                                    address: shop.address
                                }
                                shops.push(shop);
                            })
                            newUser.shops = shops;
                        }
                    })

                    User.register(newUser, function (err, user) {
                        if (err && (11000 == err.code || 11001 == err.code)) {
                            console.log("E-mailadres already in use");
                        }

                        if (err) {
                            console.log("Something went terribly wrong...", 'https://youtu.be/t3otBjVZzT0');
                        } else {
                            console.log("user added");
                        }

                    })
                };
            })
        }).on('end', () => {
            console.log("ready");
            res.send("ok");
        })
}

module.exports.resetPassword = function (req, res) {
    console.log("resetPassword");

    if (req.body.userId && req.body.password) {
        User.resetPassword(req.body.userId, req.body.password, req.body.mustReset, function (err, user) {
            if (err) {
                res.status(500).json({ message: "Something went terribly wrong..." })
            }
            if (user) {
                res.status(200).json(user);
            }
        })
    }
}

module.exports.sendPasswordResetMail = function (req, res) {
    console.log("sendPasswordResetMail");

    if (!req.body.email || req.body.email == "") {
        return res.status(404).json({ message: "No e-mailadres provided" })
    }
    var email = req.body.email;
    var message = "If there is an account with the given e-mail, you should receive an e-mail with further instructions.";
    userService.findUserByEmail(email, function (err, user) {
        //not activated accounts may be asking for a password reset
        if (err && err.code != 409) {
            if (err.code == 404) {
                //not actually found an account, but to prevent searching account via this functions, we didn't let the user know

                return res.status(200).json({ message: message })
            }
            console.log("something went wrong searching user", err);
            return res.status(err.code).json({ message: err.err });
        }

        //create an new verificationCode to generate an password reset link
        user.verificationCode = random(8);
        user.verificationDate = new Date();
        //do nothing with the password here, anybody can send request an new password, even it is not there own account
        user.save(function (err, result) {
            if (err) {
                console.log("error saving user", err);
                return res.status(500).json({ message: err.message });
            }
            //send mail with password reset instructions
            var options = {
                to: result.email,
                subject: 'Your password reset instructions for PrismaNote',
                template: 'reset-password-link'
            }
            mailService.mail(options, result, null, function (err, emailResult) {
                if (err) {
                    return res.status(500).json({ message: err });
                }
                return res.status(200).json({ message: message });
            })
        })
    })
}

module.exports.selfChangePassword = function(req,res){
    console.log("selfChangePassword", req.body);
    if(req.body){
        User.findOne({_id: req.body.userId}).exec(function(err, user){
            if(!user){
                return res.status(404).json({message: "No user found"});
            }
            if(err){
                return res.status(500).json({message: err});
            }

            if(req.body.passwords.newPassword != req.body.passwords.newPassword2){
                console.error("Passwords doesn't match!");
                return res.status(500).json({err: "PASSWORDS_NOT_MATCHING", message: "Passwords not matching!"});
            }

            User.changePassword(req.body.userId, req.body.passwords.currPassword, req.body.passwords.newPassword, function(err, result){
                console.log(err, result);
                if(err){
                    return res.status(500).json(err);
                }
                return res.status(200).json({message: result});
            })
        })
    }else{
        return res.status(500).json({message: "Not all required data is present"});
    }
}

module.exports.changePassword = function (req, res) {
    console.log("changePassword");
    if (req.body.data && req.body.data.id) {
        User.findOne({ _id: req.body.data.id }).select('+verificationCode').exec(function (err, user) {
            if (!user) {
                return res.status(404).json({ message: "No user found" });
            }
            if (err) {
                console.log("error finding user", err);
                return res.status(500).json({ message: err });
            }
            var diff = new DateDiff(new Date(), new Date(user.verificationDate));

            if (diff.days() > 2) {
                return res.status(405).json({ message: "Link is expired" })
            }

            if (user.verificationCode != req.body.data.code) {
                return res.status(403).json({ message: "Security check failed" })
            }
            if (req.body.data.password != req.body.data.confirmPassword) {
                //extra server side password check
                return res.status(401).json({ message: "Passwords do not match!" });
            }

            //finally, we can change the password

            User.resetPassword(user._id, req.body.data.password, false, function (err, changedUser) {
                if (err) {
                    return res.status(500).json({ message: err })
                }

                User.findOne({ _id: changedUser._id }).exec(function (err, user) {
                    if (!err) {
                        user.verificationCode = random(8);
                        user.mustResetPassword = false;
                        if (req.body.data.activate) {
                            user.isVerified = true;
                        }

                        user.save(function (err, result) {
                            if (err) {
                                console.log("error updating user", err);
                                return res.status(500).json({ message: err });
                            }
                            res.status(200).json({ message: "Password is changed!" });
                        })
                    }
                })
            })
        })
    } else {
        return res.status(500).json({ message: "Invalid data provided" });
    }
}

module.exports.sendActivationLink = function (req, res) {
    console.log("sendActivationLink", req.body.email);
    var email = req.body.email;

    User.findOne({ email: email }).exec(function (err, user) {
        if (err) {
            console.log("err", err);
            return res.status(500).json({ message: err });
        }
        if (!user) {
            return res.status(404).json({ mesage: "No user found" })
        }

        user.verificationCode = random(8);
        user.verificationDate = new Date();

        var options = {
            to: user.email,
            subject: 'Uw account op PrismaNote',
            template: 'new-account'
        }
        //Mail sturen met activatielink na aanmaken
        mailService.mail(options, user, null, function (err, data) {
            if (err) {
                console.log("errror", err);
                status = 500;
                response = err.message;
                return res.status(status).json(response);
            } else {
                status = 200;
                response = data;
                return res.status(status).json(response);
            }
        });
    })
}

module.exports.newCrmUser = function (req, res) {
    console.log('[newCrmUser controller]');

    if (req.body.data && req.body.data.customer) {
        var customer = req.body.data.customer;
        var nameSlug = req.body.data.shopSlug;

        var originalFileName = req.body.data.fileName;

        var fillindate = Date.now();
        var itemNumber = Math.floor(1000 + Math.random() * 9000);

        var isWatch = req.body.data.customer.productType;
        var gender = req.body.data.customer.gender == 'male';

        console.log("customer.productTypeId", customer.productTypeId);

        if (customer.productTypeId != 2){
            var predefinedWatchEmails = {
                nl: [{
                    template: "watch-mail-b-nl",
                    subject: "Bent u tevreden met onze service?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "watch-mail-c-nl",
                    subject: "Pitstop voor uw horloge",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "watch-mail-d-nl",
                    subject: "Is het al tijd voor een nieuwe batterij?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "watch-mail-e-nl",
                    subject: "Uw horloge in stijl blijven dragen",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                    //Jolmer, added the following emails for testing reasons
                    // {
                    // 	template: "paymentRequest-mail-a-nl",
                    // 	subject: "Gezamelijke marketing inspanning",
                    // 	date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    // 	enabled: true
                    // },
                    // {
                    // 	template: "paymentRequest-mail-a-en",
                    // 	subject: "Joint marketing effort",
                    // 	date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    // 	enabled: true
                    // }
                ],
                en: [{
                    template: "watch-mail-b-en",
                    subject: "Are you satisfied with our service?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "watch-mail-c-en",
                    subject: "Pitstop for your watch",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "watch-mail-d-en",
                    subject: "Is it time for a new battery?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "watch-mail-e-en",
                    subject: "Continuing to wear your watch in style",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ],
                de: [{
                    template: "watch-mail-b-de",
                    subject: "Sind Sie mit unserem Service zufrieden?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "watch-mail-c-de",
                    subject: "Pitstop für Ihre Uhr",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "watch-mail-d-de",
                    subject: "Ist es Zeit für eine neue Batterie?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "watch-mail-e-de",
                    subject: "Tragen Sie Ihre Uhr weiterhin mit Stil",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ],
                fr: [{
                    template: "watch-mail-b-fr",
                    subject: "Êtes-vous satisfait de notre service?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "watch-mail-c-fr",
                    subject: "Pitstop pour votre montre",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "watch-mail-d-fr",
                    subject: "Est-il temps pour une nouvelle batterie?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "watch-mail-e-fr",
                    subject: "Continuer à porter votre montre avec style",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ],
                es: [{
                    template: "watch-mail-b-es",
                    subject: "¿Está satisfecho con nuestro servicio?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "watch-mail-c-es",
                    subject: "Pitstop para su reloj",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "watch-mail-d-es",
                    subject: "¿Es hora de una nueva batería?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "watch-mail-e-es",
                    subject: "Continúa usando su reloj con estilo",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ]
            }

            function access(o, k) { return o[k] }
            var predefinedWatchEmailsUserLanguage = [[customer.fillinLanguage]].reduce(access, predefinedWatchEmails)

            var plannedEmailsOther = [{
                template: "other-mail-b",
                subject: "Bent u tevreden met onze service?",
                date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden 12096e5
                enabled: true
            }, {
                template: "other-mail-c",
                subject: "Draagtips voor uw sieraad",
                date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                enabled: true
            }, {
                template: "other-mail-d",
                subject: "Hoe staat het met uw sieraad?",
                date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                enabled: true
            }, {
                template: "other-mail-e",
                subject: "Uw sieraad in stijl blijven dragen. Een paar do's en don'ts",
                date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                enabled: true
            }
            ];

            var predefinedOtherEmails = {
                nl: [{
                    template: "other-mail-b-nl",
                    subject: "Bent u tevreden met onze service?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "other-mail-c-nl",
                    subject: "Draagtips voor uw sieraad",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "other-mail-d-nl",
                    subject: "Hoe staat het met uw sieraad?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "other-mail-e-nl",
                    subject: "Uw sieraad in stijl blijven dragen. Een paar do's en don'ts",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ],
                en: [{
                    template: "other-mail-b-en",
                    subject: "Are you satisfied with our service?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "other-mail-c-en",
                    subject: "Pitstop for your watch",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "other-mail-d-en",
                    subject: "Is it time for a new battery?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "other-mail-e-en",
                    subject: "Continuing to wear your watch in style",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ],
                de: [{
                    template: "other-mail-b-de",
                    subject: "Sind Sie mit unserem Service zufrieden?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "other-mail-c-de",
                    subject: "Pitstop für Ihre Uhr",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "other-mail-d-de",
                    subject: "Ist es Zeit für eine neue Batterie?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "other-mail-e-de",
                    subject: "Tragen Sie Ihre Uhr weiterhin mit Stil",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ],
                fr: [{
                    template: "other-mail-b-fr",
                    subject: "Êtes-vous satisfait de notre service?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "other-mail-c-fr",
                    subject: "Pitstop pour votre montre",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "other-mail-d-fr",
                    subject: "Est-il temps pour une nouvelle batterie?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "other-mail-e-fr",
                    subject: "Continuer à porter votre montre avec style",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ],
                es: [{
                    template: "other-mail-b-es",
                    subject: "¿Está satisfecho con nuestro servicio?",
                    date: new Date((+new Date + 12096e5)), // 14 dagen in miliseconden + 12096e5
                    enabled: true
                },
                {
                    template: "other-mail-c-es",
                    subject: "Pitstop para su reloj",
                    date: new Date((+new Date + 15552000000)), // 180 dagen in miliseconden 15552000000
                    enabled: true
                },
                {
                    template: "other-mail-d-es",
                    subject: "¿Es hora de una nueva batería?",
                    date: new Date((+new Date + 31536000000)), // 365 dagen in miliseconden 31536000000
                    enabled: true
                },
                {
                    template: "other-mail-e-es",
                    subject: "Continúa usando su reloj con estilo",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
                ]
            }

            function access(o, k) { return o[k] }
            var predefinedOtherEmailsUserLanguage = [[customer.fillinLanguage]].reduce(access, predefinedOtherEmails)

            var newUser = {
                emailName: customer.emailName,
                gender: gender,
                dateOfBirth: customer.dateOfBirth,
                email: customer.email,
                emailOfficial: customer.emailOfficial,
                shopSlug: nameSlug,
                plannedEmail: isWatch == 1 ? predefinedWatchEmailsUserLanguage : predefinedOtherEmailsUserLanguage,
                items: [{
                    priceRange: customer.priceRange,
                    itemComment:{
                        comment: customer.comment,
                        date: new Date()
                    },
                    endDateWarranty: customer.endDateWarranty,
                    watchOrJewel: true,
                    address: customer.address,
                    phone: customer.phone,
                    firstName: customer.firstName,
                    insertion: customer.insertion,
                    lastName: customer.lastName,
                    email: customer.email,
                    helpedBy: customer.helpedBy,
                    productStatus: customer.productStatus,
                    productKind: customer.productKind,
                    productMaterial: customer.productMaterial,
                    customTags: customer.customTags,
                    brandName:customer.brandName,
                    fillinLanguage: customer.fillinLanguage,
                    fillindate: fillindate,
                    itemNumber: itemNumber,
                    productTypeId: customer.productTypeId,
                }],
                fillinLanguage: customer.fillinLanguage,

                // below fields will become obsolete
                priceRange: customer.priceRange,
                endDateWarranty: customer.endDateWarranty,
                watchOrJewel: true,
                fillindate: fillindate,
                productTypeId: customer.productTypeId
            }

            var normalEmail = {
                nl: [
                    {
                        template: "mail-a-nl",
                        subject: "Uw aankoopbewijs",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ],
                en: [
                    {
                        template: "mail-a-en",
                        subject: "Your proof of purchase",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ],
                de: [
                    {
                        template: "mail-a-de",
                        subject: "Ihr Kaufbeleg",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ],
                fr: [
                    {
                        template: "mail-a-fr",
                        subject: "Votre preuve d'achat",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ],
                es: [
                    {
                        template: "mail-a-es",
                        subject: "Su comprobante de compra",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ]
            }
        } else {
            //REPAIRS

            var itemComment = {
                comment: customer.comment,
                date: new Date()
            }
            console.log("itemComment", itemComment);

            var newUser = {
                emailName: customer.emailName,
                gender: gender,
                dateOfBirth: customer.dateOfBirth,
                email: customer.email,
                shopSlug: nameSlug,
                plannedEmail:isWatch == 1 ? predefinedWatchEmailsUserLanguage : predefinedOtherEmailsUserLanguage,
             
                items: [{
                    priceRange: customer.priceRange,
                    endDateWarranty: customer.endDateWarranty,
                    repairDate: customer.repairDate,
                    status: "taken",
                    itemComment: itemComment,
                    watchOrJewel:true,
                    address: customer.address,
                    phone: customer.phone,
                    firstName: customer.firstName,
                    insertion: customer.insertion,
                    lastName: customer.lastName,
                    email: customer.email,
                    emailOfficial: customer.emailOfficial,
                    helpedBy: customer.helpedBy,
                    productStatus: customer.productStatus,
                    productKind: customer.productKind,
                    productMaterial: customer.productMaterial,
                    customTags: customer.customTags,
                    brandName:customer.brandName,
                    fillinLanguage: customer.fillinLanguage,
                    fillindate: fillindate,
                    itemNumber: itemNumber,
                    productTypeId: customer.productTypeId,                    
                }],
                
                // below fields will become obsolete
                priceRange: customer.priceRange,
                endDateWarranty: customer.endDateWarranty,
                repairDate: customer.repairDate,
                watchOrJewel: true,
                fillinLanguage: customer.fillinLanguage,
                fillindate: fillindate,
                productTypeId: customer.productTypeId
            }

            //getting the first email in user language (value '0' means first object form array)
            var normalEmail = {
                nl: [
                    {
                        template: "repair-mail-a-nl",
                        subject: "Bewijs van aflevering reparatie",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ],
                en: [
                    {
                        template: "repair-mail-a-en",
                        subject: "Your proof of delivery repair",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ],
                de: [
                    {
                        template: "repair-mail-a-de",
                        subject: "Nachweis der Lieferreparatur",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ],
                fr: [
                    {
                        template: "repair-mail-a-fr",
                        subject: "Preuve de la livraison de réparation",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ],
                es: [
                    {
                        template: "repair-mail-a-es",
                        subject: "Pruebas de la entrega de reparación",
                        date: new Date((+new Date)),
                        enabled: true
                    }
                ]
            }
        }

        function access(o, k) { return o[k] }
        var templateSlugFirstMailUserLanguage = [[newUser.fillinLanguage], 0, 'template'].reduce(access, normalEmail)
        var subjectFirstMailUserLanguage = [[newUser.fillinLanguage], 0, 'subject'].reduce(access, normalEmail)

        // console.log("originalFileName", originalFileName.photo[0].src);
        //  var photo = path.basename(originalFileName, path.extname(originalFileName));
        fileName = stringService.slugify(new Date().toISOString() + path.extname(originalFileName));
        console.log("fileName", fileName);

        //Check if the shop has an Countr integration to look if we need to send this as an transaction to Countr

        function getCountrOnShop(callback){
            Shop.findOne({nameSlug: newUser.shopSlug}).exec(function(err, shop){
                if(shop && shop.countr && shop.countr.username && shop.countr.accessToken){
                    if(!shop.countr.deviceId){
                        countrService.registerDeviceAndSaveMerchant(shop._id, function(err, result){
                            return getCountrOnShop(callback);
                        })
                    }else{
                        return callback(null, shop.countr);
                    }
                }else{
                    return callback("No countr integration");
                }
            })
        }


        function sendTransactionToCountr(callback){
            getCountrOnShop(function(err, countr){
                //No countr integration
                if(err){ return callback(err)};

                //Now we can create an transaction

                var date = new Date();
                var reicept_number = date.getYear() + "-" + date.getMonth() + "-" + date.getDay() + "-" + date.getSeconds();

                var transaction = {
                    date: date,
                    merchant: countr.merchantId,
                    store: countr.countrId,
                    device: countr.deviceId,
                    receipt_id: uuidv4(),
                    receipt_number: receipt_number,
                    payments: '', //TODO: Get a list with payments to send
                    sub_total: customer.priceRange,
                    total: customer.priceRange,
                    total_refunded: 0,
                    paid: 0,   
                    items: '', //TODO: create a item for the repair
                    currency: '' //TODO: check how to send an right currency object
                }
            })
        }

        sendTransactionToCountr(function(){

        })

        


        // loop through all the crmUsers and look if the shopSlug exists.
        crmUser.find({email: newUser.email}).exec(function(err, users){
            if(!err && users.length > 0){
                // loop through all shops to get the right shop to do Countr crmUser sync and get shop for sent email info
                Shop.findOne({ nameSlug: newUser.shopSlug }).exec(function (err, shop) {
                    if(!err && shop){
                        console.log("shop is found", shop);

                        console.log("users", users);
                        console.log("newUser.shopSlug",newUser.shopSlug);

                        function getUser(){
                            var result = false, counter =0;
                       
                            for(var i=0; i < users.length; i++){
                                if(users[i].shopSlug == newUser.shopSlug){
                                    console.log("users[i]",users[i]);
                                    result = users[i];
                                }

                                counter++;
                                console.log("counter",counter);
                                if(counter == users.length){
                                    console.log('result',result);
                                    return result;
                                }
                            }
                        }

                        if (!err && users.length > 0 && getUser()) {
            
                            var existingCrmUser = getUser();
        
                            update(existingCrmUser, function (existingCrmUserItem) {
                                if (existingCrmUser) {
                                    var item = {
                                        priceRange: customer.priceRange,
                                        endDateWarranty: customer.endDateWarranty,
                                        repairDate: customer.repairDate,
                                        watchOrJewel: true,
                                        address: customer.address,
                                        phone: customer.phone,
                                        firstName: customer.firstName,
                                        insertion: customer.insertion,
                                        lastName: customer.lastName,
                                        email: customer.email,
                                        emailOfficial: customer.emailOfficial,
                                        helpedBy: customer.helpedBy,
                                        productStatus: customer.productStatus,
                                        productKind: customer.productKind,
                                        productMaterial: customer.productMaterial,
                                        customTags: customer.customTags,
                                        brandName:customer.brandName,
                                        fillinLanguage: customer.fillinLanguage,
                                        fillindate: fillindate,
                                        itemNumber: itemNumber,
                                        productTypeId: customer.productTypeId,
                                        status: "taken",
                                        
                                    };
                
                                    if(!existingCrmUser.items){
                                        existingCrmUser.items = [];
                                    }
        
                                    existingCrmUser.items.push(item);
        
                                    crmUser.findByIdAndUpdate({ _id: existingCrmUser._id }, { $set: existingCrmUserItem }).exec(function (err, user) {
                                            var status, response;
                                            if (err) {
                                                status = 400;
                                                response = { message: 'crmUser could not be updated. Error message: ' + err };
                                            } else if (!user) {
                                                status = 404;
                                                response = { message: 'crmUser not found. ' };
                                            } else {
                                                status = 200;
                                                response = { message: 'crmUser updated. ', user, fileName};
                                            }
                
                                            console.log(status + ' ' + response.message);
                                            if (err) {
                                                console.log(err.stack);
                                            }
                                            sendMail(newUser, shop, subjectFirstMailUserLanguage,templateSlugFirstMailUserLanguage, fileName);
                                            return res.status(status).json(response);
                                            // hier stopt deze functie omdat er een user items is bijgevoegd en de response gelogd wordt!
                                    })
                                }
                            })
                        }
                    }
                })
            
            } else {
                create(newUser, function (newUserItem) {
                    if (newUserItem) {
                        crmUser.create(newUserItem, function (err, user) {
                            var status, response;

                            Shop.findOne({ nameSlug: newUser.shopSlug }).exec(function (err, shop) {
                                if(!err && shop){

                                    sendMail(user, shop, subjectFirstMailUserLanguage,templateSlugFirstMailUserLanguage, fileName);
                                    shopService.checkModule('countr', shop._id, function(active){
                                        if(active){
                                            countrController.getTokens(shop._id, null, null, function(err, tokens){
                                                if(err){                                    
                                                    status = 500;
                                                    response = err;
                                                }else{
                                                    countrService.getCurrentMerchant(tokens.accessToken, function(err, merchant){
                                                        let countrCustomer = {
                                                            merchant: merchant._id,
                                                            first_name: newUserItem.emailName,
                                                            email: newUserItem.email
                                                        }
                                                        countrController.newCustomer(countrCustomer, tokens.accessToken, function(err, customer){
            
                                                        })
                                                    }) 
                                                }
                                            })
                                        }
                                    })
                                    return res.status(200).json({ message: 'crmUser created. ', user, fileName});
                                    
                                }
                            })
                        })

                    } else {
                        status = 500;
                        response = err;
                        return res.status(status).json(response);
                    }
                })
            }
        })
                
                // } else {
                //     status = 400;
                //     response = { message: 'Shop could not be found, crmUser item could not be added. Error message: ' + err };

                //     console.log("Shop could not be found! crmUser not created!" + err);
                //     return res.status(status).json(response);
                // }
                
        function sendMail(user, shop, subject, template, fileName){

            console.log("fileName used for sending", fileName);
            console.log("originalFileName used for sending", originalFileName);
 
            var options = {
                to: user.email,
                subject: subject,
                template: template,
                type: 'CRM',
                //from: '"' + shop.name + '" <'+ shop.email +'>'
                //hier nog wat voor verzinnen, dit kan nu nog niet omdat je emailadressen moet valideren die je als 'from' wilt gebruiken
                //de emailservice ondersteunt het al wel
            }
            
            var data = {
                shop: shop,
                crmUser: user,
                fileName: fileName + path.extname(originalFileName),
                today: user.repairDate,
                Price: user.priceRange,
                comment :user.items[user.items.length-1].itemComment.comment
               

            }
            console.log("Last fileName used for sending", fileName);
            console.log("data", data);

            mailService.mail(options, data, null, function (err, data) {
                // if (err) {
                //     status = 500;
                //     response = err.message;
                // } else {
                //     status = 201;
                //     response = { user: user, fileName: fileName };

                // }
                //return res.status(status).json(response);
            });
        }
    }
}
    

module.exports.uploadCrmCardPhoto = function (req, res) {
    console.log("uploadCrmCardPhoto");
    if (req.body.crmUserId || req.body.userId) {

        if (req.body.friendlyFileName) {
            // this function is used for 'Repair ready' emails
            var filename = stringService.slugify(new Date().toISOString() + path.extname(req.files.file.originalFilename));
            var originalFileName = filename;
            var filename = originalFileName;
            var comment= req.body.comment;
        } else {
            // this function is used for ALL other emails with photo's
            var originalFileName = req.body.fileName;
            var filename = originalFileName;
            var comment= req.body.comment;
            console.log("filename (not friendly)", filename);
        }

        upload.uploadFile(req.files.file, 'loyality-photo', { fileName: filename }, function (err, result) {
            if (err) {
                return res.status(500).json({ message: 'error uploading file: ' + err })
            }
            crmUser.findOne({ _id: req.body.crmUserId }).exec(function (err, crmUser) {
                if (crmUser) {
                    var photo = {
                        src: result
                    }

                    crmUser.items[crmUser.items.length - 1].photo.src = "";
                    crmUser.items[crmUser.items.length - 1].photo.push(photo);

                    // will become obsolete
                    crmUser.photo.push(photo);


                    crmUser.comment = comment;

                    console.log("crmUser", crmUser);

                    crmUser.save(function (err, saveResult) {
                        var countr = false;
                        if (!err) {
                            Shop.findOne({nameSlug: saveResult.nameSlug}).exec(function(err, shop){
                                if(shop){
                                    countr = shop.countr && shop.countr.username && shop.countr.accessToken
                                }
                                return res.status(200).json({ file: saveResult, countr: countr});
                            })
                            
                        } else {
                            console.log("Error during updating crmUser: " + err);
                            return res.status(200).json({ message: "Error during updating crmUser: " + err });
                        }
                    })
                } else {
                    console.log("crmUser not found");
                    return res.status(404).json({ message: "crmUser not found" });
                }
            })
        })
    } else {
        console.log("no user id found");
        return res.status(404).json({ message: "no user id found" });
    }
}

module.exports.setPlannedEmail = function (req, res) {
    console.log("setPlannedEmail");
    crmUser.findOne({ _id: req.body.userId }).exec(function (err, crmUser) {
        if (crmUser) {
            crmUser.plannedEmail[req.body.emailIndex].enabled = req.body.enabled;

            crmUser.save(function (err, result) {
                if (err) {
                    console.log("could not save new plannedEmail status");
                    return res.status(400).json({ message: "could not save new plannedEmail status: " + err });
                } else {
                    return res.status(200).json({ crmUser: result });
                }
            })
        } else {
            console.log("crmUser not found");
            return res.status(204).json({ message: "crmUser not found" });
        }
    })
}

module.exports.getCrmUsers = function (req, res) {
    console.log('[getCrmUsers controller]');
    // default values, because offset and limit are required
    var offset = 0;
    var filterBy, sortBy, limit;

    if (req.query) {

        if (typeof req.query === 'string') {
            req.query = JSON.parse(req.query);
        }

        if (req.query.offset && !isNaN(parseInt(req.query.offset))) {
            offset = parseInt(req.query.offset);
            delete req.query.offset;
        }

        if (req.query.limit && !isNaN(parseInt(req.query.limit))) {
            limit = parseInt(req.query.limit);
            delete req.query.limit;
        }

        // if any filters are provided in the URL, parse them and add to the filtering params object.
        if (req.query.filter) {
            filterBy = filter(req.query.filter);
            //filterBy = req.query.filter;
        }

        // if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
        if (req.query.sort) {
            sortBy = sort(req.query.sort);
        }
    }
    crmUser.find(filterBy).sort(sortBy).skip(offset).limit(limit).exec(function (err, crmUsers) {
        let status, response, updatedCrmUsers = [];

        if (err) {
            status = 500;
            response = { message: 'Internal server error. ' + err };
            return res.status(status).json(response);
        } else if (!crmUsers || crmUsers.length == 0) {
            status = 404;
            response = { message: 'No orders found with the given search criteria. ' };
            return res.status(status).json(response);
        } else {
            async.eachOf(crmUsers, function (user, key, userDone) {
                mailService.getLogs({ to: user.email, typeMail: "CRM" }, function (err, result) {
                    if (err) {
                        return userDone(err);
                    }
                    crmUsers[key].sentMail = result;
                    userDone();
                })
            }, function (err) {
                status = 200;
                response = { message: "OK", crmUsers: crmUsers };
                return res.status(status).json(response);
            })
        }
    })
}

module.exports.getCrmUsersCount = function (req, res) {
    var filterBy;

    if (req.query.filter) {
        filterBy = filter(req.query.filter);
    }

    crmUser.count(filterBy).exec(function (err, crmUsers) {
        let status, response, updatedCrmUsers = [];

        if (err) {
            status = 500;
            response = { message: 'Internal server error. ' + err };
            return res.status(status).json(response);
        } else if (!crmUsers || crmUsers.length == 0) {
            //this can happen when a user register as first on this shop. That's OK, so we need to return 'zero'
            return res.status(200).json({ message: 'OK', crmUsers: 0 });
        } else {
            async.eachOf(crmUsers, function (user, key, userDone) {
                mailService.getLogs({ to: user.email, typeMail: "CRM" }, function (err, result) {
                    if (err) {
                        return userDone(err);
                    }
                    crmUsers[key].sentMail = result;
                    userDone();
                })
            }, function (err) {
                status = 200;
                response = { message: "OK", crmUsers: crmUsers };
                return res.status(status).json(response);
            })
        }
    })
}

module.exports.setEmailBody = function (req, res) {
    console.log("setEmailBody : " + req.body.emailIndex);
    crmUser.findOne({ _id: req.body.userId }).exec(function (err, crmUser) {
        if (crmUser) {
            console.log(crmUser.plannedEmail[req.body.emailIndex]);
            crmUser.plannedEmail[req.body.emailIndex].emailTemplate = req.body.template;

            crmUser.save(function (err, result) {
                if (err) {
                    console.log("could not save new plannedEmail template");
                    return res.status(400).json({ message: "could not save new plannedEmail template: " + err });
                } else {
                    return res.status(200).json({ crmUser: result });
                }
            })
        } else {
            console.log("crmUser not found");
            return res.status(204).json({ message: "crmUser not found" });
        }
    })
}

module.exports.createCrmUsersplannedEmail = function (req, res) {
    console.log("createCrmUsersplannedEmail : ", req.body);

    crmUser.findOne({ _id: req.body.userId }).exec(function (err, crmUser) {
        if (crmUser) {
            Shop.findOne({ _id: req.body.shopId }).exec(function (err, shop) {
                if (req.body.date == null || !req.body.date) {
                    //Send this mail immediately
                    var data = {
                        shop: shop,
                        crmUser: crmUser,
                    }

                    var options = {
                        to: crmUser.email,
                        subject: req.body.subject,
                        template: "",
                        emailTemplate: req.body.template,
                        type: "CRM"
                    };

                    mailService.mail(options, data, null, function (err, result) {
                        if (!err) {
                            res.status(200).json({ message: "Mail sent!" })
                        }
                    })

                } else {
                    crmUser.plannedEmail.push({
                        template: "",
                        emailTemplate: req.body.template,
                        subject: req.body.subject,
                        date: req.body.date, // 14 dagen in miliseconden 12096e5
                        enabled: true
                    });

                    crmUser.save(function (err, result) {
                        if (err) {
                            console.log("could not save new plannedEmail status");
                            return res.status(400).json({ message: "could not save new plannedEmail status: " + err });
                        } else {
                            return res.status(200).json({ crmUser: result });
                        }
                    })
                }
            })
        } else {
            console.log("crmUser not found");
            return res.status(204).json({ message: "crmUser not found" });
        }
    });
}

module.exports.addBrandtoBlacklist = function (req, res) {
    console.log("addBrandtoBlacklist");
    var data = JSON.parse(Buffer.from(req.body.code, 'base64').toString('ascii'));
    Brand.findOne({ name: data.brand }).exec(function (err, brand) {

        console.log(err, brand);
        if (err || !brand) {
            return res.status(500).json({ message: !brand ? "No brand found" : "Error while getting brand: " + err })
        }

        blacklistService.checkBrandBlacklist(data.email, null, brand.nameSlug, function (result) {
            if (result) {
                return res.status(500).json({ message: "Already unsubscribed from this brand" })
            } else {
                blacklistService.addBlacklistRule(data.email, { brand: { name: brand.name, nameSlug: brand.nameSlug } }, function (err, result) {
                    console.log("addBlacklistRule", err, result);
                    if (err) {
                        return res.status(500).json({ message: err });
                    }
                    return res.status(200).json({ message: "Successfully unsubscribed from brand" })
                })
            }
        })
    })
}

module.exports.repairReadyEmail = function (req, res) {
   
    console.log("Send email for repair :" + req.body.userId);

    crmUser.findOne({ _id: req.body.userId }).exec(function (err, crmUser) {
        // Todo: Change language templates.
        var normalEmail2 = {
            nl: [
                {
                    template: "repair-mail-b-nl",
                    subject: "Uw reparatie is klaar",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            en: [
                {
                    template: "repair-mail-b-en",
                    subject: "Your product is repaired",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            de: [
                {
                    template: "repair-mail-b-de",
                    subject: "Ihre Reparatur ist fertig",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            fr: [
                {
                    template: "repair-mail-b-fr",
                    subject: "Votre réparation est prête",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            es: [
                {
                    template: "repair-mail-b-es",
                    subject: "Su reparación está lista",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ]
        }
        function access(o, k) { return o[k] }
        var templateSlugFirstMailUserLanguage = [[crmUser.fillinLanguage], 0, 'template'].reduce(access, normalEmail2)
        var subjectFirstMailUserLanguage = [[crmUser.fillinLanguage], 0, 'subject'].reduce(access, normalEmail2)
        var originalFileName = "";

        Shop.findOne({ nameSlug: crmUser.shopSlug }).exec(function (err, shop) {
            if (!err && shop) {
                var options = {
                    to: crmUser.email,
                    subject: subjectFirstMailUserLanguage,
                    template: templateSlugFirstMailUserLanguage,
                    type: 'CRM',
                    //from: '"' + shop.name + '" <'+ shop.email +'>'
                    //hier nog wat voor verzinnen, dit kan nu nog niet omdat je emailadressen moet valideren die je als 'from' wilt gebruiken
                    //de emailservice ondersteunt het al wel
                }

                console.log("subjectFirstMailUserLanguage", subjectFirstMailUserLanguage);
                console.log("originalFileName", originalFileName);

                fileName = originalFileName;                
                //var fileNameLength =originalFileName.photo.length -1 ;
                var data = {
                    shop: shop,
                    crmUser: crmUser,
                    fileName: originalFileName,                    
                    today: new Date(),
                    Price: req.body.price,
                    Comment:req.body.comment
                }

                mailService.mail(options, data, null, function (err, data) {
                    if (err) {
                        status = 500;
                        response = err.message;
                    } else {
                        status = 201;
                        response = { user: crmUser, fileName: fileName };                        

                    }
                    return res.status(status).json(response);
                });

            } else {
                status = 500;
                response = err;
                return res.status(status).json(response);
            }
        });

    });

}

module.exports.repairReadyEmailCompany = function (req, res) {

    console.log("Send email for repair :" + req.body.userId);

    crmUser.findOne({ _id: req.body.userId }).exec(function (err, crmUser) {
        // Todo: Change language templates.
        var normalEmail2 = {
            nl: [
                {
                    template: "repair-mail-c-nl",
                    subject: req.body.repairStatus,
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            en: [
                {
                    template: "repair-mail-c-en",
                    subject: req.body.repairStatus,
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            de: [
                {
                    template: "repair-mail-c-de",
                    subject: req.body.repairStatus,
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            fr: [
                {
                    template: "repair-mail-c-fr",
                    subject: req.body.repairStatus,
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            es: [
                {
                    template: "repair-mail-c-es",
                    subject: req.body.repairStatus,
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ]
        }
        function access(o, k) { return o[k] }
        var templateSlugFirstMailUserLanguage = [[crmUser.fillinLanguage], 0, 'template'].reduce(access, normalEmail2)
        var subjectFirstMailUserLanguage = [[crmUser.fillinLanguage], 0, 'subject'].reduce(access, normalEmail2)
        var originalFileName = req.body.fileName;
        var repairCompPic= originalFileName.split('/');

        Shop.findOne({ nameSlug: crmUser.shopSlug }).exec(function (err, shop) {
            if (!err && shop) {
                var options = {
                    to: req.body.email,
                    subject: subjectFirstMailUserLanguage,
                    template: templateSlugFirstMailUserLanguage,
                    type: 'CRM',
                    //from: '"' + shop.name + '" <'+ shop.email +'>'
                    //hier nog wat voor verzinnen, dit kan nu nog niet omdat je emailadressen moet valideren die je als 'from' wilt gebruiken
                    //de emailservice ondersteunt het al wel
                }

                console.log("subjectFirstMailUserLanguage", subjectFirstMailUserLanguage);
                console.log("originalFileName", originalFileName);

                fileName = originalFileName;
                // var fileNameLength = originalFileName.photo.length-1;

                var data = {
                    shop: shop,
                    crmUser: crmUser,
                    fileName: repairCompPic[1],
                    today: new Date(),
                    Price: req.body.price,
                    Comment:req.body.comment
                }

                mailService.mail(options, data, null, function (err, data) {
                    if (err) {
                        status = 500;
                        response = err.message;
                    } else {
                        status = 201;
                        response = { user: crmUser, fileName: fileName };

                    }
                    return res.status(status).json(response);
                });

            } else {
                status = 500;
                response = err;
                return res.status(status).json(response);
            }
        });

    });

}

module.exports.repairReadyEmailWithPic = function (req, res) {

    //console.log("Send email for repairReadyEmailWithPic :"+ req.body.userId);

    crmUser.findOne({ _id: req.body.userId }).exec(function (err, crmUser) {

        // Todo: Change language templates.
        var normalEmail2 = {
            nl: [
                {
                    template: "repair-mail-b-nl",
                    subject: "Uw reparatie is klaar",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            en: [
                {
                    template: "repair-mail-b-en",
                    subject: "Your product is repaired",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            de: [
                {
                    template: "repair-mail-b-de",
                    subject: "Ihre Reparatur ist fertig",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            fr: [
                {
                    template: "repair-mail-b-fr",
                    subject: "Votre réparation est prête",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ],
            es: [
                {
                    template: "repair-mail-b-es",
                    subject: "Su reparación está lista",
                    date: new Date((+new Date + 47088000000)), // 545 dagen in miliseconden 47088000000
                    enabled: true
                }
            ]
        }
        function access(o, k) { return o[k] }
        var templateSlugFirstMailUserLanguage = [[crmUser.fillinLanguage], 0, 'template'].reduce(access, normalEmail2)
        var subjectFirstMailUserLanguage = [[crmUser.fillinLanguage], 0, 'subject'].reduce(access, normalEmail2)
        var originalFileName = req.body.fileName;

        // console.log("req.body.fileName", req.body.fileName);
        // fileName = stringService.slugify(new Date().toISOString() + path.extname(req.body.fileName));

        Shop.findOne({ nameSlug: crmUser.shopSlug }).exec(function (err, shop) {
            if (!err && shop) {
                var options = {
                    to: crmUser.email,
                    subject: subjectFirstMailUserLanguage,
                    template: templateSlugFirstMailUserLanguage,
                    type: 'CRM',
                    //from: '"' + shop.name + '" <'+ shop.email +'>'
                    //hier nog wat voor verzinnen, dit kan nu nog niet omdat je emailadressen moet valideren die je als 'from' wilt gebruiken
                    //de emailservice ondersteunt het al wel
                }

                //console.log("fileName", fileName);
                //-var photo = path.basename(fileName, path.extname(fileName));
                //-console.log("photo", photo);
                //-var fileName = stringService.slugify(photo) + path.extname(fileName);

                console.log("subjectFirstMailUserLanguage", subjectFirstMailUserLanguage);
                console.log("originalFileName", originalFileName);
                var fileNameLength = originalFileName.photo.length-1;

                //fileName = stringService.slugify(new Date().toISOString() + path.extname(originalFileName)); 

                //console.log("fileName", fileName);

                var data = {
                    shop: shop,
                    crmUser: crmUser,
                    fileName: originalFileName.photo[fileNameLength].src,                   
                    today: new Date(),
                    Price: req.body.price,
                    Comment:req.body.comment
                }

                mailService.mail(options, data, null, function (err, mailResult) {
                    if (err) {
                        status = 500;
                        response = err.message;
                    } else {
                        status = 201;
                        response = { user: crmUser };

                    }
                    return res.status(status).json(response);
                });

            } else {
                status = 500;
                response = err;
                return res.status(status).json(response);
            }
        });

    });
}

module.exports.updateCrmUser = function (req, res) {
    console.log('[updateCrmUser controller]');
    console.log('req.body.crmUser', req.body);
    if (req.body.crmUser) {
        update(req.body.crmUser, function (updatedCrmUser) {
            if (updatedCrmUser) {
                crmUser.findByIdAndUpdate(
                    { _id: req.body.crmUser._id },
                    { $set: updatedCrmUser }).exec(function (err, crmUser) {
                        var status, response;

                        if (err) {
                            status = 400;
                            response = { message: 'crmUser could not be updated. Error message: ' + err };
                        } else if (!crmUser) {
                            status = 404;
                            response = { message: 'crmUser not found. ' };
                        } else {
                            status = 200;
                            response = { message: 'crmUser updated. ', crmUser };
                        }

                        console.log(status + ' ' + response.message);
                        if (err) {
                            console.log(err.stack);
                        }
                        res.status(status).json(response);

                    });

            }
        });

    }
};

module.exports.getCrmUserInfoForModal = function (req, res) {
    console.log("getCrmUserInfoForModal controller");
    var userEmail = req.body.crmUser.email;
    User.findOne({email: userEmail}).exec(function(err, user){

        // check if this user exists with the specific Shop slug!

        if (user) {
            if (err) {
                console.log("error occured");
                return res.status(400).json({ message: "error occured: " + err });
            } else {
                status = 200;
                response = { message: 'crmUser updated. ', user };
            }
            return res.status(status).json(response);
        } else {
            console.log("user not found");
            return res.status(204).json({ message: "user not found" });
        }
    })
}

module.exports.getCrmUser = function (req, res) {
    console.log('[getCrmUser controller]');

    if (req && req.params && req.params.crmUserId) {

        crmUser.findOne({ '_id': req.params.crmUserId }).exec(function (err, crmUser) {
            var status, response;

            if (err) {
                status = 400;
                response = { message: 'Invalid crmUser ID. ' + err };
            } else if (!crmUser) {
                status = 404;
                response = { message: 'crmUser not found. ' };
            } else {
                status = 200;
                response = { message: 'OK', crmUser };
            }

            console.log(status + ' ' + response.message);
            res.status(status).json(response);

        });
    }
};

module.exports.getCrmUserEmail = function (req, res) {
    console.log('[getCrmUser email controller]',req.params.email);

    if (req && req.params && req.params.email) {

        crmUser.findOne({ 'email': req.params.email}).exec(function (err, crmUser) {
            var status, response;

            if (err) {
                status = 400;
                response = { message: 'Invalid crmUser ID. ' + err };
            } else if (!crmUser) {
                status = 404;
                response = { message: 'crmUser not found. ' };
            } else {
                status = 200;
                response = { message: 'OK', crmUser };
            }

            console.log(status + ' ' + response.message);
            res.status(status).json(response);

        });
    }
};

module.exports.registerMarketingUser = function (req, res) {
    var status, response;
    var verifyUser = true;
    var MarketingUser = false;
    if (req.body.verify == false) {
        verifyUser = false;
    }
    if(req.body.isMarketingUser){
        MarketingUser = req.body.isMarketingUser;
    }
    console.log('req.body',req.body);
    if (req.body.userRole) {
        var userRole = req.body.userRole
    }

    if (req.body.user.password !== req.body.user.passwordCompare) {
        console.log("Passwords does not match!");
        response = { message: 'Passwords does not match!' };
    }
    var userData = _.pick(req.body.user, 'firstName', 'lastNamePrefix','mission','altText', 'lastName', 'email', 'password', 'gender');
    if (verifyUser) {
        userData.verificationCode = random(8);
    } else {
        userData.isVerified = true;
    }
    console.log('MarketingUser',MarketingUser);
    userData.verificationDate = new Date();
    userData.username = userData.email;
    userData.isMarketingUser = MarketingUser;
    if (userRole) {
        userData.role = userRole;
    }

    marketingUser.registermarketing(userData, function (err, user) {
        if (err && (11000 == err.code || 11001 == err.code)) {
            console.log("E-mailadres already in use");
            return res.status(400).json({ status : 400,message: 'E-mailadres already in use' });
        }

        if (err) {
            console.log("Something went terribly wrong...", 'https://youtu.be/t3otBjVZzT0 : ' + err);
            status = 500;
            response = { message: "Something went terribly wrong.. https://youtu.be/t3otBjVZzT0 :" + err };
            return res.status(status).json(response);
        }

        if (!verifyUser) {
            //account hoeft niet verifieerd te worden
            //TODO: mail sturen met info dat account is aangemaakt
            req.logIn(user, function (err) { });
            return res.status(200).json({ message: "User registered and activated", user: user });
        } else {
            var options = {
                to: user.email,
                subject: 'Uw account op PrismaNote',
                template: 'new-account'
            }
            //Mail sturen met activatielink na aanmaken
            mailService.mail(options, user, null, function (err, data) {
                if (err) {
                    status = 500;
                    response = err.message;
                    return res.status(status).json(response);
                } else {
                    status = 200;
                    response = data;
                    return res.status(status).json(response);
                }
            });
        }
    })
}