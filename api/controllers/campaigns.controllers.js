const path = require('path');

const async = require('async');
const _ = require('lodash');

const mongoose = require('mongoose');
const Campaign = mongoose.model('Campaign');
const User = mongoose.model('User');
const ObjectId = require('mongodb').ObjectId;

const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));
const socialPortalService = require(path.join(__dirname, '..', 'services', 'socialportal.service'));
const blacklistService = require(path.join(__dirname, '..', 'services', 'blacklist.service'));

module.exports.getCampaign = function(req, res) {
	console.log('[getCampaign controller]');

	if(req && req.params && req.params.nameSlug) {

		Campaign.findOne({'nameSlug': req.params.nameSlug}).exec(function(err, campaign) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid campaign nameSlug. ' + err};
			} else if (!campaign) {
				status = 404;
				response = {message: 'Campaign not found. '};
			} else {
				status = 200;
				response = {message: 'OK', campaign};
			}

			console.log(status + ' ' + response.message);
			res.status(status).json(response);

		});
	}
};

module.exports.getCampaigns = function(req, res) {
	console.log('[getCampaigns controller]');
	// default values, because offset and limit are required
	var offset = 0, limit = 12;
	var filterBy, sortBy;

	if(req.query) {

		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			offset = parseInt(req.query.offset);
			delete req.query.offset;
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			limit = parseInt(req.query.limit);
			delete req.query.limit;
		}

		// if any filters are provided in the URL, parse them and add to the filtering params object.
		if(req.query.filter) {
			filterBy = filter(req.query.filter);
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
		if(req.query.sort) {
			sortBy = sort(req.query.sort);
		}

		if(req.query.lng && req.query.lat) {
			// TODO find campaigns close to the given coordinates. 
		}

	}

	Campaign.find(filterBy).sort(sortBy).skip(offset).limit(limit).exec(function(err, campaigns) {
		var status, response;

		if(err) {
			status = 500;
			response = {message: 'Internal server error. ' + err};
		} else if(!campaigns || campaigns.length == 0) {
			status = 404;
			response = {message: 'No campaigns found with the given search criteria. '};
		} else {
			status = 200;
			response = {message: 'OK', campaigns};
		}
		
		console.log(status + ' ' + response.message);
		res.status(status).json(response);

	});
};

module.exports.addCampaign = function(req, res) {
	console.log('[addCampaign controller]', req.body);

	if(req.body) {
		create(req.body, function(newCampaign) {
			if(newCampaign) {
				newCampaign.brand.id = new ObjectId(newCampaign.brand._id);
				determineCampaignSlug(req.body.nameSlug, 0,  function(err, nameSlug){

					newCampaign.nameSlug = nameSlug;
					newCampaign.user = {
						_id: req.user._id,
						name: req.user.firstName + (req.user.lastNamePrefix ? " " + req.user.lastNamePrefix + " " : " ") + req.user.lastName,
						email: req.user.email
					};
					newCampaign.wholesaler = {
						name : req.user.wholesalers[0].name
					}
					if(newCampaign.update == true){
						delete newCampaign.strategy
					}

					Campaign.create(newCampaign, function (err, campaign) {
						var status, response;
						if(err) {
							status = 400;
							response = {message: 'Campaign could not be added. Error message: ' + err};
							return res.status(status).json(response);
						} else if(!campaign) {
							status = 404;
							response = {message: 'Campaign not found. '};
							return res.status(status).json(response);
						} else {
							status = 201;
							response = {message: 'Campaign added. ', campaign};

							//send mail to the team
							var options = {
								to: 'info@prismanote.com',
								subject: 'Nieuwe campagne aangemaakt',
								template: 'campaigns/new-campaign'
							}

							//Mail sturen met activatielink na aanmaken
							mailService.mail(options, {campaign: campaign}, null, function(err, data){
								console.log(status + ' ' + response.message);
								if(err) {
									console.log(err.stack);
								}
								return res.status(status).json(response);
							});
						}
					});
				});
			}
		});
	}
};

function determineCampaignSlug(name, count, callback){
	var nameSlug = name;
	if(count > 0){
		nameSlug = name + '-' + count;
	}
	Campaign.find({'nameSlug': nameSlug}).sort({_id: -1}).exec(function(err, campaign){
		if(err){
			return callback(err);
		}
		if(campaign.length == 0){
			return callback(null, nameSlug);
		}else{
			//count = count+1;
			return determineCampaignSlug(name, count+1, callback);
		}
	})
}

module.exports.approveCampaign = function(req, res){
	console.log("approveCampaign");

	Campaign.findOne({_id: req.body.campaign._id}).exec(function(err, campaign){
		if(err){
			return res.status(500).json({message: "Error finding campaign: " + err});
		}
		campaign.isVerified = true;
		campaign.verifiedDate = new Date();

		var emailAddresses = campaign.emailReceivers.suggested.concat(campaign.emailReceivers.added);

		async.forEach(emailAddresses, function(email, emailsDone){
			if(typeof email.send != 'undefined' && !email.send){
				return emailsDone();
			}
			var param = '{"campaign": "' + campaign._id + '", "email": "' +email.email + '"}';
			var baseParam = new Buffer(param).toString('base64');

			User.findOne({email: email.email}).exec(function(err, user){
				//check if user & brand are found on the blacklist
				blacklistService.checkBrandBlacklist(email.email, campaign.brand.name, null, function(result){
					if(result){
						//don't send an email 
						return emailsDone();
					}

					var unsubscribeParam = '{"email": "' +email.email + '", "brand": "' + campaign.brand.name +'"}'
					var unsubscribeHref = new Buffer(unsubscribeParam).toString('base64');
				

					if(process.env.NODE_ENV == 'production'){
						var link = "https://prismanote.com/retailer/welcome/" + baseParam;
					}else{
						var link = "http://127.0.0.1:3000/retailer/welcome/" + baseParam;
					}

					var facebookTask = _.find(campaign.tasks, {"type": "facebook"});

					var options = {
						to: email.email,
						subject: "Verzoek delen campagne",
						template: 'campaigns/shareCampaign-mail-a'
					}

					var data = {
						campaign: campaign,
						link: link,
						photo: facebookTask.images[0].src,
						unsubscribeHref: unsubscribeHref
					}
			
					//Mail sturen met activatielink na aanmaken
					mailService.mail(options, data, null, function(err, data){
						emailsDone();
					});
				})
			})
			
		}, function(){
			//emails done

			//send confirmation to the creator of the campaign
			var options = {
				to: campaign.user.email,
				subject: 'Campagne goedgekeurd',
				template: 'campaigns/campaign-approved'
			}

			//Mail sturen met activatielink na aanmaken
			mailService.mail(options, {campaign: campaign}, null, function(err, data){
				// campaign.save(function(err, result){
				// 	res.json({'status':200,'campaign':campaign});
				// })
				Campaign.findByIdAndUpdate(campaign._id, {$set: campaign}, {new: true}, function (err, campaign) {
					res.json({'status':200,'campaign':campaign});
				})
			});

		})
	})
}

module.exports.updateCampaign = function(req, res) {
	console.log('[updateCampaign controller]');

	if(req.body.campaign) {

		Campaign.findByIdAndUpdate(req.body.campaign._id, {$set: req.body.campaign}, {new: true}, function (err, campaign) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Campaign could not be added. Error message: ' + err};
			} else if(!campaign) {
				status = 404;
				response = {message: 'Campaign not found. '};
			} else {
				status = 200;
				response = {message: 'Campaign updated. ', campaign};
			}

			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}

			res.status(status).json(response);
		});
	}
};

module.exports.removeCampaign = function(req, res) {
	console.log('[removeCampaign controller]');

	if(req.params && req.params.id) {

		Campaign.findByIdAndRemove({_id: req.params.id}, function(err, campaign) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid campaign ID. ' + err};
			} else if(!campaign) {
				status = 404;
				response = {message: 'Campaign not found. '};
			} else {
				status = 204;
				response = {message: 'Campaign deleted. '};
			}

			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}
			// Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
			res.status(status);
		});
	
	}
};


module.exports.loadRetailerWelcomeAndParseData = function(req,res){
	console.log("loadRetailerWelcomeAndParseData");
	var data = JSON.parse(Buffer.from(req.body.hashedData, 'base64').toString('ascii'));
	
	//First check if the user is already logged in
	//in every result we must send the decoded data so we can add that into the scope on the front

	Campaign.findOne({_id: ObjectId(data.campaign)}).exec(function(err, campaign){	
		if(!req.user){
			return res.status(207).json({message: 'Not logged in', data, campaign})
		}else{
			if(req.user.email != data.email){
				return res.status(203).json({message: "Different account", data, campaign})
			}else{
				return res.status(200).json({message: "Everything is OK", data, campaign})
			}
		}
	})
}

module.exports.addCampaignFromMailToSocialPortal = function(req, res){
	console.log("addCampaignFromMailToSocialPortal", req.user);
	var campaignId = req.body.campaign;

	//first find social portal or create one if it doesn't exist
	socialPortalService.getUserSocialPortal(req.user._id, function(err, data){

		if(err && err.code == 404){
			console.log("err", err);
			//no portal exisit, create new one
			socialPortalService.createSocialPortal(req.user, function(err, data){

				if(err){
					return res.status(err.code).json({message: err.msg});
				}
				addCampaignAndBrand(campaignId, data);
			})
		}else{
			addCampaignAndBrand(campaignId, data);
		}
	})

	function addCampaignAndBrand(campaignId, portal){
		console.log("addCampaignAndBrand", campaignId, portal);
		Campaign.findOne({_id: campaignId}).exec(function(err, campaign){
			if(err){
				return res.status(500).json({message: "Error while getting campaign: " + err})
			}
			console.log("campaign", campaign);

			//check if campaign is already added
			checkCampaign(portal.campaigns, campaign.nameSlug, function(result){
				console.log("campaigncall", result);
				if(result){
					console.log("here");
					//campaign does not exists on the portal, now let's check the brand
					checkBrands(portal.brands, campaign.brand.name, function(brandResult){
						console.log("callbrand", brandResult);
						if(brandResult){
							//brand does not exists, add it
							var brand = {
								_id: campaign.brand._id,
								name: campaign.brand.name
							}
							portal.brands.push(brand);
						}
						//brand already exists, do nothing with it, and continue with saving the portal
						portal.campaigns.push(campaign);
						socialPortalService.saveSocialPortal(portal._id, portal, function(err, portalResult){
							if(err){
								return res.status(500).json({message: "Error while saving portal: " + err})
							}
							return res.status(200).json({message: "OK", result})
						})
						
					})
				}else{
					//Campaign is already added to the portal, abort!
					return res.status(206).json({message: "This campaign has already been added to this portal"})
				}
			})
		})
	}
}

function checkCampaign(campaigns, searchSlug, callback){
	console.log("checkCampaign", campaigns, searchSlug);
	console.log("lenghth", campaigns.length);
	if(campaigns.length == 0){
		console.log("no campaigns");
		return callback(true)
	}else{
		var counter = 0;
		for(var i = 0; i < campaigns.length; i++){
			
			if(campaigns[i].nameSlug == searchSlug){
				return callback(false);
			}
			counter++;
		}

		if(counter == campaigns.length){
			return callback(true);
		}
	}
}

function checkBrands(brands, brandName, callback){
	console.log("checkBrands", brands, brandName);
	if(brands.length == 0){
		return callback(true);
	}
	for(var i=0; i< brands.length; i++){
		if(brands[i].name == brandName){
			return callback(false);
		}
		if(i == brands.length){
			return callback(true);
		}
	}
}



