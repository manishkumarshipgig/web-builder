const path = require('path');
const request = require('request');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const fbInsights = mongoose.model('fbInsights');
const adminToken = mongoose.model('adminToken');
const Campaign = mongoose.model('Campaign');
const Socialportal = mongoose.model('Socialportal');
const wholeSaler = mongoose.model('Wholesaler');
const Brand = mongoose.model('Brand');
const Product = mongoose.model('Product');

const accountId = "act_" + settings.facebook.advertiserId;

const accesstoken = settings.facebook.accessToken;

function getAppAccessToken(user, callback) {
    console.log("getAppAccessToken");

    //temp way to get accessToken (long live accessToken from one fb account)
    return callback(accesstoken);

    // User.findOne({_id: user._id}).select('+facebook.extendedToken +facebook.extendedPageToken +pinterest.accessCode').exec(function(err, user){

    //     var diff = new DateDiff(new Date(), new Date(user.facebook.accessTokenDate));

    //     if(diff.days() >= 59 || !user.extendedToken){
    //         facebookService.extendAccessToken(user.facebook.token, function(err, result){
    //             if (err){
    //                 console.log("err", err);
    //                 return;
    //             }
    //             user.facebook.extendedToken = result.access_token;
    //             user.facebook.tokenDate = new Date();

    //             userService.updateUser(user._id, user, function(err, saveResult){
    //                 if (err) {
    //                     console.log("update err", err);
    //                     return;
    //                 }
    //                 getToken(user, function(token){
    //                     return callback(token);
    //                 })
    //             })
    //         })
    //     }else{
    //         getToken(user, function(token){
    //             return callback(token);
    //         })
    //     }
    // })

    // function getToken(user, callback){
    //     return callback(user.facebook.extendedToken);
    // }
}
module.exports = {
    readAdset: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '/adsets?fields=name,campaign,lifetime_budget,bid_amount,bid_info,targeting&access_token=' + accessToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    readAds: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '/ads?fields=name&access_token=' + accessToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    readInsight: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '/insights?metric=page_impressions&access_token=' + accessToken + '&date_preset=' + req.body.date_preset + '&debug=all&format=json&method=get&pretty=0&suppress_http_code=1';

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    getFbPrev: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + req.body.createId + '/previews?access_token=' + accessToken + '&ad_format=' + req.body.format;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    customlocation: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + req.body.pageId + '?fields=location&access_token=' + accessToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    intagramAccount: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + req.body.pageId + '?fields=instagram_accounts&access_token=' + req.body.pageToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    readAdcamp: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '/campaigns?fields=id,name,effective_status&access_token=' + accessToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    getpagetoken: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + req.body.pageId + '?fields=access_token&access_token=' + accessToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    campInsightss: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '/insights?fields=clicks,ctr,frequency,cost_per_unique_click,spend,impressions,reach,cpm&access_token=' + accessToken + '&date_preset=last_90d';

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    fbAdsPixel: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            fbAdPixelFunc(accountId, req.body, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    adaccountInsights: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '/insights?fields=spend,impressions,reach,cpc&access_token=' + accessToken + '&date_preset=' + req.body.date_preset;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    campInsights: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '/insights?fields=spend,impressions,reach,cpc&access_token=' + accessToken + '&time_range[since]=' + req.body.start_date + '&time_range[until]=' + req.body.stop_date;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    campaignExecute: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '/adsets?fields=id,name&access_token=' + accessToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    totalSpent: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '?fields=amount_spent,currency&access_token=' + accessToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    },
    campaignName: function (req, res) {
        getAppAccessToken(req.user, function (accessToken) {
            var url = 'https://graph.facebook.com/v2.11/' + accountId + '?fields=id,name,start_time,stop_time&access_token=' + accessToken;

            getCodeRequest(url, function (err, countrycode) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(countrycode);
                }
            });
        })
    }
}

function fbAdPixelFunc(acctID, data, callback) {
    getAppAccessToken(req.user, function (accessToken) {
        request.post({
            headers: { 'content-type': 'application/json' },
            url: 'https://graph.facebook.com/v2.11/' + acctID + '/adspixels',
            body: JSON.stringify(data),
            method: 'POST'
        }, function (error, response, body) {
            callback(null, body);
        });
    });

}

module.exports.getAdminToken = function (req, res) {
    adminTokenReq(req.body, accountId, function (err, data) {
        if (err) {
            res.send(err);
        } else {
            saveAdminToken(data, function (err, result) {
                if (err) {
                    return res.status(err.code).json({ message: err.err })
                } else {
                    console.log("admin token result", result);
                    //clean the cart
                    res.send(result);
                }
            })
            //res.send(data);                
        }
    });

}
function adminTokenReq(data, id, callback) {
    request.get({
        headers: { 'content-type': 'application/json' },
        url: 'https://graph.facebook.com/v2.11/oauth/access_token?grant_type=fb_exchange_token&client_id=' + settings.facebook.appId + '&client_secret=' + settings.facebook.appSecret + '&fb_exchange_token=' + data.accessToken,
        method: 'GET'
    }, function (error, response, body) {
        if (error) {
            console.log("errror", error);
        } else {
            console.log('token body', body);
            callback(null, body);
        }
    });
}
saveAdminToken = function (body, cb) {
    var fb_token = [];
    saveFbtokenFunc(body, function (err, newToken) {
        if (err) {
            console.log("Err", err);
            cb(err);
        } else {
            fb_token.push(newToken);
            cb(null, fb_token);
        }
    })

}
saveFbtokenFunc = function (body, callback) {
    var date = new Date();
    var adminData = {
        'date': date,
        'access_token': JSON.parse(body).access_token
    }
    var admintoken = new adminToken(adminData);
    adminToken.find(function (err, adminTokens) {
        if (err) {
            console.log('admintoken error..', err);
        } else {
            if (adminTokens.length > 0) {
                adminToken.findByIdAndUpdate(adminTokens[0]._id, { $set: adminData }, { new: true }, function (err, result) {
                    if (err) {
                        console.log("Error during saving access token", err);
                        return callback(err);
                    } else {
                        console.log(" update access token", result);
                        return callback(null, result);
                    }
                })
            } else {
                admintoken.save(function (err, result) {
                    if (err) {
                        console.log("Error during saving order", err);
                        return callback(err);
                    } else {
                        return callback(null, result);
                    }
                })
            }
        }

    });

};


module.exports.createCampaign = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        req.body.access_token = accessToken;

        createCampaignRequest(req.body, accountId, function (err, campaign) {
            const campaignId = JSON.parse(campaign).id;
            res.send(campaign);
        });
    })

};

function createCampaignRequest(data, id, callback) {
    request.post({
        headers: { 'content-type': 'application/json' },
        url: 'https://graph.facebook.com/v2.11/' + id + '/campaigns',
        body: JSON.stringify(data),
        method: 'POST'
    }, function (error, response, body) {
        callback(null, body);
    });
}

// Get Country Code 
module.exports.getCountryCode = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        var url = 'https://graph.facebook.com/v2.11/search/?access_token=' + accessToken + '&location_types=["country"]&type=adgeolocation&limit=300&debug=all&format=json&method=get&pretty=0&suppress_http_code=1';

        getCodeRequest(url, function (err, countrycode) {
            if (err) {
                res.send(err);
            } else {
                res.send(countrycode);
            }
        });
    })
};

// Get Region Code 
module.exports.getRegionCode = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        // var url = 'https://graph.facebook.com/v2.11/search/?access_token=' + accessToken + '&location_types=["region"]&type=adgeolocation&q=' + req.body.query + '&debug=all&format=json&method=get&pretty=0&suppress_http_code=1';
        //var url = 'https://graph.facebook.com/v2.11/search/?access_token=' + accessToken + '&location_types=["region"]&type=adgeolocation&limit=5000&debug=all&format=json&method=get&pretty=0&suppress_http_code=1';
        var url = 'https://graph.facebook.com/v2.11/search/?access_token=' + accessToken + '&location_types=["region"]&type=adgeolocation&country_code=' + req.body.country_code + '&limit=5000&debug=all&format=json&method=get&pretty=0&suppress_http_code=1';
        getCodeRequest(url, function (err, regioncode) {
            if (err) {
                res.send(err);
            } else {
                res.send(regioncode);
            }
        });
    })
};



// Get City Code
module.exports.getCityCode = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        var url = 'https://graph.facebook.com/v2.11/search/?access_token=' + accessToken + '&location_types=["city"]&type=adgeolocation&region_id=' + req.body.regionID + '&q=' + req.body.query + '&limit=5000&debug=all&format=json&method=get&pretty=0&suppress_http_code=1';
        getCodeRequest(url, function (err, citycode) {
            if (err) {
                res.send(err);
            } else {
                res.send(citycode);
            }
        });
    })
};

module.exports.createInterest = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        var url = 'https://graph.facebook.com/v2.11/search/?access_token=' + accessToken + '&type=adinterest&q=' + req.body.query;
        console.log('url', url);
        getCodeRequest(url, function (err, citycode) {
            res.send(citycode);
        });
    })
};

module.exports.createAdset = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        req.body.access_token = accessToken;
        adsSetCreateRequest(accountId, req.body, function (err, adset) {
            res.send(adset);
        });
    })
};

function getCodeRequest(url, callback) {
    request(url, function (error, response, body) {
        if (body) {
            callback(null, JSON.parse(body));
        }
    });

}

function adsSetCreateRequest(acctID, data, callback) {
    request.post({
        headers: { 'content-type': 'application/json' },
        url: 'https://graph.facebook.com/v2.11/' + acctID + '/adsets',
        body: JSON.stringify(data),
        method: 'POST'
    }, function (error, response, body) {
        callback(null, body);
    });

}

// Create AdCreative
module.exports.createAdCreative = function (req, res) {
    req.body.access_token = accesstoken;
    adCreativeRequest(accountId, req.body, function (err, adcreative) {
        res.send(adcreative);
    });
};

function adCreativeRequest(acctID, data, callback) {
    request.post({
        headers: { 'content-type': 'application/json' },
        url: ' https://graph.facebook.com/v2.11/' + acctID + '/adcreatives',
        body: JSON.stringify(data),
        method: 'POST'
    }, function (error, response, body) {
        callback(null, body);
    });
}

// Create Ad
module.exports.createAd = function (req, res) {
    var data = req.body;
    data.access_token = accesstoken;
    adCreateRequest(accountId, data, function (err, adsresult) {
        res.send(adsresult);
    })
};

function adCreateRequest(acctID, data, callback) {
    request.post({
        headers: { 'content-type': 'application/json' },
        url: 'https://graph.facebook.com/v2.11/' + acctID + '/ads',
        body: JSON.stringify(data),
        method: 'POST'
    }, function (error, response, body) {
        callback(null, body);
    });
}

module.exports.getAdminAccountId = function (req, res) {
    getAppAccessToken(req.user, function (token) {
        var adminID = {
            "accountId": accountId,
            "access_token": token
        }
        res.send(adminID);
    })

};

// Get Estimated Reach
module.exports.getReach = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        var url = 'https://graph.facebook.com/v2.11/' + accountId + '/reachestimate';
        // var reachData = req.body; 
        var reachData = {
            "geo_locations": req.body.adGeoLocation,
            "age_min": req.body.ageMin,
            "age_max": req.body.ageMax,
            "genders": [req.body.adsetGender],
            'interests': req.body.interests
        };

        var options = {
            method: 'GET',
            url: 'https://graph.facebook.com/v2.11/' + accountId + '/reachestimate',
            qs: {
                access_token: accessToken,
                targeting_spec: JSON.stringify(reachData)
            },
            daily_budget: req.body.daily_budget,
            currency: "EURO",
            optimize_for: "REACH",
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            }
        };

        getCodeRequest(options, function (err, reach) {
            res.send(reach);
        });
    })
};


// function getReachRequest(options, callback) {
//     request(options, function (error, response, body) {
//         callback(null, JSON.parse(body));
//     });
// }


// Image Upload Base64

module.exports.createAdImageHash = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        req.body.access_token = accessToken;;

        adCreateImageHashRequest(accountId, req.body, function (err, adsresult) {
            res.send(adsresult);
        })
    })
};


function adCreateImageHashRequest(acctID, data, callback) {
    request.post({
        headers: { 'content-type': 'application/json' },
        url: 'https://graph.facebook.com/v2.11/' + acctID + '/adimages',
        body: JSON.stringify(data),
        method: 'POST'
    }, function (error, response, body) {
        callback(null, body);
    });
}
// Get Custom Audience Code

module.exports.getAudience = function (req, res) {
    getAppAccessToken(req.user, function (accessToken) {
        getCustomAudience(accessToken, function (err, audience) {
            console.log("audience", audience.data);
            if (audience.data.length > 0) {
                if (audience.data[0].id) {
                    var url = 'https://graph.facebook.com/v2.11/' + audience.data[0].id + '?access_token=' + accessToken + "&fields=name,rule";
                    getCodeRequest(url, function (err, audience) {
                        res.send(audience);
                    });
                }
            } else {
                res.send({});
            }
        });
    })
};

// function getAudience(url, callback) { 
//     request(url, function (error, response, body) {
//         if(body){
//             callback(null, JSON.parse(body));
//         }
//     });
// }

function getCustomAudience(accessToken, callback) {
    request('https://graph.facebook.com/v2.11/' + accountId + '/customaudiences?access_token=' + accessToken + '&debug=all&format=json&method=get&pretty=0&suppress_http_code=1', function (error, response, body) {
        console.log('custom audience data is:>>', JSON.parse(body));
        callback(null, JSON.parse(body));
    });
}


// Page Insights

module.exports.getInsights = function (req, res) {
    console.log(req.body);
    let data = req.body;
    getInsightsFunction(data, function (err, insights) {
        console.log('insights', insights);
        res.send(insights);
    });
};

function getInsightsFunction(data, callback) {
    let metric = data.metric;//page_fans
    let tokenPage = data.access_token;//page_access_token
    let date_preset = data.date_preset;//

    request('https://graph.facebook.com/v2.11/me/insights/' + metric + '?access_token=' + tokenPage + '&date_preset=' + date_preset + '&debug=all&format=json&method=get&pretty=0&suppress_http_code=1', function (error, response, body) {
        console.log('Insights:>>>>', JSON.parse(body));
        callback(null, JSON.parse(body));
    });
}

module.exports.saveFbInsight = function (req, res) {
    status = 200;
    processFbInsight(req.body, function (err, result) {
        if (err) {
            return res.status(err.code).json({ message: err.err })
        } else {
            console.log("saveFbInsight result", result);
            //clean the cart
            res.send(result);
        }
    })
}
processFbInsight = function (body, cb) {
    var fb_insight = [];
    saveFbinsightFunc(body, function (err, newOrder) {
        if (err) {
            cb(err);
        } else {
            fb_insight.push(newOrder);
            cb(null, fb_insight);
        }
    })

}
saveFbinsightFunc = function (body, callback) {
    var date = new Date();
    var fbInsight = new fbInsights({
        'impressions': body.data[0].impressions,
        'reach': body.data[0].reach,
        'cost_per_click': body.data[0].cpc,
        'amount_spent': body.data[0].spend,
        'end_date': body.data[0].date_stop,
        'date': body.data[0].date,
        'isFbInsight': true
    });

    fbInsight.save(function (err, result) {
        if (err) {
            return callback(err);
        } else {

            return callback(null, result);
        }
    })
};
module.exports.getFbInsight = function (req, res) {
    fbInsights.find(function (err, fbInsightss) {
        if (err) {
            res.send(err);
        } else {
            res.send(fbInsightss);
        }

    });
}
module.exports.DBWholesalers = function (req, res) {
    wholeSaler.find(function (err, wholeSalers) {
        if (err) {
            res.send(err);
        } else {
            res.send(wholeSalers);
        }

    });
}
module.exports.getCampaignDB = function (req, res) {
    Campaign.find(function (err, Campaigns) {
        if (err) {
            res.send(err);
        } else {
            res.send(Campaigns);
        }

    });
}
module.exports.getSocialPortalDB = function (req, res) {
    Socialportal.find(function (err, Socialportals) {
        if (err) {
            res.send(err);
        } else {
            res.send(Socialportals);
        }

    });
}
module.exports.getPortalUserData = function (req, res) {
    console.log('req.body.brand', req.body.brand);
    Campaign.find({ 'brand.nameSlug': req.body.brand }, function (err, Campaigns) {
        if (err) {
            res.send(err);
        } else {
            res.send(Campaigns);
        }

    });
}

function updatePermission(data, id) {
    return new Promise(function (resolve, reject) {
        request.post({
            headers: { 'content-type': 'application/json' },
            url: 'https://graph.facebook.com/v2.11/' + id + '/users',
            body: JSON.stringify(data),
            method: 'POST'
        }, function (error, response, body) {
            if (error) {
                console.log("error", error);
                reject(true);
            }
            else {
                console.log("update permission body", body);
                resolve(body);
            }
        });
    });

}

module.exports.grantAccessToBrand = function (req, res) {
    console.log('called grant access to brnad');
    var permissionData = {
        'uid': req.body.userIDBrand,
        'role': 1001,
        'access_token': req.body.token
    };
    var adAcctId = accountId;
    updatePermission(permissionData, adAcctId).then(function (data) {
        res.send({ "success": true, "data": data });
    })
        .catch(function (err) {
            res.send({ "error": err });
        });
};

module.exports.postcreditSocialPortal = function (req, res) {
    console.log('post credti called', req.body);
    var data = req.body;
    // var permissionData = {
    //   'credit': req.body.credit,
    // };
    saveCreditInSocial(data).then(function (data) {
        res.send({ "success": true, "data": data });
    }, function (err) {
        res.send({ "error": err });
    })
};
function saveCreditInSocial(data) {

    return new Promise(function (resolve, reject) {
        console.log('post saveCreditInSocial called');
        var brands = {
            "brands": [
                {
                    "name": data.brandName,
                    "credit": data.credit
                }
            ]
        }
        Socialportal.findOneAndUpdate({ "brands.name": data.brandName }, { $set: brands }, { new: true }, function (err, result) {
            if (err) {
                console.log("Error during saving access token", err);
                reject(err);
            } else {
                resolve(result);
            }
        })
    });

}
module.exports.postCreditCampaign = function (req, res) {
    console.log('post credit campaign called', req.body);
    var data = req.body;
    // var permissionData = {
    //   'credit': req.body.credit,
    // };
    CreditInCampaign(data).then(function (data) {
        res.send({ "success": true, "data": data });
    }).catch(function (err) {
        res.send({ "error": err });
    });
};
function CreditInCampaign(data) {

    return new Promise(function (resolve, reject) {
        console.log('post CreditInCampaign called');
        Socialportal.find({ "users.email": "ajeetrathore11@gmail.com" }, function (err, result) {
            if (err) {
                console.log('social portaal error..', err);
                reject(err);
            } else {
                if (result.length > 0) {
                    var selectedCamp = [];
                    var unselect = [];
                    console.log('finnded the campaigns', result);
                    result[0].campaigns.forEach(function (elem) {
                        if (elem.brand.name == "Prisma") {
                            elem.credit = true;
                            selectedCamp.push(elem);
                        } else {
                            unselect.push(elem);
                        }
                    })
                    console.log('selectedCamp', selectedCamp);
                    unselect.forEach(function (res) {
                        selectedCamp.push(res);
                    })
                    // var array = result[0].campaigns.filter(function(val){
                    //     return val.brand.name == 'Prisma';
                    // })

                    var campaign = {
                        "campaigns": selectedCamp
                    }
                    Socialportal.update({ "users.email": "ajeetrathore11@gmail.com" }, { $set: campaign }, { new: true }, function (err, result) {
                        if (err) {
                            console.log("Error during saving access token", err);
                            reject(err);
                        } else {
                            console.log(" update access token", result);
                            resolve(result);
                        }
                    })
                } else {

                }
            }

        });
    });

}
module.exports.verifyMarketingUser = function (req, res) {
    var userdata = req.body.data;
    // var permissionData = {
    //   'credit': req.body.credit,
    // };
    saveMarketingFunc(userdata).then(function (resp) {
        res.send({ "success": true, "data": resp });
    }).catch(function (err) {
        res.send({ "success": false, "error": 'this is my error' });
    });
};
function saveMarketingFunc(data) {
    return new Promise(function (resolve, reject) {

        User.find({ 'brandPortalId': data._id }, function (err, result) {
            if (result.length > 0) {
                if(data.check == true){
                //result[0].isVerified = true;
                }else if(data.verify == true){
                    result[0].isVerified = true;
                }else{
                    result[0].isVerified = false;
                }
                var UpdateData = result[0];
                User.findOneAndUpdate({ "brandPortalId": data._id }, { $set: UpdateData }, { new: true }, function (err, result) {
                    if (err) {
                        console.log("Error during saving access token", err);
                        reject(err);
                    } else {
                        resolve(result);
                    }
                })
            } else {
                reject('error');
            }
        })
    });

}
module.exports.isRegisteredUser = function (req, res) {
    var userdata = req.body.data;
    // var permissionData = {
    //   'credit': req.body.credit,
    // };
    isRegisterFunc(userdata).then(function (resp) {
        res.send({ "success": true, "data": resp });
    }).catch(function (err) {
        res.send({ "success": false, "error": err });
    });
};
function isRegisterFunc(data) {
    return new Promise(function (resolve, reject) {

        User.find({ 'brandPortalId': data.brandId }, function (err, result) {
            if (result.length > 0) {
                        reject(result);
                    } else {
                        resolve(result);
                    }
        })
    });

}

module.exports.unLinkFacebook = function (req, res) {
    if (req.user.facebook) {
    var user = {};
    user.facebook = {};
      User.findByIdAndUpdate(req.user._id, { $set: user }, { new: true }, function (err, userUpdated) {
        if (err) {
          res.send({'fbLogout':false});
        }
        res.send({'fbLogout':true,'data': userUpdated});
    });
    }

};

module.exports.getBrandListGrapgh = function(req, res) {
    Brand.find({}, function (err, data) {
        if (err) {
          res.send(err);
        }
        
        var arr = [];
        data.forEach(function(val){
            var obj = {};
            obj.name = val.name;
            obj.id = val._id;
            obj.image = settings.s3Bucket.location + val.images[0].src;
            arr.push(obj);
        })
        res.send(arr);
    });

};
module.exports.getAllBrandGrapgh = function(req, res) {
    Brand.find({}, function (err, data) {
        if (err) {
          res.send(err);
        }
        
        res.send(data);
    });

};
module.exports.getAllProductsGrapgh = function(req, res) {
    Product.find({'brand.name':'Elysee'}, function (err, data) {
        if (err) {
          res.send(err);
        }
        
        res.send(data);
    });

};
module.exports.getAllcampaignsGrapgh = function(req, res) {
    Campaign.find({}, function (err, data) {
        if (err) {
          res.send(err);
        }
        res.send(data);
    });

};
module.exports.removeMarketingUser = function(req,res){
    var userId = req.body.id;
    console.log("brand potal id",userId);
    if(userId !== undefined){
        User.findOneAndRemove({"brandPortalId": userId},function(err,data){
            if(err){
                res.send(err);
            }else{
                res.send(data);
            }
        })
    }else{
        res.send({"success":false,"error":"user id is not defined"});
    }
};

module.exports.getCurrencyFacebook = function(req, res) {
    request.get({
        headers: { 'content-type': 'application/json' },
        url: ' https://graph.facebook.com/v2.11/' +accountId +'?fields=currency&access_token='+ accesstoken,
        method: 'GET'
    }, function (error, response, body) {
        if (error) {
            res.send(error);
        } else {
            console.log('token body', body);
            res.send(body);
        }
    });

};

module.exports.getPermission = function(req,res){
    console.log('req.user',req.user.facebook.profileId);
    
    res.send('hello');
    var userId = req.user.facebook.profileId;
    var permissionData = {
        'uid': userID,
        'role': 1002,
        'access_token': accesstoken
      }
      updatePermission(permissionData, accountId, function (err, rslt) {
        console.log('rslt', rslt);
        if(err){
            res.send(err);
        }
        res.send(rslt);
      });

}

function updatePermission(data, id, callback) {
    console.log('dta of per',JSON.stringify(data));
    
    request.post({
      headers: { 'content-type': 'application/json' },
      url: 'https://graph.facebook.com/v3.0/' + accountId + '/users',
      body: JSON.stringify(data),
      method: 'POST'
    }, function (error, response, body) {
      callback(null, body);
    });
  }
