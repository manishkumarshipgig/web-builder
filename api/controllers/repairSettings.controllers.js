const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports.updateRepairSettingsStatus = function (req, res) {
    User.findByIdAndUpdate(req.body.data._id, { $set: {'productStatus':req.body.data.repairProductStatus} }, { upsert: true }, function (err, userUpdated) {
        if (err) {
            res.send({'repairProductStatus':false});
        }else {
            res.send({'repairProductStatus':true,'data': userUpdated});
        }
    });
};

module.exports.updateRepairSettingsProductKind = function (req, res) {
    console.log("updateRepairSettingsProductKind ",req.body.data);
      User.findByIdAndUpdate(req.body.data._id, { $set: {'productKind':req.body.data.productKind} }, { upsert: true }, function (err, userUpdated) {
        if (err) {
          res.send({'repairProductKind':false});
        } else {
        res.send({'repairProductKind':true,'data': userUpdated});
        }
    });
};

module.exports.updateTableOptions = function (req, res) {
    console.log("updateTableOptions ",req.body.data);
      User.findByIdAndUpdate(req.body.data._id, { $set: {'tableOptions':req.body.data.tableOptions} }, { upsert: true }, function (err, userUpdated) {
        if (err) {
          res.send({'tableOptions':false});
        } else {
        res.send({'tableOptions':true,'data': userUpdated});
        }
    });
};

module.exports.updateRepairSettingsProductMaterial = function (req, res) {
    User.findByIdAndUpdate(req.body.data._id, { $set: {'productMaterial':req.body.data.repairProductStatus} }, { upsert: true }, function (err, userUpdated) {
        if (err) {
          res.send({'repairProductMaterial':false});
        } else {
        res.send({'repairProductMaterial':true,'data': userUpdated});
        }
    });
};

module.exports.updateRepairSettingsCustomTags = function (req, res) {
    User.findByIdAndUpdate(req.body.data._id, { $set: {'customTagss':req.body.data.repairCustomTagss} }, { upsert: true }, function (err, userUpdated) {
        if (err) {
          res.send({'repairCustomTagss':false});
        } else {
        res.send({'repairCustomTagss':true,'data': userUpdated});
        }
    });
};