const path = require('path');
const mongoose = require('mongoose');
const Shop = mongoose.model('Shop');
const UpgradeOrder = mongoose.model('UpgradeOrder');
const ObjectId = require('mongodb').ObjectId;
const async = require('async');

const upgradeShopService = require(path.join(__dirname, '..', 'services', 'upgradeShop.service'));
const mollieService = require(path.join(__dirname, '..', 'services', 'mollie.service'));
const orderService = require(path.join(__dirname, '..', 'services', 'order.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));

module.exports.getShopModules = function(req, res){
    upgradeShopService.getShopModules(req.query.shopId, function(err, result){
        if(err){
            return res.status(500).json({message: err});
        }
        return res.status(200).json({message: 'OK', modules: result})
    })
}

function createorUpdateOrder(orderId, order, callback){
    upgradeShopService.createOrUpdateUpgradeOrder(orderId, order, function(err, result){
        if(err){
            return callback(err);
        }
        return callback(null, result);
    })
}

function checkShopMollieId(shop, callback){
    if(typeof shop.mollieId != 'undefined' && shop.mollieId != null){
        return callback(true, shop.mollieId);
    }else{
        mollieService.createCustomer(shop.accountHolder, shop.email, function(err, result){
            Shop.findOneAndUpdate({_id: shop._id}, {mollieId: result.id}, {new: true}).exec(function(err, shop){
                if(err){
                    console.error("error", error);
                    return callback(false);
                }
                return checkShopMollieId(shop, callback);
            })
        })
    }
}

function getModuleName(code){
    switch(code.toUpperCase()){
        case 'COUNTRP':
        case 'COUNTRC':
        case 'COUNTRB':
            return 'Countr';
        case 'MARKT30':
        case 'MARK60':
        case 'MARKT120':
        case 'MARKT200':
            return 'Marketing ondersteuning';
    }
}

module.exports.startUpgrade = function(req, res){
    var package = req.body.package;
    var shopId = req.body.shopId;

    Shop.findOne({_id: ObjectId(shopId)}).exec(function(err, shop){
        if(err || !shop){
            return res.status(err ? 500 : 404).json({message: err ? err : "Shop not found"});
        }
        checkShopMollieId(shop, function(result, mollieId){

            if(!result || !mollieId){
                return res.status(500).json({message: 'Something went wrong'})
            }

            var upgradeOrder = {
                shopId: shop._id,
                status: [
                    {
                        event: 'modules-changed'
                    }
                ],
                modules : [{
                    name: package.name,
                    active: false,
                    amount: package.price,
                    value: package.code
                }]
            }
            var counter=0;

            if (package.choosen && Object.keys(package.choosen).length > 0){
                for(var key in package.choosen){
                    if(!package.choosen.hasOwnProperty(key)) continue;

                    var option = package.choosen[key];
                    if(typeof option == 'object'){
                        var module = {
                            name: getModuleName(option.code),
                            active: false,
                            amount: option.price,
                            value: option.code
                        }
                        upgradeOrder.modules.push(module);
                    }
                    

                    counter++
                    if(counter == Object.keys(package.choosen).length){
                        continueUpgrading(upgradeOrder);
                    }

                }
            }else{
                continueUpgrading(upgradeOrder);
            }
            function continueUpgrading(upgradeOrder){
                createorUpdateOrder(null, upgradeOrder, function(err, result){
                    if(err){
                        return res.status(500).json({message: err})
                    }
                    var redirectUrl = settings.mollie.url + "retailer/upgrade?return=mollie&orderid=" + result._id.toString();
    
                    mollieService.getMandates(mollieId, function(mandates){
                        if(mandates.totalCount == 0){
                            createPayment();
                        }else{
                            var valid = false, counter =0;
                            for(var i=0; i < mandates.length; i++){
                                if(mandates[i].status == 'valid'){
                                    valid = true;
                                }
                                counter++;
                                if(counter == mandates.length){
                                    if(!valid){
                                        createPayment();
                                    }else{
                                        return res.status(201).json({message: "Mandate already exists", order: result});
                                    }
                                }
                            }
                        }
                    })
    
                    function createPayment(){
                        //Time to create a first payment
                        mollieService.createPayment({
                            amount: 0.01,
                            customerId: mollieId,
                            recurringType: 'first',
                            description: 'Toestemming voor automatische incasso',
                            redirectUrl: redirectUrl,
                        }, function(err, payment){
                            if(err){
                                return res.status(500).json({message: err})
                            }else{
                                return res.status(200).json({message: "Ok", payment})
                            }
                        })
                    }
                })
            }
        })
    })
}

module.exports.finishUpgrade = function(req, res){
    console.log("finishUpgrade", req.body);
    var orderId = req.body.orderId;

    //check if the payment is succesfull by getting the users mandates
    //first, get the upgradeorder
    UpgradeOrder.findOne({_id: ObjectId(orderId)}).exec(function(err, order){
        if(err){
            return res.status(500).json({message: "Error getting order", err});
        }
        if(order.active){
            return res.status(200).json({message: "Order is already activated"})
        }
        Shop.findOne({_id: order.shopId}).exec(function(err, shop){
            if(err){
                return res.status(500).json({message: "Error getting shop", err});
            }
            console.log("shop in finishUpgrade", shop);
            mollieService.getMandates(shop.mollieId, function(mandates){
                console.log("MANDATES", mandates);
                if(!mandates){
                    return res.status(500).json({message: "No mandates found on user"});
                }else{
                    var valid = false, counter =0;
                    for(var i=0; i < mandates.length; i++){
                        if(mandates[i].status == 'valid'){
                            valid = true;
                        }
                        counter++;
                        if(counter == mandates.length){
                            if(!valid){
                                return res.status(500).json({message: "No valid mandates found on user"});
                            }
                            upgradeShopService.getUpgradeOrderTotal(orderId, function(err, total){
                                if(err){
                                    return res.status(500).json({message: "Error getting order", err})
                                }
                                webhookUrl = settings.mollie.url + "api/recurring-webhook";

                                //https://github.com/mollie/mollie-api-node/blob/master/examples/15-recurring-payment.js
                                mollieService.checkSubscriptions(shop.mollieId, function(subscriptions){
                                    async.forEach(subscriptions, function(subscription, subscriptionsDone){
                                        if(subscription.description == "PrismaNote modules" && subscription.status == "active"){
                                            mollieService.cancelSubscription(shop.mollieId, subscription.id, function(result){
                                                upgradeShopService.cancelUpgradeOrder(subscription.id, {event: 'subscription-cancelled-and-created-new'}, function(err, result){
                                                    subscriptionsDone();
                                                })
                                            })
                                        }else{
                                            subscriptionsDone();
                                        }
                                    }, function(){
                                        //subscriptionsDone
                                        console.log("subscriptionsDone");
                                        mollieService.createSubscription(shop.mollieId, {
                                            amount: total,
                                            times: null, //ongoing
                                            interval: '1 month',
                                            description: 'PrismaNote modules',
                                            webhookUrl: webhookUrl
                                        }, function(subscription){
                                            console.log("subscription status", subscription);
                                            if(subscription.status === "active"){
                                                upgradeShopService.createOrUpdateUpgradeOrder(order._id, {subscriptionId: subscription.id}, function(err, result){
                                                    if(err){
                                                        return res.status(500).json({message: "Something went wrong while updating the order", err})
                                                    }
                                                    //activate the modules
                                                    upgradeShopService.activateUpgradeOrder(order._id, function(err, result){
                                                        if(err){
                                                            return res.status(500).json({message: "Something went wrong while activating the modules", err})
                                                        }
                                                        return res.status(200).json({message: "OK", subscription, shop})
                                                    })
                                                })
                                            }else{
                                                return res.status(500).json({message: "Something went wrong while activating the subscription", subscription})
                                            }
                                        }) //end create subscription
                                    })// end foreach
                                })// end checksubscription
                            })// end getUpgradeOrderTotal 
                        }//if counter == mandates.length
                    }//end for loop mandates.length
                }// if !mandates
            }) // end getMandates
        })// end shop findone
    }) // end upgradeorder findone
}

module.exports.cancelSubscription = function(req, res){
    console.log("cancelSubscription", req.body.shopId);
    upgradeShopService.getShopUpgradeOrders(req.body.shopId, function(err, orders){
        if(err){
            return res.status(500).json({message: err})
        }

        async.forEach(orders, function(order, ordersDone){
            if(!order.status || !order.subscriptionId){
                return ordersDone();
            }
            Shop.findOne({_id: order.shopId}).exec(function(err, shop){
                mollieService.cancelSubscription(shop.mollieId, order.subscriptionId, function(result){
                    upgradeShopService.cancelUpgradeOrder(order.subscriptionId, {event: 'subscription-cancelled', comment: 'Cancelled on users request'}, function(err, result){
                        ordersDone();
                    })
                })
            })
        }, function(order){
            //ordersdone
            res.status(200).json({message: "ok"});
        })  
    })
}

module.exports.webhook = function(req, res){
    console.log("Webhook, recurring payments");
    //Mollie webhook for recurring payments. The body only contains a payment id, which we can use to get the full payment
    //Mollie also do not like to wait long, so we send an OK here.
    res.send("OK");

    var paymentId = req.body.id;
    mollieService.getPayment(paymentId, function(payment){
        if(!payment || !payment.subscriptionId){
            //it can happen that the payment has no subscriptionId, in that case we have to reget the payment
            mollieService.getPayment(paymentId, function(payment){
                console.log("nieuw opgehaald", payment);
                if(!payment || !payment.subscriptionId){
                    console.error("Failed to get subscriptionId 2 times", payment);
                    return;
                }else{
                    finish(payment)
                }
            })
        }else{
            finish(payment);
        }

        function finish(payment){
            upgradeShopService.getSubscriptionOrder(payment.subscriptionId, function(err, order){
                console.log("getSubscriptionOrder", err, order);
                var pay = {
                    status: payment.status,
                    method: payment.method,
                    description: payment.description,
                    paidDatetime: payment.paidDatetime
                };
                order.payments.push(pay);

                upgradeShopService.createOrUpdateUpgradeOrder(order._id, order, function(err, result){
                    Shop.findOne({_id: result.shopId}).exec(function(err, shop){                
                        //create invoice
                        if(payment.status == "paid"){
                            //generate and email invoice
                            orderService.createPayment(result, function(err, payment){
                                orderService.handlePayment(null, {generateInvoiceNumber:true}, function(payment2){
                                    payment.invoiceNumber = payment2.invoiceNumber;
                                    order.number = payment.invoiceNumber;
                                    var status = {
                                        event: 'invoice-generated',
                                        number: payment.invoiceNumber
                                    }
                                    order.status.push(status);

                                    upgradeShopService.createOrUpdateUpgradeOrder(order._id, order, function(err, order2){
                                        payment.save(function(err, result){
                                            orderService.createUpgradeInvoice(order._id, function(err, invoiceResult){
                                                if(err){
                                                    console.log("error creating invoice", err);
                                                    return;
                                                }
                
                                                var data = {
                                                    order: order,
                                                    shop: shop
                                                }
                
                                                var attachment = [{
                                                    filename: "Factuur " + order.number + ".pdf",
                                                    content: invoiceResult
                                                }];
                                                    
                                                var options = {
                                                    to: shop.email,
                                                    bcc: 'info@prismanote.com',
                                                    subject: 'Factuur ' + order.number + ' voor shop upgrade ',
                                                    template: 'upgrades/upgrade-invoice'
                                                }
                    
                                                mailService.mail(options, data, attachment, function(err, mailResult){
                                                    //mail verzonden, klaar!
                                                    
                                                })
                                            })
                                        })
                                    })
                                })
                            })


                        }else if(payment.status == "failed" || payment.status == "charged_back"){
                            //Cancel upgrade order and status update to the shop
                            var status = {
                                event: payment.status == "failed" ? 'payment-failed' : 'payment-charged-back',
                                number: payment.invoiceNumber
                            }
                            order.status.push(status);
                            upgradeShopService.createOrUpdateUpgradeOrder(order._id, order, function(err, order2){
                                upgradeShopService.cancelUpgradeOrder(payment.subscriptionId, function(result){
                                    var data = {
                                        payment: payment,
                                        shop: shop
                                    };
                                    var options = {
                                        to: shop.email,
                                        subject: 'Betaling mislukt',
                                        template: 'upgrades/upgrade-cancelled'
                                    };
                                    mailService.mail(options, data, null, function(err, mailResult){
                                    })
                                })
                            })
                        }
                    })
                })
            })
        }
    })    
}