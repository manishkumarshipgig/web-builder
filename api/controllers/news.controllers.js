const path = require('path');
const mongoose = require('mongoose');
const NewsItem = mongoose.model('NewsItem');
const Shop = mongoose.model('Shop');
const ObjectId = require('mongodb').ObjectId;

const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const update = require(path.join(__dirname, '..', 'services', 'update.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));
const stringService = require(path.join(__dirname, '..', 'services', 'string.service'));

module.exports.getNewsItem = function(req, res) {
	console.log('[getNewsItem controller]');

	if(req && req.params && req.params.nameSlug) {

		NewsItem.findOne({'nameSlug': req.params.nameSlug}).exec(function(err, newsItem) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid News item ID. ' + err};
			} else if (!newsItem) {
				status = 404;
				response = {message: 'News item not found. '};
			} else {
				status = 200;
				response = {message: 'OK', newsItem};
			}

			console.log(status + ' ' + response.message);
			res.status(status).json(response);

		});
	}
};

module.exports.getNewsItems = function(req, res) {
	console.log('[getNewsItems controller]');
	// default values, because offset and limit are required
	var offset = 0, limit = 0;
	var filterBy, sortBy;

	if(req.query) {

		if(typeof req.query === 'string') {
			req.query = JSON.parse(req.query);
		}

		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			offset = parseInt(req.query.offset);
			delete req.query.offset;
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			limit = parseInt(req.query.limit);
			delete req.query.limit;
		}

		// if any filters are provided in the URL, parse them and add to the filtering params object.
		if(req.query.filter) {
			filterBy = filter(req.query.filter);
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
		if(req.query.sort) {
			sortBy = sort(req.query.sort);
		}

		if(req.query.lng && req.query.lat) {
			// TODO: find news items close to the given coordinates. 
		}
	}
	var query = NewsItem.find(filterBy).sort(sortBy).skip(offset);

	if(limit){
		query.limit(limit);
	}

	query.exec(function(err, newsItems) {
		var status, response;

		if(err) {
			status = 500;
			response = {message: 'Internal server error. ' + err};
		} else if(!newsItems || newsItems.length == 0) {
			status = 404;
			response = {message: 'No news items found with the given search criteria. '};
		} else {
			status = 200;
			response = {message: 'OK', newsItems};
		}
		
		console.log(status + ' ' + response.message);
		res.status(status).json(response);

	});
};

module.exports.addNewsItem = function(req, res) {
	console.log('[addNewsItem controller]');
	if(req.body.newsItem) {
		var newsItem = req.body.newsItem;
		
		determineSlug(stringService.slugify(newsItem.name), 0, 'news', function(err, slug){
			if(err){
				return res.status(500).json({message: 'Error while determing a slug: ' + err});
			}else{
				newsItem.nameSlug = slug;
				newsItem.lastModified = new Date();
				newsItem.creationDate = new Date();
				newsItem.publicationDate = new Date();
				create(newsItem, function(newsItemNew) {
					if(newsItemNew) {
						NewsItem.create(newsItemNew, function (err, newsItem) {
							var status, response;
							if(err) {
								status = 400;
								response = {message: 'News-item could not be added. Error message: ' + err};
							} else {
								status = 201;
								response = {message: 'OK', newsItem};
							}
							
							console.log(status + ' ' + response.message);
							if(err) {
								console.log(err.stack);
							}
							res.status(status).json(response);
						});
					};
				})
			}
		})
	}else{
		return res.status(500).json({message: "No details provided"})
	}
};

module.exports.uploadNewsItemPhoto = function(req, res){
	console.log("uploadNewsItemPhoto");
	if(req.body.newsItemId && req.files.file){
		upload.uploadFile(req.files.file, 'news', null, function(err, result){
			if (err) {
				return res.status(500).json({message: 'error uploading file: ' + err})
			}

			NewsItem.findOne({_id: req.body.newsItemId}).exec(function(err, newsItem){
				if(newsItem){
					var photo = {
						src: result,
						alt: newsItem.name
					}
					newsItem.images = [];
					newsItem.images.push(photo);

					newsItem.save(function(err, result){
						if(!err){
							return res.status(200).json({file: result});
						}else{
							console.log("Error during updating newsItem: " + err);
							return res.status(200).json({message: "Error during updating newsItem: " + err});
						}
					})
				}else{
					console.log("newsItem not found");
					return res.status(404).json({message: "newsItem not found"});
				}
			})
		})
	}else{
		console.log("no newsItem id found");
		return res.status(404).json({message: "no newsItem id found"});
	}
}




module.exports.uploadNewsContentImage = function(req, res){
	console.log("uploadNewsContentImage", req.body);

	upload.uploadFile(req.files.file, 'uploads', null, function(err, result){
		if (err) {
			return res.status(500).json({message: 'error uploading file: ' + err})
		}

		return res.status(200).json({file: result});

	})
}

module.exports.updateNewsItem = function(req, res) {
	if(req.body.newsItem) {
		update(req.body, function(updatedNewsItem) {
			console.log(updatedNewsItem);
			if(updatedNewsItem) {

				NewsItem.findByIdAndUpdate(req.body.newsItem._id, {$set: req.body.newsItem}, {new: true}, function (err, newsItem) {
					var status, response;

					if(err) {
						status = 400;
						response = {message: 'News item could not be added. Error message: ' + err};
					} else if(!newsItem) {
						status = 404;
						response = {message: 'News item not found. '};
					} else {
						status = 200;
						response = {message: 'News item updated. ', newsItem};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);

				});

			}
		});

	}
};

module.exports.uploadNewsItemShopPhoto = function(req, res){
	console.log("uploadNewsItemShopPhoto");
	if(req.files.file && req.body.newsItemId && req.body.shopId){
		upload.uploadFile(req.files.file, 'news', null, function(err, result){
			if (err) {
				return res.status(500).json({message: 'error uploading file: ' + err})
			}
			var images = [{
					src: result
				}]
			

			Shop.findOneAndUpdate({
				_id: ObjectId(req.body.shopId), 
				'news._id':  ObjectId(req.body.newsItemId)},
				{"$set": {
					"news.$.images" : images 
				}
			}).exec(function(err, shop){
				console.log(err, shop);
				return res.status(200).json({message: 'OK'});
			})
			
		})
	}else{
		console.log("Not enough details provided");
		return res.status(500).json({message: "Not enough details provided"});
	}
}

module.exports.updateNewsItemShop = function(req, res) {
	console.log(req.body.data.newsItem._id)

	if(req.body.data.newsItem) {

		update(req.body.data.newsItem, function(updateNewsItemShop) {
			console.log(updateNewsItemShop);
			if(updateNewsItemShop) {
				Shop.findOneAndUpdate({
					_id: ObjectId(req.body.data.shopId), 
					'news._id':  ObjectId(req.body.data.newsItem._id)},
					{"$set": {
						"news.$": req.body.data.newsItem
					}
				}).exec(function(err, shop){
					if(err) {
						status = 500;
						response = {message: 'News item could not be updated. Error message: ' + err};
					}else if(!shop){
						status = 500;
						response = {message: 'News item could not be updated. Error message: ' + err};
					}else{
						status = 200;
						response = {message: 'News item updated. ', shop};
					}
					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);
				})
			}
		});

	}
};

module.exports.removeNewsItem = function(req, res) {
	console.log('[removeNewsItem controller]');
	console.log(req.params.nameSlug);

	NewsItem.remove({nameSlug: req.params.nameSlug}, function (err) {
		var status, response;

		if(err) {
			status = 500;
			response = {message: 'Error while deleting newsitem. ' + err};
		} else if(!err) {
			status = 200;
			response = {message: 'News item deleted. '};
		} 

		console.log(status + ' ' + response.message);
		if(err) {
			console.log(err.stack);
		}
		res.status(status);

	});
};

module.exports.createShopNewsItemSlug = function(req, res){
	//creates a nameSlug from a given string and checks if it already used
	console.log("createNameSlug");
	var name = req.query.name;

	determineSlug(stringService.slugify(name), 0, 'shop', function(err, slug){
		if(err){
			return res.status(500).json({message: 'Error while determing a slug: ' + err});
		}else{
			return res.status(200).json({message: 'OK', nameSlug: slug});
		}
	})
}

function determineSlug(name, count, type, callback){
	var nameSlug = name;
	if(count > 0){
		nameSlug = name + '-' + count;
	}
	if(type == 'shop'){
		Shop.find({'news.nameSlug': nameSlug}).exec(function(err, shop){
			if(err){
				return callback(err);
			}
			if(shop.length == 0){
				return callback(null, nameSlug);
			}else{
				count = count+1;
				return determineSlug(name, count, 'shop', callback);
			}
		})
	}else{
		NewsItem.find({nameSlug: nameSlug}).exec(function(err, news){
			if(err){
				return callback(err);
			}
			if(news.length == 0){
				return callback(null, nameSlug)
			}else{
				count = count+1;
				return determineSlug(name, count, 'news', callback);
			}
		})
	}
	
}