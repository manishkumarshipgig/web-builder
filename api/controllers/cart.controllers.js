const path = require('path');
const async = require('async');

const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));

module.exports.saveCart = function(req, res) {
	console.log('[saveCart controller]');

	var status, response;

	if(req.body.cart) {
		req.session.cart = req.body.cart;
		status = 200;
		response = {message: 'OK', cart: req.session.cart};
	} else {
		status = 400;
		response = {message: 'Request does not contain cart contents.'};
	}
	
	console.log(status + ' ' + response.message);
	res.status(status).json(response);

};

module.exports.getCart = function(req, res) {
	console.log('[getCart controller]');

	var status, response;

	if(!req.session.cart) {
		req.session.cart = [];
	}

	if(req.query.force == "true"){
		//force update the cart (get product prices)
		async.forEachOf(req.session.cart, function(item, key, callback){
			shopService.getItemPrice(item.shop.nameSlug, item._id, function(err, price){
				if(err) {
					console.log("err!", err);
				}
				req.session.cart[key].price = price;
				callback();
			});
			
			
		}, function(){
			return res.status(200).json({message: "OK", cart: req.session.cart});
		})
	}else{
		status = 200;
		response = {message: 'OK', cart: req.session.cart};
		
		console.log(status + ' ' + response.message);
		res.status(status).json(response);
	}
};

module.exports.emptyCart = function(req,res){
	console.log("[emptyCart Controller]");

	req.session.cart = [];

	return res.status(200).json({message: "OK", cart: req.session.cart});
}