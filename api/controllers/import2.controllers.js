const path = require('path');
const async = require('async');
const Url = require('url');
const csvtojson = require('csvtojson')
const _ = require('lodash');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const User = mongoose.model('User');
const ProductCollection = mongoose.model('Collection');
const Shop = mongoose.model('Shop');
const collectionService = require(path.join(__dirname, '..', 'services', 'collection.service'));

const str = require(path.join(__dirname, '..', 'services', 'string.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));


//Step 3: Parse the file and link the columns to the properties with the given array
module.exports.fileImport2 = function(req, res){
	console.log("productImport");
// console.log(`USER OBJECT IN REQUEST`);
// console.log(req);
//send an result to the browser to avoid timeouts
res.status(200).json({message: "Import started"});

//Save the data
var headers = req.body.headers;
var category = req.body.cat;
var delimiter = req.body.delimiter;
var customData = req.body.customData;
var product = req.body.product;
var totalProducts = req.body.lines;
var currentShopId = req.body.currentShopId ? req.body.currentShopId : undefined;
console.log("SHOP ID WHILE IMPORTING = ",currentShopId);
console.log("Session Object ",req.session);
// var currentShop; // Used while INIT PARAMS.
var currentShop = req.body.currentShop ? req.body.currentShop : undefined;
var results = [];
var productCounter = 0;
var productsSaved = 0;
var errProdNum = 0;
var productJson = [];
var shopProducts = [];

csvtojson({ delimiter: delimiter })
.fromFile(req.files.file.path)
.on('json', (jsonObj) => {
	productJson.push(jsonObj);
})
.on('error', (err)=>{
	console.error("Error during file read:", err);
})
.on('done', (error)=>{
	console.log("csv done");
	jsonifyProductKey(product, (err, productKey) => {
		if(err){
			console.error(err);
			results.push({line: 'ProductKeyMap', msg: err});
			sendResults();
			return;
		}
		console.log(`Product Key : `);
		console.log(productKey);
		console.log("---------PRODUCT-------- : ",product);

		// NOTE: productJson contains csvtojson Product Array
		var myCounter = 0;
		async.forEachOfSeries(productJson, (prod, key, cb) => {

			console.log("COUNT ====================================================>  "+ myCounter++);
			var newProduct = new Product();
			// NOTE: WATERFALL FOR A EVERY PRODUCT IN THE LOOP
			async.waterfall([
				initParamsWCategory,
				assignBrand,
				assignType,
				assignProdGender,
				assignLanguageParameters,
				assignCollection,
				assignVariants,
				assignImages,
				assignRetailPrice,
				assignRemainingKeys
				], (err, finalNewProduct) => {
					console.log("Waterfall Iteration Ended. !");
					if(err || !finalNewProduct){
						err ? console.error(`WATERFALL Error - ${err}`) : null;
					// results.push({ CSV_line: productsSaved+1, msg: err});
					cb(null);
					return;
				}
				if(finalNewProduct){
					console.log("----------------------------Final Project-------", finalNewProduct);
					const productStock = finalNewProduct.stock;
					//-const productPrice = finalNewProduct.price.indexOf(',') ? parseFloat(finalNewProduct.price.replace(',','.')) : finalNewProduct.price;
					const productPrice = finalNewProduct.price;
					console.log("---------Product Price And Stock ", productStock, productPrice);
					// console.log('ASYNC WATERFALL FINAL CALLBACK ');
					// console.log(finalNewProduct);
					finalNewProduct.uploader = {
						_id: req.user._id,
						name:  req.user.firstName + (req.user.lastNamePrefix ? " " + req.user.lastNamePrefix + " " : " ") + req.user.lastName
					}
					// Fill default name
					if(!finalNewProduct.en.name && !finalNewProduct.nl.name && !finalNewProduct.fr.name && !finalNewProduct.es.name && !finalNewProduct.de.name){
						finalNewProduct.en.name = `${finalNewProduct.brand.name} ${finalNewProduct.variants[0].productNumber}`;
					}else{
						if(finalNewProduct.nl && finalNewProduct.nl.name){
							finalNewProduct.en.name = finalNewProduct.nl.name;
						}else if(finalNewProduct.fr && finalNewProduct.fr.name){
							finalNewProduct.en.name = finalNewProduct.fr.name;
						}else if(finalNewProduct.es && finalNewProduct.es.name){
							finalNewProduct.en.name = finalNewProduct.es.name;
						}else if(finalNewProduct.de && finalNewProduct.de.name){
							finalNewProduct.en.name = finalNewProduct.de.name;
						}
					}

					if(!finalNewProduct.brand.name || !finalNewProduct.variants[0].productNumber){
						console.log("No Brand or Product Number!");
						// console.log(`New Product - Product Number: ${finalNewProduct.variants[0].productNumber}`);
						results.push({ productNumber: prod[productKey.variants.productNumber.name], msg: "No brand or productnumber!"});
						cb(null);
						return;
					}
					else{
						console.log('FINAL PRODUCT - PRODUCT NUMBER ');
						var productNum = finalNewProduct.variants[0].productNumber;
						var productEAN = finalNewProduct.variants[0].ean ? finalNewProduct.variants[0].ean : undefined;
						Product.findOne({ variants: {

							productNumber : productNum,
							ean: productEAN

						}
					}, (err, foundProduct) => {
						if(err){
							results.push({productNumber: prod[productKey.variants.productNumber.name], msg: `Error while finding in Product DB: ${err}`});
							cb(null);
							return;
						}
						if(foundProduct){
							console.log('PRODUCT FOUND So skipping to create new product.');

							foundProduct.versions.push({
								author : finalNewProduct.uploader,
								date: finalNewProduct.uploadDate,
								concept: true,
								modifiedProperties: finalNewProduct,
							});
							foundProduct.verifyLater = true;
							console.log("Product For Save", foundProduct);
							foundProduct.save((err, savedFoundProd)=>{
								if(err){

									results.push({ productNumber: finalNewProduct.variants[0].productNumber, msg: `Error while saving product. ${err}`});
									cb(null);
									return;
								}
								console.log("Product Stock",productStock,"Product Price", productPrice);
								savedFoundProd.verifyLater = true;
								savedFoundProd.stock = productStock;
								savedFoundProd.price = productPrice;
								results.push({productNumber: prod[productKey.variants.productNumber.name], msg: `This product was imported but already existed in the PrismaNote Database. Therefore we added the existing verified product to your assortment. One of our employees will look at the imported existing product information within 3 working days to verify the proposed and imported changes.`});
								shopProducts.push(savedFoundProd);
								productsSaved++;
								cb(null);
								return;
							});
						}
						if(!foundProduct){
							finalNewProduct.isVerified = true;
							finalNewProduct.verifyLater = true;
							determineProductSlug(finalNewProduct.en.name, 0, (err, slug) => {
								if(err){
									results.push({productNumber: prod[productKey.variants.productNumber.name], msg: `Error while generating nameslug: ${err}`});
									cb(null);
									return;
								}
								finalNewProduct.en.nameSlug = slug;
								finalNewProduct.save((err,savedProd) => {
									if(err){
										console.error({ msg: `Error while saving product.`, err });
										results.push({ productNumber: finalNewProduct.variants[0].productNumber, msg: `Error while saving product. ${err}`});
										cb(null);
										return;
									}
									shopProducts.push(savedProd);
									productsSaved++;
									console.log(`---- NEW PRODUCT SAVED ----`);

									cb(null, savedProd);
									return;

								});
							});
						}
					});
					}
				}
			});

function initParamsWCategory(wfCB){
	console.log("InitParamsWCategory()")

	newProduct.en = {}, newProduct.nl = {}, newProduct.fr = {}, newProduct.de = {}, newProduct.es = {};
	newProduct.watch = {}, newProduct.strap, newProduct.jewel = {};

	if(productKey.category){
		newProduct.category = category;
	}
	if(currentShopId){
		Shop.findOne({_id: currentShopId},(shopFindErr, shopDoc)=>{
			if(shopFindErr){
				currentShop = undefined;
				results.push({msg: 'Shop not found!'});
				wfCB("SHOP UNDEFINED", newProduct);
				return;
			}
			if(!shopDoc){
				currentShop = undefined;
				wfCB("SHOP UNDEFINED", newProduct);

				return;
			}
			currentShop = shopDoc;
			wfCB(null, newProduct);

		});
	}
	else{
		console.log("CURRENT SHOP ID not present. So moving ahead.")
		wfCB(null, newProduct);

	}
};

function assignBrand(newProduct, wfCB){
	console.log(`-- ASSIGNING NEW PRODUCT'S BRAND --`);
	var brandName = getValueForKey(prod, productKey.brand, 'brandname', customData, (resultBrand) => {
		if(!resultBrand || resultBrand == '' || resultBrand == null){
			var log = "Brand not found: " + brandName;
			results.push({productNumber: prod[productKey.variants.productNumber.name], msg: log});
			console.error(log);
			wfCB(true);
		} 
		else{
			newProduct.brand = {
				_id: resultBrand._id,
				name: resultBrand.name,
				nameSlug: resultBrand.nameSlug,
			}
			wfCB(null, newProduct);
		}
	});
};

function assignType(newProduct, wfCB){
	console.log(`-- ASSIGNING NEW PRODUCT'S TYPE --`);
	if(productKey.watch || productKey.jewel || productKey.strap || productKey.other){
		var prodType;
		if(productKey.watch){
			prodType = "watch";
		}
		if(productKey.jewel){
			prodType = "jewel";
		}
		if(productKey.strap){
			prodType = "strap";
		}
		if(productKey.other){
			prodType = "other";
		}

		var prodVal = productKey[prodType];

		async.forEachOfSeries(prodVal,(val, key, pvCB) => {
			var result;
			var keyLen = prodVal[key] ? Object.keys(prodVal[key]).length : 0;

			if(keyLen > 1){
				for(var keyVal in val){


					let result;
					if(keyVal === 'color' || keyVal === 'designItem'){

						result = getValueForKey(prod, prodVal[key][keyVal], `${keyVal}`, customData);
					} else if( keyVal === 'material'){
						result = getValueForKey(prod, prodVal[key][keyVal], `${key}${keyVal}`, customData);
					} else{
						result = getValueForKey(prod, prodVal[key][keyVal], `${keyVal}`, customData);
					}
					if(result){
						if(newProduct[prodType]){
							if(newProduct[prodType][key]){
								if(keyVal == 'weight' || keyVal == 'diameter' || keyVal == 'width' || keyVal == 'height' || key == 'depth'){
									Object.assign(newProduct[prodType][key],{
										[keyVal] : result.indexOf(',') ? parseFloat(result.replace(',','.')) : result,
									});
								} else{
									Object.assign(newProduct[prodType][key],{
										[keyVal] : result,
									});
								}
							}else{
								if(keyVal == 'weight' || keyVal == 'diameter' || keyVal == 'width' || keyVal == 'height' || key == 'depth'){
									Object.assign(newProduct[prodType], {
										[key] : {
											[keyVal] : result.indexOf(',') ? parseFloat(result.replace(',','.')) : result,
										},
									});
								} else{
									Object.assign(newProduct[prodType], {
										[key] : {
											[keyVal] : result,
										},
									});
								}
							}
						}
						else{
							if(keyVal == 'weight' || keyVal == 'diameter' || keyVal == 'width' || keyVal == 'height' || key == 'depth'){
								Object.assign(newProduct, {
									[prodType] : {
										[key] : {
											[keyVal] : result.indexOf(',') ? parseFloat(result.replace(',','.')) : result,
										},
									}
								});
							} else{
								Object.assign(newProduct, {
									[prodType] : {
										[key] : {
											[keyVal] : result,
										},
									}
								});
							}
						}
					}
				}
				pvCB();
				return;
			} else{
				getValue((result) => {
					if(newProduct[prodType]){
						if(key == 'weight' || key == 'diameter' || key == 'width' || key == 'height' || key == 'depth'){
							Object.assign(newProduct[prodType], {
								[key] : result.indexOf(',') ? parseFloat(result.replace(',','.')) : result,
							});
						} else{
							Object.assign(newProduct[prodType], {
								[key] : result,
							});
						}
					}
					else{
						if(key == 'weight' || key == 'diameter' || key == 'width' || key == 'height' || key == 'depth'){
							Object.assign(newProduct, {
								[prodType]:{
									[key] : result.indexOf(',') ? parseFloat(result.replace(',','.')) : result,
								}
							});
						} else{
							Object.assign(newProduct, {
								[prodType]:{
									[key] : result,
								}
							});
						}
					}
					pvCB();
				});
			}

			function getValue(callback){
				if(key == 'material'){
					return callback(getValueForKey(prod, prodVal[key], `${key}${val}`, customData));
				}else if(key == 'type' && prodType == 'watch'){
					return callback(getValueForKey(prod, prodVal[key], `${prodType}${key}`, customData));
				}else{
					return callback(getValueForKey(prod, prodVal[key], `${val}`, customData));
				}
			};
		},(err) => {
			if(err){
				console.log(err);
				wfCB(true);
				return;
			}
			wfCB(null, newProduct);
		});
		return;
	}
	wfCB(null, newProduct);
};

function assignProdGender(newProduct, wfCB){
	console.log(`-- ASSIGNING NEW PRODUCT'S GENDER --`);
	if(productKey.gender){
		getValueForKey(prod, productKey.gender, 'gender', customData, (gender) => {
			newProduct.male = gender.male;
			newProduct.female = gender.female;
			newProduct.kids = gender.kids;
		});
	}
	wfCB(null, newProduct);
};

function assignLanguageParameters(newProduct, wfCB){
	console.log(`-- ASSIGNING NEW PRODUCT'S LANGUAGE PARAMS --`);
	var lang = [];
	if(productKey.nl || productKey.en || productKey.fr || productKey.es || productKey.de){
		if(productKey.nl){
			lang.push('nl');
		}
		if(productKey.en){
			lang.push('en');
		}
		if(productKey.fr){
			lang.push('fr');
		}
		if(productKey.es){
			lang.push('es');
		}
		if(productKey.de){
			lang.push('de');
		}
		console.log(lang);
		async.forEach(lang, (language, feCB)=>{
			console.log(language);
			var productKeyVal = productKey[language];
			for(var prop in productKeyVal){
				var val = getValueForKey(prod, productKeyVal[prop], prop, customData);
				newProduct[language][prop] = val;
			}
			feCB();
		},(err)=>{
			wfCB(null, newProduct, lang);
		});
		return;
	}
	wfCB(null, newProduct, lang);
};

function assignCollection(newProduct, language, wfCB){
	console.log('-- ASSIGNING COLLECTIONS --');
	console.log("Collection Language : ",language);

	// if collection language is not specified, then language is assigned to be EN.
	var colLang = (language.length > 1) ? language[0] : 'en';


	console.log("Entering if productKey.productCollection is present.");
	if(productKey.productCollection.en || productKey.productCollection.de || productKey.productCollection.es || productKey.productCollection.nl || productKey.productCollection.fr){
		console.log("PRODUCTKEY.PRODUCTCOLLECTION FOUND!! ",productKey.productCollection);
		console.log("CollLang = ",colLang);
		console.log("productKey.productCollection = ",productKey.productCollection);
		
		console.log("Getting value for ",productKey.productCollection[colLang]);
		var collectionName = getValueForKey(prod, productKey.productCollection[colLang], 'productCollection', customData);
		console.log("COLLECTION NAME OBTAINED : ",collectionName);
		
		if(collectionName !== '' && collectionName !== null && collectionName !== undefined){
			console.log("Collection Name found and is not null.. Entered the IF Statement");



			var collectionArray = collectionName.indexOf(',') != -1 ? collectionName.split(',') : [ collectionName ];
			console.log("Collection array obtained from client side.")
			console.log(collectionArray);

			async.forEachOfSeries(collectionArray, (collItem, key, collCB) => {
				console.log("Iteration on collection array : CollItem = ",collItem)

				if(collItem !== '' || collItem !== null ){
					collItem = collItem.trim();
					console.log("Trimmed collItem = ",collItem);
					var collectionWLang = {};
					var collectionLanguages = (language.length > 0)? language : 'en';
					console.log("collectionLanguages for async Loop = ",collectionLanguages);
					var counterX = 0;
					async.forEach(collectionLanguages, (langOpt, langCB) => {

						var colLangField = `${langOpt}.name`;
						console.log(++counterX," ColLangField = ",colLangField);

						ProductCollection.findOne({ [colLangField] : collItem },(err, collection) =>{
							if(err){
								console.log("No Such Collection found for language : ",langOpt)
								var log = `Error during finding product collection. So Creating new ones : '${collItem}' - ${err}`;
								results.push({productNumber: prod[productKey.variants.productNumber.name], msg: log});
								langCB();
								console.error(log,err);
								return;
							}
							if(collection == null){
								console.log("NO SUCH COLLECTION FOUND for language ",langOpt);
							}
							if(collection){
								console.log("YES COLLECTION FOUND for language "+langOpt+" : ",collection)
								collectionWLang = {
									lang: langOpt,
									_id: collection._id,
									name: collection[langOpt].name,
									nameSlug: collection[langOpt].nameSlug,
									description : collection[langOpt].description ? collection[langOpt].description : undefined,
								};

								return langCB();
							}
							return langCB();
						});
					},(err) => {
						console.log("Collection Language CallBack");

						console.log(err);
						console.log('CHECK by LODASH for COLLECTIONWLANG');
						console.log(collectionWLang);
						console.log(_.isEmpty(collectionWLang));
						if(_.isEmpty(collectionWLang)){
							console.log("Creating New Collection for collItem = ",collItem);
							var colTmp = getValueForKey(prod, productKey.productCollection.nl, 'productCollection', customData);
							console.log("colTmp for Language = nl = ",colTmp);

							var colObj = {
								uploader : {
									_id: req.user._id,
									name:  req.user.firstName + (req.user.lastNamePrefix ? " " + req.user.lastNamePrefix + " " : " ") + req.user.lastName,
								},
								en: {}, 
								nl: {},
								es: {}, 
								de:{},
								fr : {}


							};
							for(var i = 0; i < language.length; i++){
								colObj[language[i]]["name"] = getValueForKey(prod, productKey.productCollection[language[i]], 'productCollection', customData); 
								if(colObj[language[i]]["name"] != undefined) colObj[language[i]]["nameSlug"] = colObj[language[i]]["name"].toLowerCase();
							}
							console.log("Making Collection with object = ",colObj);
							var newProdColl = new ProductCollection(colObj);
							console.log("Saving");
							newProdColl.save((collSaveErr, savedProdColl)=>{
								if(collSaveErr){
									var errLog = `Error during saving a new product collection: '${collItem}' - ${collSaveErr}`;
									results.push({productNumber : prod[productKey.variants.productNumber.name], msg: errLog});
									collCB();
									return;
								}

								console.log("SavedProd = ",savedProdColl);
								console.log("'de.name' in savedProdColl = ",('de.name' in savedProdColl));
								console.log("'en.name' in savedProdColl = ",('en.name' in savedProdColl));

								var colObjectToBePushed = {
									_id: savedProdColl._id,
									en : {
										name: ('en' in savedProdColl && 'name' in savedProdColl.en) ? savedProdColl['en'].name : undefined,
										nameSlug: savedProdColl['en'].nameSlug ? savedProdColl['en'].nameSlug : undefined,
										description : savedProdColl['en'].description ? savedProdColl['en'].description : undefined,
									},
									de : {
										name: ('de' in savedProdColl && 'name' in savedProdColl.de) ? savedProdColl['de'].name : undefined,
										nameSlug: savedProdColl['de'].nameSlug ? savedProdColl['de'].nameSlug : undefined,
										description : savedProdColl['de'].description ? savedProdColl['de'].description : undefined,
									},
									nl : {
										name: ('nl' in savedProdColl && 'name' in savedProdColl.nl) ? savedProdColl['nl'].name : undefined,
										nameSlug: savedProdColl['nl'].nameSlug ? savedProdColl['nl'].nameSlug : undefined,
										description : savedProdColl['nl'].description ? savedProdColl['nl'].description : undefined,
									},
									es : {
										name: ('es' in savedProdColl && 'name' in savedProdColl.es) ? savedProdColl['es'].name : undefined,
										nameSlug: savedProdColl['es'].nameSlug ? savedProdColl['es'].nameSlug : undefined,
										description : savedProdColl['es'].description ? savedProdColl['es'].description : undefined,
									},
									fr : {
										name: ('fr' in savedProdColl && 'name' in savedProdColl.fr) ? savedProdColl['fr'].name : undefined,
										nameSlug: savedProdColl['fr'].nameSlug ? savedProdColl['fr'].nameSlug : undefined,
										description : savedProdColl['fr'].description ? savedProdColl['fr'].description : undefined,
									}
								}
								newProduct.collections.push({
									_id: colObjectToBePushed._id
								});
								console.log("Now checking current shop. ");
								if(currentShop){
									console.log("Current Shop Found");
									checkShopCollections(savedProdColl, 'new');
								}else{
									console.log("Current Shop not found");
								}
								collCB();
							});
							return;
						}
						if(!_.isEmpty(collectionWLang)){
							newProduct.collections.push({
								_id: collectionWLang._id
							});

							
							if(currentShop){
								checkShopCollections(collectionWLang, 'exists');
							}

							collCB();
							return;
						}

					});
return;
}
else{
	console.log("ColItem is null or Undefined");
}
collCB();
},() => {
	return wfCB(null, newProduct);
});
}



return;
}
else{
	console.log("NO PRODUCTKEY.productCollection NOT FOUND!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	console.log("PRODUCT KEY OBJECT : ",productKey);
}
return wfCB(null, newProduct);
};

function assignVariants(newProduct, wfCB){
	console.log(`-- ASSIGNING NEW PRODUCT'S VARIANTS --`);
	if(productKey.variants){
		var value = productKey.variants;
//NOTE: Fill variant 0 with the know details: EAN, productnumber, sku and size
if(typeof value == 'object'){
	var counter = 0;
	var variant = {};

	for(var prop in value){
counter++; // NOTE: Counter for matching KEYS' Length.
var val = getValueForKey(prod, value[prop], prop, customData);
variant[prop] = val ? val : null;

if(counter == Object.keys(value).length){
	newProduct.variants.push(variant);
}
}
wfCB(null, newProduct);
return;
}
}
wfCB(null, newProduct);
};

function assignImages(newProduct, wfCB){
	console.log(`-- ASSIGNING NEW PRODUCT IMAGES --`);
	if(productKey.image1 || productKey.image2 || productKey.image3){
		console.log('Image Found');
		var productImageArr = [];

		if(productKey.image1 && productKey.image1 !== null && productKey.image1 !== ''){
			productImageArr.push({ img: productKey.image1.name, key: 'image-1' });
		}
		if(productKey.image2 && productKey.image2 !== null && productKey.image2 !== ''){
			productImageArr.push({ img: productKey.image2.name, key: 'image-2' });
		}
		if(productKey.image3 && productKey.image3 !== null && productKey.image3 !== ''){
			productImageArr.push({ img: productKey.image3.name, key: 'image-3'});
		}
// console.log("ARRAY = ",productImageArr);
if(productImageArr.length > 0){
	var imgErr = {
		msg : '',
		image : '',
		prod : '',
	};
	console.log("Images Array: "+productImageArr.length);

	async.forEachOfSeries(productImageArr,(prodImg, index, iEOScb)=>{
		console.log("Into forEachOfSeries for uploading images.")
		var fieldName = prodImg.img;
		var key = prodImg.key;
		var imageUrl = _.get(prod, fieldName);
		console.log("Image Url",imageUrl);
		if(imageUrl !== null && imageUrl !== '' && imageUrl !== undefined){
			if(imageUrl.indexOf("http") !== -1 || imageUrl.indexOf("https") !== -1){

// console.log("Dealing with Images.")

if(imageUrl.indexOf("prismanote.com") !== -1){
	var url = Url.parse(imageUrl);
	console.log("Image Url", url ,url.pathname);
	imageUrl = "https://52.17.102.38" + url.pathname;
// console.log("ImageURL Automatically Added.")
}
// console.log("Generating Image Slug");
// console.log("With the variables  > ",prod, productKey, key);
genImageSlug2(prod, productKey, key, (imageSlug) => {
// console.log("Generated");

var options = {
	fileName: imageSlug,
};
// console.log("Starting to upload.")
// NOTE: testindia4 is being used instead of products for testing purpose.
upload.uploadStream(imageUrl, 'product', options, function(err, data){
	if(err){
// console.log("ERROR WHILE UPLOADING IMAGE> ",err);
if(err.error && err.error.message){
	var error = err.error.message;
}else{
	var error = err.statusMessage;
}
console.error("Error while download or upload photo: " + error);

var prodNum = prod[productKey.variants.productNumber.name];
// console.log(`Image key = ${key} Err in Product = ${prodNum}`);
imgErr.msg = error;
imgErr.prod = prodNum;
imgErr.image = fieldName;
iEOScb(imgErr);
return;
}else{
	newProduct.images.push({src:data});
	iEOScb();
	return;
}
})
})
}
else{
	console.log("Image Doesn't start with Http or Https ");
	if(key == 'image-1'){
		imgErr.msg =  'Not a valid URL!';
		imgErr.image = fieldName;
		imgErr.prod = prod[productKey.variants.productNumber.name];
		iEOScb(imgErr);
	} else{
		iEOScb();
	}
}
}
else{
	if(key == 'image-1'){
		imgErr.msg = 'Image field empty!';
		imgErr.image = fieldName;
		imgErr.prod = prod[productKey.variants.productNumber.name];
		iEOScb(imgErr);
	}else{
		iEOScb();
	}
}
},(err)=>{
	if(err){
		results.push({ msg: "Error while download or upload photo:" + err.msg, ImageKey: err.image, productNumber : err.prod});
// console.log(err);
wfCB(true);
return;
}
wfCB(null, newProduct);
});
}
else{
	wfCB(null, newProduct);
}
}
else {
	console.log('Image Not Found');
	wfCB(null, newProduct);
}
};

function assignRetailPrice(newProduct, wfCB){
	console.log('-- ASSINGING RETAIL PRICE --');
	var price = getValueForKey(prod, productKey.suggestedRetailPrice, 'suggestedRetailPrice', customData);
	console.log("------------Price ",price);
	if(price){
		price = price.trim();
		console.log("-----------------------------------------RETAIL PRICE FOUND = ",price)
		while(isNaN(price[0])){
			price = price.substr(1);
		}

		newProduct.suggestedRetailPrice = parseFloat(price.replace(',','.'));
	}
	wfCB(null, newProduct);
};

function assignRemainingKeys(newProduct, wfCB){
	console.log(`------********** ASSIGNING REMAINING KEYS ***********------`);

	async.forEachOfSeries(productKey, (item, key, fesCB) => {
		console.log("Key", key);
		if(key != 'category' &&
			key != 'brand' &&
			key != 'productNumber' &&
			key != 'variants' &&
			key != 'image1' &&
			key != 'image2' &&
			key != 'image3' &&
			key != 'gender' &&
			key != 'watch' && key != 'strap' && key != 'jewel' && key != 'other' &&
			key != 'nl' && key != 'en' && key != 'fr' && key != 'es' && key != 'de' &&
			key != 'suggestedRetailPrice' && key != 'productCollection'
			){
			newProduct[key] = getValueForKey(prod, productKey[key], key, customData);
		console.log("newProductKey", newProduct[key]);
	}
	else if(key == 'watch'){
		console.log("****** Watch********", key);
		console.log("Product Key", productKey[key]);
		console.log("product prod", prod);
		console.log("custom Data", customData);

	}
	fesCB();
},()=>{
	wfCB(null, newProduct);
});
};
}, () => {
// console.log(`FOR EACH OF SERIES CALLBACK`);
if(currentShop){
	addProductsToShop(currentShop, shopProducts, () => {
		console.log("Shop Updated!!! Sending Results.!");
		Shop.findOne({
			_id: currentShop._id
		})
		.populate("products._id")
		.lean()
		.exec(function(err,shop){
			if(err) return console.log("Couldn't Find the shop");

			collectionService.updateShopWithProperCollections(shop)
			.then((finalShop)=>{
				sendResults();
			},(err)=> {
				console.log("Error Updating the shop for collections.")
			})
			
		})
		
	});
}
else{
	sendResults();
}
});

function sendResults(){
	console.log("sendResults");
	console.log("all products done");
// All products are done
sendMail(results, productsSaved, req.user._id,req.user.email);
}

});
});

function checkShopCollections(collection, collectionType){
	if(collectionType === 'new'){
		if(currentShop.collections && currentShop.collections.length > 0){
			currentShop.collections.push({
				_id: collection._id,
				countr: 0
			});
		} else {
			currentShop.collections = [{
				_id: collection._id,
				countr: 0
			}];
		}
		return;
	}
	if(collectionType === 'exists'){
		if(currentShop.collections && currentShop.collections.length > 0){
			if(!_.some(currentShop.collections, ['_id', collection._id])){
				currentShop.collections.push({
					_id: collection._id,
					[collection.lang]:{
						name: collection.name,
						nameSlug: collection.nameSlug,
						longDescription: collection.description,
					}
				});
			}
		} else {
			currentShop.collections = [{
				_id: collection._id,
				[collection.lang]:{
					name: collection.name,
					nameSlug: collection.nameSlug,
					longDescription: collection.description,
				}
			}];
// currentShop.collections.push(collection);
}
return;
}
}
}


function addProductsToShop(currentShop, shopProdsArr, callback){
	console.log("----------------addProductsToShop()----------------");
	// console.log("=========================================>>>>>>>>>>>");
	// console.log("Current Shop", currentShop);
	// console.log("===========================================>>>>>>>>>>>>>>>");
	// console.log("shopProdsArr");
	Shop.findOne({ nameSlug: currentShop.nameSlug },(err, foundShop) => {
		if(err){
			callback();
			return;
		}
		if(!foundShop){
			callback();
			return;
		}
		console.log("ADDING THE PRODUCT TO SHOP !!!!! ");
		var shop = foundShop;
		shop.collections = currentShop.collections;
		if(!shop.collections || shop.collections == undefined || shop.collections == null)
			shop.collections = [];

		if(!(!currentShop.collections || currentShop.collections == undefined)){
			for(let i = 0; i< currentShop.collections.length; i++){
				if(!_.find(shop.collections, {'_id': currentShop.collections[i]._id})){
					shop.collections.push(currentShop.collections[i]);
				}
			}
		}
		console.log("Collections to the shop Added!" )
		console.log('FOUND SHOP COLLECTIONS', shop.collections);



		var counter2 = 1;
		shop.save(function(err,response){
			if(err){
				console.error("Error while adding the collections");
				console.error(err);
				callback(true);
			}
			else{
				console.log("Saved Product for collections ");
			}
			console.log("NOW ADDING PRODUCTS TO THE SHOP Counter = "+counter2)
			async.each(shopProdsArr,(product, eCB)=>{
				console.log("Product being pushed ",product._id);
				if(!_.find(shop.products,{ "_id":product._id })){
					var dropshippingPrice = (product.dropshippingPrice) ? product.dropshippingPrice : 0;
					if(dropshippingPrice !== 0){
						dropshippingPrice = trim(dropshippingPrice);
						while(isNaN(dropshippingPrice[0])) dropshippingPrice = dropshippingPrice.substr(1);
					}
					console.log("Product For Shop", product);
					var productObject = {
						_id: product._id,
						stock: product.stock ? product.stock : 0,
						price: product.suggestedRetailPrice ? product.price : 0,
						dropshippingPrice: dropshippingPrice,
						discount: product.discount ? product.discount : 0,
						collections: product.collections ? product.collections : [],
					};
					console.log("New Insert Product in shop", productObject);
					console.log("Length of products array before pushing "+shop.products.length);
					shop.products.push(productObject);
					console.log("Length of products array AFTER pushing "+shop.products.length);
					eCB();
				}
				else{
					console.log("Product Already Exists in shop.!");
				}

			},(err,data)=>{
				if(!err){
					console.log("SHOP to be saved  ======> ");
					shop.save((err, savedShop) => {
						++counter2;
						if(err){
							console.error(err);
							callback(true);
							return;
						}
						console.log('SHOP SAVED');
						callback(savedShop);
					});
				}
				else{
					console.error("There was an error in addProductsToShop ",err);
				}
			});
		})

	});
};

function getValueForKey(prod, value, key, custom, callback){
	try{
//check from fields which can not be saved directly and get the right value for the choosen value
if(key === 'brandname'){
//NOTE: CustomData Object should have values for brands in brands object and not brandname.
var brands = custom.brands;
var curBrand = prod[value.name];
var result = '';
for(var b = 0; b < brands.length; b++){
	if(brands[b].found == curBrand){
		result = brands[b].value;
	}
	if(b == brands.length-1){
		if(typeof callback === 'function'){
			return callback(result);
		}else{
			return result;
		}
	}
}
} else if(key == "gender"){
	var genders = custom.genders;
	var sex = prod[value.name];
	var result = {
		male: false,
		female: false,
		kids: false
	};
	for(var i = 0; i < genders.length; i++){
		if(genders[i].found == sex){
			var gen = genders[i].value;

			if(gen == "GENTS" || gen == "BOYS"){
				result.male = true;
				result.kids = gen == "BOYS";
			}
			if(gen == "LADIES" || gen == "GIRLS"){
				result.female = true;
				result.kids = gen == "GIRLS";
			}
			if(gen == "UNISEX" || gen == "KIDS"){
				result.male = true;
				result.female = true;
				result.kids = gen == "KIDS"
			}
		}
		if(i == genders.length-1){
			if(typeof callback === 'function'){
				return callback(result);
			}else{
				return result;
			}
		}
	}
} else if(key == "casematerial"){
	var caseMat = custom.materials;
	var material = prod[value.name];

	for(var i=0; i< caseMat.length; i++){
		if(caseMat[i].found == material){
			return caseMat[i].value;
		}
	}
} else if(key == "strapmaterial"){
	var strapMat = custom.strapMaterial;
	var material = prod[value.name];

	for(var i=0; i< strapMat.length; i++){
		if(strapMat[i].found == material){
			return strapMat[i].value;
		}
	}
} else if(key == 'watchtype'){
// console.log(custom);
var type = prod[value.name];
var watchTypes = custom.types;

for(var t=0; t< watchTypes.length; t++){
	if(watchTypes[t].found == type){
		return watchTypes[t].value;
	}
}

} 

else if(key == 'color' || key == 'designItem'){
	var genArr = prod[value.name].split(",");
	var resultArr = [];
	for (var clr=0; clr < genArr.length; clr++){
		resultArr.push(genArr[clr]);
	}
	return resultArr;
}
else{
	console.log("IN GET VALUE FOR KEY FUNCTION FOR KEY : ",key);
	console.log("RETURNING ",value.name," From prod as" , prod[value.name])
	return prod[value.name];
}
}catch(e){
	return prod[value];
}
}

function jsonifyProductKey(product, jCB){
	var productKey = {};
	try{
		if(product){
			var variants = {};
			productKey.category = product.category;
			productKey.brand = product.brand ? JSON.parse(product.brand) : undefined;
			productKey.image1 = product.image1 ? JSON.parse(product.image1) : undefined;
			productKey.image2 = product.image2 ? JSON.parse(product.image2) : undefined;
			productKey.image3 = product.image3 ? JSON.parse(product.image3) : undefined;
			productKey.suggestedRetailPrice = product.suggestedRetailPrice ? JSON.parse(product.suggestedRetailPrice) : undefined;
			productKey.price = product.price ? JSON.parse(product.price) : undefined;
			productKey.stock = product.stock ? JSON.parse(product.stock) : undefined;
			productKey.gender = product.gender ? JSON.parse(product.gender) : undefined;

			variants = {
				productNumber : product.variants[0].productNumber ? JSON.parse(product.variants[0].productNumber): undefined,
				ean: product.variants[0].ean ? JSON.parse(product.variants[0].ean) : undefined,
				sku : product.variants[0].sku ? JSON.parse(product.variants[0].sku) : undefined,
				size: product.variants[0].size ? JSON.parse(product.variants[0].size) : undefined,
			}

			productKey.variants = variants;
			productKey.productCollection = {};
			// productKey.productCollection = product.productCollection ? JSON.parse(product.productCollection) : undefined;
			console.log("********************************************************** PRODUCT *********************************************************** = ",product);
			productKey.productCollection.en = ("en" in product && "productCollection" in product.en) ? JSON.parse(product.en.productCollection) : undefined ;
			productKey.productCollection.fr = ("fr" in product && "productCollection" in product.fr) ? JSON.parse(product.fr.productCollection) : undefined ;
			productKey.productCollection.nl = ("nl" in product && "productCollection" in product.nl) ? JSON.parse(product.nl.productCollection) : undefined ;
			productKey.productCollection.es = ("es" in product && "productCollection" in product.es) ? JSON.parse(product.es.productCollection) : undefined ;
			productKey.productCollection.de = ("de" in product && "productCollection" in product.de) ? JSON.parse(product.de.productCollection) : undefined ;
			// productKey.productCollection.en = product.en ? JSON.parse(product.en.productCollection)

			// productKey.productCollection.en = (product.hasOwnProperty('en.productCollection')) ? JSON.parse(product.en.productCollection) : undefined;
			// console.log(productKey.productCollection.en);
			// productKey.productCollection.nl = (product.hasOwnProperty('nl.productCollection')) ? JSON.parse(product.nl.productCollection) : undefined;
			// productKey.productCollection.de = (product.hasOwnProperty('de.productCollection')) ? JSON.parse(product.de.productCollection) : undefined;
			// productKey.productCollection.es = (product.hasOwnProperty('es.productCollection')) ? JSON.parse(product.es.productCollection) : undefined;
			console.log("=========>>>>>>>>>>>>****************************************************************************************************************************WHILE ASSIGNING THE PRODUCT KEY, ",product.productCollection);

			productKey.en = product.en ? {
				name: product.en.name ? JSON.parse(product.en.name) : undefined,
				shortDescription : product.en.shortDescription ? JSON.parse(product.en.shortDescription) : undefined,
				longDescription : product.en.longDescription ? JSON.parse(product.en.longDescription) : undefined,
			} : undefined;

			productKey.nl = product.nl ? {
				name: product.nl.name ? JSON.parse(product.nl.name) : undefined,
				shortDescription : product.nl.shortDescription ? JSON.parse(product.nl.shortDescription) : undefined,
				longDescription : product.nl.longDescription ? JSON.parse(product.nl.longDescription) : undefined,
			} : undefined;

			productKey.de = product.de ? {
				name: product.de.name ? JSON.parse(product.de.name) : undefined,
				shortDescription : product.de.shortDescription ? JSON.parse(product.de.shortDescription) : undefined,
				longDescription : product.de.longDescription ? JSON.parse(product.de.longDescription) : undefined,
			} : undefined;

			productKey.fr = product.fr ? {
				name: product.fr.name ? JSON.parse(product.fr.name) : undefined,
				shortDescription : product.fr.shortDescription ? JSON.parse(product.fr.shortDescription) : undefined,
				longDescription : product.fr.longDescription ? JSON.parse(product.fr.longDescription) : undefined,
			} : undefined;

			productKey.es = product.es ? {
				name: product.es.name ? JSON.parse(product.es.name) : undefined,
				shortDescription : product.es.shortDescription ? JSON.parse(product.es.shortDescription) : undefined,
				longDescription : product.es.longDescription ? JSON.parse(product.es.longDescription) : undefined,
			} : undefined;



			productKey.strap = product.strap ? {
				width: product.strap.width ? JSON.parse(product.strap.width) : undefined,
				studs: product.strap.studs ? JSON.parse(product.strap.studs) : undefined,
				clasp: product.strap.clasp ? JSON.parse(product.strap.clasp) : undefined,
				model : product.strap.model ? JSON.parse(product.strap.model) : undefined,
				material : product.strap.material ? JSON.parse(product.strap.material) : undefined,
				print: product.strap.print ? JSON.parse(product.strap.print) : undefined,
				wristPermimeter : product.strap.wristPermimeter ? JSON.parse(product.strap.wristPermimeter) : undefined,
color : product.color ? JSON.parse(product.color) : undefined, //FIXME: color should come as an internal key and not parent key.
} : undefined;


console.log("**************Product.Watch ******************", product.watch);
console.log("*****************product.watch.hasLightFunction", product.watch.hasLightFunction);
console.log("**********Product.watch.anti allergry",product.watch.isAntiAllergy)
if("watch" in product){
	if("hasLightFunction" in product.watch){
// console.log("Correcting hasLightFunction and others from Yes/No to True/False");
// console.log("=============================================================================================================Corrected hasLightFunction and others from Yes/No to True/False",product.watch.hasDateFunction);
if(product.watch.hasLightFunction == 'No') product.watch.hasLightFunction = false;
if(product.watch.hasLightFunction == 'Yes') product.watch.hasLightFunction = true;
}
if("isAntiAllergy" in product.watch){	
	// console.log("=========================================================================isAntiAllergy", product.watch);
	if(product.watch.isAntiAllergy == 'No') product.watch.isAntiAllergy = false;
	if(product.watch.isAntiAllergy == 'Yes') product.watch.isAntiAllergy = true;
}

if("hasDateFunction" in product.watch){	
	if(product.watch.hasDateFunction == 'No') product.watch.hasDateFunction = false;
	if(product.watch.hasDateFunction == 'Yes') product.watch.hasDateFunction = true;
}
}


productKey.watch = product.watch ? {

	type: product.watch.type ? JSON.parse(product.watch.type) : undefined,
	indication: product.watch.indication ? JSON.parse(product.watch.indication) : undefined,
	hasSwissMovement: product.watch.hasSwissMovement ? JSON.parse(product.watch.hasSwissMovement) : undefined,
	hasDateFunction: product.watch.hasDateFunction == "Yes" ? true : ( product.watch.hasDateFunction == "No" ? false : undefined),
// hasDateFunction: product.watch.hasDateFunction ? JSON.parse(product.watch.hasDateFunction) : undefined,
waterproofLevel: product.watch.waterproofLevel ? JSON.parse(product.watch.waterproofLevel) : undefined,
isNickelFree: product.watch.isNickelFree ? JSON.parse(product.watch.isNickelFree) : undefined,
isAntiAllergy: product.watch.isAntiAllergy == "Yes" ? true : (product.watch.isAntiAllergy=="No" ? false : undefined),
hasLightFunction: product.watch.hasLightFunction == "Yes" ? true : ( product.watch.hasLightFunction == "No" ? false : undefined),
// hasLightFunction: product.watch.hasLightFunction ? JSON.parse(product.watch.hasLightFunction) : undefined,
isSmartwatch: product.watch.isSmartwatch ? JSON.parse(product.watch.isSmartwatch) : undefined,
smartWatchFunctions: product.watch.smartWatchFunctions ? JSON.parse(product.watch.smartWatchFunctions) : undefined,

case: product.watch.case ? {
	shape: product.watch.case.shape ? JSON.parse(product.watch.case.shape) : undefined,
	size: product.watch.case.size ? JSON.parse(product.watch.case.size) : undefined,
	depth: product.watch.case.depth ? JSON.parse(product.watch.case.depth) : undefined,
	material: product.watch.case.material ? JSON.parse(product.watch.case.material) : undefined,
	glassMaterial: product.watch.case.glassMaterial ? JSON.parse(product.watch.case.glassMaterial) : undefined,
	color: product.watch.case.color ? JSON.parse(product.watch.case.color) : undefined,
	designItem: product.watch.case.designItem ? JSON.parse(product.watch.case.designItem) : undefined,
} : undefined,

dial: product.watch.dial ? {
	color: product.watch.dial.color ? JSON.parse(product.watch.dial.color) : undefined,
	pattern: product.watch.dial.pattern ? JSON.parse(product.watch.dial.pattern) : undefined,
	print: product.watch.dial.print ? JSON.parse(product.watch.dial.print) : undefined,
	index: product.watch.dial.index ? JSON.parse(product.watch.dial.index) : undefined,
} : undefined,

strap: product.watch.strap ? {
	width: product.watch.strap.width ? JSON.parse(product.watch.strap.width) : undefined,
	studs: product.watch.studs == "Yes" ? true : ( product.watch.studs == "No" ? false : undefined),
// studs: product.watch.strap.studs ? JSON.parse(product.watch.strap.studs) : undefined,
clasp: product.watch.strap.clasp ? JSON.parse(product.watch.strap.clasp) : undefined,
model : product.watch.strap.model ? JSON.parse(product.watch.strap.model) : undefined,
material : product.watch.strap.material ? JSON.parse(product.watch.strap.material) : undefined,
print: product.watch.strap.print ? JSON.parse(product.watch.strap.print) : undefined,
wristPermimeter : product.watch.strap.wristPermimeter ? JSON.parse(product.watch.strap.wristPermimeter) : undefined,
color : product.watch.strap.color ? JSON.parse(product.watch.strap.color) : undefined,
} : undefined,
} : undefined;

productKey.jewel = product.jewel ? {
	material: product.jewel.material ? JSON.parse(product.jewel.material) : undefined,
	color: product.jewel.color ? JSON.parse(product.jewel.color) : undefined,
	type: product.jewel.type ? JSON.parse(product.jewel.type) : undefined,
	height: product.jewel.height ? JSON.parse(product.jewel.height) : undefined,
	width: product.jewel.width ? JSON.parse(product.jewel.width) : undefined,
	depth: product.jewel.depth ? JSON.parse(product.jewel.depth) : undefined,
	diameter: product.jewel.diameter ? JSON.parse(product.jewel.diameter) : undefined,
	weight: product.jewel.weight ? JSON.parse(product.jewel.weight) : undefined,
	chain: product.jewel.chain ? JSON.parse(product.jewel.chain) : undefined,
	clasp: product.jewel.clasp ? JSON.parse(product.jewel.clasp) : undefined,
	shape: product.jewel.shape ? JSON.parse(product.jewel.shape) : undefined,
	gloss: product.jewel.gloss ? JSON.parse(product.jewel.gloss) : undefined,
	goldPurity: product.jewel.goldPurity ? JSON.parse(product.jewel.goldPurity) : undefined,
	gemColor: product.jewel.gemColor ? JSON.parse(product.jewel.gemColor) : undefined,
	gemShape: product.jewel.gemShape ? JSON.parse(product.jewel.gemShape) : undefined,
} : undefined;
// console.log(product);
return jCB(null, productKey);
}
}
catch(e){
	return jCB(e);
}
};

function genImageSlug2(prod, productKey, imageKey, callback){
// console.log(jsonObj);
var brand = productKey.brand.name;
var prodNum = productKey.variants.productNumber.name;
var imageSlug = `${_.get(prod, brand)}-${_.get(prod, prodNum)}-${imageKey}`;
return callback(imageSlug);
};

function determineProductSlug(name, count, callback){
	var nameSlug = str.slugify(name);
	if(count > 0){
		nameSlug = str.slugify(name) + '-' + count;
	}

	Product.find({'en.nameSlug': nameSlug}).sort({_id: -1}).exec(function(err, product){
		if(err){
			return callback(err);
		}
		if(product.length == 0){
			return callback(null, nameSlug);
		}else{
//count = count+1;
return determineProductSlug(name, count+1, callback);
}
})
}

function sendMail(results, products, userId,to){
	User.findOne({_id: userId}).exec(function(err, user){
		var data = {
			results: results,
			products: products
		};

// to: 'jolmer@excellent-electronics.nl',
// to: 'tirthrajbarot2394@gmail.com',
var options = {
	to: to,
	bcc: 'info@prismanote.com, tirthrajbarot2394@gmail.com',
	subject: 'Uw import is gereed',
	template: 'updates/import-result'
};

mailService.mail(options, data, null, function(err, mailResult){

})
})
}
