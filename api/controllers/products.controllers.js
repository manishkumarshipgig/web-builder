const path = require('path');
const fs = require('fs');
const Url = require('url');
const async = require('async');
const Q = require('q');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const ProductReplacement = mongoose.model('ProductReplacement');

const ProductUpdateLog = mongoose.model('ProductUpdateLog');
const User = mongoose.model('User');
const Brand = mongoose.model('Brand');
const Shop = mongoose.model('Shop');
const Order = mongoose.model('Order');
const Old_Product = mongoose.model('OldProducts');
const DuplicateProducts = mongoose.model('DuplicateProducts');
const productSuggestings = mongoose.model('productSuggestings');

const ObjectId = require('mongodb').ObjectId;

const collectionService = require(path.join(__dirname, '..', 'services', 'collection.service'));
const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const str  = require(path.join(__dirname, '..', 'services', 'string.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));
const mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));


module.exports.addSuggestionForUpdate = function(req, res) {
	console.log('[addSuggestionForUpdate in Products controller]', req.body);
	var product = req.body.product;
	var shop = req.body.shop;
	console.log("Shop : ",shop);
	delete product.modifiedProperties;
	delete product.oldProducts;
	delete product.suggestions;
	var productId = product._id;
	var currentUser = req.session.passport.user;
	var suggestion = {
		product_id: productId,
		item : product,
		suggester: {
			id: currentUser._id,
			name: currentUser.firstName + ' ' + currentUser.lastName + ' ' + shop.name,
			usertype: currentUser.role
		}
	};

	create(suggestion, (newOldProducts) => {
		productSuggestings.create(newOldProducts, (errInsertSuggestion, insertSuggestedProduct) => {
			if(errInsertSuggestion) {
				console.log("errInsertSuggestion", errInsertSuggestion);
				res.send({
					status: false,
					message: 'Something went wrong'
				});
			} else {




				Product.findByIdAndUpdate(productId,{
					$set: {
						lastSuggestedDate: Date.now(),
						priorityKey : true,
						isVerified: false
					},
					$addToSet : {
						suggestions : insertSuggestedProduct._id
					}
				}, {new: true},function(err,updatedProduct){
					if(err) {
						res.send({
							status: false,
							message: 'Something went wrong'
						});
					} else {
						res.send({
							suggestion,
							data : updatedProduct
						});
					}
				})
			}
		})
	})
}


module.exports.getProduct = function(req, res) {
	console.log('[getProduct controller]');

	if(req.params && req.params.nameSlug) {

		Product
		.findOne({'en.nameSlug': req.params.nameSlug})
		.populate("collections._id")
		.populate('oldProducts')
		.populate('oldProducts.item.collections._id')
		.populate('duplicateProducts')
		.populate('duplicateProduct.item.collections._id')
		.populate('suggestions')
		.populate('suggestions.item.collections._id')
		.lean()
		.exec(function(err, product) {

			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid product ID. ' + err};
			} else if (!product) {
				status = 404;
				response = {message: 'Product not found. '};
			} else {
				if(product.collections) product.collections = loadCollections(product);
				var returnOldProducts = [];
				if(!product.oldProducts) product.oldProducts = [];
				if(product.oldProducts.length > 0){
					for(var i = 0; i < product.oldProducts.length; i++){
						product.oldProducts[i] = Object.assign(product.oldProducts[i].item, product.oldProducts[i]);
						delete product.oldProducts[i].item;
					}
				}
				if(!product.duplicateProducts) product.duplicateProducts = [];
				if(product.duplicateProducts.length > 0){
					for(var i = 0; i < product.duplicateProducts.length; i++){
						product.duplicateProducts[i] = Object.assign(product.duplicateProducts[i].item, product.duplicateProducts[i]);
						delete product.duplicateProducts[i].item;
					}
				}

				if(product.collections)
					for(var i = 0; i < product.collections.length; i++) 
						product.collections[i] = Object.assign(product.collections[i],product.collections[i]._id);

					status = 200;
					response = {message: 'OK', product};
				}

				console.log(status + ' ' + response.message);
				res.status(status).json(response);

			});
	}
}


module.exports.saveShopProduct = function(req,res){
	// console.log("Saving Shop Product", req.body);

	var shopId = req.body.shop;
	var prodToBeUpdated = req.body.product;

	// console.log(shopId,"Shop Id");
	// console.log("product update", prodToBeUpdated);
	console.log("Stock", prodToBeUpdated.stock);
	console.log("Stock Count",parseInt(prodToBeUpdated.stock));

	// PRODUCT WITH UPDATES
	var prod = {
		_id: prodToBeUpdated._id,
		name: prodToBeUpdated.name,
		productNumber: prodToBeUpdated.productNumber,
		suggestedRetailprice: prodToBeUpdated.suggestedRetailPrice,
		images: prodToBeUpdated.images,
		stock: Number(prodToBeUpdated.stock),
		price: prodToBeUpdated.price,
		discount: prodToBeUpdated.discount,
		show: prodToBeUpdated.show,
		status: prodToBeUpdated.status,
		dropshippingPrice: prodToBeUpdated.dropshippingPrice,
		isBestseller : prodToBeUpdated.isBestseller,
		isFeatured : prodToBeUpdated.isFeatured,
		countrId: prodToBeUpdated.countrId ? prodToBeUpdated.countrId : null
	};
	console.log("New Product", prod);
	console.log("Updating the shop");
	Shop.findOneAndUpdate({
		_id: shopId,
		"products._id": prod._id
	},
	{
		$set: {
			"products.$": prod
		}
	},
	{
		new: true,
		upsert:true
	}).exec(function(err,doc){

		if(err!==null && err.code == 16836) {
			console.log("ADDING NEW PRODUCT TO THE SHOP");
			Shop.findOneAndUpdate({
				_id: shopId
			},{
				$addToSet: {
					"products" : prod
				}
			})
			.populate("collections._id")
			.exec((err,updatedShop)=>{
				if(err) return res.status(500).send({err})
					console.log("Shop Updated")
				collectionService.updateShopWithProperCollectionsByShopId(shopId);
				return res.status(200).send({shop:updatedShop})

			})
		}else if(err)
		return res.status(500).send(err);
		else{

			return res.send({
				message: "Updated Successfully", 
				prod: doc
			});
		}

	});

}

module.exports.getProductsFromShop = function(req,res){
	console.log("getProductsFromShop");
	var status, response;

	Shop.findOne({_id: req.params.shopId})
	.populate("collections._id")
	.exec(function(err, shop){
		if (err){
			res.status(500).json({message: 'Internal server error. ' + err});
		}else if(shop == null || typeof shop == "undefined"){
			res.status(404).json({message: 'No shop found with given criteria'});
		}else{

			var products = [];
			for(var i =0; i< shop.products.length; i++){
				var shopProduct = shop.products[i];
				Product
				.findOne({_id: shop.products[i]._id})
				.exec(function(err, product){
					if(product){
						var prod = {
							_id: product._id,
							name: product.name,
							productNumber: product.productNumber,
							suggestedRetailprice: product.suggestedRetailPrice,
							images: product.images,
							stock: shopProduct.stock || 0,
							price: shopProduct.price || 0,
							discount: shopProduct.discount || 0,
							show: shopProduct.show || 0,
							status: shopProduct.status
							// dropshippingPrice: prodToBeUpdated.dropshippingPrice,
							// isBestseller : prodToBeUpdated.isBestseller,
							// isFeatured : prodToBeUpdated.isFeatured,
						}
						products.push(prod);
					}
					if(products.length == shop.products.length){
						res.status(200).json({message: "OK", products});
					}
				})
			}
		}
	})
}

module.exports.searchText = function(req,res){
	console.log(req.params.searchText);
	var param = req.params.searchText;

	Product.find({
		$text: {
			$search : param
		}
	}).limit(24).exec(function(err,products){
		if(err) return res.status(500).send({"ERROR":err})
			if(products.length == 0) return res.status(404).send();

		console.log("Search Result Length = ",products.length);
		return res.status(200).send(products);
	});
}


module.exports.updateShopsForProductUpdates = updateShopsForProductUpdates;
function updateShopsForProductUpdates(){
	//console.log("[[[[[[[[[[[[[[[updateShopsForProductUpdates]]]]]]]]]]]]]]]");
	ProductUpdateLog.find({
		status: "Pending"
	}).exec((err,prodUpdates)=>{
		//console.log("Prods for update = "+ prodUpdates.length);
		//console.log("Finding Shops that have this product. ");
		async.forEachSeries(prodUpdates,function(item,callback){
			
			Shop.find({
				"products._id" : {
					$in: [item.productId]
				}
			})
			.select("_id")
			.exec(function(err,shops){
				console.log("Number of shops holding this item = ",shops.length)
				async.forEachSeries(shops,function(shop, fesCB){
					collectionService.updateShopWithProperCollectionsByShopId(shop._id)
					.then((updatedShop)=>fesCB(),(err)=> fesCB(err))
				},(err,data)=>{
					item.status = "Done";
					item.save((err,doc)=>{
						if(err) console.error(err);
						callback();
					})
				})
			})
		},function(err,data){
			if(err) console.error(err);
			//console.log("Final Callback for updateShopsForProductUpdates AFES Recived");
		})

	})
}

updateShopsForProductUpdates();

module.exports.mergeProducts = function(req,res){
	/*
	1. Get Primary Product
	2. Get Duplicate Product
	3. Enter Duplicate Product to the DuplicateProducts collection
	4. Enter Primary Product into the OldProducts collection
	5. Push ID of obtained by insert in DuplicateProducts into the "mergedProducts" key of primaryProduct
	6. Update Primary Product
	7. Add Duplicate Product in ProductReplacement for CRON JOB Execution

	*/
	var primary = req.params.primary;
	var duplicate = req.params.duplicate;
	console.log("Primary Product Id", primary);
	Product.findOne({_id: primary}).lean().exec(function(err,primaryProduct){
		if(err){
			console.error("ERROR : ",err.message);
			return res.status(500).send({error: err.message});
		}

		console.log("Get Primary Products",primaryProduct);
		console.log("--------------------------------------------------------------------");

		Product.findOne({_id: duplicate}).exec(function(err,duplicateProduct){
			if(err) return res.status(500).send({error: err.message});
			if(duplicateProduct.length == 0) return res.status(404).send({message: "No Such Duplicate Product Found"})
			// console.log("Duplicate Product : ",duplicateProduct);

		var duplicateProdsEntry = new DuplicateProducts({
			product_id: duplicateProduct._id,
			item: duplicateProduct,
			mergedTo: primaryProduct._id
		});

		duplicateProdsEntry.save(function(err,duplicateProdsEntryResult){
				// console.log("Duplicate Product Table ID = ",duplicateProdsEntryResult._id);



				const newData = {
					item: primaryProduct,
					product_id: primaryProduct._id,
					added_by: "admin"
				};

				var oldProduct = new Old_Product(newData);

				oldProduct.save(function(err,oldProductEntry){
					// console.log("Old Product Entry ID = ", oldProductEntry._id);
					// console.log("Old PrimaryProduct.oldProducts.length = ",primaryProduct.oldProducts.length);
					// console.log("Pushing in oldProduct");
					if(! primaryProduct.oldProducts)  primaryProduct.oldProducts = [];

					primaryProduct.oldProducts.push(oldProductEntry._id);
					// console.log("Pushed into oldProduct");
					// console.log("New PrimaryProduct.oldProducts.length = ",primaryProduct.oldProducts.length);

					// if(!"duplicateProducts" in primaryProduct){
					// console.log("Initializing duplicateProducts Array for primaryProduct");

					// }
					console.log("----------------------------------------------------------");
					console.log("----------------------------------------------------------");
					console.log("----------------------------------------------------------");
					console.log("primary product", primaryProduct);
					console.log("----------------------------------------------------------");
					console.log("----------------------------------------------------------");
					console.log("----------------------------------------------------------");

					if(!primaryProduct.duplicateProducts) primaryProduct.duplicateProducts = [];

					// console.log("Old PrimaryProduct.duplicateProducts.length = ",primaryProduct.duplicateProducts.length);
					// console.log("Pushing in duplicateProducts");
					primaryProduct.duplicateProducts.push(duplicateProdsEntry._id);
					// console.log("Pushed into duplicateProducts");
					// console.log("New PrimaryProduct.duplicateProducts.length = ",primaryProduct.duplicateProducts.length);

					console.log("New Primary Products", primaryProduct);
					Product.findByIdAndUpdate({_id: primaryProduct._id},primaryProduct, { new: true },function(err,updatedProduct){
						if(err) return res.status(500).send({error: err.message});
						console.log("Product Table Updated");

						// ADDING IT FOR PRODUCT REPLACEMENT
						ProductReplacement.create({primaryProduct, duplicateProduct}, function (err, addedRecord) {
							if (err) throw err;
							res.send({addedRecord, updatedProduct});
						})
					});
				})



				// primaryProduct.mergedProducts.push(duplicateProdsEntryResult._id);

			})




	})
	});
}

module.exports.cronTest = function (){
	console.log("CRON TEST FUNCTION CALL");
}

// MUST CODE FOR COLLECTIONS IN THE SHOP
module.exports.productReplacementAfterMerge = function (req,res){
	// Finding products from ProductReplacements Table
	ProductReplacement
	.find({
		status: "Pending",
		"type": "merge"
	})
	.lean()
	.populate("primaryProduct")
	.populate("duplicateProduct")
	.exec(function(err,productsToBeReplaced){
		console.log("Products Replacement Results : ");
		console.log(productsToBeReplaced);
		if(err){
			return res.status(500).send({
				msg: "There seems to be some problem. Please try again later",
				err: err
			});
		}
		if(productsToBeReplaced.length == 0){
			return res.status(404).send({
				msg: "No Products to be replaced"
			});
		}

		// console.log("FIRST DUPLICATE PRODUCT",productsToBeReplaced[0]);

		console.log("REPLACEMENT OF PRODUCTS = "+ productsToBeReplaced.length);
		async.parallel([
			async.apply(orderTableProductIdReplacement,productsToBeReplaced),
			async.apply(merchantTableProductIdReplacement,productsToBeReplaced),
			async.apply(shopsTableProductIdReplacement,productsToBeReplaced)
			],function (err, result) {
			// FINAL CALLBACK OF PARALLEL OPERATION
			if(err){
				console.log("Final Callback ERROR = ",err);
			}
			console.log("Final Callback Received : ",result);
			var idsToBeRemoved = [];
			for(var i = 0; i < productsToBeReplaced.length; i++) idsToBeRemoved.push(productsToBeReplaced[i]._id);
				console.log("IDS to be deleted : ",idsToBeRemoved);
			var productsToBeRemovedFromProductCollection = [];

			for(var i = 0; i < productsToBeReplaced.length; i++)
				productsToBeRemovedFromProductCollection.push(productsToBeReplaced[i].duplicateProduct);
			console.log("Products To Be Removed from Product Collection " ,productsToBeRemovedFromProductCollection);

			// DELETING PRODUCTS FROM PRODUCT TABLE
			Product.deleteMany({
				_id: {
					$in: productsToBeRemovedFromProductCollection
				}
			}).exec(function(err,success){
				if(err){
					console.error(err);
					return res.status(500).send({err});
				}
				else{
					console.log("SUCCESS IN DELETING PRODUCTS! ");
				}
				// Changing status of replaced Products from Pending to Done
				ProductReplacement.update({_id:{
					$in: idsToBeRemoved
				}}, { $set: { status: 'Done' } },function(err,success){
					if(err){
						console.error(err);
						res.status(500).send({err});
					}
					console.log("DONE !");
					return res.send({
						msg: "Finished! ",
						success,
						data: result});
				});
			});

				/*async.forEachSeries(productsToBeReplaced,function(item){
				console.log("ProductReplacement Updating!!");
				ProductReplacement.findByIdAndUpdate(item._id, { $set: { status: 'Done' } ,function(err,success){
				if(err){
				console.error("ERROR DELETING REPLACEMENTs!!!",err)
				callback(err);
			}
			else{
			callback(null,success)
		}
	});
},function(err,success){
if(err){
console.error(err);
res.status(500).send({err});
}
console.log("DONE !");
return res.send({
msg: "Finished! ",
success,
data: result
});
})*/

});


// Replacements on Order Table
function orderTableProductIdReplacement(productsToBeReplaced,WFCB){
	console.log("------------************** order Table Product Id Replacement **********----------------");
	var orderData = [];
	orderData.push("--------- ORDER DATA ---------");
	async.forEachSeries(productsToBeReplaced, function(item, FESCB) {
		console.log("Duplicate Products in Order Table : ",item);
		Order.find({
			'items._id' : {
				$in : [item.duplicateProduct._id]
			}
		})
		.exec(function(err,foundOrders){
			if(err){
				console.error(err);
				FESCB(err);

			}
			console.log("Checking Order Table for ",item.duplicateProduct.name);
			// console.log("Found shop for Product ",item, " ============== ",foundShops);
			for(var i = 0; i < foundOrders.length; i++){
				// console.log(foundShops[i].name ," has this product "+item.duplicateProduct.name+" !!!");
				orderData.push(foundOrders[i] +" has this product "+item.duplicateProduct.name);
			}
			console.log("DATA PUSHED : ",orderData);
			// if(foundShops.length > 0)
			// 	console.log("Total Shops = ",foundShops.length);
			// console.log("Found Shop name = ",foundShops);
			FESCB(null,orderData);
		})
	},function(err,fesOutput){
		if(err) WFCB(err);
		console.log("ForEachSeriesCallbackReceived with orderData = ",orderData);
		console.log("FESCB Received. All products traversed ");
		console.log("FESB orderData received = ",orderData);
		WFCB(null, orderData);
		// callback(null, 'Order Table Done');
	});
}
// Replacement on Merchant Table
function merchantTableProductIdReplacement(productsToBeReplaced,WFCB){
	console.log("------------************** merchant Table Product Id Replacement **********----------------");
	WFCB(null, "Merchant Table Done");
}

// Replacement on Shops Table
function shopsTableProductIdReplacement(productsToBeReplaced,WFCB){
	var data = [];
	console.log("------------************** shops Table Product Id Replacement **********----------------");
	data.push("--------- SHOPS DATA ---------")
	var x = 0;
	async.forEachSeries(productsToBeReplaced, function(item, FESCB) {
		x++;
		console.log("ITEM NO. "+x);
		Shop.find({
			'products._id' : {
				$in : [item.duplicateProduct._id]
			}
		}).lean()
		.exec(function(err,foundShops){
			if(err){
				console.error("ERROR AT SHOP FIND SHOP TABLE PRODUCT REPLACEMENT PRODCUTs CONTROLLER",err);
				FESCB(err);
				return;
			}
			if(foundShops.length < 1){
				console.log("FOUND SHOPS = 0");
				FESCB(null,"No Matching Products Found in shop.");
				return;
			}
			console.log("Found shop for Product ",item.duplicateProduct.name, " ============== ",foundShops.length);
			for(var i = 0; i < foundShops.length; i++){
				// console.log(foundShops[i].name ," has this product "+item.duplicateProduct.name+" !!!");
				data.push("Shop: "+foundShops[i].name +" has this product "+item.duplicateProduct.name+ " which is to be replaced with "+item.primaryProduct.name);
				var currShop = foundShops[i];
				// console.log("Products in "+currShop.name+" are ",currShop.products.length);
				for(var i = 0; i < currShop.products.length; i++){
					if(currShop.products[i]._id.equals(item.duplicateProduct._id)){
						// console.log("Replacing the Product "+item.duplicateProduct.name+" in shop "+currShop.name+" by "+item.primaryProduct.name);
						// console.log("Old Product  ============= ",shopProds[i]._id);
						// currShop.products[i]._id = item.duplicateProduct._id;
						// console.log("New Product  ============= ",shopProds[i]._id);
						var shopProds = currShop.products;
						shopProds[i]._id = item.primaryProduct._id;

						Shop.findByIdAndUpdate(currShop._id,{
							products: shopProds
						},function(err,success){
							if(err){
								console.error("Error in Updating the products array of currentShop : ",err);
								return FESCB(err);
							}

							console.log("DATA PUSHED : ",data);
							console.log("Successfully Saved!!")
							FESCB(null,data);
							return;
						});
					}
				}
			}
		})
	},function(err,fesOutput){
		if(err) WFCB(err);
		console.log("ForEachSeriesCallbackReceived with data = ",data);
		console.log("FESCB Received. All products traversed ");
		console.log("FESB data received = ",data);
		WFCB(null, data);
	});
}
})
}

module.exports.newProductAddedCronJob = function(req,res){

	Shop
	.find({
		"products.1" : {
			$exists: true
		}
	})
	.populate("products._id")
	.select("products._id name")
	.exec(function(err,shops){
		if(err) return res.status(500).send({err});

		// Product Replacement Query
		ProductReplacement.find({
			type: "new",
			status: "Pending"
		}).populate("primaryProduct")
		.exec(function(err,newProducts){
			// res.send(newProducts);
			var updateShopData = [];
			var currShopName;
			async.forEachSeries(newProducts,function(newProduct,AFESnp){
				async.forEachSeries(shops,function(shop,AFESs){
					currShopName = shop.name;
					getShopBrands(shop).then(function(brands){
						console.log("BRANDS OF SHOP = ",brands);
						// console.log("CHECKING BRAND ID AGAINST ABOVE ARRAY : ",newProduct.primaryProduct.brand._id);

						Array.prototype.contains = function(element){
							return this.indexOf(element) > -1;
						};
						if(brands.includes(ObjectId(newProduct.primaryProduct.brand._id).toString())){
							updateShopData.push({
								shopID : shop._id,
								product : newProduct.primaryProduct
							})
							console.log("YES BRAND "+newProduct.primaryProduct.brand.name+" OF NEW PRODUCT EXISTS IN SHOP "+shop.name);

						}else{
							// console.log("NO IT DOESN't EXIST ON SHOP "+shop.name);
						}

						AFESs(null,shop.name);
					})
					.catch(function(err){
						console.error(err);
						AFESs(err);
					})
				},function(err,data){
					if(err) AFESnp(err);
					console.log("FINAL CALLBACK OF SHOP  : "+currShopName,data);
					AFESnp(null);
				})
			},function(err,result){
				if(err) throw err;
				console.log("FINAL CALLBACK OF NEW PRODUCTS AFES");
				updateShopWithData(updateShopData,newProducts,function(err,result){
					if(err) return res.status(500).send(err);
					res.send({result : result})
				})
			})
		});
	})

	function updateShopWithData(data,newProducts,callback){
		console.log("UPDATING RECORDS  : ",data.length);
		async.forEachSeries(data,function(item,afescb){
			var product = item.product;
			var conditions = {
				_id : item.shopID
			};
			var update = {
				$addToSet : {
					"products" : {
						_id : ObjectId(product._id),
						"price" : product.suggestedRetailPrice,
						"discount" : 0,
						"stock" : 0,
						"collections" : [product.productCollection],
						"dateAdded" : new Date().toISOString(),
						"isFeatured" : false,
						"isBestseller" : false,
						"show" : true,
						"dropshippingPrice" : 0
					}
				}
			}

			Shop.findOneAndUpdate(conditions, update, function(err, doc) {
				console.log("Updated for Product ",product._id, " in shop ",item.shopID)
				if(err) return afescb(err);
				return afescb(null,doc);
			});
		},function(err,data){
			if(err) return callback(err);
			console.log("UPDATED EVERYTHING!");
			var updateInPR = [];
			for(var i = 0; i < newProducts.length; i++){
				console.log(newProducts[i]._id)
				updateInPR.push(newProducts[i]._id);
			}
			console.log("NOW UPDATING ",updateInPR);

			ProductReplacement.update(
				{ _id: { $in: updateInPR } },
				{ $set: { status : "done" } },
				{multi: true}
				).exec(function(err,docs){
					if(err) return callback(err);
					callback(null,"Done updating Status of Product Replacement Table!");
				})
			})
	}

	function getShopBrands(shop){
		// console.log("GETTING BRANDS FOR SHOP ID = ",shop.name);
		var deferred = Q.defer();

		var productBrandID;
		var brands = [];
		var brandsName = [];
		async.forEachSeries(shop.products,function(product,callback){
			if(product == null || product._id == null) return callback(null);
			getProductBrand(product._id)
			.then(function(brandID){
				if(!brandID) console.log("BRAND ID IS NULL!!! for the product ",product);
				if ((brandID._id != null)){
					brands.push(ObjectId(brandID._id).toString());
					brandsName.push(brandID.name);
				}
				callback(null,brandID);
			})
			.catch(function(err){
				console.error(err);
				callback(err);
			});
		},function(err,data){
			if(err) deferred.reject(err);
			// console.log("FINAL CALLBACK RECEIVED ",brands);

			let uniqueBrandsName = [...new Set(brandsName)];
			// console.log("Shop: "+shop.name,"BRANDSName = ",uniqueBrandsName);
			let uniqueBrands = [...new Set(brands)];
			// console.log("BRANDS = ",uniqueBrands);
			deferred.resolve(uniqueBrands);
		})
		// console.log("RETURNING PROMISE");
		return deferred.promise;

		function getProductBrand(pid){
			var deferred = Q.defer();
			Product
			.findOne({_id: pid}).lean()
			// .select("brands._id")
			.exec(function(err,product){
				if(err) deferred.reject(err)
				// console.log("RETURNING : ",productBrands);
			deferred.resolve(product.brand);
		})
			return deferred.promise;
		}

	}

	// res.send("byee!");
	// console.log("New Products Added Cron Job");
	// ProductReplacement.find({
	// 	status: "Pending",
	// 	"type": "new"
	// }).populate("primaryProduct").exec(function(err,newProducts){
	// 	if(err) return res.status(500).send({err});
	// 	if(newProducts.length < 1) return res.status(404).send({err : "No New Products Added."});
	// 	console.log("FINDING THE PRODUCT : ",newProducts[0].primaryProduct.brand._id);
	// 	var totalProducts = 0;
	// 	var shopsWithGivenBrand = [];
	// 	var brandlessShopsProducts = [];
	// 	var idLessProducts = [];
	// 	var nullProducts = [];
	// 	var data = [];
	// 	async.forEachSeries(newProducts,function(newProduct,callback){
	// 		Shop.find({
	// 			'products.1' : 	{
	// 				$exists : true
	// 			}
	// 		})
	// 		.populate("products._id")
	// 		.select("name")
	// 		.exec(function(err,shops){
	// 			if(err) throw err;
	// 			var shopsChecked = 0;


	// 			for(var i = 0; i < shops.length; i++){
	// 				var brandsOfThisShop = [];
	// 				var doneForTheShop = false;
	// 				console.log("DONE FOR THE SHOP "+shops[i].name +" = "+ doneForTheShop)
	// 				for(var j = 0; j < shops[i].products.length; j++){
	// 					totalProducts++;
	// 					if(!"_id" in shops[i].products[j]){
	// 						console.log(shops[i].name+" "+shops[i].products[j]._id+"  _id Doesn't exist on this product");
	// 						idLessProducts.push(shops[i].products[j])
	// 						continue;
	// 					}
	// 					else{
	// 						if(shops[i].products[j]._id == null){
	// 							console.log("shops[i].products[j]._id is null So Skipping it !");
	// 							nullProducts.push(shops[i].products[j])
	// 							continue;
	// 						}
	// 						if(!"brand" in shops[i].products[j]._id){
	// 							console.log("Brand Does Not Exist ",shops[i].products[j]._id.brand._id)
	// 							continue;
	// 						}
	// 					}
	// 					console.log("ITERATION SHOP = "+(i+1)+" Iteration Product = "+(j+1));
	// 					if(!"brand" in shops[i].products[j]._id){
	// 						brandlessShopsProducts.push(shops[i].products[j]);
	// 						console.log("SKIPPING THE BRANDLESS PRODUCT");
	// 						continue;
	// 					}


	// 					if(shops[i].products[j]._id.brand._id.equals(newProduct.primaryProduct.brand._id)){

	// 						console.log("MATCHED!!!");
	// 						console.log(newProduct.primaryProduct.brand.name + " BRAND EXISTS IN SHOP "+shops[i].name);
	// 						data.push(newProduct.primaryProduct.brand.name + " BRAND EXISTS IN SHOP "+shops[i].name);
	// 						doneForTheShop = true;

	// 						// break;
	// 					}
	// 					else{
	// 						console.log("Didn't match "+newProduct.primaryProduct.brand.name+ " To "+shops[i].products[j]._id.brand.name);
	// 					}
	// 				}
	// 				if(doneForTheShop){
	// 					shopsChecked++;
	// 					// data.push({
	// 					// 	shop: shops[i],
	// 					// 	brand: newProduct.primaryProduct.brand._id
	// 					// })
	// 					doneForTheShop = false;
	// 					console.log(" -------------------------------- "+shops[i].name+" sells "+newProduct.primaryProduct.brand.name);
	// 					data.push(shops[i].name+" sells "+newProduct.primaryProduct.brand.name+" So it will sell new product "+newProduct.primaryProduct.en.name);
	// 					// return callback(null,data);
	// 					// break;
	// 				}
	// 				else{
	// 					shopsChecked++;
	// 					// data.push({
	// 					// 	shop: shops[i],
	// 					// 	brand: newProduct.primaryProduct.brand._id
	// 					// })

	// 					console.log(" -------------------------------- "+shops[i].name+" doesn't sell "+newProduct.primaryProduct.brand.name);
	// 					data.push(shops[i].name+" doesn't sell "+newProduct.primaryProduct.brand.name);
	// 				}

	// 				console.log("****************************************************** Shops Done : "+shopsChecked+"/"+shops.length+""+"***********************************************************")
	// 				return callback(null,data);
	// 			}
	// 		});
	// 	},function(err,result){
	// 		console.log("FINAL CALLBACK RECEIVED!!")
	// 		if(err) throw err;
	// 		else return res.send({
	// 			"Total Replacement Products ": newProducts.length,
	// 			"totalProdsProcessed" : totalProducts,
	// 			data,
	// 			brandlessShopsProducts,
	// 			idLessProducts,
	// 			"nullProducts":nullProducts.length});

	// 	})

	// 	function addNewProductsInShop(){

	// 	}

	// 	/*async.forEachSeries(newProducts,function(item,callback){
	// 		console.log("New Product : ", item.primaryProduct.brand._id);
	// 		Shop.find({
	// 			// "products.brand._id" : {
	// 			// 	$in: [item.primaryProduct.brand._id]
	// 			// }
	// 		})
	// 		.populate("products._id")
	// 		.exec(function(err,shopsWithBrand){
	// 			for(var i = 0; i < shopsWithBrand.length; i++)
	// 				if(shopsWithBrand[i].products.length > 0)
	// 					console.log(shopsWithBrand);

	// 			callback(null,shopsWithBrand)
	// 			// console.log("SHOPS WITH Brand "+item.primaryProduct.brand._id + " Total = "+shopsWithBrand.length);
	// 			// console.log("SHOPS WITH BRAND : ",shopsWithBrand);

	// 			// for(var j = 0 ; j < shopsWithBrand.length; j++){
	// 			// 	console.log("Shop = "+shopsWithBrand[i].name);
	// 			// }
	// 		});
	// 	},function(err,data){
	// 		console.log("Final Callback!")
	// 		if(err) return res.status(500).send({err})
	// 		res.send({data});
	// })*/
	// })
}



module.exports.getProducts = function(req, res) {
	console.log('[getProducts controller]');
	// default values, because offset and limit are required
	var filterBy, sortBy, offset = 0, limit, aggregate;
	console.log("req.query", req.query);
	if(req.query) {

		if(typeof req.query === 'string') {
			req.query = JSON.parse(req.query);
		}

		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			offset = parseInt(req.query.offset);
			delete req.query.offset;
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			limit = parseInt(req.query.limit);
			delete req.query.limit;
		}

		// if any filters are provided in the URL, parse them and add to the filtering or aggregate params object.
		if(req.query.aggregate) {
			aggregate = JSON.parse(req.query.aggregate);
		} else if(req.query.filter) {
			filterBy = filter(req.query.filter);
			console.log("Query",req.query);
			if(filterBy.isVerified == "showAll"){
				delete filterBy.isVerified;
			}
			else if(filterBy.isVerified == false){
				// Leave it False
				filterBy.isVerified = false;
			}
			else{
				if(filterBy.isVerified == null && filterBy.includeUnverified != true) {
					filterBy.isVerified = true;
				}

			}

			console.log("------ filter by -----",filterBy);
			if(filterBy.includeUnverified) {
				delete filterBy.includeUnverified;
			}
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object.
		if(req.query.sort) {
			sortBy = sort(req.query.sort);

			console.log("Sort",sortBy);
			// dateLastModified
		}

		if(req.query.lng && req.query.lat) {
			// TODO find products close to the given coordinates.
		}
		console.log("Filters = ",filterBy);
	}
	if(aggregate) {
		console.log("Aggregate products", aggregate);
		var query = Product.aggregate().match(aggregate).sort(sortBy).allowDiskUse(true);
	} else{
		console.log("Else Of aggregate");
		var query = Product.find(filterBy).sort({priorityKey: -1}); //.select("nl en es fr de suggestedRetailPrice male female kids images variants brand countrId");
	}


	if(limit){
		// query.limit(limit);
	}
	console.log("offset",offset, "limit", limit);
	// Below .limit(limit) add by akib
	query.skip(offset).limit(limit).exec(function(err, products) {
		var status, response;
		console.log("response",products.length);
		if(err) {
			status = 500;
			response = {message: 'Internal server error. ' + err};
		} else if(!products || products.length == 0) {
			status = 204;
			response = {message: 'No products found with the given search criteria. '};
		} else {
			status = 200;
			response = {message: 'OK', products};
		}
		// console.log(status + ' ' + response.message);
		// console.log("USER : ",req.user);
		res.status(status).json(response);

	});
};

module.exports.getSpecificProduct = function(req, res){
	console.log("getSpecificProduct");
	Product.findOne({_id: req.query.productId}).populate("collections._id").exec(function(err, product){
		if(err){
			return res.status(500).json({message: err})
		}
		if(!product){
			return res.status(404).json({message: "No product found"})
		}
		// Populating Collections Right..
		for(var i = 0 ; i < product.collections.length; i++)
			product.collections[i] = Object.assign(product.collections[i],product.collections[i]._id)
		

		return res.status(200).json({message: "OK", product})
	})
}




module.exports.addProduct = function(req, res) {
	console.log('[addProduct controller]', req.body);

	if(req.body) {

		req.body.name = req.body.en.name; // Backwards compatibility

		create(req.body, function(newProduct) {
			console.log("this is newProducts", newProduct);
			if(newProduct) {

				if(newProduct.en.name != null && newProduct.en.name.length > 0) {
					newProduct.en.nameSlug = str.generateNameSlug(newProduct.en.name);
				}
				if(newProduct.fr.name != null && newProduct.fr.name.length > 0) {
					newProduct.fr.nameSlug = str.generateNameSlug(newProduct.fr.name);
				}
				if(newProduct.de.name != null && newProduct.de.name.length > 0) {
					newProduct.de.nameSlug = str.generateNameSlug(newProduct.de.name);
				}
				if(newProduct.nl.name != null && newProduct.nl.name.length > 0) {
					newProduct.nl.nameSlug = str.generateNameSlug(newProduct.nl.name);
				}
				if(newProduct.es.name != null && newProduct.es.name.length > 0) {
					newProduct.es.nameSlug = str.generateNameSlug(newProduct.es.name);
				}

				function getProductTitle(){
					if(newProduct.en && newProduct.en.name){
						return newProduct.en.name;
					}
					if(newProduct.nl && newProduct.nl.name){
						return newProduct.nl.name;
					}
					if(newProduct.de && newProduct.de.name){
						return newProduct.de.name;
					}
					if(newProduct.fr && newProduct.fr.name){
						return newProduct.fr.name;
					}
					if(newProduct.es && newProduct.es.name){
						return newProduct.es.name;
					}
				}

				if(newProduct.en && (newProduct.en.nameSlug == null || newProduct.en.nameSlug.length == 0)){
					//we need a english Nameslug, so if there isn't one, create one from a existing name
					newProduct.en.nameSlug = str.generateNameSlug(getProductTitle());
				}

				newProduct.uploader = {
					_id: req.user._id,
					name: req.user.firstName + ' ' + (req.user.lastNamePrefix || '') + ' ' + req.user.lastName
				}

				Product.create(newProduct, function (err, product) {
					var status, response

					if(err) {
						status = 400;
						response = {message: 'Product could not be added. Error message: ' + err};
					} else if(!product) {
						status = 404;
						response = {message: 'Product not found. '};
					} else {
						status = 201;
						response = {message: 'Product added. ', product};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
						res.status(status).json(response);
					}
					else{
						ProductReplacement.create({
							primaryProduct: product._id,
							"type": "new",
							status: "Pending"
						},function(err,addedForGeneralEntry){
							return res.status(200).send(response);
						});
					}

				});
			}
		});
	}
};

module.exports.uploadProductImages = function(req, res){
	console.log("[uploadProductImages controller]", req.body);

	async function uploadProductImage(file) {
		return new Promise(function(resolve, reject) {

			upload.uploadFile(file, 'products', null, function(err, result) {

				if(err) {
					reject(err);
				} else if(result) {
					resolve(result);
				}
			});
		});
	}

	if(req.body.productId){
		if(req.files.files && req.files.files.length > 0) {

			let uploads = [];

			for(let i = 0; i < req.files.files.length; i ++) {
				uploads[i] = uploadProductImage(req.files.files[i]);
			}

			Promise.all(uploads)

			.then(function(results) {

				Product.findOne({_id: req.body.productId}).exec(function(err, product) {

					if(err) {
						console.log("Error during updating product: " + err);
						return res.status(200).json({message: "Error during updating product: " + err});
					}

					else if(product) {

						for(let i = 0; i < results.length; i++) {
							product.images.push({src: results[i]});
						}

						product.save(function(err, result) {

							if(err) {
								return res.status(500).json({message: 'Error occurred during saving of the product. ', err});
							} else {
								return res.status(200).json({files: result});
							}
						});

					} else {

						console.log("Product not found");
						return res.status(404).json({message: "Product not found"});
					}
				});
			})

			.catch(function(err) {
				return res.status(500).json({message: 'error uploading files: ' + err})
			});

		} else {
			console.log("no product images found");
			return res.status(404).json({message: "no product images found"});
		}
	} else{
		console.log("no productId found");
		return res.status(404).json({message: "no productId found"});
	}
}

module.exports.putImagesToAmazon = function(req, res){
	//Download productimages and upload them to Amazon S3
	console.log("putImagesToAmazon");
	var resultArray = [];

	var limit = parseInt(req.query.limit);
	var skip = parseInt(req.query.skip);

	console.log("limit", limit, "skip", skip);

	Product.find({}).sort({_id: -1}).skip(skip).limit(limit).exec(function(err, products){

		console.log("aantal producten", products.length);

		for(var p =0; p< products.length; p++){
			if(products[p].images.length > 0){

				for(var i =0; i< products[p].images.length; i++){

					var image = products[p].images[i];

					getImageUrl(image.src, products[p], i, p, function(err, result, product, i, p){
						if(!err){
							//slow down a little bit to avoid timeouts and ddos blocks
							sleep.msleep(1000);

							upload.uploadStream(result, 'products', null, function(err, data){

								if(err){
									console.log("error uploading", err);
									resultArray.push(err);

									var data = err.statusCode + ";" + err.statusMessage + ";" + err.error + ";" + err.url + "\r\n";

									fs.appendFile('errors.csv', data, function(err){
										if (err) throw err;

									})

								}else{
									console.log("image uploaded", p);
									product.images[i].src = data;

									product.save(function(err, result){
										if(err){
											console.log("error during saving", err);
										}else{
											console.log("updated!", product.name);
										}
									})
								}
							})
						}else{
							console.log("error", err);
						}
					})
				}
			}
		}
	})
	res.send("Ok");
}

function getImageUrl(src, product, i, p, callback){
	if(!src){
		return callback("No src provided " + p.toString());
	}
	if(src.indexOf("products/") !== -1){
		return callback("image already uploaded " + p.toString());
	}

	if(src.indexOf("http") !== -1){
		if(src.indexOf("prismanote.com") !== -1){
			var url = Url.parse(src);
			return callback(null, "https://52.17.102.38" + url.pathname, product, i,p);
		}else{
			return callback(null, src, product,i,p);
		}
	}else if(src.indexOf("product-image") !== -1){
		return callback(null, settings.s3Bucket.location + src, product,i,p);
	}else{
		return callback("invalid src provided" + src + " " + p.toString());
	}
}

function slugify(text){
	return text.toString().toLowerCase()
	.replace(/\s+/g, '-')           // Replace spaces with -
	.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
	.replace(/\-\-+/g, '-')         // Replace multiple - with single -
	.replace(/^-+/, '')             // Trim - from start of text
	.replace(/-+$/, '');            // Trim - from end of text
}

function parseProduct(body, callback){
	let updateMode = true;
	let verifyProduct = false;

	if(body.verifyProduct && body.verifyProduct == true) {
		verifyProduct = true;
		updateMode = false;
		delete body.verifyProduct;
	}

	if(updateMode && body.modifiedProperties){
		let currentShop = body.currentShop;

		let versions = body.versions;
		versions[versions.length-1].modifiedProperties = body.modifiedProperties;

		let product = {
			versions: versions,
			isVerified: verifyProduct
		}

		return callback(product, currentShop, updateMode, verifyProduct);
	}else{
		return callback(body, null, updateMode, verifyProduct);
	}
}

// MUST CODE FOR COLLECTIONS IN THE SHOP
module.exports.updateProduct = function(req, res) {
	console.log('[updateProduct controller]');

	/* This helper function can be called from elsewhere in the updateProduct controller and processes the HTTP response (and possibly errors) */
	function sendResponse(status, response, err) {

		/* Log HTTP response to the console server side before actually sending it to the client. */
		console.log(status + ' ' + response.message);

		/* Also log stack trace on error */
		if(err) {
			console.log('\nError message: ' + err);
			console.log(err.stack);
		}

		/* Send HTTP response */
		res.status(status).json(response);
	}

	if(req.params && req.params.nameSlug && req.body) {
		parseProduct(req.body, function(product, currentShop, updateMode, verifyProduct){
			console.log("INTO UPDATE FUNCTION");

			product.dateLastModified = Date.now();

			// Fetch current product details
			Product.findOne({'_id': req.body._id}, function(error, response) {
				/* Some error occurred while trying to update the product. */

				if(error) {
					sendResponse(400, {message: 'Product could not be added. '}, err);
					/* The product doesn't exist */
				} else if (Object.keys(response).length === 0) {
					sendResponse(404, {message: 'Product not found. '});
				} else {
					// console.log("PRODUCT TO BE UPDATED COLLECTIONS ",product.collections)
					const productId = response._id;
					// response['product_id'] = productId;
					// response._id = mongoose.Types.ObjectId();

					create(response, (newOldProducts) => {
						var currentUser = req.session && req.session.passport && req.session.passport.user ? req.session.passport.user : null;
						const newData = {
							item: newOldProducts,
							product_id: productId,
							added_by: currentUser
						};
						Old_Product.create(newData, (errInsertOldProduct, insertOldProduct) => {
							if(errInsertOldProduct) {
								sendResponse(400, {message: 'Something went wrong'}, errInsertOldProduct);
							} else {
								if (newOldProducts.oldProducts) {
									newOldProducts.oldProducts.push(insertOldProduct._id);
								} else {
									newOldProducts['oldProducts'] = [insertOldProduct._id];
								}
								var finalProduct = req.body;
								finalProduct.oldProducts = newOldProducts.oldProducts;
								finalProduct.dateLastModified = Date.now();
								finalProduct.priorityKey = false;

								console.log("Update Product",finalProduct);
								Product.findByIdAndUpdate({_id: req.body._id}, finalProduct, {upsert: true, new: true}).exec(function (err, updatedProduct) {
									console.log("this is update updatedProduct", updatedProduct);
									

									// PRODUCT UPDATE LOG ENTRY SECTION.
									var pul = new ProductUpdateLog({
										productId: req.body._id
									});
									pul.save((err,pulDoc)=>{
										/* Some error occurred while trying to update the product. */
										if(err) {
											sendResponse(400, {message: 'Product could not be added. '}, err);
											/* The product doesn't exist */
										} else if(!product) {
											sendResponse(404, {message: 'Product not found. '});
											/* The product exists and has been updated successfully. */
										} else {
											/* Check if this product is being verified by an admin or not. If so, check if the admin is the same person who initially uploaded the product. */
											if(verifyProduct == true && updatedProduct.uploader._id != req.user._id) {
												/* If so, find that user to get their email adress. */
												User.findById(updatedProduct.uploader._id, function(err, user) {
													/* An error occurred, so mailing the original uploader is not possible. */
													if(err) {
														sendResponse(200, {message: 'Product updated with errors. ', updatedProduct}, err);

														/* The original uploader does not exist (anymore), so sending mail is not possible. */
													} else if(!user) {

														sendResponse(200, {message: 'Product updated, but the uploader doesn`t seem to exist. ', updatedProduct});

														/* Send an email to the original uploader to notify them. */
													} else {
														var data = {
															product: product,
															user: user
														};

														var options = {
															to: user.email,
															subject: updatedProduct.en.name + ' is toegevoegd aan PrismaNote!',
															template: 'retailer-product-verified'
														};

														mailService.mail(options, data, null);
														sendResponse(200, {message: 'Product updated. ', updatedProduct});
													}
												});
											} else {
												sendResponse(200, {message: 'Product updated. ', updatedProduct});
											}
										}
									})
								});
							}
						})
					})
					// const newRecord = new Old_Product(response);
					// Old_Product.create(response, (errInsertOldProduct, insertOldProduct) => {
					// 	if(errInsertOldProduct) {
					// 		sendResponse(400, {message: 'Something went wrong'}, errInsertOldProduct);
					// 	} else {
					// 		console.log("insertOldProduct", insertOldProduct);
					// 		return false;
					// 		product.oldProducts.push(response);

					// 		Product.update({_id: req.body._id}, product, {upsert: true, new: true}, function (err, updatedProduct) {
					// 			console.log("this is update updatedProduct", updatedProduct);
					// 			/* Some error occurred while trying to update the product. */
					// 			if(err) {
					// 				sendResponse(400, {message: 'Product could not be added. '}, err);
					// 				/* The product doesn't exist */
					// 			} else if(!product) {
					// 				sendResponse(404, {message: 'Product not found. '});
					// 				/* The product exists and has been updated successfully. */
					// 			} else {
					// 				/* Check if this product is being verified by an admin or not. If so, check if the admin is the same person who initially uploaded the product. */
					// 				if(verifyProduct == true && updatedProduct.uploader._id != req.user._id) {
					// 					/* If so, find that user to get their email adress. */
					// 					User.findById(updatedProduct.uploader._id, function(err, user) {
					// 						/* An error occurred, so mailing the original uploader is not possible. */
					// 						if(err) {
					// 							sendResponse(200, {message: 'Product updated with errors. ', updatedProduct}, err);

					// 							/* The original uploader does not exist (anymore), so sending mail is not possible. */
					// 						} else if(!user) {

					// 							sendResponse(200, {message: 'Product updated, but the uploader doesn`t seem to exist. ', updatedProduct});

					// 							/* Send an email to the original uploader to notify them. */
					// 						} else {
					// 							var data = {
					// 								product: product,
					// 								user: user
					// 							};

					// 							var options = {
					// 								to: user.email,
					// 								subject: updatedProduct.en.name + ' is toegevoegd aan PrismaNote!',
					// 								template: 'retailer-product-verified'
					// 							};

					// 							mailService.mail(options, data, null);
					// 							sendResponse(200, {message: 'Product updated. ', updatedProduct});
					// 						}
					// 					});
					// 				} else {
					// 					sendResponse(200, {message: 'Product updated. ', updatedProduct});
					// 				}
					// 			}
					// 		});
					// 	}
					// })


				}
			})
})
}
};

// MUST CODE FOR COLLECTIONS IN THE SHOP
module.exports.removeProduct = function(req, res) {
	console.log('[removeProduct controller]');

	if(req.body && req.body.productId) {

		Product.findByIdAndRemove({_id: req.params.productId}, function(err, product) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid product ID. ' + err};
			} else if(!product) {
				status = 404;
				response = {message: 'Product not found. '};
			} else {
				status = 204;
				response = {message: 'Product deleted. '};
			}

			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}
			// Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
			res.status(status);
		});

	}
};

module.exports.getProductCount = function(req, res) {
	var filterBy;

	if(req.query.filter) {
		filterBy = filter(req.query.filter);
	}

	Product.count({filterBy}, function(err, productCount) {
		var status, response;

		if(err) {
			status = 400;
			response = {message: 'Error: ' + err};
			console.log(err.stack);
		} else {
			status = 200;
			response = {count: productCount};
		}

		res.status(status).json(response);
	});
}

module.exports.importFromJson = function(req, res){
	var file = path.join(__dirname, '..', 'import', '_products.json');
	const lines = require('lines-adapter');
	const stream = fs.createReadStream(file);

	lines(stream, 'utf8').on('data', line => {

		var oldProduct = JSON.parse(line);

		if (oldProduct.eanCode == "" || typeof(oldProduct.eanCode) === "undefined") {
			return;
		}

		Product.findOne({ean: oldProduct.eanCode}).exec(function(err, product) {

			// Alleen nieuw product aanmaken als EAN nog niet bestaat
			if(!product) {

				var newProduct = {nl: {}, en: {}, de: {}, fr: {}, es: {}, watch: {case: {}, dial: {}, strap: {}}, category: 'WATCH'};

				Brand.findOne({name: oldProduct.brandName}).exec(function(err, brand) {

					if(brand) {
						newProduct.brand = {
							_id: brand._id,
							name: brand.name,
							nameSlug: brand.nameSlug
						}

						switch(oldProduct.kindOfWatch) {
							case ('wristwatch'):
							newProduct.watch.type = 'WRIST';
							break;
							case 'pocket watch':
							newProduct.watch.type = 'POCKET';
							break;
							case 'nurse watch':
							newProduct.watch.type = 'NURSE';
							break;
							case 'smartwatch':
							newProduct.watch.type = 'WRIST';
							newProduct.watch.isSmartwatch = true;
							break;
							default:
							newProduct.watch.type = 'WRIST';
							break;
						}

						newProduct.watch.waterproofLevel = oldProduct.waterproofLevel;

						newProduct.watch.isNickelFree = oldProduct.isNickelFree;
						newProduct.watch.isAntiAllergy = oldProduct.isAntiAllergy;
						newProduct.watch.hasSwissMovement = oldProduct.hasSwissMovement;
						newProduct.watch.hasLightFunction = oldProduct.hasLightFunction;

						newProduct.male = false;
						newProduct.female = false;
						newProduct.kids = false;

						if(oldProduct.sex.toLowerCase() == "ladies"){
							newProduct.female = true;
						}
						if(oldProduct.sex.toLowerCase() == "gents") {
							newProduct.male = true;
						}
						if(oldProduct.sex.toLowerCase() == "kids") {
							newProduct.male = true;
							newProduct.female = true;
							newProduct.kids = true;
						}
						if(oldProduct.sex.toLowerCase() == "boys") {
							newProduct.male = true;
							newProduct.kids = true;
						}
						if(oldProduct.sex.toLowerCase() == "girls") {
							newProduct.female = true;
							newProduct.kids = true;
						}
						if(oldProduct.sex.toLowerCase() == "unisex" || oldProduct.sex.toLowerCase() == "ladies/gents") {
							newProduct.male = true;
							newProduct.female = true;
						}

						newProduct.isBestseller = oldProduct.isBestseller;

						newProduct.suggestedRetailPrice = oldProduct.suggestedRetailPrice;

						newProduct.variants = [];
						newProduct.variants[0] = {
							productNumber: oldProduct.productNumber,
							ean: oldProduct.eanCode
						};

						var images = [];

						if(oldProduct.image1) {
							var image = {
								src: oldProduct.image1,
								alt: oldProduct.productNameNl
							};
							images.push(image);
						}
						if(oldProduct.image2) {
							var image = {
								src: oldProduct.image2,
								alt: oldProduct.productNameNl
							};
							images.push(image);
						}
						if(oldProduct.image3) {
							var image = {
								src: oldProduct.image3,
								alt: oldProduct.productNameNl
							};
							images.push(image);
						}

						newProduct.images = images;

						newProduct.name = oldProduct.productNameEn.trim(); // Backwards compatibility
						newProduct.nameSlug = str.generateNameSlug(oldProduct.productNameEn.trim()); // Backwards compatibility

						newProduct.en.name = oldProduct.productNameEn.trim();
						newProduct.en.nameSlug = str.generateNameSlug(oldProduct.productNameEn.trim());
						newProduct.en.longDescription = oldProduct.otherProductDescription1En.trim();
						newProduct.en.shortDescription = oldProduct.productDescriptionEn.trim();

						newProduct.nl.name = oldProduct.productNameNl.trim();
						newProduct.nl.nameSlug = str.generateNameSlug(oldProduct.productNameNl.trim());
						newProduct.nl.longDescription = oldProduct.otherProductDescription1Nl.trim();
						newProduct.nl.shortDescription = oldProduct.productDescriptionNl.trim();

						newProduct.de.name = oldProduct.productNameDe.trim();
						newProduct.de.nameSlug = str.generateNameSlug(oldProduct.productNameDe.trim());
						newProduct.de.longDescription = oldProduct.otherProductDescription1De.trim();
						newProduct.de.shortDescription = oldProduct.productDescriptionDe.trim();

						newProduct.fr.name = oldProduct.productNameFr.trim();
						newProduct.fr.nameSlug = str.generateNameSlug(oldProduct.productNameFr.trim());
						newProduct.fr.longDescription = oldProduct.otherProductDescription1Fr.trim();
						newProduct.fr.shortDescription = oldProduct.productDescriptionFr.trim();

						if(oldProduct.productNameEs && oldProduct.otherProductDescription1Es && oldProduct.productDescriptionEs) {
							newProduct.es.name = oldProduct.productNameEs.trim();
							newProduct.es.nameSlug = str.generateNameSlug(oldProduct.productNameEs.trim());
							newProduct.es.longDescription = oldProduct.otherProductDescription1Es.trim();
							newProduct.es.shortDescription = oldProduct.productDescriptionEs.trim();
						} else {
							delete newProduct.es;
							// newProduct.es.name = newProduct.en.name;
							// newProduct.es.nameSlug = newProduct.en.nameSlug;
							// newProduct.es.longDescription = newProduct.en.longDescription;
							// newProduct.es.shortDescription = newProduct.en.shortDescription;
						}

						if(oldProduct.pointerFunction.toLowerCase().trim() == 'datum') {
							newProduct.watch.hasDateFunction = true;
						}

						switch(oldProduct.indication.toLowerCase()) {
							case ("chrono/multi (more clocks)" || "chrono & multi"):
							newProduct.watch.indication = "CHRONO_MULTI";
							break;
							case 'analog & digital':
							newProduct.watch.indication = "ANALOG_DIGITAL";
							break;
							case 'smartwatch':
							newProduct.watch.indication = "DIGITAL";
							break;
							case 'analog':
							newProduct.watch.indication = "ANALOG";
							break;
							case 'digital':
							newProduct.watch.indication = "DIGITAL";
							break;
							default:
							newProduct.watch.indication = null;
							break;
						}

						var smartWatchFunctions = [];
						if(oldProduct.smartProperty1) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty1));
						}
						if(oldProduct.smartProperty2) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty2));
						}
						if(oldProduct.smartProperty3) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty3));
						}
						if(oldProduct.smartProperty4) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty4));
						}
						if(oldProduct.smartProperty5) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty5));
						}
						if(oldProduct.smartProperty6) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty6));
						}
						if(oldProduct.smartProperty7) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty7));
						}
						if(oldProduct.smartProperty8) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty8));
						}
						if(oldProduct.smartProperty9) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty9));
						}
						if(oldProduct.smartProperty10) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty10));
						}
						if(oldProduct.smartProperty11) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty11));
						}
						if(oldProduct.smartProperty12) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty12));
						}
						if(oldProduct.smartProperty13) {
							smartWatchFunctions.push(str.generateTranslationTag(oldProduct.smartProperty13));
						}

						newProduct.watch.smartWatchFunctions = smartWatchFunctions;

						var caseMaterial = null;
						if(oldProduct.caseMaterial != null && oldProduct.caseMaterial != "") {
							if(oldProduct.caseMaterial == 'all stainless steel' || oldProduct.caseMaterial == 'all stainless') {
								caseMaterial == 'STAINLESS_STEEL';
							} else {
								caseMaterial = str.generateTranslationTag(oldProduct.caseMaterial);
							}
						}

						var designItem = [];
						if(oldProduct.designItemFrontCase1) {
							designItem.push(str.generateTranslationTag(oldProduct.designItemFrontCase1));
						}
						if(oldProduct.designItemFrontCase2) {
							designItem.push(str.generateTranslationTag(oldProduct.designItemFrontCase2));
						}
						if(oldProduct.designItemFrontCase3) {
							designItem.push(str.generateTranslationTag(oldProduct.designItemFrontCase3));
						}

						newProduct.watch.case = {
							shape: str.generateTranslationTag(oldProduct.caseForm),
							size: oldProduct.caseSize,
							depth: oldProduct.caseTickness,
							material: caseMaterial,
							glassMaterial: str.generateTranslationTag(oldProduct.glassMaterial),
							designItem: designItem
						};

						var dialPattern = null;
						if(oldProduct.designItemPattern != null && oldProduct.designItemPattern != "") {
							if(oldProduct.designItemPattern === 'stripes pattern') {
								dialPattern = 'STRIPES';
							} else if(oldProduct.designItemPattern === 'dots pattern') {
								dialPattern = 'DOTS';
							}
						}

						var dialColor = [];
						if(oldProduct.dialColor != null) {
							dialColor.push(str.generateTranslationTag(oldProduct.dialColor));
						}

						newProduct.watch.dial = {
							color: dialColor,
							pattern: dialPattern
						}

						if(oldProduct.index.toLowerCase() == "roman numerals") {
							newProduct.watch.dial.index = "ROMAN";
						} else if(oldProduct.index.toLowerCase() == "numbers (arabic)") {
							newProduct.watch.dial.index = "ARABIC";
						} else if(oldProduct.index.toLowerCase() == "none") {
							newProduct.watch.dial.index = "NONE";
						} else if(oldProduct.index.toLowerCase() == "#n/b" || oldProduct.index == "" || typeof(oldProduct.index) == "undefined") {
							newProduct.watch.dial.index = null;
						} else {
							newProduct.watch.dial.index = str.generateTranslationTag(oldProduct.index);
						}

						var strapColors = [];
						if(oldProduct.strapColor1) {
							strapColors.push(str.generateTranslationTag(oldProduct.strapColor1));
						}
						if(oldProduct.strapColor2) {
							strapColors.push(str.generateTranslationTag(oldProduct.strapColor2));
						}
						if(oldProduct.strapColor3){
							strapColors.push(str.generateTranslationTag(oldProduct.strapColor3));
						}
						var strapStuds = false;
						if(oldProduct.designItemStrapStuds == "studs") {
							strapStuds = true;
						}
						var strapMaterial = null;
						if(oldProduct.strapMaterial == 'all stainless steel' || oldProduct.strapMaterial == 'all stainless') {
							strapMaterial == 'STAINLESS_STEEL';
						} else {
							strapMaterial = str.generateTranslationTag(oldProduct.strapMaterial);
						}

						var strapClasp = null;
						if(oldProduct.kindOfClasp != '') {
							str.generateTranslationTag(oldProduct.kindOfClasp);
						}

						newProduct.watch.strap = {
							model: str.generateTranslationTag(oldProduct.strapModel),
							width: oldProduct.widthStrap,
							material: strapMaterial,
							color: strapColors,
							studs: strapStuds,
							wristPerimeter: oldProduct.wristPerimeter,
							clasp: strapClasp
						}

						newProduct.uploader = {
							_id: req.user._id,
							name: req.user.firstName + ' ' + (req.user.lastNamePrefix || '') + ' ' + req.user.lastName
						}

						create(newProduct, function(result){
							if(result){
								Product.create(result, function(err, product){
									if (err) {
										console.log(err);
									} else {
										console.log("Product " + product.name + " added.");
									}
								})
							}
						})
					} else {
						console.log('Brand ' + oldProduct.brandName + ' not found. Product ' + oldProduct.nameSlug + ' was skipped.');
						return;
					}
				})
}
})
});
}



module.exports.getWebshopProductsSearch = function(req,res){

	console.log("getShopProductsCategories", req.params);
	var searchText = req.params.searchText;
	var searchMode = req.params.searchMode ;
	var nameSlug = req.params.shopNameSlug;

	var params = {};
	console.log("SEARCH MODE = ",searchMode);
	if(searchMode == "exact"){
		// params = {
		// 	$aggregate: [
		// 	{
		// 		$project : {
		// 			$toLower: "$variants.productNumber"
		// 		}
		// 	},
		// 	{
		// 		$match: {
		// 			'variants.productNumber' : searchText
		// 		}
		// 	}
		// 	]
		// }
		params = {
			'variants.productNumber' : {
				$regex : new RegExp(searchText,"i")
			}
		}
	}
	else if(searchMode == "contains"){
		params = {
			$text: {
				$search : searchText
			}
		}
	}
	else{
		return res.status(500).send("INVALID MODE");
	}

	console.log('params', params);

	Product.find(params)
	.lean()
	// .select("_id")
	.exec(function(err,totalProducts){
		console.log("Result Obtained for ",params,":--->>> ",totalProducts);
		console.log("Number of matches : "+totalProducts.length);

		if(err) return res.status(500).send({err})
			if(totalProducts.length < 1) return res.status(404).send("NO PRODS FOUND ");

		// UNCOMMENT THIS IF ONLY 1 RESULT SHALL BE DISPLAYED ON THE EXACT MODE SEARCH.
		// if(totalProducts.length > 1) return res.status(200).send({products:[]});


		var totalProductsIds = [];
		// Return 404 if No Product Found


		// Else make array of Product IDs found
		for(var i = 0; i < totalProducts.length; i++)
			totalProductsIds.push(totalProducts[i]._id.toString());


		// if(searchMode == "contains"){

			return res.send({products: totalProducts});

		// }



		// Now find the shop with given NameSlug and Populate the PRODUCTS within the objects.
		Shop.findOne({
			nameSlug: req.params.nameSlug,
		})
		.populate("products._id")
		.lean()
		.select("products._id")
		.exec(function(err,shopProds){

			if(err || shopProds == null){
				return res.status(500).send({err});
			}
			// console.log("SHOP = ",shopProds);
			shopProds = shopProds.products;
			// console.log("Prods = ",shopProds);



			let shopProdsIds = [];
			let shopProdsVals = [];
			let responseToSend = [];


			// Divide the ids and the objects into 2 separate arrays with Same INDEX
			for(let i = 0; i < shopProds.length; i++){
				if(shopProds[i]._id == null) continue;
				shopProdsIds.push(shopProds[i]._id._id.toString());
				shopProdsVals.push(shopProds[i]._id);
			}

			// comparing the ids and populating the response by the value array.
			for(let i = 0; i < shopProdsIds.length; i++)
				if(totalProductsIds.includes(shopProdsIds[i]))
					responseToSend.push(shopProdsVals[i]);

			// console.log("Shop Products = ",shopProdsIds.length);
			// console.log("Total Products Found = ", totalProductsIds.length);
			// console.log("Matched Products = ",responseToSend.length);
			console.log(responseToSend);
			console.log("Now sending Response");
			return res.send({products: responseToSend});

		})

		// .exec(function(err,foundShop){
		// 	// console.log("SHOP = ",foundShop);
		// 	var shopProductsArray = [];
		// 	for(var i = 0; i < foundShop.products.length; i++)
		// 	{
		// 		console.log("FOUND IN SHOP = ",foundShop.products[i]._id._id);
		// 		shopProductsArray.push(foundShop.products[i]._id._id);
		// 	}

		// 	// console.log("TOTAL PRODUCTS ARRAY = ",totalProducts);
		// 	// console.log("SHOP PRODUCTS ARRAY = ",shopProductsArray);
		// 	console.log("TOTAL PRODUCTS Found = "+totalProducts.length);
		// 	console.log("Total products in shop = "+shopProductsArray.length);

		// 	var finalProdsList = [];
		// 	for(var i=0; i<totalProducts.length; i++){
		// 		console.log("Checking ",totalProducts[i]._id);
		// 		console.log("FoundShopArray = ",shopProductsArray);
		// 		if(shopProductsArray.includes(totalProducts[i]._id)){
		// 			console.log("Exists in shop = "+ totalProducts[i].name, totalProducts[i]._id);
		// 			finalProdsList.push(totalProducts[i]);
		// 		}
		// 		else{
		// 			console.log("Doesn't exist in shop = "+totalProducts[i].name);
		// 			console.log("foundShop.products.indexOf(totalProducts[i]._id) = ",foundShop.products.indexOf(totalProducts[i]._id))
		// 		}
		// 	}
		// 	console.log("PRODUCTS MATCHED = ",finalProdsList.length);
		// 	// console.log("FINAL PRODS LIST = ",finalProdsList);
		// 	res.send({products:finalProdsList});
		// })

	});


}

module.exports.getWebshopProductsCategories = function(req,res){
	var cats = ["WATCH","STRAP","JEWEL","OTHER"];
	// return res.send(cats);
	console.log("getShopProductsCategories");

	Shop.findOne({nameSlug: req.params.nameSlug})
	.populate('products._id')
	.select("products")
	.exec(function(err,result){
		var cats = [];
		console.log("RESULT =",result);

		if(!result || !result.products || result.products.length <=0) {
			return res.status(204).json({message: "No categories found"});
		}

		for(var i = 0; i < result.products.length; i++){
			if(result.products[i]._id == undefined) continue;
			if(!cats.includes(result.products[i]._id.category)){
				cats.push(result.products[i]._id.category);
			}
			else{
				console.log("Already Exists in cat :",result.products[i]._id.category);
			}
		}
		return res.status(200).json({cats: cats});
	})
}

module.exports.getWebshopProducts = function(req, res){
	console.log("[ getShopProductsController ]");

	console.log("Name Slug", req.params.nameSlug);
	console.log("Query  : ",req.query);
	console.log("Limit = ",req.query.limit);
	console.log("Offset = ",req.query.offset);
	limiting = parseInt(req.query.limit) || 24;
	skipping = parseInt(req.query.offset) || 0;
	var shopFilter = {};
	var isRetailer = false;
	var inStock = false;
	if('user' in req){
		if('role' in req.user)
			if(req.user.role == 'retailer' && req.user.isVerified == true){
				isRetailer = true;
				console.log("IS RETAILER!");
			}
			else{
				console.log("IS NOT RETAILER");
			}
		}

		if(!req.query) {
			console.log("NO QUERY FOUND IN REQUEST");
			return res.status(500).json({message: "Not all details provided"})
		}

		var filterBy, sortBy, offset = 0, limit, aggregate;

		if(typeof req.query === 'string') {
			req.query = JSON.parse(req.query);
		}

		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			offset = parseInt(req.query.offset);
			delete req.query.offset;
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			limit = parseInt(req.query.limit);
			delete req.query.limit;
		}

	// if any filters are provided in the URL, parse them and add to the filtering or aggregate params object.
	if(req.query.filter) {
		filterBy = filter(req.query.filter);
		if(filterBy._id){
			delete filterBy._id;
		}

		if(filterBy.isVerified == null && filterBy.includeUnverified != true) {
			filterBy.isVerified = true;
		}
		if(filterBy.includeUnverified) {
			delete filterBy.includeUnverified;
		}
	}

	if(req.query.shopFilter){
		inStock = JSON.parse(req.query.shopFilter).inStock;
		// shopFilter = JSON.parse(req.query.shopFilter);

		// shopFilter.products = {
		// 	stock: {
		// 		$gt : 0
		// 	}
		// }
	}



	// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object.
	if(req.query.sort) {
		console.log("req.query.sort", req.query.sort);
		sortBy = sort(req.query.sort);
		console.log("SORT BY PRODUCTS = ",sortBy);

	}

	if(req.query.lng && req.query.lat) {
		// TODO find products close to the given coordinates.
	}

	if(!req.query.featured || typeof req.query.featured == 'undefined'){
		req.query.featured = false;
	}
	console.log("collection", req.query.collection);

	console.log("NOT A RETAILER ");
	/*	if(req.query.collection){
	var shop = Shop.findOne({nameSlug: req.params.nameSlug, 'product.collections': {$in: ObjectId(req.query.collection)}});
}else{
var shop = Shop.findOne({nameSlug: req.params.nameSlug});
}*/

console.log("ShopFilter = ",shopFilter);

Shop.findOne({
	nameSlug: req.params.nameSlug
})
.populate("collections._id")
.lean()
.exec(function(err,shopProds){
	if(err) return res.status(500).send(err);
	if(shopProds == null) return res.status(404).send({msg: "No Products found in shop"});

	if(shopProds.collections) shopProds.collections = collectionService.loadShopCollections(shopProds);
	console.log("Shop Prods = ");
	console.log(shopProds);
	console.log("shop Prods length", shopProds.products.length);
	var shopProdsIds = [];
	console.log("WEBSHOP ACTIVE : ",shopProds.webshopActive)
	if(!shopProds.showAllProducts){
		if(inStock){
			// ONLY IF inStock = true in request object.
			console.log("In Stock Filter Enabled");
			for(var i = 0; i < shopProds.products.length; i++)
				if(shopProds.products[i].stock > 0 && shopProds.products[i]._id != null)
					shopProdsIds.push(shopProds.products[i]._id);
			}
			else{
			// ONLY IF inStock = false or inStock is absent in request object.
			for(var i = 0; i < shopProds.products.length; i++) if(shopProds.products[i]._id != null) shopProdsIds.push(shopProds.products[i]._id);
		}
}
else{
		// if webshop is active, push all the products.
		for(var i = 0; i < shopProds.products.length; i++) if(shopProds.products[i]._id != null) shopProdsIds.push(shopProds.products[i]._id);
	}


console.log("Shop Prod IDS = ",shopProdsIds)

filterBy._id = {$in: shopProdsIds};
if(req.query.collection) filterBy.collection = {$in: ObjectId(req.query.collection)}

// delete filterBy.containsFilterInfo;
console.log("FILTER BY PRODUCTS = ",filterBy);

Product
.find(filterBy)
.limit(limiting)
.skip(skipping)
.populate("collections._id")
.lean()
.exec(function(err,prods){
	if(err) return res.status(500).send({err});
	console.log("Products found : ",prods.length);
	console.log("Shop Products : ",shopProds.products.length);
	var resopnseToSend = [];
	for(var i = 0; i < prods.length; i++)
		if(prods[i].collections) prods[i].collections = loadCollections(prods[i]);
	for(var i = 0; i < prods.length; i++)
		for(var j = 0; j < shopProds.products.length; j++)
			if(shopProds.products[j] && shopProds.products[j]._id && shopProds.products[j]._id != null && prods[i]._id != null && prods[i]._id.toString() == shopProds.products[j]._id.toString())
				resopnseToSend.push(Object.assign(shopProds.products[j],prods[i]));

			return res.send({message: 'OK',products: resopnseToSend, shopProds: shopProds.products})

		});
}) 




/*
var query  = {
nameSlug: req.params.nameSlug,
}

// FIXME!!!
// Can anyone mention the use of SLICE in here please ?
var slice = {
products: {
$slice: [offset, limit ? limit : null]
}
}

if(req.query.collection){
query['products.collection'] = {$in: ObjectId(req.query.collection)};
}

console.log("query", query);
console.log("limiting: ", limiting );
console.log("skipping: ", skipping );


// SKIP AND LIMIT IMPLEMENTATION IN POPULATE
var options = {};
options.limit = limiting;
options.skip = skipping;
filterBy._id = {
"$exists": true,
"$ne": null
}
console.log("Filter before execution : ",filterBy);
console.log("Filters Raw:",req.query.filter);
var myCustomFilter = {
$and: [
]
}
console.log("Custom Filter : ",myCustomFilter);
var mykeys = [];
for(var k in filterBy){
var temp = {};
temp[k] = filterBy[k];
myCustomFilter.$and.push(temp)
// mykeys.push(k);
}
console.log("Final Filter :",myCustomFilter );
console.log(myCustomFilter._id);

shop.
*/
/*	shop
.select("products")
.populate({
path:'products._id',
match : {
category:["WATCH"],
'_id._id': {
$exists: true
}
},
options: {
limit: 5
}
})
.where('products._id').ne(null)
// .where('products').slice([skipping,limiting])
.lean()
.exec(function(err, shop){
// console.log("shop===>", shop);
if(err){
return res.status(500).json({message: "Something went wrong", err})
}else if(!shop){
return res.status(404).json({message: "Shop not found"})
}else{
console.log("Result Obtained for the request of offset = ",skipping, " and limit = ",limiting, " is of the length = ",shop.products.length)
var resopnseToSend = [];
// for(var i = skipping; (i < shop.products.length && resopnseToSend.length <= limit); i++){
for(var i = 0; i < shop.products.length; i++){
// if(shop.products[i]._id == null || shop.products[i]._id._id == null){
// 	console.log("Deleting Product "+i+" because ID = null ",shop.products[i]._id);
// 	continue;
// 	delete shop.products[i];
// }
console.log(shop.products[i]);
var tempId = shop.products[i]._id._id;
// delete shop.products[i]._id;
var item = Object.assign(shop.products[i]._id,shop.products[i]);
item._id = tempId;
console.log("PRODUCT NUMBER = "+i+" pushed.")
resopnseToSend.push(item);
console.log("Item ",item);
}
console.log("RESPONSE HAS "+resopnseToSend.length+" products");
console.log("Sending Response")
return res.send({message: 'OK',products: resopnseToSend, shopProds: shop.products})

// var products = shop.products, productIds = [], counter =0;
// return res.status(200).json({ products});
// for(var i =0; i < products.length; i++){
// 	if(products[i]._id){

// 		productIds.push(products[i]._id);
// 		//Product verwijderen als deze niet overeen komt met het prijsfilter of als deze niet overeenkomt met het 'featured' filter
// 		if(filterBy.price && (filterBy.price.$gte != null || filterBy.price.$lte != null) && (products[i].price < filterBy.price.$gte || products[i].price > filterBy.price.$lte) || (req.query.featured && products[i].isFeatured != req.query.featured)){
// 			productIds.pop();
// 		}
// 	}
// 	counter++;

// 	if(counter == products.length){
// 		filterBy._id = {$in: productIds};
// 		if(filterBy.price){
// 			delete filterBy.price;
// 		}

// 		Product.find(filterBy).sort(sortBy).select("nl en es fr de suggestedRetailPrice male female kids images variants brand").exec(function(err, products){
// 			if(err){
// 				return res.status(500).json({message: "Something went wrong", err})
// 			}
// 			else if(!products || products.length == 0){
// 				return res.status(204).json({message: "No products found"})
// 			}else{
// 				var copiedProducts = JSON.parse(JSON.stringify(products))
// 				var prods = []
// 				var prodCounter = 0;

// 				for(var i=0; i < copiedProducts.length; i++){

// 					var shopProduct = _.find(shop.products, {_id: ObjectId(copiedProducts[i]._id)})
// 					var product = Object.assign({}, copiedProducts[i], shopProduct._doc);
// 					prods.push(product);

// 					prodCounter++;
// 					if(prodCounter == copiedProducts.length){
// 						return res.status(200).json({message: 'OK', products: prods});
// 					}
// 				}
// 			}
// 		})
// 	}
// }
}
})*/
}

module.exports.getSpecificShopProducts = function(req, res){
	console.log("[ getSpecificShopProducts ]");
	//This function does exactly the same as 'getproducts' but this is working with a POST instead a GET to avoid the 'REQUEST TOO LONG' error
	var filterBy, sortBy, offset = 0, limit, aggregate;

	if(req.body) {

		if(typeof req.body === 'string') {
			req.body = JSON.parse(req.body);
		}

		if(req.body.offset && !isNaN(parseInt(req.body.offset))) {
			offset = parseInt(req.body.offset);
			delete req.body.offset;
		}

		if(req.body.limit && !isNaN(parseInt(req.body.limit))) {
			limit = parseInt(req.body.limit);
			delete req.body.limit;
		}

		// if any filters are provided in the URL, parse them and add to the filtering or aggregate params object.
		if(req.body.aggregate) {
			aggregate = JSON.parse(req.body.aggregate);
		} else if(req.body.filter) {
			filterBy = filter(req.body.filter);
			if(filterBy.isVerified == null && filterBy.includeUnverified != true) {
				filterBy.isVerified = true;
			}
			if(filterBy.includeUnverified) {
				delete filterBy.includeUnverified;
			}
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object.
		if(req.body.sort) {
			sortBy = sort(req.body.sort);
		}

		if(req.body.lng && req.body.lat) {
			// TODO find products close to the given coordinates.
		}

	}
	if(aggregate) {
		var query = Product.aggregate().match(aggregate).allowDiskUse(true);
	} else{
		var query = Product.find(filterBy).sort(sortBy).select("nl en es fr de suggestedRetailPrice male female kids images variants brand countrId");
	}

	if(limit){
		query.limit(limit);
	}

	query.skip(offset).exec(function(err, products) {
		var status, response;

		if(err) {
			status = 500;
			response = {message: 'Internal server error. ' + err};
		} else if(!products || products.length == 0) {
			status = 204;
			response = {message: 'No products found with the given search criteria. '};
		} else {
			status = 200;
			response = {message: 'OK', products};
		}

		console.log(status + ' ' + response.message);
		return res.status(status).json(response);

	});
}




module.exports.getWebshopProduct = function(req, res){
	console.log("[[[GetWebshopProduct]]]");
	var obj = req.query;
	var shopId = obj.shopId;
	var productId = obj.productId;


	Shop.findOne({
		_id: obj.shopId
	})
	.lean()
	.exec(function(err,shop){
		var myProd;
		console.log("ShopProds = ",shop.products.length);
		for(var i = 0; i < shop.products.length; i++)
			if(obj.productId && obj.productId != null && shop.products[i] && shop.products[i]._id && shop.products[i]._id != null && obj.productId.toString() == shop.products[i]._id.toString()){
				myProd = shop.products[i];
				break;
			}

			console.log("myProd = ",myProd);
			Product.findOne({
				_id: obj.productId
			})
			.lean()
			.populate("collections._id")
			.exec(function(err,prod){
				if(err) return res.status(500).send()
					if(prod == null) return res.status(404).send({msg:"No Such Product Found"})
						console.log(prod);
					if(prod.collections) prod.collections = loadCollections(prod);
				// For letting the price of product be taken from shop and not the advisory price, 
				// the prod.price is assigned to prod.retailPrice and then the prod.price is deleted.
				// It is expected that the price of product would exist in the shop and therefore the 
				// Object.assign() will do the job of assigning the new price to the product to be sent in the response of the
				// retailer product modal

				if(prod.price != 0 ){
					prod.suggestedRetailPrice = prod.price;
					delete prod.price;
				}
				// var finalProduct = Object.assign(prod,myProd);
				let finalProduct = Object.assign({},prod,myProd);
				res.send(finalProduct)
			})

		})

};

function loadCollections(prod){
	for(var i = 0; i < prod.collections.length; i++)
		prod.collections[i] = Object.assign(prod.collections[i],prod.collections[i]._id);
	return prod.collections;
}



function syncProdsForContainsFilterInfo(){
	console.log("Synching prods");
	Product.find({
		containsFilterInfo: {
			$exists: false
		}
	})
	.lean()
	.exec((err,prods)=>{
		console.log("Prods = ",prods.length)
		async.eachSeries(prods, (item,callback)=>{
			console.log(item._id);
			Product.update({
				_id: item._id
			},{
				$set: {
					containsFilterInfo: null
				}
			},
			{
				upsert: true
			})
			.exec(function(err,doc){
				if(err) callback(err);
				else callback(null,doc);
			})
		}, function(err,doc){
			console.error("err: ",err);
			console.log("Final CB Received",doc)
		});

		console.log("Total Prods = ",prods.length);
	})
}

// syncProdsForContainsFilterInfo();



