const path = require('path');
const mongoose = require('mongoose');
const fs = require('fs');
const PhoneNumber = require('awesome-phonenumber');
const Mollie = require('mollie-api-node');
const ProductDeleteSuggestion = mongoose.model('ProductDeleteSuggestion');
const Shop = mongoose.model('Shop');
const Product = mongoose.model('Product');
const User = mongoose.model('User');
const Brand = mongoose.model('Brand');
const ObjectId = require('mongodb').ObjectId;
const _ = require('lodash');

const async = require('async');
const collectionService = require(path.join(__dirname, '..', 'services', 'collection.service'));
const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const update = require(path.join(__dirname, '..', 'services', 'update.service'));
const getVCard = require(path.join(__dirname, '..', 'services', 'vcard.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));
const shopService = require(path.join(__dirname, '..', 'services', 'shop.service'));
const socialPortalService = require(path.join(__dirname, '..', 'services', 'socialportal.service'));

const mollie = new Mollie.API.Client;
mollie.setApiKey(settings.mollie.apiKey)

module.exports.getShop = function(req, res) {

	if(req && req.params && req.params.nameSlug) {
		console.log('[getShop controller]');
		console.log("Populating COllections ")
		Shop
		.findOne({nameSlug: req.params.nameSlug})
		.populate("products._id")
		.populate("collections._id")
		.lean()
		.exec(function(err, shop) {
			// For some reason, this command returns an array with a single shop in Mongoose, whereas the Mongo native driver returns it as a shop document (as it should). This is not going to be fixed in the near future.
			var status, response;
			
			if(err) {
				console.log(err);
				return res.status(500).json({message: 'An error has occurred while trying to fetch this shop.',err:err})
			} else if(!shop) {
				return res.status(404).json({message: 'No shop found.'})
			} else {
				if(!shop.products) return res.status(500).json({message: 'Shop has no products'});
				var prodIds = [];
				for(var i = 0; i < shop.products.length; i++) {
					if(shop.products[i]._id && shop.products[i]._id && shop.products[i]._id != null){
						prodIds.push(shop.products[i]._id._id);
					}
			} 

					if(shop.collections) shop.collections = collectionService.loadShopCollections(shop);
					
					shopService.getShopSocialPortal(shop._id, function (err, portal){

						return res.status(200).json({message: 'OK', shop, portal,collections: shop.collections})
					})
			}
		});
	} else {
		console.log('No nameSlug provided for getShop controller. ');
		res.status(400).json({message: 'No nameSlug provided for getShop controller. '});
	}
};

module.exports.getShops = function(req, res) {
	console.log('[getShops controller]');

	if(req && req.query) {

		if(typeof req.query === 'string') {
			req.query = JSON.parse(req.query);
		}

		let query = Shop.find({});

		if(req.query.filter && req.query.filter.shop) {
			// Aggregation queries can't use find() as a pipeline operator, but $match filters subdocuments in a similar fashion.
			query.match(filter);
		}

		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			query.skip(parseInt(req.query.offset));
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			query.limit(parseInt(req.query.limit));
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
		if(req.query.sort) {
			query.sort(sort(req.query.sort));
		}

		// if any field inclusion/exclusion (selection) params are provided in the request, parse them and add to the Mongoose sorting object. 
		if(req.query.select) {
			// Aggregation queries don't have a 'select' operator, but $project does the same thing (in this context)
			query.select(req.query.select);
		} else {
			// Default setting: exclude some array fields to reduce result size. This should greatly reduce data traffic in the long run.
			query.select('-products -tasks -reviews -brands');
		}

		if(req.query.lng && req.query.lat) {
			// TODO: find shops close to the given coordinates.
		}

		query.exec(function(err, shops) {
			var status, response;

			if(err) {
				status = 500;
				response = {message: 'Internal server error. ' + err};
			} else if(!shops || shops.length == 0) {
				status = 404;
				response = {message: 'No shops found with the given search criteria. '};
			} else {
				

				status = 200;
				response = {message: 'OK', shops};
			}
			
			console.log(status + ' ' + response.message);
			res.status(status).json(response);
		});
	} else {
		console.log('No params or invalid params provided for getShops controller. ');
		res.status(400).json({message: 'No params or invalid params provided for getShops controller. '});
	}
};

module.exports.addShop = function(req, res) {
	console.log('[addShop controller]');

	if(req.body && req.body.shop) {
		create(req.body.shop, function(newShop) {
			newShop.isPremium = false;
			newShop.showAllProducts = false;
			newShop.sendProducts = false;
			Shop.create(newShop, function(err, shop){
				var status, response;

				if(err) {
					console.error(err)
					status = 400;
					response = {message: 'Shop could not be added. Error message: ' + err};
				} else {
					status = 201;
					response = {message: 'Shop added. ', shop};
				}

				console.log(status + ' ' + response.message);
				if(err) {
					console.log(err.stack);
				}
				res.status(status).json(response);
			});
		})
	}
};



// MUST CODE FOR COLLECTIONS IN THE SHOP -- Done Phase 1
module.exports.removeProductFromShop = function(req,res){
	console.log("[ removeProductFromShop CONTROLLER ]")
	console.log("SHOP = ",req.body.shop);
	console.log("Product = ",req.body.product);
	Shop.findByIdAndUpdate(req.body.shop,
	{
		$pull : {
			products : {
				_id: {
					$in : [req.body.product]
				}
			}
		}
	},
	{ 
		upsert: true,
		new: true ,
		returnNewDocument: true
	}
	)
	.lean()
	.populate("products._id")
	.exec(function(err,updatedShop){

		if(err) return res.status(500).send({err})


			collectionService.updateShopWithProperCollections(updatedShop)
		.then((finalShop)=>{
			return res.status(200).send({finalShop});
		},(err)=> {
			return res.status(500).send({err})
		})
	})
}



module.exports.addDeleteProductToSuggestion = function(req, res) {
	console.log("[ addDeleteProductToSuggestion ]");
	var userId = req.session.passport.user._id;
	var productId = req.body.productId;
	var userName = req.session.passport.user.firstName + " " + req.session.passport.user.lastName;

	var prodDelete = new ProductDeleteSuggestion({
		product: productId,
		suggester: {
			user: userId,
			name: userName
		}
	});
	prodDelete.save(function(err,doc){
		if(err) return res.status(500).send("INTERNAL SERVER ERROR");

		return res.send({message: "Added Successfully"});
	})


}


module.exports.getDeleteProductToSuggestion = function(req, res) {
	console.log("[ getDeleteProductToSuggestion ]");
	var limit = (req.query.limit)? req.query.limit : 24;
	var skip = (req.query.offset)? req.query.offset : 0;
	ProductDeleteSuggestion
	.find()
	.populate('product')
	.limit(limit)
	.skip(skip)
	.exec(function(err,prods){
		console.error("getDeleteProductToSuggestion", err);
		if(err) return res.status(500).send({message: "Internal SERVER Error"})
			return res.send({products: prods});
	})

}

module.exports.doDeleteProductToSuggestion = function(req,res){
	console.log("[ doDeleteProductToSuggestion ]");
	console.log(req.query);
	var productToDelete = req.query.productId;
	
	return res.send();
}








// MUST CODE FOR COLLECTIONS IN THE SHOP -- Done Phase 1
module.exports.addBrandToShop = function(req, res) {
	console.log('[ADDBRANDTOSHOP controller]');
	Shop.findOne({
		_id : req.body.shopId._id
	})
	.lean()
	.exec(function(err,shop){
		if(err) return res.status(500).send("INTERNAL SERVER ERROR");
		if(shop == null) return res.status(404).send("NO SUCH SHOP FOUND");
		for(var i = 0; i < shop.brands.length; i++) if(shop.brands[i]._id == req.body.shopId._id) return res.status(400).send("BAD REQUEST. BRAND ALREADY EXISTS ON SHOP");

		// BRAND PUSHED INTO SHOP
	shop.brands.push(req.body.brand);

		// ADD PRODUCTS OF BRAND TO SHOP
		Product.find({
			'brand._id': req.body.brand._id
		})
		.exec(function(err2,products){
			if(err2) return res.status(500).send("INTERNAL SERVER ERROR");
			if(products == null) return res.status(404).send("NO SUCH PRODUCTS FOUND");
			console.log("Number of products on shop : "+ shop.products.length);
			console.log("NUMBER OF PRODUCTS OF BRAND "+req.body.brand.name+" is "+products.length);
			console.log("ADDING PRODUCTS TO THE SHOP")
			for(var i = 0; i < products.length; i++){
				var tmpProduct = {
					_id: products[i]._id,
					stock: 0,
					price: products[i].suggestedRetailPrice,
					dropshippingPrice: products[i].suggestedRetailPrice,
					discount: 0,
					dateAdded: new Date(),
					show: true,
					isBestseller: false,
					isFeatured: false
				};

				(existsInArray(products[i]._id,shop.products)) ? null : shop.products.push(tmpProduct);
			}
			console.log("Finished adding products to shop.");
			console.log("Number of products on shop : ",shop.products.length);
			console.log("Updating shop . . . ");
			Shop.findOneAndUpdate({
				_id : shop._id
			},
			shop,
			{
				new:true,
				upsert:true
			}
			)
			.populate("products._id")
			.lean()
			.exec(function(err3,updatedShop){
				if(err3) return res.status(500).send("INTERNAL SERVER ERROR");
				console.log("UpdatedShop in addBrandToShop" , updatedShop);
				collectionService.updateShopWithProperCollections(updatedShop)
				.then((finalShop)=>{
					return res.status(200).send({finalShop});
				},(err)=> {
					return res.status(500).send({err})
				})

				return res.send("BRAND ADDED Successfully");
			})
		})
		
	})

	function existsInArray(needle,hay){
		// console.log("Exists in array checking function . ")
		for(var i = 0; i < hay.length; i++) if(hay[i]._id == needle) return true;
			return false;
	}
	
};



// MUST CODE FOR COLLECTIONS IN THE SHOP -- Done for Phase 1
module.exports.removeBrandProducts = function(req,res){
	console.log("[ removeBrandProducts ShopsController ]")

	Shop.findOne({
		_id: req.body.shopId
	})
	.populate("products._id")
	.exec(function(err,shopFound){
		if(err) return res.status(500).send("INTERNAL SERVER ERROR");
		if(shopFound == null) return res.status(404).send("No Such Shop Found");
		rmBrandId = req.body.brand._id;
		var rmProdIds = [];
		console.log("Shop found with the name : "+shopFound.name);
		console.log("Brand to be deleted = ",req.body.brand.name);
		
		for(var i = 0; i < shopFound.products.length; i++){
			if(shopFound.products[i]._id == null) continue;
			if(shopFound.products[i]._id.brand._id == rmBrandId) rmProdIds.push(shopFound.products[i]._id._id);
		}
		console.log("Products in the shop before removal = ",rmProdIds.length);
		console.log("Updating ")
		Shop.findOneAndUpdate({
			_id: req.body.shopId
		},
		{
			$pull : {
				products: {
					_id : {
						$in : rmProdIds
					}
				},
				brands : {
					_id: rmBrandId
				}
			}
		},{
			new:true,
			upsert: true
		})
		.populate('products._id')
		.lean()
		.exec(function(err,updatedShop){
			if(err){
				console.error(err);
				return res.status(500).send(err);
			}

			collectionService.updateShopWithProperCollections(updatedShop)
			.then((finalShop)=>{
				return res.status(200).send({finalShop});
			},(err)=> {
				return res.status(500).send({err})
			})
			
		})
		
	})

};



// MUST CODE FOR COLLECTIONS IN THE SHOP -- Done Phase 1
// We have to check for any product change because we have often sent request to update shop for various purposes on client side.
module.exports.updateShop = function(req, res) {
	console.log('[updateShop controller]');

	if(req.body && req.body.shop) {
		console.log("Getting Data",req.body, req.params);
		Shop.findOneAndUpdate({
			nameSlug: req.params.nameSlug
		}, 
		req.body.shop, 
		{
			upsert: true,
			new: true
		})
		.populate("products._id")
		.exec(function(err, shop) {
			let status, response;

			if(err) {
				status = 400;
				response = {message: 'Shop could not be updated. Error message: ' + err};
				return res.status(status).json(response);
			} else {
				status = 201;
				response = {message: 'Shop updated. ', shop: shop};
			}
			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}


			collectionService.updateShopWithProperCollections(shop)
			.then((finalShop)=>{
				return res.status(status).json(response);
			},(err)=> {
				return res.status(500).send({err})
			})
			// res.status(status).json(response);
		});

		// Merchant.aggregate({$unwind: '$shops'}, {$match: {'shops.nameSlug': req.params.nameSlug}}, {$project: {nameSlug: '$nameSlug'}}, function(err, result) {
			
		// 	Merchant.findOne({nameSlug: result[0].nameSlug}, function(err, merchant) {

		// 	});
		// });
	}
};

module.exports.removeShop = function(req, res) {
	console.log('[removeShop controller]');

	if(req.params && req.params.id) {

		Shop.findByIdAndRemove({_id: req.params.id}, function(err, shop) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid shop ID. ' + err};
			} else if(!shop) {
				status = 404;
				response = {message: 'Shop not found. '};
			} else {
				status = 204;
				response = {message: 'Shop deleted. '};
			}

			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}
			// Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
			res.status(status);
		});

	}
}

module.exports.getVCard = function(req, res) {
	console.log('[getVCard controller]');

	if(req && req.params && req.params.nameSlug) {

		Shop.findOne({'nameSlug': req.params.nameSlug}, 'name phone address email website').exec(function(err, shop) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid shop ID. ' + err};
			} else if (!shop) {
				status = 404;
				response = {message: 'Shop not found. '};
			} else {
				status = 200;
				response = getVCard(shop);
			}

			res.setHeader('Content-Type',"text/x-vcard")
			res.set('Content-disposition', 'attachment; filename=vcard.vcf');

			if(response.message) {
				console.log(status + ' ' + response.message);
				res.status(status).json(response);
			} else {
				res.status(status).send(response);
			}

		});
	}

}

module.exports.importFromJson = function(req, res){
	var file = path.join(__dirname, '..', 'files', 'shop.json');
	const lines = require('lines-adapter');

	const stream = fs.createReadStream(file);

	lines(stream, 'utf8')
	.on('data', line => {
		var obj = JSON.parse(line);

		Shop.findOne({name:obj.name}).exec(function(err, shop){
			if (err) throw err;
			if (shop) {
				return;
			}else{
				if(obj.name == "" || obj.name == "undefined" || typeof(obj.name) == "undefined") { return ;}
				var newShop = {};
				newShop.name = obj.name;
				newShop.email= obj.email || "change@me.com";
				newShop.website = obj.web;
				newShop.logo = obj.logo || null;
				if(obj.phone && obj.contact_country && obj.contact_country != "? undefined:undefined ?" && typeof(obj.contact_country) != "undefined" && obj.contact_country != "undefined"){

					if(obj.contact_country == 'België' || obj.contact_country == "Belgiè"){
						countryCode = "BE";
					}else if(obj.contact_country == "NL" || obj.contact_country.toLowerCase() == "nederland" || obj.contact_country == "Netherland" || obj.contact_country =="Nederlands"){
						countryCode = "NL";
					}else if(obj.contact_country == "Duitsland"){
						countryCode = "DE";
					}else{
						countryCode = "NL";
					}
					var phone = new PhoneNumber(obj.phone, countryCode);

					if(phone.isValid()){
						var telephone = {
							countryCode: phone.getRegionCode() || null,
							mobilePhone: phone.isMobile() ? phone.getNumber('significant') : null,
							landLine: phone.isMobile() ? null : phone.getNumber('significant'),
						}
						newShop.phone = telephone;
					}
				}
				newShop.openingHoursOld = {
					monday: obj.openingHoursMonday ? obj.openingHoursMonday : null,
					tuesday: obj.openingHoursTuesday ? obj.openingHoursTuesday : null,
					wednesday: obj.openingHoursWednesday ? obj.openingHoursWednesday : null,
					thursday: obj.openingHoursThursday ? obj.openingHoursThursday : null,
					friday: obj.openingHoursFriday ? obj.openingHoursFriday : null,
					saturday: obj.openingHoursSaturday ? obj.openingHoursSaturday : null,
					sunday: obj.openingHoursSunday ? obj.openingHoursSunday : null
				}

				if(obj.contact_street && obj.contact_housenr && obj.contact_postalcode && obj.contact_city && obj.contact_country){
					var number = obj.contact_housenr.match(/[^\d]+|\d+/g);
					newshop.address = {
						street: obj.contact_street,
						houseNumber: number[1],
						houseNumberSuffix: number[0],
						postalCode: obj.contact_postalcode,
						city: obj.contact_city,
						country: obj.contact_country
					}
				}
				newShop.isPremium = obj.isPremium || false;
				newShop.isPublished = obj.shopShop || true;
				newShop.isDropShipper = obj.isDropShipper || false;
				newShop.sendProducts = obj.sendProducts || false;
				newShop.showAllProducts = obj.showAllProducts || true;
				newShop.kvkNumber = obj.kvkNumber;
				newShop.btwNumber = obj.btwNumber;

				if(obj.whatsapp){
					var socialMedia = [];
					var whatsapp = {
						name: 'whatsapp',
						url: obj.whatsapp,
						username: obj.whatsapp
					}
					socialMedia.push(whatsapp);
					newShop.socialMedia = socialMedia;
				}

				if(obj.brands.length > 0){
					var brands = [];
					for(var i=0; i<obj.brands.length; i++){
						Brand.findOne({name: obj.brands[i]}).exec(function(err, brand){
							if(brand) {
								var brand = {
									_id: brand._id,
									name: brand.name,
									description: brand.description,
									images: brand.images,
									restricted: brand.restricted
								}
								brands.push(brand);
							}else{
								console.log("Brand not found:", obj.brands[i]);
							}
						})
					}
					if(brands.length == obj.brands.length){
						newShop.brands = brands;
					}
				}
				if(obj.reviews.length > 0){
					var reviews = [];
					for(var i =0; i< obj.reviews.length; i++){
						if(obj.reviews[i].name && obj.reviews[i].rating && obj.reviews[i].content){
							var review = {
								author: {
									name: obj.reviews[i].name,
								},
								rating: obj.reviews[i].rating,
								content: obj.reviews[i].content,
							}
							reviews.push(review);
						}
						if(reviews.length == obj.reviews.length) {
							newShop.reviews = reviews;
						}
					}
				}

				create(newShop, function(shop){
					if(shop){
						if(shop.website == "-") {
							shop.website = null;
						}
						Shop.create(shop, function(err, result){
							if (err) {
								console.log("Fout bij aanmaken:", err, shop);
								throw err
							};

							console.log("shop created!");
						})
					}
				})
			}
		});

}).on('end', () =>{
	console.log("ready");
	res.send("ok");
})
}

module.exports.uploadSliderPhoto = function(req, res){
	console.log("uploadSliderPhoto", req.body.shopId);
	if(req.body.shopId && req.files.file){
		upload.uploadFile(req.files.file, 'shop-sliders', null, function(err, result){
			if (err) {
				return res.status(500).json({message: 'error uploading file: ' + err})
			}

			res.status(200).json({file: result});
		})
	}
}

module.exports.uploadItemGridPhoto = function(req, res){
	console.log("uploadItemGridPhoto", req.body.shopId);
	if(req.body.shopId && req.files.file){
		upload.uploadFile(req.files.file, 'shop-image', null, function(err, result){
			if (err) {
				return res.status(500).json({message: 'error uploading file: ' + err})
			}
			res.status(200).json({file: result});
		})
	}
}

module.exports.uploadLogo = function(req, res){
	console.log("uploadLogoLight", req.body.shopId);
	console.log("uploadLogoLight", req.files);
	if(req.body.shopId && req.files.file){
		upload.uploadFile(req.files.file, 'logos', null, function(err, result){
			if (err) {
				console.log('erro', err);
				return res.status(500).json({message: 'error uploading file: ' + err})
			}
			console.log("result", result);
			res.status(200).json({file: result});
		})
	}
}

module.exports.addReviewItem = function(req, res) {
	console.log('[addReviewItem controller]');
	console.log(req.body.review);
	console.log(req.body.shopId);

	if(req.body.review && req.body.shopId){
		Shop.findOne({_id: req.body.shopId}).exec(function(err, shop){ //deze query gebruikte ook shopid
			shop.reviews.push(req.body.review);

			shop.save(function(err, result){
				if(!err){
					return res.status(200).json({file: result});
				}else{
					console.log("Error during saving shop: " + err);
					return res.status(200).json({message: "Error during saving shop: " + err});
				}
			})
		})	
	}else{
		console.log("no shopId and review found");
		return res.status(404).json({message: "no shopId and review found"})
	}
}

module.exports.updateReviewsItem = function(req, res) {
	console.log(req.body.reviewItem._id)
	if(req.body.reviewItem) {

		update(req.body, function(updatedReviewItem) {
			console.log(updatedReviewItem);
			if(updatedReviewItem) {

				ReviewsItem.findByIdAndUpdate(req.body.reviewItem._id, {$set: req.body.reviewItem}, {new: true}, function (err, reviewItem) {
					var status, response;

					if(err) {
						status = 400;
						response = {message: 'Review item could not be added. Error message: ' + err};
					} else if(!reviewItem) {
						status = 404;
						response = {message: 'Review item not found. '};
					} else {
						status = 200;
						response = {message: 'Review item updated. ', reviewItem};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);
				});
			}
		});
	}
};

module.exports.removeReviewsItem = function(req, res) {
	console.log('[removeReviewItem controller]');
	console.log(req.params.nameSlug);

//	if(req.body.reviewItem) {

	//	remove(req.body, function(removedReviewsItem) {
	//		console.log(removedReviewsItem);
	//		if(removedReviewsItem) {

		ReviewsItem.remove({nameSlug: req.params.nameSlug}, function (err) {
			var status, response;

	// if(req.body.params) {

	// 	ReviewsItem.findByIdAndRemove(req.body.reviewItem._id, {$set: req.body.reviewItem}, {new: true}, function (err, reviewItem) {
	// 		var status, response;

	if(err) {
		status = 500;
		response = {message: 'Error while deleting reviewItem. ' + err};
	} else if(!err) {
		status = 200;
		response = {message: 'Review item deleted. '};
	} 

	console.log(status + ' ' + response.message);
	if(err) {
		console.log(err.stack);
	}
			// Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
			res.status(status);

		});

//			}
//		});

//	}
};

module.exports.updateBrandImages = function(req, res){
	console.log("updateBrandImages");
	//get brand images and set the new images to the shop
	var limit = parseInt(req.query.limit);
	var skip = parseInt(req.query.skip);

	Shop.find().sort({_id: -1}).limit(limit).skip(skip).exec(function(err, shops){
		console.log("aantal shops", shops.length);
		async.forEach(shops, function(shop, shopCallback){
			console.log("bezig met shop", shop.name);
			if(shop.brands.length > 0){

				async.forEach(shop.brands, function(brand, brandCallback){
					var newImages = [];
					if(brand.images.length > 0 && brand.nameSlug){
						Brand.findOne({nameSlug: brand.nameSlug}).exec(function(err, freshBrand){
							if(freshBrand.images.length > 0){
								async.forEach(freshBrand.images, function(image, imageCallback){
									var image = {
										src: image.src,
										alt: image.alt
									}
									newImages.push(image);
									imageCallback();
								}, function(){
									//All afbeeldingen van dit merk gehad, opslaan en naar het volgende merk
									console.log("imageCallback voor merk", brand.name);
									brand.images = newImages;
									brandCallback();
								});
							}else{
								brandCallback();
							}
						})
					}else{
						brandCallback();
					}
				}, function(){
					//Alle merken zijn geweest, shop opslaan
					console.log("brandCallback voor shop", shop.nameSlug);
					shop.save(function(err, result){
						if(err){
							if(err.code == 16837){
								//cannot use the part (openingHours of openingHours.friday) to traverse the element
								shop.openingHours = {
									"monday": [],
									"tuesday" : [],
									"wednesday" : [],
									"thursday" : [],
									"friday" : [],
									"saturday" : [],
									"sunday": []
								}
								shop.save(function (err2, result2){
									if (err2) {
										console.log("error2", err2);
									}else{
										console.log("saved na error 16837", result2.name);
									}
								});
							}else{
								console.log("ERROR", err);
							}
						}else{
							console.log("shop is updated", result.name);
						}	
					})
					shopCallback();
				})
			}else{
				shopCallback();
			}
			
		}, function(){
			//alle shops zijn geweest
			console.log("done!");
			res.send("ok");
		})
	})
}

module.exports.getShopNewsItem = function(req, res){
	console.log("getShopNewsItem");

	if(req && req.query.shopId && req.query.newsItemSlug){
		Shop.aggregate([
			{$match: {_id: ObjectId(req.query.shopId)}},
			{$project: {
				news: {$filter: {
					input: '$news',
					as: 'newsItem',
					cond: {$eq: ['$$newsItem.nameSlug', req.query.newsItemSlug]}
				}}
			}}
			]).exec(function(err, shop){
				if(err)
					return res.status(500).json({message: 'Error finding shop news item: ' + err});
				if(!shop){
					return res.status(404).json({message: 'Shop news item not found'});
				}
				var newsItem = shop[0].news[0];
				return res.status(200).json({newsItem: newsItem})
			})
		}else{
			return res.status(500).json({message: 'No details provided'});
		}
	}


	module.exports.createUpgradeRecurringPayment = function(req, res){
		console.log("createUpgradeRecurringPayment");
		console.log("body", req.body);

	//first create an order with a payment

	var payment = {
		amount: 0.01,
		status: {
			status: 'Created'
		},
		shopId: req.body.shopId

	}
	
	req.session.upgrade = req.body.modules;
	req.session.shopId = req.body.shopId;

	req.session.save();
	
	if(!req.user.mollieId){
		//If the user hasn't an Mollie customer ID, create one and save it on the customer
		mollie.customers.create({
			name: req.user.firstName + (req.user.lastNamePrefix ? " " + req.user.lastNamePrefix: "" ) + " " + req.user.lastName,
			email: req.user.email
		}, function(customer){
			console.log("CUS", customer);
			User.findOne({_id: req.user._id}).exec(function(err, user){
				user.mollieId = customer.id;

				user.save(function(err, result){
					if(!err){
						return createRecurringPayment(result.mollieId);
					}
				});
			})
		})
	}else{
		return createRecurringPayment(req.user.mollieId);
	}

	function createRecurringPayment(customerId){
		console.log("createRecurringPayment", customerId);
		mollie.payments.create({
			amount: req.body.total,
			customerId: customerId,
			recurringType: 'recurring',
			description: 'PrismaNote shop upgrade'
		}, function(payment){
			console.log("payment", payment);
			res.status(200).json({message: "OK", payment});
		})


		// mollie.customers_subscriptions.withParentId(customerId).create({
		// 	amount: req.body.total,
		// 	times: null,
		// 	interval: '1 month',
		// 	description: 'PrismaNote shop upgrade',
		// 	webhookUrl: process.env.NODE_ENV == 'production' ? req.headers.origin + '/api/upgrade-shop' : settings.mollie.url + 'api/upgrade-shop'
		// }, function(subscription){
		// 	console.log("subscription", subscription);
		// 	res.status(200).json({message: "OK", subscription});
		// })
	}
}



module.exports.upgradeShop = function(req, res){
	console.log("upgradeShop", req.body, req.session.upgrade, req.session.shopId);

	return res.status(200);

	Shop.findOne({_id: req.body.shopId}).exec(function(err, shop){
		if(err){
			return res.send(500).json({message: "Error while finding shop: " + err})
		}

		shop.emailMarketingPremium = req.body.modules.crm;
		shop.countrIntegration = req.body.modules.countr;
		shop.printLabels = req.body.labels;

		shop.save(function(err, result){
			if(err){
				return res.send(500).json({message: "Error while saving shop: " + err})
			}
			if(req.body.modules.marketing && req.body.minutes > 0){
				socialPortalService.getUserSocialPortal(req.user._id, function(err, socialPortal){
					if(err){
						return res.send(500).json({message: "Error while finding portal: " + err})
					}
					socialPortal.marketingSupportMinutes = req.body.minutes;

					socialPortalService.saveSocialPortal(socialPortal._id, socialPortal, function(err, result){
						if(err){
							return res.send(500).json({message: "Error while saving portal: " + err})
						}
						return res.status(200).json({message: "OK"})
					})
				});
			}
		})
	})
}



// function myFunction(){
// 	collectionService.updateShopWithProperCollectionsByShopId("587f8b9912912517f8bc7885")
// 	.then((docs)=>{
// 		console.log("Updated Successfully : ",docs);
// 	},(err)=>{
// 		console.error("There seems to be some error : ",err);
// 	})
// }
// myFunction();





