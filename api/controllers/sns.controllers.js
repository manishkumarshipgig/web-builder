var path = require('path');
var Url = require('url');

var mailService = require(path.join(__dirname, '..', 'services', 'mail.service'));


module.exports.parseResult = function(req, res) {
    
    if(req.headers['x-amz-sns-message-type'] == 'SubscriptionConfirmation'){
        var url = Url.parse(req.body.SubscribeURL, true);
        if(url.protocol == 'https:'){
            var https = require("https");
            https.get(url.href, function(response){
                console.log("HTTPS RESPONSE", response);
            });
        }else{
            var http = require("http");
            http.get(url.href, function(response){
                console.log("HTTP RESPONSE", response);
            });
        }
        
    }else if(req.headers['x-amz-sns-message-type'] != 'UnsubscribeConfirmation'){
        var message = JSON.parse(req.body.Message);
        var data = {
            notificationType: message.notificationType,
            timestamp: message.mail.timestamp,
            smtpResponse: message.notificationType == 'Delivery' ? message.delivery.smtpResponse : null,
            rawData: JSON.stringify(req.body)
        };

        mailService.updateLog(message.mail.messageId, data, function(err, result){
            if(err){
                console.log("error",err);
            }
        })
    }
    //since response is not important here (amazon doesn't wait or listen to it) a simple 'ok' is enough
    return res.send("ok");
};

