const path = require('path');


const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const update = require(path.join(__dirname, '..', 'services', 'update.service'));
const upload = require(path.join(__dirname, '..', 'services', 'upload.service'));

module.exports.getReviewItem = function(req, res) {
	console.log('[getReviewItem controller]');

	if(req && req.params && req.params.nameSlug) {

		ReviewsItem.findOne({'nameSlug': req.params.nameSlug}).exec(function(err, reviewItem) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid Review item ID. ' + err};
			} else if (!reviewItem) {
				status = 404;
				response = {message: 'Review item not found. '};
			} else {
				status = 200;
				response = {message: 'OK', reviewItem};
			}

			console.log(status + ' ' + response.message);
			res.status(status).json(response);

		});
	}
};

module.exports.getReviewItems = function(req, res) {
	console.log('[getReviewItems controller]');
	// default values, because offset and limit are required
	var offset = 0, limit = 12;
	var filterBy, sortBy;

	if(req.query) {

		if(typeof req.query === 'string') {
			req.query = JSON.parse(req.query);
		}

		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			offset = parseInt(req.query.offset);
			delete req.query.offset;
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			limit = parseInt(req.query.limit);
			delete req.query.limit;
		}

		// if any filters are provided in the URL, parse them and add to the filtering params object.
		if(req.query.filter) {
			filterBy = filter(req.query.filter);
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
		if(req.query.sort) {
			sortBy = sort(req.query.sort);
		}

		if(req.query.lng && req.query.lat) {
			// TODO: find review items close to the given coordinates. 
		}

	}

	ReviewsItem.find(filterBy).sort(sortBy).skip(offset).limit(limit).exec(function(err, reviewItems) {
		var status, response;

		if(err) {
			status = 500;
			response = {message: 'Internal server error. ' + err};
		} else if(!reviewItems || reviewItems.length == 0) {
			status = 404;
			response = {message: 'No review items found with the given search criteria. '};
		} else {
			status = 200;
			response = {message: 'OK', reviewItems};
		}
		
		console.log(status + ' ' + response.message);
		res.status(status).json(response);

	});
};

module.exports.addReviewItem = function(req, res) {
	console.log('[addReviewItem controller]');
	console.log(req.body.data);
	console.log(req.body.data.reviewItem);

	if(req.body.data && req.body.data.reviewItem) {
		var reviewItem = req.body.data.reviewItem;
		//var nameSlug = req.body.data.shopSlug;

		var lastModified = Date.now();
		var creationDate = Date.now();
		var nameSlug = "nieuw-bericht-slug-die-moet-worden-overgezet-vanuit-de-titel";

		var newNewsItem = {
			name: reviewItem.name,
			nameSlug: reviewItem.nameSlug,
			summary: reviewItem.summary,
			content: reviewItem.content,
			lastModified: lastModified,
			creationDate: creationDate,
			content: reviewItem.content,
			brand: reviewItem.brand,
			isPublished: reviewItem.isPublished
		}

		create(newReviewItem, function(newReviewItemNew) {
			if(newReviewItemNew) {
				ReviewsItem.create(newReviewItemNew, function (err, reviewItem) {
					var status, response;
					if(err) {
						status = 400;
						response = {message: 'Reviews-item could not be added. Error message: ' + err};
					} else {
						status = 201;
						response = {message: 'OK', reviewItem};
					}
					
					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);
				});
			};
		})
	}
};

module.exports.uploadReviewsItemPhoto = function(req, res){
	console.log("uploadReviewsItemPhoto");
	console.log(req.body.reviewItemId);

	if(req.body.reviewItemId && req.files.file){
		upload.uploadFile(req.files.file, 'review', function(err, result){
			if (err) {
				return res.status(500).json({message: 'error uploading file: ' + err})
			}
			console.log("upload res", result);
			ReviewsItem.findOne({_id: req.body.reviewItemId}).exec(function(err, reviewItem){ //deze query gebruikte ook shopid
				if(reviewItem){
					var photo = {
						src: result,
						alt: reviewItem.name
					}
					reviewItem.images = [];
					reviewItem.images.push(photo);

					reviewItem.save(function(err, result){
						if(!err){
							return res.status(200).json({file: result});
						}else{
							console.log("Error during updating reviewItem: " + err);
							return res.status(200).json({message: "Error during updating reviewItem: " + err});
						}
					})
				}else{
					console.log("reviewItem not found");
					return res.status(404).json({message: "reviewItem not found"});
				}
			})
		})
	}else{
		console.log("no reviewItem id found");
		return res.status(404).json({message: "no reviewItem id found"});
	}
}

// einde upload foto controller

module.exports.updateReviewsItem = function(req, res) {
	console.log(req.body.reviewItem._id)
	if(req.body.reviewItem) {

		update(req.body, function(updatedReviewItem) {
			console.log(updatedReviewItem);
			if(updatedReviewItem) {

				ReviewsItem.findByIdAndUpdate(req.body.reviewItem._id, {$set: req.body.reviewItem}, {new: true}, function (err, reviewItem) {
					var status, response;

					if(err) {
						status = 400;
						response = {message: 'Review item could not be added. Error message: ' + err};
					} else if(!reviewItem) {
						status = 404;
						response = {message: 'Review item not found. '};
					} else {
						status = 200;
						response = {message: 'Review item updated. ', reviewItem};
					}

					console.log(status + ' ' + response.message);
					if(err) {
						console.log(err.stack);
					}
					res.status(status).json(response);

				});

			}
		});

	}
};

module.exports.removeReviewsItem = function(req, res) {
	console.log('[removeReviewItem controller]');
	console.log(req.params.nameSlug);

	ReviewsItem.remove({nameSlug: req.params.nameSlug}, function (err) {
		var status, response;

		if(err) {
			status = 500;
			response = {message: 'Error while deleting reviewItem. ' + err};
		} else if(!err) {
			status = 200;
			response = {message: 'Review item deleted. '};
		} 

		console.log(status + ' ' + response.message);
		if(err) {
			console.log(err.stack);
		}
// Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
		res.status(status);
	});
};

