const mongoose = require('mongoose');
const Product = mongoose.model('Product');

const _ = require('lodash');
const async = require("async");
const json2csv = require('json2csv');

module.exports.createArticleBaseClarity = function(req,res){
    console.log("createArticleBaseClarity");
    //res.send(req.query);
    if(!req.query || !req.query.brand || !req.query.lang){
        return res.status(500).json({message: "no brand or language provided"});
    }

    Product.find({'brand.nameSlug': req.query.brand}).exec(function(err, products){
        if(!products || products.length == 0){
            return res.status(404).json({message: 'No products found'})
        }
        var version = req.query.version ? req.query.version : "1.1.0";
        var lang = req.query.lang.toLowerCase();
        var articleBase = [];

        async.forEachOf(products, function(product, key, productsDone){

            var sex = function(){
                if(product.female && product.male){
                    if(product.kids){
                        return getStringByLanguage("KIDS", lang);
                    }
                    return getStringByLanguage("UNISEX", lang);
                }
                if(product.female){
                    if(product.kids){
                        return getStringByLanguage("GIRL", lang);
                    }
                    return getStringByLanguage("WOMEN", lang);
                }
                if(product.male){
                    if(product.kids){
                        return getStringByLanguage("BOY", lang);
                    }
                    return getStringByLanguage("MEN", lang);
                }
            }
            //Check for required fields
            if(!product.variants[0].ean || !product[lang] || !product[lang].name || !product.category || !product.suggestedRetailPrice){
                return productsDone();
            }

            var article = {
                ArticleNoSuppl: product.variants[0].ean,                                    //1
                ReferenceNo: product.variants[0].productNumber,                             //2
                DescriptionShort: product[lang].name,                                       //3
                ArticleKind: getStringByLanguage(product.category, lang),                   //4
                ProductLine: "",                                                            //5
                ProductGroup: getStringByLanguage(product.category, lang),                  //6
                ArticleGroup: sex(),                                                        //7
                Brand: product.brand.name,                                                  //8
                PackingUnit: 1,                                                             //9
                MinStock: "",                                                               //10
                MaxStock: "",                                                               //11
                Collection: "",                                                             //12
                PictureName: "",                                                            //13
                Discontinued: 0,                                                            //14
                PP: Math.round(product.suggestedRetailPrice * (1 - 0.55) * 100) /100,       //15
                RP: Math.round(product.suggestedRetailPrice * 100) /100,                    //16
                Catalogue: 1,                                                               //17
                IsLocked: 0,                                                                //18
                IsCoreAssortment: 0,                                                        //19
                DescriptionLong: "",                                                        //20
                Weight: 0,                                                                  //21
                Gurantee: 24,                                                               //22
                Dimension: 0,                                                               //23
                OrderInfo: ""                                                               //24
            }

            articleBase.push(article);

            productsDone();
        }, function(){
            if(articleBase.length == 0){
                console.log("no products to export");
                return res.status(422).json({message: "Er zijn geen producten die gexporteerd kunnen worden"});
            }
           
            var tsv = json2csv({
                data: articleBase, 
                del: ';', 
                hasCSVColumnTitle:false, 
                newLine: "\r\n",
                quotes: ''
            });
            var date = new Date();

            var dateString = date.getFullYear() + (date.getMonth()+1).toString() + date.getUTCDate();
            var time =  date.getHours().toString() + date.getMinutes();

            var headerLine = "GENERAL;NL;" + lang + ";EUR;" + version + ";" + dateString + ";" + time + ";EXEC;PrismaNote;Excellent_Electronics;00AA-00AA;"
            tsv = headerLine + "\r\n" + tsv;
            //res.send(JSON.stringify(tsv));
            var filename = "GENERAL_EXEC_NL_" +lang + "_EUR.DAT";
            res.set({"Content-Disposition":"attachment; filename=\"" + filename + "\""});
            res.send(tsv);
        })
    
    })
}

var keys = {
    MEN: {
        nl: "Heren",
        en: "Gentlemen",
        de: "Herren",
        fr: "Messieurs",
        sp: "Señores"
    },
    WOMEN: {
        nl: "Dames",
        en: "Ladies",
        de: "Damen",
        fr: "Mesdames",
        sp: "Damas",
    },
    BOY: {
        nl: "Jongens",
        en: "Boys",
        de: "Jungs",
        fr: "Garçons",
        sp: "Niños",
    },
    GIRL: {
        nl: "Meisjes",
        en: "Girls",
        de: "Mädchen",
        fr: "Filles",
        sp: "Chicas",
    },
    KIDS: {
        nl: "Kinderen",
        en: "Kids",
        de: "Kinder",
        fr: "Enfants",
        sp: "Niños",
    },
    UNISEX: {
        nl: "Unisex",
        en: "Unisex",
        de: "Unisex",
        fr: "Unisexe",
        sp: "Unisex",
    },
    WATCH: {
        nl: "Horloge",
        en: "Watch",
        de: "Uhr",
        fr: "Veille",
        sp: "Reloj",
    },
    STRAP: {
        nl: "Horlogeband",
        en: "Watch strap",
        de: "Armband",
        fr: "Bracelet de montre",
        sp: "Pulsera de reloj",
    },
    JEWEL: {
        nl: "Juweel",
        en: "Jewel",
        de: "Juwel",
        fr: "juweel",
        sp: "juweel",
    }
}


function getStringByLanguage(key, lang){
    var text = _.get(keys, key);
    if(!text){
        return key;
    }
    return text[lang];
}

module.exports.downloadLabel = function(req, res){
    var label = req.body.label;
    if(!label){
        return res.status(500).json({message: "No label details"});
    }

    createLabel(label, function(err, result){
        console.log("result!", result);
        res.writeHead(200, {'Content-Type': 'application/force-download','Content-disposition':'attachment; filename=etiket.pme'});
        res.end( result );   
    })

    function getValue(value){
        if(label[value]){
            return label[value];
        }else{
            return " ";
        }
    }

    function createLabel(label, callback){
        if(label.type == 'etiket1'){
            var content = 'FR"LAYOUT1"\r\n' +
                '?\r\n' +
                getValue('line1') + '\r\n' +
                getValue('line2') + '\r\n' +
                getValue('line3') + '\r\n' +
                getValue('line4') + '\r\n' +
                getValue('line5') + '\r\n' +
                getValue('line6') + '\r\n' +
                getValue('line7') + '\r\n' +
                getValue('line8') + '\r\n' +
                getValue('price') + '\r\n' +
                'P' + label.quantity + '\r\n';

            return callback(null, content);
        }else if(label.type == 'etiket2'){
            var content = 'FR"LAYOUT2"\r\n' +
                '?\r\n' +
                getValue('line1') + '\r\n' +
                getValue('line2') + '\r\n' +
                getValue('line3') + '\r\n' +
                getValue('line4') + '\r\n' +
                label.barcode + '\r\n' +
                'P' + label.quantity + '\r\n';

            return callback(null, content);
        }else if(label.type == 'etiket3'){
            var content = 'FR"LAYOUT3"\r\n' +
                '?\r\n' +
                getValue('line1') + '\r\n' +
                label.barcode + '\r\n' +
                'P' + label.quantity + '\r\n';

            return callback(null, content);
        }
    }   
}