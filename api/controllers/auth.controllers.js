const passport=require('passport');
const mongoose = require('mongoose');
const path = require('path');
const User = mongoose.model('User');

module.exports.loginUser = function(req,res, next) {
    passport.authenticate('local', function(err, user) {
        if(!user){
            return res.status(404).json({message: err});
        }
        else if (err) {
            return res.status(401).json({message: err});
        }

    if(user.mustResetPassword){
        console.info("User is found,but must reset his password");
        return res.status(200).json({user: user, mustReset: true})
    }else{
        req.logIn(user, function(err) {
            if (err) {
                return next(err);
            }
            res.status(200).send(user);
        });
    }

  })(req, res, next);
}

module.exports.logoutUser = function(req, res) {
     if(req.user.facebook){
        var user = {};
        user.facebook = {};
          User.findByIdAndUpdate(req.user._id, { $set: user }, { new: true }, function (err, userUpdated) {
           
        });
     }
    req.logout();
    res.status(200).json({message: 'Succesfully logged out'});
}

module.exports.activate = function(req, res){
    User.findOne({_id: req.body.id, verificationCode: req.body.code}).exec(function(err, user){
        if (err || !user) {
            return res.status(404).json({message: 'unexpected error or no user found with given parameters'});
        }

        if(user) {
            user.isVerified = true;     
            user.save(function (err, result){
                if (err) {
                    return res.status(500).json({message: 'Something went wrong'});
                }
                res.status(200).json({message: "user is activated", user: result});
            });
        }
    })
}

module.exports.checkLogin = function(req,res){
	if(req.user){
        res.status(200).json({user: req.user});
	}else{
		res.status(200).json({user: null, message: 'no user found'});
	}
}

module.exports.setNewPassword = function(req, res){
    if(req.body && req.body.username && req.body.newPassword1 && req.body.newPassword2){
        //last check if passwords are matching
        if(req.body.newPassword1 != req.body.newPassword2){
            return res.status(500).json({message: "Passwords are not matching!"})
        }else{
            User.findOne({username: req.body.username}).exec(function(err, user){
                if(err || !user){
                    return res.status(500).json({message: err ? err : "User not found"})
                }

                if(user.mustResetPassword){
                    User.resetPassword(user._id, req.body.newPassword1, false, function(err, result){
                        if(err){
                            return res.status(500).json({message: err})
                        }
                        req.logIn(result, function(err) {
                            if (err) {
                                return next(err);
                            }
                            return res.status(200).json({user: result, success: true});
                        });
                    })
                }else{
                    return res.status(500).json({message: 'No need to set a new password, please use the reset password function after logging in.'})
                }
            })
        }
    }else{
        return res.status(500).json({message: "Not all required data is provided"})
    }
}