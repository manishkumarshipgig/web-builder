const path = require('path');
const mongoose = require('mongoose');
const Shop = mongoose.model('Shop');
const Wholesaler = mongoose.model('Wholesaler');

const async = require('async');
const _ = require('lodash');
const sort = require(path.join(__dirname, '..', 'services', 'sort.service'));
const filter = require(path.join(__dirname, '..', 'services', 'filter.service'));
const create = require(path.join(__dirname, '..', 'services', 'create.service'));
const str = require(path.join(__dirname, '..', 'services', 'string.service'));


module.exports.getWholesaler = function(req, res) {
	console.log('[getWholesaler controller]');

	if(req && req.params && req.params.nameSlug) {
		Wholesaler.findOne({nameSlug: req.params.nameSlug}).exec(function(err, wholesaler) {
			// For some reason, this command returns an array with a single shop in Mongoose, whereas the Mongo native driver returns it as a shop document (as it should). This is not going to be fixed in the near future.
			var status, response;

			if(err) {
				return res.status(500).json({message: 'An error has occurred while trying to fetch this wholesaler.'})
			} else if(!wholesaler) {
				return res.status(404).json({message: 'No wholesaler found.'})
			} else {
				return res.status(200).json({message: 'OK', wholesaler})
			}
		});
	} else {
		console.log('No nameSlug provided for getWholesaler controller. ');
		res.status(400).json({message: 'No nameSlug provided for getwholesaler controller. '});
	}
};

module.exports.getWholesalers = function(req, res) {
	console.log('[getWholesalers controller]');
	console.log("params", req.query.filter);
	
	if(req && req.query) {

		if(typeof req.query === 'string') {

			req.query = JSON.parse(req.query);
		}
		let query = Wholesaler.find({});

		if(req.query.filter) {

			// // Aggregation queries can't use find() as a pipeline operator, but $match filters subdocuments in a similar fashion.
			// console.log(filter);

			// query["$match"] = req.query.filter;

			filterBy = filter(req.query.filter);
			console.log("Query",req.query);
			if(filterBy.isVerified == "showAll"){
				delete filterBy.isVerified;
			}
			else if(filterBy.isVerified == false){
				// Leave it False
				filterBy.isVerified = false;
			}
			else{
				if(filterBy.isVerified == null && filterBy.includeUnverified != true) {
					filterBy.isVerified = true;
				}

			}
			console.log("------ filter by -----",filterBy);

			if(filterBy.includeUnverified) {
				delete filterBy.includeUnverified;
			}
			console.log("------ filter by -----",filterBy);

			query = Wholesaler.find(filterBy);
		}


		if(req.query.offset && !isNaN(parseInt(req.query.offset))) {
			console.log("-----query 2-----");

			query.skip(parseInt(req.query.offset));
		}

		if(req.query.limit && !isNaN(parseInt(req.query.limit))) {
			console.log("-----query limit-----");

			query.limit(parseInt(req.query.limit));
		}

		// if any sorting parameters are provided in the URL, parse them and add to the Mongoose sorting object. 
		if(req.query.sort) {
			console.log("-----query sort-----");
			query.sort(sort(req.query.sort));
		}

		// if any field inclusion/exclusion (selection) params are provided in the request, parse them and add to the Mongoose sorting object. 
		if(req.query.select) {
			console.log("-----query select-----");
			// Aggregation queries don't have a 'select' operator, but $project does the same thing (in this context)
			query.select(req.query.select);
		} else {
			// Default setting: exclude some array fields to reduce result size. This should greatly reduce data traffic in the long run.
			query.select('-brands');
		}

		if(req.query.lng && req.query.lat) {
			console.log("-------query");
			// TODO: find shops close to the given coordinates.
		}

		

		query.exec(function(err, wholesalers) {
			var status, response;
			console.log(response);
			if(err) {
				status = 500;
				response = {message: 'Internal server error. ' + err};
			} else if(!wholesalers || wholesalers.length == 0) {
				status = 404;
				response = {message: 'No wholesalers found with the given search criteria. '};
			} else {
				

				status = 200;
				response = {message: 'OK', wholesalers};
			}
			
			console.log(status + ' ' + response.message);
			res.status(status).json(response);
		});
	} else {
		console.log('No params or invalid params provided for getWholesalers controller. ');
		res.status(400).json({message: 'No params or invalid params provided for getWholesalers controller. '});
	}
};

function syncCompanies(){
    Wholesaler.update({
    	isVerified: {
    		$exists: false
    	}
    },{
        isVerified: false
    })
    .exec(function(){
        console.log("[[[[[[[[[[[[[[[[[[[[[[[Wholesaler Updated]]]]]]]]]]]]]]]]]]]]]]]");
    })
}
// syncCompanies();


module.exports.addWholesaler = function(req, res) {
	console.log('[addWholesaler controller]');

	if(req.body && req.body.wholesaler) {
		create(req.body.wholesaler, function(newWholesaler) {
			newWholesaler.nameSlug = str.slugify(newWholesaler.name);
			Wholesaler.create(newWholesaler, function(err, wholesaler){
				var status, response;

				if(err) {
					console.error(err)
					status = 400;
					response = {message: 'Wholesaler could not be added. Error message: ' + err};
				} else {
					status = 201;
					response = {message: 'Wholesaler added. ', wholesaler};
				}

				console.log(status + ' ' + response.message);
				if(err) {
					console.log(err.stack);
				}
				res.status(status).json(response);
			});
		})
	}
};

module.exports.updateWholesaler = function(req, res) {
	console.log('[updateWholesaler controller]');
	
	if(req.body && req.body.wholesaler) {

		Wholesaler.findOneAndUpdate({nameSlug: req.params.nameSlug}, req.body.wholesaler, {upsert: true}, function(err, wholesaler) {
			let status, response;

			if(err) {
				status = 400;
				response = {message: 'Wholesaler could not be updated. Error message: ' + err};
			} else {
				status = 201;
				response = {message: 'Wholesaler updated. ', wholesaler: wholesaler};
			}
			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}
			res.status(status).json(response);
		});
	}
};

module.exports.removeWholesaler = function(req, res) {
	console.log('[removeWholesaler controller]');

	if(req.params && req.params.nameSlug) {

		Wholesaler.remove({nameSlug: req.params.nameSlug}, function(err, wholesaler) {
			var status, response;

			if(err) {
				status = 400;
				response = {message: 'Invalid wholesaler slug. ' + err};
			} else if(!wholesaler) {
				status = 404;
				response = {message: 'Wholesaler not found. '};
			} else {
				status = 204;
				response = {message: 'Wholesaler deleted. '};
			}

			console.log(status + ' ' + response.message);
			if(err) {
				console.log(err.stack);
			}
			// Not necessary to send a response message, because the HTTP status is 204 No Content (it's not useful to send the entire deleted object body back to the user).
			res.status(status);
		});

	}
}

//search shops funcit (kijken naar merk) matchEmailAdressesWithShops
//- loop to all shops and check validEmailadresses with list
//- shop die match hebben uit de lijst verwijderen
//- deze lijst terugsturen naar frontend
// lijst met shops toevoegen aan de scope.

module.exports.matchEmailAdressesWithShops = function(req, res) {
	if(req && req.body){
		console.log('[matchEmailAdressesWithShops controller]');	
		Shop.find({
			'brands.nameSlug': {
				$in: [req.body.brand.nameSlug]
			}
		}).exec(function(err, shops) {
			var status, response;

			if(err) {
				status = 500;
				response = {message: 'Internal server error. ' + err};
				return res.status(status).json(response);
			} else if(!shops || shops.length == 0) {
				status = 404;
				response = {message: 'No shops found with the given search criteria.'};
				return res.status(status).json(response);
			} else {

				var addresses = req.body.emailReceivers.added;
				async.forEachOf(shops, function(shop, i, shopDone){
					if(!shop || !shop.email){
						return shopDone();
					}
					var found = _.find(addresses, ['email', shop.email]);
					if(found){
						//shop verwijderen uit shops
						shops.splice(i, 1);
						shopDone();
					}else{
						shopDone();
					}

				},function(){ // this is the function which is called when the async call is done
					status = 200;
					response = {message: "OK", shops};
					
					return res.status(status).json(response);
				})
			}
		});
	} else {
		console.log('No params or invalid params provided for getShops controller. ');
		res.status(400).json({message: 'No params or invalid params provided for getShops controller. '});
	}

};