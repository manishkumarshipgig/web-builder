var path = require('path');
var mongoose = require('mongoose');


var ProductUpdateLog = new mongoose.Schema({
	productId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Product'
	},
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	status: {
		type: String,
		default: "Pending"
	}
});

module.exports = mongoose.model('ProductUpdateLog', ProductUpdateLog);
//console.log("-------> ProductUpdateLog is a newly created Collection in MongoDB <------- ");