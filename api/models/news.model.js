var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Image = require(path.join(__dirname, 'image.model'));

var languages = ['nl', 'en', 'fr', 'de', 'es'];

var NewsItem = new mongoose.Schema({
	name: {
		type: String,
		minlength: 4,
		maxlength: 40,
		required: true
	},
	nameSlug: {
		type: String,
		required: true
	},
	content: {
		type: String,
		required: true
	},
	summary: {
		type: String,
		minlength: 10,
		maxlength: 180
	},
	author: {
	 	_id: false,
	 	name: {
	 		type: String,
	 		required: false
	 	}
	},
	creationDate: {
		type: Date,
		required: true
	},
	lastModified: {
		type: Date,
		required: true
	},
	isPublished: {
		type: Boolean,
		required: true
	},
	publicationDate: Date,
	language: {
		type: String,
		enum: languages,
		validate: validators.isIn({message: 'Content language must be one of the following: (' + languages + ')'}, languages)
	},
	brand: {
		_id: false,
	 	name: {
	 		type: String,
			required: true
		 }
	},
	// 	nameSlug: {
	// 		type: String,
	// 		required: true
	// 	}
	// },
	images: [Image],
	geoloc: {
		type: {
			type: String,
			default:'Point'
		},
		coordinates: [Number]
	}
});

module.exports = mongoose.model('NewsItem', NewsItem);
