var mongoose = require('mongoose');

// sub-schema that can be reused in all models when needed.
var Translation = new mongoose.Schema({
	keyword: {
		type: String,
		required: true,
		unique: true
	},
	en_EN: {
		type: String
	},
	nl_NL: {
		type: String
	},
	de_DE: {
		type: String
	},
	fr_FR: {
		type: String
	},
	es_ES: {
		type: String
	},
	created: {
		type: Date,
		default: Date.now,
		required: true
	}
});

module.exports = mongoose.model('Translation', Translation);