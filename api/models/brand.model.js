var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));

var imageTypes = ['gents', 'ladies', 'kids', 'jewellery'];

var Brand = new mongoose.Schema({
	name: {
		type: String,
		minlength: 2,
		maxlength: 30,
		required: true
	},
	nameSlug: {
		type: String,
		required: true
	},
	website: {
		type: String,
	},
	email: {
		type: String,
		maxlength: 40,
		lowercase: true,
	},
	isFeatured: {
		type: Boolean,
	},
	foundingYear: {
		type: Number,
		min: 1500 // TODO: find out how Mongoose can automatically find the current year
	},
	description: {
		type: String
	},
	categories: {
		type: [String],
		default: ['watches'],
	},
	manufacturer: {
			type: String,
		
	},
	restricted: {
		type: Boolean,
		default: true
	},
	isVerified: {
		type: Boolean,
		default: false,
	},
	images: [{
		src: String,
		alt: String,
		type: {
			type: String,
			enum: imageTypes,
			validate: validators.isIn({message: 'The image type should be one of the following: (' + imageTypes + ')'}, imageTypes)
		}
	}]
});

module.exports = mongoose.model('Brand', Brand);