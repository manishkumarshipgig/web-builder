var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var ProductCollection = new mongoose.Schema({
	name: {
		type: String,
		minlength: 2,
		maxlength: 30,
		required: true
	},
	nameSlug: {
		type: String,
	},
	description: {
		type: String
	},
	// categories: {
	// 	type: [String],
	// 	default: ['watches'],
	// },
});

module.exports = mongoose.model('ProductCollection', ProductCollection);

