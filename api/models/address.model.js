var mongoose = require('mongoose');

// sub-schema that can be reused in all models when needed.
var Address = new mongoose.Schema({
	attn: { //to the attention of
		firstName: String,
		lastNamePrefix: String,
		lastName: String
	},
	street: {
		type: String,
		minlength: 2,
		maxlength: 40,
		//required: true
	},
	houseNumber: {
		type: Number,
	//	min: 1,
	//	max: 10000, // Some streets have house numbers running well in the thousands.
		//required: true
	},
	houseNumberSuffix: {
		type: String,
		maxlength: 10 // i.e. '112-BIS-A' or 'A/D WERF'
	},
	postalCode: {
		type: String,
		minlength: 4,
		maxlength: 7,
		//required: true
	},
	city: {
		type: String,
		minlength: 2,
		maxlength: 30,
		//required: true
	},
	country: {
		type: String,
		
		maxlength: 20,
		//required: true
	},
	countryCode: {
		type: String,

		minlength: 1,
		maxlength: 3,
		//required: true
	}
});

module.exports = Address;
