var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Image = require(path.join(__dirname, 'image.model'));

var strategyTypes = ['target-search', 'local-plump', 'store-promotion'];

var Task = new mongoose.Schema({
	nl: {
		name: String,
		description: String
	},
	en: {
		name: String,
		description: String
	},
	de: {
		name: String,
		description: String
	}, 
	fr: {
		name: String,
		description: String
	}, 
	es: {
		name: String,
		description: String
	},
	type: {
		type: String,
		//-required: true
	},
	defaultChecked: {
		type: Boolean,
		default: true,
	},
	mandatory: {
		type: Boolean,
		//-required: true,
	},
	fbPromotionSettings:{
		goal: String,
		targetGroup: String,
		promotingAreaAroundShop: Number,
		budget: Number,
		perDayOrSpread: String,
		promotionStartDate: Date,
		promotionEndDate: Date,
		presentation: String,
		alsoOnInstagram: Boolean,
		siteUrl: String,
		gender: Number,   //- not showed
		interests: [     // - Shailesh asking
			{
				interestId: String,
				interestName:String,
				audience_size: Number,
				description: String,
				topic: String
			}
		],
		headline: String,
		newsFeed: String,
		callToAction: String,
		budget: Number,
		distance: Number,
		ageMin: Number,
		ageMax: Number
	},
	contribution: {
		percent: Number,
		maxAmount: Number
	},
	startDate: Date,
	endDate: Date,
	datePlanned: Date,
	images: [Image],
	orderLink: String,
	comment: String,
    files: [{
        name: String,
        path: String
    }]
});

var Campaign = new mongoose.Schema({
	name: String,
	nameSlug: String,
	number: Number,
	type: {
		type: String
	},
	strategy: {
		type: String,
		enum: strategyTypes,
		validate: validators.isIn({message: 'The strategy type should be one of the following: (' + strategyTypes + ')'}, strategyTypes),
	},
	description: String,
	occasion: String,
	startDate: Date,
	endDate: Date,
	update: Boolean,
	brand: {
		_id: mongoose.Schema.Types.ObjectId,
		name: {
			type: String,
			required: true
		},
		images: [Image]
	},
	wholesaler: {
		_id: mongoose.Schema.Types.ObjectId,
		name: {
			type: String,
			required: true
		},
		email: String,
		telephone: String,
	},
	user: {
		_id: false,
		name: String,
		email: String
	},
	images: [Image],
	tasks: [Task],
	url: {
		type: String,
		validate: validators.isURL({message: 'The provided URL for this campaign is invalid'})
	},
	emailReceivers: {
		added: [{
			_id: false,
			email: String,
		}],
		suggested: [{
			_id: false,
			email: String,
			send: Boolean
		}]
	},
	creationDate: {
		type: Date,
		default: Date.now,
		required: true
	},
	ranking: Number,
	isVerified: {
		type: Boolean,
		default: false
	},
	verifiedDate: Date,
	isRestricted: {
		type: Boolean, // When true, look if the emailadress of the shop is one the mailaddresses in emailReceivers
		default: false
	},
	emailColor: String,
	estimationAmount: Number,
	estimationAmountPaid: Boolean
});

module.exports = mongoose.model('Campaign', Campaign);
