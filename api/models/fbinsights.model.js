var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');
var passportLocalMongoose = require('passport-local-mongoose');
var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));


var fbInsight = new mongoose.Schema({
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	amount_spent: {
		type: Number,
	},
	isFbInsight: {
		type: Boolean,
	},
	impressions: {
		type: Number,
		required: true
	},
	reach: {
		type: Number,
		required: true
	},
	cost_per_click:{
		type: Number,
		required: true
	},
	end_date: {
		type: Date,
		required: true
	},


	number: String
});

module.exports = mongoose.model('fbInsights', fbInsight);