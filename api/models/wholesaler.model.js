var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Image = require(path.join(__dirname, 'image.model'));
var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));


var Wholesaler = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    nameSlug: {
        type: String,
        required: true
    },
    description: String,
    address: Address,
    geoloc: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [Number]
    },
    email: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 40,
        validate: validators.isEmail({message: 'Email address is invalid. '}),
    },
    website: {
        type: String,
        minlength: 8,
		validate: validators.isURL({require_protocol: true, message: 'Website URL is invalid. '})
    },
    phone: Phone,
    accountHolder: String,
    bankAccountNumber: String,
    swiftCode: String,
    bicCode: String,
    logo: {
        src: String,
        alt: String
    },
    pvc: String,
    vatNumber: String,
    isVerified: Boolean,
    brands: [{
        _id: false,
        name: {
            type: String,
            required: true
        },
        nameSlug: String,
        description: String,
        images: [Image]
    }]
})

module.exports = mongoose.model('Wholesaler', Wholesaler);

