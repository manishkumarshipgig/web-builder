var path = require('path');
var mongoose = require('mongoose');
var validators = require('mongoose-validators');
var passportLocalMongoose = require('passport-local-mongoose');
var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));


var adminToken = new mongoose.Schema({
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	access_token: {
        type: String,
        required: true
	}
});

module.exports = mongoose.model('adminToken', adminToken);