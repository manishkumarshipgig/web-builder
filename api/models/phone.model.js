var mongoose = require('mongoose');

// sub-schema that can be reused in all models when needed.
var Phone = new mongoose.Schema({
	countryCode: {
		type: String,
		minlength: 1,
		maxlength: 6
	},
	mobilePhone: {
		type: String,
		minlength: 8,
		maxlength: 12
	},
	landLine: {
		type: String,
		minlength: 8,
		maxlength: 12
	},
	fax: {
		type: String,
		minlength: 8,
		maxlength: 12
	}
});

module.exports = Phone;
