var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Chat = new mongoose.Schema({
    _id: false,
	from: {
		type: String
	},
	to: {
	    type: String
	},
	message: String,
	timestamp: String,
	rawData: String
});

module.exports = mongoose.model('Chat', Chat);