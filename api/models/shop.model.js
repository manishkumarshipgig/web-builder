// FIXME: Need to work for the validations
var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Image = require(path.join(__dirname, 'image.model'));
var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));
var CollectionDetails = require(path.join(__dirname, 'collectionDetails.model'));

var socialMediaLinks = ['facebook','instagram','twitter', 'whatsapp', 'pinterest', 'googleplus', 'youtube'];

var languages = ['nl', 'en', 'fr', 'de', 'es'];

var Shop = new mongoose.Schema({
	name: {
		type: String,
		minlength: 3,
		maxlength: 100,
		required: true
	},
	nameSlug: {
		type: String,
		required: true
	},
	description: String,
	geoloc: {
		type: {
			type: String,
			default:'Point'
		},
		coordinates: [Number]
	},
	address: Address,
	email: {
		type: String,
		minlength: 6,
		maxlength: 40,
		lowercase: true,
		validate: validators.isEmail({message: 'Email address is invalid. '}),
		required: true
	},
	phone: Phone,
	whatsappEnabled: {
		type:Boolean,
		default: false
	},
	isPublished: {
		type: Boolean,
		required: true
	},
	webshopActive: {
		type: Boolean,
		required: true,
		default: true
	},
	accountHolder: String,
	bankAccountNumber : String,
	swiftCode: String,
	bicCode: String,
	home: [{
		type: {
			type: String
		},
		enabled: Boolean,
		src: String, // Video SRC
		title: String,
		content: String,
		container: Boolean,
		slides: [{
			src: String,
			alt: String,
			title: String,
			content: String,
			external: Boolean,
			buttonText: String,
			buttonLink: String
		}],
		height: String, // Slider height
		items: [{
			title: String,
			content: String,
			alt: String, // Grid item image alt text
			src: String, // Grid item image source
			url: String, // Grid item link URL
			external: Boolean, // Grid item links to an external website or to a prismanote state ref (ui-sref) ?
		}],
		usp: [String] // List of Unique selling points. Technically the same as columns but 'columns' is confusing for an unordered list.
	}],
	header: {
		topbar: {
			type: Boolean,
			default: true
		},
		searchbar: {
			type: Boolean,
			default: true
		},
		overlay: {
			type: Boolean,
			default: false
		},
		container: {
			type: Boolean,
			default: false
		}
	},
	customPages: [{
		name: {
			type: String,
			required: true
		},
		nameSlug: {
			type: String,
			required: true
		},
		content: {
			type: String,
			required: true
		},
		images: [Image]
	}],
	// Arrays with 2 items: 1 for opening time and 1 for closing time. These fields have to be numbers in case we want to find jewellers with certain opening times later on (for example: now open)
	openingHours: [{
		monday: [Number],
		tuesday: [Number],
		wednesday: [Number],
		thursday: [Number],
		friday: [Number],
		saturday: [Number],
		sunday: [Number],
	}],
	openingHoursOld: {
		monday: String,
		tuesday: String,
		wednesday: String,
		thursday: String,
		friday: String,
		saturday: String,
		sunday: String
	},
	products: [{
		_id: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Product'
		},
		stock: {
			type: Number,
			required: true
		},
		price: {
			type: Number
		},
		dropshippingPrice: {
			type: Number,
			default: 0
		},
		// If discount is set to 0 (numeric, not null/empty), then this item is not on sale
		discount: {
			type: Number,
			required: true
		},
		show: {
			type: Boolean,
			required: true,
			default: true
		},
		isBestseller: {
			type: Boolean,
			default: false,
			required: true
		},
		isFeatured: {
			type: Boolean,
			default: false,
			required: true
		},
		dateAdded: {
			type: Date,
			default: Date.now,
			required: true
		},
		countrId: String
	}],
	// Producten opsturen voor/naar andere juweliers als dropshippartner zij kunnen die producten dan op online leverbaar zetten in hun shop.
	isDropShipper: {
		type: Boolean,
		required: true,
		default: false
	},
	// Heeft webshop UI, bij false is dit een distributiecentrum
	isWebshop: {
		type: Boolean,
		required: true,
		default: false
	},
	// Producten zelf inpakken en opsturen naar klanten
	sendProducts:{
		type: Boolean,
		required: true
	},
	// Producten van eigen merken (die niet op voorraad zijn) weergeven als online leverbaar en laten dropshippen door andere shops
	showAllProducts: {
		type: Boolean,
		required: true
	},
	shippingCosts: {
		type: Number
	},
	logoLight: {
		src: String,
		alt: String
	},
	logoDark: {
		src: String,
		alt: String
	},
	proShopUrl:{
		type: String,
		minlength: 8,
		validate: validators.isURL({require_protocol: true, message: 'ProShop URL is invalid. '})
	},
	website:{ //- website link could be different from proShopUrl
		type: String,
		minlength: 8,
		// FIXME: Need to work for the validations
		// validate: validators.isURL({require_protocol: true, message: 'Website URL is invalid. '})
	},
	pvc: String,
	vatNumber: String,
	searchBarVisible: {
		type: Boolean,
		default: true
	},
	isPremium: {
		type: Boolean,
		required: true
	},
	emailMarketingPremium: {
		type: Boolean,
		default: false
	},
	countrIntegration: {
		type: Boolean,
		default: false,
	},
	socialMediaTool: {
		type: Boolean,
		default: true
	},
	printLabels:{
		type: Boolean,
		default: false,
	},
	proShop: {
		type: Boolean,
		default: false
	},
	socialMediaSupport: {
		type: Boolean,
		default: false
	},
	userLimit: {
		type: String,
		default: '15'
	},
	socialMedia: [{
		name: {
			type: String,
			enum: socialMediaLinks,
			validate: validators.isIn({message: 'social media link should be one of the following: (' + socialMediaLinks + ')'}, socialMediaLinks),
			required: true
		},
		url: {
			type: String,
			required: true
		},
		username: {
			type: String,
			required: true
		}
	}],
	brands: [{
		_id: false,
		name: {
			type: String,
			required: true
		},
		nameSlug: String,
		description: String,
		images: [Image],
		restricted: {
			type: Boolean,
			default: false
		}
	}],
	collections: [{
		_id: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Collection'
		},
		countrId: {
			type: String,
			default: null
		}
	}],
	reviews: [{
		author: {
			_id: false,
			name: {
				type: String,
				required: true
			}
		},
		creationDate: {
			type: Date,
			default: Date.now,
			// FIXME: Validation error
			// required: true
		},
		rating: {
			type: Number,
			min: 0,
			max: 5,
			required: true
		},
		content: {
			type: String,
			minlength: 20,
			required: true
		},
		// readDate = null if the review has not yet been read by the shop owner.
		readDate: {
			type: Date,
		},
		// responseDate = null if the review has not yet been answered by the shop owner.
		responseDate: {
			type: Date,
		},
		response: {
			type: String,
			minlength: 10
		}
	}],
	tasks: [{
		_id: false,
		name: {
			type:String,
			required: true
		},
		images: [Image],
		campaign: {
			_id: false,
			name: {
				type: String,
				//required: true
			},
			startDate: {
				type: Date,
				default: Date.now,
				// FIXME: Validation fails. Needs FIX
				// required: true
			},
			endDate: {
				type: Date,
				default: Date.now,
				// FIXME: Validation fails. Needs FIX
				// required: true
			}
		},
		downloaded: {
			type: Boolean,
			//required: true
		},
		success: {
			type: Boolean,
			//required: true
		},
		startDate: {
			type: Date,
			// FIXME: Validation fails. Needs FIX
			// required: true
		},
		endDate: {
			type: Date,
			// FIXME: Validation fails. Needs FIX
			// required: true
		},
		description: {
			type: String,
			required: true
		},
		type: {
			type: String,
			//required: true
		}
	}],
	news : [{
		name: {
			type: String,
			minlength: 4,
			maxlength: 40,
			required: true
		},
		nameSlug: {
			type: String,
			required: true
		},
		content: {
			type: String,
			required: true
		},
		summary: {
			type: String,
			minlength: 10,
			maxlength: 180
		},
		author: {
			_id: false,
			name: {
				type: String,
				required: false
			}
		},
		creationDate: {
			type: Date,
			// FIXME: Validation fails. Needs FIX
			// required: true
		},
		lastModified: {
			type: Date,
			// FIXME: Validation fails. Needs FIX
			// required: true
		},
		isPublished: {
			type: Boolean,
			// FIXME: Validation fails. Needs FIX
			// required: true
		},
		publicationDate: Date,
		language: {
			type: String,
			enum: languages,
			validate: validators.isIn({message: 'Content language must be one of the following: (' + languages + ')'}, languages)
		},
		brand: {
			_id: false,
			name: {
				type: String,
				required: true
			}
		},
		images: [Image],
		geoloc: {
			type: {
				type: String,
				default:'Point'
			},
			coordinates: [Number]
		}
	}],
	mollieId: String,
	countr: {
		username: String,
		accessToken: String,
		accessTokenExpiry: Date,
		refreshToken: String,
		refreshTokenExpiry: Date,
		trialExpiresAt: Date,
		countrId: String, //Countr shop ID
		taxId: String, //Countr default tax ID
		deviceId: String, // Countr device ID
		merchantId: String // Countr user ID
	}

});

module.exports = mongoose.model('Shop', Shop);