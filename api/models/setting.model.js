var mongoose = require('mongoose');


var Setting = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	value: {
		type: String,
		required: true
	},
	date: {
		type: Date,
		required: true,
		default: Date.now
	}
});

module.exports = mongoose.model('Setting', Setting);
