var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Image = require(path.join(__dirname, 'image.model'));
var CollectionDetails = require(path.join(__dirname, 'collectionDetails.model'));

var Collection = new mongoose.Schema({
	uploader: {
		_id: mongoose.Schema.Types.ObjectId,
		name: {
			type: String,
			required: true
		}
	},
	enabled: {
		type: Boolean,
		default: true
    },
	creationDate: {
		type: Date,
		default: Date.now,
		required: true
	},
	dateLastModified: {
		type: Date,
		default: Date.now,
	},
	// Views for the collection in general, views per nationality are being tracked in the CollectionDetails schema for that language
	views: {
		type: Number,
		min: 0,
		default: 0,
		required: true
	},
	// Translated collection details (name, nameSlug, short description etc.)
	nl: CollectionDetails,
	en: CollectionDetails,
	de: CollectionDetails,
	fr: CollectionDetails,
	es: CollectionDetails,

	isVerified: {
		type: Boolean,
		default: false
	},
	images: [Image],
	// categories: [{
	// 	type: String,
	// 	required: false
	// }],
	// brand: {
	// 	_id: mongoose.Schema.Types.ObjectId,
	// 	name: {
	// 		type: String,
	// 		required: true
	// 	},
	// 	nameSlug: {
	// 		type: String,
	// 		required: true
	// 	},
	// 	images: [Image]
	// },
	versions: [{
		author: [{
			name: String,
			_id: false
		}],
		date: {
			type: Date,
			required: true
		},
		concept: {
			type: Boolean,
			required: true
		},
		modifiedProperties: [mongoose.Schema.Types.Mixed],
		history: [{
			status: String,
			author: [{
				name: String,
				_id: false
			}],
			date: Date
		}]
	}]
}, {strict: false});

module.exports = mongoose.model('Collection', Collection);