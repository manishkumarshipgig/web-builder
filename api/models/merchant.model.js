var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));
var Shop = require(path.join(__dirname, 'shop.model'));

var Merchant = new mongoose.Schema({
	demo: {
		type: Boolean,
	},
	geoloc: {
		type: {
			type: String,
			default:'Point'
		},
		coordinates: [Number]
	},
	address: Address,
	name: {
		type: String,
		minlength: 3,
		maxlength: 30,
		required: true
	},
	nameSlug: {
		type: String,
		required: true
	},
	description: String,
	website: {
		type: String,
		minlength: 8,
		validate: validators.isURL({message: 'Website URL is invalid. '})
	},
	email: {
		type: String,
		minlength: 6,
		maxlength: 40,
		lowercase: true,
		validate: validators.isEmail({message: 'Email address is invalid. '}),
		required: true
	},
	phone: Phone,
	whatsappEnabled: {
		type: Boolean,
		default: false
	},
	// What are the requirements for KvK and BTW numbers? Does this also work for companies in Germany, Spain, etc.?
	kvkNumber: String,
	btwNumber: String,
	shops: [Shop]
});

module.exports = mongoose.model('Merchant', Merchant);
