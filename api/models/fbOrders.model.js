var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');
var passportLocalMongoose = require('passport-local-mongoose');
var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));


var fbOrder = new mongoose.Schema({
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	payId: {
		type: String,
	},
	user: [{
		_id: false,
		firstName: {
			type: String,
			required: true
		},
		lastName: {
			type: String,
			required: true
		},
		email: {
			type: String
		}
	}],
	shop: [{
		_id: false,
		noShop: {
			type: Boolean,
			default: false
		},
		name: String,
		nameSlug: String,
		address: Address,
		emailAddress: String
	}],
	status: [{
		status: {
			type: String,
			required: true
		},
		comment: String,
		date: {
			type: Date,
			default: Date.now,
			required: true
		}
	}],
	paymentid: String,
	amount: Number,
	handled: Boolean,
	campaign: {
		_id: mongoose.Schema.Types.ObjectId,
		name: String
	},
	number: String
});

module.exports = mongoose.model('fbOrder', fbOrder);