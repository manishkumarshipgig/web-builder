var mongoose = require('mongoose');

var duplicateProducts = new mongoose.Schema({
	product_id: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Product'
	},
	item: {
		type: Object
	},
	mergedTo: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Product'
	}
}, {strict: false});


module.exports = mongoose.model('DuplicateProducts', duplicateProducts);