var mongoose = require('mongoose');

var oldProduct = new mongoose.Schema({
	product_id: {
		type: String
	},
	item: {
		type: Object
	},
	added_by: {
		type: Object
	}
}, {strict: false});

module.exports = mongoose.model('OldProducts', oldProduct);