var path = require('path');
var mongoose = require('mongoose');
var validators = require('mongoose-validators');
var passportLocalMongoose = require('passport-local-mongoose');

var externalApiToken = new mongoose.Schema({
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	email: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
	}
});

module.exports = mongoose.model('externalApiToken', externalApiToken);