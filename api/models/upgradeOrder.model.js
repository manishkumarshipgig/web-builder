var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var eventTypes = ['invoice-generated', 'modules-changed', 'payment-failed', 'payment-created', 'subscription-cancelled', 'subscription-cancelled-and-created-new', 'payment-charged-back']

var UpgradeOrder = new mongoose.Schema({
    shopId: mongoose.Schema.Types.ObjectId,
    modules: [
        {
            name: {
                type: String,
                required: true
            },
            active: {
                type: Boolean,
                default: false
            },
            amount: Number,
            value: String,
            dateChanged: {
                type: Date,
                default: Date.now,
			    required: true
            } 
        }
    ],
    status: [
        {
            date: {
                type: Date,
                default: Date.now,
                required: true
            },
            comment: String,
            event: {
                type: String,
                enum: eventTypes,
                validate: validators.isIn({message: 'The module name should be one of the following: (' + eventTypes + ')'}, eventTypes),
                required: true
            },
            number: String
        }
    ],
    number: String,
    active: Boolean,
    subscriptionId: String,
    payments: [{
        status: String,
        method: String,
        description: String,
        paidDatetime: Date
    }]
});

module.exports = mongoose.model('UpgradeOrder', UpgradeOrder);