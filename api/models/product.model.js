var path = require('path');
var OldProducts = require('./oldProducts.model');
var ProductSuggestions = require('./productSuggestions.model');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Image = require(path.join(__dirname, 'image.model'));
// var productCollection = require(path.join(__dirname, 'productCollection.model'));

var watchTypes = ['WRIST', 'POCKET', 'NURSE'];
var watchIndications = ['CHRONO_MULTI', 'ANALOG', 'DIGITAL', 'ANALOG_DIGITAL'];
var watchIndexTypes = ['DOTS', 'ROMAN', 'ARABIC','STRIPES','ORNAMENTAL_DIAMONDS', 'REAL_DIAMONDS', 'NONE'];
var jewelleryTypes = ['BROOCH', 'CHOKER', 'PENDANT', 'NECKLACE', 'PEARL_NECKLACE', 'TENNIS_NECKLACE', 'STUD_EARRINGS', 'HOOP_EARRINGS', 'CREOLE_EARRINGS', 'RING', 'COMBINATION_RING', 'RING_WITH_PEARL', 'RING_WITH_GEM', 'SET', 'BRACELET', 'TENNIS_BRACELET', 'SLAVE_BRACELET'];

var Case = new mongoose.Schema({
	_id: false,
	shape: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch case shape contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	},
	size: {
		type: Number,
		max: 150,
	},
	depth: {
		type: Number,
		max: 50,
	},
	material: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch case material contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g),
	},
	glassMaterial: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for glass material contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g),
	},
	color: [{
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch case color contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	}],
	designItem: {
		type: [String],
		default: []
	}
});

var Dial = new mongoose.Schema({
	_id: false,
	color: [{
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch dial color contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	}],
	pattern: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch dial pattern contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	},
	print: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch dial print contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	},
	index: {
		type: String,
		// validate: validators.isIn({skipNull: true}, watchIndexTypes),
	}
});

var Strap = new mongoose.Schema({
	_id: false,
	model: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch strap model contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	},
	width: {
		type: Number,
		max: 150,
	},
	material: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch strap material contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	},
	color: [{
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch strap color contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	}],
	// Strap pattern is deprecated
	print: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch strap print contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	},
	studs: {
		type: Boolean,
		default: false,
		required: true
	},
	clasp: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch strap clasp contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	},
	wristPermimeter: {
		type: Number,
		min: 150,
		max: 250
	}
});

var Jewel = new mongoose.Schema({
	_id: false,
	// NOTE: fields material and color are arrays in the original code. Changed for testing purposes.
	// NOTE: ASK whether material and color fields are to be kept arrays or convert like strap and case to object type.
	material: {
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch strap material contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	},
	color: [{
		type: String,
		// validate: validators.matches({skipNull: true, message: 'The provided value for watch strap color contains invalid characters'}, /\b[A-Z]+(_[A-Z]+)*\b/g)
	}],
	type: {
		type: String,
		// validate: validators.isIn({message: 'Jewellery type should be one of the predefined options: ' + jewelleryTypes}, jewelleryTypes)
	},
	height: {
		type: Number
	},
	width: {
		type: Number
	},
	depth: {
		type: Number
	},
	diameter: {
		type: Number
	},
	weight: {
		type: Number
	},
	chain: {
		type: String
	},
	clasp: {
		type: String
	},
	shape: {
		type: String
	},
	gloss: {
		type: Boolean
	},
	goldPurity: {
		type: Number
	},
	gemColor: {
		type: String
	},
	gemShape: {
		type: String
	}
});

var Watch = new mongoose.Schema({
	_id: false,
	type: {
		type: String,
		validate: validators.isIn({skipNull: true, message: 'Watch type should be one of the predefined options: ' + watchTypes}, watchTypes)
	},
	indication: {
		type: String,
		validate: validators.isIn({skipNull: true, message: 'Watch indication should be one of the predefined options: ' + watchIndications}, watchIndications)
	},
	hasSwissMovement: {
		type: Boolean,
		default: false
	},
	hasDateFunction: {
		type: Boolean,
		default: false
	},
	waterproofLevel: {
		type: Number,
		min: 0,
		max: 3000,
		default: 0
	},
	isNickelFree: {
		type: Boolean,
		default: false
	},
	isAntiAllergy: {
		type: Boolean,
		default: false
	},
	hasLightFunction: {
		type: Boolean,
		default: false
	},

	// Smartwatch functions are not predefined (yet), so any translatable tag (string) can be added to this array.
	isSmartwatch: {
		type: Boolean,
		default: false
	},
	smartWatchFunctions: {
		type: [String],
		default: []
	},

	// Sub schemas for specific parts of the watch
	case: Case,
	dial: Dial,
	strap: Strap
});

var ProductDetails = new mongoose.Schema({
	_id: false,
	name: {
		type: String,
		index: true

	},
	nameSlug: {
		type: String,
		index: true

	},
	longDescription: {
		type: String
	},
	shortDescription: {
		type: String
	},
	views: {
		type: Number,
		min: 0,
		default: 0
	}
});

// var oldProduct = new mongoose.Schema({
// 	name: { // deprecated, we use translated names now
// 		type: String,

// 	},
// 	nameSlug: { // deprecated, we use translated nameSlugs now
// 		type: String
// 	},

// 	uploader: {
// 		_id: mongoose.Schema.Types.ObjectId,
// 		name: {
// 			type: String,
// 			required: true
// 		}
// 	},
// 	enabled: {
// 		type: Boolean,
// 		default: true
// 	},

// 	uploadDate: {
// 		type: Date,
// 		default: Date.now,
// 		required: true
// 	},

// 	dateLastModified: {
// 		type: Date,
// 		default: Date.now,
// 	},
// 	// Views for the product in general, views per nationality are being tracked in the ProductDetails schema for that language
// 	views: {
// 		type: Number,
// 		min: 0,
// 		default: 0,
// 		required: true
// 	},

// 	// Translated product details (name, nameSlug, short description etc.)
// 	nl: ProductDetails,
// 	en: ProductDetails,
// 	de: ProductDetails,
// 	fr: ProductDetails,
// 	es: ProductDetails,

// 	male: {
// 		type: Boolean,
// 		default: true
// 	},
// 	female: {
// 		type: Boolean,
// 		default: false
// 	},

// 	kids: {
// 		type: Boolean,
// 		default: false
// 	},

// 	isBestseller: {
// 		type: Boolean,
// 		default: false,
// 		required: true
// 	},

// 	isVerified: {
// 		type: Boolean,
// 		default: false
// 	},

// 	verifyLater: {
// 		type: Boolean,
// 		default: false
// 	},

// 	containsFilterInfo: {
// 		type: Boolean,
// 		default: true,
// 		//required: true (deze staat nu nog even uit omdat de bestaande geuploade producten een de eigenschap 'true' moeten krijgen)
// 	},

// 	suggestedRetailPrice: {
// 		type: Number
// 	},

// 	images: [Image],

// 	brand: {
// 		_id: mongoose.Schema.Types.ObjectId,
// 		name: {
// 			type: String,
// 			required: true
// 		},
// 		nameSlug: {
// 			type: String,
// 			required: true
// 		},
// 		images: [Image]
// 	},

// 	collections: [{
// 		_id: mongoose.Schema.Types.ObjectId,
// 		name: {
// 			type: String,
// 			required: true
// 		},
// 		nameSlug: {
// 			type: String,
// 			required: true
// 		},
// 		description: {
// 			type: String,
// 		}
// 	}],

// 	category: {
// 		type: String,
// 		required: true
// 	},

// 	// Product details for the specified product category. One of these should contain the corresponding sub schema, the others should be null. More types can be added.
// 	watch: Watch,
// 	strap: Strap,
// 	jewel: Jewel,

// 	variants: [{
// 		_id: false,
// 		ean: {
// 			type: String,
// 			minlength: 5,
// 			maxlength: 13,
// 			index: true

// 			//validate: validators.isNumeric({message: 'Invalid EAN: EAN codes consist of 13 digits'}),
// 			//required: true
// 		},
// 		sku: {
// 			type: String,
// 			index: true

// 		},
// 		productNumber: {
// 			type: String,
// 			index: true
// 			//required: true
// 		},
// 		size: {
// 			type: Number
// 		}
// 	}],
// 	versions: [{
// 		author: [{
// 			name: String,
// 			_id: false
// 		}],
// 		date: {
// 			type: Date,
// 			required: true
// 		},
// 		concept: {
// 			type: Boolean,
// 			required: true
// 		},
// 		modifiedProperties: [mongoose.Schema.Types.Mixed],
// 		history: [{
// 			status: String,
// 			author: [{
// 				name: String,
// 				_id: false
// 			}],
// 			date: Date
// 		}]
// 	}]
// }, {strict: false});




var Suggestion = new mongoose.Schema({
	name: { // deprecated, we use translated names now
		type: String,

	},
	nameSlug: { // deprecated, we use translated nameSlugs now
		type: String
	},

	uploader: {
		_id: mongoose.Schema.Types.ObjectId,
		name: {
			type: String,
			required: true
		}
	},

	enabled: {
		type: Boolean,
		default: true
	},

	uploadDate: {
		type: Date,
		default: Date.now,
		required: true
	},

	dateLastModified: {
		type: Date,
		default: Date.now,
	},
	// Views for the product in general, views per nationality are being tracked in the ProductDetails schema for that language
	views: {
		type: Number,
		min: 0,
		default: 0,
		required: true
	},

	// Translated product details (name, nameSlug, short description etc.)
	nl: ProductDetails,
	en: ProductDetails,
	de: ProductDetails,
	fr: ProductDetails,
	es: ProductDetails,

	male: {
		type: Boolean,
		default: true
	},
	female: {
		type: Boolean,
		default: false
	},

	kids: {
		type: Boolean,
		default: false
	},

	isBestseller: {
		type: Boolean,
		default: false,
		required: true
	},

	isVerified: {
		type: Boolean,
		default: false
	},
	verifyLater: {
		type: Boolean,
		default: false
	},
	containsFilterInfo: {
		type: Boolean,
		default: true,
		//required: true (deze staat nu nog even uit omdat de bestaande geuploade producten een de eigenschap 'true' moeten krijgen)
	},
	suggestedRetailPrice: {
		type: Number
	},
	images: [Image],
	brand: {
		_id: mongoose.Schema.Types.ObjectId,
		name: {
			type: String,
			required: true
		},
		nameSlug: {
			type: String,
			required: true
		},
		images: [Image]
	},
	collections: [{
		_id: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Collection'
		}
		
	}],
	category: {
		type: String,
		required: true
	},
	// Product details for the specified product category. One of these should contain the corresponding sub schema, the others should be null. More types can be added.
	watch: Watch,
	strap: Strap,
	jewel: Jewel,
}, {strict: false});

var Product = new mongoose.Schema({
	/***** Deprecated ***/
	// suggestions: [{
	// 	item: {
	// 		type:Suggestion
	// 	},
	// 	suggester: {
	// 		id: {
	// 			type: mongoose.Schema.Types.ObjectId
	// 		},
	// 		name: {
	// 			type: String
	// 		},
	// 		usertype: {
	// 			type: String
	// 		}
	// 	},
	// 	dateOfSuggestion: new Date()
	// }],

	name: { // deprecated, we use translated names now
		type: String,

	},
	nameSlug: { // deprecated, we use translated nameSlugs now
		type: String
	},

	uploader: {
		_id: mongoose.Schema.Types.ObjectId,
		name: {
			type: String,
			required: true
		}
	},
	enabled: {
		type: Boolean,
		default: true
	},

	uploadDate: {
		type: Date,
		default: Date.now,
		required: true
	},

	dateLastModified: {
		type: Date,
		default: Date.now,
	},
	lastSuggestedDate: {
		type: Date
	},
	priorityKey: {
		type: Boolean
	},
	// Views for the product in general, views per nationality are being tracked in the ProductDetails schema for that language
	views: {
		type: Number,
		min: 0,
		default: 0,
		required: true
	},

	// Translated product details (name, nameSlug, short description etc.)
	nl: ProductDetails,
	en: ProductDetails,
	de: ProductDetails,
	fr: ProductDetails,
	es: ProductDetails,

	male: {
		type: Boolean,
		default: true
	},
	female: {
		type: Boolean,
		default: false
	},

	kids: {
		type: Boolean,
		default: false
	},

	isBestseller: {
		type: Boolean,
		default: false,
		required: true
	},

	isVerified: {
		type: Boolean,
		default: false
	},

	verifyLater: {
		type: Boolean,
		default: false
	},

	containsFilterInfo: {
		type: Boolean,
		default: true,
		//required: true (deze staat nu nog even uit omdat de bestaande geuploade producten een de eigenschap 'true' moeten krijgen)
	},

	suggestedRetailPrice: {
		type: Number
	},

	images: [Image],

	brand: {
		_id: mongoose.Schema.Types.ObjectId,
		name: {
			type: String,
			required: true
		},
		nameSlug: {
			type: String,
			required: true
		},
		images: [Image]
	},

	collections: [{
		_id: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Collection'
		}
	}],

	category: {
		type: String,
		required: true
	},

	// Product details for the specified product category. One of these should contain the corresponding sub schema, the others should be null. More types can be added.
	watch: Watch,
	strap: Strap,
	jewel: Jewel,

	variants: [{
		_id: false,
		ean: {
			type: String,
			minlength: 5,
			maxlength: 13,
			index: true

			//validate: validators.isNumeric({message: 'Invalid EAN: EAN codes consist of 13 digits'}),
			//required: true
		},
		sku: {
			type: String,
			index: true

		},
		productNumber: {
			type: String,
			index: true
			//required: true
		},
		size: {
			type: Number
		}
	}],
	oldProducts :[{
		type: [mongoose.Schema.Types.ObjectId],
		ref: 'OldProducts'
	}],
	duplicateProducts :[{
		type: [mongoose.Schema.Types.ObjectId],
		ref: 'DuplicateProducts'
	}],
	suggestions: [
	{
		type: [mongoose.Schema.Types.ObjectId],
		ref: 'productSuggestings'
	}
	],
	// {
	// 	// type: [Suggestion]
	// 	type: [mongoose.Schema.Types.ObjectId],
	// 	// default: [],
	// 	ref: "productSuggestings"
	// },
	versions: [{
		author: [{
			name: String,
			_id: false
		}],
		date: {
			type: Date,
			required: true
		},
		concept: {
			type: Boolean,
			required: true
		},
		modifiedProperties: [mongoose.Schema.Types.Mixed],
		history: [{
			status: String,
			author: [{
				name: String,
				_id: false
			}],
			date: Date
		}],

	}]
}, {strict: false});

Product.set('toJSON', { getters: true, virtuals: false });

Product.post('find', async function(docs) {
	//	console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Populating Collections Automatically++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	for (let doc of docs) {
		if (doc.isPublic) {
			await doc.populate('collections._id').execPopulate();
		}
	}
});

// Product.post('findOne', async function(doc) {
// 	await doc.populate('collections._id').execPopulate();
// });





// PRE SAVE CODE FOR Product.containsFilterInfo

// Product.pre('save', function(doc, next) {
// 	setTimeout(function() {
// 		console.log('AUTOMATIC TRIGGER CALL');
// 		console.log("Document = ",doc);
// 		console.log("containsFilterInfo = ",doc.containsFilterInfo);
// 		next();
// 	});
// });


function validateProductForContainsFilterInfo(item){

	console.log("[[[[[[[[[[[validateProductForContainsFilterInfo]]]]]]]]]]]");
	console.log(item);
	console.log("IN TEST 1")
	if(!("en" in item || "de" in item || "fr" in item || "es" in item )) return false;
	console.log("TEST 1 PASS");


	// name nameSlug longDescription and shortDescription
	var isValidForLang = false;
	if("en" in item){
		console.log("IN 'EN'")
		if(item.en.name != undefined && item.en.nameSlug != undefined && item.en.shortDescription != undefined && item.en.longDescription != undefined)
			isValidForLang = true;
	}
	if("de" in item){
		console.log("IN 'DE'")

		if(item.de.name != undefined && item.de.nameSlug != undefined && item.de.shortDescription != undefined && item.de.longDescription != undefined)
			isValidForLang = true;
	}
	if("fr" in item){
		console.log("IN 'FR'")

		if(item.fr.name != undefined && item.fr.nameSlug != undefined && item.fr.shortDescription != undefined && item.fr.longDescription != undefined)
			isValidForLang = true;
	}
	if("es" in item){
		console.log("IN 'ES'")

		if(item.es.name != undefined && item.es.nameSlug != undefined && item.es.shortDescription != undefined && item.es.longDescription != undefined)
			isValidForLang = true;
	}

	if(!isValidForLang) return false;
	console.log("TEST 2 PASS");

	// brand
	if(!item.brand){
		console.log("Brand Does Not exists ",item.brand);
		return false;
	}
	/*else{
	console.log("BRAND REJECTED ",item.brand);
	return false;
}*/
console.log("TEST 3 PASS");

// Category
if("category" in item){
	if(item.category == undefined) return false;
}
else return false;
console.log("TEST 4 PASS");

// sex_audience
if("kids" in item || "male" in item || "female" in item){
	if(!(item.kids || item.male || item.female)) return false;
}
else return false;
console.log("TEST 5 PASS");
// Product Number
if(!("productNumber" in item || item.productNumber == undefined)) return false;
console.log("TEST 6 PASS");

// Images
if("images" in item){
	if(item.images.length < 1) return false;
}
else return true;
console.log("TEST 7 PASS");


if(item.category == 'WATCH'){
	console.log("IN WATCH", )
	// watch-type
	if("type" in item.watch){
		if(item.watch.type == undefined) return false;
	}
	else return false;
	console.log("TEST 8 PASS");
	// watch-case-size
	if("case" in item.watch){
		console.log("case");
		if(item.watch.case.size == undefined || item.watch.case.size == "" || item.watch.case.size == null) return false;
		console.log("item.watch.case.size");
		if(item.watch.case.shape == undefined || item.watch.case.shape == "" || item.watch.case.shape == null) return false;
		console.log("item.watch.case.shape");
		if(item.watch.case.depth == undefined || item.watch.case.depth == "" || item.watch.case.depth == null) return false;
		console.log("item.watch.case.depth");
		// if(item.watch.case.material == undefined || item.watch.case.material == "" || item.watch.case.material == null) return false;
		// console.log("item.watch.case.material");
		// if(item.watch.case.color == undefined || item.watch.case.color == "" || item.watch.case.color == null) return false;
		// console.log("item.watch.case.color");
		// if(item.watch.case.glassMaterial == undefined || item.watch.case.glassMaterial == "" || item.watch.case.glassMaterial == null) return false;
		// console.log("item.watch.case.glassMaterial");
		// if(item.watch.case.waterproofLevel == undefined || item.watch.case.waterproofLevel == "" || item.watch.case.waterproofLevel == null) return false;

		// if(item.watch.case.isNickelFree == undefined || item.watch.case.isNickelFree == "" || item.watch.case.isNickelFree == null) return false;


	}
	else {
		return false
	};


	if("dial" in item.watch){
		if(item.watch.dial.color == undefined || item.watch.dial.color == "" || item.watch.dial.color == null) return false;
		console.log("item.watch.dial.color");
		// if(item.watch.dial.pattern == undefined || item.watch.dial.pattern == "" || item.watch.dial.pattern == null) return false;
		// console.log("item.watch.dial.pattern");
		// if(item.watch.dial.print == undefined || item.watch.dial.print == "" || item.watch.dial.print == null) return false;
		// console.log("item.watch.dial.print");
	}
	else {return false;}
	console.log("item.watch.dial");

	// if(item.watch.indication == undefined || item.watch.indication == "" || item.watch.indication == null) return false;
	// console.log("item.watch.indication");

}
else if(item.category == "STRAP"){
	console.log("IN STRAP");

	if(item.strap.model == undefined || item.strap.model == "" || item.strap.model == null) return false;
	console.log("item.strap.model");
	if(item.strap.material == undefined || item.strap.material == "" || item.strap.material == null) return false;
	console.log("item.strap.material");

	if(item.strap.color == undefined || item.strap.color == "" || item.strap.color == null) return false;
	console.log("item.strap.color");

	if(item.strap.clasp == undefined || item.strap.clasp == "" || item.strap.clasp == null) return false;
	console.log("item.strap.clasp");

	if(item.strap.studs == undefined || item.strap.studs == "" || item.strap.studs == null) return false;
	console.log("item.strap.studs");

	if(item.strap.print == undefined || item.strap.print == "" || item.strap.print == null) return false;
	console.log("item.strap.print");

	if(item.strap.width == undefined || item.strap.width == "" || item.strap.width == null) return false;
	console.log("item.strap.width");

}
else if(item.category == "JEWEL"){
	if(item.jewel.type == undefined || item.jewel.type == "" || item.jewel.type == null) return false;
	console.log("item.jewel.type");

	if(item.jewel.material == undefined || item.jewel.material == "" || item.jewel.material == null) return false;
	console.log("item.jewel.material");

	if(item.jewel.gloss == undefined || item.jewel.gloss == "" || item.jewel.gloss == null) return false;
	console.log("item.jewel.gloss");

	if(item.jewel.shape == undefined || item.jewel.shape == "" || item.jewel.shape == null) return false;
	console.log("item.jewel.shape");

	if(item.jewel.chain == undefined || item.jewel.chain == "" || item.jewel.chain == null) return false;
	console.log("item.jewel.chain");

	if(item.jewel.clasp == undefined || item.jewel.clasp == "" || item.jewel.clasp == null) return false;
	console.log("item.jewel.clasp");

	if(item.jewel.height == undefined || item.jewel.height == "" || item.jewel.height == null) return false;
	console.log("item.jewel.height");

	if(item.jewel.width == undefined || item.jewel.width == "" || item.jewel.width == null) return false;
	console.log("item.jewel.width");

	if(item.jewel.depth == undefined || item.jewel.depth == "" || item.jewel.depth == null) return false;
	console.log("item.jewel.depth");

	if(item.jewel.diameter == undefined || item.jewel.diameter == "" || item.jewel.diameter == null) return false;
	console.log("item.jewel.diameter");

	if(item.jewel.weight == undefined || item.jewel.weight == "" || item.jewel.weight == null) return false;
	console.log("item.jewel.weight");

	if(item.jewel.gemColor == undefined || item.jewel.gemColor == "" || item.jewel.gemColor == null) return false;
	console.log("item.jewel.gemColor");
	
}


else if(item.category == "OTHER"){
	
}
else{
	console.log("TEST 9 PASS");
	return false;
}

console.log("TEST FINAL PASS");


return true;
}


Product.pre('update', function(next) {
	var self = this;
	var validProduct = validateProductForContainsFilterInfo(self.getUpdate());
	console.log("isProductValid = ",validProduct);
	this._update.containsFilterInfo = validProduct;
	this.update({},{$set: {containsFilterInfo: validProduct}})
	next();


});

Product.post('save', function(doc) {
	var self = this;
	var validProduct = validateProductForContainsFilterInfo(doc);
	console.log("isProductValid = ",validProduct);
	// this._update.containsFilterInfo = validProduct;
	doc.update({},{$set: {containsFilterInfo: validProduct}})
});





module.exports = mongoose.model('Product', Product);
