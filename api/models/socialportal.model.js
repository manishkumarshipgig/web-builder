var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Image = require(path.join(__dirname, 'image.model'));

var strategyTypes = ['target-search', 'local-plump', 'store-promotion'];

var Task = new mongoose.Schema({
	nl: {
		name: String,
		description: String
	},
	en: {
		name: String,
		description: String
	},
	de: {
		name: String,
		description: String
	}, 
	fr: {
		name: String,
		description: String
	}, 
	es: {
		name: String,
		description: String
	},
	type: {
		type: String,
		required: true
    },
	defaultChecked: {
		type: Boolean,
		default: true,
	},
	mandatory: {
		type: Boolean,
		required: true,
	},
	fbPromotionSettings:{
		goal: String,
		targetGroup: String,
		promotingAreaAroundShop: Number,
		budget: Number,
		perDayOrSpread: String,
		promotionStartDate: Date,
		promotionEndDate: Date,
		presentation: String,
        alsoOnInstagram: Boolean,
		siteUrl: String,
		gender: Number,
		interests: [     // - Shailesh asking
			{
				interestId: String,
				interestName:String,
				audience_size: Number,
				description: String,
				topic: String
			}
		],
		headline: String,
		newsFeed: String,
		callToAction: String,
		budget: Number,
        distance: Number,
		ageMin: Number,
		ageMax: Number
    },
	contribution: {
		percent: Number,
		maxAmount: Number
	},
	startDate: Date,
	endDate: Date,
	images: [Image],
    orderLink: String,
    completed: Boolean,
    datePlanned: Date,
    comment: String,
    files: [{
        name: String,
        path: String
    }]
});

var todoTask = new mongoose.Schema({
    title: String,
    date: Date,
    status: String,
    completed: Boolean,
    dateCompleted: Date,
    type: String,
    url: {
        type: String,
        validate: validators.isURL({require_protocol: true, message: 'Task URL is invalid. '})
    },
    taskId: mongoose.Schema.Types.ObjectId,
    campaignId: mongoose.Schema.Types.ObjectId
});

var logItem = new mongoose.Schema({
    title: String,
    date: Date,
    type: String,
    taskId: mongoose.Schema.Types.ObjectId,
    user: String
});

var socialPortal = new mongoose.Schema({
    shops: [{
        _id: false,
        name: String
    }],
    wholesalers: [{
        _id: false,
        name: String
    }],
    brands: [{
        _id: false,
        name: String,
        credit: Number
    }],
    users: [{
        _id: false,
        name: String,
        email: String
    }],
    taskList: [todoTask],
    log: [logItem],
    marketingSupportMinutes: {
        type:Number,
        default: 0
    },
    campaigns: [{
        name: String,
        nameSlug: String,
        number: Number,
        type: {
            type: String,
            required: true
        },
        strategy: {
            type: String,
            enum: strategyTypes,
            validate: validators.isIn({message: 'The stragety type should be one of the following: (' + strategyTypes + ')'}, strategyTypes)
        },
        description: String,
        occasion: String,
        startDate: Date,
        endDate: Date,
        update: Boolean,
        brand: {
            _id: mongoose.Schema.Types.ObjectId,
            name: {
                type: String,
                validate: validators.isAlphanumeric({message: 'The provided brand name must consist of alphanumeric characters only'}),
                required: true
            },
            images: [Image]
        },
        wholesaler: {
            _id: mongoose.Schema.Types.ObjectId,
            name: {
                type: String,
                validate: validators.isAlpha({message: 'Invalid name: names cannot contain anything other than letters (i.e. digits/punctuation)'}),
                required: true
            }
        },
        user: {
            _id: false,
            name: String,
            email: String
        },
        images: [Image],
        tasks: [Task],
        url: {
            type: String,
            validate: validators.isURL({message: 'The provided URL for this campaign is invalid'})
        }

    }]
})

module.exports = mongoose.model('Socialportal', socialPortal);