var mongoose = require('mongoose');

// sub-schema that can be reused in all models when needed.
var Payment = new mongoose.Schema({
	orderId: {
		type: mongoose.Schema.Types.ObjectId
	},
	amount: {
		type: Number,
		required: true,
	},
	contribution: {
		type: Number
	},
	shippingCosts: {
		type: Number
	},
	date: {
        type: Date,
        default: Date.now,
		required: true,
    },
    payoutDate: {
        type: Date
    },
    status: [{
        status: {
			type: String,
		},
		comment: String,
		date: {
			type: Date,
			default: Date.now,
		},
    }],
    shopId: {
        type: mongoose.Schema.Types.ObjectId,
	},
	invoiceNumber: String,
	cancelled: Boolean,
	completed: Boolean,
	exported: Boolean,
	details: String,
	type: String
});

module.exports = mongoose.model('Payment', Payment);