var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');
var passportLocalMongoose = require('passport-local-mongoose');
var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));


var campaignOrder = new mongoose.Schema({
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	wholesalerId: {
		type: String,
	},
	amount: {
    type :Number
    },
	paid: {
        type :Boolean
        },
	campaignId: {
        type :Number
        },
    shopId: {
        type :Number
        },
    invoiceNumber: {
        type: String
    }
});

module.exports = mongoose.model('campaignOrder', campaignOrder);