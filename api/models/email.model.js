var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Email = new mongoose.Schema({
	messageId: {
		type: String,
		required: true
	},
	to: {
		type: String,
		required: true
	},
	subject: String,
	body: String,
	from: String,
	notificationType: String,
	timestamp: Date,
	smtpResponse: String,
	typeMail: String,
	rawData: String
});

module.exports = mongoose.model('Email', Email);