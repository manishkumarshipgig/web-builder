var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var passportLocalMongoose = require('passport-local-mongoose');
var passwordService = require(path.join(__dirname, '..', 'services', 'password.service'));
var _ = require('lodash');

var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));

var crmUser = new mongoose.Schema({

	user: {
		userId: mongoose.Schema.Types.ObjectId,
		name: {
			type: String
		}
	},
	shopSlug: {
		type: String,
		required: true,
		reviewId: mongoose.Schema.Types.ObjectId,
	},
	email: {
		type: String,
		minlength: 6,
		maxlength: 50,
		lowercase: true,
		//validate: validators.isEmail(),
		required: true
	},
	emailName: { //name to call the user when we sent them an email
		type: String,
		minlength: 3,
		maxlength: 55,
		required: true
	},
	dateOfBirth: { //will become obsolete
		type: Date,
		validate: validators.isBefore({message: 'Date of Birth can not be in the future. '}),
	},
	gender: Boolean,
	phone: [Phone],
	hasPlacedReview: {
		type: Boolean,
		default: false
	},
    retailerComments: [{
		comment: String,
		date: {
			type: Date,
			default: Date.now(),
			required: true
		},
		user: {
			_id: false,
			name: String,
			email: String
		}
	}],
    items: [{
		photo: [{
			src: String,
			alt: String,
		}],
		itemComment: {
			comment: String,
			date: {
				type: Date,
				default: Date.now(),
				required: true
			}
		},
		watchOrJewel: {
			type: Boolean,
			//-required: true
		},
		address: [Address],
		phone: [Phone],
		firstName: {
			type: String
		},
		insertion: {
			type: String
		},
		lastName: {
			type: String
		},
		email: {
			type: String
		},
		emailOfficial: {
			type: String
		},
		helpedBy: {
			type: String
		},
		productStatus: {
			type: String
		},
		productKind: {
			type: String
		},
		productMaterial: {
			type: String
		},
		customTags: {
			type: String
		},
		brandName: {
			type: String
		},
		sentEmailId: {
			// which emails belong to this particular item?

			// _id: mongoose.Schema.Types.ObjectId,
			// name: {
			// 	type: String,
			// 	required: true
			// }
		},
		productTypeId: {
			type: Number,
			required: true
		},
		//Only when productTypeId == 4
		transactionDetails: {
			countrId: String,
			receiptNumber: String,
			items: [
				{	
					productId: mongoose.Schema.Types.ObjectId,
					amount: Number,
					name: String,
					price: Number,
					photo: {
						//src is a full path to amazon (result from countr)
						src: String,
						alt: String
					}
				}
			] 
		},

		fillindate: {
			type: Date,
			required: true
		},
		itemNumber: {
			type: Number
		},
		fillinLanguage: {
			type: String,
			required: true
		},
		priceRange: { // in case of repair this will be the estimated price in case of a warranty registration / receipt this will be price of the product
			type: Number,
			required: true
		},

		// fields who are used specifically for repairs
		status: String,

		priceRepairman: { // this is the price the repairmen counts
			type: Number
		},

		priceRepairFinal: { // in case of repair this will be the final price
			type: Number
		},
		repairDate: { // estimated date when the repair will be ready
			type: Date,
			//validate: validators.isAfter({ message: 'Repair date must be in the future. ' }),
		},
		repairFinishDateFinal: { // 		repairFinishedEstimated
			type: Date,
			validate: validators.isAfter({ message: 'Repair date must be in the future. ' }),
		},

		// fields who are used specifically for warranty registration / receipts
		endDateWarranty: {
			type: Date,
			//validate: validators.isAfter({message: 'End warranty date must be in the future. '}),
		},
		//When synced with Countr, the id of the transaction in Countr
		countrId: String 
	}],
	sentMail: [{
		//objects from mail.model
	}],
    plannedEmail: [{
        emailTemplate: {
            type: String,
            required: false
        },
		template: {
			type: String,
            required: false
		},
		subject: {
			type: String,
			required: true
		},
		date: {
			type: Date,
			required: true
		},
		link: {
			type: String,
		},
		enabled: {
			type: Boolean,
			default: true,
			required: true
		},
	}],

	photo: [{ // will become obsolete
		src: String,
		alt: String,
	}],
	comment: {
		type: String
	},
	endDateWarranty: { //will become obsolete
		type: Date,
		
    },
    repairDate: { //will become obsolete
        type: Date,
        
    },
	watchOrJewel: { //will become obsolete
		type: Boolean,

    },
    productTypeId: { //will become obsolete
        type: Number,

    },
	fillindate: { //will become obsolete
		type: Date,

	},
	fillinLanguage: { //will become obsolete
		type: String,

	},
	priceRange: { //will become obsolete
		type: Number,
	},
	countrId: String
});

module.exports = mongoose.model('crmUser', crmUser);



