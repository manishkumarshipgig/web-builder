const mongoose = require('mongoose');
const validators = require('mongoose-validators');


var Blacklist = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    blockedBrands: [{
        name: String,
        nameSlug: String,
        dateAdded: {
            type: Date,
            default: Date.now
        }
    }],
    newsletter: {
        block: {
            type: Boolean,
            //is true when the email doesn't want to receive the newsletter
        },
        dateChanged: {
            type: Date,
            default: Date.now
        }
    }
})

module.exports = mongoose.model('Blacklist', Blacklist);