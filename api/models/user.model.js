var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');
var passportLocalMongoose = require('passport-local-mongoose');
var passwordService = require(path.join(__dirname, '..', 'services', 'password.service'));
var _ = require('lodash');

var Image = require(path.join(__dirname, 'image.model'));
var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));

var roles = ['admin', 'user', 'retailer', 'wholesaler', 'manufacturer'];

var Upload = new mongoose.Schema({
	_id: false,
	date: {
		type: Date,
		default: Date.now,
		validate: validators.isBefore({message: 'Upload date must be in the past or present. '}, Date.now),
		required: true
	},
	filename: {
		type: String,
		minlength: 6,
		required: true
	},
	success: {
		type: Boolean,
		required: true
	},
	status: {
		type: String,
		minlength: 4,
		required: true
	},
	rows: {
		type: [String],
		required: true
	},
	rowsCompleted: {
	type: Number,
		required: true
	}
});

var User = new mongoose.Schema({
	role: {
		type: String,
		lowercase: true,
		enum: roles,
		validate: validators.isIn({message: 'User must have one of the following roles: (' + roles + ')'}, roles),
		required: true,
		default: 'user'
	},
	userType : {
		type : String,
		default: 'local'
	},
	password : {
		type: String,
		// required: true,
		select: false //prevent password to show in query results
	},
	passwordSalt: {
		type: String,
		// required: true,
		select: false //prevent salt to show in query results
	},
	isVerified: {
		type: Boolean,
		required: true,
		default: false,
	},
	isMarketingUser: {
		type: Boolean,
		// required: true,
		default: false,
	},
	marketingUserImage:{
		type: String
	},
	productStatus:{
		type: [String]
	},
	productKind:{
		type: [String]
	},
	productMaterial:{
		type: [String]
	},
	customTagss:{
		type: [String]
	},
	tableOptions: {
		type: [String]
	},
	marketingUserMission:{
		type : String
	},
	brandPortalId: {
		type: String
	},
	mustResetPassword: { 
		type: Boolean,
		default: false 
		// must be true when the user is created with a import of by a administrator
	},
	verificationCode: {
		type: String,
		select: false
	},
	verificationDate: {
		type: Date,
		default: Date.now
	},
	// If undefined, this user has not logged in yet.
	lastLogin: {
		type: Date,
		default: Date.now,
	},
	creationDate: {
		type: Date,
		default: Date.now,
		// required: true
	},
	email: {
		type: String,
		minlength: 6,
		maxlength: 50,
		lowercase: true,
		//validate: validators.isEmail(),
		// required: true,
		unique: true
	},
	username: {
		type: String,
		minlength: 4,
		maxlength: 40,
		lowercase: true,
		// required: true,
		unique: true
	},
	firstName: {
		type: String,
		//minlength: 2,
		maxlength: 30,
		//validate: validators.isAlpha(),
		//required: true
	},
	lastNamePrefix: {
		type: String,
		maxlength: 8,
	},
	lastName: {
		type: String,
		//minlength: 2,
		maxlength: 30,
		//validate: validators.isAlpha(),
		//required: true
	},
	gender: Boolean,
	dateOfBirth: Date,
	address: [Address],
	phone: [Phone],
	uploads: [Upload],
	shops: [{ //Only when the user has the role 'retailer'
		_id: false,
		name: {
			type: String,
			required: true
		},
		nameSlug: {
			type: String,
			required: true
		},
		type: { //deprecated
			type: String,
			enum: ['shop', 'pro-shop'],
			required: true
		},
		address: [Address]
	}],
	wholesalers: [{ //Only when the user has the role 'wholesaler'
		_id: false,
		name: {
			type: String,
			required: true
		},
		nameSlug: {
			type: String,
			required: true
		}
	}],
	orders: [{
		_id: false,
		date: Date,
		status: String
	}],
	geoloc: {
		type: {
			type: String,
			default:'Point'
		},
		coordinates: [Number]
	},
	facebook: {
		profileId: String,
		token: {
			type: String,
			//select: false
		},
		adAccountDetails: [{
			_id: false,
			account_id: String,
			id: String
		}],
		extendedToken: {
			type: String,
			select: false
		},
		tokenDate: Date,
		name: String,
		email: String,
		pageId: String,//This can be also the profileId, when the user has choosen to post the message to his own wall
		facebookPages:  [{
			_id: false,
			name: String,
			id: String
		}], 
		pageToken: {
			type: String,
			//select: false
		},
		pageName: String,
		extendedPageToken: {
			type: String,
			select: false
		},
		pageTokenDate: Date,
		postTo: {
			type: String,
			enum: ['wall', 'page']
		}
	},
	pinterest: {
		profileId: String,
		accessCode: {
			type: String,
			select: false
		},
		board: String
	},
	mollieId: String
}); 

User.statics.authenticate = function(username, password, callback) {
	this.findOne({username: username}).select('+password +passwordSalt').exec(function(err, user) {
		if (err) {
			return callback(err, null);
		}
		if(!user) {
			return callback("No matching record found", null);
		}else{
			if(!user.isVerified) {
				return callback("User is not actived", null);
			}
			
			passwordService.verify(password, user.password, user.passwordSalt, function (err, result){
				if (err) {
					return callback(err, null);
				}
	
				if(result == false) {
					return callback(err, null);
				}
	
				//remove some data from the user
				user.password = undefined;
				user.passwordSalt = undefined;
				//user._id = undefined;
				user.creationDate = undefined;
				user.lastLogin = undefined;
	
				callback(err, user);
			})
		}
		
	})
}

User.statics.register = function(opts, callback) {
	var self = this;
	var data = _.cloneDeep(opts);
		
	passwordService.hash(data.password, function(err, hashedPassword, salt) {
		if (err) {
			return callback(err, null);
		}

		data.password = hashedPassword;
		data.passwordSalt = salt;
		self.model('User').create(data, function(err, user) {
			if (err) {
				return callback(err, null);
			}

			user.password = undefined;
			user.passwordSalt = undefined;

			callback(err, user);
		})
	})
}

User.statics.resetPassword = function(userId, newPassword, mustReset, callback) {
	var self = this;
	
	if(mustReset == null){
		mustReset = true;
	}

	self.model('User').findById(userId).select('+password +passwordSalt').exec(function(err, user){
		if (err) {
			return callback(err, null);
		}

		if(!user){
			return callback(err, user);
		}
		user.mustResetPassword = mustReset;

		passwordService.hash(newPassword, function(err, hashedPassword, salt){
			user.password = hashedPassword;
			user.passwordSalt = salt;
			
			user.save(function(err, result){
				if (err) {
					return callback(err, null);
				}
				user.password = undefined;
				user.passwordSalt = undefined;

				return callback(err, user);
			})

		})
	})
}

User.statics.changePassword = function(userId, currentPassword, newPassword, callback){
	console.log("userid", userId, "currentPasswird", currentPassword, "newPasssword", newPassword);
	var self = this;

	self.model('User').findById(userId).select('+password +passwordSalt').exec(function(err, user){

		if (err){
			return callback(err, null);
		}

		if(!user) {
			return callback(err, user);
		}


		passwordService.verify(currentPassword, user.password, user.passwordSalt, function(err, result){
			if (err) {
				return callback(err, null);
			}

			if(result == false) {
				var passNoMatchError = {
					type: "OLD_PASSWORD_NOT_MATCHING",
					message: 'Current password does not match'
				};
				
				return callback(passNoMatchError, null);
			}

			passwordService.hash(newPassword, function(err, hashedPassword, salt){
				user.password = hashedPassword;
				user.passwordSalt = salt;

				user.save(function(err, saved){
					if (err) {
						return callback(err, null);
					}

					if(callback){
						return callback(err, {
							success: true,
							message: 'Password changed',
							type: 'password_change_success'
						})
					}
				})
			})
		})

	})
}

module.exports = mongoose.model('User', User);


