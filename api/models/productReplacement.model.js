var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var ProductReplacement = new mongoose.Schema({
	primaryProduct : {
		type: mongoose.Schema.ObjectId,
		ref: 'Product'
	},
	duplicateProduct: {
		type: mongoose.Schema.ObjectId,
		ref: 'Product'
	},
	"type": {
		type: String,
		required: true,
		default: "merge"
	},
	created_at    : { 
		type: Date, 
		required: true, 
		default: Date.now
	},
	status: {
		type: String,
		required: true,
		default: "Pending"
	}
});

module.exports = mongoose.model('ProductReplacement', ProductReplacement);

