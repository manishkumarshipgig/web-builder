var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var Phone = require(path.join(__dirname, 'phone.model'));
var Address = require(path.join(__dirname, 'address.model'));

var OrderItem = new mongoose.Schema({
	_id: false,
	name: {
		type: String,
		required: true
	},
	brand: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		min: 0,
		required: true
	},
	quantity: {
		type: Number,
		min: 0,
		max: 5000,
		required: true
	}
});

var Order = new mongoose.Schema({
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	number: {
		type: String,
	},
	payId: {
		type: String,
	},
	shop: [{
        _id: false,
        noShop: {
            type: Boolean,
            default: false
        },
        name: String,
        nameSlug: String,
    }],
	user: [{
		_id: false,
		firstName: {
			type: String,
			required: true
		},
		lastName: {
			type: String,
			required: true
		},
		lastNamePrefix: {
			type: String
		}
	}],
	comment: String,
	billingDetails: [{
		paymentId: {
			type: String
		},
		payMethod: {
			type: String,
			//required: true
		},
		payDetails: { //iDeal bank or other payment details (no creditcard information!)
			type: String,
		}, 
		shippingPrice: {
			type: Number,
			min: 0,
			max: 100,
			//required: true //0 is not valid
		},
		firstName: {
			type: String,
			required: true
		},
		lastName: {
			type: String,
			required: true
		},
		lastNamePrefix: {
			type: String,
		},
		email: {
			type: String,
			required: true,
		},
		phone: {
			type: String,
			maxlength: 16 // Including country code
		},
		address: [Address]
	}],
	sendDetails: [{
		firstName: {
			type: String,
			required: true
		},
		lastName: {
			type: String,
			required: true
		},
		lastNamePrefix: {
			type: String,
		},
		phone: {
			type: String,
			minlength: 8,
			maxlength: 16 // Including country code
		},
		address: [Address]
	}],
	status: [{
		status: {
			type: String,
			required: true
		},
		comment: String,
		date: {
			type: Date,
			default: Date.now,
			required: true
		}
	}],
	handled: Boolean,
	paymentCreated: {
		type: Boolean,
		default: false
	},
	items: [OrderItem]
});

module.exports = mongoose.model('Order', Order);