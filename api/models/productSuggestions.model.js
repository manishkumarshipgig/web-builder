var mongoose = require('mongoose');

var ProductSuggestion = new mongoose.Schema({
	product_id: {
		type: String
	},
	item: {
		type: Object
	},
	suggester: {
		type: Object
	},
	time : { 
		type : Date, 
		default: Date.now 
	}

}, {strict: false});
module.exports = mongoose.model('productSuggestings', ProductSuggestion);