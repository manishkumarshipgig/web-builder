var mongoose = require('mongoose');

// sub-schema that can be reused in all models when needed.
var Image = new mongoose.Schema({
	src: {
		type: String,
		// required: true
	},
	alt: {
		type: String
	}
});

module.exports = Image;