var path = require('path');

var mongoose = require('mongoose');
var validators = require('mongoose-validators');

var ProductDeleteSuggestion = new mongoose.Schema({
	product: {
		type: mongoose.Schema.ObjectId,
		ref: 'Product'
	},
	suggester: {
		user: {
			type: mongoose.Schema.ObjectId,
			ref: 'User'
		},
		name: String
	},
	time : { 
		type : Date,
		default: Date.now 
	},
	pending:{
		type: Boolean,
		default: true
	}

}, {strict: false});
module.exports = mongoose.model('ProductDeleteSuggestion', ProductDeleteSuggestion);