var mongoose = require('mongoose');

// sub-schema that can be reused in all models when needed.
var CollectionDetails = new mongoose.Schema({
	_id: false,
	name: {
		type: String
	},
	nameSlug: {
		type: String
	},
	longDescription: {
		type: String
	},
	shortDescription: {
		type: String
	},
	views: {
		type: Number,
		min: 0,
		default: 0
	}
}, { _id: false });


module.exports = CollectionDetails;
