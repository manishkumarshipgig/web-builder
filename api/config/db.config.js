var path = require('path');

var mongoose = require('mongoose');

var config = {
	url: '127.0.0.1',
	port: '27017',
	name: 'prismanote'
}

let options = {
	socketTimeoutMS: 30000,
	keepAlive: true,
	reconnectTries: 30,
    autoIndex: false, // Don't build indexes
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
}

// Main application models
require(path.join(__dirname, '..', 'models', 'user.model'));
// require(path.join(__dirname, '..', 'models', 'merchant.model'));
require(path.join(__dirname, '..', 'models', 'shop.model'));
require(path.join(__dirname, '..', 'models', 'news.model'));
require(path.join(__dirname, '..', 'models', 'campaign.model'));
require(path.join(__dirname, '..', 'models', 'brand.model'));
require(path.join(__dirname, '..', 'models', 'manufacturer.model'));
require(path.join(__dirname, '..', 'models', 'order.model'));
require(path.join(__dirname, '..', 'models', 'fbOrders.model'));
require(path.join(__dirname, '..', 'models', 'product.model'));
require(path.join(__dirname, '..', 'models', 'productReplacement.model'));
require(path.join(__dirname, '..', 'models', 'duplicateProducts.model'));
require(path.join(__dirname, '..', 'models', 'productDeleteSuggestion.model'));
require(path.join(__dirname, '..', 'models', 'translate.model'));
require(path.join(__dirname, '..', 'models', 'crmuser.model'));
require(path.join(__dirname, '..', 'models', 'email.model'));
require(path.join(__dirname, '..', 'models', 'productUpdateLog.model'));
require(path.join(__dirname, '..', 'models', 'socialportal.model'));
require(path.join(__dirname, '..', 'models', 'payment.model'));
require(path.join(__dirname, '..', 'models', 'wholesaler.model'));
require(path.join(__dirname, '..', 'models', 'setting.model'));
require(path.join(__dirname, '..', 'models', 'blacklist.model'));
require(path.join(__dirname, '..', 'models', 'collection.model'));
require(path.join(__dirname, '..', 'models', 'upgradeOrder.model'));
require(path.join(__dirname, '..', 'models', 'fbinsights.model'));
require(path.join(__dirname, '..', 'models', 'adminfbToken.model'));
require(path.join(__dirname, '..', 'models', 'externalApiToken.model'))
require(path.join(__dirname, '..', 'models', 'chat.model'));
require(path.join(__dirname, '..', 'models', 'campaignorders.model'));

mongoose.connect('mongodb://' + config.url + ':' + config.port + '/' + config.name,options);

mongoose.connection.on('connected', function() {
	console.log('Mongoose connected to database [' + config.name + '] on ' + config.url + ":" + config.port + ".");
});

mongoose.connection.on('disconnected', function() {
	console.log('Mongoose disconnected from database [' + config.name + '] on ' + config.url + ":" + config.port + ".");
});

mongoose.connection.on('error', function(err) {
	console.log('Mongoose error on connection ' + config.url + ":" + config.port + " [" + config.name + "]. " + err);
});

process.on('SIGINT', function() {
	mongoose.connection.close(function() {
		console.log('Mongoose lost connection to the database because the app was terminated by a SIGINT event. ');
		process.exit(0);
	});
});

// The following error trapping methods only work on Unix-based platforms.
process.on('SIGTERM', function() {
	mongoose.connection.close(function() {
		console.log('Mongoose lost connection to the database because the app was terminated by a SIGTERM event. ');
		process.exit(0);
	});
});

process.once('SIGUSR2', function() {
	mongoose.connection.close(function() {
		console.log('Mongoose lost connection to the database because the app was terminated by a SIGUSR2 event. ');
		process.kill(process.pid, 'SIGUSR2');
	});
});

module.exports = config;