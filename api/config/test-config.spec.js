const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

/* These hooks are executed for all tests in all test suites. */

before(function() {
	chai.use(sinonChai);
});

after(function() {
	/* Optionally add code here to clean up after all tests have finished. */
});

beforeEach(function() {
	this.sandbox = sinon.sandbox.create();
});

afterEach(function() {
	this.sandbox.restore();
});
