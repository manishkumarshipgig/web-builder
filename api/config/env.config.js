const chalk = require('chalk');
const path = require('path');

var envs = [];

const settings = require(path.join(__dirname, 'env.json'));

envs['production'] = {
	name: 'production',
	adminEmail: 'admin@prismanote.com',
	url: 'https://prismanote.com',
	port: 80,
	s3Bucket: "prismanote",
	algolia: {
		shops: 'shops',
		brands: 'brands',
		products: 'products',
		news: 'news',
		campaigns: 'test_prismaCampaigns'
	},
	settings: settings.production
};

envs['development'] = {
	name: 'development',
	adminEmail:'admin@prismanote.com',
	url:'http://127.0.0.1',
	port:3000,
	s3Bucket: "prismanote",
	algolia: {
		shops:'test_prismaShop',
		brands:'test_prismaBrands',
		products:'test_prismaProducts',
		news:'test_prismaNews',
		campaigns:'test_prismaCampaigns'
	},
	settings: settings.development
};

envs['test'] = {
	name: 'test',
	adminEmail: 'admin@prismanote.com',
	url:'http://127.0.0.1',
	port:3000,
	s3Bucket: "prismanote",
	algolia: {
		shops: 'shops',
		brands: 'brands',
		products: 'products',
		news: 'news',
		campaigns: 'test_prismaCampaigns'
	},
	settings: settings.testing
}

// Set environment dynamically at runtime
var env = process.env.NODE_ENV;

// Default to development environment if NODE_ENV is undefined.
if(!env || !envs[env]) {
	console.log('NODE_ENV is undefined or its value was not understood. Default to development mode. ');
	env = 'development';
}

console.log('Starting in', chalk.greenBright(env), 'mode...');

module.exports = envs[env];