const path = require('path');

console.log('Initializing API...');

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const morgan = require('morgan');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const config =  require(path.join(__dirname, 'db.config'));
const amazonConfig = require(path.join(__dirname, 'amazon.config.js'));

const passport = require('passport');

const app = express();


// Serve frontend and static files like stylesheets and images from the Express server
app.use(express.static(path.resolve(__dirname, '..', '..', 'app', 'dist')));
app.use('/public', express.static(path.join(__dirname, '..', '..', 'public')));
app.get('/prismanote/*/development/*.html',function(req,res){
    var url = path.join(__dirname, '..', '..', 'public');
    res.sendFile(url+'/prismanote/'+req.params['0']+'/development/'+req.params['1']+'.html')
    // next();
});
app.get('/prismanote/*/production/*.html',function(req,res){
    var url = path.join(__dirname, '..', '..', 'public');
    res.sendFile(url+'/prismanote/'+req.params['0']+'/production/'+req.params['1']+'.html')
    // next();
});

//special parser for amazon SNS, because the express bodyparser removes the JSON body
app.use(amazonConfig.overrideContentType());
// Handle application/json requests
app.use(bodyParser.json({ limit: '50mb' }));
// Handle application/x-www-form-urlencoded requests (usually POST, PUT, etc.)
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

app.use(cookieParser());

app.use(session({
    store: new MongoStore({
        url: 'mongodb://' + config.url + ':' + config.port + '/' + config.name
    }),
    secret: 'DJL9H30Hh9nfgFNnr8926jyh',
    key: 'skey.sid',
    resave: false,
    saveUninitialized: false,
    cookie : {
        maxAge: 604800000 // 7 days in miliseconds
    }
}));

app.use(passport.initialize());
app.use(passport.session());
require(path.join(__dirname, 'auth.config'))(passport); //Load passport config

app.use(function(req, res, next) {
    req.resources = req.resources || {};
   // res.locals.app = config.app;
    res.locals.currentUser = req.user;
    res.locals._t = function (value) { return value; };
    res.locals._s = function (obj) { return JSON.stringify(obj); };
    next();
})

// Use gzip compression (following the Express.js best practices for performance)
app.use(compression());

// Morgan logger (previously named express-logger)
app.use(morgan("dev"));


// Always keep the errorHandler at the bottom of the middleware function stack!

// Returns a user-friendly error message when the server fails to fulfill the request
app.use(function(err, req, res, next) {
	var status = 500, response = {message: 'An internal server error has occurred while trying to load this page. '};

	console.error(err.stack);
	res.status(status).json(response);

	next(err);
});

module.exports = app;
