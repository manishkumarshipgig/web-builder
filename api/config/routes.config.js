const path = require('path'); // Use path.join to make all paths platform-independent
const passport=require('passport');

console.log('Initializing router...');

const multipart = require('connect-multiparty');
const multipartMiddleware = multipart({maxFields:10000});

const mainController = require(path.join(__dirname, '..', 'controllers', 'main.controllers'));
const authController = require(path.join(__dirname, '..', 'controllers', 'auth.controllers'));
const usersController = require(path.join(__dirname, '..', 'controllers', 'users.controllers'));
const emailController = require(path.join(__dirname, '..', 'controllers', 'email.controllers'));
const repairSettingsController = require(path.join(__dirname, '..', 'controllers', 'repairSettings.controllers'));
const shopsController = require(path.join(__dirname, '..', 'controllers', 'shops.controllers'));
const productsController = require(path.join(__dirname, '..', 'controllers', 'products.controllers'));
const ordersController = require(path.join(__dirname, '..', 'controllers', 'orders.controllers'));
const brandsController = require(path.join(__dirname, '..', 'controllers', 'brands.controllers'));
const campaignsController = require(path.join(__dirname, '..', 'controllers', 'campaigns.controllers'));
const newsController = require(path.join(__dirname, '..', 'controllers', 'news.controllers'));
const mollieController = require(path.join(__dirname, '..', 'controllers', 'mollie.controllers'));
const snsController = require(path.join(__dirname, '..', 'controllers', 'sns.controllers'));
const socialPortalController = require(path.join(__dirname, '..', 'controllers', 'socialPortal.controllers'));
const financialController = require(path.join(__dirname, '..', 'controllers', 'financial.controllers'));
const importController = require(path.join(__dirname, '..', 'controllers', 'import.controllers'));
const wholesalerController = require(path.join(__dirname, '..', 'controllers', 'wholesalers.controllers'));
const facebookAdsController = require(path.join(__dirname, '..', 'controllers', 'facebookAds.controllers.js' ))
const grapeController = require(path.join(__dirname, '..', 'controllers', 'grape.controller'));
const dataController = require(path.join(__dirname, '..', 'controllers', 'data.controllers.js' ))
const countrController = require(path.join(__dirname, '..', 'controllers', 'countr.controllers.js'));
const cartController = require(path.join(__dirname, '..', 'controllers', 'cart.controllers.js'));
const collectionsController = require(path.join(__dirname, '..', 'controllers', 'collections.controllers'));
const upgradeShopController = require(path.join(__dirname, '..', 'controllers', 'upgradeshop.controllers'));
const ensureAuthenticated = require(path.join(__dirname, '..', 'services', 'auth.service'));


module.exports = function(app) {
	console.log("Loading router...");

	app.route('/api/users/import')
		.get(ensureAuthenticated(['admin']), usersController.importFromJson);
	app.route('/api/user/:userId')
		.get(ensureAuthenticated(['admin']), usersController.getUser)
		.put(ensureAuthenticated(), usersController.updateUser)
		.delete(ensureAuthenticated(['admin']), usersController.removeUser);

	// user for crmUserInfoModal
	app.route('/api/user-for-crm')
		.post(ensureAuthenticated(['retailer']), usersController.getCrmUserInfoForModal)

	app.route('/api/shop/reviews')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']), shopsController.addReviewItem);
	app.route('/api/shop/loyalty-watches')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),usersController.newCrmUser);
	app.route('/api/shop/loyalty-repair/settings/status')
		.post(ensureAuthenticated(['retailer']),repairSettingsController.updateRepairSettingsStatus);
	app.route('/api/shop/loyalty-repair/settings/productkind')
		.post(ensureAuthenticated(['retailer']),repairSettingsController.updateRepairSettingsProductKind);
	app.route('/api/shop/loyalty-repair/settings/tableoptions')
		.post(ensureAuthenticated(['retailer']),repairSettingsController.updateTableOptions);
	app.route('/api/shop/loyalty-repair/settings/productmaterial')
		.post(ensureAuthenticated(['retailer']),repairSettingsController.updateRepairSettingsProductMaterial);
	app.route('/api/shop/loyalty-repair/settings/customtags')
		.post(ensureAuthenticated(['retailer']),repairSettingsController.updateRepairSettingsCustomTags);
	app.route('/api/user/loyalty-photo-upload')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),multipartMiddleware, usersController.uploadCrmCardPhoto);

	app.route('/api/crmuserscount')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),usersController.getCrmUsersCount);

	app.route('/api/repairready-email')
		.post(ensureAuthenticated(['retailer']), usersController.repairReadyEmail);

	app.route('/api/repairready-email-company')
		.post(ensureAuthenticated(['retailer']), usersController.repairReadyEmailCompany);

	app.route('/api/repairready-withpic-email')
		.post(ensureAuthenticated(['retailer']), usersController.repairReadyEmailWithPic);

	app.route('/api/crmusers')
		.get(ensureAuthenticated(['retailer']),usersController.getCrmUsers)
		.post(ensureAuthenticated(['retailer']),usersController.setPlannedEmail)
		.put(ensureAuthenticated(['retailer']),usersController.updateCrmUser);
	app.route('/api/crmuser/:crmUserId')
		.get(ensureAuthenticated(['retailer']), usersController.getCrmUser)

	app.route('/api/crmuseremail/:email')
		.get(ensureAuthenticated(['retailer']), usersController.getCrmUserEmail)

	app.route('/api/emailtemplateupdate')
		.post(ensureAuthenticated(['retailer']), usersController.setEmailBody);

	app.route('/api/add-new-planned-email')
		.post(ensureAuthenticated(['retailer']), usersController.createCrmUsersplannedEmail);

	//Note: This URL is called from setcronjob.com every day at 07.00 hour (local time (UTC+2))
	app.route('/api/send-crm-email')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),emailController.getPlannedEmails);
	app.route('/api/preview-email')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']), emailController.previewEmail);
	app.route('/api/preview-email-brand-new-campaign')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']), emailController.previewEmailBrandsNewCampaign);
	app.route('/api/load-retailer-welcome')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),campaignsController.loadRetailerWelcomeAndParseData);
	app.route('/api/add-campaign-to-shop')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),campaignsController.addCampaignFromMailToSocialPortal);


	app.route('/api/users')
		.get(ensureAuthenticated(['admin']), usersController.getUsers)
		.post(ensureAuthenticated(['admin']), usersController.addUser);
	app.route('/api/reset-password')
		.post(ensureAuthenticated(['admin']), usersController.resetPassword);
	app.route('/api/send-password-reset-link')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),usersController.sendPasswordResetMail);
	app.route('/api/send-activation-link')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),usersController.sendActivationLink);
	app.route('/api/change-password')
		.post(usersController.changePassword);
	app.route('/api/self-change-password')
		.post(ensureAuthenticated(), usersController.selfChangePassword);
	app.route('/api/set-new-password')
		.post(authController.setNewPassword);

	app.route('/api/shops/import')
		.get(ensureAuthenticated(['admin']), shopsController.importFromJson);
	app.route('/api/shops/:nameSlug/vcard')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),shopsController.getVCard);

	app.route('/api/shops/:nameSlug')
		.get(shopsController.getShop)
		.put(ensureAuthenticated(['retailer', 'admin']), shopsController.updateShop)
		.delete(ensureAuthenticated(['admin']),shopsController.removeShop);

	app.route('/api/shops/delete-product-from-shop/')
		.post(ensureAuthenticated(['retailer', 'admin']),shopsController.removeProductFromShop);
	app.route('/api/shop/save-shop-product/')
		.post(ensureAuthenticated(['retailer', 'admin']),productsController.saveShopProduct);


	app.route('/api/add-brand-to-shop')
		.post(ensureAuthenticated(['retailer', 'admin']),shopsController.addBrandToShop);
	app.route('/api/remove-brand-from-shop')
		.post(ensureAuthenticated(['retailer', 'admin']),shopsController.removeBrandProducts)

	app.route('/api/shops')
		.get(shopsController.getShops)
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),shopsController.addShop);
	app.route('/api/match-emailaddr-with-shops')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),wholesalerController.matchEmailAdressesWithShops);
	app.route('/api/add-brand-to-blacklist')
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),usersController.addBrandtoBlacklist);
	app.route('/api/shop/news')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),shopsController.getShopNewsItem)
	app.route('/api/wholesalers/:nameSlug')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),wholesalerController.getWholesaler)
		.put(ensureAuthenticated(['admin', 'wholesaler']), wholesalerController.updateWholesaler)
		.delete(ensureAuthenticated(['admin', 'wholesaler']), wholesalerController.removeWholesaler)
	app.route('/api/wholesalers')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),wholesalerController.getWholesalers)
		.post(ensureAuthenticated(['admin']), wholesalerController.addWholesaler)

	app.route('/api/shop/uploadsliderphoto')
		.post(ensureAuthenticated(), multipartMiddleware, shopsController.uploadSliderPhoto);
	app.route('/api/shop/uploaditemgridphoto')
		.post(ensureAuthenticated(), multipartMiddleware, shopsController.uploadItemGridPhoto);
	app.route('/api/shop/uploadlogo')
		.post(ensureAuthenticated(), multipartMiddleware, shopsController.uploadLogo);

	app.route('/api/campaigns/:nameSlug')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),campaignsController.getCampaign)
		.put(ensureAuthenticated(['wholesaler', 'admin']), campaignsController.updateCampaign)
		.delete(ensureAuthenticated(['admin']), campaignsController.removeCampaign);
	app.route('/api/campaigns')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),campaignsController.getCampaigns)
		.post(ensureAuthenticated(['wholesaler', 'admin']), campaignsController.addCampaign);
	app.route('/api/approve-campaign')
		.post(ensureAuthenticated(['admin']), campaignsController.approveCampaign);

	app.route('/api/get-specific-product')
		.get(productsController.getSpecificProduct);
	app.route('/api/get-specific-product-from-shop')
		.get(productsController.getWebshopProduct);
	app.route('/api/get-specific-shop-products')
		.post(productsController.getSpecificShopProducts);

	app.route('/api/products/search/:searchText')
		.get(ensureAuthenticated(['admin']), productsController.searchText);
	app.route('/api/products/merge-products/:primary/:duplicate')
		.get(ensureAuthenticated(['admin']), productsController.mergeProducts);

	app.route('/api/products/merge-products-further-test')
		.get(productsController.productReplacementAfterMerge);
	app.route('/api/products/new-products-added-further-test')
		.get(productsController.newProductAddedCronJob);


	app.route('/api/products/import')
		.get(ensureAuthenticated(['admin']), productsController.importFromJson);
	app.route('/api/products/updateimages')
		.get(ensureAuthenticated(), productsController.putImagesToAmazon);
	app.route('/api/products/count')
		.get(productsController.getProductCount);
	app.route('/api/products/:nameSlug')
		.get(productsController.getProduct)
		.put(ensureAuthenticated(['retailer', 'wholesaler', 'admin']), productsController.updateProduct);


	app.route('/api/suggest-update-product')
		.put(productsController.addSuggestionForUpdate);

	app.route('/api/products')
		.get(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),productsController.getProducts)
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),productsController.addProduct)
		.delete(ensureAuthenticated(['admin']), productsController.removeProduct);

	app.route('/api/product/delete-suggestion')
		.post(shopsController.addDeleteProductToSuggestion)
		.get(shopsController.getDeleteProductToSuggestion)
		.delete(shopsController.doDeleteProductToSuggestion)


	app.route('/api/webshop-products/:nameSlug')
		.get(productsController.getWebshopProducts);

	app.route('/api/webshop-products-categories/:nameSlug')
		.get(productsController.getWebshopProductsCategories);

	app.route('/api/webshop-products-search/:nameSlug/:searchMode/:searchText')
		.get(productsController.getWebshopProductsSearch);
	//The difference between these routes in that the above route is for the public webshop and the other route is for managing the shop
	app.route('/api/shop-products/:shopId')
		.get(ensureAuthenticated(['retailer', 'admin']), productsController.getProductsFromShop);
	app.route('/api/shop/updateimages')
		.get(ensureAuthenticated(), shopsController.updateBrandImages);

	app.route('/api/product-image-upload')
		.post(multipartMiddleware, productsController.uploadProductImages);

	app.route('/api/collections')
		.get(collectionsController.getCollections)
		.post(ensureAuthenticated(['retailer', 'wholesaler', 'admin']),collectionsController.addCollection)
		.put(ensureAuthenticated(['retailer', 'wholesaler', 'admin']), collectionsController.updateCollection);
	app.route('/api/delete-collection/:id')
		.delete(ensureAuthenticated(['retailer', 'wholesaler', 'admin']), collectionsController.removeCollection);

	app.route('/api/orders/:nameSlug')
		.get(ensureAuthenticated(), ordersController.getOrder)
		.put(ensureAuthenticated(['admin']), ordersController.updateOrder)
		.delete(ensureAuthenticated(['admin']),ordersController.removeOrder);
	app.route('/api/orders')
		.get(ensureAuthenticated(),ordersController.getOrders)
		.post(ensureAuthenticated(), ordersController.addOrder);
	app.route('/api/retailer-order-list')
		.get(ensureAuthenticated(['retailer']), ordersController.getRetailerOrderList);
	app.route('/api/package-slip')
		.post(ensureAuthenticated(), ordersController.createPackageSlip);
	app.route('/api/generate-pdf')
		.post(ensureAuthenticated(), ordersController.generatePdf);
	app.route('/api/complete-order')
		.post(ensureAuthenticated(), ordersController.completeOrder);
	app.route('/api/send-order')
		.post(ensureAuthenticated(), ordersController.sendPackage);

	app.route('/api/brands/updateimages')
		.get(ensureAuthenticated(), brandsController.changeBrandImages);
	app.route('/api/brands/import')
		.get(ensureAuthenticated(['admin']), brandsController.importFromJson);
	app.route('/api/brands/:nameSlug')
		.get(brandsController.getBrand)
		.put(ensureAuthenticated(['wholesaler','retailer' ,'admin']), brandsController.updateBrand)
		.delete(ensureAuthenticated(['admin']), brandsController.removeBrand);
	app.route('/api/brands')
		.get(brandsController.getBrands)
		.post(ensureAuthenticated(['wholesaler','admin']),brandsController.addBrand)
	app.route('/api/addBrands')
		.post(ensureAuthenticated(['wholesaler','retailer','admin']),brandsController.addBrands);
	app.route('/api/brand/uploadimage')
		.post(ensureAuthenticated(['wholesaler','retailer', 'admin']),multipartMiddleware, brandsController.changeImage);

	app.route('/api/news/:nameSlug')
		.get(newsController.getNewsItem)
		.put(ensureAuthenticated(['wholesaler', 'retailer', 'admin']), newsController.updateNewsItem)
		.delete(ensureAuthenticated(['admin']),newsController.removeNewsItem);
	app.route('/api/news')
		.get(newsController.getNewsItems)
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),newsController.addNewsItem);
	app.route('/api/news/news-photo-upload')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),multipartMiddleware, newsController.uploadNewsItemPhoto);
	app.route('/api/upload-news-content-image')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),multipartMiddleware, newsController.uploadNewsContentImage);


	app.route('/api/register')
		.post(usersController.registerUser);

	app.route('/api/login')
		.post(authController.loginUser);

	app.route('/api/logout')
		.get(authController.logoutUser);

	app.route('/api/activate-user')
		.post(authController.activate);

	app.route('/api/checklogin')
		.get(authController.checkLogin);

	app.route('/api/cart')
		.get(cartController.getCart)
		.post(cartController.saveCart);
	app.route('/api/empty-cart')
		.get(cartController.emptyCart);

	app.route('/api/translation')
		.get(mainController.getTranslations)
		.put(mainController.addMissingTranslation)

	// GET requests on the application root URL are sent to the homepage, other HTTP requests (i.e. POST) continue to the next route to return a 404 page
	app.route('/api/home')
		.get(mainController.getIndex);

	//Things with orders and payments
	app.route('/api/create-order')
		.post(ordersController.createOrder);
	app.route('/api/create-payment')
		.post(mollieController.createPayment);
	app.route('/api/payment-webhook')
		.post(mollieController.paymentWebhook);
	app.route('/api/check-payment/:payId')
		.get(mollieController.checkPayment);
	app.route('/api/generate-payid')
		.post(ordersController.setNewPayId);
	app.route('/api/cancel-order')
		.post(ordersController.cancelOrder);
	app.route('/api/get-paymentinfo')
		.post(mollieController.getPayment);
	app.route('/api/refund-payment')
		.post(mollieController.refundPayment);
	app.route('/api/get-refunds')
		.post(mollieController.getRefunds);
	app.route('/api/return-order')
		.post(ordersController.returnOrder);
	app.route('/api/payment-invoice')
		.get(ensureAuthenticated(), financialController.generateInvoicePayment);
	app.route('/api/start-upgrade-shop')
		.post(ensureAuthenticated(), shopsController.createUpgradeRecurringPayment);
	app.route('/api/upgrade-shop')
		.post(shopsController.upgradeShop);

	//Shop upgrades
	app.route('/api/get-shop-modules')
		.get(ensureAuthenticated(['admin', 'retailer']), upgradeShopController.getShopModules)
	app.route('/api/start-upgrade')
		.post(ensureAuthenticated(['admin', 'retailer']), upgradeShopController.startUpgrade);
	app.route('/api/finish-upgrade')
		.post(ensureAuthenticated(['admin', 'retailer']), upgradeShopController.finishUpgrade);
	app.route('/api/cancel-subscription')
		.post(ensureAuthenticated(['admin', 'retailer']), upgradeShopController.cancelSubscription);
	app.route('/api/recurring-webhook')
		.post(upgradeShopController.webhook);

	//Amazon SES notifications
	app.route('/api/amazon-ses')
		.post(snsController.parseResult);

	//Social Media Portal & social login
	app.route('/api/user-social-portal')
		.get(ensureAuthenticated(), socialPortalController.getUserSocialPortal)
		.post(ensureAuthenticated(), socialPortalController.createSocialPortal)
		.put(ensureAuthenticated(), socialPortalController.saveSocialPortal)
	app.route('/api/check-social-portal')
		.get(ensureAuthenticated(), socialPortalController.checkSocialPortal);
	app.route('/api/portal-campaigns')
		.get(ensureAuthenticated(), socialPortalController.getPortalCampaignsAndTasks)

	// Social portals for Admin to check which updates are needed to be made
	app.route('/api/social-portals')
		.get(ensureAuthenticated(), socialPortalController.getSocialPortals)

	// var facebookRolesRetailer = ['manage_pages','email', 'public_profile', 'publish_pages', 'user_posts', 'publish_actions', 'ads_management', 'ads_read','pages_show_list', 'instagram_basic'];
	var facebookRolesRetailer = ['manage_pages', 'email', 'public_profile', 'publish_pages', 'user_posts', 'pages_show_list','publish_actions', 'ads_management', 'read_insights', 'ads_read', 'instagram_basic','user_events'];
		app.get('/api/auth/facebook', passport.authenticate('facebook', {scope: facebookRolesRetailer}));
		//no, rerequest is not an typo. It is used to let the users reauthenticate the facebook app
		app.get('/api/auth/facebook/rerequest', passport.authenticate('facebook', {authType: 'rerequest', scope: facebookRolesRetailer}));
		app.get('/api/auth/facebook/callback', passport.authenticate('facebook', {successRedirect: '/retailer/home', failureRedirect:  '/home'}))

	app.route('/api/get-admin-fbToken')
		.post(ensureAuthenticated(['admin']),facebookAdsController.getAdminToken);

	app.route('/api/fb-grant-access-brand')
		.post(facebookAdsController.grantAccessToBrand);

	app.get('/api/auth/pinterest', passport.authenticate('pinterest', {
		scope: ['read_public', 'write_public', 'read_relationships','write_relationships'],
	}));
	app.get('/api/auth/pinterest/callback', passport.authenticate('pinterest', {
		successRedirect: '/retailer/campaigns',
		failureRedirect: '/retailer'}));

	app.route('/api/facebook/get-pages')
		.get(ensureAuthenticated(), socialPortalController.getFacebookPages)

	app.route('/api/facebook/test')
		.get(ensureAuthenticated(), socialPortalController.sendTestPost);

	app.route('/api/facebook/post')
		.post(ensureAuthenticated(), socialPortalController.postUpdate);

	app.route('/api/download-task-files')
		.get(ensureAuthenticated(), socialPortalController.downloadFiles);

	app.route('/api/get-portal-campaign')
		.get(ensureAuthenticated(), socialPortalController.getPortalCampaign);
	app.route('/api/facebook/get-fbinfo')
		.get(ensureAuthenticated(), socialPortalController.getFacebookInfo);
	app.route('/api/image-url')
		.post(ensureAuthenticated(),socialPortalController.getImageUrl);
		// app.route('/api/get-brand-campaign')
		// 	.get(ensureAuthenticated(), socialPortalController.getBrandCampaign);


	app.route('/api/send-promotion')
		.post(ensureAuthenticated(), socialPortalController.sendCampaignPromotion)

	app.route('/api/upload-promotion-images')
		.post(ensureAuthenticated(), multipartMiddleware, socialPortalController.uploadPromotionImages);

	app.route('/api/upload-marketing-user-image')
		.post(ensureAuthenticated(), multipartMiddleware,socialPortalController.uploadMarketingUserImage);

	app.route('/api/pinterest/get-boards')
		.get(ensureAuthenticated(), socialPortalController.getPinterestBoards);
	app.route('/api/pinterest/create-pin')
		.post(ensureAuthenticated(), socialPortalController.createPinterestPin);

	app.route('/api/task-details')
		.get(ensureAuthenticated(), socialPortalController.getFullTask);

	app.route('/api/financial-dashboard')
		.get(ensureAuthenticated(['retailer','admin']), financialController.getFinancialDashboard);
	app.route('/api/orders-with-payments')
		.get(ensureAuthenticated(['retailer', 'admin']), financialController.getOrdersWithPayments);

	app.route('/api/handle-payments')
		.get(financialController.handlePayments);

		// Shop news requests
	app.route('/api/shop/news')
		.put(ensureAuthenticated(['retailer']), newsController.updateNewsItemShop);
	app.route('/api/shop/news-image')
		.post(ensureAuthenticated(['retailer']), multipartMiddleware, newsController.uploadNewsItemShopPhoto);
	app.route('/api/shop/newsItemSlug')
		.get(ensureAuthenticated(), newsController.createShopNewsItemSlug);

		//flexible import routes
	app.route('/api/import/parse')
		.post(ensureAuthenticated(['admin', 'retailer']),multipartMiddleware,  importController.parseFile);
	app.route('/api/import/value')
		.post(ensureAuthenticated(['admin', 'retailer']), multipartMiddleware, importController.getValues);
	app.route('/api/import/handle')
		.post(ensureAuthenticated(['admin', 'retailer']), multipartMiddleware, importController.fileImport);


		// GET requests to pages/features that are not yet finished can be rerouted to this URL to return a HTTP 501 Not Implemented message.
	app.route('/api/501')
		.get(mainController.get501);


		// Facebook Ads Creation
	app.route('/api/facebook-ad-campaign')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.createCampaign);

	app.route('/api/facebook/prev')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getFbPrev);

	app.route('/api/facebook-country-code')
		.get(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getCountryCode);
	app.route('/api/facebook-region-code')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getRegionCode);

	app.route('/api/facebook-city-code')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getCityCode);

	app.route('/api/facebook-adInterests')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.createInterest);

	app.route('/api/facebook-adset')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.createAdset);

	app.route('/api/facebook-adcreative')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.createAdCreative);

	app.route('/api/facebook-ad')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.createAd);

	app.route('/api/facebook-admin-id')
		.get(facebookAdsController.getAdminAccountId);

	app.route('/api/facebook-adreach')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getReach);

	app.route('/api/facebook-adimage')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.createAdImageHash);
	app.route('/api/facebook-audience')
		.get(facebookAdsController.getAudience);

	app.route('/api/facebook-insights')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getInsights);
	app.route('/api/create-fborder')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),ordersController.createFbOrder);
	app.route('/api/save-fbInsights')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.saveFbInsight);

	app.route('/api/facebook/adspixels')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.fbAdsPixel);
	app.route('/api/facebook/customlocation')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.customlocation);
	app.route('/api/facebook/intagram-account')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.intagramAccount);
	app.route('/api/facebook/getpage-token')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getpagetoken);
	app.route('/api/facebook/read-adcamp')
		.get(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.readAdcamp);
	app.route('/api/facebook/camp-insightss')
		.get(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.campInsightss);
	app.route('/api/facebook/read-adset')
		.get(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.readAdset);
	app.route('/api/facebook/read-ads')
		.get(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.readAds);
	app.route('/api/facebook/read-insight')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.readInsight);
	app.route('/api/facebook/adaccount-insights')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.adaccountInsights);
	app.route('/api/facebook/camp-insights')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.campInsights);
	app.route('/api/facebook/campaign-execute')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.campaignExecute);
	app.route('/api/facebook/totalspent')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.totalSpent);
	app.route('/api/facebook/campaign-name')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.campaignName);

	app.route('/api/getData-fbInsights')
		.get(facebookAdsController.getFbInsight);
	app.route('/api/check-fbpayment/:payId')
		.get(mollieController.fbCheckPayment);
	app.route('/api/save-fbpaymentId')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),ordersController.saveFbpaymentId);
	app.route('/api/fbpayment-webhook')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),mollieController.fbPaymentWebhook);
	app.route('/api/getData-campaignDB')
		.get(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getCampaignDB);
	app.route('/api/get-social-portalDB')
		.get(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getSocialPortalDB);
	app.route('/api/get-portal-userdata')
		.post(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.getPortalUserData);
	app.route('/api/fbCtrlWholesaler')
		.get(ensureAuthenticated(['wholesaler', 'retailer', 'admin']),facebookAdsController.DBWholesalers);
		//Data API's for Clarity and maybe later some other programms or webshops
	app.route('/api/cs/article-base')
		.get(ensureAuthenticated(['admin']), dataController.createArticleBaseClarity);

	app.route('/api/download-label')
		.post(dataController.downloadLabel);

	app.route('/api/fb-save-credit-brand')
		.post(facebookAdsController.postcreditSocialPortal);
	app.route('/api/fb-credit-campaign')
		.post(facebookAdsController.postCreditCampaign);
	app.route('/api/verify-marketing-user')
		.post(facebookAdsController.verifyMarketingUser);

	app.route('/api/isregister-marketinguser')
		.post(facebookAdsController.isRegisteredUser);

	app.route('/api/remove-marketinguser')
		.post(facebookAdsController.removeMarketingUser);

	app.route('/api/facebook/unlink-facebook')
		.get(facebookAdsController.unLinkFacebook);

	app.route('/api/facebook/get-currency')
		.get(facebookAdsController.getCurrencyFacebook)
	app.route('/api/facebook/get-permission')
		.get(facebookAdsController.getPermission);
		/////////////////////////////////
		///////////page builder api//////
	app.route('/api/get-brandlist-for-grpah')
		.get(facebookAdsController.getBrandListGrapgh);
	app.route('/api/get-branddetail-for-grpah')
		.get(facebookAdsController.getAllBrandGrapgh);
	app.route('/api/get-products/grpahapi')
		.get(facebookAdsController.getAllProductsGrapgh);
	app.route('/api/allcampaigns/grpahapi')
		.get(facebookAdsController.getAllcampaignsGrapgh);

	app.route('/api/grape/template')
		.post(grapeController.postGrapeTemplate);
	app.route('/api/grape/save')
		.post(grapeController.postGrapeSave);
	app.route('/api/grape/publish')
		.post(grapeController.postGrapePublish);
	app.route('/api/grape/can-publish')
		.post(grapeController.postGrapeCanPublish);
	app.route('/api/grape/image-upload')
		.post(grapeController.postGrapeImgUpload);
	app.route('/api/grape/site-name')
		.post(multipartMiddleware,grapeController.postSiteName);
	app.route('/api/grape/change-template')
		.post(multipartMiddleware,grapeController.changeTemplate);
	app.route('/api/grape/custom-page')
		.post(multipartMiddleware,grapeController.createCustomPage)
	app.route('/api/grape/template-chooser')
		.post(multipartMiddleware,grapeController.templateChooser);
	app.route('/api/grapes/image-upload')
		.post(multipartMiddleware,grapeController.uploadNewImage);
	app.route('/api/grapes/getAllNewImage')
		.get(multipartMiddleware,grapeController.getAllNewImage)
	app.route('/api/grape/wildcards')
		.get(grapeController.wildcards);

		/////////////////////////////////
		///////////////////////////////////////////
		//////////////// COUNTR API ///////////////
		///////////////////////////////////////////
		//Authentication
	app.route('/api/countr/token')
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.getAccessToken);
		//Categories/Collections
	app.route('/api/countr/sync-collections')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.syncCollections)
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.syncCollections);

	app.route('/api/countr/categories/:id?/:shopId?')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getCategories)
		.put(ensureAuthenticated(['retailer', 'admin']), countrController.updateCategory)
		.delete(ensureAuthenticated(['retailer', 'admin']), countrController.deleteCategory);
	app.route('/api/countr/delete-all-categories')
		.post(ensureAuthenticated(['admin']), countrController.deleteAllCategories);

		//Taxes
	app.route('/api/countr/taxes/:id?/:shopId?')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getTaxes)
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.createNewTax)
		.delete(ensureAuthenticated(['retailer', 'admin']), countrController.deleteTax);

		//Store
	app.route('/api/countr/shops/:id?')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getStores)
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.createStore);

		//Products
	app.route('/api/countr/sync-products')
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.syncProducts);
	app.route('/api/countr/get-specific-store-item/:id/:item')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getSpecificStoreItem);
	app.route('/api/countr/products/count')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getProductCount);
	app.route('/api/countr/products')
		.put(ensureAuthenticated(['retailer', 'admin']), countrController.updateProduct)
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getProduct);
	app.route('/api/countr/get-products-from-countr')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.addProductsFromCountr);
		//The delete all products route is only available for Admin users
	app.route('/api/countr/delete-all-products')
		.post(ensureAuthenticated(['admin']), countrController.deleteAllProducts);

		//Customers
	app.route('/api/countr/customers/:id?/:shopId?')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getCustomers);
	app.route('/api/countr/sync-customers')
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.syncCustomers);

		//Webhooks
	app.route('/api/countr/webhook-setup')
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.webhookSetup);
		//THIS LINK IS OPEN
	app.route('/api/countr/hook/:topic/:shopid')
		.post(countrController.handleWebhook);
	app.route('/api/countr/webhooks/:id?/:shopId?')
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.createWebhook)
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getWebhooks)
		.delete(ensureAuthenticated(['retailer', 'admin']), countrController.deleteWebhook);

		//Transactions
	app.route('/api/countr/transactions')
		.get(ensureAuthenticated(['retailer', 'admin']), countrController.getTransactions);
	app.route('/api/countr/send-transaction-mail')
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.sendTransactionMail);
	app.route('/api/countr/sync-transactions')
		.post(ensureAuthenticated(['retailer', 'admin']), countrController.syncTransactions);

		//signup
	app.route('/api/countr/signup')
		.post(ensureAuthenticated(['retailer']), countrController.signup);

		// HTTP 404 response as fallback for all non-existing pages with any kind of HTTP request type (GET, POST, PUT, DELETE, etc.)
	app.use('/api/*', mainController.get404);

		//Serve the index.html for all other routes. This route MUST be the last route!
	app.get('/*', function(req, res, next){
		res.sendFile('index.html', {root: path.join(__dirname, '..', '..', 'app', 'dist')})
	})
}
