const path = require('path');
const request = require('request');

const User = require(path.join(__dirname, '..', 'models', 'user.model'));

const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const PinterestStrategy = require('passport-pinterest-oauth').OAuth2Strategy;
const pinterestService = require(path.join(__dirname, '..', 'services', 'pinterest.service'));

const accessTokenAdmin = settings.facebook.accessToken;
const accountId = "act_" + settings.facebook.advertiserId;
const userID = '1300398836738390';

module.exports = function (passport) {

  passport.serializeUser(function (user, done) {
    done(null, user);
  });
  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  //LocalStrategy for local logins
  passport.use('local', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
    function (username, password, done) {
      User.authenticate(username, password, function (err, user) {
        if (err) {
          return done(err);
        }

        if (!user) {
          return done('No matching record found', false);
        }
        user.userType = 'local';
        User.findByIdAndUpdate(user._id, { $set: user }, { new: true }, function (err, userUpdated) {
          if (err) {
            throw err;
          }
          return done(null, userUpdated);
        })
       
      });
    }
  ));

  passport.use('facebook', new FacebookStrategy({
    clientID: settings.facebook.appId,
    clientSecret: settings.facebook.appSecret,
    callbackURL: settings.facebook.callbackUrl,
    passReqToCallback: true,
    profileFields: ['id', 'first_name', 'last_name', 'gender', 'email']
  }, function (req, token, refreshToken, profile, done) {
    process.nextTick(function () {
      if(req.isAuthenticated() == false){

      var AdAccountDetails;
      if(profile.emails){
      User.findOne({ 'email': profile.emails[0].value }).exec(function (error, userdetail) {
        if (error)
          return done(err);

        if (userdetail) {
          User.findOne({ 'facebook.profileId': profile.id }).exec(function (err, user) {

            if (err)
              return done(err);

            getPageId(token, function (err, getpage) {
              var pageInfo = getpage.data;


              APICall(profile.id, token, function (err, result) {
                if (err) {

                } else {
                  AdAccountDetails = JSON.parse(result).adaccounts.data;

                  if (user) {
                    //User is already connected with facebook, but wants to reconnect, save the new token

                    user.facebook = {
                      "token": token,
                      "profileId": profile.id,
                      "adAccountDetails": AdAccountDetails,
                      "facebookPages": pageInfo
                    };
                    user.userType = 'facebook';
                    User.findByIdAndUpdate(user._id, { $set: user }, { new: true }, function (err, userUpdated) {
                      if (err) {
                        throw err;
                      }

                      return done(null, userUpdated);
                    })

                  } else {

                    let facebook = {
                      "profileId": profile.id,
                      "token": token,
                      "adAccountDetails": AdAccountDetails,
                      "facebookPages": pageInfo
                    };
                    
                    User.findByIdAndUpdate(userdetail._id, { $set: { "facebook": facebook,'userType' : 'facebook' } }, { new: true }, function (err, data) {
                      if (err)
                        throw err;
                      return done(null, data);
                    });

                  }

                }
              })

            });
          })
        } else {
          getPageId(token, function (err, getpage) {
            var pageInfo = getpage.data;
           
            APICall(profile.id, token, function (err, result) {
              if (err) {

              } else {
                AdAccountDetails = JSON.parse(result).adaccounts.data;

                let data = {
                  "firstName": profile.name.givenName,
                  "lastName": profile.name.familyName,
                  "email": profile.emails[0].value,
                  "username": profile.emails[0].value,
                  "gender": profile.gender == "male" ? true : false,
                  "role" : "retailer",
                  "userType" : 'facebook',
                  "facebook": {
                    "profileId": profile.id,
                    "token": token,
                    "adAccountDetails": AdAccountDetails,
                    "facebookPages": pageInfo
                  }
                };

                var permissionData = {
                  'uid': userID,
                  'role': 1001,
                  'access_token': token
                }
                if (userID == profile.id) {
                  var newUser = new User(data);
                  newUser.save(function (err, dbdata) {
                    if (err)
                      throw err;
                    return done(null, dbdata);
                  })
                }
                else {
                  updatePermission(permissionData, AdAccountDetails[0].id, function (err, rslt) {
                    console.log('rslt', rslt);

                    if(req.user){
                      let facebook = {
                        "profileId": profile.id,
                        "token": token,
                        "adAccountDetails": AdAccountDetails,
                        "facebookPages": pageInfo
                      };
              
                      User.findByIdAndUpdate(req.user._id, { $set: { "facebook": facebook,"userType" : 'facebook' } }, { new: true }, function (err, data) {
                        if (err)
                          throw err;
                        return done(null, data);
                      });
                    }else{
                      console.log("New User", data);
                      
                          var newUser = new User(data);
                          newUser.save(function (err, dbdata) {
                          if (err)
                            throw err;
                          return done(null, dbdata);
                      })
                    }


                   

                  })
                }

                }
              })
            });
          }
        })
      } else {
        done(null, false, 'facebook login failed')
        // res.send('facebook login failed');
      }

    }else{
      console.log('else called in auth',req.user.email);
      //return done(null, null);


      User.findOne({ 'email': req.user.email }).exec(function (error, userdetail) {
        if (error)
          return done(err);

        if (userdetail) {
          User.findOne({ 'facebook.profileId': profile.id }).exec(function (err, user) {

            if (err)
              return done(err);

            getPageId(token, function (err, getpage) {
              var pageInfo = getpage.data;
              APICall(profile.id, token, function (err, result) {
                if (err) {

                } else {
                  AdAccountDetails = JSON.parse(result).adaccounts.data;

                  // if (user) {
                  //  // if(user.email == req.user.email){
                  //   //User is already connected with facebook, but wants to reconnect, save the new token

                  //   user.facebook = {
                  //     "token": token,
                  //     "profileId": profile.id,
                  //     "adAccountDetails": AdAccountDetails,
                  //     "facebookPages": pageInfo
                  //   };

                  //   User.findByIdAndUpdate(req.user._id, { $set: user }, { new: true }, function (err, userUpdated) {
                  //     if (err) {
                  //       throw err;
                  //     }

                  //     return done(null, userUpdated);
                  //   })
                    // }
                    // else{
                    //   done(null);
                    // }



                 // } else {

                    let facebook = {
                      "profileId": profile.id,
                      "token": token,
                      "adAccountDetails": AdAccountDetails,
                      "facebookPages": pageInfo
                    };

                    User.findByIdAndUpdate(userdetail._id, { $set: { "facebook": facebook,"userType" : 'facebook' } }, { new: true }, function (err, data) {
                      if (err)
                        throw err;
                      return done(null, data);
                    });

                 // }

                }
              })

            });
          })
        } 
      })

    }
    })

   }
  ))

  passport.use(new PinterestStrategy({
    clientID: settings.pinterest.clientId,
    clientSecret: settings.pinterest.clientSecret,
    //scope: settings.pinterest.scope,
    callbackURL: settings.pinterest.callbackUrl,
    // state: true,
    passReqToCallback: true
  }, function (req, accessToken, refreshToken, profile, done) {
    console.log("req", req, "accessToken", accessToken, "refreshToken", refreshToken, "profile", profile);
    process.nextTick(function () {

      //check if this user is already connected 
      User.findOne({ 'pinterest.profileId': profile.id }).exec(function (err, user) {
        if (err)
          return done(err);

        if (user) {
          return done(null, user);
        } else {
          //there is no user found with this facebook profileid, so we must find the profile of the loggedin user
          User.findOne({ _id: req.user._id }).exec(function (err, user) {
            if (err)
              return done(err);

            if (user) {
              user.pinterest = {
                profileId: profile.id,
                accessCode: accessToken
              }

              pinterestService.getAccessToken(accessToken, req.query.code, function (result) {
                console.log("accesToken", result);
              })

              user.save(function (err) {
                if (err)
                  throw err;

                return done(null, user);
              })
            }
          })
        }
      })
    })
  }))

  //TODO: Google Strategy
}

function APICall(id, token, callback) {
  var result;
  request('https://graph.facebook.com/v3.0/' + id + '?access_token=' + token + '&fields=adaccounts&method=get&pretty=0&sdk=joey', function (error, response, body) {
    result = body;
    callback(null, body);
  });

}

function getPageId(token, callback) {
  request('https://graph.facebook.com/v3.0/me/accounts?access_token=' + token + '&debug=all&format=json&method=get&pretty=0&suppress_http_code=1', function (error, response, body) {
    callback(null, JSON.parse(body));
  });
}

function updatePermission(data, id, callback) {
  request.post({
    headers: { 'content-type': 'application/json' },
    url: 'https://graph.facebook.com/v3.0/' + id + '/users',
    body: JSON.stringify(data),
    method: 'POST'
  }, function (error, response, body) {
    callback(null, body);
  });
}

function getCustomAudience(accessTokenAdmin, callback) {
  request('https://graph.facebook.com/v3.0/' + accountId + '/customaudiences?access_token=' + accessTokenAdmin + '&debug=all&format=json&method=get&pretty=0&suppress_http_code=1', function (error, response, body) {
  if(body == undefined){
    
  }else{
    callback(null, JSON.parse(body));
  }
  });
}

function postCustomAudience(data, callback) {
  request.post({
    headers: { 'content-type': 'application/json' },
    url: 'https://graph.facebook.com/v3.0/' + accountId + '/customaudiences',
    body: JSON.stringify(data),
    method: 'POST'
  }, function (error, response, body) {
    callback(null, body);
  });
}

getCustomAudience(accessTokenAdmin, function (err, audience) {
  if (audience.data) {
    if (audience.data.length == 0) {
      let audienceData = {
        'pixel_id': '198364724077161',
        'name': 'My New Website Custom Audience',
        'subtype': 'WEBSITE',
        'retention_days': '15',
        'rule': { "url": { "i_contains": "" } },
        'prefill': '1',
        'access_token': accessTokenAdmin
      };

    postCustomAudience(audienceData, function (err, audienceCustom) {
      console.log('audienceCustom',audienceCustom);
    });
  }
  }
  
});

