const path = require('path');
const fs = require('fs');
const ios = require('socket.io');
const express = require('express');
const bodyParser = require('body-parser');
const cron = require('node-cron');
const _ = require('lodash');

// Socket io variables
var users = [];
var messages = [];
var usersActivity = [];
var onlineClient = {};

// Return values from different config files will be added to this object so you can call config.[category].[property].
config = {};

// Save the app root directory to a global variable so it can be used in config files and other parts of the app. global.root is reserved, but global.path.root can be used without problems.
global.path = { root: path.resolve(__dirname) };

// Set environment and initialize environment-specific config variables
config.env = require(path.join(__dirname, 'config', 'env.config'));

// Set up database connection to use throughout the application
config.db = require(path.join(__dirname, 'config', 'db.config'));

//Make the settings in the environment config global available
global.settings = config.env.settings;

const mongoose = require('mongoose');
const chatList = mongoose.model('Chat');

// HTTP for development environment, HTTPS for production environment
var http = require('http');
var https = require('https');

// Set up debugging/logging for the development environment
var debug = require('debug')('http');

// Start the app using the Express settings/routines in express.config.
var app = require(path.join(__dirname, 'config', 'express.config'));

var router = require(path.join(__dirname, 'config', 'routes.config'));

router(app);

// Running in production mode: HTTPS only
if (config.env.name === 'production') {
    var credentials = {
        privateKey: fs.readFileSync('/etc/letsencrypt/live/prismanote.com/privkey.pem'),
        certificate: fs.readFileSync('/etc/letsencrypt/live/prismanote.com/fullchain.pem')
    };

    var server = https.createServer(credentials, app);
    server.listen(4443);
    server.on('error', onError);
    server.on('listening', onListen);

    var server2 = http.createServer(app);
    server2.listen(8080);

    var port = Number(5001);

    var ioapp = express();
    ioapp.use(express.static(path.join(__dirname, 'public')));
    ioapp.use(bodyParser.json({ extended: true }));       // to support JSON-encoded bodies
    ioapp.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies


    var ioserver = https.createServer(ioapp).listen(port, function () {
        console.log('Socket io listening on ' + port);
    });

    var io = ios.listen(ioserver);

    io.sockets.on('connection', function (socket) {
	    /*
		* Vlidate user id duplicate
		*/
        socket.on('chkUser', function (data) {
            var chk = users.indexOf(data.name);
            if (chk == (-1)) {
                var curdatetime = currentDateTime('netherlands', '2');
                users.push(data.name);
                var data = { name: data.name, msg: ' joined chat on ' + curdatetime + ' !', color: 'text-success' };
                usersActivity.push(data);
                onlineClient[data.name] = socket;
            }
            socket.emit('chkUser', chk);
            io.sockets.emit('totalOnlineUser', users, data.name);
        });

        socket.on('joined', function (data) {
            var curdatetime = currentDateTime('netherlands', '2');
            socket.username = data.name;
            socket.email = data.email;
            io.sockets.emit('totalOnlineUser', users, socket.username);
            socket.emit('myInfo', socket.username, curdatetime);
            io.sockets.emit('newOne', data, messages);
            io.sockets.emit('usersActivity', usersActivity, curdatetime);
        });

        socket.on('chatMsg', function (data) {
            var curdatetime = currentDateTime('netherlands', '2');
            var data = { name: socket.username + ' [' + curdatetime + '] ', msg: data.msg }
            messages.push(data);

            io.sockets.emit('msgEveryOne', data, curdatetime);

        });

        socket.on('disconnect', function (data) {

            var curdatetime = currentDateTime('netherlands', '2');
            if (socket.username != undefined) {
                var ax = users.indexOf(socket.username);
                users.splice(ax, 1);
                io.sockets.emit('totalOnlineUser', users, socket.username);
                data = { name: socket.username, msg: ' left chat on ' + curdatetime + ' !', color: 'text-danger' }
                usersActivity.push(data);

                io.sockets.emit('usersActivity', usersActivity, curdatetime);
                socket.emit('usersDisconnect');
                //socket.emit('disconnect',{});
            }
        });


        socket.on('typing', function (data) {
            io.sockets.emit('isTyping', { isTyping: data, user: socket.username });
        });

        socket.on('dateTimeUpdate', function (data) {
            socket.datetime = data.datatime;
        });
        ////// Create room /////
        socket.on('sendPrivateChat', function (data) {
            var socketTo = onlineClient[data.toName];
            var socketFrom = onlineClient[data.fromName];
            var group = 'private';
            socketTo.join(group);  //create room
            socketFrom.join(group);
            //io.sockets.in(data.name).emit('privateChat', data);    
            socketTo.emit('privateChat', data);
            socketFrom.emit('privateChat', data);

        });

        socket.on('loadChatHistory', function (key) {
            var clientSocket = onlineClient[key];

            if (clientSocket != null) {
                chatList.find({ $or: [{ from: socket.username, to: key }, { from: key, to: socket.username }] }, function (err, chatHistory) {
                    clientSocket.emit('loadhistorymsg', socket.username, key, chatHistory);
                });
            }
        });

        socket.on('sendprivatechat', function (key, msg) {
            var clientSocket = onlineClient[key];

            var curdatetime = currentDateTime('netherlands', '2');

            var ObjectId = require('mongodb').ObjectId;
            var chat = { _id: new ObjectId(), to: key, from: socket.username, timestamp: curdatetime.toString(), message: msg, rawData: msg }


            chatList.create(chat, function (err, chat) {
                if (err) {
                    console.error(err)
                } else {
                    //console.log('chat added');

                }
            });

            if (clientSocket == null) {


            } else {

                clientSocket.emit('getprivatemsg', socket.username, key, chat);
            }
        });


    });

}else if(config.env.name === 'staging'){

    var credentials = {
        privateKey: fs.readFileSync('/etc/letsencrypt/live/prismanote.it/privkey.pem'),
        certificate: fs.readFileSync('/etc/letsencrypt/live/prismanote.it/fullchain.pem')
    };

    var server = https.createServer(credentials, app);
    server.listen(4443);
    server.on('error', onError);
    server.on('listening', onListen);

    var server2 = http.createServer(app);
    server2.listen(8080);

    var port = Number(5001);

    var ioapp = express();
    ioapp.use(express.static(path.join(__dirname, 'public')));
    ioapp.use(bodyParser.json({ extended: true }));       // to support JSON-encoded bodies
    ioapp.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies


    var ioserver = https.createServer(ioapp).listen(port, function () {
        console.log('Socket io listening on ' + port);
    });

    var io = ios.listen(ioserver);

    io.sockets.on('connection', function (socket) {
	    /*
		* Vlidate user id duplicate
		*/
        socket.on('chkUser', function (data) {
            var chk = users.indexOf(data.name);
            if (chk == (-1)) {
                var curdatetime = currentDateTime('netherlands', '2');
                users.push(data.name);
                var data = { name: data.name, msg: ' joined chat on ' + curdatetime + ' !', color: 'text-success' };
                usersActivity.push(data);
                onlineClient[data.name] = socket;
            }
            socket.emit('chkUser', chk);
            io.sockets.emit('totalOnlineUser', users, data.name);
        });

        socket.on('joined', function (data) {
            var curdatetime = currentDateTime('netherlands', '2');
            socket.username = data.name;
            socket.email = data.email;
            io.sockets.emit('totalOnlineUser', users, socket.username);
            socket.emit('myInfo', socket.username, curdatetime);
            io.sockets.emit('newOne', data, messages);
            io.sockets.emit('usersActivity', usersActivity, curdatetime);
        });

        socket.on('chatMsg', function (data) {
            var curdatetime = currentDateTime('netherlands', '2');
            var data = { name: socket.username + ' [' + curdatetime + '] ', msg: data.msg }
            messages.push(data);

            io.sockets.emit('msgEveryOne', data, curdatetime);

        });

        socket.on('disconnect', function (data) {

            var curdatetime = currentDateTime('netherlands', '2');
            if (socket.username != undefined) {
                var ax = users.indexOf(socket.username);
                users.splice(ax, 1);
                io.sockets.emit('totalOnlineUser', users, socket.username);
                data = { name: socket.username, msg: ' left chat on ' + curdatetime + ' !', color: 'text-danger' }
                usersActivity.push(data);

                io.sockets.emit('usersActivity', usersActivity, curdatetime);
                socket.emit('usersDisconnect');
                //socket.emit('disconnect',{});
            }
        });


        socket.on('typing', function (data) {
            io.sockets.emit('isTyping', { isTyping: data, user: socket.username });
        });

        socket.on('dateTimeUpdate', function (data) {
            socket.datetime = data.datatime;
        });
        ////// Create room /////
        socket.on('sendPrivateChat', function (data) {
            var socketTo = onlineClient[data.toName];
            var socketFrom = onlineClient[data.fromName];
            var group = 'private';
            socketTo.join(group);  //create room
            socketFrom.join(group);
            //io.sockets.in(data.name).emit('privateChat', data);    
            socketTo.emit('privateChat', data);
            socketFrom.emit('privateChat', data);

        });

        socket.on('loadChatHistory', function (key) {
            var clientSocket = onlineClient[key];

            if (clientSocket != null) {
                chatList.find({ $or: [{ from: socket.username, to: key }, { from: key, to: socket.username }] }, function (err, chatHistory) {
                    clientSocket.emit('loadhistorymsg', socket.username, key, chatHistory);
                });
            }
        });

        socket.on('sendprivatechat', function (key, msg) {
            var clientSocket = onlineClient[key];

            var curdatetime = currentDateTime('netherlands', '2');

            var ObjectId = require('mongodb').ObjectId;
            var chat = { _id: new ObjectId(), to: key, from: socket.username, timestamp: curdatetime.toString(), message: msg, rawData: msg }


            chatList.create(chat, function (err, chat) {
                if (err) {
                    console.error(err)
                } else {
                    //console.log('chat added');

                }
            });

            if (clientSocket == null) {


            } else {

                clientSocket.emit('getprivatemsg', socket.username, key, chat);
            }
        });


    });


    //Development and Testing mode
} else {
    var server = http.createServer(app);
    server.listen(config.env.port);
    server.on('error', onError);
    server.on('listening', onListen);

    var port = Number(5001);

    var ioapp = express();
    ioapp.use(express.static(path.join(__dirname, 'public')));
    ioapp.use(bodyParser.json({ extended: true }));       // to support JSON-encoded bodies
    ioapp.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies


    var ioserver = http.createServer(ioapp).listen(port, function () {
        console.log('Socket io listening on ' + port);
    });

    var io = ios.listen(ioserver);

    io.sockets.on('connection', function (socket) {
		/*
		* Vlidate user id duplicate
		*/
        socket.on('chkUser', function (data) {
            var chk = users.indexOf(data.name);
            if (chk == (-1)) {
                var curdatetime = currentDateTime('netherlands', '2');
                users.push(data.name);
                var data = { name: data.name, msg: ' joined chat on ' + curdatetime + ' !', color: 'text-success' };
                usersActivity.push(data);
                onlineClient[data.name] = socket;
            }
            socket.emit('chkUser', chk);
            io.sockets.emit('totalOnlineUser', users, data.name);
        });

        socket.on('joined', function (data) {
            var curdatetime = currentDateTime('netherlands', '2');
            socket.username = data.name;
            socket.email = data.email;
            io.sockets.emit('totalOnlineUser', users, socket.username);
            socket.emit('myInfo', socket.username, curdatetime);
            io.sockets.emit('newOne', data, messages);
            io.sockets.emit('usersActivity', usersActivity, curdatetime);
        });

        socket.on('chatMsg', function (data) {
            var curdatetime = currentDateTime('netherlands', '2');
            var data = { name: socket.username + ' [' + curdatetime + '] ', msg: data.msg }
            messages.push(data);

            io.sockets.emit('msgEveryOne', data, curdatetime);

        });

        socket.on('disconnect', function (data) {

            var curdatetime = currentDateTime('netherlands', '2');
            if (socket.username != undefined) {
                var ax = users.indexOf(socket.username);
                users.splice(ax, 1);
                io.sockets.emit('totalOnlineUser', users, socket.username);
                data = { name: socket.username, msg: ' left chat on ' + curdatetime + ' !', color: 'text-danger' }
                usersActivity.push(data);

                io.sockets.emit('usersActivity', usersActivity, curdatetime);
                socket.emit('usersDisconnect');
                //socket.emit('disconnect',{});
            }
        });


        socket.on('typing', function (data) {
            io.sockets.emit('isTyping', { isTyping: data, user: socket.username });
        });

        socket.on('dateTimeUpdate', function (data) {
            socket.datetime = data.datatime;
        });
        ////// Create room /////
        socket.on('sendPrivateChat', function (data) {
            var socketTo = onlineClient[data.toName];
            var socketFrom = onlineClient[data.fromName];
            var group = 'private';
            socketTo.join(group);  //create room
            socketFrom.join(group);
            //io.sockets.in(data.name).emit('privateChat', data);    
            socketTo.emit('privateChat', data);
            socketFrom.emit('privateChat', data);

        });

        socket.on('loadChatHistory', function (key) {
            var clientSocket = onlineClient[socket.username];

            if (clientSocket != null) {

                chatList.find({ $or: [{ from: socket.username, to: key }, { from: key, to: socket.username }] }, function (err, chatHistory) {
                    clientSocket.emit('loadhistorymsg', socket.username, key, chatHistory);
                });

            } else {
                var socketClient = onlineClient[key];
                chatList.find({ $or: [{ from: socket.username, to: key }, { from: key, to: socket.username }] }, function (err, chatHistory) {
                    socketClient.emit('loadhistorymsg', socket.username, key, chatHistory);
                });
            }

        });


        socket.on('sendprivatechat', function (key, msg) {
            var clientSocket = onlineClient[key];

            var curdatetime = currentDateTime('netherlands', '2');

            var ObjectId = require('mongodb').ObjectId;
            var chat = { _id: new ObjectId(), to: key, from: socket.username, timestamp: curdatetime.toString(), message: msg, rawData: msg }


            chatList.create(chat, function (err, chat) {
                if (err) {
                    console.error(err)
                } else {
                    //console.log('chat added');

                }
            });

            if (clientSocket == null) {

                socket.emit('getprivatemsg', socket.username, key, chat);
            } else {

                clientSocket.emit('getprivatemsg', socket.username, key, chat);
            }
        });

    });


}

// Error trapping for HTTP/HTTPS server
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    if (typeof port === 'string') {
        var bind = 'pipe ' + config.env.port;
    } else {
        var bind = 'port ' + config.env.port;
    }

    // Handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges.');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use.');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

// Print message to console and debugger when the HTTP/HTTPS server is listening
function onListen() {
    var addr = server.address();
    if (typeof addr === 'string') {
        var bind = 'pipe ' + addr;
    } else {
        var bind = 'port ' + addr.port;
    }
    if (config.env.name === 'production') {
        console.log('HTTPS server is listening on ' + bind + '.');
        debug('HTTPS server is listening on ' + bind + '.');

    } else {
        console.log('HTTP server is listening on ' + bind + '.');
        debug('HTTP server is listening on ' + bind + '.');
    }

    if (config.env.name != 'development') {
        //Cronjobs are only needed on not localhost systems
        // const productsController = require(path.join(__dirname, 'controllers', 'products.controllers'));
        const updateShopsForProductUpdates = require(path.join(__dirname, 'controllers', 'products.controllers'));

        var mycron = cron.schedule('59 0 * * *', function () {
            // productsController.productReplacementAfterMerge();
            console.log("Product Update Logs CRONJOB Started")
            updateShopsForProductUpdates();    
        });
        mycron.start();
        console.log("Product merging cronjob is set to run at 12:00 every morning.");
    }

}

// current date and time of any country!
function currentDateTime(city, offset) {
    // create Date object for current location
    var d = new Date();

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    // create new Date object for different city
    // using supplied offset
    var currentdate = new Date(utc + (3600000 * offset));

    // return time as a string

    var datetime = currentdate.getDate() + '-'
        + (currentdate.getMonth() + 1) + '-'
        + currentdate.getFullYear() + ' '
        + currentdate.getHours() + ':'
        + currentdate.getMinutes() + ':'
        + currentdate.getSeconds();
    return datetime;

}