# A comment by a user or shop owner. This is basically a more concise, basic version of a post that can be added to (for example) news stories or reviews.
type Comment implements Post, Metadata {

	_id: ID!

	# The author(s) of the post. Multiple authors can be added, but every post needs to have at least one author.
	author: [Account!]!

	# The previous versions of this post. This way of tracking edits also makes it possible to change the publication type (i.e. news item -> campaign task).
	history: [Post!]

	# The content of the post. Can include html, but no interactive content like JavaScript or VBScript. This should not be compiled and executed by Angular.
	content: [Translation!]!

	# The date this object was added to the database. Creation date should never be null. If it is, it should be updated to the current date. That way, the field can be Non-Nullable and backwards compatible.
	dateCreated: Date!

	# The user who initially created this object in the database. Creator should always be saved on creation, but it can be null for backwards compatibility.
	createdBy: Account

	# The date that one or more properties of the implementing object were most recently changed. This is a primitive way to keep track of changes without maintaining a complete edit history (which is not relevant for most objects). News items, comments, etc will have their own way of tracking changes.
	dateLastModified: Date

	# The user who last modified one or more properties of the implementing object. More detailed edit tracking for reviews, posts, comments etc. should be included in their respective interface(s) and/or type(s).
	lastModifiedBy: Account

	# Instead of hard deleting objects, set dateDeleted and deletedBy to their respective values. This enables the recovery of deleted objects.
	dateDeleted: Date

	# Instead of hard deleting objects, set dateDeleted and deletedBy to their respective values. This enables the recovery of deleted objects.
	deletedBy: Account

	# Toggle to hide/unhide any object. This ensures that hiding/unhiding is handled the same way across the platform (at least as far as the API is concerned).
	hidden: Boolean!
}
