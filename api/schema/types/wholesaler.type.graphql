# A wholesaler distributes products from the manufacturers to the retailers.
type Wholesaler implements Company, Metadata, Name {

	_id: ID!

	# The name of an object. The nameslug is generated automatically based on the name. The name may be updated later on, but the nameSlug should always stay the same to ensure that permalinks will always resolve to the right page.
	name: [Translation!]!

	# The nameslug is generated automatically based on the name. This field should always stay the same to ensure that permalinks will always resolve to the right page.
	nameSlug: [Translation!]!

	# The brands that are available to buy wholesale at this company.
	brands: [Brand!]

	# If the brand is featured across the platform. This could be a premium feature for brands. Defaults to false.
	isFeatured: Boolean!

	# The year this company (or public brand name) was founded. Not known for all brands, so non-null values can not be guaranteed.
	foundingYear: Int

	# The primary category/categories in which this brand produces products. This field uses an enum value (WATCHES, JEWELLERY, etc) that can be expanded if necessary.
	categories: [BRAND_CATEGORY!]!

	# The manufacturer for this brand. TODO: Is it possible that a brand has multiple manufacturers?
	manufacturer: String

	# If the brand is restricting sales of their products to official dealers. TODO: Is this description correct?
	restricted: Boolean!

	# A list of images for this brand. Usually promotional or atmosphere pictures, the logo, and product pictures.
	images: [Image!]

	# Pretty much self-explanatory. Note: this is the contact info email address, NOT for authorization. Companies are not accounts.
	email: String!

	# Web address for this company. Usually the (international) homepage.
	website: String

	# A list of phone numbers for contacting this company. This can be useful if a user has a home, mobile and work phone for example. The list can be null, but it cannot contain null values.
	phone: [Phone!]

	# A list of addresses for this company. This can be useful if a company has different visiting and mailing addresses, or multiple offices/shops in different places. The list can be null, but it cannot contain null values.
	address:	[Address!]

	# Government business registration number.
	kvkNumber: String

	# Tax registration number.
	btwNumber: String

	# A promotional text or a short description of the company. Can be displayed on brand or shop pages for example.
	description: [Translation!]

	# The date this object was added to the database. Creation date should never be null. If it is, it should be updated to the current date. That way, the field can be Non-Nullable and backwards compatible.
	dateCreated: Date!

	# The user who initially created this object in the database. Creator should always be saved on creation, but it can be null for backwards compatibility.
	createdBy: Account

	# The date that one or more properties of the implementing object were most recently changed. This is a primitive way to keep track of changes without maintaining a complete edit history (which is not relevant for most objects). News items, comments, etc will have their own way of tracking changes.
	dateLastModified: Date

	# The user who last modified one or more properties of the implementing object. More detailed edit tracking for reviews, posts, comments etc. should be included in their respective interface(s) and/or type(s).
	lastModifiedBy: Account

	# Instead of hard deleting objects, set dateDeleted and deletedBy to their respective values. This enables the recovery of deleted objects.
	dateDeleted: Date

	# Instead of hard deleting objects, set dateDeleted and deletedBy to their respective values. This enables the recovery of deleted objects.
	deletedBy: Account

	# Toggle to hide/unhide any object. This ensures that hiding/unhiding is handled the same way across the platform (at least as far as the API is concerned).
	hidden: Boolean!
}
