const gulp = require('gulp');
const uglify = require('gulp-uglify');
const util = require('gulp-util');
// const pump = require('pump');
const pug = require('gulp-pug2');
const stylus = require('gulp-stylus');
const concat = require('gulp-concat');
const mongobackup = require('mongobackup');
const clean = require('gulp-clean');
const sourcemaps = require('gulp-sourcemaps');
const rimraf = require('gulp-rimraf');

const development = true;

gulp.task('all', function() {
	gulp.start('scripts-minify', 'libs-minify', 'views-compile', 'index-compile', 'stylus', 'move-fonts', 'move-images', 'scripts-other');
})

gulp.task('default', function() {
	gulp.watch([
		'app/src/app.js',
		'app/src/routes/*.js',
		'app/src/controllers/**/*.js',
		'app/src/directives/*.js',
		'app/src/filters/*.js',
		'app/src/services/*.js'], ['scripts-minify']);
	gulp.watch([
		'app/src/libs/*.js'], ['libs-minify']);
	gulp.watch([
		'app/src/views/**/*.pug'], ['views-compile']);
	gulp.watch([
		'app/src/views/index.pug'], ['index-compile']);
	gulp.watch([
		'app/src/stylesheets/**/*.styl',
		'app/src/stylesheets/**/*.css'], ['stylus']);
	gulp.watch([
		'app/src/fonts/**/*'], ['move-fonts']);
	gulp.watch([
		'app/src/images/**/*'], ['move-images']);
	gulp.watch([
		'app/src/libs/scripts/**/*'], ['scripts-other']);
})

gulp.task('stylus', function() {
	return gulp.src([
		'app/src/stylesheets/styl/style.styl'
		])
	.pipe(stylus({
		'compress': development,
		'include css': true
	}))
	.pipe(gulp.dest('app/dist/inc/'))
})

gulp.task('views-compile', function() {
	return gulp.src('app/src/views/**/*.pug')
	.pipe(pug({
		pretty: development
	}))
	.pipe(gulp.dest('app/dist/views/'))
})

gulp.task('index-compile', function() {
	return gulp.src('app/src/views/index.pug')
	.pipe(pug({
		pretty: development
	}))
	.pipe(gulp.dest('app/dist/'))
})

gulp.task('scripts', function() {
	return gulp.src([
		'app/src/app.js',
		'app/src/routes/*.js',
		'app/src/controllers/**/*.js',
		'app/src/directives/*.js',
		'app/src/filters/*.js',
		'app/src/services/*.js',
		])
	.pipe(concat('scripts.js'))
	.pipe(gulp.dest('app/dist/'))
})

gulp.task('scripts-minify', function() {
	return gulp.src([
		'app/src/app.js',
		'app/src/routes/*.js',
		'app/src/controllers/**/*.js',
		'app/src/directives/*.js',
		'app/src/filters/*.js',
		'app/src/services/*.js',
		])
	.pipe(sourcemaps.init({loadMaps:true, identityMap:true}))
	.pipe(concat('scripts.min.js'))
	.pipe(uglify().on('error', util.log))
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('app/dist/inc/'))
});

gulp.task('libs', function() {
	return gulp.src([
		'app/src/libs/!(_**)*.js',
		'app/src/libs/_angular-translate-storage-cookie.js',
		'app/src/libs/_angular-translate-storage-local.js',
		'app/src/libs/jquery.toast.min.js',
		'app/src/libs/_angular-translate-handler-log.js'
		])
	.pipe(concat('libs.js'))
	.pipe(gulp.dest('app/dist/inc/'))
});

gulp.task('libs-minify', function() {
	return gulp.src([
		'app/src/libs/!(_**)*.js',
		'app/src/libs/_angular-translate-storage-cookie.js',
		'app/src/libs/_angular-translate-storage-local.js',
		'app/src/libs/jquery.toast.min.js',
		'app/src/libs/_angular-translate-handler-log.js'
		])
	.pipe(sourcemaps.init({loadMaps:true, identityMap:true}))
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('app/dist/inc/'))
});

gulp.task('scripts-other', function(){
	return gulp.src([
		'app/src/libs/scripts/*.js'
		])
	.pipe(gulp.dest('app/dist/inc/scripts/'))
})

gulp.task('move-fonts', function() {
	return gulp.src('app/src/fonts/**/*')
	.pipe(gulp.dest('app/dist/fonts'))
})

gulp.task('move-images', function() {
	return gulp.src('app/src/images/**/*')
	.pipe(gulp.dest('app/dist/images'))
})

gulp.task('mongodump', ['clean-dump-folder'], function(){
	//if the options --c passed, only that collection will be dumped, eg: --c users
	var collection;
	var option, i = process.argv.indexOf("--c");
	if(i>-1) {
		collection = process.argv[i+1];	
	}
	mongobackup.dump({
		host: 'localhost',
		db: 'prismanote',
		out: './mongodump',
		collection: collection ? collection : false
	})
})

gulp.task('mongorestore', function(){
	//restores all collections in ./mongodump/prismanote
	mongobackup.restore({
		host: 'localhost',
		db: 'prismanote',
		path: './mongodump/prismanote',
	})
})

gulp.task('clean-dump-folder', function(){
	return gulp.src('./mongodump', {read: false})
	.pipe(rimraf());
})

gulp.task('publish', function(){
	return gulp.src([
		'./app/src/',
		'./app/dist/scripts.min.js.map',
		'./app/dist/libs.min.js.map'
		], {read:false})
	.pipe(rimraf());
})
