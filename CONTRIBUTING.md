# Contributing to PrismaNote #

## Table of Contents ##

- [Issue tracker](#issue-tracker)
- [Commit guidelines](#commit-guidelines)

## Issue tracker ##

We use the JIRA issue tracker for this project.

*TODO: Add more details on how to report or fix bugs, suggested changes, and other issues.*

## Commit guidelines ##

1. Remove console.log() statements before committing new code unless they are required for logging purposes in the production environment.
2. Use descriptive and comprehensive commit messages, so it’s clear for other developers what was changed or added in your commit.
3. Smaller commits are often better. Try to keep the number of changes and the number of files per commit to a minimum, but group changes together if they are all part of the same fix or feature. As a rule of thumb: every fix and every non-breaking addition or removal of a feature should be committed separately as much as possible.
4. Make sure your commit doesn’t break the existing code before pushing. Otherwise other developers might have to solve the issue(s) in your code first after pulling before they can continue working on their own tasks.
5. If you do have to commit a non-backwards compatible change that you expect to cause problems for other developers, alert them beforehand and tell them how to resolve any issues they may encounter after pulling. That way they can decide at what time they want to pull your changes and solve the issues if needed. Keep in mind that troubleshooting your own code is much more efficient than troubleshooting someone else’s code.
6. Don’t comment out parts of the code unless you are planning on using them later. Removed or modified code can always be restored using git if necessary.
7. For complex functions or code segments, add comments to explain what the code does, so the other developers don’t have to figure that out for themselves.
8. Use asynchronous code wherever possible, because due to the way Node.js works, synchronous code slows down the website if there are a lot of visitors at the same time.
9. Don’t make new MongoDB collections if it’s not necessary. For each unique data/object type (users, products, shops, …) there should only be a single MongoDB collection.
10. Split up your code into multiple files if possible, shorter files are much more manageable than if a lot of different functions/modules are all in the same (possibly very big) file.
11. Try to minimize the number of installed npm packages and avoid installing new ones if they’re not necessary. Fewer packages makes the project less error-prone and easier to install/update.
12. Make sure filenames are all in the same style. Look at other files in the same folder and in the rest of the project to see if it should be stylized with underscores, dashes, and/or capital letters, etc. For example: *.js and *.pug files generally use only lowercase letters, where multiple words are connected by dashes.
13. Use either 4 spaces or 1 tab for indentation, but don’t mix these styles within the same file.